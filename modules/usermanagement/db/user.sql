

-- ----------------------------
-- Sequence structure for role_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."role_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for role_permission_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."role_permission_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;


-- ----------------------------
-- Sequence structure for token_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."token_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for user_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."user_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for user_role_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."user_role_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for kldi
-- ----------------------------

CREATE TABLE "public"."kldi" (
  "id" varchar(30) NOT NULL COLLATE "pg_catalog"."default",
  "nama_instansi" varchar(255) COLLATE "pg_catalog"."default",
  "alamat" varchar(255) COLLATE "pg_catalog"."default",
  "jenis" varchar(255) COLLATE "pg_catalog"."default"
)
;

ALTER TABLE "public"."kldi" ADD CONSTRAINT "kldi_id" PRIMARY KEY ("id");

-- ----------------------------
-- Table structure for permission
-- ----------------------------

-- ----------------------------
-- Sequence structure for permission_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."permission_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;


CREATE TABLE "public"."permission" (
  "id" int4 NOT NULL DEFAULT nextval('public.permission_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO "public"."permission" VALUES (1, 'superPermission');
INSERT INTO "public"."permission" VALUES (2, 'adminUserPermission');
INSERT INTO "public"."permission" VALUES (3, 'adminUserInputPermission');
INSERT INTO "public"."permission" VALUES (4, 'endUserPermission');
INSERT INTO "public"."permission" VALUES (5, 'contentPermission');
INSERT INTO "public"."permission" VALUES (6, 'adminContentPermission');
INSERT INTO "public"."permission" VALUES (7, 'adminContentInputPermission');

-- ----------------------------
-- Table structure for role
-- ----------------------------
CREATE TABLE "public"."role" (
  "id" int4 NOT NULL DEFAULT nextval('public.role_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "desc" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
  "created_by" int4,
  "updated_at" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
  "updated_by" int4,
  "deleted_at" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
  "deleted_by" int4
)
;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO "public"."role" VALUES (1, 'Super Admin', 'this is super admin', '2019-08-28 10:28:51.129933', NULL, '2019-08-28 10:28:51.129933', NULL, NULL, NULL);
INSERT INTO "public"."role" VALUES (2, 'Admin K/L', 'this is Admin K/L', '2019-08-28 10:28:51.131777', NULL, '2019-08-28 10:28:51.131777', NULL, NULL, NULL);
INSERT INTO "public"."role" VALUES (3, 'Admin Konten', 'this is admin konten', '2019-08-28 10:28:51.132852', NULL, '2019-08-28 10:28:51.132852', NULL, NULL, NULL);
INSERT INTO "public"."role" VALUES (4, 'Public User', 'this is public user', '2019-08-28 10:28:51.133711', NULL, '2019-08-28 10:28:51.133711', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for role_permission
-- ----------------------------
CREATE TABLE "public"."role_permission" (
  "id" int4 NOT NULL DEFAULT nextval('public.role_permission_id_seq'::regclass),
  "role_id" int4,
  "permission_id" int4
)
;

-- ----------------------------
-- Records of role_permission
-- ----------------------------
INSERT INTO "public"."role_permission" VALUES (2, 2, 2);
INSERT INTO "public"."role_permission" VALUES (1, 1, 1);
INSERT INTO "public"."role_permission" VALUES (3, 2, 3);
INSERT INTO "public"."role_permission" VALUES (4, 1, 2);
INSERT INTO "public"."role_permission" VALUES (5, 3, 5);
INSERT INTO "public"."role_permission" VALUES (6, 3, 6);
INSERT INTO "public"."role_permission" VALUES (7, 3, 7);
INSERT INTO "public"."role_permission" VALUES (8, 4, 4);

-- ----------------------------
-- Table structure for token
-- ----------------------------
CREATE TABLE "public"."token" (
  "id" int4 NOT NULL DEFAULT nextval('public.token_id_seq'::regclass),
  "email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "token" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "expired_date" timestamp(6),
  "created_by" int4,
  "created_at" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
  "updated_by" int4,
  "updated_at" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
  "deleted_by" int4,
  "deleted_at" timestamp(6) DEFAULT CURRENT_TIMESTAMP
)
;

-- ----------------------------
-- Table structure for user
-- ----------------------------
CREATE TABLE "public"."user" (
  "id" int4 NOT NULL DEFAULT nextval('public.user_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "password" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "phone" varchar(32) COLLATE "pg_catalog"."default" NOT NULL,
  "created_by" int4,
  "created_at" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
  "updated_by" int4,
  "updated_at" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
  "deleted_by" int4,
  "deleted_at" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
  "npwp" varchar(30) COLLATE "pg_catalog"."default",
  "nip" varchar(255) COLLATE "pg_catalog"."default",
  "approve" int4,
  "upload_file" varchar(255) COLLATE "pg_catalog"."default",
  "kldi_id" char(20) COLLATE "pg_catalog"."default",
  "avatar" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
CREATE TABLE "public"."user_role" (
  "id" int4 NOT NULL DEFAULT nextval('public.user_role_id_seq'::regclass),
  "user_id" int4,
  "role_id" int4
)
;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO "public"."user_role" VALUES (1, 1, 1);
INSERT INTO "public"."user_role" VALUES (2, 2, 1);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------

ALTER SEQUENCE "public"."permission_id_seq"
OWNED BY "public"."permission"."id";
SELECT setval('"public"."permission_id_seq"', 2, false);
ALTER SEQUENCE "public"."role_id_seq"
OWNED BY "public"."role"."id";
SELECT setval('"public"."role_id_seq"', 2, false);
ALTER SEQUENCE "public"."role_permission_id_seq"
OWNED BY "public"."role_permission"."id";
SELECT setval('"public"."role_permission_id_seq"', 2, false);
SELECT setval('"public"."token_id_seq"', 19, true);
ALTER SEQUENCE "public"."user_id_seq"
OWNED BY "public"."user"."id";
SELECT setval('"public"."user_id_seq"', 11, true);
ALTER SEQUENCE "public"."user_role_id_seq"
OWNED BY "public"."user_role"."id";
SELECT setval('"public"."user_role_id_seq"', 7, true);

-- ----------------------------
-- Remove HTML Tags to String for Landing Page Content
-- ----------------------------
CREATE OR REPLACE FUNCTION strip_tags(TEXT) RETURNS TEXT AS $$
    SELECT regexp_replace($1, '<[^>]*>', '', 'g')
$$ LANGUAGE SQL;

-- ----------------------------
-- Primary Key structure for table permission
-- ----------------------------
ALTER TABLE "public"."permission" ADD CONSTRAINT "permission_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table role
-- ----------------------------
ALTER TABLE "public"."role" ADD CONSTRAINT "role_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table role_permission
-- ----------------------------
ALTER TABLE "public"."role_permission" ADD CONSTRAINT "role_permission_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table token
-- ----------------------------
ALTER TABLE "public"."token" ADD CONSTRAINT "token_email_uindex" UNIQUE ("email");

-- ----------------------------
-- Primary Key structure for table token
-- ----------------------------
ALTER TABLE "public"."token" ADD CONSTRAINT "token_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table user
-- ----------------------------
CREATE UNIQUE INDEX "user_email_uindex" ON "public"."user" USING btree (
  "email" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table user
-- ----------------------------
ALTER TABLE "public"."user" ADD CONSTRAINT "user_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table user_role
-- ----------------------------
ALTER TABLE "public"."user_role" ADD CONSTRAINT "user_role_pkey" PRIMARY KEY ("id");

ALTER TABLE "public"."role" RENAME COLUMN "desc" TO "description";

ALTER TABLE "public"."token" ADD COLUMN "token_type" character varying(20) DEFAULT 'REGISTRATION';

-- ----------------------------
-- Drop Uniques structure for table token
-- ----------------------------
ALTER TABLE "public"."token" DROP CONSTRAINT "token_email_uindex";

INSERT INTO "public"."user" VALUES ('1', 'superUser', 'superuser@gmail.com', '1579e2eb6e13b9db210285344ec640737acf0eabd14c0dabd8ee2fb8be2136a2', '123456', '1', '2019-09-02 15:13:10.796', null, null, null, null, null, null, '4', null, null);
SELECT setval('"public"."user_id_seq"', 10, true);
SELECT setval('"public"."user_role_id_seq"', 10, true);


