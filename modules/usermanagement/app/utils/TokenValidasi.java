package utils;

import controllers.common.BaseController;
import models.user.management.Token;
import utils.common.LogUtil;

import java.sql.Timestamp;

public class TokenValidasi extends BaseController {
    private static final String TAG = "TokenValidasi";

    public void validasi(String token, Token model, Token.Type type){
        LogUtil.debug(TAG, token);
        final boolean checktoken = userService.checkToken(token, type);
        if(!checktoken) { renderTemplate("user-management/user/tokennotvalid.html"); }
        notFoundIfNull(model);
        if(!model.expired_date.after(new Timestamp(System.currentTimeMillis()))){
            renderTemplate("user-management/user/tokenexpired.html");
        }
    }

}
