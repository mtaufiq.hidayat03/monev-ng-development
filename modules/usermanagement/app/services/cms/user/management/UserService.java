package services.cms.user.management;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import constants.generated.R;
import models.common.contracts.DataTableRequest;
import models.user.management.*;
import permission.Access;
import permission.Approve;
import permission.UserRole;
import play.mvc.Router;
import repositories.user.management.TokenRepository;
import repositories.user.management.UserRepository;
import repositories.user.management.UserRoleRepository;
import utils.common.LogUtil;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author HanusaCloud on 8/27/2019 11:05 AM
 */
public class UserService {

    private static final String TAG = "UserService";
    private UserRepository repository = new UserRepository();
    private TokenRepository tokenRepository = new TokenRepository();
    private UserRoleRepository userRoleRepository = new UserRoleRepository();
    private static final UserService userService = new UserService();

    public static UserService getInstance() {
        return userService;
    }

    public void store(UserForm userForm, CurrentUser currentUser) {
        User model;
        if (userForm.isCreatedMode()) {
            model = new User();
        } else {
            model = repository.getUser(userForm.id);
        }
        model.setDataForm(userForm);
        if (currentUser.user.isSuperAdmin()) {
            model.approve = (int) Approve.USER_CREATED_BY_SUPER_ADMIN.getId();
        }
        model.saveModel();
        model.attachRoles(userForm.getUserRole(model.getUserId()));
    }

    public void insert(UserForm userForm) {
        User model = new User();
        model.setDataForm(userForm);
        if (userForm.isCreatedMode()) {
            model.id = repository.save(model);
        } else {
            repository.update(model);
        }

//        model.attachRoles(userForm.getUserRole(model.getUserId()));
    }

    public boolean deleteUser(Long userId) {
        User model = repository.getUser(userId);
        if (model != null) {
            model.deleteModel();
            return true;
        }
        return false;
    }

    public boolean checkEmail(String email) {
        User model = repository.getEmail(email);
        final boolean result = model != null;
        LogUtil.debug(TAG, "does this email exist: " + email + " result: " + result);
        return result;
    }

    public boolean checkToken(String token, Token.Type type) {
        Token model = tokenRepository.getToken(token, type);
        return model != null;
    }

    public boolean deleteToken(String token) {
        Token model = tokenRepository.getToken(token);
        if (model != null) {
            model.deleteModel();
            return true;
        }
        return false;
    }

    public void storeUser(NewUserForm newUserForm) {
        User model = new User();
        model.setNewUserForm(newUserForm);
        model.saveModel();
        UserRole userRole = new UserRole(model.getUserId(), Long.valueOf(4));
        userRole.save();
    }

    public void storeAdmin(AdminklForm adminklForm) {
        User model = repository.getUser(adminklForm.id);
        LogUtil.debug(TAG, model.getUserId());
        model.setNewAdminForm(adminklForm);
        model.saveModel();
        new UserRepository().waitingApprove(model.getUserId());
        new UserRoleRepository().updateAdminKl(model.getUserId());
    }

    public void storeTokenUser(String email, String token, Timestamp expired_date, Token.Type type) {
        Token model = new Token();
        model.setTokenUser(email, token, expired_date, type);
        model.saveModel();

    }

    public void storeTokenAdmin(String email, String token, Timestamp expired_date, Token.Type type) {
        Token model = new Token();
        model.setTokenAdmin(email, token, expired_date, type);
        model.saveModel();

    }

    public boolean approveUser(Long id) {
        User model = repository.getUser(id);
        if (model != null) {
            repository.approveUpdate(id);
            return true;
        }
        return false;
    }

    public boolean rejectUser(Long id) {
        User model = repository.getUser(id);
        if (model != null) {
            repository.rejectUpdate(id);
            return true;
        }
        return false;
    }

    public void changePassword(ForgotPasswordForm forgotPasswordForm) {
        User model = repository.getEmail(forgotPasswordForm.email);
        model.changePassword(forgotPasswordForm.password);
        model.saveModel();
    }

    public void storeManageProfileUser(ManageProfileForm manageProfileForm) {
        User model = repository.getUser(manageProfileForm.id);
        model.setManageDataProfileUserForm(manageProfileForm);
        model.saveModel();
    }

    public void storeManageProfilePassword(ManageProfilePasswordForm manageProfilePasswordForm) {
        User model = repository.getUser(manageProfilePasswordForm.id);
        model.setManageDataProfilePassword(manageProfilePasswordForm);
        model.saveModel();
    }

}
