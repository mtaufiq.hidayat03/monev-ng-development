package services.cms.user.management;

import permission.PermissionItem;
import repositories.user.management.PermissionRepository;
import utils.common.LogUtil;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author HanusaCloud on 8/27/2019 11:01 AM
 */
@Deprecated
public class PermissionService {

    public static final String TAG = "PermissionService";

    private PermissionRepository repository = new PermissionRepository();

    public List<String> getPermissions(Long userId) {
        LogUtil.debug(TAG, "get permission by userId: " + userId);
        final List<String> result = repository.getPermissions(userId)
                .stream().map(e -> e.name).collect(Collectors.toList());
        LogUtil.debug(TAG, result);
        return result;
    }

    public void deattachedPermission(Integer userId) {
        repository.deleteAllPermissionByUser(userId);
    }

    public void attachPermissionItems(Long userId, List<PermissionItem> items) {
        if (!items.isEmpty()) {
            for (PermissionItem item : items) {
                item.user_id = userId;
                repository.attachPermission(item);
            }
        }
    }

    public void attachPermissions(Long userId, List<String> items) {
        if (!items.isEmpty()) {
            for (String item : items) {
                PermissionItem permissionItem = new PermissionItem(userId, item);
                repository.attachPermission(permissionItem);
            }
        }
    }

}
