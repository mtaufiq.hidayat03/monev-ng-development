package models.user.management;

import models.common.contracts.FormContract;
import org.apache.commons.lang3.ArrayUtils;
import permission.UserRole;
import play.data.validation.*;
import utils.common.LogUtil;
import java.util.ArrayList;
import java.util.List;

public class NewUserForm implements FormContract {
    public static final String TAG = "NewUserForm";
    public String action = "create";
    public Long id;
    @Required
    public String name;
    @Required
    @Email
    public String email;
    public String token;
    public String password;
    @MaxSize(15)
    @Phone
    public String phone;
    @MaxSize(20)
    @Match("^(\\d\\d\\.\\d\\d\\d\\.\\d\\d\\d\\.\\d\\-\\d\\d\\d\\.\\d\\d\\d)$")
    public String npwp;
    @MaxSize(20)
    public String nip;
    public Integer[] roles;
    public String kldi_id;

    @Override
    public String getAction() {
        return action;
    }

    public List<UserRole> getUserRole(Long userId) {
        List<UserRole> userRoles = new ArrayList<>();
        LogUtil.debug(TAG, ArrayUtils.toString(roles));
        if (!ArrayUtils.isEmpty(roles)) {
            for (Integer i : roles) {
                userRoles.add(new UserRole(userId, i.longValue()));
            }
        }
        return userRoles;
    }
}
