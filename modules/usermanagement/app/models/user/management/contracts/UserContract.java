package models.user.management.contracts;

import constants.generated.R;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import permission.Access;
import permission.UserRole;
import play.mvc.Router;
import repositories.user.management.PermissionRepository;
import utils.cms.FileUtil;
import utils.common.JsonUtil;
import utils.common.LogUtil;

import java.io.File;
import java.util.List;
import java.util.Map;

/**
 * @author HanusaCloud on 8/27/2019 10:53 AM
 */
public interface UserContract {

    String TAG = "UserContract";

    PermissionRepository permissionRepository = new PermissionRepository();

    Long getUserId();
    String getName();
    String getEmail();
    String getKldiId();
    Integer getRoleId();

    void removePassword();

    void setRole(UserRole role);

    UserRole getRole();

    default void withRoles() {
        LogUtil.debug(TAG, "with roles");
        if (getUserId() != null) {
            setRole(UserRole.getByUser(getUserId()));
            LogUtil.debug(TAG, JsonUtil.toJson(getRole()));
        }
    }

    default boolean roleExists() {
        return getRole() != null && getRole().getAccess() != null;
    }

    default boolean isSuperAdmin() {
        return roleExists() && getRole().getAccess() == Access.SUPER_ADMIN;
    }

    default boolean isAdminKonten() {
        return roleExists() && getRole().getAccess() == Access.ADMIN_KONTEN;
    }

    default boolean isAdminKl() {
        return roleExists() && getRole().getAccess() == Access.ADMIN_KL;
    }

    default boolean isRegiseredUser() {
        return roleExists() && getRole().getAccess() == Access.PUBLIC_USER;
    }

    default boolean containRole(Long id) {
        return roleExists() && getRole().role_id.equals(id);
    }

    default void attachRoles(List<UserRole> roles) {
        permissionRepository.detachRoles(getUserId());
        permissionRepository.attachRoles(roles);
    }
}