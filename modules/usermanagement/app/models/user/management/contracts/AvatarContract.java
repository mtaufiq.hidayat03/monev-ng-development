package models.user.management.contracts;

import utils.cms.FileUtil;
import utils.cms.UploadedFile;
import utils.common.JsonUtil;
import utils.common.LogUtil;

import java.io.File;

public interface AvatarContract {

    String TAG = "AvatarContract";

    String getFeaturedImage();

    File getFeaturedImageFile();

    void setFeaturedImage(String image);

    default UploadedFile uploadAvatarImage() {
        if (getFeaturedImageFile() == null) {
            LogUtil.debug(TAG, "Oops.. featured image file is empty");
            return null;
        }
        final UploadedFile featured = FileUtil.uploadImageFile(getFeaturedImageFile());
        if (featured == null || !featured.fileExist()) {
            LogUtil.debug(TAG, "Oops.. featured file object is null");
            return null;
        }
        setFeaturedImage(JsonUtil.toJson(featured));
        return featured;
    }
}
