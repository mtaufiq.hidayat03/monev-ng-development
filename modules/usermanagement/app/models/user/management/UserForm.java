package models.user.management;

import models.common.contracts.FormContract;
import org.apache.commons.lang3.ArrayUtils;
import permission.UserRole;
import play.data.validation.*;
import utils.common.LogUtil;

import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 8/27/2019 10:56 AM
 */
public class UserForm implements FormContract {

    public static final String TAG = "UserForm";

    public String action = "create";
    public Long id;
    @Required
    public String name;
    @Required
    @Email
    public String email;
    public String password;
    @Required
    public Integer[] roles;
    public String kldiId;

    @Required
    @MaxSize(20)
    @Match("^(\\d\\d\\.\\d\\d\\d\\.\\d\\d\\d\\.\\d\\-\\d\\d\\d\\.\\d\\d\\d)$")
    public String npwp;

    @Required
    @MaxSize(13)
    @Phone
    public String phone;

    @Override
    public String getAction() {
        return action;
    }

    public List<UserRole> getUserRole(Long userId) {
        List<UserRole> userRoles = new ArrayList<>();
        LogUtil.debug(TAG, ArrayUtils.toString(roles));
        if (!ArrayUtils.isEmpty(roles)) {
            for (Integer i : roles) {
                userRoles.add(new UserRole(userId, i.longValue()));
            }
        }
        return userRoles;
    }
}