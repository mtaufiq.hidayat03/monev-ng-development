package models.user.management;

import models.user.management.contracts.AvatarContract;
import org.apache.commons.lang3.StringUtils;

import java.io.File;

public class AvatarForm implements AvatarContract {
    public static final String TAG = "AvatarForm";
    public Long id;
    public File uploadfile;
    public String avatarImage;

    public AvatarForm() {
    }

    public AvatarForm(User model) {
        id = model.id;
        if (!StringUtils.isEmpty(model.avatar)) {
            this.avatarImage = model.avatar;
        }
    }

    @Override
    public String getFeaturedImage() {
        return avatarImage;
    }

    @Override
    public File getFeaturedImageFile() {
        return uploadfile;
    }

    @Override
    public void setFeaturedImage(String image) {
        this.avatarImage = image;
    }

}
