package models.user.management;

import play.data.validation.Required;

import javax.persistence.Transient;

public class ManageProfilePasswordForm {
    public static final String TAG = "ManageProfilePasswordForm";
    public Long id;
    @Required
    public String password;
    @Transient
    public String repeat;
}
