package models.user.management;

import models.common.BaseModel;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Table;

import javax.persistence.EnumType;
import java.sql.Timestamp;

@Table(name = "token", schema = "public")
public class Token extends BaseModel {
    @Enumerated(EnumType.STRING)
    public enum Type {
        REGISTRATION,
        FORGOT_PASSWORD
    }

    public String token;
    public String email;
    public Timestamp expired_date;
    public Type token_type = Type.REGISTRATION; // default to registration

    public Token() {}


    public void setTokenUser(String email, String token, Timestamp expired_date, Type type) {
        this.email = email;
        this.token = token;
        this.expired_date = expired_date;
        this.token_type = type;
    }

    //TODO bedanya dengan setTokenUser apa ?
    public void setTokenAdmin(String email, String token, Timestamp expired_date, Type type) {
        this.email = email;
        this.token = token;
        this.expired_date = expired_date;
        this.token_type = type;
    }
}
