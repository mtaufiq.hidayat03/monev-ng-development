package models.user.management;

import play.data.validation.*;

public class ManageProfileForm {
    public static final String TAG = "ManageProfileForm";
    public Long id;
    @Required
    public String name;
    @Required
    @Email
    public String email;
    @Required
    @MaxSize(20)
    @Match("^(\\d\\d\\.\\d\\d\\d\\.\\d\\d\\d\\.\\d\\-\\d\\d\\d\\.\\d\\d\\d)$")
    public String npwp;
    @Required
    @MaxSize(14)
    @Phone
    public String phone;

}
