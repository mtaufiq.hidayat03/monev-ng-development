package models.user.management;

import models.common.contracts.FormContract;
import play.data.validation.Required;

import java.io.File;

public class AdminklForm implements FormContract {

    public static final String TAG = "AdminklForm";
    public String action = "create";
    public Long id;
    @Required
    public String kldi_id;
    @Required
    public File uploadfile;
    public String upload_file;

    @Override
    public String getAction() {
        return action;
    }

}
