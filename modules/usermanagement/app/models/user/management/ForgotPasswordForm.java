package models.user.management;

import play.data.validation.Email;
import play.data.validation.Required;

public class ForgotPasswordForm {
    public static final String TAG = "ForgotPasswordForm";
    @Required
    @Email
    public String email;
    public String token;
    public String password;
}
