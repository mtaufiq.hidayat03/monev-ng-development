package models.user.management;

import constants.generated.R;
import models.common.BaseModel;
import models.common.contracts.KldiContract;
import models.common.contracts.annotations.SoftDelete;
import models.user.management.contracts.UserContract;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import permission.Approve;
import permission.UserRole;
import play.db.jdbc.Table;
import play.mvc.Router;
import utils.common.TokenUtil;

import javax.persistence.Transient;
import java.io.File;
import java.io.Serializable;
import java.util.Map;

import static models.cms.ContentFolder.PATH_PICTURE;

/**
 * @author HanusaCloud on 8/27/2019 10:52 AM
 */
@Table(name = "user", schema = "public")
@SoftDelete
public class User extends BaseModel implements UserContract, Serializable, KldiContract {

    public String name;
    public String password;
    public String email;
    @Transient
    public Integer role_id;
    public String phone;
    public String npwp;
    public String nip;
    public Integer approve;
    public String upload_file;
    public String kldi_id;
    public String avatar;


    public User() {
    }

    public User(Long userId) {
        this.id = userId;
    }

    public void setDataForm(UserForm model) {
        this.name = model.name;
        this.email = model.email;
        this.role_id = model.roles[0];
        this.phone = model.phone;
        this.npwp = model.npwp;
        if (!StringUtils.isEmpty(model.password)) {
            this.password = TokenUtil.sha256(model.password);
        }
        if (model.roles[0] == 2 || model.roles[0] == 3) {
            this.kldi_id = model.kldiId;
        }
    }

    public void setNewUserForm(NewUserForm model) {
        this.name = model.name;
        this.email = model.email;
        this.password = TokenUtil.sha256(model.password);
        this.phone = model.phone;
        int j = (int) Approve.ACCEPTED_PUBLIC_USER.getId();
        this.approve = j;

    }

    public void setNewAdminForm(NewUserForm model) {
        this.name = model.name;
        this.email = model.email;
        this.password = TokenUtil.sha256(model.password);
        this.phone = model.phone;
        this.npwp = model.npwp;
        this.nip = model.nip;
        this.kldi_id = model.kldi_id;
    }

    public void setNewAdminForm(AdminklForm model) {
        this.upload_file = model.upload_file;
        this.kldi_id = model.kldi_id;
    }

    public void setManageDataProfilePassword(ManageProfilePasswordForm model) {
        this.password = TokenUtil.sha256(model.password);
    }

    public void setManageDataProfileUserForm(ManageProfileForm model) {
        this.name = model.name;
        this.email = model.email;
        this.phone = model.phone;
        this.npwp = model.npwp;
    }


    public void setAvatarForm(AvatarForm model) {
        this.avatar = model.avatarImage;
    }

    public void changePassword(String newPassword) {
        this.password = TokenUtil.sha256(newPassword);
    }

    @Transient
    private UserRole role = null;

    @Override
    public Long getUserId() {
        return this.id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public Integer getRoleId() {
        return this.role_id;
    }

    @Override
    public void removePassword() {
        password = null;
    }

    @Override
    public void setRole(UserRole role) {
        this.role = role;
    }

    @Override
    public UserRole getRole() {
        return this.role;
    }

    @Override
    public String getKldiId() {
        return this.kldi_id;
    }

    @Override
    public String getKldi() {
        return kldi_id;
    }

    public String getNameAvatarFile() {
        if (avatar != null) {
            String getPath = avatar.substring(avatar.lastIndexOf("/") + 1)
                    .replaceAll("[^.a-zA-Z0-9]", "");
            return getPath;
        }
        return null;
    }

    public String getAvatarImage() {
        if (avatar != null) {
            Map<String, Object> map = new HashedMap<>();
            map.put("path", getNameAvatarFile());
            return Router.reverse(R.route.cms_filecontroller_getimage, map).url;
        }
        return null;
    }

    public Boolean avatarIsExist() {
        if (avatar != null) {
            File avatar = new File(PATH_PICTURE + getNameAvatarFile());
            if (avatar.exists()) {
                return true;
            }
        }
        return false;
    }
}
