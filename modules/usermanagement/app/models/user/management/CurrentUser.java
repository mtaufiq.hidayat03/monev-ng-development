package models.user.management;

import models.user.management.contracts.UserContract;
import org.apache.commons.collections4.map.HashedMap;
import permission.Access;
import play.Play;
import play.cache.Cache;
import play.mvc.Scope;
import utils.common.LogUtil;

import java.io.Serializable;
import java.util.Map;

/**
 * @author HanusaCloud on 8/27/2019 10:17 AM
 */
public class CurrentUser implements Serializable {

    public static final String TAG = "CurrentUser";
    private static final String SESSION_EXPIRATION = Play.configuration
            .getProperty("application.session.maxAge", "30mn");

    private static final String PREFIX_KEY = "user_session_";
    private static final String KEY_BANK = "key_bank";

    public String session;
    public UserContract user;

    public CurrentUser() {}

    public CurrentUser(String session, UserContract user) {
        this.session = session;
        this.user = user;
    }

    public Long getUserId() {
        if (this.user != null) {
            return this.user.getUserId();
        }
        return -999L;
    }

    public boolean allowed(Access[] accesses) {
        if (this.user == null) {
            return false;
        }
        if (this.user.isSuperAdmin()) {
            return true;
        }
        for (Access access : accesses) {
            if (access == this.user.getRole().getAccess()) {
                return true;
            }
        }
        return false;
    }

    private synchronized void addKey(String key) {
        LogUtil.debug(TAG, "add cache key into key banks: " + key);
        Map<String, String> cached;
        if ((cached = (Map<String, String>) Cache.get(KEY_BANK)) == null) {
            cached = new HashedMap<>();
        }
        cached.put(key, "15m");
        Cache.set(KEY_BANK, cached);
    }

    public synchronized void save() {
        final String key = PREFIX_KEY + session;
        LogUtil.debug(TAG, "save session by key: " + key);
        this.user.removePassword();
        Cache.safeSet(key, this, SESSION_EXPIRATION);
        addKey(key);
    }

    public static CurrentUser getFromCache(String session) {
        return Cache.get(session, CurrentUser.class);
    }

    public static CurrentUser getInstance() {
        final Scope.Session session = Scope.Session.current();
        return CurrentUser.getFromCache(PREFIX_KEY + session.getId());
    }

    public void clear() {
        Cache.safeDelete(PREFIX_KEY + session);
    }

}
