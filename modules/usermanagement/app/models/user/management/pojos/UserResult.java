package models.user.management.pojos;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import constants.generated.R;
import controllers.common.BaseController;
import models.common.contracts.DataTablePagination;
import models.user.management.CurrentUser;
import models.user.management.User;
import permission.Access;
import permission.Approve;
import permission.UserRole;
import play.mvc.Router;
import repositories.user.management.UserRepository;
import repositories.user.management.UserRoleRepository;
import utils.common.LogUtil;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UserResult extends BaseController implements DataTablePagination.PageAble<User> {

    private List<User> items;
    private final DataTablePagination.Request request;
    private int total = 0;
    private int totalItemInOnePage = 0;
    private final String[] columns = new String[]{
            "id", "name", "email","role_id", "approve", "created_at"
    };
    private final List<String> searchAbleFields;
    private static final UserRepository repo = new UserRepository();
    private UserRoleRepository userRoleRepository = new UserRoleRepository();
    private CurrentUser currentUser = getCurrentUser();

    public UserResult(DataTablePagination.Request request) {
        this.request = request;
        List<String> fields = new ArrayList<>();
        fields.add("name::TEXT");
        fields.add("email::TEXT");
        searchAbleFields = fields;
    }

    @Override
    public JsonObject getResultsAsJson() {
        JsonObject result = getResponseTemplate();
        JsonArray jsonArray = new JsonArray();
        if (getTotal() == 0) {
            result.add("data", jsonArray);
            return result;
        }
        int i = getNumberingStartFrom();
        for (User user : getResults()) {
            JsonArray userArray = new JsonArray();
            String rolesUser = "";
            UserRole userRole = userRoleRepository.getUserRole(user.id);
            if (userRole != null) {
                Access accessRole = Access.fromId(userRole.role_id.intValue());
                rolesUser = accessRole.name;
            }
            Map<String, Object> params = new HashMap<>();
            params.put("id", user.id);
            String url = Router.reverse(R.route.user_management_usercontroller_update, params).url;
            String deleteUrl = Router.reverse(R.route.user_management_usercontroller_delete, params).url;
            String approvalUrl = Router.reverse(R.route.user_management_approvalcontroller_updateapproval, params).url;
            userArray.add(i);
            userArray.add(user.name);
            userArray.add(user.email);
            userArray.add(rolesUser);
            if (user.approve != null) {
                if (user.approve == Approve.ACCEPTED_PUBLIC_USER.getId()) {
                    userArray.add("<span class='bg-green'>" + Approve.ACCEPTED_PUBLIC_USER.getName() + "</span>");
                }
                if (user.approve == Approve.APPROVAL_ADMIN_KL.getId()) {
                    userArray.add("<span class='bg-blue'>" + Approve.APPROVAL_ADMIN_KL.getName() + "</span>");
                }
                if (user.approve == Approve.REJECTED_ADMIN_KL.getId()) {
                    userArray.add("<span class='bg-blue'>" + Approve.REJECTED_ADMIN_KL.getName() + "</span>");
                }
                if (user.approve == Approve.ACCEPTED_ADMIN_KL.getId()) {
                    userArray.add("<span class='bg-green'>" + Approve.ACCEPTED_ADMIN_KL.getName() + "</span>");
                }
                if (user.approve == Approve.USER_CREATED_BY_SUPER_ADMIN.getId()) {
                    userArray.add("<span class='bg-green'>" + Approve.USER_CREATED_BY_SUPER_ADMIN.getName() + "</span>");
                }
            } else {
                userArray.add("<span class='bg-blue'>-</span>");
            }
            String persetujuan = "";
            if (currentUser.user.isSuperAdmin()) {
                if (rolesUser.equals(Access.fromId(2).name)) {
                    persetujuan = "<li class=\"divider\"></li>" +
                            "<li><a href='" + approvalUrl + "'>Persetujuan</a></li>";
                }
                if (rolesUser.equals(Access.fromId(4).name) && user.approve != null && user.approve != Approve.ACCEPTED_PUBLIC_USER.getId() ) {
                    persetujuan = "<li class=\"divider\"></li>" +
                            "<li><a href='" + approvalUrl + "'>Persetujuan</a></li>";
                }
            }
            String delete = "";
            if (currentUser.user.isSuperAdmin()) {
                delete = "<li class=\"divider\"></li>" +
                        "<li><a href='" + deleteUrl + "' class='delete'>Delete</a></li>";
            }
            if (currentUser.user.isAdminKl()) {
                if (rolesUser.equals(Access.fromId(3).name)) {
                    delete = "<li class=\"divider\"></li>" +
                            "<li><a href='" + deleteUrl + "' class='delete'>Delete</a></li>";
                }
            }
            userArray.add(new SimpleDateFormat("dd-MM-yyyy").format(user.created_at));
            userArray.add("<div class='btn-group'>" +
                    "<button type='button' class='btn btn-success dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>" +
                    "Action <span class='sr-only'>Toggle Dropdown</span></button>" +
                    "<ul class='dropdown-menu' role='menu'>" +
                    "<li><a href='" + url + "'>Edit</a></li>" +
                    persetujuan +
                    delete +
                    "</ul></div>");
            jsonArray.add(userArray);
            i++;
        }
        result.add("data", jsonArray);
        LogUtil.debug(TAG, result);
        return result;
    }

    @Override
    public List<String> extractSearchAbleFields(Field[] fields) {
        return searchAbleFields;
    }

    @Override
    public void setResult(List<User> items) {
        this.items = items;
    }

    @Override
    public List<User> getResults() {
        return items;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setTotalItemInOnePage(int totalItem) {
        totalItemInOnePage = totalItem;
    }

    @Override
    public int getTotalItemInOnePage() {
        return totalItemInOnePage;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public String[] getColumns() {
        return columns;
    }

    @Override
    public DataTablePagination.Request getRequest() {
        return request;
    }

    @Override
    public Class<User> getReturnType() {
        return User.class;
    }
}
