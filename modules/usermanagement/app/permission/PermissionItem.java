package permission;

import models.common.BaseModel;
import play.db.jdbc.Table;

/**
 * @author HanusaCloud on 2/1/2018
 */
@Deprecated
@Table(name = "permission")
public class PermissionItem extends BaseModel {

    public Long user_id;
    public Long permission_id;
    public String name;

    public PermissionItem(Long userId, String name) {
        this.user_id = userId;
        this.name = name;
    }

}
