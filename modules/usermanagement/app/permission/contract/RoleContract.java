package permission.contract;

import permission.RolePermission;
import repositories.user.management.PermissionRepository;

import java.util.List;

/**
 * @author HanusaCloud on 2/8/2018
 */
public interface RoleContract {

    Long getId();

    void setPermissions(List<RolePermission> permissions);

    List<RolePermission> getPermissions();

    default void withPermissions() {
        if (getId() != null) {
            setPermissions(new PermissionRepository().getRolePermissions(getId()));
        }
    }

    default boolean containPermission(Long id) {
        if (getPermissions() != null && !getPermissions().isEmpty()) {
            for (RolePermission permission : getPermissions()) {
                if (permission.permission_id.equals(id)) {
                    return true;
                }
            }
        }
        return false;
    }

}
