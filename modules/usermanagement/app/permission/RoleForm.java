package permission;

import play.data.validation.Required;

/**
 * @author HanusaCloud on 2/8/2018
 */
@Deprecated
public class RoleForm {

    public Long id;
    public String action = "create";
    @Required
    public String name;
    public String description;
    @Required
    public Long[] actions;

    public boolean isCreateMode() {
        return action.equalsIgnoreCase("create");
    }

}
