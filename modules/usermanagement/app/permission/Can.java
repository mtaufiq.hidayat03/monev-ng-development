package permission;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author HanusaCloud on 2/1/2018
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Can {
    public Access[] value();
}
