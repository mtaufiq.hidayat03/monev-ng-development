package permission;

import play.db.jdbc.Enumerated;
import javax.persistence.EnumType;

@Enumerated(EnumType.ORDINAL)
public enum Approve {

    ACCEPTED_PUBLIC_USER(
            1,
            "Diterima sebagai public user",
            "Diterima sebagai public user"
    ),
    APPROVAL_ADMIN_KL(
            2,
            "Meminta persetujuan menjadi admin K/L/P/D",
            "Meminta persetujuan menjadi admin k/l"
    ),
    REJECTED_ADMIN_KL(
            3,
            "Ditolak sebagai admin K/L/P/D. Status sebagai public user",
            "Ditolak sebagai admin K/L/P/D dan tetap menjadi public user"
    ),
    ACCEPTED_ADMIN_KL(
            4,
            "Diterima sebagai admin K/L/P/D",
            "Diterima sebagai admin K/L/P/D"
    ),
    USER_CREATED_BY_SUPER_ADMIN(
            5,
            "User dibuat oleh Super Admin",
            "User dibuat oleh Super Admin"
    ),
    ;

    public final Integer id;
    public final String name;
    public final String desc;

    Approve(Integer id, String name, String desc) {
        this.id = id;
        this.name = name;
        this.desc = desc;
    }

    public long getId() {
        return id.longValue();
    }

    public String getName() {
        return name;
    }

    public static Approve fromId(int id) {
        switch (id) {
            case 1:
                return Approve.ACCEPTED_PUBLIC_USER;
            case 2:
                return Approve.APPROVAL_ADMIN_KL;
            case 3:
                return Approve.REJECTED_ADMIN_KL;
            case 4:
                return Approve.ACCEPTED_ADMIN_KL;
            default:
                return null;
        }
    }
}
