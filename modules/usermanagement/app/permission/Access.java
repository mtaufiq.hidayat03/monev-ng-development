package permission;

import play.db.jdbc.Enumerated;
import utils.common.LogUtil;

import javax.persistence.EnumType;
import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 2/1/2018
 */
@Enumerated(EnumType.ORDINAL)
public enum Access {

    SUPER_ADMIN(1,"Super Admin","All permission"),
    ADMIN_KL(2,"Admin K/L/P/D","Admin K/L/P/D"),
    ADMIN_KONTEN(3,"Admin Konten","Akses Konten"),
    PUBLIC_USER(4,"Public User","Register User");

    public final Integer id;
    public final String name;
    public final String desc;

    Access(Integer id, String name, String desc) {
        this.id = id;
        this.name = name;
        this.desc = desc;
    }

    public long getId() {
        return id.longValue();
    }

    public static Access fromId(int id) {
        switch (id) {
            case 1:
                return Access.SUPER_ADMIN;
            case 2:
                return Access.ADMIN_KL;
            case 3:
                return Access.ADMIN_KONTEN;
            case 4:
                return Access.PUBLIC_USER;
            default:
                return null;
        }
    }

    public static Role toRole(Access access) {
        return new Role(access.getId(), access.name, access.desc);
    }

    public static List<Role> toRoles() {
        List<Role> roles = new ArrayList<>();
        roles.add(toRole(SUPER_ADMIN));
        roles.add(toRole(ADMIN_KL));
        roles.add(toRole(ADMIN_KONTEN));
        roles.add(toRole(PUBLIC_USER));
        return roles;
    }

}
