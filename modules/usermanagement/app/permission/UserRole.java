package permission;

import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.io.Serializable;

/**
 * @author HanusaCloud on 2/2/2018
 */
@Table(name = "user_role", schema = "public")
public class UserRole extends BaseTable implements Serializable {

    @Id
    public Long id;
    public Long user_id;
    public Long role_id;
    @Transient
    public String name;
    @Transient
    public String desc;

    public UserRole() {}

    public UserRole(Long userId, Long roleId) {
        this.user_id = userId;
        this.role_id = roleId;
    }

    public Access getAccess() {
        return Access.fromId(role_id.intValue());
    }

    @Override
    protected void prePersist() {
        if (this.id == null) {
            id = Query.find("SELECT nextval('user_role_id_seq'::regclass)", Long.class).first();
        }
    }

    public static UserRole getByUser(Long userId) {
        return find("user_id = ? ", userId).first();
    }
}
