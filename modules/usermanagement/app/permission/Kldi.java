package permission;

import com.google.gson.annotations.SerializedName;
import models.common.contracts.KldiContract;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;

import java.io.Serializable;

@Table(name = "kldi", schema = "public")
public class Kldi extends BaseTable implements Serializable, KldiContract {

    @Id
    @SerializedName("id")
    public String id;
    @SerializedName("nama")
    public String nama_instansi;
    @SerializedName("website")
    public String alamat;
    @SerializedName("jenis")
    public String jenis;

    public Kldi() {}

    public Kldi(String id, String nama_instansi, String alamat, String jenis) {
        this.id = id;
        this.nama_instansi = nama_instansi;
        this.alamat = alamat;
        this.jenis =jenis;
    }

    @Override
    public String getKldi() {
        return id;
    }
}
