package permission;

import models.common.BaseModel;
import permission.contract.RoleContract;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 2/1/2018
 */
@Table(name = "role", schema = "public")
public class Role extends BaseModel implements RoleContract {

    public String name;
    public String description;

    public Role() {}

    public Role(Long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    @Transient
    public List<RolePermission> permissions = new ArrayList<>();

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void setPermissions(List<RolePermission> permissions) {
        this.permissions = permissions;
    }

    @Override
    public List<RolePermission> getPermissions() {
        return this.permissions;
    }
}
