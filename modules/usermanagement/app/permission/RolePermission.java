package permission;

import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;

import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author HanusaCloud on 2/1/2018
 */
@Deprecated
@Table(name = "role_permission", schema = "public")
public class RolePermission extends BaseTable {

    @Id
    public Long id;
    public Long role_id;
    public Long permission_id;
    @Transient
    public String name;

    @Transient
    public Role role;

    public RolePermission() {}

    public RolePermission(Long roleId, Long permissionId) {
        this.role_id = roleId;
        this.permission_id = permissionId;
    }

    @Override
    protected void prePersist() {
        if (this.id == null) {
            id = Query.find("SELECT nextval('role_permission_id_seq'::regclass)", Long.class).first();
        }
    }

}
