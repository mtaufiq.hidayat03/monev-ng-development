package controllers.user.management;

import constants.generated.R;
import controllers.core.LandingController;
import models.QuestionAnswers.Question;
import models.user.management.ManageProfileForm;
import models.user.management.ManageProfilePasswordForm;
import models.user.management.User;
import models.user.management.UserForm;
import models.user.management.contracts.UserContract;
import notifiers.Mails;
import org.apache.commons.lang3.StringUtils;
import permission.Access;
import permission.Can;
import permission.Kldi;
import permission.Role;
import play.i18n.Messages;
import repositories.core.QuestionAnswers.QuestionRepository;
import repositories.user.management.KldiRepository;
import repositories.user.management.UserRepository;
import utils.common.LogUtil;

import java.util.List;

import static permission.Access.*;

/**
 * @author HanusaCloud on 8/27/2019 11:03 AM
 */
public class UserController extends BaseAdminController {

    public static final String TAG = "UserController";

    private static final QuestionRepository questionRepository = new QuestionRepository();

    private static final KldiRepository kldiRepository = KldiRepository.getInstance();

    @Can({ADMIN_KL})
    public static void index() {
        String afterLogin = params.get("login");
        if (afterLogin != null && afterLogin.equals("true")) {
            Integer notAnsweredQuestions = questionRepository.countNotAnsweredQuestions(true, getCurrentUser().user);
            if (notAnsweredQuestions > 0) {
                List<Question> questions = questionRepository.getNotAnsweredQuestions(true, getCurrentUser().user, 6, 0);
                renderArgs.put("questions", questions);
            }
            if (notAnsweredQuestions > 6) {
                renderArgs.put("morequestions", "dan " + (notAnsweredQuestions - 6) + " pertanyaan lagi yang belum " +
                        "dijawab");
            }
        }
        LogUtil.debug(TAG, "Accessing index");
        renderTemplate(R.view.user_management_user_index);
    }

    @Can({ADMIN_KL})
    public static void create() {
        User model = new User();
        List<Role> roles = Access.toRoles();
        renderArgs.put("model", model);
        renderArgs.put("roles", roles);
        renderArgs.put("action", "create");
        UserContract user = getCurrentUser().user;
        List<Kldi> daftarkl = kldiRepository.getListKlByRole(user);
        renderArgs.put("daftarkl", daftarkl);
        renderTemplate(R.view.user_management_user_input);
    }

    @Can({ADMIN_KL})
    public static void update(Long id) {
        UserContract user = getCurrentUser().user;
        boolean isUser = !user.isSuperAdmin();
        User model = new UserRepository().getUser(id);
        notFoundIfNull(model);
        model.withRoles();
        List<Role> roles = Access.toRoles();
        boolean isAdminKl = model.isAdminKl();
        boolean isAdminKonten = model.isAdminKonten();
        LogUtil.debug(TAG, model);
        List<Kldi> daftarkl = kldiRepository.getListKlByRole(user);
        renderArgs.put("daftarkl", daftarkl);
        renderArgs.put("model", model);
        renderArgs.put("roles", roles);
        renderArgs.put("action", "update");
        renderArgs.put("isUser", isUser);
        renderArgs.put("isAdminKl", isAdminKl);
        renderArgs.put("isAdminKonten", isAdminKonten);
        renderTemplate(R.view.user_management_user_input);
    }

    @Can({ADMIN_KL})
    public static void delete(Long id) {
        final boolean result = userService.deleteUser(id);
        if (result) {
            flashSuccess(R.translation.user_management_user_delete_success);
        } else {
            flashError(R.translation.user_management_user_delete_failed);
        }
        index();
    }

    private static void reRenderForm(UserForm model) {
        List<Role> roles = Access.toRoles();
        if (model.isCreatedMode()) {
            renderArgs.put("model", new User());
            renderArgs.put("roles", roles);
            renderArgs.put("action", "create");
            renderTemplate(R.view.user_management_user_input);
        }
        User user = new UserRepository().getUser(model.id);
        boolean isAdminKl = user.isAdminKl();
        boolean isAdminKonten = user.isAdminKonten();
        notFoundIfNull(model);
        user.withRoles();
        List<Kldi> daftarkl = kldiRepository.getListKlByRole(user);
        renderArgs.put("daftarkl", daftarkl);
        renderArgs.put("model", user);
        renderArgs.put("roles", roles);
        renderArgs.put("action", "update");
        renderArgs.put("isAdminKl", isAdminKl);
        renderArgs.put("isAdminKonten", isAdminKonten);
        renderTemplate(R.view.user_management_user_input);
    }

    @Can({ADMIN_KL})
    public static void store(UserForm model) throws Exception {
        checkAuthenticity();
        validation.valid(model);
        LogUtil.debug(TAG, model);
        if (validation.hasErrors()) {
            params.flash();
            flashError(R.translation.form_re_check);
            reRenderForm(model);
        }
        if (model.isCreatedMode() && StringUtils.isEmpty(model.password)) {
            flashError(R.translation.form_missing_password);
            create();
        }
        try {
            userService.store(model, getCurrentUser());
            String message = Messages.get(R.translation.user_management_user_is_created);
            if (!model.isCreatedMode()) {
                message = Messages.get(R.translation.user_management_user_is_updated);
            }
            flash.success(message);
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            flashError(R.translation.user_management_email_exists);
            if (model.isCreatedMode()) {
                create();
            } else {
                update(model.id);
            }

        }
        index();
    }

    @Can({ADMIN_KL, ADMIN_KONTEN})
    public static void detail(Long id) {
        UserContract user = getCurrentUser().user;
        if (!user.getUserId().equals(id)) {
            flashError(R.translation.has_no_view_permission_over_a_user);
            detail(user.getUserId());
        }

        User model = new UserRepository().getUser(id);
        notFoundIfNull(model);
        model.withRoles();
        renderArgs.put("users", model);
        renderTemplate(R.view.user_management_user_detail);
    }

    public static void tokenNotValid() {
        renderTemplate("user-management/user/tokennotvalid.html");
    }

    public static void tokenexpired() {
        renderTemplate("user-management/user/tokenexpired.html");
    }

    public static void newTemplateAdmin() {
        LogUtil.debug(TAG, "Accessing new template admin");
        renderTemplate(R.view.user_management_user_example);
    }

    public static void backLanding() {
        LandingController.index();
    }

    @Can({Access.ADMIN_KL})
    public static void undang() {
        renderTemplate(R.view.user_management_user_undang);
    }

    @Can({Access.ADMIN_KL})
    public static void storeUndangAdminKonten(String email) {
        final boolean result = userService.checkEmail(email);
        if (result) {
            LogUtil.debug(TAG, "Email sudah digunakan");
            flashError(R.translation.user_management_invite_email_not_available);
            undang();
        }
        try {
            Mails.sendMail(
                    email,
                    "http://monev-ng.cloud.javan.co.id/admin/register/admin-konten/",
                    "Lengkapi Registrasi Admin Konten anda"
            );
            flashSuccess("Undangan admin konten telah berhasil dikirimkan kepada: "+email);
            UserController.index();
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            flashError(R.translation.user_management_send_email_failed);
            undang();
        }
    }

    @Can({ADMIN_KL, SUPER_ADMIN, ADMIN_KONTEN, PUBLIC_USER})
    public static void manageProfile(String tab) {
        if (tab != null) {
            renderArgs.put("tab", tab);
        }
        UserContract user = getCurrentUser().user;
        User model = new UserRepository().getUser(user.getUserId());
        notFoundIfNull(model);
        Boolean isAdminKl = user.isAdminKl();
        Boolean isAdminKonten = user.isAdminKonten();
        if (isAdminKl || isAdminKonten) {
            Kldi kldi = kldiRepository.getById(user.getKldiId());
            renderArgs.put("kldi", kldi);
        }
        renderArgs.put("model", model);
        renderArgs.put("action", "update");
        renderArgs.put("isAdminKl", isAdminKl);
        renderArgs.put("isAdminKonten", isAdminKonten);
        renderTemplate(R.view.user_management_manage_profile_manage_profile);
    }

    @Can({ADMIN_KL, SUPER_ADMIN, ADMIN_KONTEN, PUBLIC_USER})
    public static void storeManageProfile(ManageProfileForm model) throws Exception {
        String tab = "1";
        checkAuthenticity();
        validation.valid(model);
        LogUtil.debug(TAG, model);
        if (validation.hasErrors()) {
            params.flash();
            flashError(R.translation.form_re_check);
        }
        try {
            userService.storeManageProfileUser(model);
            String message = Messages.get(R.translation.user_management_profile_user_is_updated);
            flash.success(message);
            manageProfile(tab);
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            flashError(R.translation.user_management_email_exists);
            manageProfile(tab);
        }
    }

    @Can({Access.SUPER_ADMIN, Access.ADMIN_KL, ADMIN_KONTEN, Access.PUBLIC_USER})
    public static void storeManageProfilePassword(ManageProfilePasswordForm model) throws Exception {
        String tab = "2";
        checkAuthenticity();
        validation.valid(model);
        LogUtil.debug(TAG, model);
        if (validation.hasErrors()) {
            params.flash();
            flashError(R.translation.form_re_check);
        }
        if (StringUtils.isEmpty(model.password)) {
            flashError(R.translation.form_missing_password);
            manageProfile(tab);
        }
        if (!model.password.equals(model.repeat)) {
            flashError(R.translation.form_password_repeat_password_not_match);
            manageProfile(tab);
        }
        try {
            userService.storeManageProfilePassword(model);
            String message = Messages.get(R.translation.user_management_password_profile_user_is_updated);
            flash.success(message);
            manageProfile(tab);
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            manageProfile(tab);
        }
    }
}
