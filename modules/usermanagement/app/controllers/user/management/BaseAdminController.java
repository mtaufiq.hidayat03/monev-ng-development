package controllers.user.management;

import controllers.common.HtmlBaseController;
import tags.common.MenuTag;
import models.user.management.CurrentUser;
import play.mvc.Before;

public class BaseAdminController extends HtmlBaseController {

    @Before
    protected static void beforeAll(){
        CurrentUser currentUser = getCurrentUser();
        if (currentUser == null) {
            redirect("/");
        }

        renderArgs.put("sideMenus", MenuTag.getList());
    }

}
