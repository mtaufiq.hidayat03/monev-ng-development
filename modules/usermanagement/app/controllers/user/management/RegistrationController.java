package controllers.user.management;

import constants.generated.R;
import controllers.common.HtmlBaseController;
import models.common.ServiceResult;
import models.user.management.*;
import notifiers.Mails;
import permission.Access;
import permission.Kldi;
import permission.UserRole;
import repositories.user.management.KldiRepository;
import repositories.user.management.TokenRepository;
import repositories.user.management.UserRepository;
import utils.TokenValidasi;
import utils.common.LogUtil;

import java.sql.Timestamp;
import java.util.List;

/**
 * @author HanusaCloud on 9/25/2019 12:38 PM
 */
public class RegistrationController extends HtmlBaseController {

    public static void registerUser(String email) {
        final boolean result = userService.checkEmail(email);
        if (result) {
            LogUtil.debug(TAG, "Email sudah digunakan.");
            flash.error("Email sudah digunakan. Silahkan gunakan alamat email lain");
            renderJSON(new ServiceResult("Email sudah digunakan. Silahkan gunakan alamat email lain").toJson());
        }
        final boolean check = Mails.sendEmail(email);
        if (check) {
            renderJSON(new ServiceResult(
                    true,
                    "Terima Kasih!!! Pendaftaran user anda sukses diproses. Silahkan cek " +
                            "email anda untuk melanjutkan proses pendaftaran.").toJson()
            );
        }
        renderJSON(new ServiceResult(
                " Pendaftaran user anda gagal diproses. Silahkan ulangi beberapa saat lagi").toJson()
        );
    }

    public static void failedSendEmail() {
        renderTemplate("user-management/auth/failedSendEmail.html");
    }

    public static void registerAdminKonten() {
        CurrentUser currentUser = CurrentUser.getInstance();
        if (currentUser != null) {
            LogUtil.debug(TAG, "current user not empty, redirect user to UserController index");
            UserController.index();
        }
        LogUtil.debug(TAG, "register new admin k/l");
        renderTemplate("user-management/auth/register-admin-konten.html");
    }

    public static void checkYourEmail() {
        if (getCurrentUser() != null) {
            LogUtil.debug(TAG, "current user not empty, redirect user to UserController index");
            UserController.index();
        }
        LogUtil.debug(TAG, "Cek email anda. Proses awal pendaftaran berhasil");
        renderTemplate(R.view.user_management_auth_checkyouremail);
    }

    public static void registerAdminKonten(String email) {
        final boolean result = userService.checkEmail(email);
        if (result) {
            LogUtil.debug(TAG, "Email sudah digunakan");
            flash.error("Email sudah digunakan. Silahkan gunakan alamat email lain");
            registerAdminKonten();
        }
        try {
            if (Mails.sendEmailAdminKonten(email).equals("sukses")) {
                checkYourEmail();
            }
            failedSendEmail();
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            registerAdminKonten();
        }
    }

    public static void tokenNotValid() {
        renderTemplate("user-management/user/tokennotvalid.html");
    }

    public static void tokenexpired() {
        renderTemplate("user-management/user/tokenexpired.html");
    }

    private static void reloadUserForm(NewUserForm model) {
        renderArgs.put("email", model.email);
        renderArgs.put("token", model.token);
        renderArgs.put("action", model.action);
        renderArgs.put("model", model);
        renderTemplate("user-management/user/createuser.html");
    }

    public static void createUser(String token) {
        CurrentUser currentUser = CurrentUser.getInstance();
        if (currentUser != null) {
            LogUtil.debug(TAG, "current user not empty, redirect user to UserController index");
            UserController.index();
        }
        final boolean checktoken = userService.checkToken(token, Token.Type.REGISTRATION);
        if (!checktoken) {
            LogUtil.debug(TAG, "Token tidak ditemukan");
            flashError(R.translation.token_not_found);
            tokenNotValid();
        }
        LogUtil.debug(TAG, "Memulai untuk membuat user public baru dengan token:" + token);
        Token model = new TokenRepository().getToken(token);
        notFoundIfNull(model);
        //LogUtil.debug(TAG, new Timestamp(System.currentTimeMillis()));
        if (model.expired_date.after(new Timestamp(System.currentTimeMillis()))) {
            LogUtil.debug(TAG, "Token belum expired");
            renderArgs.put("token", model.token);
            renderArgs.put("email", model.email);
            renderArgs.put("action", "create");
            renderTemplate("user-management/user/createuser.html");
        }
        LogUtil.debug(TAG, "Token sudah expired");
        flash.error("Token sudah expired");
        tokenexpired();
    }

    public static void createUserSuccess(String email) {
        renderArgs.put("email", email);
        renderTemplate(R.view.user_management_user_successcreateuser);
    }

    public static void createUserForm(NewUserForm model, String password2) throws Exception {
        validation.valid(model);
        if (validation.hasErrors()) {
            params.flash();
            flashError(R.translation.form_re_check);
            reloadUserForm(model);
        }
        if (!model.password.equals(password2) && model.isCreatedMode()) {
            LogUtil.debug(TAG, "Password dan ulangi password yang anda masukkan tidak sama.");
            flash.error("Password dan ulangi password yang anda masukkan tidak sama.");
            reloadUserForm(model);
        }
        try {
            userService.storeUser(model);
            LogUtil.debug(TAG, "User public berhasil ditambahkan");
            userService.deleteToken(model.token);
            LogUtil.debug(TAG, "Mengganti null dengan timestamp pada column deleted_at :" + model.token);
            createUserSuccess(model.email);
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }
    }

    public static void forgotPassword(String token) {
        CurrentUser currentUser = CurrentUser.getInstance();
        if (currentUser != null) {
            LogUtil.debug(TAG, "current user not empty, redirect user to UserController index");
            UserController.index();
        }
        final boolean checktoken = userService.checkToken(token, Token.Type.FORGOT_PASSWORD);
        if (!checktoken) {
            LogUtil.debug(TAG, "Token tidak ditemukan");
            flashError(R.translation.token_not_found);
            tokenNotValid();
        }
        LogUtil.debug(TAG, "Memulai untuk reset password dengan token:" + token);
        Token model = new TokenRepository().getToken(token);
        notFoundIfNull(model);
        final boolean checkEmail = userService.checkEmail(model.email);
        if (!checkEmail) {
            LogUtil.debug(TAG, "User tidak ditemukan");
            flashError(R.translation.user_not_found);
            tokenNotValid();
        }
        //LogUtil.debug(TAG, new Timestamp(System.currentTimeMillis()));
        if (model.expired_date.after(new Timestamp(System.currentTimeMillis()))) {
            LogUtil.debug(TAG, "Token belum expired");
            renderArgs.put("token", model.token);
            renderArgs.put("email", model.email);
            renderTemplate(R.view.user_management_user_forgotpassword);
        }
        LogUtil.debug(TAG, "Token sudah expired");
        flash.error("Token sudah expired");
        tokenexpired();
    }

    public static void createAdmin(String token) throws Exception {
        CurrentUser currentUser = CurrentUser.getInstance();
        if (currentUser != null) {
            LogUtil.debug(TAG, "current user not empty, redirect user to UserController index");
            UserController.index();
        }
        final boolean tokenExists = userService.checkToken(token, Token.Type.REGISTRATION);
        if (!tokenExists) {
            LogUtil.debug(TAG, "Token tidak ditemukan");
            flash.error(R.translation.token_not_found);
            tokenNotValid();
        }
        LogUtil.debug(TAG, "Memulai untuk membuat user admin k/l baru dengan token:" + token);
        Token model = new TokenRepository().getToken(token);
        notFoundIfNull(model);
        if (model.expired_date.after(new Timestamp(System.currentTimeMillis()))) {
            LogUtil.debug(TAG, "Token belum expired");
            renderArgs.put("token", model.token);
            renderArgs.put("email", model.email);
            renderArgs.put("action", "create");
            renderTemplate(R.view.user_management_user_createadmin);
        }
        LogUtil.debug(TAG, "Token sudah expired");
        flash.error(R.translation.token_expired);
        tokenexpired();
    }

    public static void forgotPasswordForm(ForgotPasswordForm model, String password2) throws Exception {
        validation.valid(model);
        if (validation.hasErrors()) {
            params.flash();
            flash.error(R.translation.form_re_check);
            reloadForgotPasswordForm(model);
        }
        if (!model.password.equals(password2)) {
            LogUtil.debug(TAG, "Password dan ulangi password yang anda masukkan tidak sama.");
            flash.error("Password dan ulangi password yang anda masukkan tidak sama.");
            reloadForgotPasswordForm(model);
        }
        try {
            userService.changePassword(model);
            LogUtil.debug(TAG, "Password berhasil diganti");
            userService.deleteToken(model.token);
            LogUtil.debug(TAG, "Mengganti null dengan timestamp pada column deleted_at :" + model.token);
            resetPasswordSuccess(model.email);
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }
    }

    private static void reloadForgotPasswordForm(ForgotPasswordForm model) {
        renderArgs.put("email", model.email);
        renderArgs.put("token", model.token);
        renderArgs.put("model", model);
        renderTemplate(R.view.user_management_user_forgotpassword);
    }

    private static void reloadAdminForm(NewUserForm model) {
        renderArgs.put("email", model.email);
        renderArgs.put("token", model.token);
        renderArgs.put("action", model.action);
        renderArgs.put("model", model);
        renderTemplate("user-management/user/createadmin.html");
    }

    public static void createAdminSuccess(String email) {
        renderArgs.put("email", email);
        renderTemplate("user-management/user/successcreateadmin.html");
    }

    public static void resetPasswordSuccess(String email) {
        renderArgs.put("email", email);
        renderTemplate(R.view.user_management_user_successresetpassword);
    }

    public static void formRegisterAdminKonten(String token) {
        CurrentUser currentUser = CurrentUser.getInstance();
        if (currentUser != null) {
            LogUtil.debug(TAG, "current user not empty, redirect user to UserController index");
            UserController.index();
        }
        Token model = new TokenRepository().getToken(token);
        new TokenValidasi().validasi(token, model, Token.Type.REGISTRATION);
        Token cekToken = new TokenRepository().getCreatedBytoken(token);
        User user = new UserRepository().getUser(cekToken.created_by);
        KldiRepository repo = new KldiRepository();
        List<Kldi> daftarkl = repo.getListKlByToken(user.kldi_id);
        renderArgs.put("daftarkl", daftarkl);
        renderArgs.put("token", model.token);
        renderArgs.put("email", model.email);
        renderArgs.put("action", "create");
        LogUtil.debug(TAG, "register new admin konten");
        renderTemplate(R.view.user_management_admin_register_admin_konten);
    }

    public static void storeRegisterAdminKonten(NewUserForm model, String name, String passwdagain) {
        LogUtil.info(TAG, model.password + " - " + passwdagain);
        if (!model.password.equals(passwdagain)) {
            LogUtil.debug(TAG, "Password tidak sama");
            flashError(R.translation.user_management_password_miss_match);
            reloadForm(model, R.view.user_management_admin_register_admin_konten);
        }

        try {
            User users = new User();
            users.setNewAdminForm(model);
            users.save();
            UserRole userRole = new UserRole(users.getUserId(), Long.valueOf(Access.ADMIN_KONTEN.getId()));
            userRole.save();
            userService.deleteToken(model.token);
            LogUtil.debug(TAG, "Mengganti null dengan timestamp pada column deleted_at :" + model.token);
            createAdminKontenSuccess(model.email);

        } catch (Exception e) {
            LogUtil.error(TAG, e);
            reloadForm(model, R.view.user_management_admin_register_admin_konten);
        }
    }

    public static void createAdminKontenSuccess(String email) {
        renderArgs.put("email", email);
        renderTemplate(R.view.user_management_user_successcreateadmin);
    }

    private static void reloadForm(NewUserForm model, String template) {
        renderArgs.put("email", model.email);
        renderArgs.put("token", model.token);
        renderArgs.put("action", model.action);
        renderArgs.put("model", model);
        renderTemplate(template);
    }

    public static void backLogin() {
        UserController.index();
    }

}
