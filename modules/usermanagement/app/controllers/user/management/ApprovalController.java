package controllers.user.management;

import models.user.management.CurrentUser;
import models.user.management.User;
import models.user.management.contracts.UserContract;
import notifiers.Mails;
import permission.Access;
import permission.Can;
import permission.Role;
import repositories.user.management.UserRepository;
import utils.common.LogUtil;

import java.util.List;

import static permission.Access.SUPER_ADMIN;

public class ApprovalController extends BaseAdminController {

    public static final String TAG = "ApprovalController";

    public static void index() {
        UserController.index();
    }

    @Can({SUPER_ADMIN})
    private static void detail(Long id) {
        User model = new UserRepository().getUser(id);
        notFoundIfNull(model);
        model.withRoles();
        renderArgs.put("users", model);
        renderTemplate("user-management/approval/detail.html");
    }

    @Can({SUPER_ADMIN})
    public static void list() {
        CurrentUser currentUser = CurrentUser.getInstance();
        if (currentUser != null) {
            UserContract user = getCurrentUser().user;
            renderArgs.put("user.name", user.getName());
            renderArgs.put("user.email", user.getEmail());
            renderTemplate("user-management/approval/index.html");
        }
    }

    @Can({SUPER_ADMIN})
    public static void updateApproval(Long id) {
        UserContract user = getCurrentUser().user;
        LogUtil.debug(TAG, id);
        if (!user.isSuperAdmin()) {
            flash.error("Tidak memiliki wewenang untuk melakukan update pada User tersebut");
            detail(user.getUserId());
        }
        User model = new UserRepository().getUser(id);
        notFoundIfNull(model);
        model.withRoles();
        List<Role> roles = Access.toRoles();
        renderArgs.put("userApproval", model);
        renderArgs.put("roles", roles);
        renderArgs.put("action", "update");
        renderArgs.put("isUser", user.isSuperAdmin());
        renderTemplate("user-management/approval/input.html");
    }

    @Can({SUPER_ADMIN})
    public static void approveUser(Long id) throws Exception {
        LogUtil.debug(TAG,id);
        final boolean result = userService.approveUser(id);
        if (result) {
            Mails.sendEmailApproveAdminKl(id);
            flash.success("Persetujuan pengajuan berhasil dilakukan");
            UserController.index();
        }
        flash.error("Persetujuan pengajuan gagal dilakukan");
        UserController.index();

    }

    @Can({SUPER_ADMIN})
    public static void rejectUser(Long id) throws Exception {
        LogUtil.debug(TAG,id);
        final boolean result = userService.rejectUser(id);
        if (result) {
            Mails.sendEmailRejectAdminKl(id);
            flash.success("Penolakan pengajuan berhasil dilakukan");
            UserController.index();
        }
        flash.error("Penolakan pengajuan gagal dilakukan");
        UserController.index();
    }
}
