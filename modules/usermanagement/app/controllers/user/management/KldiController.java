package controllers.user.management;

import com.google.gson.Gson;
import controllers.common.BaseController;
import permission.Access;
import permission.Can;
import permission.Kldi;
import play.libs.WS;
import play.mvc.Http;
import repositories.user.management.KldiRepository;
import utils.common.LogUtil;

public class KldiController extends BaseController {

    private static final String host ="https://sirup.lkpp.go.id/sirup/service/daftarKLDI";
    public static final String TAG = "KldiController";

    @Can({Access.SUPER_ADMIN})
    public static void index() {
        LogUtil.debug(TAG, "Get data satker sirup");
        try (WS.WSRequest request = WS.url(host)) {
            WS.HttpResponse response = request.get();
            if (response.getStatus() == Http.StatusCode.OK) {
                String content = response.getString();
                Gson gson = new Gson();
                Kldi.deleteAll();
                Kldi[] list = gson.fromJson(content, Kldi[].class);
                Kldi.saveAll(list);
            }
            UserController.index();
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            UserController.index();
        }
    }

}
