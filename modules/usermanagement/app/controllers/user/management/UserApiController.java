package controllers.user.management;

import constants.generated.R;
import controllers.common.ApiBaseController;
import models.cms.ContentFolder;
import models.common.ServiceResult;
import models.common.contracts.DataTableRequest;
import models.user.management.AdminklForm;
import models.user.management.AvatarForm;
import models.user.management.CurrentUser;
import models.user.management.User;
import permission.Approve;
import permission.Can;
import play.mvc.Router;
import repositories.user.management.UserRepository;
import utils.common.DirectoryUtil;
import utils.common.LogUtil;

import java.io.File;

import static permission.Access.*;

/**
 * @author HanusaCloud on 9/24/2019 4:18 PM
 */
public class UserApiController extends ApiBaseController {

    public static final String TAG = "UserApiController";

    @Can({ADMIN_KL})
    public static void userJson() {
        DataTableRequest dataTableRequest = new DataTableRequest(request);
        renderJSON(userRepository.getUserResult(getCurrentUser(), dataTableRequest).getResultsAsJson());
    }

    public static void createAdminForm(AdminklForm model) throws Exception {
        LogUtil.debug(TAG, model);
        UserRepository repository = new UserRepository();
        User user = repository.getUser(model.id);
        validation.valid(model);
        if (validation.hasErrors()) {
            params.flash();
            renderJSON(new ServiceResult(R.translation.form_re_check).toJson());
        }
        try {
            if (user.approve == Approve.APPROVAL_ADMIN_KL.getId()
                    || user.approve == Approve.ACCEPTED_ADMIN_KL.getId()) {
                renderJSON(new ServiceResult(
                        false,
                        R.translation.user_management_had_created_admin_before
                ).toJson());
            }
            DirectoryUtil.createDir(ContentFolder.FOLDER_DOCUMENT);
            File myfile = model.uploadfile;
            model.upload_file = ContentFolder.FOLDER_DOCUMENT + model.uploadfile.getName() + "";
            myfile.renameTo(new File(ContentFolder.PATH_DOCUMENT + model.uploadfile.getName()));
            userService.storeAdmin(model);
            LogUtil.debug(TAG, "User admin k/l berhasil ditambahkan");
            renderJSON(new ServiceResult<>(
                    true,
                    R.translation.user_management_create_admin_success,
                    Router.reverse(R.route.core_landingcontroller_index).url
            ).toJson());
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            renderJSON(new ServiceResult("User admin K/L/P/D gagal ditambahkan").toJson());
        }
    }

    @Can({ADMIN_KL, SUPER_ADMIN, ADMIN_KONTEN, PUBLIC_USER})
    public static void storeAvatar(AvatarForm model) throws Exception {
        LogUtil.debug(TAG, model);
        try {
            DirectoryUtil.createDir(ContentFolder.FOLDER_PICTURE);
            String url = model.uploadAvatarImage().getUrl();
            User user = new UserRepository().getUser(model.id);
            user.setAvatarForm(model);
            user.saveModel();
            renderJSON(new ServiceResult<String>(
                    true,
                    "success",
                    url)
            );
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            renderJSON(new ServiceResult(R.translation.cms_failed_save_picture).toJson());
        }
    }

}
