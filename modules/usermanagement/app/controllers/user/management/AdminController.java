package controllers.user.management;

import constants.generated.R;
import controllers.core.LandingController;
import notifiers.Mails;
import permission.Access;
import permission.Can;
import utils.common.LogUtil;

public class AdminController extends BaseAdminController {

    @Can({Access.ADMIN_KL})
    public static void backLogin() {
        UserController.index();
    }

    @Can({Access.ADMIN_KL})
    public static void undangAdminKonten() {
        LogUtil.debug(TAG, "undang admin konten");
        renderTemplate(R.view.user_management_admin_undang_admin_konten);
    }

    @Can({Access.ADMIN_KL})
    public static void storeUndangAdminKonten(String email){
        final boolean result = userService.checkEmail(email);
        if (result) {
            LogUtil.debug(TAG,"Email sudah digunakan");
            flashError(R.translation.user_management_invite_email_not_available);
            undangAdminKonten();
        }
        try {
            Mails.sendMail(
                    email,
                    "http://monev-ng.cloud.javan.co.id/admin/register/admin-konten/",
                    "Lengkapi Registrasi Admin Konten anda"
            );
            UserController.index();
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            flashError(R.translation.user_management_send_email_failed);
            undangAdminKonten();
        }
    }

    public static void backLanding() {
        LandingController.index();
    }
}
