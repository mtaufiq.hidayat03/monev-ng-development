package controllers.user.management;

import constants.generated.R;
import controllers.common.BaseController;
import controllers.core.LandingController;
import models.common.ServiceResult;
import models.user.management.CurrentUser;
import models.user.management.contracts.UserContract;
import notifiers.Mails;
import org.apache.commons.lang3.StringUtils;
import play.mvc.Router;
import repositories.user.management.UserRepository;
import utils.common.LogUtil;
import utils.common.TokenUtil;

/**
 * @author HanusaCloud on 8/27/2019 10:32 AM
 */
public class AuthController extends BaseController {

    public static final String TAG = "AuthController";

    public static void store(String email, String password) {
        LogUtil.debug(TAG, "store login form");
        LogUtil.debug(TAG, "check authenticity");
        checkAuthenticity();
        LogUtil.debug(TAG, "login email: " + email);
        String digested = "";
        if (StringUtils.isEmpty(email)
                || StringUtils.isEmpty(password)
                || StringUtils.isEmpty((digested = TokenUtil.sha256(password)))) {
            LogUtil.debug(TAG, "Oops.. email or password is empty, exits!");
            renderJSON(new ServiceResult(R.translation.login_missing_username_or_password).toJson());
        }
        UserContract user = new UserRepository().getUser(email, digested);
        if (user == null) {
            LogUtil.debug(TAG, "Oops.. user not found!");
            renderJSON(new ServiceResult(R.translation.login_user_or_password_wrong).toJson());
        }
        user.withRoles();
        if (!user.roleExists()) {
            LogUtil.debug(TAG, "Oops.. either role or permission is empty");
            renderJSON(new ServiceResult(R.translation.login_failed).toJson());
        }
        CurrentUser currentUser = new CurrentUser(session.getId(), user);
        currentUser.save();
        String url = Router.reverse(R.route.user_management_usercontroller_index).url;
        if (currentUser.user.isAdminKl()) {
            url = Router.reverse(R.route.user_management_usercontroller_index).url + "?login=true";
        }
        if (currentUser.user.isAdminKonten()) {
            url = Router.reverse(R.route.cms_contentcontroller_index).url;
        }
        if (currentUser.user.isRegiseredUser()) {
            url = Router.reverse(R.route.core_landingcontroller_index).url;
        }
        LogUtil.debug(TAG, url);
        renderJSON(new ServiceResult<>(true, R.translation.login_success, url).toJson());
    }

    public static void logout() {
        LogUtil.debug(TAG, "logout user");
        if (getCurrentUser() != null) {
            getCurrentUser().clear();
        }
        //login();
        LandingController.index();
    }

    public static void forgot(String email) {
        final boolean result = userService.checkEmail(email);
        if (!result) {
            LogUtil.debug(TAG, "Email tidak ditemukan.");
            flash.error("Email tidak ditemukan. Silahkan periksa alamat yang Anda masukkan");
            renderJSON(new ServiceResult("Email tidak ditemukan. Silahkan periksa alamat yang Anda masukkan").toJson());
        }
        final boolean check = Mails.sendEmailForgotPassword(email);
        if (check) {
            renderJSON(new ServiceResult(true, "Permintaan anda sukses diproses. Silahkan cek " +
                    "email anda untuk melanjutkan proses selanjutnya.").toJson());
        }
        renderJSON(new ServiceResult(" Permintaan Anda gagal diproses. Silahkan ulangi beberapa saat lagi").toJson());
    }
}
