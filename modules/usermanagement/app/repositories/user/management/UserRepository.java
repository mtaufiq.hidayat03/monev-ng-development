package repositories.user.management;

import models.common.contracts.DataTablePagination;
import models.common.contracts.DataTableRequest;
import models.user.management.CurrentUser;
import models.user.management.User;
import models.user.management.contracts.UserContract;
import models.user.management.pojos.UserResult;
import permission.Access;
import permission.Approve;
import play.db.jdbc.Query;
import utils.common.LogUtil;

import java.util.List;

/**
 * @author HanusaCloud on 8/27/2019 11:05 AM
 */
public class UserRepository {

    public static final String TAG = "UserRepository";

    private static final UserRepository repository = new UserRepository();

    public static UserRepository getInstance() {
        return repository;
    }

    public List<User> getListUser(UserContract user, DataTableRequest dataTableRequest) {
        Integer start = dataTableRequest.getStart();
        Integer length = dataTableRequest.getLength();
        String search = dataTableRequest.getSearch();
        String kldiQuery = "";
        String searchQuery = "";
        if (user.isAdminKl()) {
            kldiQuery = " AND kldi_id = '" + user.getKldiId() + "'";
        }
        if (!search.equals("")) {
            searchQuery = " AND (lower(name) like '%" + search + "%' OR lower(email) like '%" + search + "%')";
        }
        return User.find("id != '1' AND deleted_at IS NULL" + kldiQuery + searchQuery + " LIMIT " + length + " OFFSET " + start).fetch();
    }

    public Integer getTotalRecords(UserContract user, DataTableRequest dataTableRequest) {
        String search = dataTableRequest.getSearch();
        String queryKldi = "";
        String searchQuery = "";
        if (!user.isSuperAdmin()) {
            queryKldi = " AND kldi_id = '" + user.getKldiId() + "'";
        }
        if (!search.equals("")) {
            searchQuery = " AND (lower(name) like '%" + search + "%' OR lower(email) like '%" + search + "%')";
        }
        return (int) Query.count("SELECT count(*) FROM public.user WHERE id != '1' AND deleted_at IS NULL" + queryKldi + searchQuery);
    }

    public List<User> getUsers() {
        LogUtil.debug(TAG, "get all users except deleted ones");
        return User.find("deleted_at IS NULL").fetch();
    }

    public User getUser(Long id) {
        LogUtil.debug(TAG, "find user by id: " + id);
        final User user = User.find("id =? AND deleted_at IS NULL", id).first();
        LogUtil.debug(TAG, "user exists: " + (user != null));
        return user;
    }

    public User getUser(String email, String password) {
        LogUtil.debug(TAG, "get user by email: " + email + " and password :" + password);
        return User.find("email =? AND password =? AND deleted_at IS NULL", email, password).first();
    }

    public long save(User user) {
        LogUtil.debug(TAG, "save user using bind update");
        return Query.bindUpdate("INSERT INTO public.user (email, name, password, role_id) values (:email, :name, :password, :role_id)", user);
    }

    public void update(User user) {
        LogUtil.debug(TAG, "update user using bind update");
        Query.bindUpdate("UPDATE public.user set email = :email, name = :name, password = :password, role_id = :role_id where id = :id", user);
    }

    public void waitingApprove(Long id) {
        LogUtil.debug(TAG, "update user approval = " + id);
        Integer waiting = (int) Approve.APPROVAL_ADMIN_KL.getId();
        Query.update("UPDATE public.user set approve ='" + waiting +
                "' where id =?", id);
    }

    public void rejectUpdate(Long id) {
        LogUtil.debug(TAG, "update user approval = " + id);
        Integer reject = (int) Approve.REJECTED_ADMIN_KL.getId();
        Integer role_id = (int) Access.PUBLIC_USER.getId();
        Query.update("UPDATE public.user set approve ='" + reject +
                "' where id =?", id);
        Query.update("UPDATE public.user_role set role_id ='" + role_id +
                "' where user_id =?", id);
    }

    public void approveUpdate(Long id) {
        LogUtil.debug(TAG, "update user approval = " + id);
        Integer approve = (int) Approve.ACCEPTED_ADMIN_KL.getId();
        Integer role_id = (int) Access.ADMIN_KL.getId();
        Query.update("UPDATE public.user set approve ='" + approve +
                "' where id =?", id);
        Query.update("UPDATE public.user_role set role_id ='" + role_id +
                "' where user_id =?", id);
    }

    public User getEmail(String email) {
        LogUtil.debug(TAG, "looking for email :" + email);
        return User.find("email =?", email).first();
    }

    public User getCreator(Long id) {
        return User.find("id =? AND deleted_at IS NULL", id).first();
    }

    public List<User> getListAdminkl() {
        return User.find("created_by=2").fetch();
    }

    public User getMaxId() {
        return User.find("order by id desc").first();
    }

    public UserResult getUserResult(CurrentUser currentUser, DataTablePagination.Request request) {
        Boolean isAdminKl = currentUser.user.isAdminKl();
        Boolean isSuperAdmin = currentUser.user.isSuperAdmin();
        UserResult userResult = new UserResult(request);
        if (isAdminKl) {
            String query = "SELECT * FROM public.user where kldi_id = '" + currentUser.user.getKldiId() + "'";
            userResult.modifiedQuery(query);
        }
        if (isSuperAdmin) {
            userResult.executeQuery();
        }
        return userResult;
    }

}
