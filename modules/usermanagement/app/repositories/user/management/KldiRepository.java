package repositories.user.management;

import models.user.management.contracts.UserContract;
import org.apache.commons.lang3.StringUtils;
import permission.Kldi;
import play.cache.Cache;
import play.db.jdbc.Query;
import utils.common.LogUtil;

import java.util.List;

public class KldiRepository {

    private static final KldiRepository repository = new KldiRepository();

    public static KldiRepository getInstance() {
        return repository;
    }

    private static final String TAG = "KldiRepository";

    public List<Kldi> getListKl() {
        return Query.find("SELECT * FROM kldi", Kldi.class).fetch();
    }

    public Kldi getById(String kldiId) {
        if (StringUtils.isEmpty(kldiId)) {
            return null;
        }
        final String key = "kldi_" + kldiId;
        Kldi kldi;
        if ((kldi = (Kldi) Cache.get(key)) != null) {
            return kldi;
        }
        kldi = Kldi.find("id = ?", kldiId).first();
        Cache.set(key, kldi, "5mn");
        return kldi;
    }

    public List<Kldi> getListKlByRole(UserContract userContract) {
        String queryKldi = "";
        if (!userContract.isSuperAdmin()) {
            queryKldi = " where id = '" + userContract.getKldiId() + "'";
        }
        return Query.find("SELECT * FROM kldi"+ queryKldi +" ORDER BY nama_instansi", Kldi.class).fetch();
    }

    public List<Kldi> getListKlByToken(String kldi_id) {
        return Query.find("SELECT * FROM kldi where id ='"+kldi_id+"'", Kldi.class).fetch();
    }

    public void resetSequence(){
        LogUtil.debug(TAG, "Reset sequence");
        Query.update("ALTER SEQUENCE kldi_no_seq RESTART WITH 1 ");
    }
}
