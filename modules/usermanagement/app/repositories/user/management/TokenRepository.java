package repositories.user.management;

import models.user.management.Token;
import utils.common.LogUtil;

import java.util.List;

public class TokenRepository {
    public static final String TAG = "TokenRepository";

    public Token getToken(String token) {
        LogUtil.debug(TAG, "Mulai mencari token:"+token);
        return Token.find("token =? AND deleted_at IS NULL", token).first();
    }

    public Token getToken(String token, Token.Type type) {
        LogUtil.debug(TAG, "Mulai mencari token:"+token);
        return Token.find("token =? AND token_type=? AND deleted_at IS NULL", token, type).first();
    }

    public List<Token> getListApprovalAdmin() {
        LogUtil.debug(TAG, "Mulai mencari token dengan admin_kl=1 dan deleted_at null");
        return Token.find("admin_kl=1 AND deleted_at IS NULL").fetch();
    }
    public Token getCreatedBytoken(String token) {
        LogUtil.debug(TAG, "get created_by by token: " + token);
        return Token.find("token =? AND deleted_at IS NULL", token).first();
    }
}
