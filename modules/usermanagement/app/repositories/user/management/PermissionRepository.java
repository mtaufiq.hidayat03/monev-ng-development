package repositories.user.management;

import permission.*;
import play.db.jdbc.Query;
import utils.common.LogUtil;

import java.util.List;

/**
 * @author HanusaCloud on 8/27/2019 10:59 AM
 */
@Deprecated
public class PermissionRepository {

    public static final String TAG = "PermissionRepository";

    public void detachRoles(Long userId) {
        UserRole.delete("user_id =?", userId);
    }

    public void attachRoles(List<UserRole> roles) {
        if (roles != null && !roles.isEmpty()) {
            for (UserRole userRole : roles) {
                userRole.save();
                //  Query.bindUpdate("INSERT into public.role (name) values (:name)", userRole);
            }
        }
    }

    public void deleteAllPermissionByUser(Integer userId) {
        PermissionItem.delete("user_id =?", userId);
    }

    public void attachPermission(PermissionItem item) {
        item.save();
    }

    public List<PermissionItem> getPermissionsById(Integer userId) {
        return PermissionItem.find("user_id =?", userId).fetch();
    }

    public List<PermissionItem> getPermissions() {
        return Query.find("SELECT * FROM permission", PermissionItem.class).fetch();
    }

    public List<RolePermission> getPermissions(Long userId) {
        LogUtil.debug(TAG, "get permission by userId: " + userId);
        final List<RolePermission> result = Query.find("SELECT rp.permission_id, p.name\n" +
                "FROM user_role ur\n" +
                "JOIN role r\n" +
                "ON r.id = ur.role_id\n" +
                "JOIN role_permission rp\n" +
                "ON rp.role_id = r.id\n" +
                "JOIN permission p\n" +
                "ON p.id = rp.permission_id\n" +
                "WHERE ur.user_id =?", RolePermission.class, userId).fetch();
        LogUtil.debug(TAG, result);
        return result;
    }

    public List<UserRole> getRoles(Long userId) {
        LogUtil.debug(TAG, "get user's roles by userId: " + userId);
        return Query.find("SELECT ur.role_id, r.name " +
                "FROM user_role ur " +
                "JOIN role r ON r.id = ur.role_id " +
                "WHERE ur.user_id =? ", UserRole.class, userId).fetch();
    }

    public Role getRole(Long id) {
        return Query.find("SELECT id, name, description FROM role where id =? AND deleted_at IS NULL", Role.class, id).first();
    }

    public List<RolePermission> getRolePermissions(Long roleId) {
        LogUtil.debug(TAG, "get role permission by roleId: " + roleId);
        return Query.find("SELECT rp.permission_id, p.name " +
                "FROM role r " +
                "JOIN role_permission rp " +
                "ON rp.role_id = r.id " +
                "JOIN permission p " +
                "ON p.id = rp.permission_id " +
                "WHERE r.id =?", RolePermission.class, roleId).fetch();
    }

    public void detachRolePermissions(Long roleId) {
        Query.update("DELETE FROM role_permission where role_id =?", roleId);
    }

    public int save(RolePermission model){
        return Query.bindUpdate("INSERT INTO public.role_permission (role_id, permission_id) values (:role_id, :permission_id)", model);
    }

    public int totalRoleUsage(Long roleId) {
        return Query.find("SELECT COUNT(*) FROM user_role WHERE role_id =?", Integer.class, roleId).first();
    }

}
