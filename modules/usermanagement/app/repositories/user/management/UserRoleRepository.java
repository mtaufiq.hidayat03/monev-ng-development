package repositories.user.management;

import permission.Access;
import permission.Role;
import permission.UserRole;
import play.db.jdbc.Query;
import utils.common.LogUtil;

public class UserRoleRepository {

    public static final String TAG = "UserRoleRepository";

    public void updateAdminKl(Long id){
        LogUtil.debug(TAG, "update admin k/l = "+id);
        Integer role_id = (int) Access.PUBLIC_USER.getId();
        Query.update("UPDATE public.user_role set role_id ='"+role_id+
                "' where user_id =?", id);
    }

    public UserRole getUserRole(Long userId) {
       return Query.find("SELECT * FROM user_role where user_id =?", UserRole.class, userId).first();
    }
}
