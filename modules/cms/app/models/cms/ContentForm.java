package models.cms;

import models.cms.contracts.ContentFeaturedImage;
import models.cms.contracts.ContentTypeContract;
import models.cms.contracts.FileContract;
import models.common.contracts.FormContract;
import models.common.contracts.KldiContract;
import models.common.contracts.PermissionContract;
import models.common.contracts.PublishingStatusContract;
import org.apache.commons.lang3.StringUtils;
import play.data.validation.Required;
import utils.cms.UploadedFile;

import java.io.File;
import java.util.List;

public class ContentForm implements FormContract,
        PermissionContract,
        KldiContract,
        PublishingStatusContract,
        FileContract,
        ContentFeaturedImage,
        ContentTypeContract
{

    public static final String TAG = "ContentForm";

    public String action = "create";
    @Required
    public String title;
    @Required
    public String content;
    public String kldiId;
    public File uploadfile;
    public String featuredImage;
    public File uploadregulasi;
    public String regulation;
    public Long id;
    @Required
    public Integer publish;
    public String akses;
    public String userSelected;
    public String permission = ContentPermission.PUBLIC.toLowerCase();
    public List<File> uploadExtra;
    public String[] deletedFiles;
    public UploadedFile uploadedFeaturedImage;

    public List<UploadedFile> uploadedExtras;

    @Override
    public String getAction() {
        return action;
    }

    public ContentForm() {
    }

    public ContentForm(Content model) {
        id = model.id;
        title = model.title;
        content = model.content;
        kldiId = model.kldi_id;
        publish = model.publish;
        permission = model.permission;
        action = UPDATE;
        uploadedExtras = model.getArticleExtras();
        uploadedFeaturedImage = model.getFeaturedImageFile();
    }

    @Override
    public String getPermission() {

        return permission;
    }

    @Override
    public String getKldi() {
        return kldiId;
    }

    @Override
    public Integer getStatus() {
        return publish;
    }

    @Override
    public File getFeaturedImageFile() {
        return uploadfile;
    }

    @Override
    public String[] getDeletedFiles() {
        return deletedFiles;
    }

    @Override
    public List<File> getExtraFiles() {
        return uploadExtra;
    }

    @Override
    public void setFeaturedImage(String image) {
        this.featuredImage = image;
    }

    @Override
    public UploadedFile getUploadedFeaturedImage() {
        return uploadedFeaturedImage;
    }

    public boolean allowToStore() {
        return !(isEditedMode() && id == null)
                && !StringUtils.isEmpty(title)
                && !StringUtils.isEmpty(content)
                && !(isCreatedMode() && uploadfile == null)
                && publish != null;
    }

    @Override
    public String getContentType() {
        return ContentType.ARTIKEL_UMUM;
    }
}