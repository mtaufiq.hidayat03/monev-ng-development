package models.cms;

import models.cms.contracts.ContentContract;
import models.cms.profilekl.ProfileForm;
import models.cms.profilekl.ProfileKlData;
import models.common.BaseModel;
import models.common.contracts.annotations.SearchAble;
import models.common.contracts.annotations.SoftDelete;
import models.searchcontent.contracts.ElasticItemDetail;
import models.searchcontent.contracts.ElasticRequestPayloadContract;
import org.apache.commons.lang3.StringUtils;
import play.db.jdbc.JsonBinaryType;
import play.db.jdbc.Table;
import tableau.library.models.TableauLibrary;

import javax.persistence.Transient;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Table(name = "contents", schema = "public")
@SoftDelete
public class Content extends BaseModel implements ContentContract,
        Serializable,
        ElasticRequestPayloadContract,
        ElasticItemDetail
{

    @SearchAble
    public String title;
    public String vision;
    public String mission;
    public String content;
    @JsonBinaryType
    public String regulation;
    @JsonBinaryType
    public String mou;
    public String featured_image;
    public String type;
    public String kldi_id;
    public String permission;
    public Integer publish;
    public String free_content;
    public String alamat_url;
    public Long tableau_library_id;

    @Transient
    private TableauLibrary library;
    @Transient
    public String[] content_permission;
    @Transient
    public String kldi_name;
    @Transient
    public String[] publishing_status;
    @Transient
    public Long viewer;

    @Transient
    private List<ProfileKlData> profileData;
    @Transient
    private List<AksesContent> accessContents;

    public Content() {}

    public static Content getNewInstance() {
        return new Content();
    }

    public Content(Long contentId) {
        this.id = contentId;
    }

    public void setProfileklForm(ProfileForm model){
        this.title = model.title;
        this.vision = model.vision;
        this.mission = model.mission;
        this.mou = model.mou;
        this.publish = model.publish;
        this.kldi_id = model.kldiId;
        this.type = ContentType.PROFILE_KLPD;
        if (!StringUtils.isEmpty(model.featuredImage)) {
            this.featured_image = model.featuredImage;
        }
        if (!StringUtils.isEmpty(model.regulation)) {
            this.regulation = model.regulation;
        }
        if (!StringUtils.isEmpty(model.mou)) {
            this.mou = model.mou;
        }
        this.content = model.content;
        this.permission = model.permission;
        if (!StringUtils.isEmpty(model.free_content)) {
            this.free_content = model.free_content;
        }
    }

    public void setContentForm(ContentForm model) {
        this.title = model.title;
        this.content = model.content;
        this.publish = model.publish;
        if (!StringUtils.isEmpty(model.featuredImage)) {
            this.featured_image = model.featuredImage;
        }
        this.kldi_id = model.kldiId;
        this.type = ContentType.ARTIKEL_UMUM;
        this.permission = model.permission;
    }

    public void setVisualisasiTableauForm(VisualisasiTableauForm model){
        this.title = model.title;
        this.alamat_url = model.getQueryString();
        this.content = model.content;
        this.publish = model.publish;
        this.kldi_id = model.kldiId;
        this.type = ContentType.VISUALISASI_TABLEAU;
        this.permission = model.permission;
        this.tableau_library_id = model.libraryId;
    }

    public void setPublishingStatuses(String[] status) {
        publishing_status = status;
    }

    @Override
    public void setViewer(Long value) {
        viewer = value;
    }

    @Override
    public List<AksesContent> getAccessContents() {
        return this.accessContents;
    }

    @Override
    public Long getContentId() {
        return this.id;
    }

    @Override
    public Integer getStatus() {
        return this.publish;
    }

    @Override
    public String getKl() {
        return this.kldi_id;
    }

    @Override
    public Timestamp getCreatedAt() {
        return created_at;
    }

    public String getAbbrContent() {
        return StringUtils.abbreviate(content, 250);
    }
    
    @Override
    public Long getTableauLibraryId() {
        return tableau_library_id;
    }

    @Override
    public TableauLibrary getLibrary() {
        return this.library;
    }

    @Override
    public void setLibrary(TableauLibrary model) {
        this.library = model;
    }

    @Override
    public String getUrl() {
        return alamat_url;
    }

    @Override
    public String getKldi() {
        return kldi_id;
    }

    @Override
    public Long getCreatedBy() {
        return created_by;
    }

    @Override
    public void setKldiName(String name) {
        this.kldi_name = name;
    }

    @Override
    public String getFreeContentFile() {
        return free_content;
    }

    @Override
    public String getMouFile() {
        return mou;
    }

    @Override
    public String getRegulationFile() {
        return regulation;
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public String getPermission() {
        return permission;
    }

    @Override
    public void setPermissions(String[] permissions) {
        content_permission = permissions;
    }

    @Override
    public Long getModelId() {
        return id;
    }

    @Override
    public String getModelType() {
        return getContentType();
    }

    @Override
    public String getModelUrl() {
        return getViewUrl();
    }

    @Override
    public void setModelRequirements() {
        generateModelPermission();
        generateModelKldiName();
        content = cleanContentFromTags(content);
        vision = cleanContentFromTags(vision);
        mission = cleanContentFromTags(mission);
        generatePublishingStatus();
        generateTotalViewer();
    }

    @Override
    public String getDetailTitle() {
        return title;
    }

    @Override
    public String getDetailContent() {
        if (isProfileKl()) {
            return getAbbreviate(vision);
        }
        if (content == null) {
            return "";
        }
        return getAbbreviate(content);
    }

    @Override
    public String getMeta() {
        return "";
    }

    @Override
    public String getFeaturedImage() {
        return featured_image;
    }

    @Override
    public void setAccessContents(List<AksesContent> accessContents) {
        this.accessContents = accessContents;
    }

    @Override
    public String getImageUrl() {
        return getFeaturedImageUrl();
    }

    @Override
    public List<ProfileKlData> getProfileData() {
        return profileData;
    }

    @Override
    public void setProfileData(List<ProfileKlData> data) {
        this.profileData = data;
    }

    @Override
    public String getContentType() {
        return type;
    }

    @Override
    public void saveModel() {
        super.saveModel();
        if (accessContents != null && accessContents.isEmpty()) {
            accessContents.stream().mapToInt(AksesContent::save).count();
        }
    }
}