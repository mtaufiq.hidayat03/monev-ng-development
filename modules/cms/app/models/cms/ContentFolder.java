package models.cms;

import play.Play;

public class ContentFolder {

    public static final String APP_PATH = Play.configuration.getProperty("uploadDir");
    public static final String FOLDER_PICTURE = "/uploadfile/picture/";
    public static final String FOLDER_DOCUMENT = "/uploadfile/document/";
    public static final String PATH_UPLOAD = APP_PATH+"/uploadfile/";
    public static final String PATH_PICTURE = APP_PATH+"/uploadfile/picture/";
    public static final String PATH_DOCUMENT = APP_PATH+"/uploadfile/document/";
    public static final String REPORT_CACHE = APP_PATH+"/uploadfile/cache/reports";

}
