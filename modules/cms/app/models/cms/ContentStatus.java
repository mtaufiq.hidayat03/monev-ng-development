package models.cms;

import play.db.jdbc.Enumerated;

import javax.persistence.EnumType;
import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 9/20/2019 5:25 PM
 */
@Enumerated(EnumType.ORDINAL)
public enum ContentStatus {

    DRAFT(0,"DRAFT","DRAFT ARTIKEL"),
    STOCK_REVIEW(1,"STOCK REVIEW","STOCK REVIEW YANG MEMBUTUHKAN APPROVAL SUPER ADMIN"),
    PUBLISH (2,"PUBLISH","PUBLISH ARTIKEL. HANYA BISA DIAKSES SUPER ADMIN");

    public final Integer id;
    public final String name;
    public final String desc;

    ContentStatus(Integer id, String name, String desc) {
        this.id = id;
        this.name = name;
        this.desc = desc;
    }

    public Integer getContentStatusId() {
        return id;
    }

    public static ContentStatus fromId(int id) {
        switch (id) {
            case 0:
                return ContentStatus.DRAFT;
            case 1:
                return ContentStatus.STOCK_REVIEW;
            case 2:
                return ContentStatus.PUBLISH;
            default:
                return null;
        }
    }

    public static List<ContentStatus> getStatus() {
        List<ContentStatus> myStatus = new ArrayList<>();
        myStatus.add(DRAFT);
        myStatus.add(STOCK_REVIEW);
        myStatus.add(PUBLISH);
        return myStatus;
    }

}
