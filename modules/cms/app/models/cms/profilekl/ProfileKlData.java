package models.cms.profilekl;

import models.user.management.CurrentUser;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import utils.common.LogUtil;

import javax.persistence.Transient;
import java.sql.Timestamp;
import java.util.List;

/**
 * @author HanusaCloud on 12/2/2019 4:29 PM
 */
@Table(name = "profile_kl_data", schema = "public")
public class ProfileKlData extends BaseTable {

    public static final String TAG = "ProfileKlData";

    @Id
    public Long id;
    public String header;
    public Double value;
    public Timestamp created_at;
    public Long created_by;
    public Long content_id;

    @Transient
    public ProfileKlHeader profileHeader;

    public ProfileKlData() {}

    public ProfileKlData(ProfileKlHeader header, Double value) {
        this.header = header.getName();
        this.value = value;
    }

    @Override
    protected void prePersist() {
        super.prePersist();

        CurrentUser session = CurrentUser.getInstance();
        Long currentUser;
        if (session == null
                || session.user == null
                || (currentUser = session.user.getUserId()) == null) {
            currentUser = -999L;
        }

        if (this.id == null) {
            created_at = new Timestamp(System.currentTimeMillis());
            created_by = currentUser;
        }
        if (this.id == null) {
            id = Query.find("SELECT nextval('profile_kl_data_id_seq'::regclass)", Long.class).first();
        }
    }

    public ProfileKlHeader getProfileHeader() {
        if (profileHeader == null) {
            try {
                profileHeader = ProfileKlHeader.valueOf(header.toUpperCase());
            } catch (Exception e) {
                LogUtil.error(TAG, e);
            }
        }
        return profileHeader;
    }

    public static List<ProfileKlData> getDataBy(Long contentId) {
        LogUtil.debug(TAG, "get data by content: " + contentId);
        final List<ProfileKlData> results = ProfileKlData.find("content_id = ?", contentId).fetch();
        LogUtil.debug(TAG, "is data empty: " + (results.isEmpty()));
        return results;
    }

}
