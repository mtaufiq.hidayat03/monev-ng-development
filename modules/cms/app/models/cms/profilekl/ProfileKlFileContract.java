package models.cms.profilekl;

import models.cms.contracts.FileContract;
import utils.cms.FileUtil;
import utils.cms.UploadedFile;

import java.io.File;
import java.util.List;

/**
 * @author HanusaCloud on 12/18/2019 2:10 PM
 */
public interface ProfileKlFileContract extends FileContract {

    List<File> getMous();
    List<File> getRegulations();

    default String getJsonMouFiles(List<UploadedFile> currentFiles) {
        return uploadMultipleFilesAsString(currentFiles, getMous());
    }

    default String getJsonRegulationFiles(List<UploadedFile> currentFiles) {
        return uploadMultipleFilesAsString(currentFiles, getRegulations());
    }

    default boolean allowUpdateMou() {
        return getDeletedFiles() != null || getMous() != null;
    }

    default boolean allowUpdateRegulation() {
        return getDeletedFiles() != null || getRegulations() != null;
    }

}
