package models.cms.profilekl;

import models.cms.Content;
import models.cms.ContentPermission;
import models.cms.ContentType;
import models.cms.contracts.ContentFeaturedImage;
import models.cms.contracts.ContentTypeContract;
import models.common.contracts.FormContract;
import models.common.contracts.KldiContract;
import models.common.contracts.PermissionContract;
import models.common.contracts.PublishingStatusContract;
import play.data.validation.Required;
import utils.cms.UploadedFile;

import java.io.File;
import java.util.List;

public class ProfileForm implements FormContract,
        PermissionContract,
        PublishingStatusContract,
        KldiContract,
        ContentFeaturedImage,
        ProfileKlFileContract,
        ContentTypeContract
{

    public static final String TAG = "ProfileForm";
    public String action = CREATE;
    public Long id;
    @Required
    public String title;
    @Required
    public String vision;
    @Required
    public String mission;
    public String kldiId;
    public File uploadfile;
    public String featuredImage;
    public List<File> uploadMou;
    public String mou;
    public List<File> uploadRegulasi;
    public String regulation;
    public List<File> uploadExtra;
    @Required
    public String content;
    public String free_content;
    public String type;
    @Required
    public Integer publish;
    public String akses;
    public String userSelected;
    public String permission = ContentPermission.PUBLIC.toLowerCase();

    public String[] deletedFiles;
    public UploadedFile uploadedFeaturedImage;
    public List<UploadedFile> uploadedExtras;
    public List<UploadedFile> uploadedMous;
    public List<UploadedFile> uploadedRegulations;

    public ProfileForm() {}

    public ProfileForm(Content model) {
        action = UPDATE;
        id = model.id;
        content = model.content;
        free_content = model.free_content;
        publish = model.publish;
        permission = model.permission;
        mission = model.mission;
        title = model.title;
        vision = model.vision;
        kldiId = model.kldi_id;
        uploadedFeaturedImage = model.getFeaturedImageFile();
        uploadedExtras = model.getArticleExtras();
        uploadedMous = model.getMouFiles();
        uploadedRegulations = model.getRegulationFiles();
    }

    @Override
    public String getAction() { return action; }

    @Override
    public String getPermission() {
        return permission;
    }

    @Override
    public Integer getStatus() {
        return publish;
    }

    @Override
    public String getKldi() {
        return kldiId;
    }

    @Override
    public UploadedFile getUploadedFeaturedImage() {
        return uploadedFeaturedImage;
    }

    @Override
    public File getFeaturedImageFile() {
        return uploadfile;
    }

    @Override
    public String[] getDeletedFiles() {
        return deletedFiles;
    }

    @Override
    public List<File> getExtraFiles() {
        return uploadExtra;
    }

    @Override
    public void setFeaturedImage(String image) {
        this.featuredImage = image;
    }

    @Override
    public List<File> getMous() {
        return uploadMou;
    }

    @Override
    public List<File> getRegulations() {
        return uploadRegulasi;
    }

    @Override
    public String getContentType() {
        return ContentType.PROFILE_KLPD;
    }
}
