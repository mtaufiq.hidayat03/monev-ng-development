package models.cms.profilekl;

import models.cms.contracts.ContentPrimaryContract;
import models.common.contracts.KldiContract;
import models.dashboard.RekapPengadaan;
import permission.Kldi;
import ppsdm.CertificateHolderRepository;
import ppsdm.pojos.OccupationalMeta;
import ppsdm.pojos.PbjCertificateMeta;
import ppsdm.pojos.PbjMeta;
import repositories.dashboard.RekapPengadaanRepository;
import repositories.searchcontent.BlacklistElasticRepository;
import utils.common.LogUtil;

import java.util.ArrayList;
import java.util.List;

import static models.cms.profilekl.ProfileKlHeader.BLACKLIST;
import static models.cms.profilekl.ProfileKlHeader.SERTIFIKAT_PBJ_TINGKAT_DASAR;

/**
 * @author HanusaCloud on 12/2/2019 4:47 PM
 */
public interface ContentProfileDataContract extends
        ContentPrimaryContract,
        KldiContract,
        ProfileKlDataUtilContract
{

    String TAG = "ContentProfileDataContract";

    void setProfileData(List<ProfileKlData> data);

    default void withProfileData() {
        LogUtil.debug(TAG, "get profile data by content id: " + getContentId());
        if (getContentId() == null) {
            return;
        }
        if (getProfileData() != null && !getProfileData().isEmpty()) {
            return;
        }
        setProfileData(ProfileKlData.getDataBy(getContentId()));
    }

    default void attachProfileData() {
        LogUtil.debug(TAG, "attach profile data");
        if (getContentId() == null) {
            LogUtil.warn(TAG, "Oops.. content id is null exit");
            return;
        }
        final Kldi kldi = getKldiData();
        if (kldi == null) {
            LogUtil.warn(TAG, "Oops.. no kldi is set for profil kl");
            return;
        }
        List<ProfileKlData> data = new ArrayList<>();
        Long totalBlacklist = BlacklistElasticRepository.getInstance().getTotalByKldiAggragation(getKldi());
        data.add(new ProfileKlData(BLACKLIST, totalBlacklist.doubleValue()));
        final PbjMeta pbjMeta = new PbjMeta(kldi);
        data.addAll(pbjMeta.getProfileData());
        final OccupationalMeta occupationalMeta = new OccupationalMeta(kldi);
        data.addAll(occupationalMeta.getProfileData());
        data.add(new ProfileKlData(
                SERTIFIKAT_PBJ_TINGKAT_DASAR,
                CertificateHolderRepository.getInstance().getCountBy(kldi).doubleValue()
                )
        );
        RekapPengadaan rekapPengadaan = RekapPengadaanRepository.getInstance().getBy(kldi);
        if (rekapPengadaan != null) {
            data.addAll(rekapPengadaan.getProfileData());
        }
        final PbjCertificateMeta pbjCertificateMeta = new PbjCertificateMeta(kldi);
        data.addAll(pbjCertificateMeta.getProfileData());
        LogUtil.debug(TAG, data);
        setProfileData(data);
        for (ProfileKlData profileKlData : getProfileData()) {
            profileKlData.content_id = getContentId();
            profileKlData.save();
        }
    }

    default RekapPengadaan getRekapPengadaan() {
        withProfileData();
        return new RekapPengadaan(getProfileData());
    }

    default PbjCertificateMeta getCertificateCompetencies() {
        withProfileData();
        return new PbjCertificateMeta(getProfileData());
    }

    default PbjMeta getJabfungPbj() {
        withProfileData();
        return new PbjMeta(getProfileData());
    }

    default Long getTotalBlacklist() {
        withProfileData();
        return searchFor(BLACKLIST, getProfileData());
    }

    default OccupationalMeta getOcupationalMeta() {
        withProfileData();
        return new OccupationalMeta(getProfileData());
    }

    default Long getTotalCertificateHolder() {
        withProfileData();
        return searchFor(SERTIFIKAT_PBJ_TINGKAT_DASAR, getProfileData());
    }

}
