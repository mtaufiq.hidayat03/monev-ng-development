package models.cms.profilekl;

import play.db.jdbc.Enumerated;

import javax.persistence.EnumType;

/**
 * @author HanusaCloud on 12/2/2019 4:33 PM
 */
@Enumerated(EnumType.ORDINAL)
public enum  ProfileKlHeader {

    TOTAL_ANGGARAN("total_anggaran", "Total Anggaran", 1),
    BELANJA_PENGADAAN("belanja_pengadaan", "Belanja Pengadaan", 2),
    PERENCANAAN("perencanaan", "Perencanaan", 3),
    PELAKSANAAN("pelaksanaan", "Pelaksanaan", 4),
    HASIL_PEMILIHAN("hasil_pemilihan", "Hasil Pemilihan", 5),
    KONTRAK("kontrak", "Kontrak", 6),
    PEMBAYARAN("pembayaran", "Pembayaran", 23),
    BLACKLIST("blacklist", "Daftar Hitam", 7),
    SERTIFIKAT_PBJ_TINGKAT_DASAR("sertifikat_pbj_tingkat_dasar", "Sertifikat PBJ Tingkat Dasar", 10),
    JABFUNG_PBJ_TOTAL("jabfung_pbj_total", "Jabfung PBJ", 11),
    JABFUNG_PBJ_MADYA("jabfung_pbj_madya", "Jabfung PBJ Madya", 12),
    JABFUNG_PBJ_MUDA("jabfung_pbj_muda", "Jabfung PBJ Muda", 13),
    JABFUNG_PBJ_PERTAMA("jabfung_pbj_pertama", "Jabfung PBJ Pertama", 14),
    SERTIFIKAT_KOMPETENSI_TOTAL("sertifikat_kompetensi_total", "Sertifikat Kompetensi Total", 15),
    SERTIFIKAT_KOMPETENSI_MUDA("sertifikat_kompetensi_muda", "Sertifikat Kompetensi Muda", 17),
    SERTIFIKAT_KOMPETENSI_MADYA("sertifikat_kompetensi_madya", "Sertifikat Kompetensi Madya", 18),
    SERTIFIKAT_KOMPETENSI_PERTAMA("sertifikat_kompetensi_pertama", "Sertifikat Kompetensi Pertama", 16),
    SERTIFIKAT_OKUPASI("sertifikat_okupasi", "Sertifikat Okupasi", 19),
    SERTIFIKAT_OKUPASI_PP("sertifikat_okupasi_pp", "Sertifikat Okupasi PP", 20),
    SERTIFIKAT_OKUPASI_POKJA("sertifikat_okupasi_pokja", "Sertifikat Okupasi POKJA", 21),
    SERTIFIKAT_OKUPASI_PPK("sertifikat_okupasi_ppk", "Sertifikat Okupasi PPK", 22),
    ;

    private final String name;
    private final String label;
    private final int order;

    ProfileKlHeader(String name, String label, int order) {
        this.name = name;
        this.label = label;
        this.order = order;
    }

    public String getName() {
        return name;
    }

    public String getLabel() {
        return label;
    }}
