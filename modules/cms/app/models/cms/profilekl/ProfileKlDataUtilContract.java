package models.cms.profilekl;

import java.util.Collections;
import java.util.List;

/**
 * @author HanusaCloud on 12/3/2019 9:53 AM
 */
public interface ProfileKlDataUtilContract {

    default List<ProfileKlData> getProfileData() {
        return Collections.emptyList();
    }

    default Long searchFor(ProfileKlHeader header, List<ProfileKlData> onData) {
        Long result = 0L;
        for (ProfileKlData profileKlData : onData) {
            if (profileKlData.header.equalsIgnoreCase(header.getName())) {
                return profileKlData.value.longValue();
            }
        }
        return result;
    }

}
