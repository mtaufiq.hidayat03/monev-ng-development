package models.cms.contracts;

/**
 * @author HanusaCloud on 12/2/2019 4:48 PM
 */
public interface ContentPrimaryContract {

    Long getContentId();

}
