package models.cms.contracts;

import utils.cms.FileUtil;
import utils.cms.UploadedFile;
import utils.common.JsonUtil;
import utils.common.LogUtil;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author HanusaCloud on 12/13/2019 11:01 AM
 */
public interface FileContract {

    String TAG = "FileContract";

    File getFeaturedImageFile();
    String[] getDeletedFiles();
    List<File> getExtraFiles();
    void setFeaturedImage(String image);

    default Set<String> getDeletedAsSet() {
        if (getDeletedFiles() == null) {
            return new HashSet<>();
        }
        return new HashSet<>(Arrays.asList(getDeletedFiles()));
    }

    default List<UploadedFile> deleteByFiltering(List<UploadedFile> uploadedFiles) {
        if (uploadedFiles == null || uploadedFiles.isEmpty()) {
            LogUtil.debug(TAG, "uploaded files empty do not proceed");
            return new ArrayList<>();
        }
        Set<String> deleted = getDeletedAsSet();
        if (deleted.isEmpty()) {
            LogUtil.debug(TAG, "no files to delete");
            return uploadedFiles;
        }

        return uploadedFiles.stream()
                .map(e -> {
                    if (deleted.contains(e.getNewFileName())) {
                        FileUtil.deleteFile(e.getPath());
                        return null;
                    }
                    return e;
                })
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    default String getJsonExtraFiles(List<UploadedFile> currentFiles) {
        return uploadMultipleFilesAsString(currentFiles, getExtraFiles());
    }

    default String uploadMultipleFilesAsString(
            List<UploadedFile> currentFiles,
            List<File> uploadedFiles
    ) {
        List<UploadedFile> files = uploadMultipleFiles(currentFiles, uploadedFiles);
        final String resultJson = JsonUtil.toJson(files);
        LogUtil.debug(TAG, "files: " + resultJson);
        return resultJson;
    }

    default List<UploadedFile> uploadMultipleFiles(
            List<UploadedFile> currentFiles,
            List<File> uploadedFiles
    ) {
        LogUtil.debug(TAG, "process extra files");
        List<UploadedFile> saved = deleteByFiltering(currentFiles);
        List<UploadedFile> files = FileUtil.uploadMultipleFiles(uploadedFiles);
        files.addAll(saved);
        return files;
    }

    default UploadedFile uploadFeaturedImage() {
        if (getFeaturedImageFile() == null) {
            LogUtil.debug(TAG, "Oops.. featured image file is empty");
            return null;
        }
        final UploadedFile featured = FileUtil.uploadImageFile(getFeaturedImageFile());
        if (featured == null || !featured.fileExist()) {
            LogUtil.debug(TAG, "Oops.. featured file object is null");
            return null;
        }
        setFeaturedImage(JsonUtil.toJson(featured));
        return featured;
    }

    default boolean allowUpdateExtra() {
        return getDeletedFiles() != null || getExtraFiles() != null;
    }

}
