package models.cms.contracts;

import utils.cms.UploadedFile;

/**
 * @author HanusaCloud on 12/18/2019 1:09 PM
 */
public interface ContentFeaturedImage {

    UploadedFile getUploadedFeaturedImage();

}
