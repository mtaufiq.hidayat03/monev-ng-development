package models.cms.contracts;

import com.google.gson.reflect.TypeToken;
import constants.generated.R;
import models.cms.AksesContent;
import models.cms.ContentStatus;
import models.cms.profilekl.ContentProfileDataContract;
import models.common.contracts.CreationContract;
import models.common.contracts.PermissionContract;
import models.common.contracts.PublishingStatusContract;
import models.user.management.User;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import permission.Kldi;
import play.mvc.Router;
import repositories.cms.AksesContentRepository;
import repositories.searchcontent.PopularContentRepository;
import repositories.user.management.KldiRepository;
import tableau.library.TableauRepository;
import tableau.library.models.TableauLibrary;
import utils.cms.TableauUtils;
import utils.cms.UploadedFile;
import utils.common.JsonUtil;
import utils.common.LogUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public interface ContentContract extends ContentPrimaryContract,
        CreationContract,
        PermissionContract,
        PublishingStatusContract,
        ContentProfileDataContract,
        ContentTypeContract
{

    String TAG = "ContentContract";

    String getKl();
    Long getTableauLibraryId();
    String getUrl();
    void setKldiName(String name);
    String getFreeContentFile();
    String getMouFile();
    String getRegulationFile();
    String getContent();
    String getFeaturedImage();
    void setAccessContents(List<AksesContent> accessContents);

    TableauLibrary getLibrary();
    void setLibrary(TableauLibrary model);
    void setPublishingStatuses(String[] status);
    void setViewer(Long value);
    List<AksesContent> getAccessContents();

    default boolean klExists() {
        return getContentId() != null && getKl() != null;
    }

    default boolean containKl(String kldi_id) {
        return klExists() && getKl().equals(kldi_id);
    }

    default String getFormattedCreatedAt() {
        return new SimpleDateFormat(STANDARD_TIME_READABLE_FORMAT).format(getCreatedAt());
    }

    default void withLibrary() {
        if (!isTableau()) {
            LogUtil.debug(TAG, "Oops.. it's not tableau visualization, not allow to get tableau library");
            return;
        }
        if (getTableauLibraryId() == null) {
            LogUtil.debug(TAG, "Oops.. tableau library is null");
            return;
        }
        final TableauLibrary model = TableauRepository.getInstance().getById(getTableauLibraryId());
        if (model == null) {
            LogUtil.debug(TAG, "Oops.. tableau library not found");
            return;
        }
        model.withParams();
        setLibrary(model);
    }

    default boolean isItMyLibrary(Long id) {
        return getTableauLibraryId() != null && getTableauLibraryId().equals(id);
    }

    default TableauUtils.TableauUrl getTableauUrl() {
        if (getLibrary() == null) {
            withLibrary();
        }
        final String libUrl = getLibrary() == null ? "" :  getLibrary().getUrl();
        Kldi kldi = KldiRepository.getInstance().getById(getKldi());
        String kldiName = kldi != null ? kldi.nama_instansi : "";
        return TableauUtils.tokenizedFrameUrl(libUrl, getUrl(), kldiName);
    }

    default String getViewUrl() {
        Map<String, Object> map = new HashedMap<>();
        map.put("id", getContentId());
        return Router.reverse(R.route.cms_publiccontentcontroller_view, map).url;
    }

    default void generatePublishingStatus() {
        List<String> results = new ArrayList<>();
        ContentStatus status;
        if ((status = ContentStatus.fromId(getStatus())) != null) {
            results.add(status.name);
        }
        if (getCreatedBy() != null) {
            results.add(getCreatedBy().toString());
        }
        String[] arr = new String[results.size()];
        setPublishingStatuses(results.toArray(arr));
    }

    default void generateModelKldiName() {
        if (StringUtils.isEmpty(getKldi())) {
            LogUtil.debug(TAG, "Oops kldiId is empty");
            return;
        }
        Kldi kldi = KldiRepository.getInstance().getById(getKldi());
        if (kldi == null) {
            LogUtil.debug(TAG, "Oops kldi is empty");
            return;
        }
        setKldiName(kldi.nama_instansi);
    }

    default String cleanContentFromTags(String value) {
        if (StringUtils.isEmpty(value)) {
            return value;
        }
        return value.replaceAll("\\<.*?\\>", "")
                .replaceAll("&nbsp;", " ");
    }

    default String getAbbreviate(String value) {
        if (StringUtils.isEmpty(value)) {
            return "";
        }
        return StringUtils.abbreviate(value, 100);
    }

    default String cleanAndAbbreviateContent() {
        return getAbbreviate(cleanContentFromTags(getContent()));
    }

    default List<AksesContent> getUserAccess() {
        return new AksesContentRepository().getListAksesContent(getContentId());
    }

    default void generateModelPermission() {
        List<AksesContent> results = getUserAccess();
        List<String> permissions = results.stream()
                .map(e -> String.valueOf(e.getUserId()))
                .collect(Collectors.toList());
        permissions.add(getPermission());
        String[] arr = new String[permissions.size()];
        setPermissions(permissions.toArray(arr));
    }

    default boolean isExtraFileExist() {
        return !StringUtils.isEmpty(getFreeContentFile());
    }

    default List<UploadedFile> getArticleExtras() {
        LogUtil.debug(TAG, "get article extras");
        return turnJsonFilesToObject(getFreeContentFile());
    }

    default List<UploadedFile> getMouFiles() {
        LogUtil.debug(TAG, "get mou file as object");
        return turnJsonFilesToObject(getMouFile());
    }

    default List<UploadedFile> getRegulationFiles() {
        LogUtil.debug(TAG, "get regulation file as object");
        return turnJsonFilesToObject(getRegulationFile());
    }

    default List<UploadedFile> turnJsonFilesToObject(String files) {
        LogUtil.debug(TAG, "generate files from: %s", files);
        if (StringUtils.isEmpty(files)) {
            LogUtil.warn(TAG, "ït seems files empty");
            return new ArrayList<>();
        }
        try {
            return JsonUtil.fromJson(
                    files,
                    new TypeToken<List<UploadedFile>>() {
                    }.getType()
            );
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }
        return new ArrayList<>();
    }

    default void generateTotalViewer() {
        LogUtil.debug(TAG, "inject new value to viewer");
        if (getContentId() == null || !isGeneralArticle() || !isPublish()) {
            LogUtil.debug(TAG, "Oops.. id is null " +
                    "or not general article " +
                    "or not published .. exit");
            return;
        }
        setViewer(getTotalViewer());
    }

    default Long getTotalViewer() {
        final String id = getContentId().toString();
        List<PopularContentRepository.PopularContent> results = PopularContentRepository.getInstance()
                .getPopularContents(id);
        if (results.isEmpty()) {
            setViewer(0L);
        }
        LogUtil.debug(TAG, "search for popular item");
        Optional<PopularContentRepository.PopularContent> popularContent = results.stream()
                .filter(p -> p.contentId.equalsIgnoreCase(id)).findFirst();
        if (!popularContent.isPresent()) {
            return 0L;
        }
        return popularContent.get().getViewerIn();
    }

    default String getFeaturedImageUrl() {
        if (StringUtils.isEmpty(getFeaturedImage())) {
            return "";
        }
        UploadedFile featured = getFeaturedImageFile();
        if (featured != null) {
            return featured.getUrl();
        }
        final String filename = FilenameUtils.getName(getFeaturedImage());
        Map<String, Object> map = new HashedMap<>();
        map.put("path", filename);
        return Router.reverse(R.route.cms_filecontroller_getimage, map).url;
    }

    default UploadedFile getFeaturedImageFile() {
        LogUtil.debug(TAG, "get featured image from string json: %s", getFeaturedImage());
        if (StringUtils.isEmpty(getFeaturedImage())) {
            LogUtil.debug(TAG, "Oops featured_image seems to be empty");
            return null;
        }
        if (!(getFeaturedImage().contains("{")
                || getFeaturedImage().contains("}"))) {
            LogUtil.debug(TAG, "featured image does not seem to be valid json");
            return null;
        }
        try {
            LogUtil.debug(TAG, "convert json to object");
            UploadedFile uploadedFile = JsonUtil.fromJson(
                    getFeaturedImage(),
                    UploadedFile.class);
            if (uploadedFile != null) {
                LogUtil.debug(TAG, "return uploaded file object for featuredImage");
                return uploadedFile;
            }
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }
        return null;
    }

    default String getProfileCreatedInformation() {
        String createdBy = "";
        if (getCreatedBy() != null) {
            User user;
            Kldi kldi;
            if ((user = User.findById(getCreatedBy())) != null
                    && (kldi = user.getKldiData()) != null) {
                createdBy = kldi.nama_instansi +", ";
            }
        }
        return createdBy  + getCreatedAsDateOnly();
    }

}
