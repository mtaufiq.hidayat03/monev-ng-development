package models.cms.contracts;

import static models.cms.ContentType.*;
import static models.cms.ContentType.ARTIKEL_UMUM;

/**
 * @author HanusaCloud on 12/16/2019 4:39 PM
 */
public interface ContentTypeContract {

    String getContentType();

    default boolean isProcurementProfile() {
        return getContentType().equals(PROCUREMENT_PROFILE);
    }

    default boolean isProfileKl() {
        return getContentType().equals(PROFILE_KLPD);
    }

    default boolean isBlacklist() {
        return getContentType().equals(BLACKLIST);
    }

    default boolean isTableau() {
        return getContentType().contains(VISUALISASI_TABLEAU);
    }

    default boolean isGeneralArticle() {
        return getContentType().equalsIgnoreCase(ARTIKEL_UMUM);
    }

    default String getTypeAsLabel() {
        if (isTableau()) {
            return "VISUALISASI";
        }
        return getContentType();
    }

}
