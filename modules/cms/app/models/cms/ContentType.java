package models.cms;

public class ContentType {

    public static final String PROFILE_KLPD = "PROFILE K/L/P/D";
    public static final String ARTIKEL_UMUM = "ARTIKEL UMUM";
    public static final String VISUALISASI_TABLEAU = "VISUALISASI TABLEAU";
    public static final String CHART = "CHART";
    public static final String BLACKLIST = "BLACKLIST";
    public static final String PROCUREMENT_PROFILE = "PROCUREMENT PROFILE";

}
