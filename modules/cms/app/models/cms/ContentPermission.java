package models.cms;

import java.util.LinkedHashMap;
import java.util.Map;

public class ContentPermission {

    public static final String PUBLIC = "PUBLIC";
    public static final String INTERNAL ="INTERNAL";
    public static final String PRIVATE = "PRIVATE";

    public static final Map<String, String> VALUES;

    static {
        VALUES = new LinkedHashMap<>();
        VALUES.put(PUBLIC, PUBLIC + " (Bisa dilihat siapapun)");
        VALUES.put(INTERNAL, INTERNAL + " (Hanya bisa dilihat oleh user memiliki akun/yang log-in)");
        VALUES.put(PRIVATE, PRIVATE + " (Hanya bisa dilihat oleh user yang dikasi akses secara spesifik)");
    }

}
