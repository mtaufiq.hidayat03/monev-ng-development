package models.cms;

import models.user.management.User;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import repositories.user.management.UserRepository;

import java.io.Serializable;

@Table(name = "akses_content", schema = "public")
public class AksesContent extends BaseTable implements Serializable {

    @Id
    public Long id;
    public Long user_id;
    public Long content_id;

    public AksesContent() {}

    public AksesContent(Long userId, Long contentId){
        this.user_id = userId;
        this.content_id = contentId;
    }

    public static AksesContent generateBy(Long userId, Long contentId) {
        return new AksesContent(userId, contentId);
    }

    public Long getUserId() {
        return user_id;
    }

    public User getUser() {
        return UserRepository.getInstance().getUser(user_id);
    }

    public boolean allowToSave() {
        return user_id != null
                && user_id > 0
                && content_id != null
                && content_id > 0;
    }

    @Override
    protected void prePersist() {
        if (this.id == null) {
            id = Query.find("SELECT nextval('akses_content_id_seq'::regclass)", Long.class).first();
        }
    }
}
