package models.cms;

import io.netty.handler.codec.http.QueryStringDecoder;
import models.cms.contracts.ContentTypeContract;
import models.common.contracts.FormContract;
import models.common.contracts.KldiContract;
import models.common.contracts.PermissionContract;
import models.common.contracts.PublishingStatusContract;
import org.apache.http.client.utils.URIBuilder;
import play.data.validation.Required;
import tableau.library.TableauRepository;
import tableau.library.models.TableauLibrary;
import utils.common.JsonUtil;
import utils.common.LogUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class VisualisasiTableauForm implements FormContract,
        PermissionContract,
        KldiContract,
        PublishingStatusContract,
        ContentTypeContract
{

    public static final String TAG = "VisualisasiTableauForm";

    public String action = CREATE;
    public Long id;
    @Required
    public String title;

    public String alamatUrl;
    @Required
    public String content;
    @Required
    public Integer publish;
    public String kldiId;
    public String permission = ContentPermission.PUBLIC.toLowerCase();

    public Long libraryId;
    public String[] paramKeys;
    public String[] paramValues;
    public Map<String, String> params;

    public VisualisasiTableauForm() {}

    public VisualisasiTableauForm(Content content) {
        id = content.id;
        title = content.title;
        alamatUrl = content.alamat_url;
        this.content = content.content;
        publish = content.publish;
        kldiId = content.kldi_id;
        permission = content.permission;
        libraryId = content.tableau_library_id;
        this.params = extractQueryString();
        this.action = id != null ? UPDATE : CREATE;
    }

    @Override
    public String getAction() { return action; }

    public String getQueryString() {
        LogUtil.debug(TAG, "get query string");
        if (libraryId == null) {
            LogUtil.debug(TAG, "Oops.. library id is null");
            return "";
        }
        TableauLibrary model = TableauRepository.getInstance().getById(libraryId);
        return generateQueryString(model);
    }

    public String generateQueryString(TableauLibrary model) {
        LogUtil.debug(TAG, "trying to generate query string");
        if (model == null) {
            LogUtil.debug(TAG, "Oops.. it seems tableau library is null, exit");
            return "";
        }
        if (paramKeys == null
                || paramValues == null
                || paramKeys.length != paramValues.length) {
            LogUtil.debug(TAG, "Oops.. it seems there's null in params or not the same length");
            return "";
        }
        URIBuilder uriBuilder = new URIBuilder();
        for (int i = 0; i < paramKeys.length; i++) {
            final String key = paramKeys[i];
            final String value = paramValues[i];
            uriBuilder.addParameter(key, value);
        }
        try {
            return uriBuilder.build().toString();
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            return "";
        }
    }

    public Map<String, String> extractQueryString() {
        Map<String, String> results = new HashMap<>();
        if (alamatUrl == null || alamatUrl.isEmpty()) {
            return results;
        }
        Map<String, List<String>> params = new QueryStringDecoder(alamatUrl).parameters();
        for (Map.Entry<String, List<String>> entry : params.entrySet()) {
            results.put(entry.getKey(), String.join(",", entry.getValue()));
        }
        return  results;
    }

    @Override
    public Integer getStatus() {
        return publish;
    }

    public boolean containStatus(Integer value) {
        return publish != null && publish.equals(value);
    }

    public boolean isItMyLibrary(Long id) {
        return libraryId != null && libraryId.equals(id);
    }

    public String getJsonParams() {
        return params != null ? JsonUtil.toJson(params) : "{}";
    }

    @Override
    public String getPermission() {
        return permission;
    }

    @Override
    public String getKldi() {
        return kldiId;
    }

    @Override
    public String getContentType() {
        return ContentType.VISUALISASI_TABLEAU;
    }
}
