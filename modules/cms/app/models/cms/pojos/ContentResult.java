package models.cms.pojos;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import constants.generated.R;
import controllers.common.BaseController;
import models.cms.Content;
import models.cms.ContentStatus;
import models.common.contracts.DataTablePagination;
import models.user.management.User;
import models.user.management.contracts.UserContract;
import play.mvc.Router;
import repositories.user.management.UserRepository;
import utils.common.LogUtil;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static models.cms.ContentStatus.STOCK_REVIEW;

public class ContentResult extends BaseController implements DataTablePagination.PageAble<Content> {

    private List<Content> items;
    private final DataTablePagination.Request request;
    private int total = 0;
    private int totalItemInOnePage = 0;
    private final String[] columns = new String[]{
            "id", "title", "created_by", "type", "created_at", "permission"
    };

    private final List<String> searchAbleFields;
    private static final UserRepository repo = new UserRepository();

    public ContentResult(DataTablePagination.Request request) {
        this.request = request;
        List<String> fields = new ArrayList<>();
        fields.add("title::TEXT");
        searchAbleFields = fields;
    }

    @Override
    public JsonObject getResultsAsJson() {
        JsonObject result = getResponseTemplate();
        JsonArray jsonArray = new JsonArray();
        if (getTotal() == 0) {
            result.add("data", jsonArray);
            return result;
        }
        int i = getNumberingStartFrom();
        for (Content content : getResults()) {
            UserRepository repo = new UserRepository();
            User model = repo.getCreator(content.created_by);
            JsonArray contentArray = new JsonArray();
            Map<String, Object> params = new HashMap<>();
            params.put("id", content.id);
            UserContract user = getCurrentUser().user;
            boolean isSuperAdmin = user.isSuperAdmin();
            String deleteUrl = Router.reverse(R.route.cms_contentcontroller_delete, params).url;
            String editUrl = Router.reverse(R.route.cms_contentcontroller_updateartikelumum, params).url;
            String editLabel = "Edit Artikel Umum";
            String previewUrl = Router.reverse(R.route.cms_contentcontroller_previewartikelumum, params).url;
            String previewLabel = "Preview Artikel Umum";
            String addAction = "<li><a class='update-content' href='" + editUrl + "'>" + editLabel + "</a></li>" +
                    "<li class=\"divider\"></li>" +
                    "<li><a class='preview-content' href='" + previewUrl + "'>" + previewLabel + "</a></li>";
            if (content.isProfileKl()) {
                editUrl = Router.reverse(R.route.cms_contentcontroller_updateprofilekl, params).url;
                editLabel = "Edit Profile K/L/P/D";
                previewUrl = Router.reverse(R.route.cms_contentcontroller_previewprofilekl, params).url;
                previewLabel = "Preview Profile K/L/P/D";
                addAction = "<li><a href='" + editUrl + "'>" + editLabel + "</a></li>" +
                        "<li class=\"divider\"></li>" +
                        "<li><a href='" + previewUrl + "'>" + previewLabel + "</a></li>";
            }
            if (content.isTableau()) {
                editUrl = Router.reverse(R.route.cms_contentcontroller_updatevisualisasitableau, params).url;
                editLabel = "Edit Visualisasi";
                previewUrl = Router.reverse(R.route.cms_contentcontroller_previewvisualisasitableau, params).url;
                previewLabel = "Preview Visualisasi";
                addAction = "<li><a href='" + editUrl + "'>" + editLabel + "</a></li>" +
                        "<li class=\"divider\"></li>" +
                        "<li><a href='" + previewUrl + "'>" + previewLabel + "</a></li>";
            }
            String deactivateLabel = "Atur Sebagai Draft";
            String deactivatedUrl = Router.reverse(R.route.cms_contentcontroller_draft, params).url;
            String stockLabel = "Atur Sebagai Stock Review";
            String stockUrl = Router.reverse(R.route.cms_contentcontroller_stockreview, params).url;
            String addActionStatus = "";
            if (!content.isProfileKl()) {
                if (!content.publish.equals(STOCK_REVIEW.getContentStatusId())) {
                    addActionStatus += "<li class=\"divider\"></li>" +
                            "<li><a href='" + stockUrl + "'>" + stockLabel + "</a></li>";
                }
            }
            if (!content.publish.equals(ContentStatus.DRAFT.getContentStatusId())) {
                addActionStatus += "<li class=\"divider\"></li>" +
                        "<li><a href='" + deactivatedUrl + "'>" + deactivateLabel + "</a></li>";
            }
            if (isSuperAdmin) {
                if (!content.publish.equals(ContentStatus.PUBLISH.getContentStatusId())) {
                    String activateLabel = "Atur Sebagai Publish";
                    String activatedUrl = Router.reverse(R.route.cms_contentcontroller_publish, params).url;
                    addActionStatus += "<li class=\"divider\"></li>" +
                            "<li><a href='" + activatedUrl + "'>" + activateLabel + "</a></li>";
                }
            }
            contentArray.add(i);
            contentArray.add(content.title);
            contentArray.add(model.name);
            contentArray.add(content.getTypeAsLabel());
            contentArray.add(new SimpleDateFormat("dd-MM-yyyy").format(content.created_at));
            contentArray.add(content.publish.equals(ContentStatus.PUBLISH.getContentStatusId())
                    ? "PUBLISH"
                    : content.publish.equals(ContentStatus.DRAFT.getContentStatusId())
                    ? "DRAFT"
                    : "STOCK REVIEW");
            contentArray.add("<div class='btn-group'>" +
                    "<button type='button' class='btn btn-success " +
                    "dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>" +
                    "Action <span class='sr-only'>Toggle Dropdown</span></button>" +
                    "<ul class='dropdown-menu' role='menu'>" +
                    addAction + addActionStatus +
                    "<li class=\"divider\"></li>" +
                    "<li><a href='" + deleteUrl + "' class='delete'>Delete</a></li>" +
                    "</ul></div>");
            jsonArray.add(contentArray);
            i++;
        }
        result.add("data", jsonArray);
        LogUtil.debug(TAG, result);
        return result;
    }

    @Override
    public List<String> extractSearchAbleFields(Field[] fields) {
        return searchAbleFields;
    }

    @Override
    public void setResult(List<Content> items) {
        this.items = items;
    }

    @Override
    public List<Content> getResults() {
        return items;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setTotalItemInOnePage(int totalItem) {
        totalItemInOnePage = totalItem;
    }

    @Override
    public int getTotalItemInOnePage() {
        return totalItemInOnePage;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public String[] getColumns() {
        return columns;
    }

    @Override
    public DataTablePagination.Request getRequest() {
        return request;
    }

    @Override
    public Class<Content> getReturnType() {
        return Content.class;
    }
}
