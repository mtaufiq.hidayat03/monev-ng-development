package models.cms;

import models.common.contracts.FormContract;
import models.common.contracts.PermissionContract;
import org.apache.commons.lang3.StringUtils;
import utils.common.IdUtils;
import utils.common.LogUtil;

import java.util.ArrayList;
import java.util.List;

public class AccessForm implements FormContract, PermissionContract {

    public static final String TAG = "AccessForm";
    public String action = CREATE;
    public Long id;
    public String akses;
    public String userSelected;

    @Override
    public String getAction() { return action; }

    public List<Long> getUserId() {
        LogUtil.debug(TAG, "get user id: " + userSelected);
        if (!isPrivate()) {
            LogUtil.debug(TAG, "Oops.. not a private, exits");
            return new ArrayList<>();
        }
        return IdUtils.getNumericalsFrom(userSelected);
    }

    public boolean userSeletedExist() {
        return !StringUtils.isEmpty(userSelected);
    }

    @Override
    public String getPermission() {
        return akses;
    }

    public boolean allowToStore() {
        return !StringUtils.isEmpty(akses)
                && id != null
                && !(isPrivate() && StringUtils.isEmpty(userSelected));
    }

}
