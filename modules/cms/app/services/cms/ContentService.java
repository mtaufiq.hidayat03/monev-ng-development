package services.cms;

import com.google.gson.JsonObject;
import constants.generated.R;
import models.cms.*;
import models.cms.profilekl.ProfileForm;
import models.common.ServiceResult;
import models.searchcontent.ElasticRequest;
import models.user.management.CurrentUser;
import models.user.management.contracts.UserContract;
import org.apache.commons.lang3.StringUtils;
import play.i18n.Messages;
import repositories.cms.AksesContentRepository;
import repositories.cms.ContentRepository;
import repositories.searchcontent.ArticleRepository;
import repositories.user.management.UserRepository;
import utils.common.DirectoryUtil;
import utils.common.LogUtil;

import java.util.Objects;
import java.util.stream.Collectors;

import static models.cms.ContentFolder.FOLDER_PICTURE;
import static models.cms.ContentPermission.PUBLIC;

public class ContentService {

    private static final String TAG = "ContentService";
    public ContentRepository repository = ContentRepository.getInstance();
    public AksesContentRepository aksesContentRepository = AksesContentRepository.getInstance();
    public UserRepository userRepository = UserRepository.getInstance();
    public static final ContentService instance = new ContentService();
    public ArticleRepository elasticRepo = new ArticleRepository();

    public static ContentService getInstance() {
        return instance;
    }

    public ServiceResult<JsonObject> storeGeneralArticle(ContentForm model, CurrentUser currentUser) {
        if (!model.allowToStore()) {
            return new ServiceResult<>(R.translation.cms_failed_form_not_completed);
        }

        try {
            Content content = repository.getNewContent();
            if (model.id != null) {
                content = repository.getContent(model.id);
                if (content == null) {
                    return new ServiceResult<>(R.translation.cms_content_not_exist, getFailedMessage());
                }
            }
            DirectoryUtil.createDir(FOLDER_PICTURE);
            LogUtil.debug(TAG, " file :" + model.uploadfile);
            model.uploadFeaturedImage();
            if (model.allowUpdateExtra()) {
                content.free_content = model.getJsonExtraFiles(
                        content.getArticleExtras()
                );
            }
            if (StringUtils.isEmpty(model.permission)) {
                model.permission = PUBLIC.toLowerCase();
            }
            final UserContract user = currentUser.user;
            if (StringUtils.isEmpty(model.kldiId) && !user.isSuperAdmin()) {
                LogUtil.debug(TAG, "Oops.. not a super admin set this: " + user.getKldiId() + " to content");
                model.kldiId = user.getKldiId();
            }
            LogUtil.debug(TAG, model);
            content.setContentForm(model);
            content.saveModel();
            if (!elasticRepo.storeToElastic(content)) {
                return new ServiceResult<>(R.translation.cms_save_failed);
            }
            return new ServiceResult<>(true, "success", getSuccessMessage(content));
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            return new ServiceResult<>(R.translation.cms_save_failed, getFailedMessage());
        }
    }

    public ServiceResult<Content> storeAccessContent(AccessForm accessForm) {
        if (!accessForm.allowToStore()) {
            LogUtil.debug(TAG, "it seems you're not allow to store access form");
            return new ServiceResult<>(R.translation.cms_failed_form_not_completed);
        }
        Content content = repository.getContent(accessForm.id);
        if (content == null) {
            LogUtil.debug(TAG, "Oops.. content not found");
            return new ServiceResult<>(R.translation.cms_content_not_exist);
        }
        try {
            aksesContentRepository.detachAksesContent(accessForm.id);
            if (accessForm.userSeletedExist() && accessForm.isPrivate()) {
                content.setAccessContents(accessForm.getUserId().stream()
                        .map(e -> userRepository.getUser(e))
                        .filter(Objects::nonNull)
                        .map(user -> AksesContent.generateBy(user.getUserId(), accessForm.id))
                        .filter(AksesContent::allowToSave)
                        .collect(Collectors.toList()));
            }
            content.permission = accessForm.akses.toLowerCase();
            content.saveModel();
            if (!elasticRepo.storeToElastic(content)) {
                LogUtil.debug(TAG, "send changes to elastic");
                return new ServiceResult<>(R.translation.cms_save_failed);
            }
            return new ServiceResult<>(true, R.translation.cms_save_success, content);
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            return new ServiceResult<>(R.translation.cms_save_failed);
        }
    }

    public ServiceResult<JsonObject> storeTableau(VisualisasiTableauForm model, CurrentUser currentUser) {
        try {
            Content content = new Content();
            if (model.id != null && model.id > 0) {
                content = repository.getContent(model.id);
            }
            if (StringUtils.isEmpty(model.permission)) {
                model.permission = ContentPermission.PUBLIC.toLowerCase();
            }
            final UserContract user = currentUser.user;
            if (StringUtils.isEmpty(model.kldiId) && !user.isSuperAdmin()) {
                LogUtil.debug(TAG, "Oops.. not a super admin set this: " + user.getKldiId() + " to content");
                model.kldiId = user.getKldiId();
            }
            content.setVisualisasiTableauForm(model);
            if (StringUtils.isEmpty(content.alamat_url)) {
                return new ServiceResult<>(R.translation.cms_failed_create_visualisasi_tableau);
            }
            content.saveModel();
            elasticRepo.storeToElastic(content);
            return new ServiceResult<>(true, "success", getSuccessMessage(content));
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            return new ServiceResult<>(R.translation.cms_save_failed, getFailedMessage());
        }
    }

    public ServiceResult<JsonObject> storeProfile(ProfileForm model, CurrentUser currentUser) {
        if (model.isCreatedMode() && (model.uploadfile == null
                || model.uploadMou == null
                || model.uploadRegulasi == null
                || model.uploadExtra == null)) {
            LogUtil.debug(TAG, "Oops.. missing files, exit!");
            return new ServiceResult<>(R.translation.cms_profile_missing_file);
        }
        Content content = new Content();
        if (model.isEditedMode() || model.id != null) {
            content = repository.getContent(model.id);
        }
        if (content == null) {
            return new ServiceResult<>(R.translation.cms_content_not_exist);
        }
        if (StringUtils.isEmpty(model.permission)) {
            model.permission = ContentPermission.PUBLIC.toLowerCase();
        }
        final UserContract user = currentUser.user;
        if (StringUtils.isEmpty(model.kldiId) && !user.isSuperAdmin()) {
            LogUtil.debug(TAG, "Oops.. not a super admin set this: " + user.getKldiId() + " to content");
            model.kldiId = user.getKldiId();
        }
        try {
            DirectoryUtil.createDir(ContentFolder.FOLDER_PICTURE);
            DirectoryUtil.createDir(ContentFolder.FOLDER_DOCUMENT);
            model.uploadFeaturedImage();
            if (model.allowUpdateExtra()) {
                model.free_content = model.getJsonExtraFiles(content.getArticleExtras());
            }
            if (model.allowUpdateMou()) {
                model.mou = model.getJsonMouFiles(content.getMouFiles());
            }
            if (model.allowUpdateRegulation()) {
                model.regulation = model.getJsonRegulationFiles(content.getRegulationFiles());
            }
            content.setProfileklForm(model);
            content.saveModel();
            if (model.isCreatedMode()) {
                content.attachProfileData();
            }
            elasticRepo.storeToElastic(content);
            return new ServiceResult<>(true, "success", getSuccessMessage(content));
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            return new ServiceResult<>(R.translation.cms_save_failed, getFailedMessage());
        }
    }

    public boolean changeStatus(Long id, ContentStatus status) {
        Content model = repository.getContent(id);
        if (model == null) {
            return false;
        }
        model.publish = status.getContentStatusId();
        model.saveModel();
        return elasticRepo.storeToElastic(model);
    }

    private JsonObject getSuccessMessage(Content content) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("status", true);
        jsonObject.addProperty("id", content.id);
        jsonObject.addProperty("kldi", content.kldi_id);
        jsonObject.addProperty("message", Messages.get(R.translation.cms_save_success));
        return jsonObject;
    }

    private JsonObject getFailedMessage() {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("status", false);
        jsonObject.addProperty("status", Messages.get(R.translation.cms_save_failed));
        return jsonObject;
    }

    public boolean deleteContent(Long contentID) {
        Content model = repository.getContent(contentID);
        if (model != null) {
            model.deleteModel();
            elasticRepo.deleteDocument(new ElasticRequest(model));
            ArticleRepository.getInstance().deleteLatestContentsCache();
            return true;
        }
        return false;
    }
}
