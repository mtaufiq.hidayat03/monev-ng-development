package repositories.cms;

import models.cms.Content;
import models.cms.ContentPermission;
import models.cms.ContentStatus;
import models.cms.ContentType;
import models.cms.pojos.ContentResult;
import models.common.contracts.DataTablePagination;
import models.user.management.CurrentUser;
import models.user.management.contracts.UserContract;
import org.apache.commons.lang3.StringUtils;
import play.db.jdbc.Query;
import utils.common.LogUtil;

import java.util.ArrayList;
import java.util.List;

public class ContentRepository {

    public static final String TAG = "ContentRepository";
    public static final ContentRepository instance = new ContentRepository();

    public static ContentRepository getInstance() {
        return instance;
    }

    public List<Content> getContents(boolean ownershipCheck, UserContract user) {
        String queryKldi = "";
        if (ownershipCheck && user != null && !user.isSuperAdmin()) {
            queryKldi = " AND kldi_id = '" + user.getKldiId() + "'";
        }
        return Content.find("deleted_at is NULL" + queryKldi).fetch();
    }

    public List<Content> getContents() {
        return Content.find("deleted_at is NULL").fetch();
    }

    public List<Content> getContents(Long offset, Integer limit) {
        LogUtil.debug(TAG, "offset: " + offset + " limit: " + limit);
        return Content.find("deleted_at is NULL ORDER BY id ASC LIMIT ? OFFSET ?", limit, offset).fetch();
    }

    public Long count() {
        return Content.count("deleted_at is NULL");
    }

    public List<Content> getContents(
            boolean ownershipCheck,
            UserContract user,
            String search,
            Integer start,
            Integer length
    ) {
        List params = new ArrayList();
        String queryKldi = "";
        if (ownershipCheck && user != null && !user.isSuperAdmin()) {
            params.add(user.getKldiId());
            queryKldi = " AND kldi_id = ?";
        }
        String querySearch = "";
        if (!StringUtils.isEmpty(search)) {
            params.add("%" + search + "%");
            querySearch = " AND title ilike ?";
        }
        return Content.find(
                "deleted_at is NULL" + queryKldi + querySearch + " LIMIT " + length + " OFFSET " + start,
                params.toArray()
        ).fetch();
    }

    public Long getAllFilteringRecords(boolean ownershipCheck, UserContract user, String search, Integer length) {
        List params = new ArrayList();
        LogUtil.debug(TAG, user);
        LogUtil.debug(TAG, "search : " + search);
        LogUtil.debug(TAG, "length : " + length);
        String queryKldi = "";
        if (ownershipCheck && user != null && !user.isSuperAdmin()) {
            queryKldi = " AND kldi_id = ?";
            params.add(user.getKldiId());
        }
        String querySearch = "";
        if (!StringUtils.isEmpty(search)) {
            params.add("%" + search + "%");
            querySearch = " AND title ilike ?";
        }
        return Query.count("SELECT count(*) " +
                "FROM contents " +
                "WHERE deleted_at IS NULL" + queryKldi + querySearch + " LIMIT " + length, params.toArray());
    }

    public Integer getAllRecords(boolean ownershipCheck, UserContract user, String search) {
        String queryKldi = "";
        String searchQuery = "";
        if (ownershipCheck && user != null && !user.isSuperAdmin()) {
            queryKldi = " AND kldi_id = '" + user.getKldiId() + "'";
        }
        if (!search.equals("")) {
            searchQuery = " AND (title like '%" + search + "%')";
        }
        return (int) Query.count("SELECT count(*) FROM contents WHERE deleted_at IS NULL" + queryKldi + searchQuery);
    }

    public List<Content> getListContent() {
        return Query.find("SELECT * FROM contents WHERE deleted_at IS NULL", Content.class).fetch();
    }

    public List<Content> getListArtikel(Integer offset, Integer limit, CurrentUser currentUser) {
        String type = ContentType.ARTIKEL_UMUM;
        String permissionPublic = ContentPermission.PUBLIC.toLowerCase();
        String permissionInternal = ContentPermission.INTERNAL.toLowerCase();
        String permissionPrivate = ContentPermission.PRIVATE.toLowerCase();
        Integer status = ContentStatus.PUBLISH.getContentStatusId();
        String sqlArtikel = "";
        if (currentUser == null) {
            sqlArtikel = "SELECT content, featured_image, title, id, created_at " +
                    "FROM contents " +
                    "WHERE type='" + type + "' " +
                    "and publish ='" + status + "' " +
                    "and permission='" + permissionPublic + "' " +
                    "and deleted_at IS NULL " +
                    "order by created_at DESC LIMIT " + limit + " OFFSET " + offset;
        }
        if (currentUser != null) {
            String kldiId = currentUser.user.getKldiId();
            Integer userId = (int) (long) currentUser.user.getUserId();
            if (currentUser.user.isRegiseredUser()) {
                sqlArtikel = "SELECT a.content, a.featured_image, a.title, a.id, a.created_at " +
                        "FROM contents AS a " +
                        "LEFT JOIN akses_content AS b ON a.id=b.content_id " +
                        "WHERE a.type='" + type + "' " +
                        "and a.publish ='" + status + "' " +
                        "and (a.permission='" + permissionPublic + "' " +
                        "or (a.permission='" + permissionPrivate + "' and b.user_id='" + userId + "')) " +
                        "and a.deleted_at IS NULL " +
                        "order by a.created_at DESC " +
                        "LIMIT " + limit + " OFFSET " + offset;
            }
            if (currentUser.user.isAdminKonten() || currentUser.user.isAdminKl()) {
                sqlArtikel = "SELECT a.content, a.featured_image, a.title, a.id, a.created_at " +
                        "FROM contents AS a " +
                        "LEFT JOIN akses_content AS b ON a.id=b.content_id " +
                        "WHERE a.type='" + type + "' " +
                        "and a.publish ='" + status + "' " +
                        "and (a.permission='" + permissionPublic + "' " +
                        "or (a.permission='" + permissionInternal + "' " +
                        "and a.kldi_id='" + kldiId + "') " +
                        "or (a.permission='" + permissionPrivate + "' " +
                        "and b.user_id='" + userId + "')) " +
                        "and a.deleted_at IS NULL " +
                        "order by a.created_at DESC " +
                        "LIMIT " + limit + " OFFSET " + offset;
            }
            if (currentUser.user.isSuperAdmin()) {
                sqlArtikel = "SELECT content, featured_image, title, id, created_at " +
                        "FROM contents " +
                        "WHERE type='" + type + "' " +
                        "and publish ='" + status + "' " +
                        "and deleted_at IS NULL " +
                        "order by created_at DESC " +
                        "LIMIT " + limit + " OFFSET " + offset;
            }
        }
        return Query.find(sqlArtikel, Content.class).fetch();
    }

    public Content getDetailContent(Long id, CurrentUser currentUser) {
        String permissionPublic = ContentPermission.PUBLIC.toLowerCase();
        String permissionInternal = ContentPermission.INTERNAL.toLowerCase();
        String permissionPrivate = ContentPermission.PRIVATE.toLowerCase();
        String sqlArtikel = "";
        if (currentUser == null) {
            sqlArtikel = "SELECT content, featured_image, free_content, title, id, created_at " +
                    "FROM contents " +
                    "WHERE id='" + id + "' " +
                    "and permission='" + permissionPublic + "' " +
                    "and deleted_at IS NULL";
        }
        if (currentUser != null) {
            String kldiId = currentUser.user.getKldiId();
            Integer userId = (int) (long) currentUser.user.getUserId();
            if (currentUser.user.isRegiseredUser()) {
                sqlArtikel = "SELECT a.content, a.featured_image, a.free_content, a.title, a.id, a.created_at " +
                        "FROM contents AS a " +
                        "LEFT JOIN akses_content AS b ON a.id=b.content_id " +
                        "WHERE a.id='" + id + "' " +
                        "and (a.permission='" + permissionPublic + "' " +
                        "or (a.permission='" + permissionPrivate + "' " +
                        "and b.user_id='" + userId + "')) and deleted_at IS NULL";
            }
            if (currentUser.user.isAdminKonten() || currentUser.user.isAdminKl()) {
                sqlArtikel = "SELECT a.content, a.featured_image, a.free_content, a.title, a.id, a.created_at " +
                        "FROM contents AS a " +
                        "LEFT JOIN akses_content AS b ON a.id=b.content_id " +
                        "WHERE a.id='" + id + "' " +
                        "and (a.permission='" + permissionPublic + "' " +
                        "or (a.permission='" + permissionInternal + "' " +
                        "and a.kldi_id='" + kldiId + "') " +
                        "or (a.permission='" + permissionPrivate + "' " +
                        "and b.user_id='" + userId + "')) and deleted_at IS NULL";
            }
            if (currentUser.user.isSuperAdmin()) {
                sqlArtikel = "SELECT content, featured_image, free_content, title, id, created_at " +
                        "FROM contents " +
                        "WHERE id='" + id + "' " +
                        "and deleted_at IS NULL";
            }
        }
        return Query.find(sqlArtikel, Content.class).first();
    }

    public Content getContentByKldi(UserContract user) {
        return Content.find("kldi_id =? AND deleted_at IS NULL", user.getKldiId()).first();
    }

    //pencarian berdasarkan id
    public Content getContent(Long id) {
        LogUtil.debug(TAG, "get content by id: " + id);
        final Content model = Content.find("id =? AND deleted_at IS NULL", id).first();
        LogUtil.debug(TAG, "result exist: " + (model != null));
        return model;
    }

    public Content getNewContent() {
        return new Content();
    }

    public Content getContent(String title, String content) {
        //  LogUtil.debug(TAG, "get Content by title: " + title + " and content :" + content);
        return Content.find("title =? AND isi_content =? AND deleted_at IS NULL", title, content).first();
    }

    public long save(Content content) {
        //LogUtil.debug(TAG, "save Content using bind update");
        return Query.bindUpdate("INSERT INTO public.contents (title, isi_content) " +
                "values (:title, :isi_content)", content);
    }

    public void update(Content content) {
        // LogUtil.debug(TAG, "update Content using bind update");
        Query.bindUpdate("UPDATE public.Content set title = :title, " +
                "isi_content = :isi_content where id = :id", content);
    }

    public static Content getByContent(Long contentId) {
        return Content.find("id = ? ", contentId).first();
    }

    public long getTotalContentByLibrary(Long id) {
        return Content.count("tableau_library_id = ?", id);
    }

    public ContentResult getContentResult(CurrentUser currentUser, DataTablePagination.Request request) {
        Boolean isAdminKl = currentUser.user.isAdminKl();
        Boolean isSuperAdmin = currentUser.user.isSuperAdmin();
        Boolean isAdminKonten = currentUser.user.isAdminKonten();
        ContentResult contentResult = new ContentResult(request);
        if (isAdminKl || isAdminKonten) {
            String query = "SELECT * FROM public.contents where kldi_id = '" + currentUser.user.getKldiId() + "'";
            contentResult.modifiedQuery(query);
        }
        if (isSuperAdmin) {
            contentResult.executeQuery();
        }
        return contentResult;
    }

}
