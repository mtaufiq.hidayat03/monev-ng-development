package repositories.cms;


import models.cms.AksesContent;
import play.db.jdbc.Query;
import utils.common.LogUtil;

import java.util.List;

public class AksesContentRepository {

    public static final String TAG = "AksesContentRepository";

    private static final AksesContentRepository respository = new AksesContentRepository();

    public static AksesContentRepository getInstance() {
        return respository;
    }

    public List<AksesContent> getListAksesContent(Long idContent) {
        return Query.find("SELECT * FROM akses_content WHERE content_id =?",
                AksesContent.class,
                idContent).fetch();
    }

    public int detachAksesContent(Long idContent) {
        LogUtil.debug(TAG, "detach akses content By content id: " + idContent);
        return Query.update("DELETE FROM akses_content where content_id =?", idContent);
    }

}
