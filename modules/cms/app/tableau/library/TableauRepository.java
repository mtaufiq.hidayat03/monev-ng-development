package tableau.library;

import models.user.management.contracts.UserContract;
import tableau.library.models.TableauLibrary;
import utils.common.LogUtil;

import java.util.List;

/**
 * @author HanusaCloud on 11/6/2019 8:34 AM
 */
public class TableauRepository {

    public static final String TAG = "TableauRepository";
    private static TableauRepository repository = new TableauRepository();

    public static TableauRepository getInstance() {
        return repository;
    }

    public TableauLibrary getById(Long id) {
        LogUtil.debug(TAG, "get library by id: " + id);
        TableauLibrary model = TableauLibrary.find("id = ? AND deleted_at IS NULL", id).first();
        LogUtil.debug(TAG, "is library by id exists: " + (model != null));
        return model;
    }

    public List<TableauLibrary> getAll() {
        LogUtil.debug(TAG, "get libraries");
        return TableauLibrary.find("deleted_at IS NULL").fetch();
    }

    public List<TableauLibrary> getAllForKLPD(UserContract user) {
        LogUtil.debug(TAG, "get all libaries for klpd");
        StringBuilder sb = new StringBuilder();
        sb.append("deleted_at IS NULL");
        if (!user.isSuperAdmin()) {
            sb.append(" AND available_for_klpd = true ");
        }
        final List<TableauLibrary> results = TableauLibrary.find(sb.toString()).fetch();
        LogUtil.debug(TAG, "total library: " + results.size());
        return results;
    }

}
