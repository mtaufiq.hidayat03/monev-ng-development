package tableau.library.models;

import constants.generated.R;
import models.common.ServiceResult;
import models.common.contracts.CreationContract;
import models.user.management.User;
import play.mvc.Router;
import repositories.cms.ContentRepository;
import repositories.user.management.UserRepository;
import tableau.library.TableauParamRepository;
import utils.common.LogUtil;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author HanusaCloud on 10/29/2019 2:43 PM
 */
public interface TableauContract extends CreationContract {

    String TAG = "TableauContract";

    Long getId();
    void deleteThisModel();
    Long getCreatedBy();
    void setParams(List<TableauLibraryParams> params);
    List<TableauLibraryParams> getParams();
    String getUrl();
    String getDescription();

    default Map<String, Object> getMapArgsUrl() {
        Map<String, Object> map = new HashMap<>();
        map.put("id", getId());
        return map;
    }

    default String getUpdateUrl() {
        return Router.reverse(R.route.cms_tableau_tableaucontroller_update, getMapArgsUrl()).url;
    }

    default String getDeleteUrl() {
        return Router.reverse(R.route.cms_tableau_tableaucontroller_delete, getMapArgsUrl()).url;
    }

    default String getCreator() {
        if (getCreatedBy() == null) {
            return "";
        }
        final User user = new UserRepository().getUser(getCreatedBy());
        return user != null ? user.name : "Admin";
    }

    default Long getTotalChildren() {
        return ContentRepository.getInstance().getTotalContentByLibrary(getId());
    }

    static List<TableauLibrary> getLibraries() {
        return TableauLibrary.find("deleted_at IS NULL").fetch();
    }

    default ServiceResult deleteLibrary() {
        if (getTotalChildren() > 0) {
            LogUtil.debug(TAG, "Oops.. it's either items is empty or not null, " +
                    "or children have relations to contents");
            return new ServiceResult(R.translation.cms_delete_tableau_relation_failed);
        }
        LogUtil.debug(TAG, "library is deleted");
        deleteThisModel();
        TableauParamRepository.getInstance().deleteByLibraryId(getId());
        return new ServiceResult(true, R.translation.cms_delete_tableau_success);
    }

    default void withParams() {
        List<TableauLibraryParams> params = TableauParamRepository.getInstance().getByLibrary(getId());
        setParams(params);
    }

    default String getShorterUrl() {
        if (getUrl() == null || getUrl().isEmpty()) {
            return "";
        }
        return getUrl().length() >= 100 ? getUrl().substring(0, 100) + "..." : getUrl();
    }

    default String getShorterDescription() {
        if (getDescription() == null || getDescription().isEmpty()) {
            return "";
        }
        return getDescription().length() >= 150 ? getDescription().substring(0, 150) + "..." : getDescription();
    }

}
