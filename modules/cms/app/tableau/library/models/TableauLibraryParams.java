package tableau.library.models;

import models.common.BaseModel;
import play.db.jdbc.Table;

/**
 * @author HanusaCloud on 11/4/2019 10:59 AM
 */
@Table(name = "tableau_library_parameters", schema = "public")
public class TableauLibraryParams extends BaseModel implements TableauParamContract {

    public static final String TAG = "TableauLibraryParams";

    public String label;
    public String value;
    public Long library_id;

    public String getLabel() {
        return label;
    }

    @Override
    public String getValue() {
        return value;
    }

}
