package tableau.library.models;

import org.apache.commons.lang3.StringUtils;

/**
 * @author HanusaCloud on 11/6/2019 8:42 AM
 */
public interface TableauParamContract {

    String getValue();

    default String[] getValues() {
        return getValue() != null && !getValue().isEmpty() ? getValue().split(",") : new String[]{};
    }

    default boolean isValueExist() {
        return StringUtils.isNotEmpty(getValue());
    }

}
