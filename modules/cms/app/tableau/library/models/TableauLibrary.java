package tableau.library.models;

import models.common.BaseModel;
import models.common.contracts.annotations.SearchAble;
import models.common.contracts.annotations.SoftDelete;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.sql.Timestamp;
import java.util.List;

/**
 * @author HanusaCloud on 10/29/2019 2:38 PM
 */
@Table(name = "tableau_library", schema = "public")
@SoftDelete
public class TableauLibrary extends BaseModel implements TableauContract {

    public static final String TAG = "TableauLibrary";

    @SearchAble
    public String title;
    @SearchAble
    public String description;
    public String kldi_id;
    @SearchAble
    public String url;
    public boolean available_for_klpd;

    @Transient
    public List<TableauLibraryParams> params;

    public TableauLibrary() {}

    public void setBy(TableauLibraryForm model) {
        title = model.title;
        description = model.description;
        available_for_klpd = model.availableForKldi;
        url = model.uri;
    }

    @Override
    public Long getId() {
        return this.id;
    }

    @Override
    public void deleteThisModel() {
        deleteModel();
    }

    @Override
    public Timestamp getCreatedAt() {
        return created_at;
    }

    @Override
    public void setParams(List<TableauLibraryParams> params) {
        this.params = params;
    }

    @Override
    public List<TableauLibraryParams> getParams() {
        return params;
    }

    @Override
    public Long getCreatedBy() {
        return created_by;
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public String getDescription() {
        return description;
    }

}
