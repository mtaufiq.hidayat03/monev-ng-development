package tableau.library.models;

import models.common.contracts.FormContract;
import play.data.validation.Required;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author HanusaCloud on 11/4/2019 10:45 AM
 */
public class TableauLibraryForm implements FormContract {

    public Long id;
    public String action = FormContract.CREATE;
    @Required
    public String uri;
    @Required
    public String title;
    public String description;
    public boolean availableForKldi = false;

    public Map<String, TableauLibraryParams> params;

    public TableauLibraryForm() {}

    public TableauLibraryForm(TableauLibrary model) {
        this.action = FormContract.UPDATE;
        this.id = model.id;
        this.uri = model.url;
        this.title = model.title;
        this.description = model.description;
        this.availableForKldi = model.available_for_klpd;
        this.params = paramToForm(model.getParams());
    }

    public boolean isParamsEmpty() {
        return params == null || params.isEmpty();
    }

    public boolean isValidUri() {
        return uri != null
                && !uri.isEmpty()
                && uri.contains("http");
    }

    private Map<String, TableauLibraryParams> paramToForm(List<TableauLibraryParams> params) {
        Map<String, TableauLibraryParams> map = new HashMap<>();
        if (params != null && !params.isEmpty()) {
            for (TableauLibraryParams param : params) {
                map.put("model.params." + UUID.randomUUID().toString() + "", param);
            }
        }
        return map;
    }

    @Override
    public String getAction() {
        return action;
    }

    public boolean isValidParams() {
        return params != null && params.values()
                .stream()
                .map(TableauLibraryParams::getLabel)
                .collect(Collectors.toSet()).size() == params.size();
    }

}
