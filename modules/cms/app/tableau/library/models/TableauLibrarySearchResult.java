package tableau.library.models;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import models.common.contracts.DataTablePagination;
import utils.common.LogUtil;

import java.util.List;

/**
 * @author HanusaCloud on 10/30/2019 8:26 AM
 */
public class TableauLibrarySearchResult implements DataTablePagination.PageAble<TableauLibrary> {

    public static final String TAG = "TableauLibrarySearchResult";

    private List<TableauLibrary> items;
    private final DataTablePagination.Request request;
    private int total = 0;
    private int totalItemInOnePage = 0;
    private final String[] columns = new String[]{
            "id", "title", "description", "url", "available_for_klpd", "created_at"
    };

    public TableauLibrarySearchResult(DataTablePagination.Request request) {
        this.request = request;
        executeQuery();
    }

    @Override
    public JsonObject getResultsAsJson() {
        JsonObject jsonObject = getResponseTemplate();
        JsonArray data = new JsonArray();
        if (getTotal() == 0) {
            jsonObject.add("data", data);
            return jsonObject;
        }
        jsonObject.add("data", data);
        int i = getNumberingStartFrom();
        for (TableauLibrary library : getResults()) {
            JsonArray jsonArray = new JsonArray();
            jsonArray.add(i);
            jsonArray.add(library.title);
            jsonArray.add(library.getShorterDescription());
            jsonArray.add(library.getUpdateUrl());
            jsonArray.add(library.getDeleteUrl());
            jsonArray.add(library.getCreatedAsDateOnly());
            jsonArray.add(library.getShorterUrl());
            jsonArray.add(library.available_for_klpd ? "Ya" : "Tidak" );
            jsonArray.add(library.getCreator());
            data.add(jsonArray);
            i++;
        }
        return jsonObject;
    }

    @Override
    public void setResult(List<TableauLibrary> items) {
        this.items = items;
    }

    @Override
    public List<TableauLibrary> getResults() {
        return this.items;
    }

    @Override
    public void setTotalItemInOnePage(int totalItem) {
        this.totalItemInOnePage = totalItem;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public int getTotalItemInOnePage() {
        return totalItemInOnePage;
    }

    @Override
    public int getTotal() {
        LogUtil.debug(TAG, "total items: " + total);
        return total;
    }

    @Override
    public DataTablePagination.Request getRequest() {
        return request;
    }

    @Override
    public Class<TableauLibrary> getReturnType() {
        return TableauLibrary.class;
    }

    @Override
    public String[] getColumns() {
        return columns;
    }
}
