package tableau.library;

import tableau.library.models.TableauLibraryParams;
import utils.common.LogUtil;

import java.util.List;

/**
 * @author HanusaCloud on 11/6/2019 8:38 AM
 */
public class TableauParamRepository {

    public static final String TAG = "TableauParamRepository";

    private static TableauParamRepository repository = new TableauParamRepository();

    public static TableauParamRepository getInstance() {
        return repository;
    }

    public void deleteByLibraryId(Long id) {
        LogUtil.debug(TAG, "delete params by library id: " + id);
        TableauLibraryParams.delete("library_id = ?", id);
    }

    public List<TableauLibraryParams> getByLibrary(Long id) {
        LogUtil.debug(TAG, "get all params by library id: " + id);
        final List<TableauLibraryParams> results = TableauLibraryParams.find("library_id = ?", id).fetch();
        LogUtil.debug(TAG, "total params: " + results);
        return results;
    }

}
