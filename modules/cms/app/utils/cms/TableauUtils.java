package utils.cms;

import io.netty.handler.codec.http.QueryStringDecoder;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.utils.URIBuilder;
import play.Play;
import play.libs.URLs;
import play.libs.WS;
import play.mvc.Http;
import utils.common.LogUtil;

import java.util.List;
import java.util.Map;

/**
 * @author HanusaCloud on 11/19/2019 12:59 PM
 */
public class TableauUtils {

    public static final String TAG = "TableauUtils";

    public static final String TABLEAU_HOST = Play.configuration.getProperty("tableau.host");
    private static final String TABLEAU_USER = Play.configuration.getProperty("tableau.user");
    private static final String TRUSTED = TABLEAU_HOST + "/trusted";

    public static String getToken() {
        try (WS.WSRequest request = WS.url(TRUSTED)) {
            String content = "";
            WS.HttpResponse response = request.setParameter("trusted_site","")
                    .setParameter("username", TABLEAU_USER)
                    .post();
            if (response.getStatus() == Http.StatusCode.OK) {
                content = response.getString();
                LogUtil.debug(TAG, "content: " + content);
            }
            if (content.equals("-1")) {
                return null;
            }
            return content;
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            return null;
        }
    }

    public static TableauUrl tokenizedFrameUrl(String url, String kldiName) {
        LogUtil.debug(TAG, "original: " + url);
        if (StringUtils.isEmpty(url)) {
            return null;
        }
        return reConstructTableauUrl(url, "?", kldiName, true, getToken(), TABLEAU_HOST);
    }

    public static TableauUrl tokenizedFrameUrl(String url, String queryString, String kldiName) {
        LogUtil.debug(TAG, "original: " + url);
        if (StringUtils.isEmpty(url)) {
            return null;
        }
        return reConstructTableauUrl(url, queryString, kldiName, true, getToken(), TABLEAU_HOST);
    }

    public static TableauUrl reConstructTableauUrl(
            String libUrl,
            String contentQueryString,
            String kldiName,
            boolean tokenized,
            String content,
            String domain) {
        try {
            URIBuilder builder = new URIBuilder()
                    .addParameter(":toolbar", "no");
            Map<String, List<String>> params = new QueryStringDecoder(contentQueryString).parameters();
            for (Map.Entry<String, List<String>> entry : params.entrySet()) {
                builder.addParameter(entry.getKey(), String.join(",", entry.getValue()));
            }
            final int questionMarkIndex = libUrl.indexOf("?");
            LogUtil.debug(TAG, "question mark index: " + questionMarkIndex);
            final String libQueryString = questionMarkIndex >= 0 ? libUrl.substring(questionMarkIndex) : "";
            Map<String, List<String>> queryStrings = new QueryStringDecoder(libQueryString).parameters();
            for (Map.Entry<String, List<String>> entry : queryStrings.entrySet()) {
                builder.addParameter(entry.getKey(), String.join(",", entry.getValue()));
            }
            if (!StringUtils.isEmpty(kldiName) && builder.getQueryParams().stream()
                    .noneMatch(e -> e.getName().equals("Nama Kldi"))) {
                LogUtil.debug(TAG, "inject kldiName: " + kldiName);
                builder.addParameter("Nama Kldi", kldiName);
            }
            LogUtil.debug(TAG, builder.getQueryParams());
            final String queryString = builder.build().toString();
            LogUtil.debug(TAG, "query string: " + queryString);
            final String result = libUrl.replace(libQueryString, "") + queryString;
            LogUtil.debug(TAG, "result url: " + result);
            String tokenizedUrl = "";
            if (tokenized) {
                LogUtil.debug(TAG, "return tokenized url");
                if (StringUtils.isEmpty(content)) {
                    return new TableauUrl(result, "");
                }
                final String parts = extractUrlSegmentWithoutHash(domain, libUrl);
                tokenizedUrl = domain + "/trusted" + "/" + URLs.encodePart(content) + parts + queryString;
                LogUtil.debug(TAG, "result: " + tokenizedUrl);
            }
            return new TableauUrl(result, tokenizedUrl);
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            return null;
        }
    }

    public static String extractUrlSegmentWithoutHash(String domain, String url) {
        final String hashes = url.replace(domain, "")
                .replaceAll("/#", "");
        LogUtil.debug(TAG, "hashes: " + hashes);
        final int hashIndex = hashes.indexOf("?");
        final String parts = hashIndex >= 0 ? hashes.substring(0, hashIndex) : hashes;
        LogUtil.debug(TAG, "parts: " + parts);
        return parts;
    }

    public static final class TableauUrl {

        public final String original;
        public final String tokenized;

        public TableauUrl(String original, String tokenized) {
            this.original = original;
            this.tokenized = tokenized;
        }

    }

}
