package utils.cms;

import constants.generated.R;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.io.FilenameUtils;
import play.libs.URLs;
import play.mvc.Router;

import java.io.File;
import java.util.Map;

/**
 * @author HanusaCloud on 1/10/2020 1:27 PM
 */
public class UploadedFile {

    public final String originalName;
    public final String path;

    public UploadedFile(String originalName, String path) {
        this.originalName = originalName;
        this.path = path;
    }

    public String getOriginalName() {
        return originalName;
    }

    public String getPath() {
        return path;
    }

    public boolean isImage() {
        return FileUtil.isImage(getPath());
    }

    public boolean isDocument() {
        return FileUtil.isDocument(getPath());
    }

    public String getFileExtension() {
        return FileUtil.getFileExtension(getPath());
    }

    public boolean fileExist() {
        return new File(getPath()).exists();
    }

    public String getNewFileName() {
        return FilenameUtils.getName(getPath());
    }

    public String getUrl() {
        Map<String, Object> map = new HashedMap<>();
        map.put("path", getNewFileName());
        map.put("label", URLs.encodePart(getOriginalName()));
        if (isImage()) {
            return Router.reverse(R.route.cms_filecontroller_getimage, map).url;
        }
        return Router.reverse(R.route.cms_filecontroller_getdocuments, map).url;
    }

}
