package utils.cms;

import models.cms.ContentFolder;
import org.apache.commons.io.FilenameUtils;
import utils.common.JsonUtil;
import utils.common.LogUtil;
import utils.common.TokenUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class FileUtil {

    public static final String TAG = "FileUtil";

    public static UploadedFile uploadImageFile(File file) {
        return uploadFile(file, ContentFolder.PATH_PICTURE);
    }

    private static final String[] SUPPORTED_DOCUMENTS = {"pdf", "doc", "docx"};
    private static final String[] SUPPORTED_IMAGES = {"jpg", "jpeg", "png"};

    public static final long DOCUMENT_MAX_FILE_SIZE = 5 * 1048576;
    public static final String DOCUMENT_MAX_LABEL = "5 MB";

    public static UploadedFile uploadFile(File file, String directory) {
        if (file == null || !file.exists()) {
            return null;
        }
        LogUtil.debug(TAG, "upload file: " + file.getName());
        final String extension = FilenameUtils.getExtension(file.getAbsolutePath());
        LogUtil.debug(TAG, "file's extension: " + extension);
        final String renameFile = TokenUtil.generateMd5Token(file.getName()) + "." + extension;
        LogUtil.debug(TAG, "new filename: " + renameFile);
        final File newFile = new File(directory + renameFile);
        if (file.renameTo(newFile)) {
            LogUtil.debug(TAG, "file renamed");
        }
        final UploadedFile result = new UploadedFile(
                file.getName(),
                newFile.getAbsolutePath()
        );
        LogUtil.debug(TAG, "uploaded object: %s", result);
        return result;
    }

    public static String uploadMultipleFile(List<File> files) {
        List<String> results = uploadMultipleFiles(files)
                .stream()
                .map(UploadedFile::getPath)
                .collect(Collectors.toList());
        return JsonUtil.toJson(results);
    }

    public static boolean validateDocFileSize(List<File> files) {
        return validFileSize(files, DOCUMENT_MAX_FILE_SIZE);
    }

    public static Boolean validFileSize(List<File> files, long size) {
        if (files == null || files.isEmpty()) {
            return false;
        }
        return files.stream().allMatch(e -> compareFileSize(e.length(), size));
    }

    public static Boolean compareFileSize(long fileSize, long rule) {
        return fileSize <= rule;
    }

    public static boolean isDocument(String path) {
        return check(path, SUPPORTED_DOCUMENTS);
    }

    public static boolean isImage(String path) {
        return check(path, SUPPORTED_IMAGES);
    }

    public static String getFileExtension(String path) {
        return FilenameUtils.getExtension(path);
    }

    private static boolean check(String path, String[] extensions) {
        final String extension = getFileExtension(path);
        for (String s : extensions) {
            if (extension.contains(s)) {
                return true;
            }
        }
        return false;
    }
    
    public static List<UploadedFile> uploadMultipleFiles(List<File> files) {
        LogUtil.debug(TAG, "upload multiple files");
        if (files == null || files.isEmpty()) {
            LogUtil.debug(TAG, "Oops.. files is null or empty");
            return new ArrayList<>();
        }
        return files.stream()
                .map(e -> uploadFile(e, ContentFolder.PATH_DOCUMENT))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public static void deleteFile(String fileName) {
        LogUtil.debug(TAG, "delete file " + fileName);
        File deleteOldFile = new File(fileName);
        if (deleteOldFile.exists() && deleteOldFile.delete()) {
            LogUtil.debug(TAG, "files's " + deleteOldFile.getName() + " deleted");
        }
    }

}
