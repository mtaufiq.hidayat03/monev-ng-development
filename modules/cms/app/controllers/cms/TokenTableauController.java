package controllers.cms;

import com.google.gson.JsonObject;
import controllers.common.ApiBaseController;
import org.apache.commons.lang3.StringUtils;
import permission.Can;
import permission.Kldi;
import repositories.user.management.KldiRepository;
import utils.cms.TableauUtils;
import utils.common.LogUtil;

import static permission.Access.*;

public class TokenTableauController extends ApiBaseController {

    public static final String TAG = "TokenTableauController";

    @Can({SUPER_ADMIN, ADMIN_KL, ADMIN_KONTEN})
    public static void getTokenTableau() throws Exception {
        JsonObject jsonObject = new JsonObject();
        final String url = params.get("url");
        final String kldiParam = params.get("kldi");
        LogUtil.debug(TAG, "kldi from param: " + kldiParam);
        jsonObject.addProperty("origin", url);
        final String kldiId = !StringUtils.isEmpty(kldiParam) ? kldiParam : getCurrentUser().user.getKldiId();
        Kldi kldi = KldiRepository.getInstance().getById(kldiId);
        final TableauUtils.TableauUrl result = TableauUtils.tokenizedFrameUrl(
                url,
                kldi != null ? kldi.nama_instansi : null
        );
        final boolean resultExist = result != null && !StringUtils.isEmpty(result.tokenized);
        jsonObject.addProperty("status", resultExist);
        if (resultExist) {
            jsonObject.addProperty("url", result.tokenized);
        }
        renderJSON(jsonObject);
    }


}
