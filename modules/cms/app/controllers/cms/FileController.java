package controllers.cms;

import constants.generated.R;
import controllers.common.BaseController;
import models.cms.ContentFolder;
import org.apache.commons.lang3.StringUtils;
import utils.common.LogUtil;

import java.io.File;

public class FileController extends BaseController {

    public static final String TAG = "FileController";

    public static void getFile(String path) throws Exception{
        File file = new File(ContentFolder.PATH_UPLOAD + path);
        if (file.exists() && !file.isDirectory()) {
            renderBinary(file);
        }
        renderTemplate(R.view.cms_filenotfound);
    }

    public static void getImage(String path) {
        File file = new File(ContentFolder.PATH_PICTURE + path);
        if(file.exists() && !file.isDirectory()) {
            renderBinary(file);
        }
        renderTemplate(R.view.cms_filenotfound);
    }

    public static void getDocuments(String path) {
        String label = params.get("label");
        if (StringUtils.isEmpty(label)) {
            label = path;
        }
        File file = new File(ContentFolder.PATH_DOCUMENT + path);
        LogUtil.debug(TAG, "label: " + label);
        if(file.exists() && !file.isDirectory()) {
            renderBinary(file, label);
        }
        renderTemplate(R.view.cms_filenotfound);
    }
}
