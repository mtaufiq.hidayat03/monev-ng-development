package controllers.cms;

import constants.generated.R;
import controllers.common.HtmlBaseController;
import models.cms.Content;
import models.cms.ContentType;
import models.searchcontent.SearchQueryParams;
import models.user.management.contracts.UserContract;
import org.apache.commons.lang3.StringUtils;
import play.cache.Cache;
import repositories.searchcontent.ArticleRepository;
import repositories.searchcontent.PopularContentRepository;
import utils.common.LogUtil;

/**
 * @author HanusaCloud on 11/26/2019 8:57 AM
 */
public class PublicContentController extends HtmlBaseController {

    public static final String TAG = "PublicContentController";

    public static void list() {
        UserContract user = getCurrentUser() != null ? getCurrentUser().user : null;
        renderArgs.put("populars", ArticleRepository.getInstance()
                .populars(user));
        renderArgs.put("result", ArticleRepository.getInstance()
                .generalArticleSearch(new SearchQueryParams(params), user));
        renderTemplate(R.view.cms_public_artikel);
    }

    public static void view(Long id) {
        Content model = contentRepository.getContent(id);
        notFoundIfNull(model);
        LogUtil.debug(TAG, model);
        renderArgs.put("content", model);
        renderArgs.put("date", model.getFormattedCreatedAt());
        renderArgs.put("action", "preview");
        if (model.isTableau()) {
            renderTemplate(R.view.cms_tableau_content_view_visualisasi_tableau);
        } else if (model.isProfileKl()) {
            model.withProfileData();
            renderTemplate(R.view.cms_profile_kl_view_profile_kl);
        } else {
            UserContract user = getCurrentUser() != null ? getCurrentUser().user : null;
            renderArgs.put("populars", ArticleRepository.getInstance()
                    .populars(user));
            recordViewer(model);
            renderTemplate(R.view.cms_general_article_view_artikel_umum);
        }
    }

    private static boolean recordViewer(Content model) {
        final String cookie = getPlaySessionCookie();
        if (!StringUtils.isEmpty(cookie)) {
            final String userId = getUserId().toString();
            final String key = cookie + "-" + userId + "-" + model.id;
            if (Cache.get(key) != null) {
                return false;
            }
            Cache.set(key, "set", "2mn");
            PopularContentRepository.getInstance()
                    .popularIndexRequest(new PopularContentRepository.PopularContent(
                                    model.id.toString(),
                                    cookie,
                                    ContentType.ARTIKEL_UMUM,
                                    userId
                            )
                    );
            return true;
        }
        return false;
    }


}
