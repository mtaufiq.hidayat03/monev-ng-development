package controllers.cms;

import com.google.gson.JsonObject;
import constants.generated.R;
import controllers.common.ApiBaseController;
import models.cms.ContentFolder;
import models.cms.ContentForm;
import models.cms.VisualisasiTableauForm;
import models.cms.profilekl.ProfileForm;
import models.common.ServiceResult;
import models.common.contracts.DataTableRequest;
import permission.Can;
import utils.cms.FileUtil;
import utils.cms.UploadedFile;
import utils.common.DirectoryUtil;
import utils.common.LogUtil;

import java.io.File;

import static permission.Access.*;

public class ContentApiController extends ApiBaseController {

    public static final String TAG = "ContentApiController";

    @Can({ADMIN_KL, ADMIN_KONTEN, SUPER_ADMIN})
    public static void contentJson() {
        DataTableRequest dataTableRequest = new DataTableRequest(request);
        renderJSON(contentRepository.getContentResult(getCurrentUser(), dataTableRequest).getResultsAsJson());
    }

    @Can({ADMIN_KL, SUPER_ADMIN, ADMIN_KONTEN})
    public static void storeArtikelUmum(ContentForm model) throws Exception {
        LogUtil.debug(TAG, model);
        validation.valid(model);
        if (validation.hasErrors()) {
            params.flash();
            LogUtil.debug(TAG, validation.errorsMap());
            renderJSON(new ServiceResult(R.translation.cms_failed_form_not_completed));
        }
        final ServiceResult<JsonObject> result = contentService.storeGeneralArticle(model, getCurrentUser());
        renderJSON(result.payload);
    }

    @Can({ADMIN_KL, SUPER_ADMIN, ADMIN_KONTEN})
    public static void storePicture(File image) throws Exception {
        LogUtil.debug(TAG, " file :" + image);
        try {
            DirectoryUtil.createDir(ContentFolder.FOLDER_PICTURE);
            LogUtil.debug(TAG, " file :" + image);
            if (image.isFile()) {
                final UploadedFile uploadedFile = FileUtil.uploadImageFile(image);
                if (uploadedFile == null) {
                    renderJSON(new ServiceResult("Upload failed").toJson());
                }
                renderJSON(new ServiceResult<String>(
                        true,
                        "success",
                        uploadedFile.getUrl())
                );
            }
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            renderJSON(new ServiceResult(R.translation.cms_failed_save_picture).toJson());
        }
    }

    @Can({ADMIN_KL, SUPER_ADMIN, ADMIN_KONTEN})
    public static void deletePicture(String image) throws Exception {
        LogUtil.debug(TAG, " file string :" + image);
        try {
            if (image != null) {
                File deleteImage = new File(ContentFolder.PATH_PICTURE + image);
                if (deleteImage.delete()) {
                    String message = R.translation.cms_success_delete_picture;
                    renderJSON(new ServiceResult(true, R.translation.cms_success_delete_picture).toJson());
                    flash.success(message);
                }
            }
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            flash.error(R.translation.cms_failed_delete_picture);
            renderJSON(new ServiceResult(R.translation.cms_failed_delete_picture).toJson());
        }
    }

    @Can({ADMIN_KL, SUPER_ADMIN, ADMIN_KONTEN})
    public static void storeProfilekl(ProfileForm model) {
        LogUtil.debug(TAG, "upload profile kl");
        LogUtil.debug(TAG, model);
        validation.valid(model);
        if (validation.hasErrors()) {
            LogUtil.debug(TAG, "Oops.. missing something");
            LogUtil.debug(TAG, validation.errorsMap());
            renderJSON(new ServiceResult(R.translation.cms_failed_form_not_completed).toJson());
        }
        final ServiceResult<JsonObject> result = contentService.storeProfile(model, getCurrentUser());
        renderJSON(result.payload);
    }

    @Can({ADMIN_KL, SUPER_ADMIN, ADMIN_KONTEN})
    public static void storeVisualisasiTableau(VisualisasiTableauForm model) throws Exception {
        LogUtil.debug(TAG, model);
        validation.valid(model);
        if (validation.hasErrors()) {
            LogUtil.debug(TAG, validation.errorsMap());
            renderJSON(new ServiceResult(R.translation.cms_failed_form_not_completed).toJson());
        }
        final ServiceResult<JsonObject> result = contentService.storeTableau(model, getCurrentUser());
        renderJSON(result.payload);
    }
}
