package controllers.cms;

import constants.generated.R;
import controllers.user.management.BaseAdminController;
import models.cms.*;
import models.cms.profilekl.ProfileForm;
import models.common.ServiceResult;
import models.user.management.User;
import models.user.management.contracts.UserContract;
import permission.Can;
import permission.Kldi;
import repositories.cms.AksesContentRepository;
import repositories.cms.ContentRepository;
import repositories.user.management.KldiRepository;
import repositories.user.management.UserRepository;
import tableau.library.TableauRepository;
import tableau.library.models.TableauLibrary;
import utils.common.JsonUtil;
import utils.common.LogUtil;

import java.util.List;

import static permission.Access.*;

public class ContentController extends BaseAdminController {

    public static final String TAG = "ContentController";

    private static final KldiRepository kldiRepository = KldiRepository.getInstance();
    private static final UserRepository userRepository = UserRepository.getInstance();
    private static final AksesContentRepository aksesRepository = new AksesContentRepository();

    @Can({ADMIN_KL, ADMIN_KONTEN, SUPER_ADMIN})
    public static void index() {
        UserContract user = getCurrentUser().user;
        ContentRepository repoContent = new ContentRepository();
        List<Content> contents = repoContent.getContents(true, user);
        renderArgs.put("content", contents);
        renderTemplate("cms/index.html");
    }

    @Can({SUPER_ADMIN})
    public static void publish(Long id) {
        LogUtil.debug(TAG, "activate content by id: " + id);
        if (contentService.changeStatus(id, ContentStatus.PUBLISH)) {
            flashSuccess(R.translation.cms_save_success);
            index();
        }
        flashError(R.translation.cms_save_failed);
        index();
    }

    @Can({SUPER_ADMIN, ADMIN_KL, ADMIN_KONTEN})
    public static void stockReview(Long id) {
        LogUtil.debug(TAG, "activate content by id: " + id);
        if (contentService.changeStatus(id, ContentStatus.STOCK_REVIEW)) {
            flashSuccess(R.translation.cms_save_success);
            index();
        }
        flashError(R.translation.cms_save_failed);
        index();
    }

    @Can({SUPER_ADMIN, ADMIN_KL, ADMIN_KONTEN})
    public static void draft(Long id) {
        LogUtil.debug(TAG, "deactivate content by id: " + id);
        if (contentService.changeStatus(id, ContentStatus.DRAFT)) {
            flashSuccess(R.translation.cms_save_success);
            index();
        }
        flashError(R.translation.cms_save_failed);
        index();
    }

    public static void delete(Long id) {
        final boolean result = contentService.deleteContent(id);
        if (result) {
            flash.success("Content berhasil dihapus");
        } else {
            flash.error("Content gagal dihapus");
        }
        index();
    }

    @Can({ADMIN_KL, SUPER_ADMIN, ADMIN_KONTEN})
    public static void inputArtikelUmum() {
        UserContract user = getCurrentUser().user;
        boolean isUser = !user.isSuperAdmin();
        List<User> content = userRepository.getUsers();
        List<Kldi> daftarkl = kldiRepository.getListKlByRole(user);
        renderArgs.put("model", new ContentForm());
        renderArgs.put("daftarkl", daftarkl);
        renderArgs.put("isUser", isUser);
        renderArgs.put("usersList", content);
        renderTemplate(R.view.cms_general_article_input_artikel_umum);
    }

    @Can({ADMIN_KL, SUPER_ADMIN, ADMIN_KONTEN})
    public static void updateArtikelUmum(Long id) {
        Content model = contentRepository.getContent(id);
        notFoundIfNull(model);
        if (!model.isGeneralArticle()) {
            notFound();
        }
        LogUtil.debug(TAG, model);
        List<User> users = userRepository.getUsers();
        String radSelect = model.permission.toLowerCase();
        UserContract user = getCurrentUser().user;
        boolean isUser = !user.isSuperAdmin();
        List<Kldi> daftarkl = kldiRepository.getListKlByRole(user);
        List<ContentStatus> statuspublish = ContentStatus.getStatus();
        List<AksesContent> listAkses = aksesRepository.getListAksesContent(model.getContentId());
        LogUtil.debug(TAG, "status publish :" + statuspublish);
        renderArgs.put("model", new ContentForm(model));
        renderArgs.put("action", "update");
        renderArgs.put("daftarkl", daftarkl);
        renderArgs.put("isUser", isUser);
        renderArgs.put("radios", radSelect);
        renderArgs.put("listAkses", listAkses);
        renderArgs.put("usersList", users);
        renderTemplate(R.view.cms_general_article_input_artikel_umum);
    }

    @Can({ADMIN_KL, SUPER_ADMIN, ADMIN_KONTEN})
    public static void previewArtikelUmum(Long id) {
        Content model = new ContentRepository().getContent(id);
        notFoundIfNull(model);
        if (!model.isGeneralArticle()) {
            notFound();
        }
        LogUtil.debug(TAG, model);
        UserContract user = getCurrentUser().user;
        boolean isUser = !user.isSuperAdmin();
        renderArgs.put("content", model);
        renderArgs.put("action", "preview");
        renderArgs.put("isUser", isUser);
        renderArgs.put("preview", true);
        renderTemplate(R.view.cms_general_article_view_artikel_umum);
    }

    @Can({ADMIN_KL, SUPER_ADMIN, ADMIN_KONTEN})
    public static void inputProfilekl() {
        UserContract user = getCurrentUser().user;
        List<Kldi> daftarkl = kldiRepository.getListKlByRole(user);
        boolean isUser = !user.isSuperAdmin();
        List<User> users = userRepository.getUsers();
        renderArgs.put("isUser", isUser);
        renderArgs.put("model", new ProfileForm());
        renderArgs.put("action", "create");
        renderArgs.put("daftarkl", daftarkl);
        renderArgs.put("usersList", users);
        renderTemplate(R.view.cms_profile_kl_input_profile_kl);
    }

    @Can({ADMIN_KL, SUPER_ADMIN, ADMIN_KONTEN})
    public static void updateProfileKl(Long id) {
        Content model = contentRepository.getContent(id);
        notFoundIfNull(model);
        if (!model.isProfileKl()) {
            notFound();
        }
        LogUtil.debug(TAG, model);
        UserContract user = getCurrentUser().user;
        boolean isUser = !user.isSuperAdmin();
        List<Kldi> daftarkl = kldiRepository.getListKlByRole(user);
        String radSelect = model.permission.toLowerCase();
        List<User> content = userRepository.getUsers();
        List<AksesContent> listAkses = aksesRepository.getListAksesContent(model.getContentId());
        renderArgs.put("model", new ProfileForm(model));
        renderArgs.put("action", "update");
        renderArgs.put("daftarkl", daftarkl);
        renderArgs.put("isUser", isUser);
        renderArgs.put("radios", radSelect);
        renderArgs.put("listAkses", listAkses);
        renderArgs.put("usersList", content);
        renderTemplate(R.view.cms_profile_kl_input_profile_kl);
    }

    @Can({ADMIN_KL, SUPER_ADMIN, ADMIN_KONTEN})
    public static void previewProfileKl(Long id) {
        Content model = contentRepository.getContent(id);
        notFoundIfNull(model);
        if ((!getCurrentUser().user.isSuperAdmin()
                || !model.kldi_id.equals(getCurrentUser().user.getKldiId()))
                && !model.isProfileKl()) {
            notFound();
        }
        LogUtil.debug(TAG, model);
        renderArgs.put("content", model);
        renderArgs.put("preview", true);
        renderTemplate(R.view.cms_profile_kl_view_profile_kl);
    }

    private static boolean setTableauRequirements() {
        UserContract user = getCurrentUser().user;
        List<Kldi> daftarkl = kldiRepository.getListKlByRole(user);
        List<User> users = userRepository.getUsers();
        List<TableauLibrary> libraries = TableauRepository.getInstance().getAllForKLPD(user);
        libraries.forEach(TableauLibrary::withParams);
        renderArgs.put("daftarkl", daftarkl);
        renderArgs.put("isUser", !user.isSuperAdmin());
        renderArgs.put("usersList", users);
        renderArgs.put("libraries", libraries);
        renderArgs.put("librariesJson", JsonUtil.toJson(libraries));
        return true;
    }

    @Can({ADMIN_KL, SUPER_ADMIN, ADMIN_KONTEN})
    public static void inputVisualisasiTableau() throws Exception {
        setTableauRequirements();
        renderArgs.put("model", new VisualisasiTableauForm());
        renderTemplate(R.view.cms_tableau_content_input_visualisasi_tableau);
    }

    @Can({ADMIN_KL, SUPER_ADMIN, ADMIN_KONTEN})
    public static void updateVisualisasiTableau(Long id) {
        Content model = contentRepository.getContent(id);
        notFoundIfNull(model);
        if (!model.isTableau()) {
            notFound();
        }
        LogUtil.debug(TAG, model);
        List<AksesContent> listAkses = aksesRepository.getListAksesContent(model.getContentId());
        setTableauRequirements();
        renderArgs.put("model", new VisualisasiTableauForm(model));
        renderArgs.put("listAkses", listAkses);
        renderTemplate(R.view.cms_tableau_content_input_visualisasi_tableau);
    }

    @Can({ADMIN_KL, SUPER_ADMIN, ADMIN_KONTEN})
    public static void storeAccess(AccessForm accessForm) {
        LogUtil.debug(TAG, accessForm);
        validation.valid(accessForm);
        if (validation.hasErrors()) {
            params.flash();
            flash.error(R.translation.cms_failed_form_not_completed);
            inputProfilekl();
        }
        ServiceResult result = contentService.storeAccessContent(accessForm);
        if (result.status) {
            flashSuccess(result.message);
            index();
        }
        flashError(result.message);
        redirectToEditPage(accessForm.id);
    }

    private static void redirectToEditPage(Long id) {
        Content content = contentRepository.getContent(id);
        if (content.isProfileKl()) {
            updateProfileKl(id);
        } else if (content.isTableau()) {
            updateVisualisasiTableau(id);
        }
        updateArtikelUmum(id);
    }

    @Can({ADMIN_KL, SUPER_ADMIN, ADMIN_KONTEN})
    public static void previewVisualisasiTableau(Long id) {
        Content model = contentRepository.getContent(id);
        notFoundIfNull(model);
        if (!model.isTableau()) {
            notFound();
        }
        model.withLibrary();
        LogUtil.debug(TAG, model);
        UserContract user = getCurrentUser().user;
        renderArgs.put("content", model);
        renderArgs.put("action", "preview");
        renderArgs.put("isUser", !user.isSuperAdmin());
        renderTemplate(R.view.cms_tableau_content_preview_visualisasi_tableau);
    }

}