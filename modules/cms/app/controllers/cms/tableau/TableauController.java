package controllers.cms.tableau;

import constants.generated.R;
import controllers.user.management.BaseAdminController;
import models.common.ServiceResult;
import permission.Access;
import permission.Can;
import play.data.validation.Validation;
import tableau.library.TableauParamRepository;
import tableau.library.TableauRepository;
import tableau.library.models.TableauLibrary;
import tableau.library.models.TableauLibraryForm;
import tableau.library.models.TableauLibraryParams;
import utils.common.LogUtil;

import static permission.Access.SUPER_ADMIN;

/**
 * @author HanusaCloud on 10/29/2019 3:16 PM
 */
public class TableauController extends BaseAdminController {

    public static final String TAG = "TableauController";
    private static TableauRepository repository = TableauRepository.getInstance();
    private static TableauParamRepository paramRepository = TableauParamRepository.getInstance();

    @Can({SUPER_ADMIN})
    public static void index() {
        renderTemplate(R.view.cms_tableau_library_index);
    }

    @Can({Access.SUPER_ADMIN})
    public static void insert() {
        renderArgs.put("model", new TableauLibraryForm());
        renderTemplate(R.view.cms_tableau_library_input);
    }

    @Can({Access.SUPER_ADMIN})
    public static void update(Long id) {
        notFoundIfNull(id);
        TableauLibrary model = repository.getById(id);
        notFoundIfNull(model);
        model.withParams();
        renderArgs.put("model", new TableauLibraryForm(model));
        renderTemplate(R.view.cms_tableau_library_input);
    }

    @Can({Access.SUPER_ADMIN})
    public static void store(TableauLibraryForm model) {
        LogUtil.debug(TAG, "store tableau model");
        LogUtil.debug(TAG, model);
        validation.valid(model);
        if (Validation.hasErrors() || !model.isValidUri() || !model.isValidParams()) {
            params.flash();
            Validation.keep();
            if (!model.isValidUri()) {
                flashError(R.translation.cms_tableau_invalid_uri);
            }
            if (!model.isValidParams()) {
                flashError(R.translation.cms_tableau_label_or_value_missing);
            }
            if (model.isEditedMode()) {
                update(model.id);
            }
            insert();
        }
        TableauLibrary library = new TableauLibrary();
        if (model.isEditedMode()) {
            library = repository.getById(model.id);
        }
        library.setBy(model);
        library.saveModel();
        paramRepository.deleteByLibraryId(library.id);
        if (model.params != null) {
            for (TableauLibraryParams param : model.params.values()) {
                param.library_id = library.id;
                param.saveModel();
            }
        }
        flashSuccess(R.translation.cms_tableau_save_success);
        index();
    }

    @Can({Access.SUPER_ADMIN})
    public static void delete(Long id) {
        LogUtil.debug(TAG, "delete library by id: " + id);
        notFoundIfNull(id);
        TableauLibrary model = repository.getById(id);
        notFoundIfNull(model);
        ServiceResult result = model.deleteLibrary();
        if (!result.status) {
            flashError(result.message);
        } else {
            flashSuccess(result.message);
        }
        index();
    }

}
