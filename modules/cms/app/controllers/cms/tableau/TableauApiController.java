package controllers.cms.tableau;

import controllers.common.ApiBaseController;
import models.common.contracts.DataTableRequest;
import permission.Access;
import permission.Can;
import tableau.library.models.TableauLibrarySearchResult;

/**
 * @author HanusaCloud on 10/29/2019 4:23 PM
 */
public class TableauApiController extends ApiBaseController {

    public static final String TAG = "TableauApiController";

    @Can({Access.SUPER_ADMIN})
    public static void libraries() {
        TableauLibrarySearchResult model = new TableauLibrarySearchResult(new DataTableRequest(request));
        renderJSON(model.getResultsAsJson());
    }

}
