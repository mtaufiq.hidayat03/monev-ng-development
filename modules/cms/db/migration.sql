-- ----------------------------
-- Sequence structure for akses_content_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."akses_content_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for akses_content
-- ----------------------------
CREATE TABLE "public"."akses_content" (
  "id" bigint NOT NULL DEFAULT nextval('akses_content_id_seq'::regclass),
  "user_id" bigint,
  "content_id" bigint,
  "profilkl_id" bigint
)
;

ALTER TABLE "public"."akses_content" ADD CONSTRAINT "akses_content_pkey" PRIMARY KEY ("id");


-- ----------------------------
-- Sequence structure for contents_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."contents_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for contents
-- ----------------------------
CREATE TABLE "public"."contents" (
  "id" bigint NOT NULL DEFAULT nextval('contents_id_seq'::regclass),
  "title" character varying(255),
  "vision" character varying(255),
  "mission" character varying(255),
  "content" text,
  "regulation" json,
  "mou" json,
  "featured_image" character varying(255),
  "type" character varying(255),
  "kldi_id" character varying(255),
  "publish" integer,
  "created_by" integer,
  "created_at" timestamp(6),
  "updated_by" integer,
  "updated_at" timestamp(6),
  "deleted_by" integer,
  "deleted_at" timestamp(6),
  "permission" character(20),
  "free_content" text,
  "alamat_url" text
)
;

-- ----------------------------
-- Primary Key structure for table contents
-- ----------------------------
ALTER TABLE "public"."contents" ADD CONSTRAINT "contents_pkey" PRIMARY KEY ("id");


CREATE SEQUENCE "public"."tableau_library_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."tableau_library" (
"id" int8 DEFAULT nextval('tableau_library_id_seq'::regclass) NOT NULL,
"title" varchar(255) COLLATE "default",
"description" text COLLATE "default",
"available_for_klpd" bool,
"url" text COLLATE "default",
"created_by" int4,
"created_at" timestamp(6),
"updated_by" int4,
"updated_at" timestamp(6),
"deleted_by" int4,
"deleted_at" timestamp(6),
"kldi_id" varchar(30) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of tableau_library
-- ----------------------------
INSERT INTO "public"."tableau_library" VALUES ('5', 'Belanja Pengadaan Daerah Kabupaten', 'belanja pengadaan kabupaten', 't', 'https://monevtab.lkpp.go.id/views/MonevPusat/BelanjaPengadaanKabKota?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-06 14:36:43.829', '1', '2019-12-06 13:15:12.522', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('7', 'Belanja Pengadaan (Nasional)', 'Belanja Pengadaan nasional', 't', 'https://monevtab.lkpp.go.id/views/MonevPusat/BelanjaPengadaanNasional?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-07 09:22:32.339', '1', '2019-11-14 17:29:06.091', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('8', 'klasifikasi penyedia nasional', 'klasifikasi penyedia nasional', 'f', 'https://monevtab.lkpp.go.id/#/views/RencanaBelanja/2_2KlasifikasiPenyedia?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-07 14:12:57.283', '1', '2019-11-07 14:19:58.203', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('9', 'Jenis Belanja Pengadaan Pusat', 'jenis belanja pengadaan pusat', 't', 'https://monevtab.lkpp.go.id/views/MonevPusat/JenisBelanjaPengadaanPusat?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-07 14:19:31.35', '1', '2019-11-07 14:22:29.904', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('10', 'jenis pengadaan - nasional', '[perencanaan] Belanja - Rencana Pemilihan/ Pelaksanaan Jenis Pengadaan', 'f', 'https://monevtab.lkpp.go.id/#/views/RencanaBelanja/2_3JenisPengadaan?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-07 16:24:03.89', '1', '2019-11-13 13:56:28.718', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('11', 'Hasil Pemilihan Nasional', 'hasil pemilihan - nasional', 't', 'https://monevtab.lkpp.go.id/#/views/HasilPemilihan/DashboardPaket-NilaiPusatDaerah?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-13 15:28:18.268', '1', '2019-11-14 16:03:19.933', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('12', 'Belanja - Rencana Pemilihan/ Pelaksanaan Klasifikasi Penyedia', 'Belanja - Rencana Pemilihan/ Pelaksanaan Klasifikasi Penyedia', 't', 'https://monevtab.lkpp.go.id/#/views/RencanaBelanja/2_2KlasifikasiPenyedia?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-13 15:48:40.45', '1', '2019-11-14 16:48:10.952', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('13', 'Belanja - Rencana Pemilihan/ Pelaksanaan Jenis Pengadaan', 'Belanja - Rencana Pemilihan/ Pelaksanaan Jenis Pengadaan', 't', 'https://monevtab.lkpp.go.id/#/views/RencanaBelanja/2_7JenisPengKontrak?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-13 15:57:52.561', '1', '2019-11-14 16:48:37.474', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('14', 'Belanja - Rencana Pemilihan/ Pelaksanaan Grafik Spasial', 'Belanja - Rencana Pemilihan/ Pelaksanaan Grafik Spasial', 't', 'https://monevtab.lkpp.go.id/#/views/Perencanaan-GrafikSpasial/2_4Belanja-Nasional?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-13 16:11:16.249', '1', '2019-11-14 16:47:30.461', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('15', 'Belanja - Rencana Kontrak Total', 'Belanja - Rencana Kontrak Total', 't', 'https://monevtab.lkpp.go.id/#/views/RencanaBelanja/2_5RencanaKontrak?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-13 16:32:36.802', '1', '2019-11-14 16:49:10.829', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('16', 'Belanja - Rencana Kontrak Kelola Penyedia', 'Belanja - Rencana Kontrak Kelola Penyedia', 't', 'https://monevtab.lkpp.go.id/views/RencanaBelanja/2_6KlasPenydKontrak?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-13 16:41:45.196', '1', '2019-11-14 17:02:12.392', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('17', 'Belanja - Rencana Kontrak Jenis Pengadaan', 'Belanja - Rencana Kontrak Jenis Pengadaan', 't', 'https://monevtab.lkpp.go.id/views/RencanaBelanja/2_3JenisPengadaan?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-13 17:08:25.063', '1', '2019-11-14 16:56:10.43', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('18', 'Belanja - Rencana Kontrak Grafik Spasial', 'Belanja - Rencana Kontrak Grafik Spasial', 't', 'https://monevtab.lkpp.go.id/views/Perencanaan-GrafikSpasial/2_4Belanja-Nasional?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-13 17:17:26.157', '1', '2019-11-14 16:44:14.849', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('19', 'Kegiatan - Rencana Pemilihan/Pelaksanaan Total', 'Kegiatan - Rencana Pemilihan/Pelaksanaan Total', 'f', 'https://monevtab.lkpp.go.id/views/RencanaKegiatan/2_9RencanaKegiatanPusat?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-13 17:42:54.966', '1', '2019-11-14 16:37:07.526', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('20', 'Kegiatan - Rencana Pemilihan/Pelaksanaan Klasifikasi Kegiatan', 'Kegiatan - Rencana Pemilihan/Pelaksanaan Klasifikasi Kegiatan', 'f', 'https://monevtab.lkpp.go.id/#/views/RencanaKegiatan/2_14KlasPenyKegKontrakPusat?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-13 17:52:56.803', null, null, null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('21', 'Hasil Pemilihan Nasional (Sample)', 'Hasil Pemilihan Nasional', 't', 'https://monevtab.lkpp.go.id/views/HasilPemilihan/DashboardPaket-NilaiPusatDaerah?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-14 12:18:42.403', '1', '2019-11-14 12:52:29.067', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('22', 'Kegiatan - Rencana Pemilihan/Pelaksanaan Grafik Spasial', 'Kegiatan - Rencana Pemilihan/Pelaksanaan Grafik Spasial', 't', 'https://monevtab.lkpp.go.id/views/Perencanaan-GrafikSpasial/2_12Keg-Nas?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-14 12:38:45.359', '1', '2019-11-14 16:43:08.74', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('25', 'Kegiatan Rencana Kontrak Grafik Spasial', 'Kegiatan Rencana Kontrak Grafik Spasial', 'f', 'https://monevtab.lkpp.go.id/#/views/Perencanaan-GrafikSpasial/2_4Belanja-Nasional?:iid=3', '1', '2019-11-14 13:05:39.806', null, null, null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('26', 'HPS Pemilihan Penyedia', 'HPS Pemilihan Penyedia', 'f', 'https://monevtab.lkpp.go.id/#/views/HasilPemilihan/DashboardPaket-NilaiPusatDaerah?:iid=1', '1', '2019-11-14 13:09:09.414', null, null, null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('27', 'Waktu Pengumuman Mulai Lelang (berdasarkan Paket)', 'Waktu Pengumuman Mulai Lelang (berdasarkan Paket)', 'f', 'https://monevtab.lkpp.go.id/#/views/1DistribusiLelang/1_1DistribusiJumlahLelang?:iid=2', '1', '2019-11-14 13:11:54.584', null, null, null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('28', 'Belanja - Rencana Pengadaan', 'Belanja - Rencana Pengadaan', 't', 'https://monevtab.lkpp.go.id/views/RencanaBelanja/2_1RencanaPengadaanPusatNasional?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', null, '1', '2019-11-14 13:39:18.777', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('29', 'Hasil Lelang', 'Hasil Lelang', 'f', 'https://monevtab.lkpp.go.id/#/views/1DistribusiLelang/1_2DistribusiNilaiLelang?:iid=4', '1', '2019-11-14 13:40:08.923', null, null, null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('31', 'Kegiatan - Rencana Kontrak', 'Kegiatan - Rencana Kontrak', 't', 'https://monevtab.lkpp.go.id/views/RencanaKegiatan/2_13RencanaPengKegKontrakPusat?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', null, '1', '2019-11-14 17:01:51.884', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('32', 'Kegiatan - Rencana Kontrak Jenis Pengadaaan', 'Kegiatan - Rencana Kontrak Jenis Pengadaaan', 't', 'https://monevtab.lkpp.go.id/views/RencanaKegiatan/2_15JenisPengadaanKegiatanKontrakPusat?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', null, null, '1', '2019-11-14 16:43:11.588', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('33', 'Kegiatan - Rencana Pemilihan Jenis Pengadaan', 'Kegiatan - Rencana Pemilihan Jenis Pengadaan', 't', 'https://monevtab.lkpp.go.id/views/RencanaKegiatan/2_11JenisPengadaanKegiatanPusat?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', null, null, '1', '2019-11-14 16:46:57.551', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('34', 'Belanja Pengadaan KL', 'Belanja Pengadaan KL', 'f', 'https://monevtab.lkpp.go.id/views/MonevPusat/BelanjaPengadaanKL?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', null, null, '1', '2019-11-14 16:04:46.531', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('35', 'Belanja Pengadaan Terbesar Pusat', 'Belanja Pengadaan Terbesar Pusat', 't', 'https://monevtab.lkpp.go.id/views/BelanjaTerbesar/10BelanjaPengadaanTerbesarPusat?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', null, null, '1', '2019-11-14 17:01:25.08', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('36', 'Kegiatan - Klafisikasi Penyedia Kegiatan', 'Kegiatan - Klafisikasi Penyedia Kegiatan', 'f', 'https://monevtab.lkpp.go.id/views/RencanaKegiatan/2_10KlasifikasiPenyediaKegiatanPusat?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', null, null, '1', '2019-11-14 16:41:10.252', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('39', 'Trend', 'Trend', 'f', 'https://monevtab.lkpp.go.id/views/RencanaKegiatan/2_9RencanaKegiatanKL?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-24 23:42:46.142', null, null, null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('40', 'Hasil Pemilihan Paket Nilai Pusat Daerah', 'Hasil Pemilihan Paket Nilai Pusat Daerah', 't', 'https://monevtab.lkpp.go.id/views/HasilPemilihan/DashboardPaket-NilaiPusatDaerahasli?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-26 05:24:22.423', null, null, null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('41', 'Belanja Pengadaan Daerah Kabupaten', 'belanja pengadaan kabupaten', 't', 'https://s.id/UTScitra', '1', '2019-11-30 16:37:04.507', null, null, null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('42', 'belanja negara', 'belanja cuyy', 't', 'https://monevtab.lkpp.go.id/views/MonevPusat/BelanjaPengadaanNasional?iframeSizedToWindow=true&:embe...', '1', '2019-12-03 17:36:06.532', null, null, null, null, null);

-- ----------------------------
-- Alter Sequences Owned By
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table tableau_library
-- ----------------------------
ALTER TABLE "public"."tableau_library" ADD PRIMARY KEY ("id");

SELECT setval('"public"."tableau_library_id_seq"', 50, true);

CREATE SEQUENCE "public"."tableau_library_parameters_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."tableau_library_parameters" (
"id" int8 DEFAULT nextval('tableau_library_parameters_id_seq'::regclass) NOT NULL,
"label" varchar(255) COLLATE "default",
"value" varchar COLLATE "default",
"library_id" int8,
"created_by" int4,
"created_at" timestamp(6),
"updated_by" int4,
"updated_at" timestamp(6),
"deleted_by" int4,
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of tableau_library_parameters
-- ----------------------------
INSERT INTO "public"."tableau_library_parameters" VALUES ('8', 'YEAR(published_date)', '2018,2017,2016', '6', '1', '2019-11-07 01:54:08.014', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('13', 'tahun anggaran', '2017,2018,2019', '8', '1', '2019-11-07 14:19:58.206', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('14', 'tahun_anggaran', '2017,2018', '9', '1', '2019-11-07 14:22:29.905', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('19', 'Choose Years (copy)', '2017,2018,2016,2013', '10', '1', '2019-11-13 13:56:28.72', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('32', 'Choose Years (copy)', '2018', '20', '1', '2019-11-13 17:52:56.898', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('40', 'Choose Years (copy)', '2017', '23', '1', '2019-11-14 12:47:04.689', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('41', 'Select a View', 'Paket,Nilai', '21', '1', '2019-11-14 12:52:29.069', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('42', 'Choose Years (copy)', '2018', '24', '1', '2019-11-14 12:55:18.749', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('43', 'tahun anggaran', '2017', '25', '1', '2019-11-14 13:05:39.809', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('44', 'Choose Years (copy)', '2013,2014,2015,2016,2017,2018', '28', '1', '2019-11-14 13:39:18.782', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('45', 'Lelang Option', 'perbandingan perbulan', '29', '1', '2019-11-14 13:40:08.928', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('47', 'Select a View', 'nilai,Paket', '11', '1', '2019-11-14 16:03:19.935', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('48', 'Choose Years (copy)', '2013,2014,2015,2017,2018', '34', '1', '2019-11-14 16:04:46.532', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('54', 'Choose Years (copy)', '2013,2014,2015,2016,2017,2018', '19', '1', '2019-11-14 16:37:07.527', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('56', 'Choose Years (copy)', '2013,2014,2015,2016,2017,2018', '36', '1', '2019-11-14 16:41:10.253', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('57', 'Tahun Anggaran', '2013,2014,2015,2016,2017', '22', '1', '2019-11-14 16:43:08.742', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('58', 'Choose Years (copy)', '2013,2014,2015,2016,2017,2018', '32', '1', '2019-11-14 16:43:12.266', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('59', 'Tahun Anggaran', '2013,2014,2015,2016,2017', '18', '1', '2019-11-14 16:44:14.85', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('60', 'Choose Years (copy)', '2013,2014,2015,2016,2017,2018', '33', '1', '2019-11-14 16:46:57.552', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('61', 'Tahun Anggaran', '2013,2014,2015,2016,2017', '14', '1', '2019-11-14 16:47:30.464', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('62', 'Choose Years (copy)', '2013,2014,2015,2016,2017,2018', '12', '1', '2019-11-14 16:48:10.953', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('63', 'Choose Years (copy)', '2013,2015,2014,2016,2017,2018', '13', '1', '2019-11-14 16:48:37.476', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('64', 'Choose Years (copy)', '2013,2014,2015,2016,2017,2018', '15', '1', '2019-11-14 16:49:10.83', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('66', 'Choose Years (copy)', '2013,2014,2015,2016,2017,2018', '17', '1', '2019-11-14 16:56:10.431', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('67', 'Tahun Anggaran', '2012,2013,2014,2015,2017', '35', '1', '2019-11-14 17:01:25.08', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('68', 'Choose Years (copy)', '2013,2014,2015,2016,2017,2018', '31', '1', '2019-11-14 17:01:51.885', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('69', 'Choose Years (copy)', '2013,2014,2015,2016,2017,2018', '16', '1', '2019-11-14 17:02:12.393', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('70', 'Tahun Anggaran', '2012, 2013,2014,2015,2017,2018', '7', '1', '2019-11-14 17:29:06.094', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('73', 'tahun anggaran', '2013,2014,2015,2016,2017', '41', '1', '2019-11-30 16:37:04.514', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('74', 'tahun', '2018,2019', '42', '1', '2019-12-03 17:36:06.556', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('76', 'tahun anggaran', '2013,2014,2015,2016,2017,2018', '5', '1', '2019-12-06 13:15:12.528', null, null, null, null);

-- ----------------------------
-- Alter Sequences Owned By
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table tableau_library_parameters
-- ----------------------------
ALTER TABLE "public"."tableau_library_parameters" ADD PRIMARY KEY ("id");

SELECT setval('"public"."tableau_library_parameters_id_seq"', 80, true);

ALTER TABLE "public"."contents" ADD COLUMN "tableau_library_id" bigint;

CREATE TABLE "public"."content_tableau_parameters" (
  "content_id" bigint,
  "tableau_param_id" bigint,
  "value" text
)
;

-- ----------------------------
-- Primary Key structure for table contents
-- ----------------------------
ALTER TABLE "public"."content_tableau_parameters" ADD CONSTRAINT "content_tableau_parameters_pkey" PRIMARY KEY ("content_id", "tableau_param_id");

CREATE SEQUENCE "public"."profile_kl_data_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."profile_kl_data" (
  "id" bigint NOT NULL DEFAULT nextval('profile_kl_data_id_seq'::regclass),
  "content_id" bigint NOT NULL,
  "header" character varying(250),
  "value" NUMERIC NULL,
  "created_by" integer,
  "created_at" timestamp(6)
)
;
ALTER TABLE "public"."profile_kl_data" ADD CONSTRAINT "profile_kl_data_pkey" PRIMARY KEY ("id");
