/*
Navicat PGSQL Data Transfer

Source Server         : Ubuntu-ui-psql
Source Server Version : 101000
Source Host           : 192.168.120.30:5432
Source Database       : monev_ng
Source Schema         : dwh_procurement

Target Server Type    : PGSQL
Target Server Version : 101000
File Encoding         : 65001

Date: 2019-12-11 14:55:39
*/

CREATE SCHEMA IF NOT EXISTS dwh_procurement;

-- ----------------------------
-- Table structure for check_paket_id
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."check_paket_id";
CREATE TABLE "dwh_procurement"."check_paket_id" (
"kode_paket" int4,
"nama" text COLLATE "default",
"hasil" text COLLATE "default",
"kode_paket_bl" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for cpi_report_01
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."cpi_report_01";
CREATE TABLE "dwh_procurement"."cpi_report_01" (
"tahun_anggaran" int2,
"kode_paket" numeric(19),
"kode_paket_rup" numeric(19),
"kode_lelang" numeric(19),
"kode_kldi" varchar(20) COLLATE "default",
"nama_lpse" varchar(255) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"jenis_kldi" varchar(255) COLLATE "default",
"nama_satker" varchar(200) COLLATE "default",
"nama_kabupaten" varchar(80) COLLATE "default",
"nama_propinsi" varchar(80) COLLATE "default",
"sumber_dana" text COLLATE "default",
"jenis_pengadaan" varchar(80) COLLATE "default",
"metode_pemilihan" text COLLATE "default",
"nama_paket" text COLLATE "default",
"versi_lelang" int2,
"kode_rekening" text COLLATE "default",
"nilai_pagu" float8,
"nilai_hps" numeric(20,2),
"nilai_anggaran" float8,
"tgl_lelang_dibuat" timestamp(6),
"tgl_umum_prakualifikasi" timestamp(6),
"tgl_umum_pascakualifikasi" timestamp(6),
"tgl_sppbj" timestamp(6),
"tgl_ttd_kontrak" timestamp(6),
"tgl_buka_dok_penawaran" timestamp(6),
"tgl_rencana_awal_pengadaan" timestamp(6),
"tgl_rencana_akhir_pengadaan" timestamp(6),
"tgl_umum_pemenang" timestamp(6),
"psr_harga" float8,
"psr_harga_terkoreksi" float8,
"jenis_belanja" text COLLATE "default",
"tgl_dok_lelang" timestamp(6),
"tgl_upload_dok_penawaran" timestamp(6),
"kode_lpse" int4,
"tgl_umum_pemenang_awal" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for cpi_report_01_a
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."cpi_report_01_a";
CREATE TABLE "dwh_procurement"."cpi_report_01_a" (
"tahun_anggaran" float8,
"kode_lpse" int4,
"nama_lpse" varchar(255) COLLATE "default",
"kode_kldi" text COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"kode_kldi_rup" varchar(255) COLLATE "default",
"nama_satker" text COLLATE "default",
"nama_satker_rup" varchar(200) COLLATE "default",
"nama_kabupaten" varchar(80) COLLATE "default",
"nama_propinsi" varchar(80) COLLATE "default",
"jenis_kldi" varchar(255) COLLATE "default",
"kode_paket" numeric(19),
"kode_paket_rup" numeric(19),
"nama_paket" text COLLATE "default",
"nama_paket_rup" text COLLATE "default",
"jenis_pengadaan" varchar(80) COLLATE "default",
"metode_pemilihan" text COLLATE "default",
"sumber_dana" text COLLATE "default",
"kode_lelang" numeric(19),
"versi_lelang" int2,
"kode_rekening" text COLLATE "default",
"kode_rekening_rup" varchar(200) COLLATE "default",
"nilai_pagu" float8,
"nilai_hps" float8,
"nilai_anggaran" float8,
"tgl_lelang_dibuat" timestamp(6),
"tgl_umum_prakualifikasi" timestamp(6),
"tgl_umum_pascakualifikasi" timestamp(6),
"tgl_sppbj" timestamp(6),
"tgl_ttd_kontrak" timestamp(6),
"tgl_buka_dok_penawaran" timestamp(6),
"tgl_rencana_awal_pengadaan" timestamp(6),
"tgl_rencana_akhir_pengadaan" timestamp(6),
"tgl_umum_pemenang" timestamp(6),
"tgl_umum_pemenang_awal" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for cpi_report_02
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."cpi_report_02";
CREATE TABLE "dwh_procurement"."cpi_report_02" (
"tahun_anggaran" int2,
"kode_kldi" varchar(20) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"jenis_kldi" varchar(255) COLLATE "default",
"sumber_dana" text COLLATE "default",
"nama_kabupaten" varchar(80) COLLATE "default",
"nama_propinsi" varchar(80) COLLATE "default",
"kode_lpse" int4,
"nama_lpse" varchar(255) COLLATE "default",
"kode_paket" numeric(19),
"nama_paket" varchar(5000) COLLATE "default",
"kode_paket_rup" numeric(19),
"nama_paket_rup" text COLLATE "default",
"versi_lelang" float8,
"kode_lelang" numeric(19),
"kode_peserta" numeric(19),
"nama_penyedia" varchar(100) COLLATE "default",
"npwp_penyedia" varchar(50) COLLATE "default",
"is_pemenang" int2,
"ev_admin" int4,
"ev_teknis" int4,
"ev_harga" int4,
"ev_kualifikasi" int4,
"harga_penawaran" float8,
"harga_terkoreksi" float8,
"nama_satker" varchar(200) COLLATE "default",
"nama_satker_rup" varchar(200) COLLATE "default",
"kls_id" varchar(2) COLLATE "default",
"klasifikasi_usaha" varchar(80) COLLATE "default",
"metode_pemilihan" text COLLATE "default",
"metode_kualifikasi" varchar(50) COLLATE "default",
"jenis_pengadaan" varchar(80) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for cpi_report_03
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."cpi_report_03";
CREATE TABLE "dwh_procurement"."cpi_report_03" (
"tahun_anggaran" int2,
"tgl_sanggah" timestamp(6),
"tgl_jawab_sanggah" timestamp(6),
"thp_nama" varchar(200) COLLATE "default",
"kode_lpse" int4,
"nama_lpse" varchar(255) COLLATE "default",
"kode_kldi" varchar(20) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"jenis_kldi" varchar(255) COLLATE "default",
"nama_kabupaten" varchar(80) COLLATE "default",
"nama_propinsi" varchar(80) COLLATE "default",
"kode_lelang" numeric(19),
"nama_paket" varchar(500) COLLATE "default",
"sumber_dana" text COLLATE "default",
"kode_rekening" text COLLATE "default",
"kode_penyedia" numeric(19),
"nama_penyedia" varchar(100) COLLATE "default",
"npwp_penyedia" varchar(50) COLLATE "default",
"nilai_pagu" numeric(20,2),
"jenis_pengadaan" varchar(80) COLLATE "default",
"metode_pemilihan" varchar(255) COLLATE "default",
"kode_paket" numeric(19)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for cpi_report_03_sg
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."cpi_report_03_sg";
CREATE TABLE "dwh_procurement"."cpi_report_03_sg" (
"repo_id" int4,
"lls_id" numeric(19),
"pkt_id" numeric(19),
"pkt_nama" varchar(500) COLLATE "default",
"min" float8,
"pagu" numeric(20,2),
"lls_versi_lelang" int2,
"eva_versi" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for cpi_report_04
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."cpi_report_04";
CREATE TABLE "dwh_procurement"."cpi_report_04" (
"kode_paket" int4,
"id_swakelola" int8,
"nama_jenis" varchar(255) COLLATE "default",
"nama_paket" text COLLATE "default",
"jenis_pengadaan" varchar(40) COLLATE "default",
"satuan_kerja" varchar(255) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"tipe_kldi" varchar(255) COLLATE "default",
"kabupaten" varchar(80) COLLATE "default",
"propinsi" varchar(80) COLLATE "default",
"tahun_anggaran" int4,
"nilai_pagu" int8,
"metode_pemilihan" varchar(40) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for dm_fraud_address
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."dm_fraud_address";
CREATE TABLE "dwh_procurement"."dm_fraud_address" (
"nama_penyedia" text COLLATE "default",
"rkn_alamat" varchar(1000) COLLATE "default",
"alamat_penyedia" text COLLATE "default",
"rkn_kodepos" varchar(8) COLLATE "default",
"rkn_telepon" varchar(50) COLLATE "default",
"rkn_fax" varchar(50) COLLATE "default",
"rkn_mobile_phone" varchar(50) COLLATE "default",
"rkn_npwp" varchar(50) COLLATE "default",
"rkn_pkp" varchar(100) COLLATE "default",
"rkn_statcabang" varchar(1) COLLATE "default",
"rkn_email" varchar(1000) COLLATE "default",
"rkn_website" varchar(40) COLLATE "default",
"rkn_almtpusat" varchar(1000) COLLATE "default",
"rkn_telppusat" varchar(50) COLLATE "default",
"rkn_faxpusat" varchar(50) COLLATE "default",
"rkn_emailpusat" varchar(40) COLLATE "default",
"rkn_namauser" varchar(50) COLLATE "default",
"rkn_isactive" int2,
"key_penyedia" text COLLATE "default",
"key_alamat" text COLLATE "default",
"key_npwp" text COLLATE "default",
"key_phone" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for dm_klasifikasipenyedia
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."dm_klasifikasipenyedia";
CREATE TABLE "dwh_procurement"."dm_klasifikasipenyedia" (
"nama_penyedia" varchar(200) COLLATE "default",
"npwp_penyedia" varchar(200) COLLATE "default",
"lokasi_penyedia_kabupaten" varchar(80) COLLATE "default",
"lokasi_penyedia_provinsi" varchar(80) COLLATE "default",
"tipe" varchar(10) COLLATE "default",
"kode_jenis_usaha" varchar(10) COLLATE "default",
"klasifikasi" varchar(200) COLLATE "default",
"kategori" varchar(200) COLLATE "default",
"golongan_pokok" varchar(250) COLLATE "default",
"golongan" varchar(200) COLLATE "default",
"sub_golongan" varchar(250) COLLATE "default",
"kelompok" varchar(200) COLLATE "default",
"verified" bool,
"tahun" int2,
"update_date" timestamp(6),
"status_adp" bool
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for dm_profilpenyedia
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."dm_profilpenyedia";
CREATE TABLE "dwh_procurement"."dm_profilpenyedia" (
"tahun_anggaran" int2,
"nama_penyedia" text COLLATE "default",
"npwp_penyedia" text COLLATE "default",
"lolos_evaluasi_thp0" int8,
"lolos_evaluasi_thp1" int8,
"lolos_evaluasi_thp2" int8,
"lolos_evaluasi_thp3" int8,
"lolos_evaluasi_thp4" int8,
"jumlah_paket" int8,
"jumlah_ikut_lelang" int8,
"jumlah_ikut_peserta" int8,
"jumlah_menang" int8,
"total_perolehan" numeric(22,2),
"lokasi_lelang_kabupaten" varchar(80) COLLATE "default",
"lokasi_lelang_provinsi" varchar(80) COLLATE "default",
"repo_lelang" int4,
"lokasi_penyedia_kabupaten" varchar(80) COLLATE "default",
"lokasi_penyedia_provinsi" varchar(80) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for dm_rekappengadaan
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."dm_rekappengadaan";
CREATE TABLE "dwh_procurement"."dm_rekappengadaan" (
"tahun_anggaran" int2,
"jenis_kldi" varchar(255) COLLATE "default",
"id_kldi" varchar(255) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"nama_propinsi" varchar(80) COLLATE "default",
"nama_kabupaten" varchar(80) COLLATE "default",
"jumlah_paket_penyedia" int8,
"jumlah_kegiatan_swakelola" int8,
"jumlah_kegiatan_penyedia_swakelola" int8,
"jumlah_paket_lelang" int8,
"jumlah_paket_lelang_selesai" int8,
"jumlah_paket_ekatalog" int8,
"anggaran_sismontepra" numeric(22,2),
"pagu_paket_penyedia" numeric(22,2),
"pagu_kegiatan_swakelola" numeric(22,2),
"pagu_kegiatan_penyedia_swakelola" numeric(22,2),
"dekon_tp" numeric(22,2),
"anggaran_pegawai_penyedia" numeric(22,2),
"anggaran_pegawai_swakelola" numeric(22,2),
"pagu_lelang" numeric(22,2),
"pagu_lelang_selesai" numeric(22,2),
"nilai_hasil_lelang" numeric(22,2),
"nilai_hps" numeric(22,2),
"pagu_ekatalog" numeric(22,2),
"update_date" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for dm_rekappengadaanperbulan
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."dm_rekappengadaanperbulan";
CREATE TABLE "dwh_procurement"."dm_rekappengadaanperbulan" (
"tahun_anggaran" int2,
"jenis_kldi" varchar(255) COLLATE "default",
"id_kldi" varchar(255) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"periode" timestamp(6),
"nama_propinsi" varchar(80) COLLATE "default",
"nama_kabupaten" varchar(80) COLLATE "default",
"jumlah_paket_penyedia" int8,
"jumlah_kegiatan_swakelola" int8,
"jumlah_kegiatan_penyedia_swakelola" int8,
"jumlah_paket_lelang" int8,
"jumlah_paket_lelang_selesai" int8,
"jumlah_paket_ekatalog" int8,
"pagu_paket_penyedia" numeric(22,2),
"pagu_kegiatan_swakelola" numeric(22,2),
"pagu_kegiatan_penyedia_swakelola" numeric(22,2),
"pagu_lelang" numeric(22,2),
"pagu_lelang_selesai" numeric(22,2),
"nilai_hasil_lelang" numeric(22,2),
"nilai_hps" numeric(22,2),
"pagu_ekatalog" numeric(22,2),
"update_date" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for dm_rekappengadaansatker
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."dm_rekappengadaansatker";
CREATE TABLE "dwh_procurement"."dm_rekappengadaansatker" (
"tahun_anggaran" int2,
"id_satker" varchar(500) COLLATE "default",
"nama_satuan_kerja" varchar(500) COLLATE "default",
"jenis_kldi" varchar(500) COLLATE "default",
"id_kldi" varchar(500) COLLATE "default",
"nama_kldi" varchar(500) COLLATE "default",
"nama_propinsi" varchar(100) COLLATE "default",
"nama_kabupaten" varchar(100) COLLATE "default",
"jumlah_paket_penyedia" int8,
"jumlah_kegiatan_swakelola" int8,
"jumlah_kegiatan_penyedia_swakelola" int8,
"jumlah_paket_lelang" int8,
"jumlah_paket_lelang_selesai" int8,
"jumlah_paket_ekatalog" int8,
"anggaran_sismontepra" numeric(22,2),
"pagu_paket_penyedia" numeric(22,2),
"pagu_kegiatan_swakelola" numeric(22,2),
"pagu_kegiatan_penyedia_swakelola" numeric(22,2),
"dekon_tp" numeric(22,2),
"anggaran_pegawai_penyedia" numeric(22,2),
"anggaran_pegawai_swakelola" numeric(22,2),
"pagu_lelang" numeric(22,2),
"pagu_lelang_selesai" numeric(22,2),
"nilai_hasil_lelang" numeric(22,2),
"nilai_hps" numeric(22,2),
"pagu_ekatalog" numeric(22,2),
"update_date" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for dm_rekappengadaansatker_lama
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."dm_rekappengadaansatker_lama";
CREATE TABLE "dwh_procurement"."dm_rekappengadaansatker_lama" (
"tahun_anggaran" int2,
"nama_kldi" varchar(500) COLLATE "default",
"jenis_kldi" varchar(500) COLLATE "default",
"id_satker" varchar(500) COLLATE "default",
"nama_satuan_kerja" varchar(500) COLLATE "default",
"nama_propinsi" varchar(100) COLLATE "default",
"nama_kabupaten" varchar(100) COLLATE "default",
"jumlah_paket_penyedia" int8,
"jumlah_kegiatan_swakelola" int8,
"jumlah_kegiatan_penyedia_swakelola" int8,
"anggaran_sirup" numeric(22,2),
"pagu_paket_penyedia" numeric(22,2),
"pagu_kegiatan_swakelola" numeric(22,2),
"pagu_kegiatan_penyedia_swakelola" numeric(22,2),
"dekon_tp" numeric(22,2),
"anggaran_pegawai_penyedia" numeric(22,2),
"anggaran_pegawai_swakelola" numeric(22,2),
"update_date" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for dm_riwayatlelang
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."dm_riwayatlelang";
CREATE TABLE "dwh_procurement"."dm_riwayatlelang" (
"repo_id" int4,
"pkt_id" numeric(19),
"tahun_anggaran" int2,
"nama_paket" text COLLATE "default",
"pkt_pagu" numeric(22,2),
"pkt_hps" numeric(22,2),
"tanggal_lelang_dibuat" timestamp(6),
"versi_lelang" int2,
"status_lelang" int2,
"alasan_lelang_diulang" text COLLATE "default",
"alasan_lelang_ditutup" text COLLATE "default",
"jumlah_peserta_lelang" int8,
"evaluasi0_lulus" int2,
"evaluasi1_lulus" int2,
"evaluasi2_lulus" int2,
"evaluasi3_lulus" int2,
"evaluasi4_lulus" int2,
"harga_terendah" numeric(22,2),
"harga_tertinggi" numeric(22,2),
"harga_hasil_lelang" numeric(22,2),
"harga_rata_rata" numeric(22,2),
"ada_pemenang" int2,
"nama_pemenang" text COLLATE "default",
"npwp_pemenang" text COLLATE "default",
"nama_satker" text COLLATE "default",
"nama_lpse" varchar(500) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"lelang_selesai" int2
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for epurchasing
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."epurchasing";
CREATE TABLE "dwh_procurement"."epurchasing" (
"tahun_anggaran" int2,
"kode_paket" numeric(19),
"konversi_harga_total" float8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for fraud_report_02
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."fraud_report_02";
CREATE TABLE "dwh_procurement"."fraud_report_02" (
"tahun_anggaran" int2,
"kode_kldi" varchar(20) COLLATE "default",
"kode_lpse" int4,
"nama_lpse" varchar(255) COLLATE "default",
"sumber_dana" text COLLATE "default",
"kode_paket" numeric(19),
"nama_paket" varchar(500) COLLATE "default",
"kode_paket_rup" numeric(19),
"nama_paket_rup" text COLLATE "default",
"kode_rekening" text COLLATE "default",
"versi_lelang" float8,
"kode_lelang" numeric(19),
"kode_peserta" numeric(19),
"kode_penyedia" numeric(19),
"nama_penyedia" varchar(100) COLLATE "default",
"npwp_penyedia" varchar(40) COLLATE "default",
"rkn_alamat" varchar(1000) COLLATE "default",
"rkn_email" varchar(1000) COLLATE "default",
"rkn_fax" varchar(50) COLLATE "default",
"rkn_mobile_phone" varchar(50) COLLATE "default",
"rkn_telepon" varchar(50) COLLATE "default",
"rkn_tgl_daftar" timestamp(6),
"rkn_tgl_setuju" timestamp(6),
"rkn_website" varchar(40) COLLATE "default",
"is_pemenang" int2,
"ev_admin" int4,
"ev_teknis" int4,
"ev_harga" int4,
"ev_kualifikasi" int4,
"nilai_pagu" numeric(52,2),
"harga_penawaran" float8,
"harga_terkoreksi" float8,
"nama_satker" varchar(200) COLLATE "default",
"nama_satker_rup" varchar(200) COLLATE "default",
"kls_id" varchar(2) COLLATE "default",
"klasifikasi_usaha" varchar(80) COLLATE "default",
"metode_pemilihan" text COLLATE "default",
"metode_kualifikasi" varchar(50) COLLATE "default",
"nilai_anggaran" float8,
"pkt_hps" numeric(20,2),
"tgl_umum_pemenang" timestamp(6),
"tgl_buka_dok_penawaran" timestamp(6),
"tgl_dok_lelang" timestamp(6),
"tgl_sppbj" timestamp(6),
"tgl_ttd_kontrak" timestamp(6),
"tgl_upload_dok_penawaran" timestamp(6),
"nama_kldi" varchar(255) COLLATE "default",
"jenis_kldi" varchar(255) COLLATE "default",
"nama_kabupaten" varchar(80) COLLATE "default",
"nama_propinsi" varchar(80) COLLATE "default",
"jenis_pengadaan" varchar(80) COLLATE "default",
"tgl_umum_pascakualifikasi" timestamp(6),
"tgl_umum_prakualifikasi" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for fraud_report_02a
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."fraud_report_02a";
CREATE TABLE "dwh_procurement"."fraud_report_02a" (
"tahun_anggaran" int2,
"kode_kldi" varchar(20) COLLATE "default",
"kode_lpse" int4,
"nama_lpse" varchar(255) COLLATE "default",
"sumber_dana" text COLLATE "default",
"kode_paket" numeric(19),
"nama_paket" varchar(500) COLLATE "default",
"kode_paket_rup" numeric(19),
"nama_paket_rup" text COLLATE "default",
"kode_rekening" text COLLATE "default",
"versi_lelang" float8,
"kode_lelang" numeric(19),
"kode_peserta" numeric(19),
"kode_penyedia" numeric(19),
"nama_penyedia" varchar(100) COLLATE "default",
"npwp_penyedia" varchar(40) COLLATE "default",
"rkn_alamat" varchar(1000) COLLATE "default",
"rkn_email" varchar(1000) COLLATE "default",
"rkn_fax" varchar(50) COLLATE "default",
"rkn_mobile_phone" varchar(50) COLLATE "default",
"rkn_telepon" varchar(50) COLLATE "default",
"rkn_tgl_daftar" timestamp(6),
"rkn_tgl_setuju" timestamp(6),
"rkn_website" varchar(40) COLLATE "default",
"is_pemenang" int2,
"ev_admin" int4,
"ev_teknis" int4,
"ev_harga" int4,
"ev_kualifikasi" int4,
"nilai_pagu" numeric(54,2),
"harga_penawaran" float8,
"harga_terkoreksi" float8,
"nama_satker" varchar(200) COLLATE "default",
"nama_satker_rup" varchar(200) COLLATE "default",
"kls_id" varchar(2) COLLATE "default",
"klasifikasi_usaha" varchar(80) COLLATE "default",
"metode_pemilihan" text COLLATE "default",
"metode_kualifikasi" varchar(50) COLLATE "default",
"nilai_anggaran" float8,
"pkt_hps" numeric(22,2),
"tgl_umum_pemenang" timestamp(6),
"tgl_buka_dok_penawaran" timestamp(6),
"tgl_dok_lelang" timestamp(6),
"tgl_sppbj" timestamp(6),
"tgl_ttd_kontrak" timestamp(6),
"tgl_upload_dok_penawaran" timestamp(6),
"nama_kldi" varchar(255) COLLATE "default",
"jenis_kldi" varchar(255) COLLATE "default",
"nama_kabupaten" varchar(80) COLLATE "default",
"nama_propinsi" varchar(80) COLLATE "default",
"jenis_pengadaan" varchar(80) COLLATE "default",
"tgl_umum_pascakualifikasi" timestamp(6),
"tgl_umum_prakualifikasi" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for jenis_pengadaan
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."jenis_pengadaan";
CREATE TABLE "dwh_procurement"."jenis_pengadaan" (
"id" int4 NOT NULL,
"jenis" varchar(40) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for master_pemilihan
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."master_pemilihan";
CREATE TABLE "dwh_procurement"."master_pemilihan" (
"no" int4 NOT NULL,
"mtd_pemilihan" varchar(30) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for monev_ng_report_01a
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."monev_ng_report_01a";
CREATE TABLE "dwh_procurement"."monev_ng_report_01a" (
"tahun_anggaran" int4,
"header1" varchar(50) COLLATE "default",
"header2" varchar(50) COLLATE "default",
"jenis_penyedia" varchar(50) COLLATE "default",
"systems" varchar(50) COLLATE "default",
"fields" varchar(255) COLLATE "default",
"tipe_kldi" varchar(255) COLLATE "default",
"kode_kldi" varchar(255) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"id" int4,
"paket" int4,
"nilai" numeric(22,2),
"dekon_flag" varchar(50) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for monev_ng_report_01a_satker
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."monev_ng_report_01a_satker";
CREATE TABLE "dwh_procurement"."monev_ng_report_01a_satker" (
"tahun_anggaran" int4,
"header1" varchar(50) COLLATE "default",
"header2" varchar(50) COLLATE "default",
"jenis_penyedia" varchar(50) COLLATE "default",
"systems" varchar(50) COLLATE "default",
"fields" varchar(255) COLLATE "default",
"tipe_kldi" varchar(255) COLLATE "default",
"kode_kldi" varchar(255) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"id" int4,
"paket" int4,
"nilai" numeric(22,2),
"dekon_flag" varchar(50) COLLATE "default",
"satker" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for monev_ng_report_01b
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."monev_ng_report_01b";
CREATE TABLE "dwh_procurement"."monev_ng_report_01b" (
"tahun_anggaran" int4,
"header1" varchar(50) COLLATE "default",
"header2" varchar(50) COLLATE "default",
"jenis_belanja" varchar(50) COLLATE "default",
"jenis_penyedia" varchar(50) COLLATE "default",
"systems" varchar(50) COLLATE "default",
"fields" varchar(255) COLLATE "default",
"tipe_kldi" varchar(255) COLLATE "default",
"kode_kldi" varchar(255) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"id" int4,
"paket" numeric(20),
"nilai" numeric(22,2)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for monev_ng_report_01b_satker
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."monev_ng_report_01b_satker";
CREATE TABLE "dwh_procurement"."monev_ng_report_01b_satker" (
"tahun_anggaran" int4,
"header1" varchar(50) COLLATE "default",
"header2" varchar(50) COLLATE "default",
"jenis_belanja" varchar(50) COLLATE "default",
"jenis_penyedia" varchar(50) COLLATE "default",
"systems" varchar(50) COLLATE "default",
"fields" varchar(255) COLLATE "default",
"tipe_kldi" varchar(255) COLLATE "default",
"kode_kldi" varchar(255) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"id" int4,
"paket" numeric(20),
"nilai" numeric(22,2),
"satker" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for monev_ng_report_01c
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."monev_ng_report_01c";
CREATE TABLE "dwh_procurement"."monev_ng_report_01c" (
"tahun_anggaran" int4,
"pusat_daerah" varchar(50) COLLATE "default",
"tanggal" timestamp(6),
"jenis_penyedia" varchar(50) COLLATE "default",
"jenis_swakelola" varchar(50) COLLATE "default",
"jenis_pengadaan" varchar(40) COLLATE "default",
"kelola_penyedia" text COLLATE "default",
"tipe_kldi" varchar(255) COLLATE "default",
"kode_kldi" varchar(255) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"paket" int4,
"nilai" numeric(22,2),
"tanggal_awal_pengadaan" timestamp(6),
"tanggal_akhir_pengadaan" timestamp(6),
"tanggal_awal_pekerjaan" timestamp(6),
"tanggal_akhir_pekerjaan" timestamp(6),
"satker" varchar(255) COLLATE "default",
"tanggal_pengumuman" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for monev_ng_report_02
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."monev_ng_report_02";
CREATE TABLE "dwh_procurement"."monev_ng_report_02" (
"id" int4,
"alamat" varchar(255) COLLATE "default",
"additional_address" varchar(255) COLLATE "default",
"alasan_terdaftar" int4,
"correspondence_email" varchar(255) COLLATE "default",
"correspondence_id" varchar(255) COLLATE "default",
"correspondence_name" varchar(255) COLLATE "default",
"correspondence_phone" varchar(255) COLLATE "default",
"description" text COLLATE "default",
"direktur" varchar(255) COLLATE "default",
"instansi" varchar(255) COLLATE "default",
"kab_kota" int4,
"keterangan" varchar(65535) COLLATE "default",
"kode_lelang" numeric(19),
"nama_jenis" text COLLATE "default",
"nama_penyedia" varchar(255) COLLATE "default",
"npwp" varchar(255) COLLATE "default",
"propinsi_id" int4,
"kabupaten_penetapan" varchar(80) COLLATE "default",
"propinsi_penetapan" varchar(80) COLLATE "default",
"kabupaten_domisili" varchar(80) COLLATE "default",
"propinsi_domisili" varchar(80) COLLATE "default",
"created_at" timestamp(6),
"deleted_at" timestamp(6),
"start_date" timestamp(6),
"expired_date" timestamp(6),
"tgl_umum_pemenang" timestamp(6),
"violation" float8,
"is_pemenang" int2,
"tahun_anggaran" varchar(20) COLLATE "default",
"nama_paket" varchar(500) COLLATE "default",
"kode_rup" numeric(19),
"nama_kldi" varchar(255) COLLATE "default",
"nama_satker" varchar(200) COLLATE "default",
"jenis_pengadaan" varchar(80) COLLATE "default",
"metode_pemilihan" text COLLATE "default",
"nilai_pagu" float8,
"nilai_hps" numeric(22,2),
"tgl_lelang_dibuat" timestamp(6),
"publish" varchar(25) COLLATE "default",
"publish_date" timestamp(6),
"sk" varchar(255) COLLATE "default",
"update_at_nama_jenis" timestamp(6),
"hps_inaproc" int8,
"pagu_inaproc" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for monev_ng_report_03
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."monev_ng_report_03";
CREATE TABLE "dwh_procurement"."monev_ng_report_03" (
"id" int4,
"alamat" varchar(255) COLLATE "default",
"additional_address" varchar(255) COLLATE "default",
"alasan_terdaftar" int4,
"correspondence_email" varchar(255) COLLATE "default",
"correspondence_id" varchar(255) COLLATE "default",
"correspondence_name" varchar(255) COLLATE "default",
"correspondence_phone" varchar(255) COLLATE "default",
"direktur" varchar(255) COLLATE "default",
"instansi" varchar(255) COLLATE "default",
"kab_kota" int4,
"keterangan" varchar(65535) COLLATE "default",
"kode_lelang" numeric(19),
"nama_penyedia" varchar(255) COLLATE "default",
"npwp" varchar(255) COLLATE "default",
"propinsi_id" int4,
"kabupaten_penetapan" varchar(80) COLLATE "default",
"propinsi_penetapan" varchar(80) COLLATE "default",
"kabupaten_domisili" varchar(80) COLLATE "default",
"propinsi_domisili" varchar(80) COLLATE "default",
"created_at" timestamp(6),
"deleted_at" timestamp(6),
"start_date" timestamp(6),
"expired_date" timestamp(6),
"tgl_umum_pemenang" timestamp(6),
"violation" float8,
"is_pemenang" int2,
"tahun_anggaran" varchar(20) COLLATE "default",
"nama_paket" varchar(500) COLLATE "default",
"kode_rup" numeric(19),
"nama_kldi" varchar(255) COLLATE "default",
"nama_satker" varchar(200) COLLATE "default",
"jenis_pengadaan" varchar(80) COLLATE "default",
"metode_pemilihan" text COLLATE "default",
"tgl_lelang_dibuat" timestamp(6),
"publish" varchar(25) COLLATE "default",
"description" text COLLATE "default",
"nama_jenis" text COLLATE "default",
"publish_date" timestamp(6),
"sk" varchar(255) COLLATE "default",
"update_at_nama_jenis" timestamp(6),
"hps_inaproc" int8,
"pagu_inaproc" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for monev_ng_report_04
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."monev_ng_report_04";
CREATE TABLE "dwh_procurement"."monev_ng_report_04" (
"kode_kldi" varchar(6) COLLATE "default",
"nama_kldi" varchar(256) COLLATE "default",
"jenis_kldi" varchar(256) COLLATE "default",
"kode_satker" int8,
"nama_satker" varchar(200) COLLATE "default",
"komoditas_id" int8,
"nama_komoditas" varchar(100) COLLATE "default",
"sub_komoditas" varchar(300) COLLATE "default",
"sub_kategori" varchar(300) COLLATE "default",
"no_produk" varchar(200) COLLATE "default",
"nama_produk" varchar(200) COLLATE "default",
"konversi_harga_total" numeric(22,2),
"nilai_paket" numeric(22,2),
"nama_penyedia" varchar(100) COLLATE "default",
"status_paket" varchar(100) COLLATE "default",
"kuantitas" numeric(25,5),
"no_paket" varchar(50) COLLATE "default",
"rup_id" int8,
"nama_kabupaten" varchar(80) COLLATE "default",
"nama_provinsi" varchar(80) COLLATE "default",
"nama_paket" varchar(1000) COLLATE "default",
"id_produk" int4,
"npwp_penyedia" varchar(100) COLLATE "default",
"tanggal_buat_paket" timestamp(6),
"tanggal_selesai" timestamp(6),
"tanggal_awal_pengadaan" timestamp(6),
"active" int2,
"apakah_berlaku" int2,
"tgl_masa_berlaku_selesai" timestamp(6),
"paket_id" int8,
"tahun_anggaran" int4,
"jenis_pengadaan" varchar(40) COLLATE "default",
"tanggal_akhir_pengadaan" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for monev_ng_report_06
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."monev_ng_report_06";
CREATE TABLE "dwh_procurement"."monev_ng_report_06" (
"tahun_anggaran" int4,
"title" varchar(50) COLLATE "default",
"tanggal" timestamp(6),
"tipe_kldi" varchar(255) COLLATE "default",
"kode_kldi" varchar(255) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"paket" int4,
"nilai" numeric(22,2),
"nama_satker" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for proc_indicator
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."proc_indicator";
CREATE TABLE "dwh_procurement"."proc_indicator" (
"tahun_anggaran" int2,
"kode_paket" numeric(19),
"kode_paket_rup" numeric(19),
"kode_lelang" numeric(19),
"kode_kldi" text COLLATE "default",
"nama_lpse" varchar(255) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"jenis_kldi" varchar(255) COLLATE "default",
"nama_satker" varchar(200) COLLATE "default",
"nama_agency" varchar(80) COLLATE "default",
"nama_kabupaten" varchar(80) COLLATE "default",
"nama_propinsi" varchar(80) COLLATE "default",
"sumber_dana" text COLLATE "default",
"jenis_pengadaan" varchar(80) COLLATE "default",
"metode_pemilihan" text COLLATE "default",
"jenis_belanja" text COLLATE "default",
"metode_kualifikasi" varchar(50) COLLATE "default",
"nama_paket" text COLLATE "default",
"versi_lelang" int2,
"kode_rekening" text COLLATE "default",
"kode_lpse" int4,
"nilai_pagu" numeric(20,2),
"nilai_hps" numeric(20,2),
"nilai_anggaran" float8,
"tgl_lelang_dibuat" timestamp(6),
"tgl_pengumuman" timestamp(6),
"tgl_umum_prakualifikasi" timestamp(6),
"tgl_umum_pascakualifikasi" timestamp(6),
"tgl_sppbj" timestamp(6),
"tgl_ttd_kontrak" timestamp(6),
"tgl_buka_dok_penawaran" timestamp(6),
"tgl_rencana_awal_pengadaan" timestamp(6),
"tgl_rencana_akhir_pengadaan" timestamp(6),
"tgl_umum_pemenang" timestamp(6),
"psr_harga" float8,
"psr_harga_terkoreksi" float8,
"tgl_dok_lelang" timestamp(6),
"tgl_upload_dok_penawaran" timestamp(6),
"jml_peserta" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for proc_indicator2
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."proc_indicator2";
CREATE TABLE "dwh_procurement"."proc_indicator2" (
"tahun" int4,
"no_paket" varchar(50) COLLATE "default",
"rup_id" int8,
"tipe_kldi" varchar(256) COLLATE "default",
"kode_kldi" varchar(6) COLLATE "default",
"nama_satuan_kerja" varchar(200) COLLATE "default",
"nama_kldi" varchar(256) COLLATE "default",
"nama_komoditas" varchar(100) COLLATE "default",
"nama_kabupaten" varchar(80) COLLATE "default",
"nama_provinsi" varchar(80) COLLATE "default",
"nama_paket" varchar(1000) COLLATE "default",
"nilai_paket" numeric(22,2),
"nama_penyedia" varchar(100) COLLATE "default",
"npwp_penyedia" varchar(100) COLLATE "default",
"tanggal_buat_paket" timestamp(6),
"tanggal_selesai" timestamp(6),
"tanggal_awal_pengadaan" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for same_address
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."same_address";
CREATE TABLE "dwh_procurement"."same_address" (
"rkn_nama_ref" varchar(255) COLLATE "default",
"rkn_nama" varchar(255) COLLATE "default",
"rkn_npwp" varchar(50) COLLATE "default",
"rkn_id" numeric(19),
"rkn_email" varchar(1000) COLLATE "default",
"rkn_telepon" varchar(50) COLLATE "default",
"rkn_alamat" varchar(4000) COLLATE "default",
"nama_similarity" float8,
"rkn_alamat_sim" varchar(4000) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for same_email
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."same_email";
CREATE TABLE "dwh_procurement"."same_email" (
"rkn_nama_ref" varchar(255) COLLATE "default",
"rkn_nama" varchar(255) COLLATE "default",
"rkn_npwp" varchar(50) COLLATE "default",
"rkn_id" numeric(19),
"rkn_email" varchar(1000) COLLATE "default",
"rkn_alamat" varchar(4000) COLLATE "default",
"nama_similarity" float8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for same_npwp
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."same_npwp";
CREATE TABLE "dwh_procurement"."same_npwp" (
"rkn_nama" varchar(255) COLLATE "default",
"rkn_npwp" varchar(50) COLLATE "default",
"rkn_id" numeric(19),
"src" int4,
"nama_similarity" float8,
"rkn_nama_ref" varchar(255) COLLATE "default",
"rkn_email" varchar(1000) COLLATE "default",
"rkn_alamat" varchar(4000) COLLATE "default",
"rkn_telepon" varchar(50) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for same_phone
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."same_phone";
CREATE TABLE "dwh_procurement"."same_phone" (
"rkn_nama_ref" varchar(255) COLLATE "default",
"rkn_nama" varchar(255) COLLATE "default",
"rkn_npwp" varchar(50) COLLATE "default",
"rkn_id" numeric(19),
"rkn_email" varchar(1000) COLLATE "default",
"rkn_alamat" varchar(4000) COLLATE "default",
"rkn_telepon" varchar(50) COLLATE "default",
"nama_similarity" float8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for similar_email
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."similar_email";
CREATE TABLE "dwh_procurement"."similar_email" (
"src" int4,
"rkn_id" numeric(19),
"rkn_id_1" numeric(19),
"rkn_npwp" varchar(50) COLLATE "default",
"rkn_npwp_1" varchar(50) COLLATE "default",
"rkn_email" varchar(1000) COLLATE "default",
"rkn_email_1" varchar(1000) COLLATE "default",
"email_similarity" float8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for similar_email_uq
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."similar_email_uq";
CREATE TABLE "dwh_procurement"."similar_email_uq" (
"src" int4,
"rkn_id" numeric(19),
"rkn_id_1" numeric(19),
"rkn_npwp" varchar(50) COLLATE "default",
"rkn_npwp_1" varchar(50) COLLATE "default",
"rkn_email" varchar(1000) COLLATE "default",
"rkn_email_1" varchar(1000) COLLATE "default",
"email_similarity" float8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for similar_name
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."similar_name";
CREATE TABLE "dwh_procurement"."similar_name" (
"src" int4,
"rkn_id" numeric(19),
"rkn_id_1" numeric(19),
"rkn_npwp" varchar(50) COLLATE "default",
"rkn_npwp_1" varchar(50) COLLATE "default",
"rkn_nama" varchar(100) COLLATE "default",
"rkn_nama_1" varchar(100) COLLATE "default",
"nama_similarity" float8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for similar_name_uniq
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."similar_name_uniq";
CREATE TABLE "dwh_procurement"."similar_name_uniq" (
"src" int4,
"rkn_id" numeric(19),
"rkn_id_1" numeric(19),
"rkn_npwp" varchar(50) COLLATE "default",
"rkn_npwp_1" varchar(50) COLLATE "default",
"rkn_nama" varchar(100) COLLATE "default",
"rkn_nama_1" varchar(100) COLLATE "default",
"nama_similarity" float8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for similar_name_uq
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."similar_name_uq";
CREATE TABLE "dwh_procurement"."similar_name_uq" (
"src" int4,
"rkn_id" numeric(19),
"rkn_id_1" numeric(19),
"rkn_npwp" varchar(50) COLLATE "default",
"rkn_npwp_1" varchar(50) COLLATE "default",
"rkn_nama" varchar(100) COLLATE "default",
"rkn_nama_1" varchar(100) COLLATE "default",
"nama_similarity" float8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for similar_phone
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."similar_phone";
CREATE TABLE "dwh_procurement"."similar_phone" (
"src" int4,
"rkn_id" numeric(19),
"rkn_id_1" numeric(19),
"rkn_npwp" varchar(50) COLLATE "default",
"rkn_npwp_1" varchar(50) COLLATE "default",
"rkn_telepon" varchar(50) COLLATE "default",
"rkn_telepon_1" varchar(50) COLLATE "default",
"phone_similarity" float8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for similar_phone_uq
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."similar_phone_uq";
CREATE TABLE "dwh_procurement"."similar_phone_uq" (
"src" int4,
"rkn_id" numeric(19),
"rkn_id_1" numeric(19),
"rkn_npwp" varchar(50) COLLATE "default",
"rkn_npwp_1" varchar(50) COLLATE "default",
"rkn_telepon" varchar(50) COLLATE "default",
"rkn_telepon_1" varchar(50) COLLATE "default",
"phone_similarity" float8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------

-- ----------------------------
-- Indexes structure for table fraud_report_02
-- ----------------------------
CREATE INDEX "idx_fraud_report_02_1" ON "dwh_procurement"."fraud_report_02" USING btree ("nama_kldi", "nama_propinsi");
CREATE INDEX "idx_fraud_report_02_2" ON "dwh_procurement"."fraud_report_02" USING btree ("nama_kldi");
CREATE INDEX "idx_fraud_report_02_3" ON "dwh_procurement"."fraud_report_02" USING btree ("nama_penyedia");

-- ----------------------------
-- Primary Key structure for table master_pemilihan
-- ----------------------------
ALTER TABLE "dwh_procurement"."master_pemilihan" ADD PRIMARY KEY ("no");
