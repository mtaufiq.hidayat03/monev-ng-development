CREATE SEQUENCE "public"."procurement_profiles_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."procurement_profiles" (
  "id" bigint NOT NULL DEFAULT nextval('procurement_profiles_id_seq'::regclass),
  "title" character varying(255),
  "kldi_id" character varying(30),
  "status" character varying(20),
  "mined_at" timestamp(6),
  "adjusted_by" integer,
  "adjusted_at" timestamp(6),
  "created_by" integer,
  "created_at" timestamp(6),
  "updated_by" integer,
  "updated_at" timestamp(6),
  "deleted_by" integer,
  "deleted_at" timestamp(6)
)
;
ALTER TABLE "public"."procurement_profiles" ADD CONSTRAINT "procurement_profiles_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."procurement_data_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."procurement_data" (
  "id" bigint NOT NULL DEFAULT nextval('procurement_data_id_seq'::regclass),
  "procurement_id" bigint NOT NULL,
  "header" character varying(250),
  "flag" INT NOT NULL,
  "value" NUMERIC NULL,
  "pagu" NUMERIC NULL,
  "value_text" TEXT NULL,
  "extra" TEXT NULL,
  "section" character varying(250),
  "created_by" integer,
  "created_at" timestamp(6)
)
;
ALTER TABLE "public"."procurement_data" ADD CONSTRAINT "procurement_data_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."procurement_data_original_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."procurement_data_original" (
  "id" bigint NOT NULL DEFAULT nextval('procurement_data_original_id_seq'::regclass),
  "header" character varying(250),
  "value" NUMERIC NULL,
  "pagu" NUMERIC NULL,
  "value_text" TEXT NULL,
  "created_by" integer,
  "created_at" timestamp(6)
)
;
ALTER TABLE "public"."procurement_data_original" ADD CONSTRAINT "procurement_data_original_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."rekap_pengadaan_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."rekap_pengadaan" (
  "id" bigint NOT NULL DEFAULT nextval('rekap_pengadaan_id_seq'::regclass),
  "tahun_anggaran" integer NOT NULL,
  "jenis_belanja" character varying(50),
  "jenis_pengadaan" character varying(50),
  "metode_pemilihan" character varying(50),
  "sumber_dana" character varying(50),
  "tipe_kldi" character varying(50),
  "kode_kldi" character varying(50),
  "nama_kldi" character varying(255),
  "kode_satker" character varying(50),
  "nama_satker" character varying(255),
  "total_anggaran" bigint NOT NULL,
  "belanja_pengadaan" bigint NOT NULL,
  "rencana_pagu" bigint NOT NULL,
  "rencana_paket" integer NOT NULL,
  "pelaksanaan_pagu" bigint NOT NULL,
  "pelaksanaan_paket" integer NOT NULL,
  "hasil_paket" integer NOT NULL,
  "hasil_pagu" bigint NOT NULL,
  "kontrak" bigint NOT NULL,
  "pembayaran" bigint NOT NULL
);
ALTER TABLE "public"."rekap_pengadaan" ADD CONSTRAINT "rekap_pengadaan_pkey" PRIMARY KEY ("id");
CREATE INDEX ON "public"."rekap_pengadaan" ((lower(sumber_dana)));
CREATE INDEX ON "public"."rekap_pengadaan" ((lower(tipe_kldi)));
CREATE INDEX ON "public"."rekap_pengadaan" ((lower(nama_kldi)));
CREATE INDEX ON "public"."rekap_pengadaan" ((lower(nama_satker)));
CREATE INDEX ON "public"."rekap_pengadaan" (kode_kldi);
CREATE INDEX ON "public"."rekap_pengadaan" (kode_satker);
CREATE INDEX ON "public"."rekap_pengadaan" (jenis_belanja);
CREATE INDEX ON "public"."rekap_pengadaan" (jenis_pengadaan);
CREATE INDEX ON "public"."rekap_pengadaan" (metode_pemilihan);

CREATE SEQUENCE "public"."rekap_pengadaan_nasional_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."rekap_pengadaan_nasional" (
  "id" bigint NOT NULL DEFAULT nextval('rekap_pengadaan_nasional_id_seq'::regclass),
  "tahun_anggaran" integer NOT NULL,
  "total_anggaran" bigint NOT NULL,
  "belanja_pengadaan" bigint NOT NULL,
  "rencana_pagu" bigint NOT NULL,
  "rencana_paket" integer NOT NULL,
  "pelaksanaan_pagu" bigint NOT NULL,
  "pelaksanaan_paket" integer NOT NULL,
  "hasil_paket" integer NOT NULL,
  "hasil_pagu" bigint NOT NULL,
  "kontrak" bigint NOT NULL,
  "pembayaran" bigint NOT NULL
);
ALTER TABLE "public"."rekap_pengadaan_nasional" ADD CONSTRAINT "rekap_pengadaan_nasional_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."rekap_pengadaan_nasional_original_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."rekap_pengadaan_nasional_original" (
  "id" bigint NOT NULL DEFAULT nextval('rekap_pengadaan_nasional_original_id_seq'::regclass),
  "tahun_anggaran" integer NOT NULL,
  "total_anggaran" bigint NOT NULL,
  "belanja_pengadaan" bigint NOT NULL,
  "rencana_pagu" bigint NOT NULL,
  "rencana_paket" integer NOT NULL,
  "pelaksanaan_pagu" bigint NOT NULL,
  "pelaksanaan_paket" integer NOT NULL,
  "hasil_paket" integer NOT NULL,
  "hasil_pagu" bigint NOT NULL,
  "kontrak" bigint NOT NULL,
  "pembayaran" bigint NOT NULL
);
ALTER TABLE "public"."rekap_pengadaan_nasional_original" ADD CONSTRAINT "rekap_pengadaan_nasional_original_pkey" PRIMARY KEY ("id");

ALTER TABLE "public"."rekap_pengadaan_nasional" ADD COLUMN tanggal_update timestamp(6);
ALTER TABLE "public"."rekap_pengadaan_nasional_original" ADD COLUMN tanggal_update timestamp(6);

ALTER TABLE "public"."rekap_pengadaan_nasional" rename COLUMN tahun_anggaran to tahun;
ALTER TABLE "public"."rekap_pengadaan_nasional_original" rename COLUMN tahun_anggaran to tahun;

CREATE SEQUENCE "public"."rekap_ukpbj_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."rekap_ukpbj" (
  "id" bigint NOT NULL DEFAULT nextval('rekap_ukpbj_id_seq'::regclass),
  "nama" character varying(255),
  "manajemen_pengadaan" character varying(10),
  "manajemen_penyedia" character varying(10),
  "manajemen_kinerja" character varying(10),
  "manajemen_resiko" character varying(10),
  "organisasi" character varying(10),
  "tugas_fungsi" character varying(10),
  "perencanaan_sdm_pengadaan" character varying(10),
  "pengembangan_sdm_pengadaan" character varying(10),
  "sistem_informasi" character varying(10),
  "kelembagaan" character varying(20)
);
ALTER TABLE "public"."rekap_ukpbj" ADD CONSTRAINT "rekap_ukpbj_pkey" PRIMARY KEY ("id");
ALTER TABLE "public"."rekap_ukpbj" ADD COLUMN kematangan integer not null;
ALTER TABLE "public"."rekap_ukpbj" ADD COLUMN kode_kldi character varying(50) not null;
ALTER TABLE "public"."rekap_ukpbj" ADD COLUMN nama_kldi character varying(255) not null;

CREATE SEQUENCE "public"."rekap_advokasi_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."rekap_advokasi" (
  "id" bigint NOT NULL DEFAULT nextval('rekap_advokasi_id_seq'::regclass),
  "jumlah_advokasi" integer not null,
  "nilai_advokasi" bigint not null,
  "jumlah_kasus_pengadaan" integer not null,
  "nilai_kasus_pengadaan" bigint not null,
  "jumlah_kasus_ahli" integer not null,
  "nilai_kasus_ahli" bigint not null,
  "jumlah_permasalahan_kontrak" integer not null,
  "nilai_permasalahan_kontrak" bigint not null,
  pengaduan_konstruksi numeric not null,
  pengaduan_barang numeric not null,
  pengaduan_konsultasi numeric not null,
  pengaduan_lainnya numeric not null,
  tanggal_update timestamp(6),
  tahun integer not null
);
ALTER TABLE "public"."rekap_advokasi" ADD CONSTRAINT "rekap_advokasi_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."rekap_lpse_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."rekap_lpse" (
  "id" bigint NOT NULL DEFAULT nextval('rekap_lpse_id_seq'::regclass),
  jumlah integer not null,
  jumlah_standar_lengkap integer not null,
  tanggal_update timestamp(6)
);
ALTER TABLE "public"."rekap_lpse" ADD CONSTRAINT "rekap_lpse_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."detail_lpse_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."detail_lpse" (
"id" int8 DEFAULT nextval('detail_lpse_id_seq'::regclass) NOT NULL,
"nama" varchar(255) COLLATE "default",
"jumlah_standar" int4 NOT NULL,
"versi_spse" varchar(20) COLLATE "default",
"longitude" double precision,
"latitude" double precision,
"alamat" text COLLATE "default",
"website" text COLLATE "default"
)
WITH (OIDS=FALSE)

;
ALTER TABLE "public"."detail_lpse" ADD CONSTRAINT "detail_lpse_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."rekap_perencanaan_bulan_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."rekap_perencanaan_bulan" (
  "id" bigint NOT NULL DEFAULT nextval('rekap_perencanaan_bulan_id_seq'::regclass),
  "bulan" timestamp(6),
  rencana_pagu bigint not null
);
ALTER TABLE "public"."rekap_perencanaan_bulan" ADD CONSTRAINT "rekap_perencanaan_bulan_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."rekap_penyedia_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."rekap_penyedia" (
  "id" bigint NOT NULL DEFAULT nextval('rekap_penyedia_id_seq'::regclass),
  rekanan_id bigint not null,
  nama character varying (255),
  npwp character varying (100),
  alamat text,
  propinsi character varying (100),
  kabupaten character varying (100),
  kontrak_menang bigint not null default 0,
  paket_menang bigint not null default 0,
  paket_menawar bigint not null default 0,
  paket_prakualifikasi bigint not null default 0,
  paket_daftar bigint not null default 0
);
ALTER TABLE "public"."rekap_penyedia" ADD CONSTRAINT "rekap_penyedia_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."paket_penyedia_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."paket_penyedia" (
  "id" bigint NOT NULL DEFAULT nextval('paket_penyedia_id_seq'::regclass),
  rekanan_id bigint not null,
  "id_paket" bigint NOT NULL,
  "id_lelang" bigint NOT NULL,
  nama_paket character varying (255),
  pagu bigint not null default 0,
  status character varying (100),
  "kualifikasi_penyedia" character varying(100)
);
ALTER TABLE "public"."paket_penyedia" ADD CONSTRAINT "paket_penyedia_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."rekap_perencanaan_metode_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."rekap_perencanaan_metode" (
  "id" bigint NOT NULL DEFAULT nextval('rekap_perencanaan_metode_id_seq'::regclass),
  "paket_konsolidasi" integer NOT NULL,
  "pagu_konsolidasi" bigint NOT NULL,
  "paket_umkm" integer NOT NULL,
  "pagu_umkm" bigint NOT NULL,
  "paket_pdn" integer NOT NULL,
  "pagu_pdn" bigint NOT NULL,
  tanggal_update timestamp(6),
  tahun integer
);
ALTER TABLE "public"."rekap_perencanaan_metode" ADD CONSTRAINT "rekap_perencanaan_metode_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."rekap_perencanaan_metode_detail_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."rekap_perencanaan_metode_detail" (
  "id" bigint NOT NULL DEFAULT nextval('rekap_perencanaan_metode_detail_id_seq'::regclass),
  "kode_kldi" character varying(50),
  "nama_kldi" character varying(255),
  "kode_satker" character varying(50),
  "nama_satker" character varying(255),
  "paket_konsolidasi" integer NOT NULL,
  "pagu_konsolidasi" bigint NOT NULL,
  "paket_umkm" integer NOT NULL,
  "pagu_umkm" bigint NOT NULL,
  "paket_pdn" integer NOT NULL,
  "pagu_pdn" bigint NOT NULL,
  tanggal_update timestamp(6),
  tahun integer
);
ALTER TABLE "public"."rekap_perencanaan_metode_detail" ADD CONSTRAINT "rekap_perencanaan_metode_detail_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."rekap_pemilihan_metode_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."rekap_pemilihan_metode" (
  "id" bigint NOT NULL DEFAULT nextval('rekap_pemilihan_metode_id_seq'::regclass),
  "paket_konsolidasi" integer NOT NULL,
  "pagu_konsolidasi" bigint NOT NULL,
  "paket_umkm" integer NOT NULL,
  "pagu_umkm" bigint NOT NULL,
  "paket_pdn" integer NOT NULL,
  "pagu_pdn" bigint NOT NULL,
  "paket_itemize" integer NOT NULL,
  "pagu_itemize" bigint NOT NULL,
  "paket_ulang" integer NOT NULL,
  "pagu_ulang" bigint NOT NULL,
  "paket_gagal" integer NOT NULL,
  "pagu_gagal" bigint NOT NULL,
  "paket_lelang_cepat" integer NOT NULL,
  "pagu_lelang_cepat" bigint NOT NULL,
  "paket_reverse_auction" integer NOT NULL,
  "pagu_reverse_auction" bigint NOT NULL,
  tanggal_update timestamp(6),
  tahun integer
);
ALTER TABLE "public"."rekap_pemilihan_metode" ADD CONSTRAINT "rekap_pemilihan_metode_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."rekap_pemilihan_metode_detail_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."rekap_pemilihan_metode_detail" (
  "id" bigint NOT NULL DEFAULT nextval('rekap_pemilihan_metode_detail_id_seq'::regclass),
  "kode_kldi" character varying(50),
  "nama_kldi" character varying(255),
  "kode_satker" character varying(50),
  "nama_satker" character varying(255),
  "paket_konsolidasi" integer NOT NULL,
  "pagu_konsolidasi" bigint NOT NULL,
  "paket_umkm" integer NOT NULL,
  "pagu_umkm" bigint NOT NULL,
  "paket_pdn" integer NOT NULL,
  "pagu_pdn" bigint NOT NULL,
  "paket_itemize" integer NOT NULL,
  "pagu_itemize" bigint NOT NULL,
  "paket_ulang" integer NOT NULL,
  "pagu_ulang" bigint NOT NULL,
  "paket_gagal" integer NOT NULL,
  "pagu_gagal" bigint NOT NULL,
  "paket_reverse_auction" integer NOT NULL,
  "pagu_reverse_auction" bigint NOT NULL,
  tanggal_update timestamp(6),
  tahun integer
);
ALTER TABLE "public"."rekap_pemilihan_metode_detail" ADD CONSTRAINT "rekap_pemilihan_metode_detail_pkey" PRIMARY KEY ("id");



CREATE SEQUENCE "public"."jenis_kontrak_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for jenis_kontrak
-- ----------------------------
CREATE TABLE "public"."jenis_kontrak" (
  "id" int8 NOT NULL DEFAULT nextval('jenis_kontrak_id_seq'::regclass),
  "kode_kldi" varchar(50) COLLATE "pg_catalog"."default",
  "nama_kldi" varchar(255) COLLATE "pg_catalog"."default",
  "kode_satker" varchar(50) COLLATE "pg_catalog"."default",
  "nama_satker" varchar(255) COLLATE "pg_catalog"."default",
  "total_anggaran" int8 NOT NULL,
  "belanja_pengadaan" int8 NOT NULL,
  "pelaksanaan_pagu" int8 NOT NULL,
  "pelaksanaan_paket" int4 NOT NULL,
  "hasil_paket" int4 NOT NULL,
  "hasil_pagu" int8 NOT NULL,
  "jenis_kontrak" int4,
  "tahun_anggaran" int4
)
;

-- ----------------------------
-- Primary Key structure for table jenis_kontrak
-- ----------------------------
ALTER TABLE "public"."jenis_kontrak" ADD CONSTRAINT "jenis_kontrak_pkey" PRIMARY KEY ("id");


CREATE SEQUENCE "public"."kualifikasi_usaha_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for kualifikasi_usaha
-- ----------------------------
CREATE TABLE "public"."kualifikasi_usaha" (
  "id" int8 NOT NULL DEFAULT nextval('kualifikasi_usaha_id_seq'::regclass),
  "kode_kldi" varchar(50) COLLATE "pg_catalog"."default",
  "nama_kldi" varchar(255) COLLATE "pg_catalog"."default",
  "kode_satker" varchar(50) COLLATE "pg_catalog"."default",
  "nama_satker" varchar(255) COLLATE "pg_catalog"."default",
  "total_anggaran" int8 NOT NULL,
  "belanja_pengadaan" int8 NOT NULL,
  "pelaksanaan_pagu" int8 NOT NULL,
  "pelaksanaan_paket" int4 NOT NULL,
  "hasil_paket" int4 NOT NULL,
  "hasil_pagu" int8 NOT NULL,
  "kualifikasi_usaha" varchar(100) COLLATE "pg_catalog"."default",
  "tahun" int4
)
;

-- ----------------------------
-- Primary Key structure for table kualifikasi_usaha
-- ----------------------------
ALTER TABLE "public"."kualifikasi_usaha" ADD CONSTRAINT "kualifikasi_usaha_pkey" PRIMARY KEY ("id");


CREATE SEQUENCE "public"."pembayaran_amel_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for pembayaran_amel
-- ----------------------------
CREATE TABLE "public"."pembayaran_amel" (
  "id" int8 NOT NULL DEFAULT nextval('pembayaran_amel_id_seq'::regclass),
  "kode_satker" varchar(50) COLLATE "pg_catalog"."default",
  "jenis_pengadaan" varchar(255) COLLATE "pg_catalog"."default",
  "pembayaran" int8,
  "tahun" int4
)
;

-- ----------------------------
-- Primary Key structure for table pembayaran_amel
-- ----------------------------
ALTER TABLE "public"."pembayaran_amel" ADD CONSTRAINT "pembayaran_amel_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Sequence for data_penyedia_nasional
-- ----------------------------
CREATE SEQUENCE "public"."data_penyedia_nasional_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for data_penyedia_nasional
-- ----------------------------
CREATE TABLE "public"."data_penyedia_nasional" (
  "id" int8 NOT NULL DEFAULT nextval('data_penyedia_nasional_id_seq'::regclass),
  "terverifikasi" int4 NOT NULL,
  "teragregasi" int4 NOT NULL,
  "terdaftar_sikap" int4 NOT NULL,
  "aktif" int4 NOT NULL,
  "lolos_pra" int4 NOT NULL,
  "menang" int4 NOT NULL,
  tanggal_update timestamp(6),
  tahun integer
)
;

-- ----------------------------
-- Primary Key structure for table data_penyedia_nasional
-- ----------------------------
ALTER TABLE "public"."data_penyedia_nasional" ADD CONSTRAINT "data_penyedia_nasional_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Sequence for inovasi_pengadaan
-- ----------------------------
CREATE SEQUENCE "public"."inovasi_pengadaan_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for inovasi_pengadaan
-- ----------------------------
CREATE TABLE "public"."inovasi_pengadaan" (
  "id" int8 NOT NULL DEFAULT nextval('inovasi_pengadaan_id_seq'::regclass),
  "pagu_dini" bigint NOT NULL,
  "paket_dini" integer NOT NULL,
  "pagu_konsolidasi" bigint NOT NULL,
  "paket_konsolidasi" integer NOT NULL,
  "pagu_itemize" bigint NOT NULL,
  "paket_itemize" integer NOT NULL,
  "pagu_lelang_cepat" bigint NOT NULL,
  "paket_lelang_cepat" integer NOT NULL,
  "pagu_reverse_auction" bigint NOT NULL,
  "paket_reverse_auction" integer NOT NULL,
  tanggal_update timestamp(6),
  tahun integer
)
;

-- ----------------------------
-- Primary Key structure for table inovasi_pengadaan
-- ----------------------------
ALTER TABLE "public"."inovasi_pengadaan" ADD CONSTRAINT "inovasi_pengadaan_pkey" PRIMARY KEY ("id");


CREATE SEQUENCE "public"."rekap_serah_terima_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."rekap_serah_terima" (
  "id" bigint NOT NULL DEFAULT nextval('rekap_serah_terima_id_seq'::regclass),
  "tahun_anggaran" integer NOT NULL,
  "jenis_belanja" character varying(50),
  "jenis_pengadaan" character varying(50),
  "metode_pemilihan" character varying(50),
  "sumber_dana" character varying(50),
  "tipe_kldi" character varying(50),
  "kode_kldi" character varying(50),
  "nama_kldi" character varying(255),
  "kode_satker" character varying(50),
  "nama_satker" character varying(255),
  "paket" integer NOT NULL,
  "pagu" bigint NOT NULL
);
ALTER TABLE "public"."rekap_serah_terima" ADD CONSTRAINT "rekap_serah_terima_pkey" PRIMARY KEY ("id");
CREATE INDEX ON "public"."rekap_serah_terima" ((lower(sumber_dana)));
CREATE INDEX ON "public"."rekap_serah_terima" ((lower(tipe_kldi)));
CREATE INDEX ON "public"."rekap_serah_terima" ((lower(nama_kldi)));
CREATE INDEX ON "public"."rekap_serah_terima" ((lower(nama_satker)));
CREATE INDEX ON "public"."rekap_serah_terima" (kode_kldi);
CREATE INDEX ON "public"."rekap_serah_terima" (kode_satker);
CREATE INDEX ON "public"."rekap_serah_terima" (jenis_belanja);
CREATE INDEX ON "public"."rekap_serah_terima" (jenis_pengadaan);
CREATE INDEX ON "public"."rekap_serah_terima" (metode_pemilihan);

-- ----------------------------
-- Table structure for adjsutment advokasi original and summary
-- ----------------------------

CREATE SEQUENCE "public"."adjustment_advokasi_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."adjustment_advokasi" (
"id" int8 NOT NULL DEFAULT nextval('adjustment_advokasi_id_seq'::regclass),
"layanan_advokasi" int4,
"kasus_pengadaan" int4,
"permasalahan_dilayani" int4,
"tanggal_update" timestamp(6),
"tahun" int4
);

ALTER TABLE "public"."adjustment_advokasi" ADD CONSTRAINT "adjustment_advokasi_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."adjustment_advokasi_original_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."adjustment_advokasi_original" (
"id" int8 NOT NULL DEFAULT nextval('adjustment_advokasi_original_id_seq'::regclass),
"layanan_advokasi" int4,
"kasus_pengadaan" int4,
"permasalahan_dilayani" int4,
"tanggal_update" timestamp(6),
"tahun" int4
);

ALTER TABLE "public"."adjustment_advokasi_original" ADD CONSTRAINT "adjustment_advokasi_original_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Table structure for adjustment ukpbj original and summary
-- ----------------------------

CREATE SEQUENCE "public"."adjustment_ukpbj_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."adjustment_ukpbj" (
"id" int8 NOT NULL DEFAULT nextval('adjustment_ukpbj_id_seq'::regclass),
"struktural" int4,
"adhoc" int4,
"belum_terbentuk" int4,
"sembilan_kematangan" int4,
"tanggal_update" timestamp(6),
"tahun" int4
);

ALTER TABLE "public"."adjustment_ukpbj" ADD CONSTRAINT "adjustment_ukpbj_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."adjustment_ukpbj_original_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."adjustment_ukpbj_original" (
"id" int8 NOT NULL DEFAULT nextval('adjustment_ukpbj_original_id_seq'::regclass),
"struktural" int4,
"adhoc" int4,
"belum_terbentuk" int4,
"sembilan_kematangan" int4,
"tanggal_update" timestamp(6),
"tahun" int4
);

ALTER TABLE "public"."adjustment_ukpbj_original" ADD CONSTRAINT "adjustment_ukpbj_original_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Table structure for adjustment lpse original and summary
-- ----------------------------

CREATE SEQUENCE "public"."adjustment_lpse_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."adjustment_lpse" (
"id" int8 NOT NULL DEFAULT nextval('adjustment_lpse_id_seq'::regclass),
"total" int4,
"jumlah_highlight_lpse" int4,
"standar" int4,
"tanggal_update" timestamp(6),
"tahun" int4
);

ALTER TABLE "public"."adjustment_lpse" ADD CONSTRAINT "adjustment_lpse_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."adjustment_lpse_original_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."adjustment_lpse_original" (
"id" int8 NOT NULL DEFAULT nextval('adjustment_lpse_original_id_seq'::regclass),
"total" int4,
"jumlah_highlight_lpse" int4,
"standar" int4,
"tanggal_update" timestamp(6),
"tahun" int4
);

ALTER TABLE "public"."adjustment_lpse_original" ADD CONSTRAINT "adjustment_lpse_original_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Table structure for adjustment data penyedia original and summary
-- ----------------------------

CREATE SEQUENCE "public"."adjustment_data_penyedia_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."adjustment_data_penyedia" (
"id" int8 NOT NULL DEFAULT nextval('adjustment_data_penyedia_id_seq'::regclass),
"terverifikasi" int8,
"terdaftar_sikap" int8,
"aktif" int8,
"menang" int8,
"tanggal_update" timestamp(6),
"tahun" int4
);

ALTER TABLE "public"."adjustment_data_penyedia" ADD CONSTRAINT "adjustment_data_penyedia_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."adjustment_data_penyedia_original_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."adjustment_data_penyedia_original" (
"id" int8 NOT NULL DEFAULT nextval('adjustment_data_penyedia_original_id_seq'::regclass),
"terverifikasi" int8,
"terdaftar_sikap" int8,
"aktif" int8,
"menang" int8,
"tanggal_update" timestamp(6),
"tahun" int4
);

ALTER TABLE "public"."adjustment_data_penyedia_original" ADD CONSTRAINT "adjustment_data_penyedia_original_pkey" PRIMARY KEY ("id");


-- ----------------------------
-- Table structure for adjustment inovasi pengadaan original and summary
-- ----------------------------

CREATE SEQUENCE "public"."adjustment_inovasi_pengadaan_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."adjustment_inovasi_pengadaan" (
"id" int8 NOT NULL DEFAULT nextval('adjustment_inovasi_pengadaan_id_seq'::regclass),
"lelang_dini" float8,
"konsolidasi" float8,
"tender_cepat" float8,
"kontrak_payung" float8,
"tender_itemized" float8,
"tanggal_update" timestamp(6),
"tahun" int4
);

ALTER TABLE "public"."adjustment_inovasi_pengadaan" ADD CONSTRAINT "adjustment_inovasi_pengadaan_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."adjustment_inovasi_pengadaan_original_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."adjustment_inovasi_pengadaan_original" (
"id" int8 NOT NULL DEFAULT nextval('adjustment_inovasi_pengadaan_original_id_seq'::regclass),
"lelang_dini" float8,
"konsolidasi" float8,
"tender_cepat" float8,
"kontrak_payung" float8,
"tender_itemized" float8,
"tanggal_update" timestamp(6),
"tahun" int4
);

ALTER TABLE "public"."adjustment_inovasi_pengadaan_original" ADD CONSTRAINT "adjustment_inovasi_pengadaan_original_pkey" PRIMARY KEY ("id");


-- ----------------------------
-- Table structure for adjustment pdn umkm original and summary
-- ----------------------------

CREATE SEQUENCE "public"."adjustment_pdn_umkm_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."adjustment_pdn_umkm" (
"id" int8 NOT NULL DEFAULT nextval('adjustment_pdn_umkm_id_seq'::regclass),
"pdn" float8,
"pagu_umkm" float8,
"tanggal_update" timestamp(6),
"tahun" int4
);

ALTER TABLE "public"."adjustment_pdn_umkm" ADD CONSTRAINT "adjustment_pdn_umkm_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."adjustment_pdn_umkm_original_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."adjustment_pdn_umkm_original" (
"id" int8 NOT NULL DEFAULT nextval('adjustment_pdn_umkm_original_id_seq'::regclass),
"pdn" float8,
"pagu_umkm" float8,
"tanggal_update" timestamp(6),
"tahun" int4
);

ALTER TABLE "public"."adjustment_pdn_umkm_original" ADD CONSTRAINT "adjustment_pdn_umkm_original_pkey" PRIMARY KEY ("id");


CREATE SEQUENCE "public"."pmm_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."pmm" (
    "id" bigint NOT NULL DEFAULT nextval('pmm_id_seq'::regclass),
    "kode_kldi" character varying(50),
    "nama_kldi" character varying(255),
    "tahun" int4,
    "bulan" int4,
    "jumlah_tepat_waktu" NUMERIC NULL,
    "jumlah_total" NUMERIC NULL,
    "nilai_tepat_waktu" NUMERIC NULL,
    "nilai_total" NUMERIC NULL,
    "pagu_pemilihan" NUMERIC NULL,
    "hasil_pemilihan" NUMERIC NULL
);

ALTER TABLE "public"."pmm" ADD CONSTRAINT "pmm_pkey" PRIMARY KEY ("id");

ALTER TABLE "public"."pembayaran_amel" ADD COLUMN created_at timestamp(6);
ALTER TABLE "public"."pembayaran_amel" ADD COLUMN source_repo numeric;

CREATE TABLE "public"."rekap_pengadaan_nasional_detail" (
"total_anggaran" int8 NOT NULL,
"belanja_pengadaan" int8 NOT NULL,
"rencana_pagu" int8 NOT NULL,
"rencana_paket" int8 NOT NULL,
"pelaksanaan_pagu" int8 NOT NULL,
"pelaksanaan_paket" int8 NOT NULL,
"hasil_paket" int8 NOT NULL,
"hasil_pagu" int8 NOT NULL,
"kontrak" int8 NOT NULL,
"pembayaran" int8 NOT NULL,
"kode_kldi" varchar(255) COLLATE "default",
"nama_kldi" text COLLATE "default",
"jenis_kldi" varchar(255) COLLATE "default",
"jenis_pemerintahan" varchar(255) COLLATE "default",
"tahun_anggaran" int4
)
WITH (OIDS=FALSE)

;