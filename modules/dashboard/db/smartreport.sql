CREATE SCHEMA IF NOT EXISTS smartreport;

CREATE SEQUENCE smartreport.e_tendering_summary_id_seq
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

create table smartreport.e_tendering_summary (
  id bigint NOT NULL DEFAULT nextval('smartreport.e_tendering_summary_id_seq'::regclass),
  tahun integer NOT NULL,
  jumlah_lelang integer,
  pagu_lelang double precision,
  jumlah_lelang_selesai integer,
  pagu_lelang_selesai double precision,
  nilai_hasil_lelang double precision,
  selisih_pagu_hasil_lelang double precision,
  selisih_hps_hasil_lelang double precision,
  jumlah_ppk integer,
  jumlah_panitia integer,
  penyedia_terblacklist integer,
  penyedia_terdaftar integer,
  penyedia_tertolak integer,
  penyedia_terverifikasi integer,
  penyedia_terkoreksi integer
);

ALTER TABLE smartreport.e_tendering_summary ADD CONSTRAINT "smartreport.e_tendering_summary_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE smartreport.e_tendering_detail_id_seq
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

create table smartreport.e_tendering_detail (
  id bigint NOT NULL DEFAULT nextval('smartreport.e_tendering_detail_id_seq'::regclass),
  nama_lpse character varying (255),
  kode_lpse numeric,
  tahun integer NOT NULL,
  jumlah_lelang integer,
  pagu_lelang double precision,
  jumlah_lelang_selesai integer,
  pagu_lelang_selesai double precision,
  nilai_hasil_lelang double precision,
  selisih_pagu_hasil_lelang double precision,
  selisih_hps_hasil_lelang double precision,
  jumlah_ppk integer,
  jumlah_panitia integer,
  penyedia_terblacklist integer,
  penyedia_terdaftar integer,
  penyedia_tertolak integer,
  penyedia_terverifikasi integer,
  penyedia_terkoreksi integer
);

ALTER TABLE smartreport.e_tendering_detail ADD CONSTRAINT "smartreport.e_tendering_detail_pkey" PRIMARY KEY ("id");


alter table smartreport.e_tendering_summary
  add column selisih_pagu_hasil_persen double precision,
  add column selisih_hps_hasil_persen double precision;

alter table smartreport.e_tendering_detail
  add column selisih_pagu_hasil_persen double precision,
  add column selisih_hps_hasil_persen double precision;


CREATE SEQUENCE smartreport.e_purchasing_id_seq
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

create table smartreport.e_purchasing (
  id bigint NOT NULL DEFAULT nextval('smartreport.e_purchasing_id_seq'::regclass),
  tahun integer NOT NULL,
  jumlah_paket integer,
  nilai_transaksi double precision,
  jumlah_kategori_komoditas integer,
  jumlah_penyedia integer,
  jumlah_produk integer
);

ALTER TABLE smartreport.e_purchasing ADD CONSTRAINT "smartreport.e_purchasing_pkey" PRIMARY KEY ("id");


CREATE SEQUENCE smartreport.non_e_tendering_summary_id_seq
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

create table smartreport.non_e_tendering_summary (
  id bigint NOT NULL DEFAULT nextval('smartreport.non_e_tendering_summary_id_seq'::regclass),
  tahun integer NOT NULL,
  jumlah_paket integer,
  nilai_pagu double precision,
  nilai_transaksi double precision
);

ALTER TABLE smartreport.non_e_tendering_summary ADD CONSTRAINT "smartreport.non_e_tendering_summary_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE smartreport.non_e_tendering_detail_id_seq
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

create table smartreport.non_e_tendering_detail (
  id bigint NOT NULL DEFAULT nextval('smartreport.non_e_tendering_detail_id_seq'::regclass),
  nama_lpse character varying (255),
  kode_lpse numeric,
  tahun integer NOT NULL,
  jumlah_paket integer,
  nilai_pagu double precision,
  nilai_transaksi double precision
);

ALTER TABLE smartreport.non_e_tendering_detail ADD CONSTRAINT "smartreport.non_e_tendering_detail_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE smartreport.e_tendering_original_summary_id_seq
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

create table smartreport.e_tendering_original_summary (
  id bigint NOT NULL DEFAULT nextval('smartreport.e_tendering_original_summary_id_seq'::regclass),
  tahun integer NOT NULL,
  jumlah_lelang integer,
  pagu_lelang double precision,
  jumlah_lelang_selesai integer,
  pagu_lelang_selesai double precision,
  nilai_hasil_lelang double precision,
  selisih_pagu_hasil_lelang double precision,
  selisih_hps_hasil_lelang double precision,
  jumlah_ppk integer,
  jumlah_panitia integer,
  penyedia_terblacklist integer,
  penyedia_terdaftar integer,
  penyedia_tertolak integer,
  penyedia_terverifikasi integer,
  penyedia_terkoreksi integer
);

ALTER TABLE smartreport.e_tendering_original_summary ADD CONSTRAINT "smartreport.e_tendering_original_summary_pkey" PRIMARY KEY ("id");

alter table smartreport.e_tendering_original_summary
  add column selisih_pagu_hasil_persen double precision,
  add column selisih_hps_hasil_persen double precision;

ALTER TABLE smartreport.e_tendering_summary ADD COLUMN tanggal_update timestamp(6);
ALTER TABLE smartreport.e_tendering_original_summary ADD COLUMN tanggal_update timestamp(6);

CREATE SEQUENCE smartreport.e_purchasing_original_id_seq
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

create table smartreport.e_purchasing_original (
  id bigint NOT NULL DEFAULT nextval('smartreport.e_purchasing_original_id_seq'::regclass),
  tahun integer NOT NULL,
  jumlah_paket integer,
  nilai_transaksi double precision,
  jumlah_kategori_komoditas integer,
  jumlah_penyedia integer,
  jumlah_produk integer
);

ALTER TABLE smartreport.e_purchasing_original ADD CONSTRAINT "smartreport.e_purchasing_original_pkey" PRIMARY KEY ("id");

ALTER TABLE smartreport.e_purchasing ADD COLUMN tanggal_update timestamp(6);
ALTER TABLE smartreport.e_purchasing_original ADD COLUMN tanggal_update timestamp(6);

CREATE SEQUENCE smartreport.non_e_tendering_original_summary_id_seq
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

create table smartreport.non_e_tendering_original_summary (
  id bigint NOT NULL DEFAULT nextval('smartreport.non_e_tendering_original_summary_id_seq'::regclass),
  tahun integer NOT NULL,
  jumlah_paket integer,
  nilai_pagu double precision,
  nilai_transaksi double precision
);

ALTER TABLE smartreport.non_e_tendering_original_summary ADD CONSTRAINT "smartreport.non_e_tendering_original_summary_pkey" PRIMARY KEY ("id");

ALTER TABLE smartreport.non_e_tendering_summary ADD COLUMN tanggal_update timestamp(6);
ALTER TABLE smartreport.non_e_tendering_original_summary ADD COLUMN tanggal_update timestamp(6);


CREATE SEQUENCE "smartreport"."e_purchasing_detail_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for e_purchasing
-- ----------------------------
CREATE TABLE "smartreport"."e_purchasing_detail" (
  "id" int8 NOT NULL DEFAULT nextval('"smartreport".e_purchasing_detail_id_seq'::regclass),
  "tahun" int4 NOT NULL,
  "kode_kldi" character varying(50),
  "nama_kldi" character varying(255),
  "kode_satker" character varying(50),
  "nama_satker" character varying(255),
  "jumlah_paket" int4,
  "nilai_transaksi" float8,
  "jumlah_kategori_komoditas" int4,
  "jumlah_penyedia" int4,
  "jumlah_produk" int4
)
;

-- ----------------------------
-- Primary Key structure for table e_purchasing
-- ----------------------------
ALTER TABLE "smartreport"."e_purchasing_detail" ADD CONSTRAINT "smartreport.e_purchasing_detail_pkey" PRIMARY KEY ("id");
