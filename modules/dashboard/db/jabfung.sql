--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6 (Debian 10.6-1.pgdg80+1)
-- Dumped by pg_dump version 10.11 (Ubuntu 10.11-1.pgdg16.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'SQL_ASCII';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: jabfung; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA jabfung;


ALTER SCHEMA jabfung OWNER TO postgres;

--
-- Name: adjustment_jabfung_ppbj_id_seq; Type: SEQUENCE; Schema: jabfung; Owner: postgres
--

CREATE SEQUENCE jabfung.adjustment_jabfung_ppbj_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE jabfung.adjustment_jabfung_ppbj_id_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: adjustment_jabfung_ppbj; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.adjustment_jabfung_ppbj (
    id bigint DEFAULT nextval('jabfung.adjustment_jabfung_ppbj_id_seq'::regclass) NOT NULL,
    pertama integer,
    muda integer,
    madya integer,
    tanggal_update timestamp(6) without time zone,
    tahun integer
);


ALTER TABLE jabfung.adjustment_jabfung_ppbj OWNER TO postgres;

--
-- Name: adjustment_jabfung_ppbj_original_id_seq; Type: SEQUENCE; Schema: jabfung; Owner: postgres
--

CREATE SEQUENCE jabfung.adjustment_jabfung_ppbj_original_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE jabfung.adjustment_jabfung_ppbj_original_id_seq OWNER TO postgres;

--
-- Name: adjustment_jabfung_ppbj_original; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.adjustment_jabfung_ppbj_original (
    id bigint DEFAULT nextval('jabfung.adjustment_jabfung_ppbj_original_id_seq'::regclass) NOT NULL,
    pertama integer,
    muda integer,
    madya integer,
    tanggal_update timestamp(6) without time zone,
    tahun integer
);


ALTER TABLE jabfung.adjustment_jabfung_ppbj_original OWNER TO postgres;

--
-- Name: adjustment_pemegang_sertifikat_kompetensi_id_seq; Type: SEQUENCE; Schema: jabfung; Owner: postgres
--

CREATE SEQUENCE jabfung.adjustment_pemegang_sertifikat_kompetensi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE jabfung.adjustment_pemegang_sertifikat_kompetensi_id_seq OWNER TO postgres;

--
-- Name: adjustment_pemegang_sertifikat_kompetensi; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.adjustment_pemegang_sertifikat_kompetensi (
    id bigint DEFAULT nextval('jabfung.adjustment_pemegang_sertifikat_kompetensi_id_seq'::regclass) NOT NULL,
    muda integer,
    madya integer,
    pertama integer,
    tanggal_update timestamp(6) without time zone,
    tahun integer
);


ALTER TABLE jabfung.adjustment_pemegang_sertifikat_kompetensi OWNER TO postgres;

--
-- Name: adjustment_pemegang_sertifikat_kompetensi_original_id_seq; Type: SEQUENCE; Schema: jabfung; Owner: postgres
--

CREATE SEQUENCE jabfung.adjustment_pemegang_sertifikat_kompetensi_original_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE jabfung.adjustment_pemegang_sertifikat_kompetensi_original_id_seq OWNER TO postgres;

--
-- Name: adjustment_pemegang_sertifikat_kompetensi_original; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.adjustment_pemegang_sertifikat_kompetensi_original (
    id bigint DEFAULT nextval('jabfung.adjustment_pemegang_sertifikat_kompetensi_original_id_seq'::regclass) NOT NULL,
    muda integer,
    madya integer,
    pertama integer,
    tanggal_update timestamp(6) without time zone,
    tahun integer
);


ALTER TABLE jabfung.adjustment_pemegang_sertifikat_kompetensi_original OWNER TO postgres;

--
-- Name: adjustment_pemegang_sertifikat_okupasi_id_seq; Type: SEQUENCE; Schema: jabfung; Owner: postgres
--

CREATE SEQUENCE jabfung.adjustment_pemegang_sertifikat_okupasi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE jabfung.adjustment_pemegang_sertifikat_okupasi_id_seq OWNER TO postgres;

--
-- Name: adjustment_pemegang_sertifikat_okupasi; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.adjustment_pemegang_sertifikat_okupasi (
    id bigint DEFAULT nextval('jabfung.adjustment_pemegang_sertifikat_okupasi_id_seq'::regclass) NOT NULL,
    pokja_pemilihan integer,
    pejabat_pengadaan integer,
    pejabat_pembuat_komitmen integer,
    tanggal_update timestamp(6) without time zone,
    tahun integer
);


ALTER TABLE jabfung.adjustment_pemegang_sertifikat_okupasi OWNER TO postgres;

--
-- Name: adjustment_pemegang_sertifikat_okupasi_original_id_seq; Type: SEQUENCE; Schema: jabfung; Owner: postgres
--

CREATE SEQUENCE jabfung.adjustment_pemegang_sertifikat_okupasi_original_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE jabfung.adjustment_pemegang_sertifikat_okupasi_original_id_seq OWNER TO postgres;

--
-- Name: adjustment_pemegang_sertifikat_okupasi_original; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.adjustment_pemegang_sertifikat_okupasi_original (
    id bigint DEFAULT nextval('jabfung.adjustment_pemegang_sertifikat_okupasi_original_id_seq'::regclass) NOT NULL,
    pokja_pemilihan integer,
    pejabat_pengadaan integer,
    pejabat_pembuat_komitmen integer,
    tanggal_update timestamp(6) without time zone,
    tahun integer
);


ALTER TABLE jabfung.adjustment_pemegang_sertifikat_okupasi_original OWNER TO postgres;

--
-- Name: adjustment_sertifikat_dasar_id_seq; Type: SEQUENCE; Schema: jabfung; Owner: postgres
--

CREATE SEQUENCE jabfung.adjustment_sertifikat_dasar_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE jabfung.adjustment_sertifikat_dasar_id_seq OWNER TO postgres;

--
-- Name: adjustment_sertifikat_dasar; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.adjustment_sertifikat_dasar (
    id bigint DEFAULT nextval('jabfung.adjustment_sertifikat_dasar_id_seq'::regclass) NOT NULL,
    total integer,
    tanggal_update timestamp(6) without time zone,
    tahun integer
);


ALTER TABLE jabfung.adjustment_sertifikat_dasar OWNER TO postgres;

--
-- Name: adjustment_sertifikat_dasar_original_id_seq; Type: SEQUENCE; Schema: jabfung; Owner: postgres
--

CREATE SEQUENCE jabfung.adjustment_sertifikat_dasar_original_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE jabfung.adjustment_sertifikat_dasar_original_id_seq OWNER TO postgres;

--
-- Name: adjustment_sertifikat_dasar_original; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.adjustment_sertifikat_dasar_original (
    id bigint DEFAULT nextval('jabfung.adjustment_sertifikat_dasar_original_id_seq'::regclass) NOT NULL,
    total integer,
    tanggal_update timestamp(6) without time zone,
    tahun integer
);


ALTER TABLE jabfung.adjustment_sertifikat_dasar_original OWNER TO postgres;

--
-- Name: jabfung_pbj; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.jabfung_pbj (
    id integer NOT NULL,
    certificate_number character varying(255),
    name character varying(255),
    nip character varying(255),
    institution_id character varying(255),
    institution_name character varying(255),
    satker_id character varying(255),
    satker_name character varying(255),
    issued_date timestamp(6) without time zone,
    level character varying(255),
    rank character varying(255)
);


ALTER TABLE jabfung.jabfung_pbj OWNER TO postgres;

--
-- Name: jabfung_pbj_lama; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.jabfung_pbj_lama (
    id integer,
    nama character varying(255),
    jenjang character varying(255),
    satker character varying(255),
    pangkat_gol character varying(255),
    issued_date timestamp(6) without time zone
);


ALTER TABLE jabfung.jabfung_pbj_lama OWNER TO postgres;

--
-- Name: pemegang_sertifikat; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.pemegang_sertifikat (
    id integer NOT NULL,
    certificate_number character varying(255),
    name character varying(255),
    nip character varying(255),
    institution_id character varying(255),
    institution_name character varying(255),
    satker_id character varying(255),
    satker_name character varying(255),
    issued_date timestamp(6) without time zone,
    rank character varying(255)
);


ALTER TABLE jabfung.pemegang_sertifikat OWNER TO postgres;

--
-- Name: pemegang_sertifikat_lama; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.pemegang_sertifikat_lama (
    id integer,
    certificate_number character varying(255),
    name character varying(255),
    nip character varying(255),
    institution character varying(255),
    issued_date timestamp(6) without time zone,
    jenjang character varying(255)
);


ALTER TABLE jabfung.pemegang_sertifikat_lama OWNER TO postgres;

--
-- Name: sertifikat_kompetensi; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.sertifikat_kompetensi (
    id integer,
    certificate_number character varying(255),
    name character varying(255),
    nip character varying(255),
    institution_id character varying(255),
    institution_name character varying(255),
    satker_id character varying(255),
    satker_name character varying(255),
    issued_date timestamp(6) without time zone,
    level character varying(255),
    rank character varying(255)
);


ALTER TABLE jabfung.sertifikat_kompetensi OWNER TO postgres;

--
-- Name: sertifikat_okupasi; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.sertifikat_okupasi (
    id integer NOT NULL,
    certificate_number character varying(255),
    name character varying(255),
    nip character varying(255),
    institution_id character varying(255),
    institution_name character varying(255),
    satker_id character varying(255),
    satker_name character varying(255),
    issued_date timestamp(6) without time zone,
    type character varying(255),
    rank character varying(255)
);


ALTER TABLE jabfung.sertifikat_okupasi OWNER TO postgres;

--
-- Name: sertifikat_okupasi_lama; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.sertifikat_okupasi_lama (
    id integer,
    certificate_number character varying(255),
    name character varying(255),
    nip character varying(255),
    institution_id character varying(255),
    institution_name character varying(255),
    satker_id character varying(255),
    satker_name character varying(255),
    issued_date timestamp(6) without time zone,
    type character varying(255)
);


ALTER TABLE jabfung.sertifikat_okupasi_lama OWNER TO postgres;

--
-- Name: adjustment_jabfung_ppbj_original adjustment_jabfung_ppbj_original_pkey; Type: CONSTRAINT; Schema: jabfung; Owner: postgres
--

ALTER TABLE ONLY jabfung.adjustment_jabfung_ppbj_original
    ADD CONSTRAINT adjustment_jabfung_ppbj_original_pkey PRIMARY KEY (id);


--
-- Name: adjustment_jabfung_ppbj adjustment_jabfung_ppbj_pkey; Type: CONSTRAINT; Schema: jabfung; Owner: postgres
--

ALTER TABLE ONLY jabfung.adjustment_jabfung_ppbj
    ADD CONSTRAINT adjustment_jabfung_ppbj_pkey PRIMARY KEY (id);


--
-- Name: adjustment_pemegang_sertifikat_kompetensi_original adjustment_pemegang_sertifikat_kompetensi_original_pkey; Type: CONSTRAINT; Schema: jabfung; Owner: postgres
--

ALTER TABLE ONLY jabfung.adjustment_pemegang_sertifikat_kompetensi_original
    ADD CONSTRAINT adjustment_pemegang_sertifikat_kompetensi_original_pkey PRIMARY KEY (id);


--
-- Name: adjustment_pemegang_sertifikat_kompetensi adjustment_pemegang_sertifikat_kompetensi_pkey; Type: CONSTRAINT; Schema: jabfung; Owner: postgres
--

ALTER TABLE ONLY jabfung.adjustment_pemegang_sertifikat_kompetensi
    ADD CONSTRAINT adjustment_pemegang_sertifikat_kompetensi_pkey PRIMARY KEY (id);


--
-- Name: adjustment_pemegang_sertifikat_okupasi_original adjustment_pemegang_sertifikat_okupasi_original_pkey; Type: CONSTRAINT; Schema: jabfung; Owner: postgres
--

ALTER TABLE ONLY jabfung.adjustment_pemegang_sertifikat_okupasi_original
    ADD CONSTRAINT adjustment_pemegang_sertifikat_okupasi_original_pkey PRIMARY KEY (id);


--
-- Name: adjustment_pemegang_sertifikat_okupasi adjustment_pemegang_sertifikat_okupasi_pkey; Type: CONSTRAINT; Schema: jabfung; Owner: postgres
--

ALTER TABLE ONLY jabfung.adjustment_pemegang_sertifikat_okupasi
    ADD CONSTRAINT adjustment_pemegang_sertifikat_okupasi_pkey PRIMARY KEY (id);


--
-- Name: adjustment_sertifikat_dasar_original adjustment_sertifikat_dasar_original_pkey; Type: CONSTRAINT; Schema: jabfung; Owner: postgres
--

ALTER TABLE ONLY jabfung.adjustment_sertifikat_dasar_original
    ADD CONSTRAINT adjustment_sertifikat_dasar_original_pkey PRIMARY KEY (id);


--
-- Name: adjustment_sertifikat_dasar adjustment_sertifikat_dasar_pkey; Type: CONSTRAINT; Schema: jabfung; Owner: postgres
--

ALTER TABLE ONLY jabfung.adjustment_sertifikat_dasar
    ADD CONSTRAINT adjustment_sertifikat_dasar_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

