package utils;

import com.google.gson.reflect.TypeToken;
import play.cache.Cache;
import play.db.jdbc.Query;
import utils.common.JsonUtil;

import java.util.List;

public class DashboardLOVHelper {

    private static final String TAG = "DashboardLOVHelper";

    public static List<String> listJenisBelanja() {

        String lovJson = Cache.get("jenis_belanja_list", String.class);
        if (lovJson == null || lovJson.length() < 4) { // null or empty json
            List<String> lov = Query.findList("select distinct jenis_belanja from rekap_pengadaan", String.class);
            Cache.set("jenis_belanja_list", JsonUtil.toJson(lov), "24h");
            return lov;
        }

        return JsonUtil.fromJson(lovJson, new TypeToken<List<String>>() {
        }.getType());
    }

    public static List<String> listJenisPengadaan() {

        String lovJson = Cache.get("jenis_pengadaan_list", String.class);
        if (lovJson == null || lovJson.length() < 4) { // null or empty json
            List<String> lov = Query.findList("select distinct jenis_pengadaan from rekap_pengadaan", String.class);
            Cache.set("jenis_pengadaan_list", JsonUtil.toJson(lov), "24h");
            return lov;
        }
        return JsonUtil.fromJson(lovJson, new TypeToken<List<String>>() {
        }.getType());
    }

    public static List<String> listMetodePemilihan() {

        String lovJson = Cache.get("metode_pemilihan_list", String.class);
        if (lovJson == null || lovJson.length() < 4) { // null or empty json
            List<String> lov = Query.findList("select distinct metode_pemilihan from rekap_pengadaan", String.class);
            Cache.set("metode_pemilihan_list", JsonUtil.toJson(lov), "24h");
            return lov;
        }

        return JsonUtil.fromJson(lovJson, new TypeToken<List<String>>() {
        }.getType());
    }

    public static List<String> listTahunAnggaran() {

        String lovJson = Cache.get("tahun_anggaran_list", String.class);
        if (lovJson == null || lovJson.length() < 4) { // null or empty json
            List<String> lov = Query.findList("select distinct tahun_anggaran from rekap_pengadaan_nasional_detail", String.class);
            Cache.set("tahun_anggaran_list", JsonUtil.toJson(lov), "24h");
            return lov;
        }

        return JsonUtil.fromJson(lovJson, new TypeToken<List<String>>() {
        }.getType());
    }
}
