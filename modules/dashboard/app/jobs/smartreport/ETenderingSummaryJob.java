package jobs.smartreport;

import play.jobs.Job;
import play.jobs.On;
import repositories.smartreport.EPurchasingRepository;
import repositories.smartreport.ETenderingSummaryRepository;

import java.time.Year;

@On("*/20 * * * * ?")
public class ETenderingSummaryJob extends Job {

    @Override
    public void doJob() throws Exception {
        super.doJob();
        int thisYear = Year.now().getValue();
        ETenderingSummaryRepository.getInstance().createNewYearData(thisYear);
        EPurchasingRepository.getInstance().createNewYearData(thisYear);
    }
}
