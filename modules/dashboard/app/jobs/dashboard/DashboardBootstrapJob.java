package jobs.dashboard;

import play.cache.Cache;
import play.db.jdbc.Query;
import play.jobs.Job;
import play.jobs.OnApplicationStart;
import utils.common.JsonUtil;

import java.util.List;

@OnApplicationStart
public class DashboardBootstrapJob extends Job {
    @Override
    public void execute() throws Exception {
        super.execute();
        List<String> jenisBelanjaList =  Query.findList(
                "select distinct jenis_belanja from rekap_pengadaan",
                String.class
        );
        List<String> jenisPengadaanList =  Query.findList(
                "select distinct jenis_pengadaan from rekap_pengadaan",
                String.class
        );
        List<String> metodePemilihanList =  Query.findList(
                "select distinct metode_pemilihan from rekap_pengadaan",
                String.class
        );
        List<String> tahunAnggaranList =  Query.findList(
                "select distinct tahun_anggaran from rekap_pengadaan",
                String.class
        );
        Cache.set("jenis_belanja_list", JsonUtil.toJson(jenisBelanjaList), "24h");
        Cache.set("jenis_pengadaan_list", JsonUtil.toJson(jenisPengadaanList), "24h");
        Cache.set("metode_pemilihan_list", JsonUtil.toJson(metodePemilihanList), "24h");
        Cache.set("tahun_anggaran_list", JsonUtil.toJson(tahunAnggaranList), "24h");
    }
}
