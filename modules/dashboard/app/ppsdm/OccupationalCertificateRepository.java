package ppsdm;

import models.common.contracts.Chartable;
import models.dashboard.SearchQuery;
import org.joda.time.LocalDate;
import permission.Kldi;
import play.db.jdbc.Query;
import ppsdm.models.OccupationalCertificate;
import ppsdm.pojos.OccupationalCertificateSearchResult;
import ppsdm.pojos.OccupationalMeta;
import utils.common.ChartBuilder;
import utils.common.DateFormatter;
import utils.common.LogUtil;
import utils.common.QueryUtil;

import java.sql.ResultSet;
import java.time.Year;
import java.util.List;

public class OccupationalCertificateRepository {

    public static final String TAG = "CertificateHolderRepository";

    public Long getTotalBy(String institutionId, String type) {
        final Long total = OccupationalCertificate.count("institution_id = ? AND type = ?", institutionId, type);
        return total;
    }

    public Long getTotalThisYearBy(Kldi kldi, String type) {
        final Long total = OccupationalCertificate.count(
                "institution_id = ? AND type = ? AND EXTRACT(YEAR FROM issued_date) = ?",
                kldi.getKldi(),
                type,
                Year.now().getValue()
        );
        return total;
    }

    public OccupationalMeta getTotalAll() {
        return new OccupationalMeta();
    }

    public Long getTotalByType(String type) {
        LogUtil.debug(TAG, "get total by type: " + type);
        final Long result = OccupationalCertificate.count("type = ? ", type);
        LogUtil.debug(TAG,  result);
        return result;
    }

    private static final OccupationalCertificateRepository repository = new OccupationalCertificateRepository();

    public static OccupationalCertificateRepository getInstance() {
        return repository;
    }

    public OccupationalCertificateSearchResult levelOne(SearchQuery model) {
        OccupationalCertificateSearchResult result = new OccupationalCertificateSearchResult(model);
        StringBuilder sb = new StringBuilder();
        sb.append(
                "SELECT j.institution_id, j.institution_name,j.satker_name, " +
                        "    COALESCE(k.total, 0) as pokja_pemilihan, " +
                        "    COALESCE(m.total, 0) as pejabat_pengadaan, " +
                        "    COALESCE(mm.total, 0) as pejabat_pembuat_komitmen " +
                        "FROM (" +
                        "    SELECT institution_id, institution_name, satker_name " +
                        "    FROM jabfung.sertifikat_okupasi " +
                        "    GROUP BY institution_id, institution_name, satker_name " +
                        ") as j LEFT JOIN (" +
                        "    SELECT COUNT(*) as total, institution_id, satker_name " +
                        "    FROM jabfung.sertifikat_okupasi " +
                        "    WHERE type = 'Pokja Pemilihan' " +
                        "    GROUP BY institution_id, satker_name" +
                        ") as k ON k.institution_id=j.institution_id and k.satker_name = j.satker_name " +
                        "LEFT JOIN (" +
                        "    SELECT COUNT(*) as total, institution_id, satker_name " +
                        "    FROM jabfung.sertifikat_okupasi " +
                        "    WHERE type = 'Pejabat Pengadaan' " +
                        "    GROUP BY institution_id, satker_name" +
                        ") as m ON m.institution_id=j.institution_id and m.satker_name = j.satker_name " +
                        "LEFT JOIN (" +
                        "    SELECT COUNT(*) as total, institution_id, satker_name" +
                        "    FROM jabfung.sertifikat_okupasi " +
                        "    WHERE type = 'Pejabat Pembuat Komitmen' " +
                        "    GROUP BY institution_id, satker_name" +
                        ") as mm ON mm.institution_id=j.institution_id and mm.satker_name = j.satker_name");
        if (model.isKeywordExist()) {
            sb.append(" WHERE j.satker_name ILIKE ").append("'%").append(model.getKeyword()).append("%'")
                    .append("or j.institution_id ILIKE ").append("'%").append(model.getKeyword()).append("%'")
                    .append("or j.institution_name ILIKE ").append("'%").append(model.getKeyword()).append("%'");
        }
        sb.append(" ORDER BY j.institution_name,j.satker_name ");
        result.executeQuery(sb.toString());
        return result;
    }

    public Chartable levelOneChart(SearchQuery model) {
        // ambil label seriesnya
        List<String> seriesLabels = Query.find(
                "select distinct type from jabfung.sertifikat_okupasi",
                String.class
        ).fetch();
        // prepare query data
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1) as total, " +
                "extract(year from issued_date) as year," +
                "extract(month from issued_date) as month, " +
                "type " +
                "from jabfung.sertifikat_okupasi ");
        sb.append("where issued_date >= ? and issued_date <= ?");
        if (model.isKeywordExist()) {
            sb.append(" and institution_name ILIKE ").append("'%").append(model.getKeyword()).append("%'");
        }
        sb.append(" group BY extract(year from issued_date),extract(month from issued_date), type");
        sb.append(" ORDER BY extract(year from issued_date),extract(month from issued_date) ASC ");

        // query
        LocalDate now = LocalDate.now();
        LocalDate aYearAgo = now.minusYears(1).plusMonths(1).withDayOfMonth(1);
        List<Chartable.ChartQueryResult> result = Query.find(sb.toString(), (ResultSet resultSet) -> {
            Chartable.ChartQueryResult res = new Chartable.ChartQueryResult();
            res.timeLabel = DateFormatter.format(
                    new LocalDate(
                            resultSet.getInt("year"),
                            resultSet.getInt("month"),
                            1).toDate(),
                    "MMMM yy"
            );
            res.seriesLabel = resultSet.getString("type");
            res.data = resultSet.getBigDecimal(1);
            return res;
        }, aYearAgo.toDate(), now.toDate()).fetch();
        // build and return chart
        return ChartBuilder.buildTimeSeries(aYearAgo, now, seriesLabels, result);
    }

    public OccupationalCertificateSearchResult levelTwo(SearchQuery model, String kode_kldi, String satker) {
        OccupationalCertificateSearchResult result = new OccupationalCertificateSearchResult(model);
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT name, type, rank FROM jabfung.sertifikat_okupasi ");
        sb.append(" WHERE institution_id = '").append(QueryUtil.sanitize(kode_kldi)).append("'");
        sb.append(" AND satker_name = '").append(QueryUtil.sanitize(satker)).append("'");
        if (model.isKeywordExist()) {
        sb.append(" AND ")
                .append(QueryUtil.searchLikeQuery(
                        new String[] {"name", "type", "rank"
                        }, model.getKeyword()));
    }
        sb.append(" ORDER BY name ASC ");
        result.executeQuery(sb.toString());
        return result;
    }

    public Long getTotalBy(String kldiId) {
        LogUtil.debug(TAG, "get total by kldi: " + kldiId);
        Long total = OccupationalCertificate.count("institution_id = ?", kldiId);
        LogUtil.debug(TAG, "total by kldi: " + total);
        return total;
    }

}
