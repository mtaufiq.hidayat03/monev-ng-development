package ppsdm.pojos;

import models.cms.profilekl.ProfileKlDataUtilContract;
import repositories.dashboard.TotalPenyediaRepository;

public class TotalPenyediaMeta implements ProfileKlDataUtilContract {

    private final Long total;

    public TotalPenyediaMeta(String propinsi) {
        total = TotalPenyediaRepository.getInstance().getTotalByPropinsi(propinsi);
    }
}
