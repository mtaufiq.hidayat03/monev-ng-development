package ppsdm.pojos;

import models.common.contracts.TablePagination.PageAble;
import models.common.contracts.TablePagination.ParamAble;
import ppsdm.models.JabfungPbj;

import java.util.List;

/**
 * @author HanusaCloud on 11/8/2019 9:00 AM
 */
public class PbjSearchResult implements PageAble<JabfungPbj> {

    private List<JabfungPbj> items;
    private int total = 0;
    private final ParamAble params;
    private Meta meta;

    public PbjSearchResult(ParamAble params) {
        this.params = params;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public ParamAble getParamAble() {
        return params;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setItems(List<JabfungPbj> collection) {
        this.items = collection;
    }

    @Override
    public List<JabfungPbj> getItems() {
        return items;
    }

    @Override
    public Class<JabfungPbj> getReturnType() {
        return JabfungPbj.class;
    }

    @Override
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    @Override
    public Meta getMeta() {
        return meta;
    }
}
