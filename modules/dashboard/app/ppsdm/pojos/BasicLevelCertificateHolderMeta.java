package ppsdm.pojos;

import models.cms.profilekl.ProfileKlData;
import models.cms.profilekl.ProfileKlDataUtilContract;
import models.cms.profilekl.ProfileKlHeader;
import permission.Kldi;
import ppsdm.BasicLevelCertificateHolderRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 11/8/2019 2:33 PM
 */
public class BasicLevelCertificateHolderMeta implements ProfileKlDataUtilContract {

    private final Long total;

    public BasicLevelCertificateHolderMeta() {
        total = BasicLevelCertificateHolderRepository.getInstance().getTotal();
    }

    public BasicLevelCertificateHolderMeta(String kode_kldi) {
        total = BasicLevelCertificateHolderRepository.getInstance().getTotalBy(kode_kldi);
    }

    public BasicLevelCertificateHolderMeta(Kldi kldi) {
        total = BasicLevelCertificateHolderRepository.getInstance().getTotalThisYearIn(kldi.id);
    }

    public BasicLevelCertificateHolderMeta(Long total) {
        this.total = total;
    }

    public BasicLevelCertificateHolderMeta(List<ProfileKlData> data) {
        total = searchFor(ProfileKlHeader.JABFUNG_PBJ_TOTAL, data);
    }

    @Override
    public List<ProfileKlData> getProfileData() {
        List<ProfileKlData> data = new ArrayList<>();
        data.add(new ProfileKlData(ProfileKlHeader.JABFUNG_PBJ_TOTAL, getAll().doubleValue()));
        return data;
    }

    public Long getAll() {
        return total;
    }

}
