package ppsdm.pojos;

import models.common.contracts.TablePagination.PageAble;
import models.common.contracts.TablePagination.ParamAble;
import models.dashboard.SearchQuery;
import ppsdm.models.OccupationalCertificate;

import java.util.List;

public class OccupationalCertificateSearchResult implements PageAble<OccupationalCertificate> {

    private List<OccupationalCertificate> items;
    private final ParamAble params;
    private int total = 0;
    private Meta meta;

    public OccupationalCertificateSearchResult(ParamAble params) {
        this.params = params;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setItems(List<OccupationalCertificate> collection) {
        this.items = collection;
    }

    @Override
    public List<OccupationalCertificate> getItems() {
        return items;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public ParamAble getParamAble() {
        return params;
    }

    @Override
    public Class<OccupationalCertificate> getReturnType() {
        return OccupationalCertificate.class;
    }

    @Override
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    @Override
    public Meta getMeta() {
        return meta;
    }
}
