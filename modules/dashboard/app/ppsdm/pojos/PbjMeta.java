package ppsdm.pojos;

import models.cms.profilekl.ProfileKlData;
import models.cms.profilekl.ProfileKlDataUtilContract;
import models.cms.profilekl.ProfileKlHeader;
import permission.Kldi;
import ppsdm.JabfungRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 11/8/2019 2:33 PM
 */
public class PbjMeta implements ProfileKlDataUtilContract {

    private final Long pertama;
    private final Long muda;
    private final Long madya;

    public PbjMeta() {
        pertama = JabfungRepository.getInstance().getTotalByJenjang("Pertama");
        muda = JabfungRepository.getInstance().getTotalByJenjang("Muda");
        madya = JabfungRepository.getInstance().getTotalByJenjang("Madya");
    }

    public PbjMeta(String kode_kldi) {
        muda = JabfungRepository.getInstance().getTotalBy(kode_kldi, "Muda");
        pertama = JabfungRepository.getInstance().getTotalBy(kode_kldi, "Pertama");
        madya = JabfungRepository.getInstance().getTotalBy(kode_kldi, "Madya");
    }

    public PbjMeta(Kldi kldi) {
        muda = JabfungRepository.getInstance().getTotalThisYearIn(kldi.id, "Muda");
        pertama = JabfungRepository.getInstance().getTotalThisYearIn(kldi.id, "Pertama");
        madya = JabfungRepository.getInstance().getTotalThisYearIn(kldi.id, "Madya");
    }

    public PbjMeta(Long muda, Long pertama, Long madya) {
        this.muda = muda;
        this.pertama = pertama;
        this.madya = madya;
    }

    public PbjMeta(List<ProfileKlData> data) {
        muda = searchFor(ProfileKlHeader.JABFUNG_PBJ_MUDA, data);
        pertama = searchFor(ProfileKlHeader.JABFUNG_PBJ_PERTAMA, data);
        madya = searchFor(ProfileKlHeader.JABFUNG_PBJ_MADYA, data);
    }

    @Override
    public List<ProfileKlData> getProfileData() {
        List<ProfileKlData> data = new ArrayList<>();
        data.add(new ProfileKlData(ProfileKlHeader.JABFUNG_PBJ_TOTAL, getAll().doubleValue()));
        data.add(new ProfileKlData(ProfileKlHeader.JABFUNG_PBJ_MADYA, getMadya().doubleValue()));
        data.add(new ProfileKlData(ProfileKlHeader.JABFUNG_PBJ_MUDA, getMuda().doubleValue()));
        data.add(new ProfileKlData(ProfileKlHeader.JABFUNG_PBJ_PERTAMA, getPertama().doubleValue()));
        return data;
    }

    public Long getMadya() {
        return madya;
    }

    public Long getMuda() {
        return muda;
    }

    public Long getPertama() {
        return pertama;
    }

    public Long getAll() {
        return getMadya() + getMuda() + getPertama();
    }

}
