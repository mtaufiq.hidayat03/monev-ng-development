package ppsdm.pojos;

import models.common.contracts.TablePagination.PageAble;
import models.common.contracts.TablePagination.ParamAble;
import models.dashboard.SearchQuery;
import ppsdm.models.BasicLevelCertificateHolder;

import java.util.List;

/**
 * @author HanusaCloud on 11/7/2019 12:35 PM
 */
public class BasicLevelCertificateHolderSearchResult implements PageAble<BasicLevelCertificateHolder> {

    private List<BasicLevelCertificateHolder> items;
    private final SearchQuery request;
    private int total = 0;
    private Meta meta;

    public BasicLevelCertificateHolderSearchResult(SearchQuery request) {
        this.request = request;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setItems(List<BasicLevelCertificateHolder> collection) {
        this.items = collection;
    }

    @Override
    public List<BasicLevelCertificateHolder> getItems() {
        return items;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public ParamAble getParamAble() {
        return request;
    }

    @Override
    public Class<BasicLevelCertificateHolder> getReturnType() {
        return BasicLevelCertificateHolder.class;
    }

    @Override
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    @Override
    public Meta getMeta() {
        return meta;
    }
}
