package ppsdm.pojos;

import models.cms.profilekl.ProfileKlData;
import models.cms.profilekl.ProfileKlHeader;
import models.cms.profilekl.ProfileKlDataUtilContract;
import permission.Kldi;
import ppsdm.CertificateHolderRepository;
import utils.common.LogUtil;

import java.util.ArrayList;
import java.util.List;

public class PbjCertificateMeta implements ProfileKlDataUtilContract {

    private static final String TAG = "";
    private final Long muda;
    private final Long madya;
    private final Long pertama;

    public PbjCertificateMeta() {
        pertama = CertificateHolderRepository.getInstance().getTotalByLevel("Pertama");
        muda = CertificateHolderRepository.getInstance().getTotalByLevel("Muda");
        madya = CertificateHolderRepository.getInstance().getTotalByLevel("Madya");
    }

    public PbjCertificateMeta(String instituionId) {
        LogUtil.debug(TAG, instituionId);
        pertama = CertificateHolderRepository.getInstance().getTotalBy(instituionId, "Pertama");
        muda = CertificateHolderRepository.getInstance().getTotalBy(instituionId, "Muda");
        madya = CertificateHolderRepository.getInstance().getTotalBy(instituionId, "Madya");
    }

    public PbjCertificateMeta(Kldi kldi) {
        pertama = CertificateHolderRepository.getInstance().getTotalThisYearBy(kldi, "Pertama");
        muda = CertificateHolderRepository.getInstance().getTotalThisYearBy(kldi, "Muda");
        madya = CertificateHolderRepository.getInstance().getTotalThisYearBy(kldi, "Madya");
    }

    public PbjCertificateMeta(List<ProfileKlData> data) {
        pertama = searchFor(ProfileKlHeader.SERTIFIKAT_KOMPETENSI_PERTAMA, data);
        muda = searchFor(ProfileKlHeader.SERTIFIKAT_KOMPETENSI_MUDA, data);
        madya = searchFor(ProfileKlHeader.SERTIFIKAT_KOMPETENSI_MADYA, data);
    }

    @Override
    public List<ProfileKlData> getProfileData() {
        List<ProfileKlData> data = new ArrayList<>();
        data.add(new ProfileKlData(ProfileKlHeader.SERTIFIKAT_KOMPETENSI_TOTAL, getAll().doubleValue()));
        data.add(new ProfileKlData(ProfileKlHeader.SERTIFIKAT_KOMPETENSI_PERTAMA, pertama.doubleValue()));
        data.add(new ProfileKlData(ProfileKlHeader.SERTIFIKAT_KOMPETENSI_MUDA, muda.doubleValue()));
        data.add(new ProfileKlData(ProfileKlHeader.SERTIFIKAT_KOMPETENSI_MADYA, madya.doubleValue()));
        return data;
    }

    public PbjCertificateMeta(Long pertama, Long muda, Long madya) {
        this.pertama = pertama;
        this.muda = muda;
        this.madya = madya;
    }

    public Long getPertama() { return pertama;}

    public Long getMadya() {
        return madya;
    }

    public Long getMuda() {
        return muda;
    }


    public Long getAll() {
        return getPertama() + getMadya() + getMuda();
    }
}
