package ppsdm.pojos;

import models.common.contracts.TablePagination.PageAble;
import models.common.contracts.TablePagination.ParamAble;
import models.dashboard.SearchQuery;
import ppsdm.models.CertificateHolder;

import java.util.List;

/**
 * @author HanusaCloud on 11/7/2019 12:35 PM
 */
public class CertificateHolderSearchResult implements PageAble<CertificateHolder> {

    private List<CertificateHolder> items;
    private final ParamAble params;
    private int total = 0;
    private Meta meta;

    public CertificateHolderSearchResult(ParamAble params) {
        this.params = params;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setItems(List<CertificateHolder> collection) {
        this.items = collection;
    }

    @Override
    public List<CertificateHolder> getItems() {
        return items;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public ParamAble getParamAble() {
        return params;
    }

    @Override
    public Class<CertificateHolder> getReturnType() {
        return CertificateHolder.class;
    }

    @Override
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    @Override
    public Meta getMeta() {
        return meta;
    }
}
