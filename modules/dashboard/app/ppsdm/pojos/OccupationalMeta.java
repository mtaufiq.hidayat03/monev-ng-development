package ppsdm.pojos;

import models.cms.profilekl.ProfileKlData;
import models.cms.profilekl.ProfileKlDataUtilContract;
import permission.Kldi;
import ppsdm.OccupationalCertificateRepository;
import ppsdm.models.OccupationalType;

import java.util.ArrayList;
import java.util.List;

import static models.cms.profilekl.ProfileKlHeader.*;

public class OccupationalMeta implements ProfileKlDataUtilContract {
    private final Long pokjaPemilihan;
    private final Long pejabatPengadaan;
    private final Long pejabatPembuatKomitmen;

    public OccupationalMeta() {
        pokjaPemilihan = OccupationalCertificateRepository.getInstance()
                .getTotalByType(OccupationalType.POKJA_PEMILIHAN);
        pejabatPengadaan = OccupationalCertificateRepository.getInstance()
                .getTotalByType(OccupationalType.PEJABAT_PENGADAAN);
        pejabatPembuatKomitmen = OccupationalCertificateRepository.getInstance()
                .getTotalByType(OccupationalType.PEJABAT_PEMBUAT_KOMITMEN);
    }

    public OccupationalMeta(String institutionId) {
        pokjaPemilihan = OccupationalCertificateRepository.getInstance()
                .getTotalBy(institutionId, OccupationalType.POKJA_PEMILIHAN);
        pejabatPengadaan = OccupationalCertificateRepository.getInstance()
                .getTotalBy(institutionId, OccupationalType.PEJABAT_PENGADAAN);
        pejabatPembuatKomitmen = OccupationalCertificateRepository.getInstance()
                .getTotalBy(institutionId, OccupationalType.PEJABAT_PEMBUAT_KOMITMEN);
    }

    public OccupationalMeta(Kldi kldi) {
        pokjaPemilihan = OccupationalCertificateRepository.getInstance()
                .getTotalThisYearBy(kldi, OccupationalType.POKJA_PEMILIHAN);
        pejabatPengadaan = OccupationalCertificateRepository.getInstance()
                .getTotalThisYearBy(kldi, OccupationalType.PEJABAT_PENGADAAN);
        pejabatPembuatKomitmen = OccupationalCertificateRepository.getInstance()
                .getTotalThisYearBy(kldi, OccupationalType.PEJABAT_PEMBUAT_KOMITMEN);
    }

    public OccupationalMeta(Long pokjaPemilihan, Long pejabatPengadaan, Long pejabatPembuatKomitmen) {
        this.pokjaPemilihan = pokjaPemilihan;
        this.pejabatPengadaan = pejabatPengadaan;
        this.pejabatPembuatKomitmen = pejabatPembuatKomitmen;
    }

    public OccupationalMeta(List<ProfileKlData> data) {
        pokjaPemilihan = searchFor(SERTIFIKAT_OKUPASI_POKJA, data);
        pejabatPembuatKomitmen = searchFor(SERTIFIKAT_OKUPASI_PPK, data);
        pejabatPengadaan = searchFor(SERTIFIKAT_OKUPASI_PP, data);
    }

    @Override
    public List<ProfileKlData> getProfileData() {
        List<ProfileKlData> data = new ArrayList<>();
        data.add(new ProfileKlData(SERTIFIKAT_OKUPASI, getAll().doubleValue()));
        data.add(new ProfileKlData(SERTIFIKAT_OKUPASI_POKJA, pokjaPemilihan.doubleValue()));
        data.add(new ProfileKlData(SERTIFIKAT_OKUPASI_PP, pejabatPengadaan.doubleValue()));
        data.add(new ProfileKlData(SERTIFIKAT_OKUPASI_PPK, pejabatPembuatKomitmen.doubleValue()));
        return data;
    }

    public Long getPokjaPemilihan() {
        return pokjaPemilihan;
    }

    public Long getPejabatPengadaan() {
        return pejabatPengadaan;
    }

    public Long getPejabatPembuatKomitmen() {
        return pejabatPembuatKomitmen;
    }

    public Long getAll() {
        return getPokjaPemilihan() + getPejabatPengadaan() + getPejabatPembuatKomitmen();
    }
}
