package ppsdm;

import models.common.contracts.Chartable;
import models.dashboard.SearchQuery;
import org.joda.time.LocalDate;
import permission.Kldi;
import play.db.jdbc.Query;
import ppsdm.models.CertificateHolder;
import ppsdm.pojos.CertificateHolderSearchResult;
import ppsdm.pojos.PbjCertificateMeta;
import utils.common.ChartBuilder;
import utils.common.DateFormatter;
import utils.common.LogUtil;
import utils.common.QueryUtil;

import java.sql.ResultSet;
import java.time.Year;
import java.util.List;

/**
 * @author HanusaCloud on 11/8/2019 3:16 PM
 */
public class CertificateHolderRepository {

    public static final String TAG = "CertificateHolderRepository";

    public Long getTotalBy(String instituionId, String level) {
        final Long total = CertificateHolder.count("institution_id = ? AND level = ?", instituionId, level);
        return total;
    }

    public Long getTotalThisYearBy(Kldi kldi, String level) {
        LogUtil.debug(TAG, "get total this year by kldi: " + kldi.nama_instansi );
        final Long total = CertificateHolder.count(
                "institution_name = ? AND level = ? AND EXTRACT(YEAR FROM issued_date) = ?",
                kldi.nama_instansi,
                level,
                Year.now().getValue()
        );
        return total;
    }

    public PbjCertificateMeta getTotalAll() {
        return new PbjCertificateMeta();
    }

    public Long getCount() {
        final Long total = CertificateHolder.count();
        LogUtil.debug(TAG, "total certificate holder: " +total);
        return total;
    }

    public Long getCountBy(Kldi kldi) {
        LogUtil.debug(TAG, "get count by kldi: ", kldi);
        final Long total = CertificateHolder.count("UPPER(institution_name) = ? ", kldi.nama_instansi.toUpperCase());
        LogUtil.debug(TAG, "total: " + total);
        return total;
    }

    public Long getTotalByLevel(String level) {
        LogUtil.debug(TAG, "get total by level: " + level);
        final Long result = CertificateHolder.count("level = ? ", level);
        LogUtil.debug(TAG,  result);
        return result;
    }

    private static final CertificateHolderRepository repositoru = new CertificateHolderRepository();

    public static CertificateHolderRepository getInstance() {
        return repositoru;
    }

    public CertificateHolderSearchResult levelOne(SearchQuery model) {
        CertificateHolderSearchResult result = new CertificateHolderSearchResult(model);
        StringBuilder sb = new StringBuilder();
        sb.append(
                "SELECT j.institution_id, j.institution_name,j.satker_name, " +
                        "    COALESCE(k.total, 0) as pertama, " +
                        "    COALESCE(m.total, 0) as muda, " +
                        "    COALESCE(mm.total, 0) as madya " +
                        "FROM (" +
                        "    SELECT institution_id, institution_name, satker_name " +
                        "    FROM jabfung.sertifikat_kompetensi " +
                        "    GROUP BY institution_id, institution_name, satker_name " +
                        ") as j LEFT JOIN (" +
                        "    SELECT COUNT(*) as total, institution_id, satker_name " +
                        "    FROM jabfung.sertifikat_kompetensi " +
                        "    WHERE level = 'Pertama' " +
                        "    GROUP BY institution_id, satker_name" +
                        ") as k ON k.institution_id=j.institution_id and k.satker_name = j.satker_name " +
                        "LEFT JOIN (" +
                        "    SELECT COUNT(*) as total, institution_id, satker_name " +
                        "    FROM jabfung.sertifikat_kompetensi " +
                        "    WHERE level = 'Muda' " +
                        "    GROUP BY institution_id, satker_name" +
                        ") as m ON m.institution_id=j.institution_id and m.satker_name = j.satker_name " +
                        "LEFT JOIN (" +
                        "    SELECT COUNT(*) as total, institution_id, satker_name" +
                        "    FROM jabfung.sertifikat_kompetensi " +
                        "    WHERE level = 'Madya' " +
                        "    GROUP BY institution_id, satker_name" +
                        ") as mm ON mm.institution_id=j.institution_id and mm.satker_name = j.satker_name");
        if (model.isKeywordExist()) {
            sb.append(" WHERE j.satker_name ILIKE ").append("'%").append(model.getKeyword()).append("%'")
                    .append("or j.institution_id ILIKE ").append("'%").append(model.getKeyword()).append("%'")
                    .append("or j.institution_name ILIKE ").append("'%").append(model.getKeyword()).append("%'");
        }
        sb.append(" ORDER BY j.institution_name,j.satker_name ");
        result.executeQuery(sb.toString());
        return result;
    }

    public Chartable levelOneChart(SearchQuery model) {
        // ambil label seriesnya
        List<String> seriesLabels = Query.find("select distinct level from jabfung.sertifikat_kompetensi", String.class).fetch();
        // prepare query data
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1) as total, extract(year from issued_date) as year,extract(month from issued_date) as month, level from jabfung.sertifikat_kompetensi ");
        sb.append("where issued_date >= ? and issued_date <= ?");
        if (model.isKeywordExist()) {
            sb.append(" and institution_name ILIKE ").append("'%").append(model.getKeyword()).append("%'");
        }
        sb.append(" group BY extract(year from issued_date),extract(month from issued_date), level");
        sb.append(" ORDER BY extract(year from issued_date),extract(month from issued_date) ASC ");

        // query
        LocalDate now = LocalDate.now();
        LocalDate aYearAgo = now.minusYears(1).plusMonths(1).withDayOfMonth(1);
        List<Chartable.ChartQueryResult> result = Query.find(sb.toString(), (ResultSet resultSet) -> {
            Chartable.ChartQueryResult res = new Chartable.ChartQueryResult();
            res.timeLabel = DateFormatter.format(new LocalDate(resultSet.getInt("year"), resultSet.getInt("month"), 1).toDate(), "MMMM yy");
            res.seriesLabel = resultSet.getString("level");
            res.data = resultSet.getBigDecimal(1);
            return res;
        }, aYearAgo.toDate(), now.toDate()).fetch();
        // build and return chart
        return ChartBuilder.buildTimeSeries(aYearAgo, now, seriesLabels, result);
    }

    public CertificateHolderSearchResult levelTwo(SearchQuery model, String kode_kldi, String satker) {
        CertificateHolderSearchResult result = new CertificateHolderSearchResult(model);
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT name, level, rank FROM jabfung.sertifikat_kompetensi ");
        sb.append(" WHERE institution_id = '").append(QueryUtil.sanitize(kode_kldi)).append("'");
        sb.append(" AND satker_name = '").append(QueryUtil.sanitize(satker)).append("'");
        if (model.isKeywordExist()) {
            sb.append(" AND ")
                    .append(QueryUtil.searchLikeQuery(
                            new String[] {"name", "level", "rank"
                            }, model.getKeyword()));
        }
        sb.append(" ORDER BY name ASC ");
        result.executeQuery(sb.toString());
        return result;
    }

    public Long getCountBy(List<String> satkers) {
        LogUtil.debug(TAG, "get total by satkers: ", satkers);
        StringBuilder sb = new StringBuilder();
        String glue = "";
        for (String s : satkers) {
            sb.append(glue).append("'").append(s).append("'");
            glue = ", ";
        }
        final Long total = CertificateHolder.count("satker IN (" + sb.toString() + " )");
        LogUtil.debug(TAG, "total by satkers: " + total);
        return total;
    }



}
