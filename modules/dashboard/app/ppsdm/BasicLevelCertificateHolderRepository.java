package ppsdm;

import models.common.contracts.Chartable;
import models.dashboard.SearchQuery;
import org.joda.time.LocalDate;
import permission.Kldi;
import play.db.jdbc.Query;
import ppsdm.models.BasicLevelCertificateHolder;
import ppsdm.pojos.BasicLevelCertificateHolderMeta;
import ppsdm.pojos.BasicLevelCertificateHolderSearchResult;
import utils.common.ChartBuilder;
import utils.common.DateFormatter;
import utils.common.LogUtil;
import utils.common.QueryUtil;

import java.sql.ResultSet;
import java.time.Year;
import java.util.Arrays;
import java.util.List;

/**
 * @author HanusaCloud on 11/8/2019 3:16 PM
 */
public class BasicLevelCertificateHolderRepository {

    public static final String TAG = "BasicLevelCertificateHolderRepository";

    public Long getTotalBy(String kode_kldi) {
        final Long total = BasicLevelCertificateHolder.count("institution_id = ?", kode_kldi);
        return total;
    }

    public Long getTotalThisYearIn(String kode_kldi) {
        LogUtil.debug(TAG, "get total by: " + kode_kldi);
        final Long total = BasicLevelCertificateHolder.count(
                "institution_id = ? AND EXTRACT(YEAR FROM issued_date) = ?",
                Year.now().getValue()
        );
        LogUtil.debug(TAG, "result: " + total);
        return total;
    }

    public BasicLevelCertificateHolderMeta getTotalAll() {
        return new BasicLevelCertificateHolderMeta();
    }

    public Long getCount() {
        final Long total = BasicLevelCertificateHolder.count();
        LogUtil.debug(TAG, "total certificate holder: " +total);
        return total;
    }

    public Long getCountBy(Kldi kldi) {
        LogUtil.debug(TAG, "get count by kldi: ", kldi);
        final Long total = BasicLevelCertificateHolder.count("UPPER(institution) = ? ", kldi.nama_instansi.toUpperCase());
        LogUtil.debug(TAG, "total: " + total);
        return total;
    }

    public Long getTotal() {
        LogUtil.debug(TAG, "get total");
        final Long result = BasicLevelCertificateHolder.count();
        LogUtil.debug(TAG,  result);
        return result;
    }

    private static final BasicLevelCertificateHolderRepository repositoru = new BasicLevelCertificateHolderRepository();

    public static BasicLevelCertificateHolderRepository getInstance() {
        return repositoru;
    }

    public BasicLevelCertificateHolderSearchResult levelOne(SearchQuery model) {
        BasicLevelCertificateHolderSearchResult result = new BasicLevelCertificateHolderSearchResult(model);
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT COUNT(*) as total, institution_id, institution_name, satker_name FROM jabfung.pemegang_sertifikat ");
        if (model.isKeywordExist()) {
            sb.append(" WHERE institution_name ILIKE ").append("'%").append(model.getKeyword()).append("%'");
            sb.append(" OR satker_name ILIKE ").append("'%").append(model.getKeyword()).append("%'");
        }
        sb.append(" GROUP BY institution_id, institution_name, satker_name");
        sb.append(" ORDER BY institution_name, satker_name ");
        result.executeQuery(sb.toString());
        return result;
    }

    public Chartable levelOneChart(SearchQuery model) {
        // ambil label seriesnya
        List<String> seriesLabels = Arrays.asList("Jumlah");
        // prepare query data
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1) as total, extract(year from issued_date) as year,extract(month from issued_date) as month from jabfung.pemegang_sertifikat ");
        sb.append("where issued_date >= ? and issued_date <= ?");
        if (model.isKeywordExist()) {
            sb.append(" and ")
                    .append("(")
                    .append("institution_name ILIKE ").append("'%").append(model.getKeyword()).append("%'")
                    .append(" or ")
                    .append("satker_name ILIKE ").append("'%").append(model.getKeyword()).append("%'")
                    .append(")");
        }
        sb.append(" group BY extract(year from issued_date),extract(month from issued_date)");
        sb.append(" ORDER BY extract(year from issued_date),extract(month from issued_date) ASC ");

        // query
        LocalDate now = LocalDate.now();
        LocalDate aYearAgo = now.minusYears(1).plusMonths(1).withDayOfMonth(1);
        List<Chartable.ChartQueryResult> result = Query.find(sb.toString(), (ResultSet resultSet) -> {
            Chartable.ChartQueryResult res = new Chartable.ChartQueryResult();
            res.timeLabel = DateFormatter.format(new LocalDate(resultSet.getInt("year"), resultSet.getInt("month"), 1).toDate(), "MMMM yy");
            res.seriesLabel = "Jumlah";
            res.data = resultSet.getBigDecimal(1);
            return res;
        }, aYearAgo.toDate(), now.toDate()).fetch();
        // build and return chart
        return ChartBuilder.buildTimeSeries(aYearAgo, now, seriesLabels, result);
    }

    public BasicLevelCertificateHolderSearchResult levelTwo(SearchQuery model, String kode_kldi, String satker) {
        BasicLevelCertificateHolderSearchResult result = new BasicLevelCertificateHolderSearchResult(model);
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT name, rank FROM jabfung.pemegang_sertifikat ");
        sb.append(" WHERE institution_id = '").append(QueryUtil.sanitize(kode_kldi)).append("'");
        sb.append(" AND satker_name = '").append(QueryUtil.sanitize(satker)).append("'");
        if (model.isKeywordExist()) {
            sb.append(" AND ")
                    .append(QueryUtil.searchLikeQuery(
                            new String[] {"name", "rank"
                            }, model.getKeyword()));
        }
        sb.append(" ORDER BY name ASC ");
        result.executeQuery(sb.toString());
        return result;
    }
}
