package ppsdm;

import models.common.contracts.Chartable;
import models.dashboard.SearchQuery;
import org.joda.time.LocalDate;
import play.db.jdbc.Query;
import ppsdm.models.JabfungPbj;
import ppsdm.pojos.PbjMeta;
import ppsdm.pojos.PbjSearchResult;
import utils.common.ChartBuilder;
import utils.common.DateFormatter;
import utils.common.LogUtil;
import utils.common.QueryUtil;

import java.sql.ResultSet;
import java.time.Year;
import java.util.List;

/**
 * @author HanusaCloud on 11/8/2019 2:32 PM
 */
public class JabfungRepository {

    public static final String TAG = "JabfungRepository";

    private static final JabfungRepository repository = new JabfungRepository();

    public static JabfungRepository getInstance() {
        return repository;
    }

    public Long getTotalBy(String kode_kldi, String level) {
        LogUtil.debug(TAG, "get total by: " + kode_kldi + " level: " + level);
        final Long total = JabfungPbj.count("institution_id = ? AND level = ?", kode_kldi, level);
        LogUtil.debug(TAG, "result: " + total);
        return total;
    }

    public Long getTotalThisYearIn(String kode_kldi, String level) {
        LogUtil.debug(TAG, "get total by: " + kode_kldi + " level: " + level);
        final Long total = JabfungPbj.count(
                "institution_id = ? AND level = ? AND EXTRACT(YEAR FROM issued_date) = ?",
                kode_kldi,
                level,
                Year.now().getValue()
        );
        LogUtil.debug(TAG, "result: " + total);
        return total;
    }

    public PbjMeta getTotalAll() {
        LogUtil.debug(TAG, "get total all item");
        return new PbjMeta();
    }

    public Long getTotalByJenjang(String level) {
        LogUtil.debug(TAG, "get total by level: " + level);
        final Long result = JabfungPbj.count("level = ? ", level);
        LogUtil.debug(TAG, "result: " + result);
        return result;
    }

    public Chartable levelOneChart(SearchQuery model) {
        // ambil label seriesnya
        List<String> seriesLabels = Query.find(
                "select distinct level from jabfung.jabfung_pbj",
                String.class
        ).fetch();

        // prepare query data
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1) as total, " +
                "extract(year from issued_date) as year," +
                "extract(month from issued_date) as month, " +
                "level from jabfung.jabfung_pbj ");
        sb.append("where issued_date >= ? and issued_date <= ?");
        if (model.isKeywordExist()) {
            sb.append(" and satker_name ILIKE ").append("'%").append(model.getKeyword()).append("%'");
        }
        sb.append(" group BY extract(year from issued_date),extract(month from issued_date), level");
        sb.append(" ORDER BY extract(year from issued_date),extract(month from issued_date) ASC ");

        // query
        LocalDate now = LocalDate.now();
        LocalDate aYearAgo = now.minusYears(1).plusMonths(1).withDayOfMonth(1);
        List<Chartable.ChartQueryResult> result = Query.find(sb.toString(), (ResultSet resultSet) -> {
            Chartable.ChartQueryResult res = new Chartable.ChartQueryResult();
            res.timeLabel = DateFormatter.format(
                    new LocalDate(
                            resultSet.getInt("year"),
                            resultSet.getInt("month"),
                            1).toDate()
                    , "MMMM yy"
            );
            res.seriesLabel = resultSet.getString("level");
            res.data = resultSet.getBigDecimal(1);
            return res;
        }, aYearAgo.toDate(), now.toDate()).fetch();

        // build and return chart
        return ChartBuilder.buildTimeSeries(aYearAgo, now, seriesLabels, result);
    }

    public PbjSearchResult levelOne(SearchQuery model) {
        PbjSearchResult result = new PbjSearchResult(model);
        StringBuilder sb = new StringBuilder();
        sb.append(
                "SELECT j.institution_id, j.institution_name,j.satker_name, " +
                "    COALESCE(k.total, 0) as pertama, " +
                "    COALESCE(m.total, 0) as muda, " +
                "    COALESCE(mm.total, 0) as madya " +
                "FROM (" +
                "    SELECT institution_id, institution_name, satker_name " +
                "    FROM jabfung.jabfung_pbj " +
                "    GROUP BY institution_id, institution_name, satker_name " +
                ") as j LEFT JOIN (" +
                "    SELECT COUNT(*) as total, institution_id, satker_name " +
                "    FROM jabfung.jabfung_pbj " +
                "    WHERE level = 'Pertama' " +
                "    GROUP BY institution_id, satker_name" +
                ") as k ON k.institution_id=j.institution_id and k.satker_name = j.satker_name " +
                "LEFT JOIN (" +
                "    SELECT COUNT(*) as total, institution_id, satker_name " +
                "    FROM jabfung.jabfung_pbj " +
                "    WHERE level = 'Muda' " +
                "    GROUP BY institution_id, satker_name" +
                ") as m ON m.institution_id=j.institution_id and m.satker_name = j.satker_name " +
                "LEFT JOIN (" +
                "    SELECT COUNT(*) as total, institution_id, satker_name" +
                "    FROM jabfung.jabfung_pbj " +
                "    WHERE level = 'Madya' " +
                "    GROUP BY institution_id, satker_name" +
                ") as mm ON mm.institution_id=j.institution_id and mm.satker_name = j.satker_name");
        if (model.isKeywordExist()) {
            sb.append(" WHERE j.satker_name ILIKE ").append("'%").append(model.getKeyword()).append("%'")
                    .append("or j.institution_id ILIKE ").append("'%").append(model.getKeyword()).append("%'")
                    .append("or j.institution_name ILIKE ").append("'%").append(model.getKeyword()).append("%'");
        }
        sb.append(" ORDER BY j.institution_name,j.satker_name ");
        result.executeQuery(sb.toString());
        return result;
    }

    public PbjSearchResult levelTwo(SearchQuery model, String kode_kldi, String satker) {
        PbjSearchResult result = new PbjSearchResult(model);
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT name, level, rank FROM jabfung.jabfung_pbj ");
        sb.append(" WHERE institution_id = '").append(QueryUtil.sanitize(kode_kldi)).append("'");
        sb.append(" AND satker_name = '").append(QueryUtil.sanitize(satker)).append("'");
        if (model.isKeywordExist()) {
            sb.append(" AND ")
                    .append(QueryUtil.searchLikeQuery(
                            new String[] {"name", "level", "rank"
                            }, model.getKeyword()));
        }
        sb.append(" ORDER BY name ASC ");
        result.executeQuery(sb.toString());
        return result;
    }

}
