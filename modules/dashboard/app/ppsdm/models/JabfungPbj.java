package ppsdm.models;

import constants.generated.R;
import models.common.ReadOnlyModel;
import org.apache.commons.collections4.map.HashedMap;
import play.db.jdbc.Table;
import play.mvc.Router;
import ppsdm.pojos.PbjMeta;

import javax.persistence.Transient;
import java.util.Date;
import java.util.Map;

/**
 * @author HanusaCloud on 11/8/2019 8:50 AM
 */
@Table(name = "jabfung_pbj", schema = "jabfung")
public class JabfungPbj extends ReadOnlyModel {

    public static final String TAG = "JabfungPbj";

    public String name;
    public String certificate_number;
    public String nip;
    public String institution_id;
    public String institution_name;
    public String satker_id;
    public String satker_name;
    public Date issued_date;
    public String level;
    public String rank;

    @Transient
    public Long total;
    @Transient
    private Long pertama;
    @Transient
    private Long muda;
    @Transient
    private Long madya;

    @Transient
    public PbjMeta meta;

    public PbjMeta getMeta() {
        if (meta == null) {
            if (pertama != null && muda != null && madya != null) {
                meta = new PbjMeta(muda, pertama, madya);
                return meta;
            }
            meta = new PbjMeta(institution_id);
        }
        return meta;
    }

    public Long getPertama() {
        return pertama;
    }

    public Long getMuda() {
        return muda;
    }

    public Long getMadya() {
        return madya;
    }

    public String getUrlBySatker() {
        Map<String, Object> map = new HashedMap<>();
        map.put("satker", satker_name);
        map.put("kode_kldi", institution_id);
        map.put("nama_kldi", institution_name);
        return Router.reverse(R.route.dashboard_pbjcontroller_leveltwo, map).url;
    }

}
