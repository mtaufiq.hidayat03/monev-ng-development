package ppsdm.models;

public class OccupationalType {

    public static final String POKJA_PEMILIHAN = "Pokja Pemilihan";
    public static final String PEJABAT_PENGADAAN = "Pejabat Pengadaan";
    public static final String PEJABAT_PEMBUAT_KOMITMEN = "Pejabat Pembuat Komitmen";

}
