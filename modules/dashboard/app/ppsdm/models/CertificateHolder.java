package ppsdm.models;

import constants.generated.R;
import models.common.ReadOnlyModel;
import models.common.contracts.annotations.SearchAble;
import org.apache.commons.collections4.map.HashedMap;
import play.db.jdbc.Table;
import play.libs.URLs;
import play.mvc.Router;
import ppsdm.pojos.PbjCertificateMeta;
import utils.common.LogUtil;

import javax.persistence.Transient;
import java.util.Date;
import java.util.Map;

/**
 * @author HanusaCloud on 11/7/2019 12:29 PM
 */
@Table(name = "sertifikat_kompetensi", schema = "jabfung")
public class CertificateHolder extends ReadOnlyModel {

    public static final String TAG = "CertificateHolder";

    @SearchAble
    public String certificate_number;
    @SearchAble
    public String name;
    @SearchAble
    public String nip;
    public String institution_id;
    public String institution_name;
    public String satker_id;
    public String satker_name;
    public Date issued_date;
    public String level;
    public String rank;

    @Transient
    public Long total;
    @Transient
    private Long pertama;
    @Transient
    private Long muda;
    @Transient
    private Long madya;
    @Transient
    public PbjCertificateMeta meta;

    public PbjCertificateMeta getMeta() {
        LogUtil.debug(TAG, meta);
        if (meta == null) {
            if (pertama != null && muda != null && madya != null) {
                meta = new PbjCertificateMeta(pertama, muda , madya);
                return meta;
            }
            meta = new PbjCertificateMeta(institution_id);
        }
        return meta;
    }

    public Long getPertama() {
        return pertama;
    }

    public Long getMuda() {
        return muda;
    }

    public Long getMadya() {
        return madya;
    }

    public String getUrlBySatker() {
        Map<String, Object> map = new HashedMap<>();
        map.put("satker", satker_name);
        map.put("kodeKldi", institution_id);
        map.put("namaKldi", institution_name);
        return Router.reverse(R.route.dashboard_pbjcertificatecontroller_leveltwo, map).url;
    }

}
