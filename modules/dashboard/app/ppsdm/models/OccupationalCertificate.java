package ppsdm.models;

import constants.generated.R;
import models.common.ReadOnlyModel;
import models.common.contracts.annotations.SearchAble;
import org.apache.commons.collections4.map.HashedMap;
import play.db.jdbc.Table;
import play.mvc.Router;
import ppsdm.pojos.OccupationalMeta;
import utils.common.LogUtil;
import javax.persistence.Transient;
import java.util.Date;
import java.util.Map;

@Table(name = "sertifikat_okupasi", schema = "jabfung")
public class OccupationalCertificate extends ReadOnlyModel {

    public static final String TAG = "OccupationalCertificate";
    @SearchAble
    public String certificate_number;
    @SearchAble
    public String name;
    @SearchAble
    public String nip;
    public String institution_id;
    public String institution_name;
    public String satker_id;
    public String satker_name;
    public Date issued_date;
    public String type;
    public String rank;

    @Transient
    public Long total;
    @Transient
    private Long pokjaPemilihan;
    @Transient
    private Long pejabatPengadaan;
    @Transient
    private Long pejabatPembuatKomitmen;
    @Transient
    public OccupationalMeta meta;

    public static Long getCount() {
        final Long total = CertificateHolder.count();
        LogUtil.debug(TAG, "total occupational certificate: " +total);
        return total;
    }

    public OccupationalMeta getMeta() {
        if (meta == null) {
            if (pokjaPemilihan != null && pejabatPengadaan != null && pejabatPembuatKomitmen != null) {
                meta = new OccupationalMeta(pokjaPemilihan , pejabatPengadaan, pejabatPembuatKomitmen);
                return meta;
            }
            meta = new OccupationalMeta(institution_id);
        }
        return meta;
    }

    public Long getPokjaPemilihan() {
        return pokjaPemilihan;
    }

    public Long getPejabatPengadaan() {
        return pejabatPengadaan;
    }

    public Long getPejabatPembuatKomitmen() {
        return pejabatPembuatKomitmen;
    }

    public String getInstitution() {
        return institution_name;
    }

    public String getUrlBySatker() {
        Map<String, Object> map = new HashedMap<>();
        map.put("satker", satker_name);
        map.put("kodeKldi", institution_id);
        map.put("namaKldi", institution_name);
        return Router.reverse(R.route.dashboard_occupationalcertificatecontroller_leveltwo, map).url;
    }
}
