package ppsdm.models;

import constants.generated.R;
import models.common.ReadOnlyModel;
import models.common.contracts.annotations.SearchAble;
import org.apache.commons.collections4.map.HashedMap;
import play.db.jdbc.Table;
import play.mvc.Router;
import ppsdm.pojos.BasicLevelCertificateHolderMeta;

import javax.persistence.Transient;
import java.util.Date;
import java.util.Map;

/**
 * @author HanusaCloud on 11/7/2019 12:29 PM
 */
@Table(name = "pemegang_sertifikat", schema = "jabfung")
public class BasicLevelCertificateHolder extends ReadOnlyModel {

    public static final String TAG = "CertificateHolder";

    @SearchAble
    public String name;
    @SearchAble
    public String certificate_number;
    public String nip;
    public String institution_id;
    @SearchAble
    public String institution_name;
    public String satker_id;
    public String satker_name;
    public Date issued_date;
    public String rank;

    @Transient
    public Long total;
    @Transient
    public BasicLevelCertificateHolderMeta meta;

    public BasicLevelCertificateHolderMeta getMeta() {
        if (meta == null) {
            if (total != null) {
                meta = new BasicLevelCertificateHolderMeta(total);
                return meta;
            }
            meta = new BasicLevelCertificateHolderMeta(institution_id);
        }
        return meta;
    }

    public Long getTotal() {
        return total;
    }

    public String getUrl() {
        Map<String, Object> map = new HashedMap<>();
        map.put("satker", satker_name);
        map.put("kode_kldi", institution_id);
        map.put("nama_kldi", institution_name);
        return Router.reverse(R.route.dashboard_basiclevelcertificateholdercontroller_leveltwo, map).url;
    }

}
