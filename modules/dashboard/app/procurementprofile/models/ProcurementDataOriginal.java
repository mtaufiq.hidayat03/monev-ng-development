package procurementprofile.models;

import models.user.management.CurrentUser;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import utils.common.FormatterUtil;
import utils.common.LogUtil;

import javax.persistence.Transient;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author HanusaCloud on 9/26/2019 10:41 AM
 */
@Table(name = "procurement_data_original", schema = "public")
public class ProcurementDataOriginal extends BaseTable {

    public static final String TAG = "ProcurementDataOriginal";

    @Id
    public Long id;
    public String header;
    public Long value;
    public Long pagu;
    public String value_text;
    public Timestamp created_at;
    public Long created_by;

    @Transient
    public ProfileSection profileSection;

    public ProcurementDataOriginal() {}

    public ProcurementDataOriginal(ProfileSection section) {
        this.header = section.getName();
    }

    public ProcurementDataOriginal(ProfileSection section, Long value) {
        this.header = section.getName();
        this.value = value;
    }

    public ProcurementDataOriginal(ProfileSection section, Long value, Long pagu) {
        this.header = section.getName();
        this.value = value;
        this.pagu = pagu;
    }


    public ProfileSection getProfileSection() {
        try {
            if (profileSection == null) {
                profileSection = ProfileSection.valueOf(header.toUpperCase());
            }
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }        
        return profileSection;
    }

    public String getLabel() {
        return getProfileSection().getLabel();
    }

    public int getOrder() {
        return getProfileSection() == null ? 100 : getProfileSection().getOrder();
    }

    public int getPage() {
        return getProfileSection().getPage();
    }

    public boolean isMultiple() {
        return getProfileSection().isMultiple();
    }

    public String getStringifyPagu() {
        return String.format("%s", pagu);
    }

    public String getStringifyValue() {
        return String.format("%s", value);
    }

    public String getFormattedValue() {
        return "Rp. " + FormatterUtil.currencyReadableFormatter(value);
    }

    public static List<ProcurementDataOriginal> getLatest() {
        final String now = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        final List<ProcurementDataOriginal> data = ProcurementDataOriginal.find(
                "DATE(created_at) = DATE('" + now +"') " +
                "ORDER BY created_at ASC").fetch();
        data.sort(comparator);
        return data;
    }

    public static List<ProcurementDataOriginal> getEmptyWithSection() {
        LogUtil.debug(TAG, "return original but empty except header");
        return Arrays.stream(ProfileSection.values())
                .map(ProcurementDataOriginal::new)
                .collect(Collectors.toList());
    }

    public final static Comparator<ProcurementDataOriginal> comparator = ((o1, o2) -> o1.getOrder() - o2.getOrder());

    @Override
    protected void prePersist() {
        super.prePersist();

        CurrentUser session = CurrentUser.getInstance();
        Long currentUser;
        if (session == null
                || session.user == null
                || (currentUser = session.user.getUserId()) == null) {
            currentUser = -999L;
        }

        if (this.id == null) {
            created_at = new Timestamp(System.currentTimeMillis());
            created_by = currentUser;
        }
        if (this.id == null) {
            id = Query.find("SELECT nextval('procurement_data_original_id_seq'::regclass)", Long.class).first();
        }
    }

}
