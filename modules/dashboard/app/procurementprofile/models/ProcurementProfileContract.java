package procurementprofile.models;

import constants.generated.R;
import models.common.contracts.CreationContract;
import org.apache.commons.lang3.StringUtils;
import permission.Kldi;
import play.mvc.Router;
import repositories.user.management.KldiRepository;
import utils.common.LogUtil;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author HanusaCloud on 11/12/2019 12:40 PM
 */
public interface ProcurementProfileContract extends CreationContract {

    String TAG = "ProcurementProfileContract";

    Long getId();
    String getKldiId();
    void setKldiName(String name);
    void setData(List<ProcurementData> data);
    List<ProcurementData> getData();
    ProcurementProfile.PublicationStatus getPublicationStatus();

    default String getPreviewUrl() {
        Map<String, Object> params = new HashMap<>();
        params.put("id", getId());
        return Router.reverse(R.route.core_landingcontroller_report, params).url;
    }

    default String getDownloadUrl() {
        Map<String, Object> params = new HashMap<>();
        params.put("id", getId());
        return Router.reverse(R.route.core_dummycontroller_downloadpdfprofile, params).url;
    }

    default void setModelKldiName() {
        if (StringUtils.isEmpty(getKldiId())) {
            LogUtil.debug(TAG, "Oops kldiId is empty");
            return;
        }
        Kldi kldi = KldiRepository.getInstance().getById(getKldiId());
        if (kldi == null) {
            LogUtil.debug(TAG, "Oops kldi is empty");
            return;
        }
        setKldiName(kldi.nama_instansi);
    }

    default void withDataByPage(Integer page) {
        setData(ProcurementData.getLatestBy(getId(), ProfileSection.getHeadersByPage(page)));
    }

    default void withData() {
        setData(ProcurementData.getLatestByProcurementId(getId()));
    }

    default Map<Integer, List<ProcurementData>> getDataAsMap() {
        return getData().stream()
                .collect(Collectors.groupingBy(
                        ProcurementData::getPage,
                        LinkedHashMap::new,
                        Collectors.toList())
                );
    }

    default void withDataOriginal() {
        LogUtil.debug(TAG, "with original data");
        List<ProcurementDataOriginal> results = ProcurementDataOriginal.getLatest();
        if (results.isEmpty()) {
            LogUtil.debug(TAG, "Oops.. it seems no new data create " +
                    "from profile section directly with empty value");
            results = ProcurementDataOriginal.getEmptyWithSection();
        }
        setData(results.stream()
                .map(p -> {
                    ProcurementData data = new ProcurementData(p);
                    data.generateExtra();
                    return data;
                })
                .collect(Collectors.toList()));
    }

    default String getDataAsString() {
        LogUtil.debug(TAG, "generate procurement data as string");
        if (getData() == null || getData().isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        for (ProcurementData data : getData()) {
            final String label = data.getProfileSection() != null ? data.getProfileSection().getLabel() : "";
            sb.append(label).append(" ").append(data.getStringifyValue());
            if (data.isMultiple()) {
                sb.append(" pagu ").append(data.getProfileSection()).append(" ");
            } else {
                sb.append(" ");
            }
        }
        return sb.toString();
    }

    default boolean isDataExist() {
        return getData() != null && !getData().isEmpty();
    }

    default boolean isPublish() {
        return getPublicationStatus() != null
                && getPublicationStatus() == ProcurementProfile.PublicationStatus.PUBLISHED;
    }

    default Integer getLatestVersion() {
        return ProcurementData.getLatestVersion(getId());
    }

}
