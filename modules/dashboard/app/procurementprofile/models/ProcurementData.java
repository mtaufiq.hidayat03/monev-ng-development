package procurementprofile.models;

import models.user.management.CurrentUser;
import org.apache.commons.lang3.StringUtils;
import play.data.binding.As;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import utils.common.FormatterUtil;
import utils.common.IndonesianNumberBinder;
import utils.common.LogUtil;

import javax.persistence.Transient;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Year;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author HanusaCloud on 9/26/2019 10:41 AM
 */
@Table(name = "procurement_data", schema = "public")
public class ProcurementData extends BaseTable {

    public static final String TAG = "ProcurementData";

    @Id
    public Long id;
    public Long procurement_id;
    public String header;
    public String section;
    public Integer flag;
    @As(binder = IndonesianNumberBinder.class)
    public Long value;
    @As(binder = IndonesianNumberBinder.class)
    public Long pagu;
    public Timestamp created_at;
    public Long created_by;
    public String extra;
    public String value_text;

    @Transient
    public ProfileSection profileSection;

    public ProcurementData() {}

    public ProcurementData(ProcurementDataOriginal original) {
        this.header = original.header;
        this.value = original.value;
        this.pagu = original.pagu;
        this.value_text = original.value_text;
        getProfileSection();
    }

    public ProcurementData(Long procurementId, Long value, ProfileSection section) {
        this.procurement_id = procurementId;
        this.header = section.getName();
        this.section = section.getGroup();
        this.flag = 0;
        this.value = value;
    }

    public ProcurementData(Long procurementId, Long value, ProfileSection section, Integer flag) {
        this.procurement_id = procurementId;
        this.header = section.getName();
        this.section = section.getGroup();
        this.flag = flag;
        this.value = value;
    }

    public ProcurementData(Long procurementId, Long value, ProfileSection section, Long pagu) {
        this.procurement_id = procurementId;
        this.header = section.getName();
        this.section = section.getGroup();
        this.flag = 0;
        this.value = value;
        this.pagu = pagu;
    }

    public ProfileSection getProfileSection() {
        try {
            if (profileSection == null) {
                profileSection = ProfileSection.valueOf(header.toUpperCase());
            }
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }        
        return profileSection;
    }

    public int getNumberFromLabel(ProfileSection profileSection) {
        if (profileSection == null) {
            return 0;
        }
        return getNumberFromAndDecreaseByOne(profileSection.getName());
    }

    public int getNumberFromAndDecreaseByOne(String label) {
        if (StringUtils.isEmpty(label)) {
            return 0;
        }
        LogUtil.debug(TAG, "get number form label: " + label);
        Matcher m = NUMBER_PATTERN.matcher(label);
        String result = m.find() ? m.group("number") : "0";
        LogUtil.debug(TAG, "get number result: " + result);
        final int value = !StringUtils.isEmpty(result) &&  StringUtils.isNumeric(result)
                ? Integer.valueOf(result) - 1
                : 0;
        LogUtil.debug(TAG, "result: " + result);
        return value;
    }

    public int getCalculatedYear(int currentYear) {
        final int year = currentYear - getNumberFromLabel(getProfileSection());
        LogUtil.debug(TAG, "calculated year result: " + year);
        return year;
    }

    private static final Pattern NUMBER_PATTERN = Pattern.compile("(?:_)(?<number>[\\d]+)$");

    public String getCalculatedMonth(LocalDate localDate) {
        int decrease = getNumberFromLabel(getProfileSection());
        Date date = java.sql.Date.valueOf(localDate.minusMonths(decrease));
        LogUtil.debug(TAG, date);
        return new SimpleDateFormat("MMM yyyy").format(date);
    }

    public void generateExtra() {
        LogUtil.debug(TAG, "generate extra");
        if (getProfileSection() == null) {
            LogUtil.debug(TAG, "Oops profile section is null");
            return;
        }
        if (!getProfileSection().extraExist()) {
            LogUtil.debug(TAG, "Oops.. does not have extra");
            return;
        }
        if (getProfileSection().isExtraYear()) {
            LogUtil.debug(TAG, "extra type year");
            extra = String.valueOf(getCalculatedYear(Year.now().getValue()));
            return;
        }
        if (getProfileSection().isMonth()) {
            LogUtil.debug(TAG, "extra type month");
            extra = getCalculatedMonth(LocalDate.now());
        }
    }

    private String getGeneratedExtra() {
        if (getProfileSection() == null || !getProfileSection().extraExist()) {
            return "";
        }
        return StringUtils.isEmpty(extra) ? "" : extra;
    }

    public String getLabel() {
        if (getProfileSection().extraExist()) {
            return getProfileSection().getLabel() + " " + getGeneratedExtra();
        }
        return getProfileSection().getLabel();
    }

    public int getOrder() {
        return getProfileSection() == null ? 100 : getProfileSection().getOrder();
    }

    public int getPage() {
        return getProfileSection().getPage();
    }

    public boolean isMultiple() {
        return getProfileSection().isMultiple();
    }

    public String getStringifyPagu() {
        if (pagu == null) {
            return "";
        }
        return String.format("%s", pagu);
    }

    public String getStringifyValue() {
        if (value == null) {
            return "";
        }
        return String.format("%s", value);
    }

    public String getFormattedValue() {
        return "Rp. " + FormatterUtil.currencyReadableFormatter(value);
    }

    public String getValueText() {
        return value_text;
    }

    public String getExtra() {
        return extra;
    }

    @Override
    protected void prePersist() {
        super.prePersist();

        CurrentUser session = CurrentUser.getInstance();
        Long currentUser;
        if (session == null
                || session.user == null
                || (currentUser = session.user.getUserId()) == null) {
            currentUser = -999L;
        }

        if (this.id == null) {
            created_at = new Timestamp(System.currentTimeMillis());
            created_by = currentUser;
        }
        if (this.id == null) {
            id = Query.find("SELECT nextval('procurement_data_id_seq'::regclass)", Long.class).first();
        }
    }

    public static List<ProcurementData> getByProcurementId(Long procurementId) {
        return ProcurementData.find("procurement_id = ? ORDER BY created_at ASC", procurementId).fetch();
    }

    public static Integer getLatestVersion(Long procurementId) {
        if (procurementId == null || procurementId == 0) {
            return 0;
        }
        LogUtil.debug(TAG, "get latest by procurement id");
        return Query.find("SELECT COALESCE(MAX(flag), 0) as latest " +
                "FROM procurement_data " +
                "WHERE procurement_id =?", Integer.class, procurementId).first();
    }

    public static List<ProcurementData> getLatestByProcurementId(Long procurementId) {
        final Integer latestVersion = getLatestVersion(procurementId);
        if (latestVersion == null) {
            return Collections.emptyList();
        }
        LogUtil.debug(TAG, "latest flag: " + latestVersion);
        final List<ProcurementData> data = ProcurementData.find("procurement_id = ? " +
                "AND flag = ? " +
                "ORDER BY created_at ASC", procurementId, latestVersion).fetch();
        data.sort(comparator);
        return data;
    }

    public static List<ProcurementData> getLatestBy(Long procurementId, List<String> headers) {
        final Integer latestVersion = getLatestVersion(procurementId);
        if (latestVersion == null) {
            return Collections.emptyList();
        }
        StringBuilder sb = new StringBuilder();
        String glue = "";
        for (String s : headers) {
            sb.append(glue).append("'").append(s).append("'");
            glue = ", ";
        }
        final String headerString = sb.toString();
        LogUtil.debug(TAG, "with headers: " + headerString);
        LogUtil.debug(TAG, "latest flag: " + latestVersion);
        final List<ProcurementData> data = ProcurementData.find("procurement_id = ? " +
                "AND flag = ? " +
                "AND header IN (?) " +
                "ORDER BY created_at ASC", procurementId, latestVersion, headerString).fetch();
        data.sort(comparator);
        return data;
    }

    public static Comparator<ProcurementData> comparator = new Comparator<ProcurementData>() {

        public int compare(ProcurementData s1, ProcurementData s2) {
            return s1.getOrder() - s2.getOrder();
        }
    };

}
