package procurementprofile.models;

import models.cms.ContentPermission;
import models.cms.ContentStatus;
import models.cms.ContentType;
import models.common.BaseModel;
import models.common.contracts.PermissionContract;
import models.common.contracts.annotations.SearchAble;
import models.common.contracts.annotations.SoftDelete;
import models.searchcontent.contracts.ElasticItemDetail;
import models.searchcontent.contracts.ElasticRequestPayloadContract;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Table;

import javax.persistence.EnumType;
import javax.persistence.Transient;
import java.sql.Timestamp;
import java.util.List;

import static procurementprofile.models.ProcurementProfile.PublicationStatus.DRAFT;
import static procurementprofile.models.ProcurementProfile.PublicationStatus.PUBLISHED;

/**
 * @author HanusaCloud on 9/26/2019 10:36 AM
 */
@Table(name = "PUBLIC.PROCUREMENT_PROFILES")
@SoftDelete
public class ProcurementProfile extends BaseModel implements ProcurementProfileContract,
        ElasticRequestPayloadContract,
        ElasticItemDetail,
        PermissionContract
{

    @Enumerated(EnumType.STRING)
    public enum PublicationStatus {
        DRAFT, PUBLISHED, UNPUBLISHED, WAITING_FOR_APPROVAL
    }

    @SearchAble
    public String title;
    @SearchAble
    public PublicationStatus status;
    public Timestamp mined_at;
    public Timestamp adjusted_at;
    public Long adjusted_by;
    public String kldi_id;

    @Transient
    public List<ProcurementData> data;
    @Transient
    public String kldi_name;
    @Transient
    public String[] content_permission;
    @Transient
    public String[] publishing_status = new String[]{ContentStatus.PUBLISH.name};
    @Transient
    public String content;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public String getKldiId() {
        return kldi_id;
    }

    @Override
    public void setKldiName(String name) {
        this.kldi_name = name;
    }

    @Override
    public void setData(List<ProcurementData> data) {
        this.data = data;
    }

    public boolean isPublished() {
        return status == PUBLISHED;
    }

    public List<ProcurementData> getData() {
        return data;
    }

    @Override
    public PublicationStatus getPublicationStatus() {
        return status;
    }

    public ProcurementData getBySection(ProfileSection section) {
        for (ProcurementData data : getData()) {
            if (data.getProfileSection() == section) {
                return data;
            }
        }
        return null;
    }

    public Long getValueBySection(ProfileSection section) {
        ProcurementData data;
        if ((data = getBySection(section)) != null) {
            return data.value;
        }
        return 0L;
    }

    public String getValueForDescription() {
        StringBuilder sb = new StringBuilder();
        if (getData() != null && !getData().isEmpty()) {
            for (ProcurementData data : getData()) {

            }
        }
        return sb.toString();
    }

    @Override
    public Long getModelId() {
        return id;
    }

    @Override
    public String getModelType() {
        return ContentType.PROCUREMENT_PROFILE;
    }

    @Override
    public String getModelUrl() {
        return getPreviewUrl();
    }

    @Override
    public void setModelRequirements() {
        setModelKldiName();
        withData();
        content_permission = new String[]{ContentPermission.PUBLIC.toLowerCase()};
        content = getDataAsString();
        data = null;
    }

    @Override
    public Timestamp getCreatedAt() {
        return created_at;
    }

    @Override
    public String getDetailTitle() {
        return title;
    }

    @Override
    public String getDetailContent() {
        return getValueForDescription();
    }

    @Override
    public String getMeta() {
        return "";
    }

    @Override
    public String getPermission() {
        return status == DRAFT ? ContentPermission.PRIVATE : ContentPermission.PUBLIC;
    }

    @Override
    public String getDetailDownloadFileUrl() {
        return getDownloadUrl();
    }
}
