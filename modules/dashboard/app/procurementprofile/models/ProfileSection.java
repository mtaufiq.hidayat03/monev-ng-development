package procurementprofile.models;


import org.apache.commons.lang3.StringUtils;
import play.db.jdbc.Enumerated;

import javax.persistence.EnumType;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * @author HanusaCloud on 9/26/2019 12:55 PM
 */
@Enumerated(EnumType.ORDINAL)
public enum  ProfileSection {

    //START AUTO GENERATED
	BKLP(
		"bklp",
		"repn1",
		"Belanja Kementerian, Lembaga, Pemda",
		362,
		6
	),
	LPSE_STANDAR(
		"lpse_standar",
		"repn2",
		"Unit Layanan Pengadaan Secara Elektronik Standar",
		369,
		6
	),
	LPSE_2(
		"lpse_2",
		"repn2",
		"Layanan Pengadaan Secara Elektronik (LPSE)",
		368,
		6
	),
	LPSE_1(
		"lpse_1",
		"repn2",
		"Unit Layanan Pengadaan Secara Elektronik",
		367,
		6
	),
	UKPBK_PERMANEN_STRUKTURAL(
		"ukpbk_permanen_struktural",
		"repn2",
		"Unit Layanan Pengadaan Dan Unit Kerja Pengadaan Barang/Jasa Permanen/Struktural",
		366,
		6
	),
	P6_ULP_UKPBJ(
		"p6_ulp_ukpbj",
		"repn2",
		"Unit Layanan Pengadaan Dan Unit Kerja Pengadaan Barang/Jasa",
		365,
		6
	),
	PERSENTASE_BELANJA_PENGADAAN(
		"persentase_belanja_pengadaan",
		"repn1",
		"Persentase Belanja Pengadaan",
		364,
		6
	),
	HARGA_BELANJA_PENGADAAN(
		"harga_belanja_pengadaan",
		"repn1",
		"Harga Belanja Pengadaan",
		363,
		6
	),
	PURCHASING_RP(
		"purchasing_rp",
		"pengadaan2_list_rp",
		"E-Purchasing Rencana Pengadaan Barang/Jasa",
		390,
		7
	),
	IPP_LD_PAKET(
		"ipp_ld_paket",
		"pengadaan1",
		"Inovasi & Percepatan Pengadaan Tender Dini Paket",
		370,
		7
	),
	IPP_LSC_PAGU(
		"ipp_lsc_pagu",
		"pengadaan1",
		"Inovasi & Percepatan Pengadaan Tender/Seleksi Cepat Dini Pagu",
		373,
		7
	),
	IPP_LI_PAKET(
		"ipp_li_paket",
		"pengadaan1",
		"Inovasi & Percepatan Pengadaan Tender Itemized Paket",
		374,
		7
	),
	IPP_LI_PAGU(
		"ipp_li_pagu",
		"pengadaan1",
		"Inovasi & Percepatan Pengadaan Tender Itemized Pagu",
		375,
		7
	),
	IPP_KONSOLIDASI_PAKET(
		"ipp_konsolidasi_paket",
		"pengadaan1",
		"Inovasi & Percepatan Pengadaan Konsolidasi Paket",
		376,
		7
	),
	IPP_KONSOLIDASI_PAGU(
		"ipp_konsolidasi_pagu",
		"pengadaan1",
		"Inovasi & Percepatan Pengadaan Konsolidasi Pagu",
		377,
		7
	),
	IPP_PAYUNG_PAKET(
		"ipp_payung_paket",
		"pengadaan1",
		"Inovasi & Percepatan Pengadaan Kontrak Payung Paket",
		378,
		7
	),
	IPP_PAYUNG_PAGU(
		"ipp_payung_pagu",
		"pengadaan1",
		"Inovasi & Percepatan Pengadaan Kontrak Payung Pagu",
		379,
		7
	),
	IPP_JAMAK_PAKET(
		"ipp_jamak_paket",
		"pengadaan1",
		"Inovasi & Percepatan Pengadaan Kontrak Tahun Jamak Paket",
		380,
		7
	),
	IPP_JAMAK_PAGU(
		"ipp_jamak_pagu",
		"pengadaan1",
		"Inovasi & Percepatan Pengadaan Kontrak Tahun Jamak Pagu",
		381,
		7
	),
	PSUK_RUP_PAKET(
		"psuk_rup_paket",
		"pengadaan1",
		"Peran Serta Usaha Kecil RUP Paket",
		382,
		7
	),
	PSUK_RUP_PAGU(
		"psuk_rup_pagu",
		"pengadaan1",
		"Peran Serta Usaha Kecil RUP Pagu",
		383,
		7
	),
	PSUK_REALISASI_PAKET(
		"psuk_realisasi_paket",
		"pengadaan1",
		"Peran Serta Usaha Kecil Realisasi Paket",
		384,
		7
	),
	PSUK_REALISASI_PAGU(
		"psuk_realisasi_pagu",
		"pengadaan1",
		"Peran Serta Usaha Kecil Realisasi Pagu",
		385,
		7
	),
	PERSENTASE_RP(
		"persentase_rp",
		"pengadaan2",
		"Persentase Rencana Pengadaan Barang/Jasa",
		386,
		7
	),
	HARGA_RP(
		"harga_rp",
		"pengadaan2",
		"Harga Rencana Pengadaan Barang/Jasa",
		387,
		7
	),
	SWAKELOLA_RP(
		"swakelola_rp",
		"pengadaan2_list_rp",
		"Swakelola Rencana Pengadaan Barang/Jasa",
		388,
		7
	),
	TENDERING_RP(
		"tendering_rp",
		"pengadaan2_list_rp",
		"E-Tendering Rencana Pengadaan Barang/Jasa",
		389,
		7
	),
	IPP_LSC_PAKET(
		"ipp_lsc_paket",
		"pengadaan1",
		"Inovasi & Percepatan Pengadaan Tender/Seleksi Cepat Paket",
		372,
		7
	),
	PENGADAAN_LANGSUNG_RP(
		"pengadaan_langsung_rp",
		"pengadaan2_list_rp",
		"Pengadaan Langsung Rencana Pengadaan Barang/Jasa",
		391,
		7
	),
	PENUNJUKAN_LANGSUNG_RP(
		"penunjukan_langsung_rp",
		"pengadaan2_list_rp",
		"Penunjukan Langsung Rencana Pengadaan Barang/Jasa",
		392,
		7
	),
	SAYEMBARA_KONTES_RP(
		"sayembara_kontes_rp",
		"pengadaan2_list_rp",
		"Sayembara Dan Kontes Rencana Pengadaan Barang/Jasa",
		393,
		7
	),
	LAINNYA_RP(
		"lainnya_rp",
		"pengadaan2_list_rp",
		"Lainnya Rencana Pengadaan Barang/Jasa",
		394,
		7
	),
	PAGU_PENGUMUMAN_ET(
		"pagu_pengumuman_et",
		"pengadaan3",
		"Pagu Pengumuman",
		395,
		7
	),
	HASIL_TENDER_ET(
		"hasil_tender_et",
		"pengadaan3",
		"Hasil Tender",
		396,
		7
	),
	NILAI_HARGA_EP(
		"nilai_harga_ep",
		"pengadaan3",
		"Nilai Harga Pesanan",
		397,
		7
	),
	PPB_DASAR(
		"ppb_dasar",
		"pengadaan4",
		"Pelaku Pengadaan Bersetifikat Tingkat Dasar PBJP",
		398,
		7
	),
	PPBK(
		"ppbk",
		"pengadaan4",
		"Pelaku Pengadaan Bersetifikat Kompetensi",
		399,
		7
	),
	PPBK_JF(
		"ppbk_jf",
		"pengadaan4",
		"Jabatan Fungsional",
		400,
		7
	),
	PD_SPSE(
		"pd_spse",
		"pengadaan4",
		"Penyedia Dalam SPSE",
		401,
		7
	),
	SANKSI_DF(
		"sanksi_df",
		"pengadaan4",
		"Daftar Hitam",
		402,
		7
	),
	PH_LAYANAN_ADVOKASI(
		"ph_layanan_advokasi",
		"pengadaan4",
		"Permasalahan Hukum Layanan Advokasi",
		403,
		7
	),
	PH_LAYANAN_ADVOKASI_VALUE(
		"ph_layanan_advokasi_value",
		"pengadaan4",
		"Nilai Layanan Advokasi",
		404,
		7
	),
	PH_KASUS_PENGADAAN(
		"ph_kasus_pengadaan",
		"pengadaan4",
		"Permasalahan Hukum Kasus Pengadaan",
		405,
		7
	),
	PH_NILAI_KASUS(
		"ph_nilai_kasus",
		"pengadaan4",
		"Permasalahan Hukum Nilai Kasus",
		406,
		7
	),
	PH_KONTRAK(
		"ph_kontrak",
		"pengadaan4",
		"Permasalahan Hukum Kontrak",
		407,
		7
	),
	PH_NILAI_KONTRAK(
		"ph_nilai_kontrak",
		"pengadaan4",
		"Permasalahan Hukum Nilai Kontrak",
		408,
		7
	),
	IPP_LD_PAGU(
		"ipp_ld_pagu",
		"pengadaan1",
		"Inovasi & Percepatan Pengadaan Tender Dini Pagu",
		371,
		7
	),
	BP_2018(
		"bp_2018",
		"pbjn1",
		"Belanja Pengadaan 218",
		409,
		8
	),
	PERENCANAAN_PENGADAAN(
		"perencanaan_pengadaan",
		"pbjn1",
		"Perencanaan Pengadaan",
		410,
		8
	),
	PERSENTASE_PENYEDIA(
		"persentase_penyedia",
		"pbjn1",
		"Persentase Penyedia",
		411,
		8
	),
	HARGA_PENYEDIA(
		"harga_penyedia",
		"pbjn1",
		"Harga Penyedia",
		412,
		8
	),
	PAKET_PENYEDIA(
		"paket_penyedia",
		"pbjn1",
		"Paket Penyedia",
		413,
		8
	),
	RWPP_LIST_ITEM_17(
		"rwpp_list_item_17",
		"rwpp_list",
		"Waktu Pemilihan Penyedia",
		457,
		8,
		"month"
	),
	HARGA_SWAKELOLA(
		"harga_swakelola",
		"pbjn1",
		"Harga Swakelola",
		415,
		8
	),
	PAKET_SWAKELOLA(
		"paket_swakelola",
		"pbjn1",
		"Paket Swakelola",
		416,
		8
	),
	JP_PK_HARGA(
		"jp_pk_harga",
		"pbjn1",
		"Harga Pekerjaan Konstruksi",
		417,
		8
	),
	JP_PK_PAKET(
		"jp_pk_paket",
		"pbjn1",
		"Paket Pekerjaan Konstruksi",
		418,
		8
	),
	JP_BARANG_HARGA(
		"jp_barang_harga",
		"pbjn1",
		"Harga Barang",
		419,
		8
	),
	JP_BARANG_PAKET(
		"jp_barang_paket",
		"pbjn1",
		"Paket Barang",
		420,
		8
	),
	JP_JK_HARGA(
		"jp_jk_harga",
		"pbjn1",
		"Harga Jasa Konsultasi",
		421,
		8
	),
	JP_JK_PAKET(
		"jp_jk_paket",
		"pbjn1",
		"Paket Jasa Konsultasi",
		422,
		8
	),
	JP_JL_HARGA(
		"jp_jl_harga",
		"pbjn1",
		"Harga Jasa Lainnya",
		423,
		8
	),
	JP_JL_PAKET(
		"jp_jl_paket",
		"pbjn1",
		"Paket Jasa Lainnya",
		424,
		8
	),
	PENGADAAN_PAGU(
		"pengadaan_pagu",
		"pbjn2",
		"Pengadaan Langsung Pagu",
		425,
		8
	),
	PENGADAAN_PAKET(
		"pengadaan_paket",
		"pbjn2",
		"Pengadaan Langsung Paket",
		426,
		8
	),
	PENUNJUKAN_PAGU(
		"penunjukan_pagu",
		"pbjn2",
		"Penunjukan Langsung Pagu",
		427,
		8
	),
	PENUNJUKAN_PAKET(
		"penunjukan_paket",
		"pbjn2",
		"Penunjukan Langsung Paket",
		428,
		8
	),
	PURCHASING_PAGU(
		"purchasing_pagu",
		"pbjn2",
		"E-Purchasing Pagu",
		429,
		8
	),
	PURCHASING_PAKET(
		"purchasing_paket",
		"pbjn2",
		"E-Purchasing Paket",
		430,
		8
	),
	TENDER_PAGU(
		"tender_pagu",
		"pbjn2",
		"Tender Pagu",
		431,
		8
	),
	TENDER_PAKET(
		"tender_paket",
		"pbjn2",
		"Tender Paket",
		432,
		8
	),
	TENDER_CEPAT_PAGU(
		"tender_cepat_pagu",
		"pbjn2",
		"Tender Cepat Pagu",
		433,
		8
	),
	TENDER_CEPAT_PAKET(
		"tender_cepat_paket",
		"pbjn2",
		"Tender Cepat Paket",
		434,
		8
	),
	SELEKSI_PAGU(
		"seleksi_pagu",
		"pbjn2",
		"Seleksi Pagu",
		435,
		8
	),
	SELEKSI_PAKET(
		"seleksi_paket",
		"pbjn2",
		"Seleksi Paket",
		436,
		8
	),
	SDK_PAGU(
		"sdk_pagu",
		"pbjn2",
		"Sayembara Dan Kontes Pagu",
		437,
		8
	),
	SDK_PAKET(
		"sdk_paket",
		"pbjn2",
		"Sayembara Dan Kontes Paket",
		438,
		8
	),
	LAINNYA_PAGU(
		"lainnya_pagu",
		"pbjn2",
		"Lainnya Pagu",
		439,
		8
	),
	LAINNYA_PAKET(
		"lainnya_paket",
		"pbjn2",
		"Lainnya Paket",
		440,
		8
	),
	RWPP_LIST_ITEM_1(
		"rwpp_list_item_1",
		"rwpp_list",
		"Waktu Pemilihan Penyedia",
		441,
		8,
		"month"
	),
	RWPP_LIST_ITEM_2(
		"rwpp_list_item_2",
		"rwpp_list",
		"Waktu Pemilihan Penyedia",
		442,
		8,
		"month"
	),
	RWPP_LIST_ITEM_3(
		"rwpp_list_item_3",
		"rwpp_list",
		"Waktu Pemilihan Penyedia",
		443,
		8,
		"month"
	),
	RWPP_LIST_ITEM_4(
		"rwpp_list_item_4",
		"rwpp_list",
		"Waktu Pemilihan Penyedia ",
		444,
		8,
		"month"
	),
	RWPP_LIST_ITEM_5(
		"rwpp_list_item_5",
		"rwpp_list",
		"Waktu Pemilihan Penyedia",
		445,
		8,
		"month"
	),
	RWPP_LIST_ITEM_6(
		"rwpp_list_item_6",
		"rwpp_list",
		"Waktu Pemilihan Penyedia",
		446,
		8,
		"month"
	),
	RWPP_LIST_ITEM_7(
		"rwpp_list_item_7",
		"rwpp_list",
		"Waktu Pemilihan Penyedia",
		447,
		8,
		"month"
	),
	RWPP_LIST_ITEM_8(
		"rwpp_list_item_8",
		"rwpp_list",
		"Waktu Pemilihan Penyedia",
		448,
		8,
		"month"
	),
	RWPP_LIST_ITEM_9(
		"rwpp_list_item_9",
		"rwpp_list",
		"Waktu Pemilihan Penyedia",
		449,
		8,
		"month"
	),
	RWPP_LIST_ITEM_10(
		"rwpp_list_item_10",
		"rwpp_list",
		"Waktu Pemilihan Penyedia",
		450,
		8,
		"month"
	),
	RWPP_LIST_ITEM_11(
		"rwpp_list_item_11",
		"rwpp_list",
		"Waktu Pemilihan Penyedia",
		451,
		8,
		"month"
	),
	RWPP_LIST_ITEM_12(
		"rwpp_list_item_12",
		"rwpp_list",
		"Waktu Pemilihan Penyedia",
		452,
		8,
		"month"
	),
	RWPP_LIST_ITEM_13(
		"rwpp_list_item_13",
		"rwpp_list",
		"Waktu Pemilihan Penyedia",
		453,
		8,
		"month"
	),
	RWPP_LIST_ITEM_14(
		"rwpp_list_item_14",
		"rwpp_list",
		"Waktu Pemilihan Penyedia",
		454,
		8,
		"month"
	),
	RWPP_LIST_ITEM_15(
		"rwpp_list_item_15",
		"rwpp_list",
		"Waktu Pemilihan Penyedia",
		455,
		8,
		"month"
	),
	RWPP_LIST_ITEM_16(
		"rwpp_list_item_16",
		"rwpp_list",
		"Waktu Pemilihan Penyedia",
		456,
		8,
		"month"
	),
	PERSENTASE_SWAKELOLA(
		"persentase_swakelola",
		"pbjn1",
		"Persentase Swakelola",
		414,
		8
	),
	WPML_LIST_ITEM_14(
		"wpml_list_item_14",
		"wpml_list",
		"Waktu Pengumuman Mulai Tender",
		487,
		9,
		"month"
	),
	HARGA_PAGU_PENGUMUMAN(
		"harga_pagu_pengumuman",
		"pepbjn1",
		"Harga Pagu Pengumuman",
		458,
		9
	),
	PAKET_PAGU_PENGUMUMAN(
		"paket_pagu_pengumuman",
		"pepbjn1",
		"Paket Pagu Pengumuman",
		459,
		9
	),
	HARGA_HPS_PP(
		"harga_hps_pp",
		"pepbjn1",
		"Harga HPS Pemilihan Penyedia",
		460,
		9
	),
	PAKET_HPS_PP(
		"paket_hps_pp",
		"pepbjn1",
		"Paket HPS Pemilihan Penyedia",
		461,
		9
	),
	HARGA_HASIL_TENDER(
		"harga_hasil_tender",
		"pepbjn1",
		"Harga Hasil Tender",
		462,
		9
	),
	PAKET_HASIL_TENDER(
		"paket_hasil_tender",
		"pepbjn1",
		"Paket Hasil Tender",
		463,
		9
	),
	HARGA_PENGADAAN_BARANG(
		"harga_pengadaan_barang",
		"pepbjn2",
		"Harga Pengadaan Barang",
		464,
		9
	),
	PAKET_PENGADAAN_BARANG(
		"paket_pengadaan_barang",
		"pepbjn2",
		"Paket Pengadaan Barang",
		465,
		9
	),
	HARGA_BARANG_JP(
		"harga_barang_jp",
		"pepbjn2",
		"Jenis Pengadaan Harga Barang",
		466,
		9
	),
	PAKET_BARANG_JP(
		"paket_barang_jp",
		"pepbjn2",
		"Jenis Pengadaan Paket Barang",
		467,
		9
	),
	HARGA_JKBU_JP(
		"harga_jkbu_jp",
		"pepbjn2",
		"Jenis Pengadaan Harga Jasa Konsultasi Badan",
		468,
		9
	),
	PAKET_JKBU_JP(
		"paket_jkbu_jp",
		"pepbjn2",
		"Jenis Pengadaan Paket Jasa Konsultasi Badan",
		469,
		9
	),
	HARGA_JKP_JP(
		"harga_jkp_jp",
		"pepbjn2",
		"Jenis Pengadaan Harga Jasa Konsultasi",
		470,
		9
	),
	PAKET_JKP_JP(
		"paket_jkp_jp",
		"pepbjn2",
		"Jenis Pengadaan Paket Jasa Konsultasi",
		471,
		9
	),
	HARGA_JL_JP(
		"harga_jl_jp",
		"pepbjn2",
		"Jenis Pengadaan Harga Jasa Lainnya",
		472,
		9
	),
	PAKET_JL_JP(
		"paket_jl_jp",
		"pepbjn2",
		"Jenis Pengadaan Paket Jasa Lainnya",
		473,
		9
	),
	WPML_LIST_ITEM_1(
		"wpml_list_item_1",
		"wpml_list",
		"Waktu Pengumuman Mulai Tender",
		474,
		9,
		"month"
	),
	SELISIH_PAGU_PENGUMUMAN(
		"selisih_pagu_pengumuman",
		"p9_spp",
		"Selisih Pagu Pengumuman Dengan Hasil Tender",
		499,
		9
	),
	MPC_PAKET(
		"mpc_paket",
		"pepbjn4",
		"Metode Pengadaan Seleksi Cepat Paket",
		498,
		9
	),
	MPC_HPS(
		"mpc_hps",
		"pepbjn4",
		"Metode Pengadaan Seleksi Cepat HPS",
		497,
		9
	),
	MPS_PAKET(
		"mps_paket",
		"pepbjn4",
		"Metode Pengadaan Seleksi Paket",
		496,
		9
	),
	MPS_HPS(
		"mps_hps",
		"pepbjn4",
		"Metode Pengadaan Seleksi HPS",
		495,
		9
	),
	MPTC_PAKET(
		"mptc_paket",
		"pepbjn4",
		"Metode Pengadaan Tender Cepat Paket",
		494,
		9
	),
	MPTC_HPS(
		"mptc_hps",
		"pepbjn4",
		"Metode Pengadaan Tender Cepat HPS",
		493,
		9
	),
	MPT_PAKET(
		"mpt_paket",
		"pepbjn4",
		"Metode Pengadaan Tender Paket",
		492,
		9
	),
	MPT_HPS(
		"mpt_hps",
		"pepbjn4",
		"Metode Pengadaan Tender HPS",
		491,
		9
	),
	WPML_LIST_ITEM_17(
		"wpml_list_item_17",
		"wpml_list",
		"Waktu Pengumuman Mulai Tender",
		490,
		9,
		"month"
	),
	WPML_LIST_ITEM_16(
		"wpml_list_item_16",
		"wpml_list",
		"Waktu Pengumuman Mulai Tender",
		489,
		9,
		"month"
	),
	WPML_LIST_ITEM_15(
		"wpml_list_item_15",
		"wpml_list",
		"Waktu Pengumuman Mulai Tender",
		488,
		9,
		"month"
	),
	WPML_LIST_ITEM_2(
		"wpml_list_item_2",
		"wpml_list",
		"Waktu Pengumuman Mulai Tender",
		475,
		9,
		"month"
	),
	WPML_LIST_ITEM_13(
		"wpml_list_item_13",
		"wpml_list",
		"Waktu Pengumuman Mulai Tender",
		486,
		9,
		"month"
	),
	WPML_LIST_ITEM_12(
		"wpml_list_item_12",
		"wpml_list",
		"Waktu Pengumuman Mulai Tender",
		485,
		9,
		"month"
	),
	WPML_LIST_ITEM_11(
		"wpml_list_item_11",
		"wpml_list",
		"Waktu Pengumuman Mulai Tender",
		484,
		9,
		"month"
	),
	WPML_LIST_ITEM_10(
		"wpml_list_item_10",
		"wpml_list",
		"Waktu Pengumuman Mulai Tender",
		483,
		9,
		"month"
	),
	WPML_LIST_ITEM_9(
		"wpml_list_item_9",
		"wpml_list",
		"Waktu Pengumuman Mulai Tender",
		482,
		9,
		"month"
	),
	WPML_LIST_ITEM_8(
		"wpml_list_item_8",
		"wpml_list",
		"Waktu Pengumuman Mulai Tender",
		481,
		9,
		"month"
	),
	WPML_LIST_ITEM_7(
		"wpml_list_item_7",
		"wpml_list",
		"Waktu Pengumuman Mulai Tender",
		480,
		9,
		"month"
	),
	WPML_LIST_ITEM_6(
		"wpml_list_item_6",
		"wpml_list",
		"Waktu Pengumuman Mulai Tender",
		479,
		9,
		"month"
	),
	WPML_LIST_ITEM_5(
		"wpml_list_item_5",
		"wpml_list",
		"Waktu Pengumuman Mulai Tender",
		478,
		9,
		"month"
	),
	WPML_LIST_ITEM_4(
		"wpml_list_item_4",
		"wpml_list",
		"Waktu Pengumuman Mulai Tender",
		477,
		9,
		"month"
	),
	WPML_LIST_ITEM_3(
		"wpml_list_item_3",
		"wpml_list",
		"Waktu Pengumuman Mulai Tender",
		476,
		9,
		"month"
	),
	PTEN_KOMODITAS_PAKET_1(
		"pten_komoditas_paket_1",
		"p10_komoditas_list",
		"Paket Komoditas",
		528,
		10
	),
	PTEN_KOMODITAS_PAKET_9(
		"pten_komoditas_paket_9",
		"p10_komoditas_list",
		"Paket Komoditas",
		552,
		10
	),
	KOMODITAS_HARGA_PESANAN(
		"komoditas_harga_pesanan",
		"peppbjn1",
		"Komoditas Harga Pesanan",
		502,
		10
	),
	PRODUK_HARGA_PESANAN(
		"produk_harga_pesanan",
		"peppbjn1",
		"Produk Harga Pesanan",
		503,
		10
	),
	PENYEDIA_HARGA_PESANAN(
		"penyedia_harga_pesanan",
		"peppbjn1",
		"Penyedia Harga Pesanan",
		504,
		10
	),
	KOMODITAS_NASIONAL(
		"komoditas_nasional",
		"peppbjn2",
		"Komoditas Nasional",
		505,
		10
	),
	PRODUK_NASIONAL(
		"produk_nasional",
		"peppbjn2",
		"Produk Nasional",
		506,
		10
	),
	PENYEDIA_NASIONAL(
		"penyedia_nasional",
		"peppbjn2",
		"Penyedia Nasional",
		507,
		10
	),
	KEMENTRIAN_SEKTORAL_LOKAL(
		"kementrian_sektoral_lokal",
		"peppbjn2",
		"Kementerian/Lembaga Sektoral Dan Lokal",
		508,
		10
	),
	PEMDA_SEKTORAL_LOKAL(
		"pemda_sektoral_lokal",
		"peppbjn2",
		"Pemda Sektoral Dan Lokal",
		509,
		10
	),
	KOMODITAS_SEKTORAL_LOKAL(
		"komoditas_sektoral_lokal",
		"peppbjn2",
		"Komoditas Sektoral Dan Lokal",
		510,
		10
	),
	PRODUK_SEKTORAL_LOKAL(
		"produk_sektoral_lokal",
		"peppbjn2",
		"Produk Sektoral Dan Lokal",
		511,
		10
	),
	PENYEDIA_SEKTORAL_LOKAL(
		"penyedia_sektoral_lokal",
		"peppbjn2",
		"Penyedia Sektoral Dan Lokal",
		512,
		10
	),
	WMP_LIST_ITEM_1(
		"wmp_list_item_1",
		"pten_wmp_list",
		"Waktu Mulai Pesanan",
		513,
		10,
		"month"
	),
	WMP_LIST_ITEM_2(
		"wmp_list_item_2",
		"pten_wmp_list",
		"Waktu Mulai Pesanan",
		514,
		10,
		"month"
	),
	WMP_LIST_ITEM_3(
		"wmp_list_item_3",
		"pten_wmp_list",
		"Waktu Mulai Pesanan",
		515,
		10,
		"month"
	),
	WMP_LIST_ITEM_4(
		"wmp_list_item_4",
		"pten_wmp_list",
		"Waktu Mulai Pesanan",
		516,
		10,
		"month"
	),
	WMP_LIST_ITEM_5(
		"wmp_list_item_5",
		"pten_wmp_list",
		"Waktu Mulai Pesanan",
		517,
		10,
		"month"
	),
	WMP_LIST_ITEM_6(
		"wmp_list_item_6",
		"pten_wmp_list",
		"Waktu Mulai Pesanan",
		518,
		10,
		"month"
	),
	WMP_LIST_ITEM_7(
		"wmp_list_item_7",
		"pten_wmp_list",
		"Waktu Mulai Pesanan",
		519,
		10,
		"month"
	),
	WMP_LIST_ITEM_8(
		"wmp_list_item_8",
		"pten_wmp_list",
		"Waktu Mulai Pesanan",
		520,
		10,
		"month"
	),
	WMP_LIST_ITEM_9(
		"wmp_list_item_9",
		"pten_wmp_list",
		"Waktu Mulai Pesanan",
		521,
		10,
		"month"
	),
	WMP_LIST_ITEM_10(
		"wmp_list_item_10",
		"pten_wmp_list",
		"Waktu Mulai Pesanan",
		522,
		10,
		"month"
	),
	WMP_LIST_ITEM_11(
		"wmp_list_item_11",
		"pten_wmp_list",
		"Waktu Mulai Pesanan",
		523,
		10,
		"month"
	),
	WMP_LIST_ITEM_12(
		"wmp_list_item_12",
		"pten_wmp_list",
		"Waktu Mulai Pesanan",
		524,
		10,
		"month"
	),
	WMP_LIST_ITEM_13(
		"wmp_list_item_13",
		"pten_wmp_list",
		"Waktu Mulai Pesanan",
		525,
		10,
		"month"
	),
	WMP_LIST_ITEM_14(
		"wmp_list_item_14",
		"pten_wmp_list",
		"Waktu Mulai Pesanan",
		526,
		10,
		"month"
	),
	PTEN_KOMODITAS_NAME_1(
		"pten_komoditas_name_1",
		"p10_komoditas_list",
		"Nama Komoditas",
		527,
		10,
		"text"
	),
	PAKET_HARGA_PESANAN(
		"paket_harga_pesanan",
		"peppbjn1",
		"Paket Harga Pesanan",
		501,
		10
	),
	PTEN_KOMODITAS_HARGA_1(
		"pten_komoditas_harga_1",
		"p10_komoditas_list",
		"Harga Pesanan Komoditas",
		529,
		10
	),
	PTEN_KOMODITAS_NAME_2(
		"pten_komoditas_name_2",
		"p10_komoditas_list",
		"Nama Komoditas",
		530,
		10,
		"text"
	),
	PTEN_KOMODITAS_PAKET_2(
		"pten_komoditas_paket_2",
		"p10_komoditas_list",
		"Paket Komoditas",
		531,
		10
	),
	PTEN_KOMODITAS_HARGA_2(
		"pten_komoditas_harga_2",
		"p10_komoditas_list",
		"Harga Pesanan Komoditas",
		532,
		10
	),
	PTEN_KOMODITAS_NAME_3(
		"pten_komoditas_name_3",
		"p10_komoditas_list",
		"Nama Komoditas",
		533,
		10,
		"text"
	),
	PTEN_KOMODITAS_PAKET_3(
		"pten_komoditas_paket_3",
		"p10_komoditas_list",
		"Paket Komoditas",
		534,
		10
	),
	PTEN_KOMODITAS_HARGA_3(
		"pten_komoditas_harga_3",
		"p10_komoditas_list",
		"Harga Pesanan Komoditas",
		535,
		10
	),
	PTEN_KOMODITAS_NAME_4(
		"pten_komoditas_name_4",
		"p10_komoditas_list",
		"Nama Komoditas",
		536,
		10,
		"text"
	),
	PTEN_KOMODITAS_PAKET_4(
		"pten_komoditas_paket_4",
		"p10_komoditas_list",
		"Paket Komoditas",
		537,
		10
	),
	PTEN_KOMODITAS_HARGA_4(
		"pten_komoditas_harga_4",
		"p10_komoditas_list",
		"Harga Pesanan Komoditas",
		538,
		10
	),
	PTEN_KOMODITAS_NAME_5(
		"pten_komoditas_name_5",
		"p10_komoditas_list",
		"Nama Komoditas",
		539,
		10,
		"text"
	),
	PTEN_KOMODITAS_PAKET_5(
		"pten_komoditas_paket_5",
		"p10_komoditas_list",
		"Paket Komoditas",
		540,
		10
	),
	PTEN_KOMODITAS_HARGA_5(
		"pten_komoditas_harga_5",
		"p10_komoditas_list",
		"Harga Pesanan Komoditas",
		541,
		10
	),
	PTEN_KOMODITAS_NAME_6(
		"pten_komoditas_name_6",
		"p10_komoditas_list",
		"Nama Komoditas",
		542,
		10,
		"text"
	),
	PTEN_KOMODITAS_PAKET_6(
		"pten_komoditas_paket_6",
		"p10_komoditas_list",
		"Paket Komoditas",
		543,
		10
	),
	PTEN_KOMODITAS_HARGA_6(
		"pten_komoditas_harga_6",
		"p10_komoditas_list",
		"Harga Pesanan Komoditas",
		544,
		10
	),
	PTEN_KOMODITAS_NAME_7(
		"pten_komoditas_name_7",
		"p10_komoditas_list",
		"Nama Komoditas",
		545,
		10,
		"text"
	),
	PTEN_KOMODITAS_PAKET_7(
		"pten_komoditas_paket_7",
		"p10_komoditas_list",
		"Paket Komoditas",
		546,
		10
	),
	PTEN_KOMODITAS_HARGA_7(
		"pten_komoditas_harga_7",
		"p10_komoditas_list",
		"Harga Pesanan Komoditas",
		547,
		10
	),
	PTEN_KOMODITAS_NAME_8(
		"pten_komoditas_name_8",
		"p10_komoditas_list",
		"Nama Komoditas",
		548,
		10,
		"text"
	),
	PTEN_KOMODITAS_PAKET_8(
		"pten_komoditas_paket_8",
		"p10_komoditas_list",
		"Paket Komoditas",
		549,
		10
	),
	PTEN_KOMODITAS_HARGA_8(
		"pten_komoditas_harga_8",
		"p10_komoditas_list",
		"Harga Pesanan Komoditas",
		550,
		10
	),
	PTEN_KOMODITAS_NAME_9(
		"pten_komoditas_name_9",
		"p10_komoditas_list",
		"Nama Komoditas",
		551,
		10,
		"text"
	),
	HARGA_NILAI_PESANAN(
		"harga_nilai_pesanan",
		"peppbjn1",
		"Harga Nilai Pesanan",
		500,
		10
	),
	PTEN_KOMODITAS_HARGA_9(
		"pten_komoditas_harga_9",
		"p10_komoditas_list",
		"Harga Pesanan Komoditas",
		553,
		10
	),
	PTEN_KOMODITAS_NAME_10(
		"pten_komoditas_name_10",
		"p10_komoditas_list",
		"Nama Komoditas",
		554,
		10,
		"text"
	),
	PTEN_KOMODITAS_PAKET_10(
		"pten_komoditas_paket_10",
		"p10_komoditas_list",
		"Paket Komoditas",
		555,
		10
	),
	PTEN_KOMODITAS_HARGA_10(
		"pten_komoditas_harga_10",
		"p10_komoditas_list",
		"Harga Pesanan Komoditas",
		556,
		10
	),
	REALISASI_DESEMBER(
		"realisasi_desember",
		"strk2",
		"Realisasi Desember",
		604,
		11
	),
	ST_JANUARI(
		"st_januari",
		"strk1",
		"Sismontepra Target Januari",
		557,
		11
	),
	SRSP_JANUARI(
		"srsp_januari",
		"strk1",
		"Sismontepra Realisasi Satuan Persen Januari",
		558,
		11
	),
	ST_FEBRUARI(
		"st_februari",
		"strk1",
		"Sismontepra Target Februari",
		559,
		11
	),
	SRSP_FEBRUARI(
		"srsp_februari",
		"strk1",
		"Sismontepra Realisasi Satuan Persen Februari",
		560,
		11
	),
	ST_MARET(
		"st_maret",
		"strk1",
		"Sismontepra Target Maret",
		561,
		11
	),
	SRSP_MARET(
		"srsp_maret",
		"strk1",
		"Sismontepra Realisasi Satuan Persen Maret",
		562,
		11
	),
	ST_APRIL(
		"st_april",
		"strk1",
		"Sismontepra Target April",
		563,
		11
	),
	SRSP_APRIL(
		"srsp_april",
		"strk1",
		"Sismontepra Realisasi Satuan Persen April",
		564,
		11
	),
	ST_MEI(
		"st_mei",
		"strk1",
		"Sismontepra Target Mei",
		565,
		11
	),
	SRSP_MEI(
		"srsp_mei",
		"strk1",
		"Sismontepra Realisasi Satuan Persen Mei",
		566,
		11
	),
	ST_JUNI(
		"st_juni",
		"strk1",
		"Sismontepra Target Juni",
		567,
		11
	),
	SRSP_JUNI(
		"srsp_juni",
		"strk1",
		"Sismontepra Realisasi Satuan Persen Juni",
		568,
		11
	),
	ST_JULI(
		"st_juli",
		"strk1",
		"Sismontepra Target Juli",
		569,
		11
	),
	SRSP_JULI(
		"srsp_juli",
		"strk1",
		"Sismontepra Realisasi Satuan Persen Juli",
		570,
		11
	),
	ST_AGUSTUS(
		"st_agustus",
		"strk1",
		"Sismontepra Target Agustus",
		571,
		11
	),
	SRSP_AGUSTUS(
		"srsp_agustus",
		"strk1",
		"Sismontepra Realisasi Satuan Persen Agustus",
		572,
		11
	),
	ST_SEPTEMBER(
		"st_september",
		"strk1",
		"Sismontepra Target September",
		573,
		11
	),
	SRSP_SEPTEMBER(
		"srsp_september",
		"strk1",
		"Sismontepra Realisasi Satuan Persen September",
		574,
		11
	),
	ST_OKTOBER(
		"st_oktober",
		"strk1",
		"Sismontepra Target Oktober",
		575,
		11
	),
	SRSP_OKTOBER(
		"srsp_oktober",
		"strk1",
		"Sismontepra Realisasi Satuan Persen Oktober",
		576,
		11
	),
	ST_NOVEMBER(
		"st_november",
		"strk1",
		"Sismontepra Target November",
		577,
		11
	),
	SRSP_NOVEMBER(
		"srsp_november",
		"strk1",
		"Sismontepra Realisasi Satuan Persen November",
		578,
		11
	),
	ST_DESEMBER(
		"st_desember",
		"strk1",
		"Sismontepra Target Desember",
		579,
		11
	),
	SRSP_DESEMBER(
		"srsp_desember",
		"strk1",
		"Sismontepra Realisasi Satuan Persen Desember",
		580,
		11
	),
	TARGET_JANUARI(
		"target_januari",
		"strk2",
		"Target Januari",
		581,
		11
	),
	REALISASI_JANUARI(
		"realisasi_januari",
		"strk2",
		"Realisasi Januari",
		582,
		11
	),
	TARGET_FEBRUARI(
		"target_februari",
		"strk2",
		"Target Februari",
		583,
		11
	),
	REALISASI_FEBRUARI(
		"realisasi_februari",
		"strk2",
		"Realisasi Februari",
		584,
		11
	),
	TARGET_MARET(
		"target_maret",
		"strk2",
		"Target Maret",
		585,
		11
	),
	REALISASI_MARET(
		"realisasi_maret",
		"strk2",
		"Realisasi Maret",
		586,
		11
	),
	TARGET_APRIL(
		"target_april",
		"strk2",
		"Target April",
		587,
		11
	),
	REALISASI_APRIL(
		"realisasi_april",
		"strk2",
		"Realisasi April",
		588,
		11
	),
	TARGET_MEI(
		"target_mei",
		"strk2",
		"Target Mei",
		589,
		11
	),
	REALISASI_MEI(
		"realisasi_mei",
		"strk2",
		"Realisasi Mei",
		590,
		11
	),
	TARGET_JUNI(
		"target_juni",
		"strk2",
		"Target Juni",
		591,
		11
	),
	REALISASI_JUNI(
		"realisasi_juni",
		"strk2",
		"Realisasi Juni",
		592,
		11
	),
	TARGET_JULI(
		"target_juli",
		"strk2",
		"Target Juli",
		593,
		11
	),
	REALISASI_JULI(
		"realisasi_juli",
		"strk2",
		"Realisasi Juli",
		594,
		11
	),
	TARGET_AGUSTUS(
		"target_agustus",
		"strk2",
		"Target Agustus",
		595,
		11
	),
	REALISASI_AGUSTUS(
		"realisasi_agustus",
		"strk2",
		"Realisasi Agustus",
		596,
		11
	),
	TARGET_SEPTEMBER(
		"target_september",
		"strk2",
		"Target September",
		597,
		11
	),
	REALISASI_SEPTEMBER(
		"realisasi_september",
		"strk2",
		"Realisasi September",
		598,
		11
	),
	TARGET_OKTOBER(
		"target_oktober",
		"strk2",
		"Target Oktober",
		599,
		11
	),
	REALISASI_OKTOBER(
		"realisasi_oktober",
		"strk2",
		"Realisasi Oktober",
		600,
		11
	),
	TARGET_NOVEMBER(
		"target_november",
		"strk2",
		"Target November",
		601,
		11
	),
	REALISASI_NOVEMBER(
		"realisasi_november",
		"strk2",
		"Realisasi November",
		602,
		11
	),
	TARGET_DESEMBER(
		"target_desember",
		"strk2",
		"Target Desember",
		603,
		11
	),
	KL_ULP_UKPBJ_PERMANEN_STRUKTURAL(
		"kl_ulp_ukpbj_permanen_struktural",
		"kelembagaan1",
		"Ulp & Ukpbj Permanen Struktural Kementrian/lembaga",
		29,
		13
	),
	LPSE_17STANDAR(
		"lpse_17standar",
		"kelembagaan2",
		"Lpse Dengan 17 Standar",
		36,
		13
	),
	LPSE_PENGGUNA_SPSE4(
		"lpse_pengguna_spse4",
		"kelembagaan2",
		"Lpse Pengguna Spse 4",
		37,
		13
	),
	LPSE_TERSTANDAR(
		"lpse_terstandar",
		"kelembagaan2",
		"Lpse Terstandar",
		34,
		13
	),
	ULP_UKPBJ_PERMANEN_STRUKTURAL(
		"ulp_ukpbj_permanen_struktural",
		"kelembagaan1",
		"Ulp & Ukpbj Permanen Struktural",
		27,
		13
	),
	ULP_UKPBJ_ADHOC(
		"ulp_ukpbj_adhoc",
		"kelembagaan1",
		"Ulp & Ukpbj Adhoc/ex-officio",
		28,
		13
	),
	LPSE_TERINSTAL_SPSE4(
		"lpse_terinstal_spse4",
		"kelembagaan2",
		"Lpse Terinstal Spse 4.3",
		35,
		13
	),
	PD_ULP_UKPBJ_PERMANEN_STRUKTURAL(
		"pd_ulp_ukpbj_permanen_struktural",
		"kelembagaan1",
		"Ulp & Ukpbj Permanen Struktural Pemerintah Daerah",
		30,
		13
	),
	KL_ULP_UKPBJ_ADHOC(
		"kl_ulp_ukpbj_adhoc",
		"kelembagaan1",
		"Ulp & Ukpbj Adhoc/ex-officio Kementrian/lembaga",
		31,
		13
	),
	PD_ULP_UKPBJ_ADHOC(
		"pd_ulp_ukpbj_adhoc",
		"kelembagaan1",
		"Ulp & Ukpbj Adhoc/ex-officio Pemerintah Daerah",
		32,
		13
	),
	LAYANAN_PENGADAAN_ELEKTRONIK(
		"layanan_pengadaan_elektronik",
		"kelembagaan2",
		"Layanan Pengadaan Secara Elektronik",
		33,
		13
	),
	ULP_UKPBJ(
		"ulp_ukpbj",
		"kelembagaan1",
		"Ulp & Ukpbj",
		26,
		13
	),
	TD_FASILITATOR_PBJP(
		"td_fasilitator_pbjp",
		"pf2",
		"Tingkat Dasar Fasilitator PBJ",
		52,
		14
	),
	MPJF_INPASSING(
		"mpjf_inpassing",
		"pf1",
		"Mekanisme Pengangkatan Jabatan Fungsional Inpassing",
		39,
		14
	),
	PF_WANITA(
		"pf_wanita",
		"pf1",
		"Pejabat Fungsional Wanita",
		42,
		14
	),
	PF_PRIA(
		"pf_pria",
		"pf1",
		"Pejabat Fungsional Pria",
		43,
		14
	),
	PF_PERTAMA(
		"pf_pertama",
		"pf1",
		"Pejabat Fungsional Pertama",
		44,
		14
	),
	PF_FUNGSIONAL_MUDA(
		"pf_fungsional_muda",
		"pf1",
		"Pejabat Fungsional Muda",
		45,
		14
	),
	PF_FUNGSIONAL_MADYA(
		"pf_fungsional_madya",
		"pf1",
		"Pejabat Fungsional Madya",
		46,
		14
	),
	SJF_AKTIF(
		"sjf_aktif",
		"pf1",
		"Status Jabatan Aktif",
		47,
		14
	),
	SJF_PEMBEBASAN_SEMENTARA(
		"sjf_pembebasan_sementara",
		"pf1",
		"Status Jabatan Pembebasan Sementara",
		48,
		14
	),
	SJF_PENSIUN(
		"sjf_pensiun",
		"pf1",
		"Status Jabatan Pensiun",
		49,
		14
	),
	SJF_WAFAT(
		"sjf_wafat",
		"pf1",
		"Status Jabatan Wafat",
		50,
		14
	),
	TD_DASAR_PBJP(
		"td_dasar_pbjp",
		"pf2",
		"Tingkat Dasar Pemilik Sertifikat Keahlian Tingkat Dasar PBJP",
		51,
		14
	),
	MPJF_PENGANGKATAN_PERTAMA(
		"mpjf_pengangkatan_pertama",
		"pf1",
		"Mekanisme Pengangkatan Jabatan Fungsional Pengangkatan Pertama",
		41,
		14
	),
	KPF_MUDA(
		"kpf_muda",
		"pf2",
		"Kompetensi Pejabat Fungsional Pbjp Muda",
		53,
		14
	),
	KPF_MADYA(
		"kpf_madya",
		"pf2",
		"Kompetensi Pejabat Fungsional Pbjp Madya",
		54,
		14
	),
	KO_PPK_PBJ(
		"ko_ppk_pbj",
		"pf2",
		"Kompetensi Okupasi Ppk Pbj",
		55,
		14
	),
	KO_POKJA_PEMILIHAN(
		"ko_pokja_pemilihan",
		"pf2",
		"Kompetensi Okupasi Pokja Pemilihan",
		56,
		14
	),
	KO_PP(
		"ko_pp",
		"pf2",
		"Kompetensi Okupasi Ppk Pbj",
		57,
		14
	),
	K_ASSESOR_KOMPETENSI(
		"k_assesor_kompetensi",
		"pf2",
		"Kompetensi Assesor Kompetensi",
		58,
		14
	),
	MPJF_PERPINDAHAN(
		"mpjf_perpindahan",
		"pf1",
		"Mekanisme Pengangkatan Jabatan Fungsional Perpindahan",
		40,
		14
	),
	ARBITER(
		"arbiter",
		"pf3",
		"Arbiter",
		62,
		14
	),
	MEDIATOR(
		"mediator",
		"pf3",
		"Mediator",
		61,
		14
	),
	KETERANGAN_AHLI(
		"keterangan_ahli",
		"pf3",
		"Keterangan Ahli",
		60,
		14
	),
	KFK_PBJ(
		"kfk_pbj",
		"pf2",
		"Kompetensi Fasilitator Komptensi Pbj",
		59,
		14
	),
	PENJABAT_FUNGSIONAL(
		"penjabat_fungsional",
		"pf1",
		"Penjabat Fungsional",
		38,
		14
	),
	DIKLAT_JABFUNG_PBJ_PERTAMA(
		"diklat_jabfung_pbj_pertama",
		"lpd2",
		"Diklat Japfung Pbj Pertama",
		65,
		15
	),
	DIKLAT_PEMBENTUKAN(
		"diklat_pembentukan",
		"lpd2",
		"Diklat Pembentukan",
		64,
		15
	),
	TOTAL_PESERTA_DIKLAT(
		"total_peserta_diklat",
		"lpd1",
		"Total Peserta Diklat Pembentukan Dan Penjejangan",
		63,
		15
	),
	PENYELENGGARA_UJIAN_SERT_KOMP(
		"penyelenggara_ujian_sert_komp",
		"lpd3",
		"Penyelenggaraan Ujian Sertifikasi Kompetensi",
		70,
		15
	),
	PENYELENGGARA_UJIAN_SERT_DASAR(
		"penyelenggara_ujian_sert_dasar",
		"lpd3",
		"Penyelenggaraan Ujian Sertifikasi Dasar",
		69,
		15
	),
	LEMBAGA_PENDIDIKAN_PELATIHAN_TERAKREDITASI(
		"lembaga_pendidikan_pelatihan_terakreditasi",
		"lpd3",
		"Lembaga Pendidikan Dan Pelatihan Terakreditasi",
		68,
		15
	),
	DIKLAT_JABFUNG_PBJ_MUDA(
		"diklat_jabfung_pbj_muda",
		"lpd2",
		"Diklat Japfung Madya",
		67,
		15
	),
	DIKLAT_JABFUNG_MUDA(
		"diklat_jabfung_muda",
		"lpd2",
		"Diklat Japfung Muda",
		66,
		15
	),
	JASA_LAINNYA_ADUAN(
		"jasa_lainnya_aduan",
		"apph2",
		"Jasa Launnya",
		619,
		16
	),
	JLA_PRESENTASE_WILAYAH2(
		"jla_presentase_wilayah2",
		"apph1",
		"Persentase Jumlah Layanan Advokasi Wilayah 2",
		609,
		16
	),
	PEMOHON_OMBUDSMAN(
		"pemohon_ombudsman",
		"apph3",
		"Ombudsman",
		631,
		16
	),
	PEMOHON_KPPU(
		"pemohon_kppu",
		"apph3",
		"Kppu",
		630,
		16
	),
	PEMOHON_KPK(
		"pemohon_kpk",
		"apph3",
		"Kpk",
		629,
		16
	),
	PEMOHON_KEJAKSAAN(
		"pemohon_kejaksaan",
		"apph3",
		"Kejaksaan",
		628,
		16
	),
	PEMOHON_KEPOLISIAN(
		"pemohon_kepolisian",
		"apph3",
		"Kepolisian",
		627,
		16
	),
	PERSENTASE_PENYELIDIKAN(
		"persentase_penyelidikan",
		"apph3",
		"Persentasi Penyelidikan",
		626,
		16
	),
	PERSENTASE_PERSIDANGAN(
		"persentase_persidangan",
		"apph3",
		"Persentasi Persidangan",
		625,
		16
	),
	PERSENTASE_PENYIDIKAN(
		"persentase_penyidikan",
		"apph3",
		"Persentasi Penyidikan",
		624,
		16
	),
	TOTAL_PAGU_KASUS(
		"total_pagu_kasus",
		"apph3",
		"Total Pagu Paket Kasus Pengadaan",
		623,
		16
	),
	KASUS_PENGADAAN(
		"kasus_pengadaan",
		"apph3",
		"Kasus Pengadaan",
		622,
		16
	),
	PASCAKONTRAK_PENGADUAN(
		"pascakontrak_pengaduan",
		"apph2",
		"Tahap Pengaduan Pasca Kontrak",
		621,
		16
	),
	PRAKONTRAK_PENGADUAN(
		"prakontrak_pengaduan",
		"apph2",
		"Tahap Pengaduan Pra Kontrak",
		620,
		16
	),
	PEMOHON_LAINNYA(
		"pemohon_lainnya",
		"apph3",
		"Pemohon Lainnya",
		632,
		16
	),
	JASA_KONSULTASI_ADUAN(
		"jasa_konsultasi_aduan",
		"apph2",
		"Jasa Konsultasi",
		618,
		16
	),
	BARANG_ADUAN(
		"barang_aduan",
		"apph2",
		"Barang",
		617,
		16
	),
	PEKERJAAN_KONSTRUKSI(
		"pekerjaan_konstruksi",
		"apph2",
		"Pekerjaan Konstruksi",
		616,
		16
	),
	TOTAL_PAGU_PENGADUAN(
		"total_pagu_pengaduan",
		"apph2",
		"Total Pagu Yang Diadukan",
		615,
		16
	),
	PENGADUAN_PAKET(
		"pengaduan_paket",
		"apph2",
		"Paket Yang Diadukan",
		614,
		16
	),
	NPTLA_PRESENTASE_WILAYAH2(
		"nptla_presentase_wilayah2",
		"apph1",
		"Nilai Pengadaan Terdampak Layanan Advokasi Harga Wilayah 2",
		613,
		16
	),
	NPTLA_WILAYAH2(
		"nptla_wilayah2",
		"apph1",
		"Persentase Nilai Pengadaan Terdampak Layanan Advokasi Harga Wilayah 1",
		612,
		16
	),
	NPTLA_PRESENTASE_WILAYAH1(
		"nptla_presentase_wilayah1",
		"apph1",
		"Nilai Pengadaan Terdampak Layanan Advokasi Harga Wilayah 1",
		611,
		16
	),
	NPTLA_WILAYAH1(
		"nptla_wilayah1",
		"apph1",
		"Nilai Pengadaan Terdampak Layanan Advokasi",
		610,
		16
	),
	JLA_WILAYAH2(
		"jla_wilayah2",
		"apph1",
		"Jumlah Layanan Advokasi Wilayah 2",
		608,
		16
	),
	JLA_PRESENTASE_WILAYAH1(
		"jla_presentase_wilayah1",
		"apph1",
		"Persentase Jumlah Layanan Advokasi Wilayah 1",
		607,
		16
	),
	JLA_WILAYAH1(
		"jla_wilayah1",
		"apph1",
		"Jumlah Layanan Advokasi Wilayah 1",
		606,
		16
	),
	JUMLAH_LAYANAN_ADVOKASI(
		"jumlah_layanan_advokasi",
		"apph1",
		"Jumlah Layanan Advokasi",
		605,
		16
	),
	PEMBERIAN_KESEMPATAN(
		"pemberian_kesempatan",
		"ppk2",
		"Pemberian Kesempatan/denda Keterlambatan",
		636,
		17
	),
	PERMASALAHAN_KONTRAK(
		"permasalahan_kontrak",
		"ppk1",
		"Permasalahan Kontrak Yang Telah Dilayani",
		633,
		17
	),
	UANG_MUKA(
		"uang_muka",
		"ppk2",
		"Uang Muka/pembayaran Prestasi",
		638,
		17
	),
	PEMUTUSAN_KONTRAK(
		"pemutusan_kontrak",
		"ppk2",
		"Pemutusan Kontrak/wanpresentasi",
		635,
		17
	),
	NILAI_PENGADAAN(
		"nilai_pengadaan",
		"ppk1",
		"Nilai Pengadaan Terdampak Layanan",
		634,
		17
	),
	DAFTAR_HITAM(
		"daftar_hitam",
		"ppk2",
		"Daftar Hitam",
		639,
		17
	),
	KEADAAN_KAHAR(
		"keadaan_kahar",
		"ppk2",
		"Keadaan Kahar/peristiwa Kompensasi",
		640,
		17
	),
	PENYESUAIAN_HARGA(
		"penyesuaian_harga",
		"ppk2",
		"Penyesuaian Harga",
		641,
		17
	),
	PERMASALAHAN_LAINNYA(
		"permasalahan_lainnya",
		"ppk2",
		"Permasalahan Kontrak Lainnya",
		642,
		17
	),
	PUTUSAN_SIDANG_PBJP(
		"putusan_sidang_pbjp",
		"ppk3",
		"Putusan Sidang Kasus Pbjp Berkekuatan Hukum Tetap",
		643,
		17
	),
	KASUS_PBJP_BARANG(
		"kasus_pbjp_barang",
		"ppk3",
		"Kasus Pbjp Barang",
		644,
		17
	),
	KASUS_PBJP_KONSTRUKSI(
		"kasus_pbjp_konstruksi",
		"ppk3",
		"Kasus Pbjp Pekerjaan Konstruksi",
		645,
		17
	),
	KASUS_PBJP_KONSULTASI(
		"kasus_pbjp_konsultasi",
		"ppk3",
		"Kasus Pbjp Jasa Konsultasi",
		646,
		17
	),
	KASUS_JASA_LAINNYA(
		"kasus_jasa_lainnya",
		"ppk3",
		"Kasus Pbjp Jasa Lainnya",
		647,
		17
	),
	PERKARA_PIDANA(
		"perkara_pidana",
		"ppk3",
		"Jenis Perkara Pidana",
		648,
		17
	),
	PERKARA_PERSAINGAN_USAHA(
		"perkara_persaingan_usaha",
		"ppk3",
		"Jenis Perkara Persaingan Usaha",
		649,
		17
	),
	PERKARA_PERDATA(
		"perkara_perdata",
		"ppk3",
		"Jenis Perkara Perdata",
		650,
		17
	),
	PERKARA_TUN(
		"perkara_tun",
		"ppk3",
		"Jenis Perkara Tata Usaha Negara",
		651,
		17
	),
	PERUBAHAN_KONTRAK(
		"perubahan_kontrak",
		"ppk2",
		"Perubahan Kontrak",
		637,
		17
	),
	PTDS_ITEM_1(
		"ptds_item_1",
		"ptds_list",
		"Penyedia Terverifikasi Dalam SPSE",
		71,
		19,
		"year"
	),
	PTDS_ITEM_3(
		"ptds_item_3",
		"ptds_list",
		"Penyedia Terverifikasi Dalam SPSE",
		73,
		19,
		"year"
	),
	PTDS_ITEM_4(
		"ptds_item_4",
		"ptds_list",
		"Penyedia Terverifikasi Dalam SPSE",
		74,
		19,
		"year"
	),
	PTDS_ITEM_5(
		"ptds_item_5",
		"ptds_list",
		"Penyedia Terverifikasi Dalam SPSE",
		75,
		19,
		"year"
	),
	PTDS_ITEM_2(
		"ptds_item_2",
		"ptds_list",
		"Penyedia Terverifikasi Dalam SPSE",
		72,
		19,
		"year"
	),
	PENYEDIA_DLM_SIKAP(
		"penyedia_dlm_sikap",
		"pt2",
		"Penyedia Dalam Sikap",
		77,
		19
	),
	PENYEDIA_LOLOS_PRAKUALIFIKASI(
		"penyedia_lolos_prakualifikasi",
		"pt2",
		"Penyedia Lolos Prakualifikasi",
		78,
		19
	),
	PENYEDIA_AKTIF_IKUT_LELANG(
		"penyedia_aktif_ikut_lelang",
		"pt2",
		"Penyedia Aktif Ikut Tender",
		79,
		19
	),
	PENYEDIA_MENANG(
		"penyedia_menang",
		"pt2",
		"Penyedia Yang Menang",
		80,
		19
	),
	PENYEDIA_TERAGREGASI_LPSE(
		"penyedia_teragregasi_lpse",
		"pt2",
		"Penyedia Yang Teragregasi Di Lpse",
		76,
		19
	),
	PTWENTY_ITEM_NAME_1(
		"ptwenty_item_name_1",
		"p20_pr_list",
		"Nama Provinsi",
		81,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_1(
		"ptwenty_item_value_1",
		"p20_pr_list",
		"Nilai Total Penyedia",
		82,
		20
	),
	PTWENTY_ITEM_NAME_2(
		"ptwenty_item_name_2",
		"p20_pr_list",
		"Nama Provinsi",
		83,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_2(
		"ptwenty_item_value_2",
		"p20_pr_list",
		"Nilai Total Penyedia",
		84,
		20
	),
	PTWENTY_ITEM_NAME_3(
		"ptwenty_item_name_3",
		"p20_pr_list",
		"Nama Provinsi",
		85,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_28(
		"ptwenty_item_value_28",
		"p20_pr_list",
		"Nilai Total Penyedia",
		136,
		20
	),
	PTWENTY_ITEM_NAME_4(
		"ptwenty_item_name_4",
		"p20_pr_list",
		"Nama Provinsi",
		87,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_4(
		"ptwenty_item_value_4",
		"p20_pr_list",
		"Nilai Total Penyedia",
		88,
		20
	),
	PTWENTY_ITEM_NAME_5(
		"ptwenty_item_name_5",
		"p20_pr_list",
		"Nama Provinsi",
		89,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_5(
		"ptwenty_item_value_5",
		"p20_pr_list",
		"Nilai Total Penyedia",
		90,
		20
	),
	PTWENTY_ITEM_NAME_6(
		"ptwenty_item_name_6",
		"p20_pr_list",
		"Nama Provinsi",
		91,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_6(
		"ptwenty_item_value_6",
		"p20_pr_list",
		"Nilai Total Penyedia",
		92,
		20
	),
	PTWENTY_ITEM_NAME_7(
		"ptwenty_item_name_7",
		"p20_pr_list",
		"Nama Provinsi",
		93,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_7(
		"ptwenty_item_value_7",
		"p20_pr_list",
		"Nilai Total Penyedia",
		94,
		20
	),
	PTWENTY_ITEM_NAME_8(
		"ptwenty_item_name_8",
		"p20_pr_list",
		"Nama Provinsi",
		95,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_8(
		"ptwenty_item_value_8",
		"p20_pr_list",
		"Nilai Total Penyedia",
		96,
		20
	),
	PTWENTY_ITEM_NAME_9(
		"ptwenty_item_name_9",
		"p20_pr_list",
		"Nama Provinsi",
		97,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_9(
		"ptwenty_item_value_9",
		"p20_pr_list",
		"Nilai Total Penyedia",
		98,
		20
	),
	PTWENTY_ITEM_NAME_10(
		"ptwenty_item_name_10",
		"p20_pr_list",
		"Nama Provinsi",
		99,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_10(
		"ptwenty_item_value_10",
		"p20_pr_list",
		"Nilai Total Penyedia",
		100,
		20
	),
	PTWENTY_ITEM_NAME_11(
		"ptwenty_item_name_11",
		"p20_pr_list",
		"Nama Provinsi",
		101,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_11(
		"ptwenty_item_value_11",
		"p20_pr_list",
		"Nilai Total Penyedia",
		102,
		20
	),
	PTWENTY_ITEM_NAME_12(
		"ptwenty_item_name_12",
		"p20_pr_list",
		"Nama Provinsi",
		103,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_12(
		"ptwenty_item_value_12",
		"p20_pr_list",
		"Nilai Total Penyedia",
		104,
		20
	),
	PTWENTY_ITEM_NAME_13(
		"ptwenty_item_name_13",
		"p20_pr_list",
		"Nama Provinsi",
		105,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_13(
		"ptwenty_item_value_13",
		"p20_pr_list",
		"Nilai Total Penyedia",
		106,
		20
	),
	PTWENTY_ITEM_NAME_14(
		"ptwenty_item_name_14",
		"p20_pr_list",
		"Nama Provinsi",
		107,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_14(
		"ptwenty_item_value_14",
		"p20_pr_list",
		"Nilai Total Penyedia",
		108,
		20
	),
	PTWENTY_ITEM_NAME_15(
		"ptwenty_item_name_15",
		"p20_pr_list",
		"Nama Provinsi",
		109,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_15(
		"ptwenty_item_value_15",
		"p20_pr_list",
		"Nilai Total Penyedia",
		110,
		20
	),
	PTWENTY_ITEM_NAME_16(
		"ptwenty_item_name_16",
		"p20_pr_list",
		"Nama Provinsi",
		111,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_16(
		"ptwenty_item_value_16",
		"p20_pr_list",
		"Nilai Total Penyedia",
		112,
		20
	),
	PTWENTY_ITEM_NAME_17(
		"ptwenty_item_name_17",
		"p20_pr_list",
		"Nama Provinsi",
		113,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_17(
		"ptwenty_item_value_17",
		"p20_pr_list",
		"Nilai Total Penyedia",
		114,
		20
	),
	PTWENTY_ITEM_NAME_18(
		"ptwenty_item_name_18",
		"p20_pr_list",
		"Nama Provinsi",
		115,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_18(
		"ptwenty_item_value_18",
		"p20_pr_list",
		"Nilai Total Penyedia",
		116,
		20
	),
	PTWENTY_ITEM_NAME_19(
		"ptwenty_item_name_19",
		"p20_pr_list",
		"Nama Provinsi",
		117,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_19(
		"ptwenty_item_value_19",
		"p20_pr_list",
		"Nilai Total Penyedia",
		118,
		20
	),
	PTWENTY_ITEM_NAME_20(
		"ptwenty_item_name_20",
		"p20_pr_list",
		"Nama Provinsi",
		119,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_20(
		"ptwenty_item_value_20",
		"p20_pr_list",
		"Nilai Total Penyedia",
		120,
		20
	),
	PTWENTY_ITEM_NAME_21(
		"ptwenty_item_name_21",
		"p20_pr_list",
		"Nama Provinsi",
		121,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_21(
		"ptwenty_item_value_21",
		"p20_pr_list",
		"Nilai Total Penyedia",
		122,
		20
	),
	PTWENTY_ITEM_NAME_22(
		"ptwenty_item_name_22",
		"p20_pr_list",
		"Nama Provinsi",
		123,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_22(
		"ptwenty_item_value_22",
		"p20_pr_list",
		"Nilai Total Penyedia",
		124,
		20
	),
	PTWENTY_ITEM_NAME_23(
		"ptwenty_item_name_23",
		"p20_pr_list",
		"Nama Provinsi",
		125,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_23(
		"ptwenty_item_value_23",
		"p20_pr_list",
		"Nilai Total Penyedia",
		126,
		20
	),
	PTWENTY_ITEM_NAME_24(
		"ptwenty_item_name_24",
		"p20_pr_list",
		"Nama Provinsi",
		127,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_24(
		"ptwenty_item_value_24",
		"p20_pr_list",
		"Nilai Total Penyedia",
		128,
		20
	),
	PTWENTY_ITEM_NAME_25(
		"ptwenty_item_name_25",
		"p20_pr_list",
		"Nama Provinsi",
		129,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_25(
		"ptwenty_item_value_25",
		"p20_pr_list",
		"Nilai Total Penyedia",
		130,
		20
	),
	PTWENTY_ITEM_NAME_26(
		"ptwenty_item_name_26",
		"p20_pr_list",
		"Nama Provinsi",
		131,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_26(
		"ptwenty_item_value_26",
		"p20_pr_list",
		"Nilai Total Penyedia",
		132,
		20
	),
	PTWENTY_ITEM_NAME_27(
		"ptwenty_item_name_27",
		"p20_pr_list",
		"Nama Provinsi",
		133,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_27(
		"ptwenty_item_value_27",
		"p20_pr_list",
		"Nilai Total Penyedia",
		134,
		20
	),
	PTWENTY_ITEM_NAME_28(
		"ptwenty_item_name_28",
		"p20_pr_list",
		"Nama Provinsi",
		135,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_34(
		"ptwenty_item_value_34",
		"p20_pr_list",
		"Nilai Total Penyedia",
		148,
		20
	),
	PTWENTY_ITEM_NAME_34(
		"ptwenty_item_name_34",
		"p20_pr_list",
		"Nama Provinsi",
		147,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_33(
		"ptwenty_item_value_33",
		"p20_pr_list",
		"Nilai Total Penyedia",
		146,
		20
	),
	PTWENTY_ITEM_NAME_33(
		"ptwenty_item_name_33",
		"p20_pr_list",
		"Nama Provinsi",
		145,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_32(
		"ptwenty_item_value_32",
		"p20_pr_list",
		"Nilai Total Penyedia",
		144,
		20
	),
	PTWENTY_ITEM_NAME_32(
		"ptwenty_item_name_32",
		"p20_pr_list",
		"Nama Provinsi",
		143,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_31(
		"ptwenty_item_value_31",
		"p20_pr_list",
		"Nilai Total Penyedia",
		142,
		20
	),
	PTWENTY_ITEM_NAME_31(
		"ptwenty_item_name_31",
		"p20_pr_list",
		"Nama Provinsi",
		141,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_30(
		"ptwenty_item_value_30",
		"p20_pr_list",
		"Nilai Total Penyedia",
		140,
		20
	),
	PTWENTY_ITEM_NAME_30(
		"ptwenty_item_name_30",
		"p20_pr_list",
		"Nama Provinsi",
		139,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_29(
		"ptwenty_item_value_29",
		"p20_pr_list",
		"Nilai Total Penyedia",
		138,
		20
	),
	PTWENTY_ITEM_NAME_29(
		"ptwenty_item_name_29",
		"p20_pr_list",
		"Nama Provinsi",
		137,
		20,
		"text"
	),
	PTWENTY_ITEM_VALUE_3(
		"ptwenty_item_value_3",
		"p20_pr_list",
		"Nilai Total Penyedia",
		86,
		20
	),
	PBK_ITEM_KECIL_11(
		"pbk_item_kecil_11",
		"p21_pbk_list",
		"Nilai Kategori Kecil",
		180,
		21
	),
	PBK_ITEM_NAME_11(
		"pbk_item_name_11",
		"p21_pbk_list",
		"Nama Kategori Item ",
		179,
		21,
		"text"
	),
	PBK_ITEM_NON_10(
		"pbk_item_non_10",
		"p21_pbk_list",
		"Nilai Kategori Non Kecil",
		178,
		21
	),
	PBK_ITEM_KECIL_10(
		"pbk_item_kecil_10",
		"p21_pbk_list",
		"Nilai Kategori Kecil",
		177,
		21
	),
	PBK_ITEM_NAME_10(
		"pbk_item_name_10",
		"p21_pbk_list",
		"Nama Kategori Item ",
		176,
		21,
		"text"
	),
	PBK_ITEM_NON_9(
		"pbk_item_non_9",
		"p21_pbk_list",
		"Nilai Kategori Non Kecil",
		175,
		21
	),
	PBK_ITEM_KECIL_9(
		"pbk_item_kecil_9",
		"p21_pbk_list",
		"Nilai Kategori Kecil",
		174,
		21
	),
	PBK_ITEM_NAME_9(
		"pbk_item_name_9",
		"p21_pbk_list",
		"Nama Kategori Item ",
		173,
		21,
		"text"
	),
	PBK_ITEM_NON_8(
		"pbk_item_non_8",
		"p21_pbk_list",
		"Nilai Kategori Non Kecil",
		172,
		21
	),
	PBK_ITEM_KECIL_8(
		"pbk_item_kecil_8",
		"p21_pbk_list",
		"Nilai Kategori Kecil",
		171,
		21
	),
	PBK_ITEM_NAME_8(
		"pbk_item_name_8",
		"p21_pbk_list",
		"Nama Kategori Item ",
		170,
		21,
		"text"
	),
	PBK_ITEM_NON_7(
		"pbk_item_non_7",
		"p21_pbk_list",
		"Nilai Kategori Non Kecil",
		169,
		21
	),
	PBK_ITEM_KECIL_7(
		"pbk_item_kecil_7",
		"p21_pbk_list",
		"Nilai Kategori Kecil",
		168,
		21
	),
	PBK_ITEM_NAME_7(
		"pbk_item_name_7",
		"p21_pbk_list",
		"Nama Kategori Item ",
		167,
		21,
		"text"
	),
	PBK_ITEM_NON_6(
		"pbk_item_non_6",
		"p21_pbk_list",
		"Nilai Kategori Non Kecil",
		166,
		21
	),
	PBK_ITEM_KECIL_6(
		"pbk_item_kecil_6",
		"p21_pbk_list",
		"Nilai Kategori Kecil",
		165,
		21
	),
	PBK_ITEM_NAME_6(
		"pbk_item_name_6",
		"p21_pbk_list",
		"Nama Kategori Item ",
		164,
		21,
		"text"
	),
	PBK_ITEM_NON_5(
		"pbk_item_non_5",
		"p21_pbk_list",
		"Nilai Kategori Non Kecil",
		163,
		21
	),
	PBK_ITEM_KECIL_5(
		"pbk_item_kecil_5",
		"p21_pbk_list",
		"Nilai Kategori Kecil",
		162,
		21
	),
	PBK_ITEM_NAME_5(
		"pbk_item_name_5",
		"p21_pbk_list",
		"Nama Kategori Item ",
		161,
		21,
		"text"
	),
	PBK_ITEM_NON_4(
		"pbk_item_non_4",
		"p21_pbk_list",
		"Nilai Kategori Non Kecil",
		160,
		21
	),
	PBK_ITEM_KECIL_4(
		"pbk_item_kecil_4",
		"p21_pbk_list",
		"Nilai Kategori Kecil",
		159,
		21
	),
	PBK_ITEM_NAME_4(
		"pbk_item_name_4",
		"p21_pbk_list",
		"Nama Kategori Item ",
		158,
		21,
		"text"
	),
	PBK_ITEM_NON_3(
		"pbk_item_non_3",
		"p21_pbk_list",
		"Nilai Kategori Non Kecil",
		157,
		21
	),
	PBK_ITEM_KECIL_3(
		"pbk_item_kecil_3",
		"p21_pbk_list",
		"Nilai Kategori Kecil",
		156,
		21
	),
	PBK_ITEM_NAME_3(
		"pbk_item_name_3",
		"p21_pbk_list",
		"Nama Kategori Item ",
		155,
		21,
		"text"
	),
	PBK_ITEM_NON_2(
		"pbk_item_non_2",
		"p21_pbk_list",
		"Nilai Kategori Non Kecil",
		154,
		21
	),
	PBK_ITEM_KECIL_2(
		"pbk_item_kecil_2",
		"p21_pbk_list",
		"Nilai Kategori Kecil",
		153,
		21
	),
	PBK_ITEM_NON_1(
		"pbk_item_non_1",
		"p21_pbk_list",
		"Nilai Kategori Non Kecil",
		151,
		21
	),
	PBK_ITEM_KECIL_1(
		"pbk_item_kecil_1",
		"p21_pbk_list",
		"Nilai Kategori Kecil",
		150,
		21
	),
	PBK_ITEM_NAME_1(
		"pbk_item_name_1",
		"p21_pbk_list",
		"Nama Kategori Item ",
		149,
		21,
		"text"
	),
	PBK_ITEM_NON_15(
		"pbk_item_non_15",
		"p21_pbk_list",
		"Nilai Kategori Non Kecil",
		193,
		21
	),
	PBK_ITEM_KECIL_15(
		"pbk_item_kecil_15",
		"p21_pbk_list",
		"Nilai Kategori Kecil",
		192,
		21
	),
	PBK_ITEM_NAME_15(
		"pbk_item_name_15",
		"p21_pbk_list",
		"Nama Kategori Item ",
		191,
		21,
		"text"
	),
	PBK_ITEM_NON_14(
		"pbk_item_non_14",
		"p21_pbk_list",
		"Nilai Kategori Non Kecil",
		190,
		21
	),
	PBK_ITEM_NAME_2(
		"pbk_item_name_2",
		"p21_pbk_list",
		"Nama Kategori Item ",
		152,
		21,
		"text"
	),
	PBK_ITEM_NAME_14(
		"pbk_item_name_14",
		"p21_pbk_list",
		"Nama Kategori Item ",
		188,
		21,
		"text"
	),
	PBK_ITEM_NON_13(
		"pbk_item_non_13",
		"p21_pbk_list",
		"Nilai Kategori Non Kecil",
		187,
		21
	),
	PBK_ITEM_KECIL_13(
		"pbk_item_kecil_13",
		"p21_pbk_list",
		"Nilai Kategori Kecil",
		186,
		21
	),
	PBK_ITEM_NAME_13(
		"pbk_item_name_13",
		"p21_pbk_list",
		"Nama Kategori Item ",
		185,
		21,
		"text"
	),
	PBK_ITEM_NON_12(
		"pbk_item_non_12",
		"p21_pbk_list",
		"Nilai Kategori Non Kecil",
		184,
		21
	),
	PBK_ITEM_KECIL_12(
		"pbk_item_kecil_12",
		"p21_pbk_list",
		"Nilai Kategori Kecil",
		183,
		21
	),
	PBK_ITEM_NAME_12(
		"pbk_item_name_12",
		"p21_pbk_list",
		"Nama Kategori Item ",
		182,
		21,
		"text"
	),
	PBK_ITEM_NON_11(
		"pbk_item_non_11",
		"p21_pbk_list",
		"Nilai Kategori Non Kecil",
		181,
		21
	),
	PBK_ITEM_KECIL_14(
		"pbk_item_kecil_14",
		"p21_pbk_list",
		"Nilai Kategori Kecil",
		189,
		21
	),
	PTT_ITEM_VALUE_10(
		"ptt_item_value_10",
		"p22_list",
		"Nilai Sub Klasifikasi Bidang",
		671,
		22
	),
	PTT_ITEM_NAME_10(
		"ptt_item_name_10",
		"p22_list",
		"Nama Sub Klasifikasi Bidang",
		670,
		22,
		"text"
	),
	PTT_ITEM_VALUE_9(
		"ptt_item_value_9",
		"p22_list",
		"Nilai Sub Klasifikasi Bidang",
		669,
		22
	),
	PTT_ITEM_NAME_9(
		"ptt_item_name_9",
		"p22_list",
		"Nama Sub Klasifikasi Bidang",
		668,
		22,
		"text"
	),
	PTT_ITEM_VALUE_8(
		"ptt_item_value_8",
		"p22_list",
		"Nilai Sub Klasifikasi Bidang",
		667,
		22
	),
	PTT_ITEM_NAME_8(
		"ptt_item_name_8",
		"p22_list",
		"Nama Sub Klasifikasi Bidang",
		666,
		22,
		"text"
	),
	PTT_ITEM_VALUE_7(
		"ptt_item_value_7",
		"p22_list",
		"Nilai Sub Klasifikasi Bidang",
		665,
		22
	),
	PTT_ITEM_NAME_7(
		"ptt_item_name_7",
		"p22_list",
		"Nama Sub Klasifikasi Bidang",
		664,
		22,
		"text"
	),
	PTT_ITEM_VALUE_6(
		"ptt_item_value_6",
		"p22_list",
		"Nilai Sub Klasifikasi Bidang",
		663,
		22
	),
	PTT_ITEM_NAME_6(
		"ptt_item_name_6",
		"p22_list",
		"Nama Sub Klasifikasi Bidang",
		662,
		22,
		"text"
	),
	PTT_ITEM_VALUE_5(
		"ptt_item_value_5",
		"p22_list",
		"Nilai Sub Klasifikasi Bidang",
		661,
		22
	),
	PTT_ITEM_NAME_5(
		"ptt_item_name_5",
		"p22_list",
		"Nama Sub Klasifikasi Bidang",
		660,
		22,
		"text"
	),
	PTT_ITEM_VALUE_4(
		"ptt_item_value_4",
		"p22_list",
		"Nilai Sub Klasifikasi Bidang",
		659,
		22
	),
	PTT_ITEM_NAME_4(
		"ptt_item_name_4",
		"p22_list",
		"Nama Sub Klasifikasi Bidang",
		658,
		22,
		"text"
	),
	PTT_ITEM_VALUE_3(
		"ptt_item_value_3",
		"p22_list",
		"Nilai Sub Klasifikasi Bidang",
		657,
		22
	),
	PTT_ITEM_NAME_3(
		"ptt_item_name_3",
		"p22_list",
		"Nama Sub Klasifikasi Bidang",
		656,
		22,
		"text"
	),
	PTT_ITEM_VALUE_2(
		"ptt_item_value_2",
		"p22_list",
		"Nilai Sub Klasifikasi Bidang",
		655,
		22
	),
	PTT_ITEM_NAME_2(
		"ptt_item_name_2",
		"p22_list",
		"Nama Sub Klasifikasi Bidang",
		654,
		22,
		"text"
	),
	PTT_ITEM_VALUE_1(
		"ptt_item_value_1",
		"p22_list",
		"Nilai Sub Klasifikasi Bidang",
		653,
		22
	),
	PTT_ITEM_NAME_1(
		"ptt_item_name_1",
		"p22_list",
		"Nama Sub Klasifikasi Bidang",
		652,
		22,
		"text"
	),
	KONSULTANSI_LAINNYA_KECIL_2(
		"konsultansi_lainnya_kecil_2",
		"btm_list",
		"Konsultansi Lainnya Kecil",
		252,
		23,
		"year"
	),
	SIPIL_KECIL_1(
		"sipil_kecil_1",
		"btm_list",
		"Sipil Kecil",
		214,
		23,
		"year"
	),
	SIPIL_KECIL_2(
		"sipil_kecil_2",
		"btm_list",
		"Sipil Kecil",
		216,
		23,
		"year"
	),
	SIPIL_NON_2(
		"sipil_non_2",
		"btm_list",
		"Sipil Non Kecil",
		217,
		23,
		"year"
	),
	BANGUNAN_GEDUNG_KECIL_1(
		"bangunan_gedung_kecil_1",
		"btm_list",
		"Bangunan Gedung Kecil",
		218,
		23,
		"year"
	),
	BANGUNAN_GEDUNG_NON_1(
		"bangunan_gedung_non_1",
		"btm_list",
		"Bangunan Gedung Non Kecil ",
		219,
		23,
		"year"
	),
	BANGUNAN_GEDUNG_KECIL_2(
		"bangunan_gedung_kecil_2",
		"btm_list",
		"Bangunan Gedung Kecil",
		220,
		23,
		"year"
	),
	BANGUNAN_GEDUNG_NON_2(
		"bangunan_gedung_non_2",
		"btm_list",
		"Bangunan Gedung Non Kecil ",
		221,
		23,
		"year"
	),
	ENGINEER_KECIL_1(
		"engineer_kecil_1",
		"btm_list",
		"Rekayasa (engineer) Kecil ",
		222,
		23,
		"year"
	),
	ENGINEER_NON_1(
		"engineer_non_1",
		"btm_list",
		"Rekayasa (engineer) Non Kecil",
		223,
		23,
		"year"
	),
	ENGINEER_KECIL_2(
		"engineer_kecil_2",
		"btm_list",
		"Rekayasa (engineer) Kecil",
		224,
		23,
		"year"
	),
	ENGINEER_NON_2(
		"engineer_non_2",
		"btm_list",
		"Rekayasa (engineer) Non Kecil",
		225,
		23,
		"year"
	),
	ELEKTRIKAL_KECIL_1(
		"elektrikal_kecil_1",
		"btm_list",
		"Instalasi Elektrikal Kecil",
		226,
		23,
		"year"
	),
	ELEKTRIKAL_NON_1(
		"elektrikal_non_1",
		"btm_list",
		"Instalasi Elektrikal Non Kecil",
		227,
		23,
		"year"
	),
	ELEKTRIKAL_KECIL_2(
		"elektrikal_kecil_2",
		"btm_list",
		"Instalasi Elektrikal Kecil",
		228,
		23,
		"year"
	),
	ELEKTRIKAL_NON_2(
		"elektrikal_non_2",
		"btm_list",
		"Instalasi Elektrikal Non Kecil",
		229,
		23,
		"year"
	),
	KONSULTAN_SPESIALIS_KECIL_1(
		"konsultan_spesialis_kecil_1",
		"btm_list",
		"Konsultansi Spesialis Kecil",
		230,
		23,
		"year"
	),
	KONSULTAN_SPESIALIS_NON_1(
		"konsultan_spesialis_non_1",
		"btm_list",
		"Konsultansi Spesialis Non Kecil",
		231,
		23,
		"year"
	),
	KONSULTAN_SPESIALIS_KECIL_2(
		"konsultan_spesialis_kecil_2",
		"btm_list",
		"Konsultansi Spesialis Non Kecil",
		232,
		23,
		"year"
	),
	KONSULTAN_SPESIALIS_NON_2(
		"konsultan_spesialis_non_2",
		"btm_list",
		"Konsultansi Spesialis Non Kecil",
		233,
		23,
		"year"
	),
	PELAKSANA_SPESIALIS_KECIL_1(
		"pelaksana_spesialis_kecil_1",
		"btm_list",
		"Pelaksana Spesialis Kecil",
		234,
		23,
		"year"
	),
	PELAKSANA_SPESIALIS_NON_1(
		"pelaksana_spesialis_non_1",
		"btm_list",
		"Pelaksana Spesialis Non Kecil",
		235,
		23,
		"year"
	),
	PELAKSANA_SPESIALIS_KECIL_2(
		"pelaksana_spesialis_kecil_2",
		"btm_list",
		"Pelaksana Spesialis Kecil",
		236,
		23,
		"year"
	),
	PELAKSANA_SPESIALIS_NON_2(
		"pelaksana_spesialis_non_2",
		"btm_list",
		"Pelaksana Spesialis Non Kecil",
		237,
		23,
		"year"
	),
	PENATAAN_RUANG_KECIL_1(
		"penataan_ruang_kecil_1",
		"btm_list",
		"Penataan Ruang Kecil",
		238,
		23,
		"year"
	),
	PENATAAN_RUANG_NON_1(
		"penataan_ruang_non_1",
		"btm_list",
		"Penataan Ruang Non Kecil",
		239,
		23,
		"year"
	),
	PENATAAN_RUANG_KECIL_2(
		"penataan_ruang_kecil_2",
		"btm_list",
		"Penataan Ruang Kecil",
		240,
		23,
		"year"
	),
	PTWENTYTHREE_FIRST_NAME_1(
		"ptwentythree_first_name_1",
		"p23_first_list",
		"Nama Sub Klasifikasi Konstruksi Usaha Non Kecil",
		194,
		23,
		"text"
	),
	ARSITEKTUR_KECIL_1(
		"arsitektur_kecil_1",
		"btm_list",
		"Arsitektur Kecil",
		242,
		23,
		"year"
	),
	ARSITEKTUR_NON_1(
		"arsitektur_non_1",
		"btm_list",
		"Arsitektur Non Kecil",
		243,
		23,
		"year"
	),
	ARSITEKTUR_KECIL_2(
		"arsitektur_kecil_2",
		"btm_list",
		"Arsitektur Kecil",
		244,
		23,
		"year"
	),
	ARSITEKTUR_NON_2(
		"arsitektur_non_2",
		"btm_list",
		"Arsitektur Non Kecil",
		245,
		23,
		"year"
	),
	INSTALASI_MEKANIK_KECIL_1(
		"instalasi_mekanik_kecil_1",
		"btm_list",
		"Instalasi Mekamikal Kecil",
		246,
		23,
		"year"
	),
	INSTALASI_MEKANIK_NON_1(
		"instalasi_mekanik_non_1",
		"btm_list",
		"Instalasi Mekamikal Non Kecil",
		247,
		23,
		"year"
	),
	INSTALASI_MEKANIK_KECIL_2(
		"instalasi_mekanik_kecil_2",
		"btm_list",
		"Instalasi Mekamikal Kecil",
		248,
		23,
		"year"
	),
	INSTALASI_MEKANIK_NON_2(
		"instalasi_mekanik_non_2",
		"btm_list",
		"Instalasi Mekamikal Non Kecil",
		249,
		23,
		"year"
	),
	KONSULTANSI_LAINNYA_KECIL_1(
		"konsultansi_lainnya_kecil_1",
		"btm_list",
		"Konsultansi Lainnya Kecil",
		250,
		23,
		"year"
	),
	KONSULTANSI_LAINNYA_NON_1(
		"konsultansi_lainnya_non_1",
		"btm_list",
		"Konsultansi Lainnya Non Kecil",
		251,
		23,
		"year"
	),
	SIPIL_NON_1(
		"sipil_non_1",
		"btm_list",
		"Sipil Non Kecil",
		215,
		23,
		"year"
	),
	KONSULTANSI_LAINNYA_NON_2(
		"konsultansi_lainnya_non_2",
		"btm_list",
		"Konsultansi Lainnya Non Kecil",
		253,
		23,
		"year"
	),
	PELAKSANA_LAINNYA_KECIL_1(
		"pelaksana_lainnya_kecil_1",
		"btm_list",
		"Pelaksana Lainnya Kecil",
		254,
		23,
		"year"
	),
	PELAKSANA_LAINNYA_NON_1(
		"pelaksana_lainnya_non_1",
		"btm_list",
		"Pelaksana Lainnya Non Kecil",
		255,
		23,
		"year"
	),
	PELAKSANA_LAINNYA_KECIL_2(
		"pelaksana_lainnya_kecil_2",
		"btm_list",
		"Pelaksana Lainnya Kecil",
		256,
		23,
		"year"
	),
	PELAKSANA_LAINNYA_NON_2(
		"pelaksana_lainnya_non_2",
		"btm_list",
		"Pelaksana Lainnya Non Kecil",
		257,
		23,
		"year"
	),
	TERINTEGRASI_KECIL_1(
		"terintegrasi_kecil_1",
		"btm_list",
		"Terintegrasi Kecil",
		258,
		23,
		"year"
	),
	TERINTEGRASI_NON_1(
		"terintegrasi_non_1",
		"btm_list",
		"Terintegrasi Non Kecil",
		259,
		23,
		"year"
	),
	TERINTEGRASI_KECIL_2(
		"terintegrasi_kecil_2",
		"btm_list",
		"Terintegrasi Kecil",
		260,
		23,
		"year"
	),
	TERINTEGRASI_NON_2(
		"terintegrasi_non_2",
		"btm_list",
		"Terintegrasi Non Kecil",
		261,
		23,
		"year"
	),
	PELAKSANA_KETERAMPILAN_KECIL_1(
		"pelaksana_keterampilan_kecil_1",
		"btm_list",
		"Pelaksana Keterampilan Kecil",
		262,
		23,
		"year"
	),
	PELAKSANA_KETERAMPILAN_NON_1(
		"pelaksana_keterampilan_non_1",
		"btm_list",
		"Pelaksana Keterampilan Non Kecil",
		263,
		23,
		"year"
	),
	PELAKSANA_KETERAMPILAN_KECIL_2(
		"pelaksana_keterampilan_kecil_2",
		"btm_list",
		"Pelaksana Keterampilan Kecil",
		264,
		23,
		"year"
	),
	PELAKSANA_KETERAMPILAN_NON_2(
		"pelaksana_keterampilan_non_2",
		"btm_list",
		"Pelaksana Keterampilan Non Kecil",
		265,
		23,
		"year"
	),
	PTWENTYTHREE_FIRST_NILAI_1(
		"ptwentythree_first_nilai_1",
		"p23_first_list",
		"Nilai Sub Klasifikasi Konstruksi Usaha Non Kecil",
		195,
		23
	),
	PTWENTYTHREE_FIRST_NAME_2(
		"ptwentythree_first_name_2",
		"p23_first_list",
		"Nama Sub Klasifikasi Konstruksi Usaha Non Kecil",
		196,
		23,
		"text"
	),
	PTWENTYTHREE_FIRST_NILAI_2(
		"ptwentythree_first_nilai_2",
		"p23_first_list",
		"Nilai Sub Klasifikasi Konstruksi Usaha Non Kecil",
		197,
		23
	),
	PTWENTYTHREE_FIRST_NAME_3(
		"ptwentythree_first_name_3",
		"p23_first_list",
		"Nama Sub Klasifikasi Konstruksi Usaha Non Kecil",
		198,
		23,
		"text"
	),
	PTWENTYTHREE_FIRST_NILAI_10(
		"ptwentythree_first_nilai_10",
		"p23_first_list",
		"Nilai Sub Klasifikasi Konstruksi Usaha Non Kecil",
		213,
		23
	),
	PTWENTYTHREE_FIRST_NAME_10(
		"ptwentythree_first_name_10",
		"p23_first_list",
		"Nama Sub Klasifikasi Konstruksi Usaha Non Kecil",
		212,
		23,
		"text"
	),
	PTWENTYTHREE_FIRST_NILAI_9(
		"ptwentythree_first_nilai_9",
		"p23_first_list",
		"Nilai Sub Klasifikasi Konstruksi Usaha Non Kecil",
		211,
		23
	),
	PTWENTYTHREE_FIRST_NAME_9(
		"ptwentythree_first_name_9",
		"p23_first_list",
		"Nama Sub Klasifikasi Konstruksi Usaha Non Kecil",
		210,
		23,
		"text"
	),
	PTWENTYTHREE_FIRST_NILAI_8(
		"ptwentythree_first_nilai_8",
		"p23_first_list",
		"Nilai Sub Klasifikasi Konstruksi Usaha Non Kecil",
		209,
		23
	),
	PTWENTYTHREE_FIRST_NAME_8(
		"ptwentythree_first_name_8",
		"p23_first_list",
		"Nama Sub Klasifikasi Konstruksi Usaha Non Kecil",
		208,
		23,
		"text"
	),
	PTWENTYTHREE_FIRST_NILAI_7(
		"ptwentythree_first_nilai_7",
		"p23_first_list",
		"Nilai Sub Klasifikasi Konstruksi Usaha Non Kecil",
		207,
		23
	),
	PTWENTYTHREE_FIRST_NAME_7(
		"ptwentythree_first_name_7",
		"p23_first_list",
		"Nama Sub Klasifikasi Konstruksi Usaha Non Kecil",
		206,
		23,
		"text"
	),
	PTWENTYTHREE_FIRST_NILAI_6(
		"ptwentythree_first_nilai_6",
		"p23_first_list",
		"Nilai Sub Klasifikasi Konstruksi Usaha Non Kecil",
		205,
		23
	),
	PTWENTYTHREE_FIRST_NAME_6(
		"ptwentythree_first_name_6",
		"p23_first_list",
		"Nama Sub Klasifikasi Konstruksi Usaha Non Kecil",
		204,
		23,
		"text"
	),
	PTWENTYTHREE_FIRST_NILAI_5(
		"ptwentythree_first_nilai_5",
		"p23_first_list",
		"Nilai Sub Klasifikasi Konstruksi Usaha Non Kecil",
		203,
		23
	),
	PTWENTYTHREE_FIRST_NAME_5(
		"ptwentythree_first_name_5",
		"p23_first_list",
		"Nama Sub Klasifikasi Konstruksi Usaha Non Kecil",
		202,
		23,
		"text"
	),
	PTWENTYTHREE_FIRST_NILAI_4(
		"ptwentythree_first_nilai_4",
		"p23_first_list",
		"Nilai Sub Klasifikasi Konstruksi Usaha Non Kecil",
		201,
		23
	),
	PTWENTYTHREE_FIRST_NAME_4(
		"ptwentythree_first_name_4",
		"p23_first_list",
		"Nama Sub Klasifikasi Konstruksi Usaha Non Kecil",
		200,
		23,
		"text"
	),
	PTWENTYTHREE_FIRST_NILAI_3(
		"ptwentythree_first_nilai_3",
		"p23_first_list",
		"Nilai Sub Klasifikasi Konstruksi Usaha Non Kecil",
		199,
		23
	),
	PENATAAN_RUANG_NON_2(
		"penataan_ruang_non_2",
		"btm_list",
		"Penataan Ruang Non Kecil",
		241,
		23,
		"year"
	),
	JASA_KONSULTASI_BLACKLIST(
		"jasa_konsultasi_blacklist",
		"blacklist2",
		"Jasa Konsultasi",
		269,
		24
	),
	BARANG_BLACKLIST(
		"barang_blacklist",
		"blacklist2",
		"Barang",
		268,
		24
	),
	PEKERJAAN_KONSTRUKSI_BLACKLIST(
		"pekerjaan_konstruksi_blacklist",
		"blacklist2",
		"Pekerjaan Konstruksi",
		267,
		24
	),
	KENA_SANKSI_BLACKLIST_1(
		"kena_sanksi_blacklist_1",
		"blacklist1",
		"Terkena Sanksi Daftar Hitam",
		266,
		24,
		"year"
	),
	PENYEDIA_BLACKLIST_HPS_5M(
		"penyedia_blacklist_hps_5m",
		"blacklist_hps_list",
		"Penyedia Blacklist Hps 5 M - 50 M",
		273,
		25
	),
	PENYEDIA_BLACKLIST_HPS_2M(
		"penyedia_blacklist_hps_2m",
		"blacklist_hps_list",
		"Penyedia Blacklist Hps 2,5 Juta - 5 M",
		272,
		25
	),
	PENYEDIA_BLACKLIST_HPS_200JT(
		"penyedia_blacklist_hps_200jt",
		"blacklist_hps_list",
		"Penyedia Blacklist Hps 200 Juta - 2,5 M",
		271,
		25
	),
	PENYEDIA_BLACKLIST_HPS_KURANG_200JT(
		"penyedia_blacklist_hps_kurang_200jt",
		"blacklist_hps_list",
		"Penyedia Blacklist Hps < 200 Juta",
		270,
		25
	),
	PAGE25_BLN_9(
		"page25_bln_9",
		"page25_ttp_list",
		"September",
		290,
		25
	),
	PAGE25_BLN_10(
		"page25_bln_10",
		"page25_ttp_list",
		"Oktober",
		291,
		25
	),
	PAGE25_BLN_1(
		"page25_bln_1",
		"page25_ttp_list",
		"Januari",
		282,
		25
	),
	PAGE25_BLN_11(
		"page25_bln_11",
		"page25_ttp_list",
		"November",
		292,
		25
	),
	PAGE25_BLN_12(
		"page25_bln_12",
		"page25_ttp_list",
		"Desember",
		293,
		25
	),
	DESKRIPSI_PELANGGARAN_3(
		"deskripsi_pelanggaran_3",
		"pelanggaran",
		"Deskripsi Pelanggaran 3",
		281,
		25,
		"text"
	),
	PAGE25_BLN_2(
		"page25_bln_2",
		"page25_ttp_list",
		"Februari",
		283,
		25
	),
	PAGE25_BLN_3(
		"page25_bln_3",
		"page25_ttp_list",
		"Maret",
		284,
		25
	),
	PAGE25_BLN_4(
		"page25_bln_4",
		"page25_ttp_list",
		"April",
		285,
		25
	),
	PAGE25_BLN_5(
		"page25_bln_5",
		"page25_ttp_list",
		"Mei",
		286,
		25
	),
	PAGE25_BLN_6(
		"page25_bln_6",
		"page25_ttp_list",
		"Juni",
		287,
		25
	),
	PAGE25_BLN_7(
		"page25_bln_7",
		"page25_ttp_list",
		"Juli",
		288,
		25
	),
	PAGE25_BLN_8(
		"page25_bln_8",
		"page25_ttp_list",
		"Agustus",
		289,
		25
	),
	TOTAL_PELANGGARAN_3(
		"total_pelanggaran_3",
		"pelanggaran",
		"Total Pelanggaran 3",
		280,
		25
	),
	DESKRIPSI_PELANGGARAN_2(
		"deskripsi_pelanggaran_2",
		"pelanggaran",
		"Deskripsi Pelanggaran 2",
		279,
		25,
		"text"
	),
	TOTAL_PELANGGARAN_2(
		"total_pelanggaran_2",
		"pelanggaran",
		"Total Pelanggaran 2",
		278,
		25
	),
	DESKRIPSI_PELANGGARAN_1(
		"deskripsi_pelanggaran_1",
		"pelanggaran",
		"Deskripsi Pelanggaran 1",
		277,
		25,
		"text"
	),
	TOTAL_PELANGGARAN_1(
		"total_pelanggaran_1",
		"pelanggaran",
		"Total Pelanggaran 1",
		276,
		25
	),
	PENYEDIA_BLACKLIST_HPS_LEBIH_50M(
		"penyedia_blacklist_hps_lebih_50m",
		"blacklist_hps_list",
		"Penyedia Blacklist Hps > 100 M",
		275,
		25
	),
	PENYEDIA_BLACKLIST_HPS_50M(
		"penyedia_blacklist_hps_50m",
		"blacklist_hps_list",
		"Penyedia Blacklist Hps 50 M - 100 M",
		274,
		25
	),
	PAKET_T_3(
		"paket_t_3",
		"p26_paket_chart",
		"Paket",
		307,
		26,
		"year"
	),
	SELESAI_3(
		"selesai_3",
		"p26_buttom_chart",
		"Data Tender Selesai",
		313,
		26,
		"year"
	),
	ALASAN_2(
		"alasan_2",
		"desc_gagal",
		"Top 3 Alasan Tender/seleksi Gagal 2",
		320,
		26,
		"text"
	),
	ALASAN_1(
		"alasan_1",
		"desc_gagal",
		"Top 3 Alasan Tender/seleksi Gagal 1",
		319,
		26,
		"text"
	),
	GAGAL_5(
		"gagal_5",
		"p26_buttom_chart",
		"Data Tender Gagal",
		318,
		26,
		"year"
	),
	SELESAI_5(
		"selesai_5",
		"p26_buttom_chart",
		"Data Tender Selesai",
		317,
		26,
		"year"
	),
	GAGAL_4(
		"gagal_4",
		"p26_buttom_chart",
		"Data Tender Gagal",
		316,
		26,
		"year"
	),
	SELESAI_4(
		"selesai_4",
		"p26_buttom_chart",
		"Data Tender Selesai",
		315,
		26,
		"year"
	),
	GAGAL_3(
		"gagal_3",
		"p26_buttom_chart",
		"Data Tender Gagal",
		314,
		26,
		"year"
	),
	GAGAL_2(
		"gagal_2",
		"p26_buttom_chart",
		"Data Tender Gagal",
		312,
		26,
		"year"
	),
	SELESAI_2(
		"selesai_2",
		"p26_buttom_chart",
		"Data Tender Selesai",
		311,
		26,
		"year"
	),
	GAGAL_1(
		"gagal_1",
		"p26_buttom_chart",
		"Data Tender Gagal",
		310,
		26,
		"year"
	),
	SELESAI_1(
		"selesai_1",
		"p26_buttom_chart",
		"Data Tender Selesai",
		309,
		26,
		"year"
	),
	PAKET_T_4(
		"paket_t_4",
		"p26_paket_chart",
		"Paket",
		308,
		26,
		"year"
	),
	ALASAN_3(
		"alasan_3",
		"desc_gagal",
		"Top 3 Alasan Tender/seleksi Gagal 3",
		321,
		26,
		"text"
	),
	PAKET_T_2(
		"paket_t_2",
		"p26_paket_chart",
		"Paket",
		306,
		26,
		"year"
	),
	PAKET_T_1(
		"paket_t_1",
		"p26_paket_chart",
		"Paket",
		305,
		26,
		"year"
	),
	PAGU_T_4(
		"pagu_t_4",
		"p26_pagu_chart",
		"Pagu",
		304,
		26,
		"year"
	),
	PAGU_T_3(
		"pagu_t_3",
		"p26_pagu_chart",
		"Pagu",
		303,
		26,
		"year"
	),
	PAGU_T_2(
		"pagu_t_2",
		"p26_pagu_chart",
		"Pagu",
		302,
		26,
		"year"
	),
	PAGU_T_1(
		"pagu_t_1",
		"p26_pagu_chart",
		"Pagu",
		301,
		26,
		"year"
	),
	HARI_2(
		"hari_2",
		"left",
		"Rata Rata Hari Pemilihan Tender Cepat Hari 2",
		300,
		26
	),
	PAGU_2(
		"pagu_2",
		"left",
		"Rata Rata Hari Pemilihan Tender Cepat Pagu 2",
		299,
		26
	),
	PAKET_2(
		"paket_2",
		"left",
		"Rata Rata Hari Pemilihan Tender Cepat Paket 2",
		298,
		26
	),
	HARI(
		"hari",
		"left",
		"Rata Rata Hari Pemilihan Tender Cepat Hari 1",
		297,
		26
	),
	PAGU(
		"pagu",
		"left",
		"Rata Rata Hari Pemilihan Tender Cepat Pagu 1",
		296,
		26
	),
	PAKET(
		"paket",
		"left",
		"Rata Rata Hari Pemilihan Tender Cepat Paket 1",
		295,
		26
	),
	TAHUN(
		"tahun",
		"left",
		"Rata Rata Hari Pemilihan Tender Cepat Tahun Anggaran 1",
		294,
		26
	),
	P27_PAKET_NON_1(
		"p27_paket_non_1",
		"p27_rup_paket_list",
		"RUP Paket Non Kecil",
		333,
		27,
		"year"
	),
	P27_PAKET_KECIL_1(
		"p27_paket_kecil_1",
		"p27_rup_paket_list",
		"RUP Paket Kecil",
		332,
		27,
		"year"
	),
	P27_PAGU_NON_5(
		"p27_pagu_non_5",
		"p27_rup_pagu_list",
		"RUP Pagu Non Kecil",
		331,
		27,
		"year"
	),
	P27_PAGU_KECIL_5(
		"p27_pagu_kecil_5",
		"p27_rup_pagu_list",
		"RUP Pagu Kecil",
		330,
		27,
		"year"
	),
	P27_PAGU_NON_4(
		"p27_pagu_non_4",
		"p27_rup_pagu_list",
		"RUP Pagu Non Kecil",
		329,
		27,
		"year"
	),
	P27_PAGU_KECIL_4(
		"p27_pagu_kecil_4",
		"p27_rup_pagu_list",
		"RUP Pagu Kecil",
		328,
		27,
		"year"
	),
	P27_PAGU_NON_3(
		"p27_pagu_non_3",
		"p27_rup_pagu_list",
		"RUP Pagu Non Kecil",
		327,
		27,
		"year"
	),
	P27_PAGU_KECIL_3(
		"p27_pagu_kecil_3",
		"p27_rup_pagu_list",
		"RUP Pagu Kecil",
		326,
		27,
		"year"
	),
	P27_PAGU_NON_2(
		"p27_pagu_non_2",
		"p27_rup_pagu_list",
		"RUP Pagu Non Kecil",
		325,
		27,
		"year"
	),
	P27_PAGU_KECIL_2(
		"p27_pagu_kecil_2",
		"p27_rup_pagu_list",
		"RUP Pagu Kecil",
		324,
		27,
		"year"
	),
	P27_PAGU_NON_1(
		"p27_pagu_non_1",
		"p27_rup_pagu_list",
		"RUP Pagu Non Kecil ",
		323,
		27,
		"year"
	),
	P27_PAGU_KECIL_1(
		"p27_pagu_kecil_1",
		"p27_rup_pagu_list",
		"RUP Pagu Kecil ",
		322,
		27,
		"year"
	),
	P27_TENDER_PAKET_NON_5(
		"p27_tender_paket_non_5",
		"p27_tender_paket_list",
		"E-Tendering Paket Non Kecil",
		361,
		27,
		"year"
	),
	P27_TENDER_PAKET_NON_4(
		"p27_tender_paket_non_4",
		"p27_tender_paket_list",
		"E-Tendering Paket Non Kecil",
		359,
		27,
		"year"
	),
	P27_TENDER_PAKET_KECIL_4(
		"p27_tender_paket_kecil_4",
		"p27_tender_paket_list",
		"E-Tendering Paket Kecil",
		358,
		27,
		"year"
	),
	P27_TENDER_PAKET_NON_3(
		"p27_tender_paket_non_3",
		"p27_tender_paket_list",
		"E-Tendering Paket Non Kecil",
		357,
		27,
		"year"
	),
	P27_TENDER_PAKET_KECIL_3(
		"p27_tender_paket_kecil_3",
		"p27_tender_paket_list",
		"E-Tendering Paket Kecil",
		356,
		27,
		"year"
	),
	P27_TENDER_PAKET_NON_2(
		"p27_tender_paket_non_2",
		"p27_tender_paket_list",
		"E-Tendering Paket Non Kecil",
		355,
		27,
		"year"
	),
	P27_TENDER_PAKET_KECIL_2(
		"p27_tender_paket_kecil_2",
		"p27_tender_paket_list",
		"E-Tendering Paket Kecil",
		354,
		27,
		"year"
	),
	P27_TENDER_PAKET_NON_1(
		"p27_tender_paket_non_1",
		"p27_tender_paket_list",
		"E-Tendering Paket Non Kecil",
		353,
		27,
		"year"
	),
	P27_TENDER_PAKET_KECIL_5(
		"p27_tender_paket_kecil_5",
		"p27_tender_paket_list",
		"E-Tendering Paket Kecil",
		360,
		27,
		"year"
	),
	P27_TENDER_PAKET_KECIL_1(
		"p27_tender_paket_kecil_1",
		"p27_tender_paket_list",
		"E-Tendering Paket Kecil",
		352,
		27,
		"year"
	),
	P27_TENDER_PAGU_NON_5(
		"p27_tender_pagu_non_5",
		"p27_tender_pagu_list",
		"E-Tendering Pagu Non Kecil",
		351,
		27,
		"year"
	),
	P27_TENDER_PAGU_KECIL_5(
		"p27_tender_pagu_kecil_5",
		"p27_tender_pagu_list",
		"E-Tendering Pagu Kecil",
		350,
		27,
		"year"
	),
	P27_TENDER_PAGU_NON_4(
		"p27_tender_pagu_non_4",
		"p27_tender_pagu_list",
		"E-Tendering Pagu Non Kecil",
		349,
		27,
		"year"
	),
	P27_TENDER_PAGU_KECIL_4(
		"p27_tender_pagu_kecil_4",
		"p27_tender_pagu_list",
		"E-Tendering Pagu Kecil",
		348,
		27,
		"year"
	),
	P27_TENDER_PAGU_NON_3(
		"p27_tender_pagu_non_3",
		"p27_tender_pagu_list",
		"E-Tendering Pagu Non Kecil",
		347,
		27,
		"year"
	),
	P27_TENDER_PAGU_KECIL_3(
		"p27_tender_pagu_kecil_3",
		"p27_tender_pagu_list",
		"E-Tendering Pagu Kecil",
		346,
		27,
		"year"
	),
	P27_TENDER_PAGU_NON_2(
		"p27_tender_pagu_non_2",
		"p27_tender_pagu_list",
		"E-Tendering Pagu Non Kecil",
		345,
		27,
		"year"
	),
	P27_TENDER_PAGU_KECIL_2(
		"p27_tender_pagu_kecil_2",
		"p27_tender_pagu_list",
		"E-Tendering Pagu Kecil",
		344,
		27,
		"year"
	),
	P27_TENDER_PAGU_NON_1(
		"p27_tender_pagu_non_1",
		"p27_tender_pagu_list",
		"E-Tendering Pagu Non Kecil",
		343,
		27,
		"year"
	),
	P27_TENDER_PAGU_KECIL_1(
		"p27_tender_pagu_kecil_1",
		"p27_tender_pagu_list",
		"E-Tendering Pagu Kecil",
		342,
		27,
		"year"
	),
	P27_PAKET_NON_5(
		"p27_paket_non_5",
		"p27_rup_paket_list",
		"RUP Paket Non Kecil",
		341,
		27,
		"year"
	),
	P27_PAKET_KECIL_5(
		"p27_paket_kecil_5",
		"p27_rup_paket_list",
		"RUP Paket Kecil",
		340,
		27,
		"year"
	),
	P27_PAKET_NON_4(
		"p27_paket_non_4",
		"p27_rup_paket_list",
		"RUP Paket Non Kecil",
		339,
		27,
		"year"
	),
	P27_PAKET_KECIL_4(
		"p27_paket_kecil_4",
		"p27_rup_paket_list",
		"RUP Paket Kecil",
		338,
		27,
		"year"
	),
	P27_PAKET_NON_3(
		"p27_paket_non_3",
		"p27_rup_paket_list",
		"RUP Paket Non Kecil",
		337,
		27,
		"year"
	),
	P27_PAKET_KECIL_3(
		"p27_paket_kecil_3",
		"p27_rup_paket_list",
		"RUP Paket Kecil",
		336,
		27,
		"year"
	),
	P27_PAKET_NON_2(
		"p27_paket_non_2",
		"p27_rup_paket_list",
		"RUP Paket Non Kecil",
		335,
		27,
		"year"
	),
	P27_PAKET_KECIL_2(
		"p27_paket_kecil_2",
		"p27_rup_paket_list",
		"RUP Paket Kecil",
		334,
		27,
		"year"
	),
	PTF_BARANG_PAGU(
		"ptf_barang_pagu",
		"p35_list",
		"Nilai Pagu Jenis Barang",
		672,
		35
	),
	PTF_PEKERJAAN_KONSTRUKSI_PAGU(
		"ptf_pekerjaan_konstruksi_pagu",
		"p35_list",
		"Nilai Pagu Jenis Pekerjaan Konstruksi ",
		674,
		35
	),
	PTF_PEKERJAAN_KONSTRUKSI_PAKET(
		"ptf_pekerjaan_konstruksi_paket",
		"p35_list",
		"Nilai Paket Jenis Pekerjaan Konstruksi ",
		675,
		35
	),
	PTF_JASA_KONSULTANSI_PAGU(
		"ptf_jasa_konsultansi_pagu",
		"p35_list",
		"Nilai Pagu Jenis Jasa Konsultansi",
		676,
		35
	),
	PTF_JASA_KONSULTANSI_PAKET(
		"ptf_jasa_konsultansi_paket",
		"p35_list",
		"Nilai Paket Jenis Jasa Konsultansi",
		677,
		35
	),
	PTF_JASA_LAINNYA_PAGU(
		"ptf_jasa_lainnya_pagu",
		"p35_list",
		"Nilai Pagu Jenis Jasa Lainnya",
		678,
		35
	),
	PTF_JASA_LAINNYA_PAKET(
		"ptf_jasa_lainnya_paket",
		"p35_list",
		"Nilai Paket Jenis Jasa Lainnya",
		679,
		35
	),
	PTF_BARANG_PAKET(
		"ptf_barang_paket",
		"p35_list",
		"Nilai Paket Jenis Barang",
		673,
		35
	),
	PTS_PENGADAAN_LANGSUNG_PAGU(
		"pts_pengadaan_langsung_pagu",
		"p36_list",
		"Nilai Pagu Pengadaan Langsung",
		680,
		36
	),
	PTS_PENGADAAN_LANGSUNG_PAKET(
		"pts_pengadaan_langsung_paket",
		"p36_list",
		"Nilai Paket Pengadaan Langsung",
		681,
		36
	),
	PTS_EPURCHASING_PAGU(
		"pts_epurchasing_pagu",
		"p36_list",
		"Nilai Pagu E-Purchasing",
		682,
		36
	),
	PTS_EPURCHASING_PAKET(
		"pts_epurchasing_paket",
		"p36_list",
		"Nilai Paket E-Purchasing",
		683,
		36
	),
	PTS_PENUNJUKAN_LANGSUNG_PAGU(
		"pts_penunjukan_langsung_pagu",
		"p36_list",
		"Nilai Pagu Penunjukan Langsung",
		684,
		36
	),
	PTS_PENUNJUKAN_LANGSUNG_PAKET(
		"pts_penunjukan_langsung_paket",
		"p36_list",
		"Nilai Paket Penunjunkan Langsung",
		685,
		36
	),
	PTS_LAINNYA_PAKET(
		"pts_lainnya_paket",
		"p36_list",
		"Nilai Paket Lainnya",
		699,
		36
	),
	PTS_TENDER_LELANG_PAKET(
		"pts_tender_lelang_paket",
		"p36_list",
		"Nilai Paket Tender",
		687,
		36
	),
	PTS_PEMILIHAN_LANGSUNG_PAGU(
		"pts_pemilihan_langsung_pagu",
		"p36_list",
		"Nilai Pagu Pemilihan Langsung",
		688,
		36
	),
	PTS_PEMILIHAN_LANGSUNG_PAKET(
		"pts_pemilihan_langsung_paket",
		"p36_list",
		"Nilai Paket Pemilihan Langsung",
		689,
		36
	),
	PTS_SELEKSI_PAGU(
		"pts_seleksi_pagu",
		"p36_list",
		"Nilai Pagu Seleksi",
		690,
		36
	),
	PTS_SELEKSI_PAKET(
		"pts_seleksi_paket",
		"p36_list",
		"Nilai Paket Seleksi",
		691,
		36
	),
	PTS_TENDER_LELANG_CEPAT_PAGU(
		"pts_tender_lelang_cepat_pagu",
		"p36_list",
		"Nilai Pagu Tender Cepat",
		692,
		36
	),
	PTS_TENDER_LELANG_CEPAT_PAKET(
		"pts_tender_lelang_cepat_paket",
		"p36_list",
		"Nilai Paket Tender Cepat",
		693,
		36
	),
	PTS_KONTES_PAGU(
		"pts_kontes_pagu",
		"p36_list",
		"Nilai Pagu Kontes",
		694,
		36
	),
	PTS_KONTES_PAKET(
		"pts_kontes_paket",
		"p36_list",
		"Nilai Paket Kontes",
		695,
		36
	),
	PTS_SAYEMBARA_PAGU(
		"pts_sayembara_pagu",
		"p36_list",
		"Nilai Pagu Sayembara",
		696,
		36
	),
	PTS_SAYEMBARA_PAKET(
		"pts_sayembara_paket",
		"p36_list",
		"Nilai Paket Sayembara",
		697,
		36
	),
	PTS_LAINNYA_PAGU(
		"pts_lainnya_pagu",
		"p36_list",
		"Nilai Pagu Lainnya",
		698,
		36
	),
	PTS_TENDER_LELANG_PAGU(
		"pts_tender_lelang_pagu",
		"p36_list",
		"Nilai Pagu Tender",
		686,
		36
	);
//END OF AUTO GENERATED

    private final String name;
    private final String group;
    private final String label;
    private final boolean multiple;
    private final int order;
    private final int page;
    private final String extra;

    ProfileSection(String name, String group, String label, int order, int page) {
        this.name = name;
        this.group = group;
        this.label = label;
        this.multiple = false;
        this.order = order;
        this.page = page;
        this.extra = null;
    }

	ProfileSection(String name, String group, String label, int order, int page, String extra) {
		this.name = name;
		this.group = group;
		this.label = label;
		this.multiple = false;
		this.order = order;
		this.page = page;
		this.extra = extra;
	}

    public String getName() {
        return name;
    }

    public String getGroup() {
        return group;
    }

    public String getLabel() {
        return label;
    }

    public boolean extraExist() {
    	return !StringUtils.isEmpty(extra);
	}

	public String getExtra() {
		return extra;
	}

	public boolean isExtraYear() {
    	return extraExist() && getExtra().equalsIgnoreCase("year");
	}

	public boolean isMonth() {
    	return extraExist() && getExtra().equalsIgnoreCase("month");
	}

	public boolean isText() {
    	return extraExist() && getExtra().equalsIgnoreCase("text");
	}

	public boolean isMultiple() { return multiple; }

    public int getOrder() {
        return order;
    }

    public int getPage() {
        return page;
    }

    public static List<Integer> getPages() {
        Set<Integer> pages = new LinkedHashSet<>();
        for (ProfileSection section : ProfileSection.values()) {
            pages.add(section.getPage());
        }
        List<Integer> results = new ArrayList<>(pages);
        results.sort((o1, o2) -> o1 - o2);
        return results;
    }

    public static List<String> getHeadersByPage(Integer page) {
        List<String> headers = new ArrayList<>();
        for (ProfileSection section : ProfileSection.values()) {
            if (section.getPage() == page) {
                headers.add(section.getName());
            }
        }
        return headers;
    }

}
