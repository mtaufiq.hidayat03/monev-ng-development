package procurementprofile;

import procurementprofile.models.ProcurementData;
import procurementprofile.models.ProcurementProfile;

import java.util.List;

/**
 * @author HanusaCloud on 9/26/2019 11:06 AM
 */
public class ProfileRepository {

    public static final String TAG = "ProfileRepository";

    public static ProfileRepository getInstance() {
        return new ProfileRepository();
    }

    public List<ProcurementProfile> getProfiles() {
        return ProcurementProfile.find("deleted_at IS NULL").fetch();
    }

    public List<ProcurementProfile> getProfiles(Long offset, Integer limit) {
        return ProcurementProfile.find("deleted_at IS NULL LIMIT ? OFFSET ?", limit, offset).fetch();
    }

    public List<ProcurementProfile> getPublishedProfiles() {
        return ProcurementProfile.find("status = ? AND deleted_at IS NULL",
                ProcurementProfile.PublicationStatus.PUBLISHED.name()).fetch();
    }

    public Long count() {
        return ProcurementProfile.count("deleted_at is NULL");
    }

    public ProcurementProfile getById(Long id) {
        return ProcurementProfile.findById(id);
    }

    public void store(ProcurementProfile model, List<ProcurementData> data) {
        model.saveModel();
        for (ProcurementData detail : data) {
            detail.procurement_id = model.id;
            detail.save();
        }
    }

}
