package procurementprofile;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import constants.generated.R;
import models.common.contracts.DataTablePagination;
import models.common.contracts.DataTableRequest;
import play.mvc.Router;
import procurementprofile.models.ProcurementProfile;
import utils.common.LogUtil;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author HanusaCloud on 12/2/2019 12:52 PM
 */
public class ProfileDataTable implements DataTablePagination.PageAble<ProcurementProfile> {

    private List<ProcurementProfile> items;
    private final DataTablePagination.Request request;
    private int total = 0;
    private int totalInOnePage = 0;
    private static final String[] columns = new String[]{"id", "title", "status", "mined_at", "created_at"};

    public ProfileDataTable(DataTableRequest dataTableRequest) {
        this.request = dataTableRequest;
        executeQuery();
    }

    @Override
    public JsonObject getResultsAsJson() {
        LogUtil.debug(TAG, "generate result as json");
        JsonObject result = getResponseTemplate();
        JsonArray jsonArray = new JsonArray();
        if (getTotal() == 0) {
            result.add("data", jsonArray);
            return result;
        }
        int i = getNumberingStartFrom();
        for (ProcurementProfile model : getResults()) {
            JsonArray array = new JsonArray();
            Map<String, Object> params = new HashMap<>();
            params.put("id", model.id);
            final String adjustment = Router.reverse(
                    R.route.dashboard_procurementprofile_profilecontroller_adjust,
                    params
            ).url;
            final String preview = model.getPreviewUrl();
            final String delete = Router.reverse(
                    R.route.dashboard_procurementprofile_profilecontroller_delete,
                    params
            ).url;
            final String export = Router.reverse(
                    R.route.core_dummycontroller_downloadpdfprofile,
                    params
            ).url;

            array.add(i);
            array.add(model.title);
            array.add(model.status.name());
            array.add(new SimpleDateFormat("dd-MM-yyyy").format(model.mined_at));
            array.add(model.getCreatedAsDateOnly());
            array.add(adjustment);
            array.add(preview);
            array.add(delete);
            array.add(export);
            jsonArray.add(array);
            i++;
        }
        result.add("data", jsonArray);
        LogUtil.debug(TAG, result);
        return result;
    }

    @Override
    public void setResult(List<ProcurementProfile> items) {
        this.items = items;
    }

    @Override
    public List<ProcurementProfile> getResults() {
        return items;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setTotalItemInOnePage(int totalItem) {
        this.totalInOnePage = totalItem;
    }

    @Override
    public int getTotalItemInOnePage() {
        return totalInOnePage;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public String[] getColumns() {
        return columns;
    }

    @Override
    public DataTablePagination.Request getRequest() {
        return request;
    }

    @Override
    public Class<ProcurementProfile> getReturnType() {
        return ProcurementProfile.class;
    }
}
