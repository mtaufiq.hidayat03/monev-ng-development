package repositories.dashboard;

import models.dashboard.RekapPenyedia;
import models.dashboard.SearchQuery;
import models.dashboard.pojo.TotalPenyediaResult;
import utils.common.QueryUtil;

public class TotalPenyediaRepository {

    public static final String TAG = "TotalPenyediaRepository";

    private static final TotalPenyediaRepository repository = new TotalPenyediaRepository();

    public static TotalPenyediaRepository getInstance() {
        return repository;
    }

    public TotalPenyediaResult levelOne(SearchQuery model) {
        TotalPenyediaResult result = new TotalPenyediaResult(model);
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT COUNT(*) as total, propinsi FROM public.rekap_penyedia");
        if (model.isKeywordExist()) {
            sb.append(" WHERE nama ILIKE ").append("'%").append(model.getKeyword()).append("%'")
                    .append("or alamat ILIKE ").append("'%").append(model.getKeyword()).append("%'")
                    .append("or propinsi ILIKE ").append("'%").append(model.getKeyword()).append("%'")
                    .append("or kabupaten ILIKE ").append("'%").append(model.getKeyword()).append("%'");
        }
        sb.append(" GROUP BY propinsi");
        sb.append(" ORDER BY total DESC ");
        result.executeQuery(sb.toString());
        return result;
    }

    public Long getTotalByPropinsi(String propinsi) {
        final Long total = RekapPenyedia.count("propinsi = ?", propinsi);
        return total;
    }

    public TotalPenyediaResult levelTwo(SearchQuery model) {
        TotalPenyediaResult result = new TotalPenyediaResult(model);
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM public.rekap_penyedia");
        if (model.propinsiExist()) {
            sb.append(" WHERE propinsi = '").append(QueryUtil.sanitize(model.getPropinsi())).append("'")
                    .append(" and (nama ILIKE ").append("'%").append(model.getKeyword()).append("%'")
                    .append("or alamat ILIKE ").append("'%").append(model.getKeyword()).append("%'")
                    .append("or propinsi ILIKE ").append("'%").append(model.getKeyword()).append("%'")
                    .append("or kabupaten ILIKE ").append("'%").append(model.getKeyword()).append("%')");
        }
        sb.append(" ORDER BY propinsi ASC ");
        result.executeQuery(sb.toString());
        return result;
    }
}
