package repositories.dashboard;

import models.common.contracts.Chartable;
import models.dashboard.RekapPemilihanMetode;
import models.dashboard.RekapPengadaanNasional;
import umkm.BaseRekapPerencanaanRepository;
import utils.common.ChartBuilder;

import java.time.Year;
import java.util.Arrays;
import java.util.List;

public class RekapPemilihanMetodeRepository implements BaseRekapPerencanaanRepository<RekapPemilihanMetode> {

    public static final String TAG = "RekapPemilihanMetodeRepository";

    private static final RekapPemilihanMetodeRepository repository = new RekapPemilihanMetodeRepository();

    public static RekapPemilihanMetodeRepository getInstance() {
        return repository;
    }

    public RekapPemilihanMetode getLatestOrEmpty() {
        RekapPemilihanMetode rekap = RekapPemilihanMetode.find("1=1 order by tahun desc").first();
        if (rekap != null) {
            RekapPengadaanNasional rekapNasional = RekapPengadaanNasional.find("tahun = ?", rekap.tahun).first();
            if (rekapNasional != null) {
                rekap.paket_total = rekapNasional.pelaksanaan_paket;
                rekap.pagu_total = rekapNasional.pelaksanaan_pagu;
            }

            return rekap;
        }

        return new RekapPemilihanMetode();
    }

    public List<RekapPemilihanMetode> list10Years() {
        int tenYearAgo = Year.now().getValue() - 10;
        return RekapPemilihanMetode.find("tahun > ? order by tahun", tenYearAgo).fetch();
    }

    public Chartable chartKonsolidasi() {
        // ambil label seriesnya
        List<String> seriesLabels = Arrays.asList("Konsolidasi", "Non Konsolidasi");
        // query
        RekapPemilihanMetode rekap = getLatestOrEmpty();

        List<Number> data = Arrays.asList(rekap.pagu_konsolidasi,
                rekap.pagu_total- rekap.pagu_konsolidasi);
        // build and return chart
        return ChartBuilder.buildDonut("Pagu Konsolidasi", seriesLabels, data);
    }

    public Chartable chartItemize() {
        // ambil label seriesnya
        List<String> seriesLabels = Arrays.asList("Itemize", "Non Itemize");
        // query
        RekapPemilihanMetode rekap = getLatestOrEmpty();

        List<Number> data = Arrays.asList(rekap.pagu_itemize,
                rekap.pagu_total - rekap.pagu_itemize);
        // build and return chart
        return ChartBuilder.buildDonut("Pagu Itemize", seriesLabels, data);
    }

    public Chartable chartExpress() {
        // ambil label seriesnya
        List<String> seriesLabels = Arrays.asList("Tender Cepat", "Non Tender Cepat");
        // query
        RekapPemilihanMetode rekap = getLatestOrEmpty();

        List<Number> data = Arrays.asList(rekap.pagu_lelang_cepat,
                rekap.pagu_total - rekap.pagu_lelang_cepat);
        // build and return chart
        return ChartBuilder.buildDonut("Pagu Tender/Seleksi Cepat", seriesLabels, data);
    }

    public Chartable chartReverse() {
        // ambil label seriesnya
        List<String> seriesLabels = Arrays.asList("Reverse Auction", "Non Reverse Auction");
        // query
        RekapPemilihanMetode rekap = getLatestOrEmpty();

        List<Number> data = Arrays.asList(rekap.pagu_reverse_auction,
                rekap.pagu_total - rekap.pagu_reverse_auction);
        // build and return chart
        return ChartBuilder.buildDonut("Pagu Reverse Auction", seriesLabels, data);
    }

    public Chartable chartGagalUlang() {
        // query
        List<RekapPemilihanMetode> data = list10Years();

        // build and return chart
        int thisYear = Year.now().getValue();
        int tenYearAgo = thisYear - 9;
        return ChartBuilder.buildYearSeries(
                tenYearAgo,
                thisYear,
                Arrays.asList("pagu_ulang", "pagu_gagal"),
                Arrays.asList("Tender/Seleksi Ulang", "Tender/Seleksi Gagal"),
                data
        );
    }

    @Override
    public Class<RekapPemilihanMetode> getReturnType() {
        return RekapPemilihanMetode.class;
    }
}
