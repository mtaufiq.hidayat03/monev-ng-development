package repositories.dashboard;

import models.common.contracts.Chartable;
import models.dashboard.RekapUkpbj;
import models.dashboard.pojo.RekapUkbpjSummary;
import models.dashboard.pojo.RekapUkpbjResult;
import models.dashboard.queries.RekapUkpbjSearchQuery;
import play.db.jdbc.Query;
import utils.common.ChartBuilder;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class RekapUkpbjRepository {

    public static final String TAG = "RekapUkpbjRepository";


    private static final RekapUkpbjRepository repository = new RekapUkpbjRepository();

    public static RekapUkpbjRepository getInstance() {
        return repository;
    }

    public RekapUkbpjSummary getSummary() {
        RekapUkbpjSummary summary = new RekapUkbpjSummary();
        summary.jumlahUkpbj = RekapUkpbj.count();

        String sql = "select count(1) as jumlah, kelembagaan from rekap_ukpbj group by kelembagaan";
        List<Object[]> kelambagaanList = Query.findList(sql, (ResultSet resultSet) -> {
            Object[] res = new Object[2];
            res[0] = resultSet.getLong("jumlah");
            res[1] = RekapUkpbj.KELEMBAGAAN.valueOf(resultSet.getString("kelembagaan"));
            return res;
        });
        for (Object[] kelembagaan : kelambagaanList) {
            if (kelembagaan[1].equals(RekapUkpbj.KELEMBAGAAN.STRUKTURAL)) {
                summary.jumlahStruktural = (Long) kelembagaan[0];
                continue;
            }
            if (kelembagaan[1].equals(RekapUkpbj.KELEMBAGAAN.ADHOC)) {
                summary.jumlahAdhoc = (Long) kelembagaan[0];
                continue;
            }
            if (kelembagaan[1].equals(RekapUkpbj.KELEMBAGAAN.BELUM_TERBENTUK)) {
                summary.jumlahBelumTerbentuk = (Long) kelembagaan[0];
                continue;
            }
        }

        summary.jumlahKematangan9 = RekapUkpbj.count("kematangan = 9");

        return summary;
    }

    public RekapUkpbjResult list(RekapUkpbjSearchQuery searchQuery) {
        StringBuilder sql = new StringBuilder("select * from rekap_ukpbj");
        List<Object> params = new ArrayList<>();
        if (searchQuery.isKelembagaanExists()) {
            sql.append(" where kelembagaan = ?");
            params.add(searchQuery.getKelembagaan());
        }

        if (searchQuery.isKematanganExists()) {
            sql.append(params.isEmpty() ? " where " : " and ").append("kematangan = ?");
            params.add(searchQuery.getKematangan());
        }

        if (searchQuery.isKeywordExist()) {
            sql.append(params.isEmpty() ? " where " : " and ").append("nama ilike ?");
            params.add("%" + searchQuery.getKeyword() + "%");
        }
        sql.append(" order by nama asc");
        RekapUkpbjResult result = new RekapUkpbjResult(searchQuery);
        result.executeQuery(sql.toString(), params.toArray());

        return result;
    }

    public Chartable kelembagaanChart() {
        // ambil label seriesnya
        List<RekapUkpbj.KELEMBAGAAN> kelembagaanList = new ArrayList<>(Arrays.asList(RekapUkpbj.KELEMBAGAAN.values()));
        List<String> seriesLabels = kelembagaanList.stream().map(kelembagaan -> kelembagaan.label).collect(Collectors.toList());
        // prepare query data
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1) as total, kelembagaan from rekap_ukpbj ");
        sb.append(" where kelembagaan is NOT NULL group BY kelembagaan");

        // query
        List<Object[]> result = Query.find(sb.toString(), (ResultSet resultSet) -> {
            Object[] res = new Object[2];
            res[0] = RekapUkpbj.KELEMBAGAAN.valueOf(resultSet.getString("kelembagaan"));
            res[1] = resultSet.getLong("total");
            return res;
        }).fetch();
        List<Number> data = new ArrayList<>(Collections.nCopies(kelembagaanList.size(), 0));
        for (Object[] obj : result) {
            int idx = kelembagaanList.indexOf(obj[0]);
            data.set(idx, (Long) obj[1]);
        }
        // build and return chart
        return ChartBuilder.buildDonut("Kelembagaan", seriesLabels, data);
    }

    public Chartable kematanganChart() {
        // ambil label seriesnya
        List<Integer> kematanganList = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9);
        List<String> seriesLabels = kematanganList.stream().map(i -> i + "/" + (kematanganList.size() - 1)).collect(Collectors.toList());
        // prepare query data
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1) as total, kematangan from rekap_ukpbj ");
        sb.append(" group BY kematangan");

        // query
        List<Object[]> result = Query.find(sb.toString(), (ResultSet resultSet) -> {
            Object[] res = new Object[2];
            res[0] = resultSet.getInt("kematangan");
            res[1] = resultSet.getLong("total");
            return res;
        }).fetch();
        List<Number> data = new ArrayList<>(Collections.nCopies(kematanganList.size(), 0));
        for (Object[] obj : result) {
            int idx = kematanganList.indexOf(obj[0]);
            data.set(idx, (Long) obj[1]);
        }
        // build and return chart
        return ChartBuilder.buildDonut("Kematangan", seriesLabels, data);
    }

    public Chartable perKldiChart() {
        // siapkan label seriesnya
        List<String> seriesLabels = new ArrayList<>();
        // prepare query data
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1) as total, kode_kldi, nama_kldi from rekap_ukpbj ");
        sb.append(" group BY kode_kldi, nama_kldi");
        sb.append(" order BY count(1) desc");

        // query
        List<Object[]> result = Query.find(sb.toString(), (ResultSet resultSet) -> {
            Object[] res = new Object[3];
            res[0] = resultSet.getString("kode_kldi");
            res[1] = resultSet.getLong("total");
            res[2] = resultSet.getString("nama_kldi");
            return res;
        }).fetch();
        int jumlahData = 10;
        List<Number> data = new ArrayList<>();
        for (int i = 0; i < result.size() && i < jumlahData; i++) {
            Object[] obj = result.get(i);
            data.add((Long) obj[1]);
            seriesLabels.add(obj[2].toString());
        }
        // build and return chart
        return ChartBuilder.buildDonut("Jumlah per KLPD", seriesLabels, data);
    }
}
