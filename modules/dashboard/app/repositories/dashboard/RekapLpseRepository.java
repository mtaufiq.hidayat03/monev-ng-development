package repositories.dashboard;

import models.common.contracts.Chartable;
import models.dashboard.DetailLpse;
import models.dashboard.RekapLpse;
import models.dashboard.pojo.DetailLpseResult;
import models.dashboard.queries.DetailLpseSearchQuery;
import play.db.jdbc.Query;
import utils.common.ChartBuilder;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class RekapLpseRepository {

    public static final String TAG = "RekapLpseRepository";


    private static final RekapLpseRepository repository = new RekapLpseRepository();

    public static RekapLpseRepository getInstance() {
        return repository;
    }

    public RekapLpse getLatest() {
        return RekapLpse.find("1=1 order by tanggal_update desc").first();
    }

    public List<String> distinctVersiSpse() {
        return Query.findList("select distinct versi_spse from detail_lpse", String.class);
    }

    private Object[] getListQuery(DetailLpseSearchQuery searchQuery) {
        StringBuilder sql = new StringBuilder("select * from detail_lpse");
        List<Object> params = new ArrayList<>();
        if (searchQuery.isStandarLengkapExists()) {
            sql.append(" where jumlah_standar ").append(searchQuery.isStandarLengkap() ? "=" : "<").append(" ?");
            params.add(17);
        }

        if (searchQuery.isVersiSpseExists()) {
            sql.append(params.isEmpty() ? " where " : " and ").append("versi_spse = ?");
            params.add(searchQuery.getVersiSpse());
        }

        if (searchQuery.isKeywordExist()) {
            sql.append(params.isEmpty() ? " where " : " and ").append("nama ilike ?");
            params.add("%"+searchQuery.getKeyword()+"%");
        }
        sql.append(" order by nama asc");
        return new Object[]{sql.toString(), params};
    }

    public List<DetailLpse> getAllForMapBy(DetailLpseSearchQuery searchQuery) {
        final Object[] sql = getListQuery(searchQuery);
        final List<Object> params = (List<Object>) sql[1];
        return Query.find((String) sql[0], DetailLpse.class, params.toArray()).fetch();
    }

    public DetailLpseResult listDetail(DetailLpseSearchQuery searchQuery) {
        DetailLpseResult result = new DetailLpseResult(searchQuery);
        final Object[] sql = getListQuery(searchQuery);
        final List<Object> params = (List<Object>) sql[1];
        result.executeQuery((String) sql[0], params.toArray());
        return result;
    }

    public Chartable standardisasiChart() {
        // ambil label seriesnya
        List<String> seriesLabels = Arrays.asList("17 Standar", "Terstandar");
        // prepare query data
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1) as total, jumlah_standar from detail_lpse ");
        sb.append(" group BY jumlah_standar");

        // query
        List<Object[]> result = Query.find(sb.toString(), (ResultSet resultSet) -> {
            Object[] res = new Object[2];
            res[0] = resultSet.getInt("jumlah_standar");
            res[1] = resultSet.getLong("total");
            return res;
        }).fetch();
        List<Number> data = new ArrayList<>(Collections.nCopies(seriesLabels.size(), 0l));
        for (Object[] obj: result) {
            Integer jumlahStandar = (Integer)obj[0];
            Long total = (Long)obj[1];
            if (jumlahStandar.equals(17)) {
                data.set(0, total);
                continue;
            }

            data.set(1, (Long)data.get(1) + total);
        }
        // build and return chart
        return ChartBuilder.buildDonut("Standardisasi LPSE", seriesLabels, data);
    }

    public Chartable versiSpseChart() {
        // ambil label seriesnya
        List<String> seriesLabels = Query.findList("select distinct versi_spse from detail_lpse", String.class);

        // prepare query data
        StringBuilder sb = new StringBuilder();
        sb.append("select count(1) as total, versi_spse from detail_lpse ");
        sb.append(" group BY versi_spse");

        // query
        List<Object[]> result = Query.find(sb.toString(), (ResultSet resultSet) -> {
            Object[] res = new Object[2];
            res[0] = resultSet.getString("versi_spse");
            res[1] = resultSet.getLong("total");
            return res;
        }).fetch();
        List<Number> data = new ArrayList<>(Collections.nCopies(seriesLabels.size(), 0));
        for (Object[] obj: result) {
            int idx = seriesLabels.indexOf(obj[0]);
            data.set(idx, (Long)obj[1]);
        }
        // build and return chart
        return ChartBuilder.buildDonut("Versi SPSE", seriesLabels, data);
    }

}
