package repositories.dashboard;

import models.common.contracts.Chartable;
import models.dashboard.InovasiPengadaan;
import utils.common.ChartBuilder;

import java.time.Year;
import java.util.Arrays;
import java.util.List;

public class InovasiPengadaanRepository {

    public static final String TAG = "InovasiPengadaanRepository";

    private static final InovasiPengadaanRepository repository = new InovasiPengadaanRepository();

    public static InovasiPengadaanRepository getInstance() {
        return repository;
    }

    public InovasiPengadaan getLatestOrEmpty() {
        InovasiPengadaan rekap = InovasiPengadaan.find("1=1 order by tahun desc").first();
        if (rekap != null) {

            return rekap;
        }

        return new InovasiPengadaan();
    }

    public List<InovasiPengadaan> list10Years() {
        int tenYearAgo = Year.now().getValue() - 10;
        return InovasiPengadaan.find("tahun > ? order by tahun", tenYearAgo).fetch();
    }

    public Chartable chartPagu() {
        // query
        List<InovasiPengadaan> data = list10Years();

        // build and return chart
        return ChartBuilder.buildNmostBar(
                Arrays.asList("pagu_dini", "pagu_konsolidasi", "pagu_itemize", "pagu_lelang_cepat", "pagu_reverse_auction"),
                Arrays.asList("Tender Dini", "Konsolidasi", "Itemize", "Tender Cepat", "Reverse Auction"),
                data, 10);
    }

    public Chartable chartPaket() {
        // query
        List<InovasiPengadaan> data = list10Years();

        // build and return chart
        return ChartBuilder.buildNmostBar(
                Arrays.asList("paket_dini", "paket_konsolidasi", "paket_itemize", "paket_lelang_cepat", "paket_reverse_auction"),
                Arrays.asList("Tender Dini", "Konsolidasi", "Itemize", "Tender Cepat", "Reverse Auction"),
                data, 10);
    }

}
