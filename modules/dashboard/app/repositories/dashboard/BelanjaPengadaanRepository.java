package repositories.dashboard;

import models.common.contracts.Chartable;
import models.dashboard.BelanjaPengadaan;
import play.db.jdbc.Query;
import utils.common.LogUtil;

import java.sql.ResultSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author HanusaCloud on 9/12/2019 1:47 PM
 */
public class BelanjaPengadaanRepository {

    public static final String TAG = "BelanjaPengadaanRepository";

    public static List<BelanjaPengadaan> getReport() {
        LogUtil.debug(TAG, "get belanja pengadaan data chart");
        return Query.find("SELECT header2, " +
                "jenis_penyedia, " +
                "sum(nilai) as nilai " +
                "FROM dwh_procurement.monev_ng_report_01a " +
                "WHERE header2 in ('Total','Belanja Pengadaan','RUP','Pemilihan/Pelaksanaan','Hasil Pemilihan') " +
                "GROUP BY header2, jenis_penyedia", BelanjaPengadaan.class).fetch();
    }

    public static Chartable trendPengadaan() {
        String rencanaBelanjaQ = "select sum(nilai) as total,extract(year from tanggal_awal_pengadaan) as year from dwh_procurement.monev_ng_report_01c " +
                "group by extract(year from tanggal_awal_pengadaan) " +
                "order by extract(year from tanggal_awal_pengadaan)";
        String rencanaKontrakQ = "select sum(nilai) as total,extract(year from tanggal_awal_pekerjaan) as year from dwh_procurement.monev_ng_report_01c " +
                "group by extract(year from tanggal_awal_pekerjaan) " +
                "order by extract(year from tanggal_awal_pekerjaan)";

        List<Object[]> resultRencanaBelanja = Query.find(rencanaBelanjaQ, (ResultSet resultSet) -> {
                Object[] res = new Object[2];
                res[0] = resultSet.getLong("total");
                res[1] = resultSet.getInt("year");
                return res;
            }).fetch();

        List<Object[]> resultRencanaKontrak = Query.find(rencanaKontrakQ, (ResultSet resultSet) -> {
            Object[] res = new Object[2];
            res[0] = resultSet.getLong("total");
            res[1] = resultSet.getInt("year");
            return res;
        }).fetch();

        int i = 0;
        Chartable chartable = new Chartable();
        Map<Integer, Long> rencanaBelanja = new LinkedHashMap<>();
        Map<Integer, Long> rencanaKontrak = new LinkedHashMap<>();
        for (int year=2015; year<=2019; year++) {
            rencanaBelanja.put(year, 0l);
            rencanaKontrak.put(year, 0l);
            chartable.labels.add(year);
        }

        for (Object[] item: resultRencanaBelanja) {
            rencanaBelanja.put((Integer)item[1], (Long)item[0]);
        }
        for (Object[] item: resultRencanaKontrak) {
            rencanaKontrak.put((Integer)item[1], (Long)item[0]);
        }

        Chartable.ChartItem itemRencanaBelanja = new Chartable.ChartItem("Rencana Belanja", 1);
        for (Long data: rencanaBelanja.values()) {
            itemRencanaBelanja.data.add(data);
        }
        Chartable.ChartItem itemRencanaKontrak = new Chartable.ChartItem("Rencana Kontrak", 2);
        for (Long data: rencanaKontrak.values()) {
            itemRencanaKontrak.data.add(data);
        }
        chartable.datasets.add(itemRencanaBelanja);
        chartable.datasets.add(itemRencanaKontrak);

        return chartable;
    }

}
