package repositories.dashboard;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import models.common.contracts.Chartable;
import models.common.contracts.DataTablePagination;
import models.common.contracts.TablePagination;
import models.dashboard.*;
import models.dashboard.pojo.*;
import org.joda.time.YearMonth;
import permission.Kldi;
import play.db.jdbc.Query;
import utils.common.ChartBuilder;
import utils.common.LogUtil;

import java.time.Year;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RekapPengadaanRepository {

    public static final String TAG = "RekapPengadaanRepository";

    private static final Map<String, String> filterColumnName = Stream.of(new String[][]{
            {"Jenis Belanja", "jenis_belanja"},
            {"Metode Pemilihan", "metode_pemilihan"},
            {"Jenis Pengadaan", "jenis_pengadaan"}
    }).collect(Collectors.toMap(data -> data[0], data -> data[1]));

    private static final RekapPengadaanRepository repository = new RekapPengadaanRepository();

    public static RekapPengadaanRepository getInstance() {
        return repository;
    }

    public boolean isExistsTahun(int tahun) {
        return RekapPengadaanNasional.count("tahun = ?", tahun) > 0;
    }

    public RekapPengadaanNasional getNasional(Long id) {
        return RekapPengadaanNasional.findById(id);
    }

    public Chartable trend10Tahun() {
        // query
        int tenYearAgo = Year.now().getValue() - 10;
        List<RekapPengadaanNasional> data =
                RekapPengadaanNasional.find("tahun > ? order by tahun asc", tenYearAgo).fetch();

        // build and return chart
        return ChartBuilder.buildNmostBar(
                Arrays.asList("belanja_pengadaan", "rencana_pagu", "pelaksanaan_pagu", "kontrak"),
                Arrays.asList("Belanja Pengadaan K/L/PD", "Perencanaan Pengadaan", "Pagu Pengumuman", "Kontrak"),
                data, 10);
    }

    public RekapPengadaanNasional rekapNasional() {
        String sql = "select total_anggaran , belanja_pengadaan," +
                " rencana_paket, rencana_pagu, pelaksanaan_paket, pelaksanaan_pagu," +
                " hasil_paket, hasil_pagu, kontrak, pembayaran, tahun" +
                " from public.rekap_pengadaan_nasional" +
                " order by tahun desc";
        return Query.find(sql, RekapPengadaanNasional.class).first();
    }

    public RekapPengadaanNasional rekapNasional(String year) {
        String sql = "select total_anggaran , belanja_pengadaan," +
                " rencana_paket, rencana_pagu, pelaksanaan_paket, pelaksanaan_pagu," +
                " hasil_paket, hasil_pagu, kontrak, pembayaran, tahun" +
                " from public.rekap_pengadaan_nasional" +
                " where tahun='" + year + "'" +
                " order by tahun desc";
        return Query.find(sql, RekapPengadaanNasional.class).first();
    }

    public List<RekapPengadaanNasional> listYears() {
        String sql = "select DISTINCT(tahun)" +
                " from public.rekap_pengadaan_nasional" +
                " order by tahun desc";
        return Query.find(sql, RekapPengadaanNasional.class).fetch();
    }

    public RekapPengadaanNasionalDetailResult listNasionalDetail(SearchQuery searchQuery, String year) {
        RekapPengadaanNasionalDetailResult result = new RekapPengadaanNasionalDetailResult(searchQuery);
        String sql = "select jenis_pemerintahan as label, jenis_pemerintahan as kode, sum(total_anggaran) total_anggaran, " +
                "sum(belanja_pengadaan) belanja_pengadaan," +
                " sum(rencana_paket) rencana_paket, sum(rencana_pagu) rencana_pagu, sum(pelaksanaan_paket) " +
                "pelaksanaan_paket, sum(pelaksanaan_pagu) pelaksanaan_pagu," +
                " sum(hasil_paket) hasil_paket, sum(hasil_pagu) hasil_pagu, sum(kontrak) kontrak, sum(pembayaran) " +
                "pembayaran" +
                " from public.rekap_pengadaan_nasional_detail where tahun_anggaran='" + year + "'" +
                " GROUP BY jenis_pemerintahan ORDER BY" +
                " CASE WHEN jenis_pemerintahan = 'Pusat' THEN 0" +
                " WHEN jenis_pemerintahan = 'Daerah' THEN 1" +
                " ELSE 2 END";

        result.executeQuery(sql);
        return result;
    }

    public RekapPengadaanResult listRoot(SearchQuery searchQuery) {
        RekapPengadaanResult result = new RekapPengadaanResult(searchQuery);
        result.executeQuery("select sumber_dana as label, sumber_dana as kode, sum(total_anggaran) total_anggaran, " +
                "sum(belanja_pengadaan) belanja_pengadaan," +
                " sum(rencana_paket) rencana_paket, sum(rencana_pagu) rencana_pagu, sum(pelaksanaan_paket) " +
                "pelaksanaan_paket, sum(pelaksanaan_pagu) pelaksanaan_pagu," +
                " sum(hasil_paket) hasil_paket, sum(hasil_pagu) hasil_pagu, sum(kontrak) kontrak, sum(pembayaran) " +
                "pembayaran" +
                " from public.rekap_pengadaan" +
                " WHERE tahun_anggaran=" + searchQuery.getTahun() +
                (searchQuery.isFilterExist() && filterColumnName.containsKey(searchQuery.getFilter()) ?
                        " and " + filterColumnName.get(searchQuery.getFilter()) + "='" +
                                searchQuery.getSubFilter() + "'" : "") +
                (searchQuery.isKeywordExist() ? " and sumber_dana ilike '%" + searchQuery.getKeyword() + "%'" : "") +
                " GROUP BY sumber_dana ORDER BY" +
                " CASE WHEN sumber_dana = 'Pusat' THEN 0" +
                " WHEN sumber_dana = 'Daerah' THEN 1" +
                " ELSE 2 END");
        return result;
    }

    public RekapPengadaanNasionalDetailResult listRootNasionalDetail(SearchQuery searchQuery) {
        RekapPengadaanNasionalDetailResult result = new RekapPengadaanNasionalDetailResult(searchQuery);
        result.executeQuery("select jenis_pemerintahan as label, jenis_pemerintahan as kode, sum(total_anggaran) total_anggaran, " +
                "sum(belanja_pengadaan) belanja_pengadaan," +
                " sum(rencana_paket) rencana_paket, sum(rencana_pagu) rencana_pagu, sum(pelaksanaan_paket) " +
                "pelaksanaan_paket, sum(pelaksanaan_pagu) pelaksanaan_pagu," +
                " sum(hasil_paket) hasil_paket, sum(hasil_pagu) hasil_pagu, sum(kontrak) kontrak, sum(pembayaran) " +
                "pembayaran" +
                " from public.rekap_pengadaan_nasional_detail" +
                " WHERE tahun_anggaran=" + searchQuery.getTahun() +
                (searchQuery.isFilterExist() && filterColumnName.containsKey(searchQuery.getFilter()) ?
                        " and " + filterColumnName.get(searchQuery.getFilter()) + "='" +
                                searchQuery.getSubFilter() + "'" : "") +
                (searchQuery.isKeywordExist() ? " and jenis_pemerintahan ilike '%" + searchQuery.getKeyword() + "%'" : "") +
                " GROUP BY jenis_pemerintahan ORDER BY" +
                " CASE WHEN jenis_pemerintahan = 'Pusat' THEN 0" +
                " WHEN jenis_pemerintahan = 'Daerah' THEN 1" +
                " ELSE 2 END");
        return result;
    }

    public RekapPengadaanResult listRoot(SearchQuery searchQuery, String year) {
        RekapPengadaanResult result = new RekapPengadaanResult(searchQuery);
        result.executeQuery("select sumber_dana as label, sumber_dana as kode, sum(total_anggaran) total_anggaran, " +
                "sum(belanja_pengadaan) belanja_pengadaan," +
                " sum(rencana_paket) rencana_paket, sum(rencana_pagu) rencana_pagu, sum(pelaksanaan_paket) " +
                "pelaksanaan_paket, sum(pelaksanaan_pagu) pelaksanaan_pagu," +
                " sum(hasil_paket) hasil_paket, sum(hasil_pagu) hasil_pagu, sum(kontrak) kontrak, sum(pembayaran) " +
                "pembayaran" +
                " from public.rekap_pengadaan" +
                " WHERE tahun_anggaran='" + year + "'" +
                (searchQuery.isFilterExist() && filterColumnName.containsKey(searchQuery.getFilter()) ?
                        " and " + filterColumnName.get(searchQuery.getFilter()) + "='" +
                                (filterColumnName.get(searchQuery.getFilter()) == "jenis_pengadaan" ?
                                        searchQuery.getType() : searchQuery.getSubFilter()) + "'" : "") +
                (searchQuery.isKeywordExist() ? " and sumber_dana ilike '%" + searchQuery.getKeyword() + "%'" : "") +
                " GROUP BY sumber_dana" +
                " ORDER BY sumber_dana");
        return result;
    }

    public RekapPengadaanResult listAnggaran(SearchQuery searchQuery) {
        RekapPengadaanResult result = new RekapPengadaanResult(searchQuery);
        result.executeQuery("select tipe_kldi as label, tipe_kldi as kode, sum(total_anggaran) total_anggaran, sum" +
                "(belanja_pengadaan) belanja_pengadaan," +
                " sum(rencana_paket) rencana_paket, sum(rencana_pagu) rencana_pagu, sum(pelaksanaan_paket) " +
                "pelaksanaan_paket, sum(pelaksanaan_pagu) pelaksanaan_pagu," +
                " sum(hasil_paket) hasil_paket, sum(hasil_pagu) hasil_pagu, sum(kontrak) kontrak, sum(pembayaran) " +
                "pembayaran" +
                " from public.rekap_pengadaan" +
                " WHERE tahun_anggaran=" + searchQuery.getTahun() +
                (searchQuery.isFilterExist() && filterColumnName.containsKey(searchQuery.getFilter()) ?
                        " and " + filterColumnName.get(searchQuery.getFilter()) + "='" +
                                (filterColumnName.get(searchQuery.getFilter()) == "jenis_pengadaan" ?
                                        searchQuery.getType() : searchQuery.getSubFilter()) + "'" : "") +
                " and sumber_dana = '" + searchQuery.getAnggaran() + "'" +
                (searchQuery.isKeywordExist() ? " and tipe_kldi ilike '%" + searchQuery.getKeyword() + "%'" : "") +
                " GROUP BY tipe_kldi" +
                " ORDER BY tipe_kldi");
        return result;
    }

    public RekapPengadaanNasionalDetailResult listAnggaranNasionalDetail(SearchQuery searchQuery) {
        RekapPengadaanNasionalDetailResult result = new RekapPengadaanNasionalDetailResult(searchQuery);
        result.executeQuery("select jenis_kldi as label, jenis_kldi as kode, sum(total_anggaran) total_anggaran, sum" +
                "(belanja_pengadaan) belanja_pengadaan," +
                " sum(rencana_paket) rencana_paket, sum(rencana_pagu) rencana_pagu, sum(pelaksanaan_paket) " +
                "pelaksanaan_paket, sum(pelaksanaan_pagu) pelaksanaan_pagu," +
                " sum(hasil_paket) hasil_paket, sum(hasil_pagu) hasil_pagu, sum(kontrak) kontrak, sum(pembayaran) " +
                "pembayaran" +
                " from public.rekap_pengadaan_nasional_detail" +
                " WHERE tahun_anggaran=" + searchQuery.getTahun() +
                (searchQuery.isFilterExist() && filterColumnName.containsKey(searchQuery.getFilter()) ?
                        " and " + filterColumnName.get(searchQuery.getFilter()) + "='" +
                                (filterColumnName.get(searchQuery.getFilter()) == "jenis_pengadaan" ?
                                        searchQuery.getType() : searchQuery.getSubFilter()) + "'" : "") +
                " and jenis_pemerintahan = '" + searchQuery.getAnggaran() + "'" +
                (searchQuery.isKeywordExist() ? " and jenis_kldi ilike '%" + searchQuery.getKeyword() + "%'" : "") +
                " GROUP BY jenis_kldi" +
                " ORDER BY jenis_kldi");
        return result;
    }

    public RekapPengadaanResult listKlpd(SearchQuery searchQuery) {
        RekapPengadaanResult result = new RekapPengadaanResult(searchQuery);
        result.executeQuery("select nama_kldi as label, kode_kldi as kode, sum(total_anggaran) total_anggaran, sum" +
                "(belanja_pengadaan) belanja_pengadaan," +
                " sum(rencana_paket) rencana_paket, sum(rencana_pagu) rencana_pagu, sum(pelaksanaan_paket) " +
                "pelaksanaan_paket, sum(pelaksanaan_pagu) pelaksanaan_pagu," +
                " sum(hasil_paket) hasil_paket, sum(hasil_pagu) hasil_pagu, sum(kontrak) kontrak, sum(pembayaran) " +
                "pembayaran" +
                " from public.rekap_pengadaan" +
                " WHERE tahun_anggaran=" + searchQuery.getTahun() +
                (searchQuery.isFilterExist() && filterColumnName.containsKey(searchQuery.getFilter()) ?
                        " and " + filterColumnName.get(searchQuery.getFilter()) + "='" +
                                (filterColumnName.get(searchQuery.getFilter()) == "jenis_pengadaan" ?
                                        searchQuery.getType() : searchQuery.getSubFilter()) + "'" : "") +
                " and sumber_dana = '" + searchQuery.getAnggaran() + "'" +
                " and tipe_kldi = '" + searchQuery.getTipe() + "'" + (searchQuery.isKeywordExist() ? " and nama_kldi " +
                "ilike '%" + searchQuery.getKeyword() + "%'" : "") +
                " GROUP BY kode_kldi, nama_kldi" +
                " ORDER BY nama_kldi");
        return result;
    }

    public RekapPengadaanNasionalDetailResult listKlpdNasionalDetail(SearchQuery searchQuery) {
        RekapPengadaanNasionalDetailResult result = new RekapPengadaanNasionalDetailResult(searchQuery);
        result.executeQuery("select nama_kldi as label, kode_kldi as kode, sum(total_anggaran) total_anggaran, sum" +
                "(belanja_pengadaan) belanja_pengadaan," +
                " sum(rencana_paket) rencana_paket, sum(rencana_pagu) rencana_pagu, sum(pelaksanaan_paket) " +
                "pelaksanaan_paket, sum(pelaksanaan_pagu) pelaksanaan_pagu," +
                " sum(hasil_paket) hasil_paket, sum(hasil_pagu) hasil_pagu, sum(kontrak) kontrak, sum(pembayaran) " +
                "pembayaran" +
                " from public.rekap_pengadaan_nasional_detail" +
                " WHERE tahun_anggaran=" + searchQuery.getTahun() +
                (searchQuery.isFilterExist() && filterColumnName.containsKey(searchQuery.getFilter()) ?
                        " and " + filterColumnName.get(searchQuery.getFilter()) + "='" +
                                (filterColumnName.get(searchQuery.getFilter()) == "jenis_pengadaan" ?
                                        searchQuery.getType() : searchQuery.getSubFilter()) + "'" : "") +
                " and jenis_pemerintahan = '" + searchQuery.getAnggaran() + "'" +
                " and jenis_kldi = '" + searchQuery.getTipe() + "'" + (searchQuery.isKeywordExist() ? " and nama_kldi " +
                "ilike '%" + searchQuery.getKeyword() + "%'" : "") +
                " GROUP BY kode_kldi, nama_kldi" +
                " ORDER BY nama_kldi");
        return result;
    }

    public RekapPengadaanResult listSatker(SearchQuery searchQuery) {
        RekapPengadaanResult result = new RekapPengadaanResult(searchQuery);
        result.executeQuery("select kode_satker as kode, nama_satker as label, sum(total_anggaran) total_anggaran, " +
                "sum(belanja_pengadaan) belanja_pengadaan," +
                " sum(rencana_paket) rencana_paket, sum(rencana_pagu) rencana_pagu, sum(pelaksanaan_paket) " +
                "pelaksanaan_paket, sum(pelaksanaan_pagu) pelaksanaan_pagu," +
                " sum(hasil_paket) hasil_paket, sum(hasil_pagu) hasil_pagu, sum(kontrak) kontrak, sum(pembayaran) " +
                "pembayaran" +
                " from public.rekap_pengadaan" +
                " WHERE tahun_anggaran=" + searchQuery.getTahun() +
                (searchQuery.isFilterExist() && filterColumnName.containsKey(searchQuery.getFilter()) ?
                        " and " + filterColumnName.get(searchQuery.getFilter()) + "='" +
                                (filterColumnName.get(searchQuery.getFilter()) == "jenis_pengadaan" ?
                                        searchQuery.getType() : searchQuery.getSubFilter()) + "'" : "") +
                " and sumber_dana = '" + searchQuery.getAnggaran() + "'" +
                " and tipe_kldi = '" + searchQuery.getTipe() + "'" + " and kode_kldi = '" + searchQuery.getKlpd() +
                "'" +
                (searchQuery.isKeywordExist() ? " and nama_satker ilike '%" + searchQuery.getKeyword() + "%'" : "") +
                " GROUP BY kode_satker, nama_satker" +
                " ORDER BY kode_satker");
        return result;
    }

    public List<DetailPengadaan> getTenderTypes() {
        return Query.find("SELECT jenis_pengadaan as label, sum(paket) as paket, sum(nilai) as pagu" +
                " FROM \"dwh_procurement\".\"monev_ng_report_01c\"" +
                " GROUP BY jenis_pengadaan" +
                " ORDER BY pagu DESC", DetailPengadaan.class).fetch();
    }

    public JenisResult getTenderTypes(TablePagination.ParamAble paramAble) {
        JenisResult result = new JenisResult(paramAble);
        result.executeQuery("SELECT jenis_pengadaan as label, sum(paket) as paket, sum(nilai) as pagu" +
                " FROM \"dwh_procurement\".\"monev_ng_report_01c\"" +
                " GROUP BY jenis_pengadaan" +
                " ORDER BY pagu DESC");
        return result;
    }

    public JenisResult getByRegion(QueryByTenderType paramAble) {
        JenisResult result = new JenisResult(paramAble);
        result.executeQuery("SELECT pusat_daerah as label, sum(paket) as paket, sum(nilai) as pagu" +
                " FROM \"dwh_procurement\".\"monev_ng_report_01c\"" +
                " WHERE jenis_pengadaan = '" + paramAble.getType() + "'" +
                " GROUP BY pusat_daerah" +
                " ORDER BY pagu DESC");
        return result;
    }

    public JenisResult getKldiType(QueryByTenderType paramAble) {
        JenisResult result = new JenisResult(paramAble);
        result.executeQuery("SELECT tipe_kldi as label, sum(paket) as paket, sum(nilai) as pagu" +
                " FROM \"dwh_procurement\".\"monev_ng_report_01c\"" +
                " WHERE jenis_pengadaan = '" + paramAble.getType() + "'" +
                " AND pusat_daerah = '" + paramAble.getRegion() + "'" +
                " GROUP BY tipe_kldi" +
                " ORDER BY pagu DESC");
        return result;
    }

    public JenisResult getKldiList(KldiQueryByTenderType paramAble) {
        JenisResult result = new JenisResult(paramAble);
        result.executeQuery("SELECT kode_kldi as kode," +
                " nama_kldi as label," +
                " sum(paket) as paket," +
                " sum(nilai) as pagu" +
                " FROM \"dwh_procurement\".\"monev_ng_report_01c\"" +
                " WHERE jenis_pengadaan = '" + paramAble.getType() + "'" +
                " AND pusat_daerah = '" + paramAble.getRegion() + "'" +
                " AND tipe_kldi = '" + paramAble.getKldiType() + "'" +
                " GROUP BY kode_kldi, nama_kldi" +
                " ORDER BY pagu DESC");
        return result;
    }

    public JenisResult getSatkers(SatkerQueryByTenderType query) {
        JenisResult result = new JenisResult(query);
        result.executeQuery("SELECT satker as label," +
                " sum(paket) as paket," +
                " sum(nilai) as pagu" +
                " FROM \"dwh_procurement\".\"monev_ng_report_01c\"" +
                " WHERE jenis_pengadaan = '" + query.getType() + "'" +
                " AND pusat_daerah = '" + query.getRegion() + "'" +
                " AND tipe_kldi = '" + query.getKldiType() + "'" +
                " AND kode_kldi = '" + query.getKldi() + "'" +
                " GROUP BY satker" +
                " ORDER BY pagu DESC ");
        return result;
    }

    public JsonObject generateJson() {
        List<RekapPengadaanNasional> summaries = RekapPengadaanNasional.find("1=1 order by tahun desc").fetch();
        JsonObject result = new JsonObject();
        JsonArray jsonArray = new JsonArray();

        int i = 1;
        for (RekapPengadaanNasional model : summaries) {
            JsonArray array = new JsonArray();
            array.add(i);
            array.add(model.tahun);
            array.add(model.getFormattedTanggalUpdate());
            array.add(model.isAdaUpdate() ? "Ada" : "Tidak");
            array.add(model.id);
            jsonArray.add(array);
            i++;
        }
        result.add("data", jsonArray);
        LogUtil.debug(TAG, result);
        return result;
    }

    public RekapPengadaanNasionalResult getRekapPengadaanResult(DataTablePagination.Request request) {
        RekapPengadaanNasionalResult rekapPengadaanNasionalResult = new RekapPengadaanNasionalResult(request);
        rekapPengadaanNasionalResult.executeQuery();
        return rekapPengadaanNasionalResult;
    }

    /**************** START: Original ***********************/
    public RekapPengadaanNasionalOriginal getOriginalByYear(int tahun) {
        return RekapPengadaanNasionalOriginal.find("tahun = ?", tahun).first();
    }
    /**************** END: Original **********************/

    /**************** START: Per bulan ***********************/
    public Chartable perencanaan1TahunChart() {
        YearMonth thisMonth = YearMonth.now();
        YearMonth oneYearAgo = thisMonth.minusYears(1);
        List<RekapPerencanaanBulan> rekaps = RekapPerencanaanBulan.find("bulan > ? and bulan <= ?",
                oneYearAgo.toLocalDate(1).toDate(), thisMonth.toLocalDate(1).toDate()).fetch();

        // build and return chart
        return ChartBuilder.buildNmostBar(
                Arrays.asList("rencana_pagu"),
                Arrays.asList("Perencanaan"),
                rekaps, 12);
    }

    /**************** END: Per bulan ***********************/

    public RekapPengadaan getBy(Kldi kldi) {
        LogUtil.debug(TAG, "get rekap pengadaan by kldi: " + kldi.id);
        final String query = "SELECT sum(total_anggaran) AS total_anggaran, " +
                "sum(belanja_pengadaan) AS belanja_pengadaan, " +
                "sum(rencana_paket) AS rencana_paket, " +
                "sum(rencana_pagu) AS rencana_pagu, " +
                "sum(pelaksanaan_paket) AS pelaksanaan_paket, " +
                "sum(pelaksanaan_pagu) AS pelaksanaan_pagu, " +
                "sum(hasil_paket) AS hasil_paket, " +
                "sum(hasil_pagu) AS hasil_pagu, " +
                "sum(kontrak) AS kontrak, " +
                "sum(pembayaran) AS pembayaran " +
                "from public.rekap_pengadaan " +
                "WHERE tahun_anggaran = '" + Year.now().toString() + "' " +
                "AND kode_kldi = '" + kldi.id + "'";
        LogUtil.debug(TAG, query);
        RekapPengadaan result = Query.find(query, RekapPengadaan.class).first();
        LogUtil.debug(TAG, result);
        LogUtil.debug(TAG, "rekap:  %s", result);
        return result;
    }

}
