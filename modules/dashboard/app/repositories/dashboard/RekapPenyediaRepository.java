package repositories.dashboard;

import models.common.contracts.Chartable;
import models.dashboard.DataPenyediaNasional;
import models.dashboard.RekapPenyedia;
import models.dashboard.enums.header.HeaderTableDataPenyediaAttribute;
import models.dashboard.pojo.PaketPenyediaResult;
import models.dashboard.pojo.RekapPenyediaResult;
import models.dashboard.queries.PaketPenyediaSearchQuery;
import models.dashboard.queries.RekapPenyediaSearchQuery;
import utils.common.ChartBuilder;

import java.util.Arrays;
import java.util.List;

public class RekapPenyediaRepository {

    public static final String TAG = "RekapPenyediaRepository";


    private static final RekapPenyediaRepository repository = new RekapPenyediaRepository();

    public static RekapPenyediaRepository getInstance() {
        return repository;
    }

    /************* NASIONAL ****************/
    public DataPenyediaNasional dataNasionalTerbaru() {
        return DataPenyediaNasional.find("1=1 order by tahun desc").first();
    }

    /************* END: NASIONAL ****************/

    public Chartable kontrakTerbanyakChart() {
        // query
        List<RekapPenyedia> data = RekapPenyedia.find("1=1 order by kontrak_menang desc").fetch();

        // build and return chart
        return ChartBuilder.buildNmostBar(
                Arrays.asList("kontrak_menang"),
                Arrays.asList("Nilai Dimenangi"),
                data, 10);
    }

    public Chartable paketTerbanyakChart() {
        // query
        List<RekapPenyedia> data = RekapPenyedia.find("1=1 order by paket_menang desc").fetch();

        // build and return chart
        return ChartBuilder.buildNmostBar(
                Arrays.asList("paket_menang"),
                Arrays.asList("Paket Menang"),
                data, 10);
    }

    public RekapPenyediaResult list(RekapPenyediaSearchQuery searchQuery) {
        String sorting = "";
        if(searchQuery.getOrder() != null && searchQuery.getAttr() != null) {
            for(HeaderTableDataPenyediaAttribute headerTableDataPenyediaAttribute:
                    HeaderTableDataPenyediaAttribute.values()) {
                if (searchQuery.getAttr().equals(headerTableDataPenyediaAttribute.toString())) {
                    sorting = " order by " + searchQuery.getAttr() + " "+searchQuery.getOrder();
                }
            }
        }
        RekapPenyediaResult result = new RekapPenyediaResult(searchQuery);
        result.executeQuery(
                "select * from rekap_penyedia" +
                        (searchQuery.isKeywordExist() ? " where nama ilike '%" + searchQuery.getKeyword() + "%'" : "") + sorting
        );
        return result;
    }

    public PaketPenyediaResult listPenyediaStatus(PaketPenyediaSearchQuery searchQuery) {
        PaketPenyediaResult result = new PaketPenyediaResult(searchQuery);
        result.executeQuery(
                "select * from paket_penyedia where rekanan_id = " + searchQuery.getRekananId() +
                        (searchQuery.isStatusExists() ? " and status = '" + searchQuery.getStatus().name() + "'" : "")
        );
        return result;
    }
}
