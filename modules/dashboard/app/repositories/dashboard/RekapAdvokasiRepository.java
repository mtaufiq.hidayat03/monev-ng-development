package repositories.dashboard;

import models.common.contracts.Chartable;
import models.dashboard.RekapAdvokasi;
import utils.common.ChartBuilder;

import java.util.Arrays;
import java.util.List;

public class RekapAdvokasiRepository {

    public static final String TAG = "RekapAdvokasiRepository";


    private static final RekapAdvokasiRepository repository = new RekapAdvokasiRepository();

    public static RekapAdvokasiRepository getInstance() {
        return repository;
    }

    public RekapAdvokasi getLatest() {
        return RekapAdvokasi.find("1=1 order by tanggal_update desc").first();
    }

    public List<RekapAdvokasi> list() {
        return RekapAdvokasi.findAll();
    }

    public Chartable chart() {
        // ambil label seriesnya
        List<String> seriesLabels = Arrays.asList("Pekerjaan Konstruksi", "Barang", "Jasa Konsultasi", "Jasa Lainnya");
        // query
        RekapAdvokasi rekapAdvokasi = getLatest();

        List<Number> data = Arrays.asList(
                rekapAdvokasi.pengaduan_konstruksi,
                rekapAdvokasi.pengaduan_barang,
                rekapAdvokasi.pengaduan_konsultasi,
                rekapAdvokasi.pengaduan_lainnya
        );
        // build and return chart
        return ChartBuilder.buildDonut("Layanan Advokasi", seriesLabels, data);
    }

}
