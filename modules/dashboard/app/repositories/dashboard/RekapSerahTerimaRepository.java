package repositories.dashboard;

import models.common.contracts.Chartable;
import models.dashboard.RekapSerahTerima;
import play.db.jdbc.Query;
import utils.common.ChartBuilder;

import java.time.Year;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RekapSerahTerimaRepository {

    public static final String TAG = "RekapSerahTerimaRepository";

    private static final Map<String, String> filterColumnName = Stream.of(new String[][]{
            {"Jenis Belanja", "jenis_belanja"},
            {"Metode Pemilihan", "metode_pemilihan"},
            {"Jenis Pengadaan", "jenis_pengadaan"}
    }).collect(Collectors.toMap(data -> data[0], data -> data[1]));

    private static final RekapSerahTerimaRepository repository = new RekapSerahTerimaRepository();

    public static RekapSerahTerimaRepository getInstance() {
        return repository;
    }

    public Chartable chartJenisBelanja(int tahun, boolean pagu) {
        String sql = "select sum(pagu) as pagu, sum(paket) as paket, jenis_belanja as label"+
                " from public.rekap_serah_terima" +
                " where tahun_anggaran = " + tahun +
                " group by jenis_belanja";
        // query
        List<RekapSerahTerima> data = Query.find(sql, RekapSerahTerima.class).fetch();

        // build and return chart
        return ChartBuilder.buildNmostBar(
                Arrays.asList(pagu ? "pagu" : "paket"),
                Arrays.asList(pagu ? "Pagu" : "Paket"),
                data, 10);
    }

    public Chartable chartMetodePemilihan(int tahun, boolean pagu) {
        String sql = "select sum(pagu) as pagu, sum(paket) as paket, metode_pemilihan as label"+
                " from public.rekap_serah_terima" +
                " where tahun_anggaran = " + tahun +
                " group by metode_pemilihan";
        // query
        List<RekapSerahTerima> data = Query.find(sql, RekapSerahTerima.class).fetch();

        // build and return chart
        return ChartBuilder.buildNmostBar(
                Arrays.asList(pagu ? "pagu" : "paket"),
                Arrays.asList(pagu ? "Pagu" : "Paket"),
                data, 10);
    }

    public List<RekapSerahTerima> listYears() {
        String sql = "select DISTINCT(tahun_anggaran)" +
                " from public.rekap_serah_terima" +
                " order by tahun_anggaran desc";
        return Query.find(sql, RekapSerahTerima.class).fetch();
    }

    public RekapSerahTerima rekapSerahTerima() {
        String sql = "select DISTINCT(tahun_anggaran)" +
                " from public.rekap_serah_terima" +
                " order by tahun_anggaran desc";
        return Query.find(sql, RekapSerahTerima.class).first();
    }

    public Chartable chartJenisPengadaan(int tahun, boolean pagu) {
        String sql = "select sum(pagu) as pagu, sum(paket) as paket, jenis_pengadaan as label"+
                " from public.rekap_serah_terima" +
                " where tahun_anggaran = " + tahun +
                " group by jenis_pengadaan";
        // query
        List<RekapSerahTerima> data = Query.find(sql, RekapSerahTerima.class).fetch();

        // build and return chart
        return ChartBuilder.buildNmostBar(
                Arrays.asList(pagu ? "pagu" : "paket"),
                Arrays.asList(pagu ? "Pagu" : "Paket"),
                data, 10);
    }


    public Chartable chartPerTahun() {
        // query
        int thisYear = Year.now().getValue();
        int tenYearAgo = thisYear - 10;
        String sql = "select sum(pagu) as pagu, tahun_anggaran as label"+
                " from public.rekap_serah_terima" +
                " where tahun_anggaran > " + tenYearAgo +
                " group by tahun_anggaran"+
                " order by tahun_anggaran";
        List<RekapSerahTerima> data = Query.find(sql, RekapSerahTerima.class).fetch();

        // build and return chart
        return ChartBuilder.buildYearSeries(
                tenYearAgo,
                thisYear,
                Arrays.asList("pagu"),
                Arrays.asList("Pagu"),
                data
        );
    }
}
