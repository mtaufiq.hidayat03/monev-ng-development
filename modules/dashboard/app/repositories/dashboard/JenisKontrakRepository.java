package repositories.dashboard;

import models.dashboard.enums.JenisKontrakDisplayAttribute;
import models.dashboard.pojo.JenisKontrakResult;
import models.dashboard.queries.JenisKontrakSearchQuery;
import org.apache.commons.lang3.StringUtils;
import play.db.jdbc.Query;
import utils.common.LogUtil;

import java.util.List;

public class JenisKontrakRepository {

    public static final String TAG = "JenisKontrakRepository";

    private static final JenisKontrakRepository repository = new JenisKontrakRepository();

    public static JenisKontrakRepository getInstance() {
        return repository;
    }

    public JenisKontrakResult list(JenisKontrakSearchQuery searchQuery,
                                   JenisKontrakDisplayAttribute jenisKontrakDisplayAttribute) {
        Integer a = jenisKontrakDisplayAttribute.values().length;
        String queryFilter ="";
        for (Integer i=0; i < a; i++) {
            Boolean attribute = jenisKontrakDisplayAttribute.getId().toString().equals(i.toString());
            if (attribute && i != 0) {
                queryFilter = " where jenis_kontrak='" + i +"'";
            }
            if (i == 0) {
                queryFilter = "";
            }
        }
        if (searchQuery.isYearExist()) {
            queryFilter += (!StringUtils.isEmpty(queryFilter) ? " AND" : " WHERE")
                    + " tahun_anggaran = '" + searchQuery.getYear() + "' ";
        }
        LogUtil.debug(TAG, queryFilter);
        JenisKontrakResult result = new JenisKontrakResult(searchQuery);
        result.executeQuery(
                "select * from jenis_kontrak" + queryFilter
        );
        return result;
    }

    public List<Integer> getYears() {
        return Query.find(
                "SELECT tahun_anggaran " +
                        "FROM jenis_kontrak " +
                        "GROUP BY tahun_anggaran " +
                        "ORDER BY tahun_anggaran DESC",
                Integer.class)
                .fetch();
    }

}
