package repositories.dashboard;

import models.common.contracts.Chartable;
import models.dashboard.KualifikasiUsaha;
import models.dashboard.enums.KualifikasiUsahaChartDisplayAttribute;
import models.dashboard.pojo.KualifikasiUsahaResult;
import models.dashboard.queries.KualifikasiUsahaSearchQuery;
import play.db.jdbc.Query;
import utils.common.ChartBuilder;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class KualifikasiUsahaRepository {

    public static final String TAG = "KualifikasiUsahaRepository";

    private static final KualifikasiUsahaRepository repository = new KualifikasiUsahaRepository();

    public static KualifikasiUsahaRepository getInstance() {
        return repository;
    }

    public List<Number> getData(List<String> seriesLabels, String type, String kualifikasi) {
        StringBuilder sb = new StringBuilder();
        if (type.equals(KualifikasiUsahaChartDisplayAttribute.PAKET.toString())) {
            sb.append("select count(*) as total, kualifikasi_usaha from public.kualifikasi_usaha ");
        } else {
            sb.append("select sum(pelaksanaan_pagu) as total, kualifikasi_usaha from public.kualifikasi_usaha ");
        }
        sb.append("where kualifikasi_usaha='" + kualifikasi + "' GROUP BY kualifikasi_usaha");

        List<Object[]> result = Query.find(sb.toString(), (ResultSet resultSet) -> {
            Object[] res = new Object[2];
            res[0] = resultSet.getString("kualifikasi_usaha");
            res[1] = resultSet.getLong("total");
            return res;
        }).fetch();

        List<Number> data = new ArrayList<>(Collections.nCopies(seriesLabels.size(), 0));
        for (Object[] obj : result) {
            int idx = seriesLabels.indexOf(obj[0].toString().replaceAll("_"," "));
            data.set(idx, (Long) obj[1]);
        }
        return data;
    }

    public Chartable kecilChart(String type) {
        List<String> seriesLabels = Arrays.asList("KECIL");
        List<Number> data = getData(seriesLabels, type, "KECIL");
        return ChartBuilder.buildDonut("Kualifikasi Usaha Kecil", seriesLabels, data);
    }

    public Chartable nonKecilChart(String type) {
        List<String> seriesLabels = Arrays.asList("NON KECIL");
        List<Number> data = getData(seriesLabels, type, "NON_KECIL");
        return ChartBuilder.buildDonut("Kualifikasi Usaha Non Kecil", seriesLabels, data);
    }

    public Chartable kecilNonKecilChart(String type) {
        List<String> seriesLabels = Arrays.asList("KECIL NON KECIL");
        List<Number> data = getData(seriesLabels, type, "KECIL_NON_KECIL");
        return ChartBuilder.buildDonut("Kualifikasi Usaha Kecil Non Kecil", seriesLabels, data);
    }

    public List<KualifikasiUsaha> getKualifikasiUsaha() {
        return KualifikasiUsaha.find("1=1 order by kualifikasi_usaha asc").fetch();
    }

    public KualifikasiUsahaResult list(KualifikasiUsahaSearchQuery searchQuery) {
        KualifikasiUsahaResult result = new KualifikasiUsahaResult(searchQuery);
        String sql = "SELECT * FROM kualifikasi_usaha WHERE 1=1 ";
        if (searchQuery.isStatusExists()) {
            sql += " AND kualifikasi_usaha = '" + searchQuery.getStatus().toString() + "'";
        }
        if (searchQuery.isKeywordExist()) {
            sql += " AND (nama_kldi ILIKE '%" + searchQuery.getKeyword() + "%' " +
                    "OR nama_satker ILIKE '%" + searchQuery.getKeyword() + "%')";
        }
        result.executeQuery(sql);
        return result;
    }
}
