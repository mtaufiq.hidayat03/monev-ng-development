package repositories.adjustment;

import controllers.user.management.BaseAdminController;
import models.adjustment.*;

public class AdjustmentRepository extends BaseAdminController {

    public static final String TAG = "AdjustmentRepository";
    
    public static final AdjustmentRepository instance = new AdjustmentRepository();

    public static AdjustmentRepository getInstance() {
        return instance;
    }

    public PemegangSertifikatKompetensi getSummaryPemegangSertifikatKompetensi() {
        return PemegangSertifikatKompetensi.find("1=1").first();
    }

    public PemegangSertifikatKompetensiOriginal getOriginalPemegangSertifikatKompetensi() {
        return PemegangSertifikatKompetensiOriginal.find("1=1").first();
    }

    public PemegangSertifikatOkupasi getSummaryPemegangSertifikatOkupasi() {
        return PemegangSertifikatOkupasi.find("1=1").first();
    }

    public PemegangSertifikatOkupasiOriginal getOriginalPemegangSertifikatOkupasi() {
        return PemegangSertifikatOkupasiOriginal.find("1=1").first();
    }

    public Advokasi getSummaryAdvokasi() {
        return Advokasi.find("1=1").first();
    }

    public AdvokasiOriginal getOriginalAdvokasi() {
        return AdvokasiOriginal.find("1=1").first();
    }

    public Ukpbj getSummaryUkpbj() {
        return Ukpbj.find("1=1").first();
    }

    public UkpbjOriginal getOriginalUkpbj() {
        return UkpbjOriginal.find("1=1").first();
    }

    public JabfungPpbj getSummaryJabfungPpbj() {
        return JabfungPpbj.find("1=1").first();
    }

    public JabfungPpbjOriginal getOriginalJabfungPpbj() {
        return JabfungPpbjOriginal.find("1=1").first();
    }

    public Lpse getSummaryLpse() {
        return Lpse.find("1=1").first();
    }

    public LpseOriginal getOriginalLpse() {
        return LpseOriginal.find("1=1").first();
    }

    public PemegangSertifikatDasar getSummaryPemegangSertifikatDasar() {
        return PemegangSertifikatDasar.find("1=1").first();
    }

    public PemegangSertifikatDasarOriginal getOriginalPemegangSertifikatDasar() {
        return PemegangSertifikatDasarOriginal.find("1=1").first();
    }

    public DataPenyedia getSummaryDataPenyedia() {
        return DataPenyedia.find("1=1").first();
    }

    public DataPenyediaOriginal getOriginalDataPenyedia() {
        return DataPenyediaOriginal.find("1=1").first();
    }

    public InovasiPengadaan getSummaryInovasiPengadaan() {
        return InovasiPengadaan.find("1=1").first();
    }

    public InovasiPengadaanOriginal getOriginalInovasiPengadaan() {
        return InovasiPengadaanOriginal.find("1=1").first();
    }

    public PdnUmkm getSummaryPdnPeranSertaUsahaKecil() {
        return PdnUmkm.find("1=1").first();
    }

    public PdnUmkmOriginal getOriginalPdnPeranSertaUsahaKecil() {
        return PdnUmkmOriginal.find("1=1").first();
    }

}
