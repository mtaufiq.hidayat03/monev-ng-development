package repositories.smartreport;

import models.common.contracts.Chartable;
import models.smartreport.NonETenderingDetail;
import models.smartreport.enums.NonETenderingDisplayAttribute;
import models.smartreport.pojo.NonETenderingDetailResult;
import models.smartreport.queries.NonETenderingDetailSearchQuery;
import utils.common.ChartBuilder;

import java.util.List;

public class NonETenderingDetailRepository {


    public static final String TAG = "NonETenderingDetailRepository";

    private static final NonETenderingDetailRepository repository = new NonETenderingDetailRepository();

    public static NonETenderingDetailRepository getInstance() {
        return repository;
    }

    public NonETenderingDetailResult list(
            NonETenderingDetailSearchQuery searchQuery,
            int tahun,
            NonETenderingDisplayAttribute filter) {
        NonETenderingDetailResult result = new NonETenderingDetailResult(searchQuery);
        StringBuilder sb = new StringBuilder();
        sb.append("select * from smartreport.non_e_tendering_detail");
        sb.append(" where tahun ='").append(tahun).append("'");
        if (searchQuery.isKeywordExist()) {
            sb.append(" and nama_lpse ILIKE ").append("'%").append(searchQuery.getKeyword()).append("%'");
        }
        sb.append(" and ").append(filter.attributes.get(0)).append(" is not null");
        sb.append(" order by ").append(filter.attributes.get(0)).append(" desc");
        result.executeQuery(sb.toString());
        return result;
    }

    public List<NonETenderingDetail> list(int tahun, NonETenderingDisplayAttribute filter) {
        if (filter == null)
            return NonETenderingDetail.find("tahun = ? order by nama_lpse asc", tahun).fetch();

        return NonETenderingDetail.find("tahun = ? " +
                        "and " + filter.attributes.get(0) + " is not null " +
                        "order by " + filter.attributes.get(0) + " desc",
                tahun
        ).fetch();
    }

    public Chartable chart(int tahun, NonETenderingDisplayAttribute displayAttribute) {
        // query
        List<NonETenderingDetail> data = list(tahun, displayAttribute);

        // build and return chart
        return ChartBuilder.buildNmostBar(
                displayAttribute.attributes,
                displayAttribute.attributeLabels,
                data,
                10
        );
    }
}
