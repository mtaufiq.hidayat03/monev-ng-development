package repositories.smartreport;

import models.common.contracts.Chartable;
import models.smartreport.EPurchasingDetail;
import models.smartreport.enums.EPurchasingDisplayAttribute;
import models.smartreport.pojo.EPurchasingDetailResult;
import models.smartreport.queries.EPurchasingDetailSearchQuery;
import play.db.jdbc.Query;
import utils.common.ChartBuilder;

import java.util.List;

public class EPurchasingDetailRepository {

    public static final String TAG = "EPurchasingDetailRepository";

    private static final EPurchasingDetailRepository repository = new EPurchasingDetailRepository();

    public static EPurchasingDetailRepository getInstance() {
        return repository;
    }

    public EPurchasingDetailResult list(
            EPurchasingDetailSearchQuery searchQuery,
            int tahun,
            EPurchasingDisplayAttribute filter) {
        EPurchasingDetailResult result = new EPurchasingDetailResult(searchQuery);
        StringBuilder sb = new StringBuilder();
        sb.append("select * from smartreport.e_purchasing_detail");
        sb.append(" where tahun ='").append(tahun).append("'");
        if (searchQuery.isKeywordExist()) {
            sb.append(" and nama_kldi ILIKE ").append("'%").append(searchQuery.getKeyword()).append("%'");
        }
        sb.append(" and ").append(filter.attributes.get(0)).append(" is not null");
        sb.append(" order by ").append(filter.attributes.get(0)).append(" desc");
        result.executeQuery(sb.toString());
        return result;
    }

    public List<EPurchasingDetail> list(int tahun, EPurchasingDisplayAttribute filter) {
        String defaultColumn = "tahun, nama_kldi";
        return Query.find(
                "SELECT " + defaultColumn +
                        ", SUM(" + filter.attributes.get(0) + ") as " + filter.attributes.get(0) +
                        " FROM smartreport.e_purchasing_detail where tahun='" + tahun + "'" +
                        " and " + filter.attributes.get(0) + " is not null " +
                        " group by " + defaultColumn
                , EPurchasingDetail.class
        ).fetch();
    }

    public Chartable chart(int tahun, EPurchasingDisplayAttribute displayAttribute) {
        // query
        List<EPurchasingDetail> data = list(tahun, displayAttribute);

        // build and return chart
        return ChartBuilder.buildNmostBar(
                displayAttribute.attributes,
                displayAttribute.attributeLabels,
                data,
                10
        );
    }
}
