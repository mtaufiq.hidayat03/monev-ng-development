package repositories.smartreport;

import models.common.contracts.Chartable;
import models.smartreport.ETenderingDetail;
import models.smartreport.enums.ETenderingDisplayAttribute;
import models.smartreport.pojo.ETenderingDetailResult;
import models.smartreport.queries.ETenderingDetailSearchQuery;
import utils.common.ChartBuilder;

import java.util.List;

public class ETenderingDetailRepository {


    public static final String TAG = "ETenderingDetailRepository";

    private static final ETenderingDetailRepository repository = new ETenderingDetailRepository();

    public static ETenderingDetailRepository getInstance() {
        return repository;
    }

    public ETenderingDetailResult list(
            ETenderingDetailSearchQuery searchQuery,
            int tahun,
            ETenderingDisplayAttribute filter) {
        ETenderingDetailResult result = new ETenderingDetailResult(searchQuery);
        StringBuilder sb = new StringBuilder();
        sb.append("select * from smartreport.e_tendering_detail");
        sb.append(" where tahun ='").append(tahun).append("'");
        if (searchQuery.isKeywordExist()) {
            sb.append(" and nama_lpse ILIKE ").append("'%").append(searchQuery.getKeyword()).append("%'");
        }
        sb.append(" and ").append(filter.attributes.get(0)).append(" is not null");
        sb.append(" order by ").append(filter.attributes.get(0)).append(" desc");
        result.executeQuery(sb.toString());
        return result;
    }

    public List<ETenderingDetail> list(int tahun, ETenderingDisplayAttribute filter) {
        if (filter == null)
            return ETenderingDetail.find("tahun = ? order by nama_lpse asc", tahun).fetch();

        return ETenderingDetail.find("tahun = ? " +
                        "and " + filter.attributes.get(0) + " is not null " +
                        "order by " + filter.attributes.get(0) + " desc",
                tahun
        ).fetch();
    }

    public Chartable chart(int tahun, ETenderingDisplayAttribute displayAttribute) {
        // query
        List<ETenderingDetail> data = list(tahun, displayAttribute);

        // build and return chart
        return ChartBuilder.buildNmostBar(
                displayAttribute.attributes,
                displayAttribute.attributeLabels,
                data,
                10
        );
    }
}
