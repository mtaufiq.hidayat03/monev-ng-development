package repositories.smartreport;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import models.common.contracts.Chartable;
import models.smartreport.EPurchasing;
import models.smartreport.EPurchasingOriginal;
import models.smartreport.enums.EPurchasingDisplayAttribute;
import utils.common.ChartBuilder;
import utils.common.DateFormatter;
import utils.common.LogUtil;

import java.time.Year;
import java.util.Date;
import java.util.List;

public class EPurchasingRepository {
    public static final String TAG = "EPurchasingRepository";

    private static final EPurchasingRepository repository = new EPurchasingRepository();

    private EPurchasingRepository(){}

    public static EPurchasingRepository getInstance() {
        return repository;
    }

    public EPurchasing get(Long id) {
        return EPurchasing.findById(id);
    }

    public List<EPurchasing> listLast10Years() {
        int tenYearAgo = Year.now().getValue() - 10;
        return EPurchasing.find("tahun > ? order by tahun asc", tenYearAgo).fetch();
    }

    public JsonObject generateJson() {
        List<EPurchasing> summaries = EPurchasing.find("1=1 order by tahun desc").fetch();
        JsonObject result = new JsonObject();
        JsonArray jsonArray = new JsonArray();

        int i = 1;
        for (EPurchasing model : summaries) {
            JsonArray array = new JsonArray();
            array.add(i);
            array.add(model.tahun);
            array.add(DateFormatter.format(model.tanggal_update, "dd-MM-yyyy HH:mm:ss"));
            array.add(model.isAdaUpdate() ? "Ada" : "Tidak");
            array.add(model.id);
            jsonArray.add(array);
            i++;
        }
        result.add("data", jsonArray);
        LogUtil.debug(TAG, result);
        return result;
    }

    public Chartable chart(EPurchasingDisplayAttribute displayAttribute) {
        // query
        List<EPurchasing> data = listLast10Years();

        // build and return chart
        int thisYear = Year.now().getValue();
        int tenYearAgo = thisYear - 9;
        return ChartBuilder.buildYearSeries(
                tenYearAgo,
                thisYear,
                displayAttribute.attributes,
                displayAttribute.attributeLabels,
                data
        );
    }

    public void createNewYearData(int thisYear) {
        EPurchasing summary = EPurchasing.find("tahun = ?", thisYear).first();
        if (summary != null) {
            return;
        }

        summary = new EPurchasing();
        summary.tahun = thisYear;
        summary.tanggal_update = new Date();
        summary.save();
    }

    /**************** START: Original ***********************/
    public EPurchasingOriginal getOriginalByYear(int tahun) {
        return EPurchasingOriginal.find("tahun = ?", tahun).first();
    }
    /**************** END: Original **********************/
}
