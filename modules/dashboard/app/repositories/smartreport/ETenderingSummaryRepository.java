package repositories.smartreport;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import models.common.contracts.Chartable;
import models.common.contracts.DataTablePagination;
import models.smartreport.ETenderingOriginalSummary;
import models.smartreport.ETenderingSummary;
import models.smartreport.EtenderingResult;
import models.smartreport.enums.ETenderingDisplayAttribute;
import utils.common.ChartBuilder;
import utils.common.DateFormatter;
import utils.common.LogUtil;

import java.time.Year;
import java.util.Date;
import java.util.List;

public class ETenderingSummaryRepository {
    public static final String TAG = "ETenderingSummaryRepository";

    private static final ETenderingSummaryRepository repository = new ETenderingSummaryRepository();

    private ETenderingSummaryRepository(){}

    public static ETenderingSummaryRepository getInstance() {
        return repository;
    }

    public ETenderingSummary get(Long id) {
        return ETenderingSummary.findById(id);
    }

    public List<ETenderingSummary> listLast10Years() {
        int tenYearAgo = Year.now().getValue() - 10;
        return ETenderingSummary.find("tahun > ? order by tahun asc", tenYearAgo).fetch();
    }

    public JsonObject generateJson() {
        List<ETenderingSummary> summaries = ETenderingSummary.find("1=1 order by tahun desc").fetch();
        JsonObject result = new JsonObject();
        JsonArray jsonArray = new JsonArray();

        int i = 1;
        for (ETenderingSummary model : summaries) {
            JsonArray array = new JsonArray();
            array.add(i);
            array.add(model.tahun);
            array.add(DateFormatter.format(model.tanggal_update, "dd-MM-yyyy HH:mm:ss"));
            array.add(model.isAdaUpdate() ? "Ada" : "Tidak");
            array.add(model.id);
            jsonArray.add(array);
            i++;
        }
        result.add("data", jsonArray);
        LogUtil.debug(TAG, result);
        return result;
    }

    public EtenderingResult getEtenderingResult(DataTablePagination.Request request) {
        EtenderingResult etenderingResult = new EtenderingResult(request);
        etenderingResult.executeQuery();
        return etenderingResult;
    }

    public Chartable chart(ETenderingDisplayAttribute displayAttribute) {
        // query
        List<ETenderingSummary> data = listLast10Years();

        // build and return chart
        int thisYear = Year.now().getValue();
        int tenYearAgo = thisYear - 9;
        return ChartBuilder.buildYearSeries(
                tenYearAgo,
                thisYear,
                displayAttribute.attributes,
                displayAttribute.attributeLabels,
                data
        );
    }

    public void createNewYearData(int thisYear) {
        ETenderingSummary summary = ETenderingSummary.find("tahun = ?", thisYear).first();
        if (summary != null) {
            return;
        }

        summary = new ETenderingSummary();
        summary.tahun = thisYear;
        summary.tanggal_update = new Date();
        summary.save();
    }

    /**************** START: Original ***********************/
    public ETenderingOriginalSummary getOriginalByYear(int tahun) {
        return ETenderingOriginalSummary.find("tahun = ?", tahun).first();
    }
    /**************** END: Original **********************/
}
