package repositories.smartreport;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import models.common.contracts.Chartable;
import models.smartreport.NonETenderingOriginalSummary;
import models.smartreport.NonETenderingSummary;
import models.smartreport.enums.NonETenderingDisplayAttribute;
import utils.common.ChartBuilder;
import utils.common.DateFormatter;
import utils.common.LogUtil;

import java.time.Year;
import java.util.List;

public class NonETenderingSummaryRepository {
    public static final String TAG = "NonETenderingSummaryRepository";

    private static final NonETenderingSummaryRepository repository = new NonETenderingSummaryRepository();

    private NonETenderingSummaryRepository(){}

    public static NonETenderingSummaryRepository getInstance() {
        return repository;
    }

    public NonETenderingSummary get(Long id) {
        return NonETenderingSummary.findById(id);
    }

    public List<NonETenderingSummary> listLast10Years() {
        int tenYearAgo = Year.now().getValue() - 10;
        return NonETenderingSummary.find("tahun > ? order by tahun asc", tenYearAgo).fetch();
    }

    public JsonObject generateJson() {
        List<NonETenderingSummary> summaries = NonETenderingSummary.find("1=1 order by tahun desc").fetch();
        JsonObject result = new JsonObject();
        JsonArray jsonArray = new JsonArray();

        int i = 1;
        for (NonETenderingSummary model : summaries) {
            JsonArray array = new JsonArray();
            array.add(i);
            array.add(model.tahun);
            array.add(DateFormatter.format(model.tanggal_update, "dd-MM-yyyy HH:mm:ss"));
            array.add(model.isAdaUpdate() ? "Ada" : "Tidak");
            array.add(model.id);
            jsonArray.add(array);
            i++;
        }
        result.add("data", jsonArray);
        LogUtil.debug(TAG, result);
        return result;
    }

    public Chartable chart(NonETenderingDisplayAttribute displayAttribute) {
        // query
        List<NonETenderingSummary> data = listLast10Years();

        // build and return chart
        int thisYear = Year.now().getValue();
        int tenYearAgo = thisYear - 9;
        return ChartBuilder.buildYearSeries(
                tenYearAgo,
                thisYear,
                displayAttribute.attributes,
                displayAttribute.attributeLabels,
                data
        );
    }

    /**************** START: Original ***********************/
    public NonETenderingOriginalSummary getOriginalByYear(int tahun) {
        return NonETenderingOriginalSummary.find("tahun = ?", tahun).first();
    }
    /**************** END: Original **********************/
}
