package models.dashboard;

import play.data.binding.As;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import utils.common.ExtractableChartQueryResult;
import utils.common.IndonesianNumberBinder;

import javax.persistence.EnumType;

@Table(name = "kualifikasi_usaha", schema = "public")
public class KualifikasiUsaha extends BaseTable implements ExtractableChartQueryResult {

    public static final String TAG = "KualifikasiUsaha";

    @Enumerated(EnumType.STRING)
    public enum KUALIFIKASI_USAHA {
        KECIL,
        NON_KECIL,
        KECIL_NON_KECIL,
        BELUM_DITENTUKAN
    }
    @Id
    public Long id;
    public Integer tahun;
    public String kode_kldi;
    public String nama_kldi;
    public String kode_satker;
    public String nama_satker;
    @As(binder = IndonesianNumberBinder.class)
    public Long total_anggaran;
    @As(binder = IndonesianNumberBinder.class)
    public Long belanja_pengadaan;
    @As(binder = IndonesianNumberBinder.class)
    public Long pelaksanaan_pagu;
    @As(binder = IndonesianNumberBinder.class)
    public Long pelaksanaan_paket;
    @As(binder = IndonesianNumberBinder.class)
    public Long hasil_paket;
    @As(binder = IndonesianNumberBinder.class)
    public Long hasil_pagu;
    public KUALIFIKASI_USAHA kualifikasi_usaha;

    public static KualifikasiUsaha create(int tahun) {
        KualifikasiUsaha rekap = new KualifikasiUsaha();
        rekap.tahun = tahun;
        return rekap;
    }

    @Override
    public String getXAxisLabel() {
        return String.valueOf(tahun);
    }

    public String getPlainKualifikasiUsaha() {
        if (kualifikasi_usaha == null) {
            return "";
        }
        return kualifikasi_usaha.toString().replaceAll("_"," ");
    }
}
