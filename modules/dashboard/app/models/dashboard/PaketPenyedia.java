package models.dashboard;

import models.cms.ContentStatus;
import models.common.ReadOnlyModel;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Table;

import javax.persistence.EnumType;
import javax.persistence.Transient;

/**
 * @author idoej
 */
@Table(name = "paket_penyedia", schema = "public")
public class PaketPenyedia extends ReadOnlyModel {

    public static final String TAG = "PaketPenyedia";

    @Enumerated(EnumType.STRING)
    public enum STATUS_IKUT_SERTA {
        DAFTAR,
        PRAKUALIFIKASI,
        MENAWAR,
        MENANG
    }

    @Enumerated(EnumType.STRING)
    public enum KUALIFIKASI_PENYEDIA {
        LOKAL,
        NONLOKAL
    }

    public Long rekanan_id;
    public Long id_paket;
    public Long id_lelang;
    public String nama_paket;
    public Long pagu;
    public STATUS_IKUT_SERTA status;
    @Transient
    public KUALIFIKASI_PENYEDIA kualifikasi_penyedia;

}
