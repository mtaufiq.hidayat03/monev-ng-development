package models.dashboard;

import models.common.ReadOnlyModel;
import play.db.jdbc.Table;
import utils.common.ExtractableChartQueryResult;

import javax.persistence.Transient;

/**
 * @author idoej
 */
@Table(name = "rekap_serah_terima", schema = "public")
public class RekapSerahTerima extends ReadOnlyModel implements ExtractableChartQueryResult {

    public static final String TAG = "RekapSerahTerima";

    public Integer tahun_anggaran;
    public String jenis_belanja;
    public String jenis_pengadaan;
    public String metode_pemilihan;
    public String sumber_dana;
    public String tipe_kldi;
    public String kode_kldi;
    public String nama_kldi;
    public String kode_satker;
    public String nama_satker;
    public long pagu;
    public int paket;

    @Transient
    public String label;

    @Transient
    public String kode;

    public RekapSerahTerima() {}

    @Override
    public String getXAxisLabel() {
        return label;
    }
}
