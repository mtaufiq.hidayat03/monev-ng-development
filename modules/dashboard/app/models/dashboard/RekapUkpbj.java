package models.dashboard;

import models.common.ReadOnlyModel;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Table;
import utils.common.AttributeExtractable;

import javax.persistence.EnumType;
import java.util.ArrayList;
import java.util.List;

/**
 * @author idoej
 */
@Table(name = "rekap_ukpbj", schema = "public")
public class RekapUkpbj extends ReadOnlyModel implements AttributeExtractable {

    public static final String TAG = "RekapUkpbj";

    public enum MATRIX_KEMATANGAN {
        MANAJEMEN_PENGADAAN("Manajemen Pengadaan", "manajemen_pengadaan"),
        MANAJEMEN_PENYEDIA("Manajemen Penyedia", "manajemen_penyedia"),
        MANAJEMEN_KINERJA("Manajemen Kinerja", "manajemen_kinerja"),
        MANAJEMEN_RESIKO("Manajemen Resiko", "manajemen_resiko"),
        PENGORGANISASIAN("Pengorganisasian", "organisasi"),
        TUGAS_FUNGSI("Tugas & Fungsi", "tugas_fungsi"),
        PERENCANAAN_SDM("Perencanaan SDM Pengadaan", "perencanaan_sdm_pengadaan"),
        PENGEMBANGAN_SDM("Pengembangan SDM Pengadaan", "pengembangan_sdm_pengadaan"),
        SISTEM_INFORMASI("Sistem Informasi", "sistem_informasi");

        public final String label;
        public final String attribute;
        MATRIX_KEMATANGAN(String label, String attribute) {
            this.label = label;
            this.attribute = attribute;
        }

        public static MATRIX_KEMATANGAN fromAttribute(String attribute) {
            for (MATRIX_KEMATANGAN matrix: MATRIX_KEMATANGAN.values()) {
                if (matrix.attribute.equals(attribute)) return matrix;
            }
            return null;
        }
    }

    @Enumerated(EnumType.STRING)
    public enum TINGKAT_KEMATANGAN {
        INISIASI,
        ESENSI,
        PROAKTIF
    }

    @Enumerated(EnumType.STRING)
    public enum KELEMBAGAAN {
        STRUKTURAL("Struktural"),
        ADHOC("Adhoc"),
        BELUM_TERBENTUK("Belum Terbentuk");

        public final String label;
        KELEMBAGAAN(String label) {
            this.label = label;
        }
    }

    public String nama;
    public TINGKAT_KEMATANGAN manajemen_pengadaan;
    public TINGKAT_KEMATANGAN manajemen_penyedia;
    public TINGKAT_KEMATANGAN manajemen_kinerja;
    public TINGKAT_KEMATANGAN manajemen_resiko;
    public TINGKAT_KEMATANGAN organisasi;
    public TINGKAT_KEMATANGAN tugas_fungsi;
    public TINGKAT_KEMATANGAN perencanaan_sdm_pengadaan;
    public TINGKAT_KEMATANGAN pengembangan_sdm_pengadaan;
    public TINGKAT_KEMATANGAN sistem_informasi;
    public KELEMBAGAAN kelembagaan;
    public String kode_kldi;
    public String nama_kldi;
    public int kematangan;

    public List<MATRIX_KEMATANGAN> listProaktif() {
        List<MATRIX_KEMATANGAN> items = new ArrayList<>();
        for (MATRIX_KEMATANGAN matrix: MATRIX_KEMATANGAN.values()) {
            TINGKAT_KEMATANGAN kematangan = get(matrix.attribute, TINGKAT_KEMATANGAN.class);
            if (kematangan != null && kematangan.equals(TINGKAT_KEMATANGAN.PROAKTIF)) {
                items.add(matrix);
            }
        }
        return items;
    }

    public List<MATRIX_KEMATANGAN> listEsensi() {
        List<MATRIX_KEMATANGAN> items = new ArrayList<>();
        for (MATRIX_KEMATANGAN matrix: MATRIX_KEMATANGAN.values()) {
            TINGKAT_KEMATANGAN kematangan = get(matrix.attribute, TINGKAT_KEMATANGAN.class);
            if (kematangan != null && kematangan.equals(TINGKAT_KEMATANGAN.ESENSI)) {
                items.add(matrix);
            }
        }
        return items;
    }

    public List<MATRIX_KEMATANGAN> listInisiasi() {
        List<MATRIX_KEMATANGAN> items = new ArrayList<>();
        for (MATRIX_KEMATANGAN matrix: MATRIX_KEMATANGAN.values()) {
            TINGKAT_KEMATANGAN kematangan = get(matrix.attribute, TINGKAT_KEMATANGAN.class);
            if (kematangan != null && kematangan.equals(TINGKAT_KEMATANGAN.INISIASI)) {
                items.add(matrix);
            }
        }
        return items;
    }
}
