package models.dashboard.enums;

import java.util.Arrays;
import java.util.List;

public enum DataPenyediaDisplayAttribute {
    NILAI_MENANG_TERBANYAK(
            Arrays.asList("NILAI MENANG TERBANYAK")
    ),
    PAKET_MENANG_TERBANYAK(
            Arrays.asList("PAKET MENANG TERBANYAK")
    );

    public List<String> attributes;

    DataPenyediaDisplayAttribute(List<String> attributes) {
        this.attributes = attributes;
    }
}
