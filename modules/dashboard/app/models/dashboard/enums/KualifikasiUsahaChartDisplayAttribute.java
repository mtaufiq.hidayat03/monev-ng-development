package models.dashboard.enums;

import java.util.Arrays;
import java.util.List;

public enum KualifikasiUsahaChartDisplayAttribute {

    PAGU(
            Arrays.asList("PAGU"),
            true
    ),
    PAKET(
            Arrays.asList("PAKET"),
            false
    );

    public List<String> attributes;
    public boolean money;

    KualifikasiUsahaChartDisplayAttribute(List<String> attributes, boolean money) {
        this.attributes = attributes;
        this.money = money;
    }
    public Boolean isMoney() {
        return money;
    }
}
