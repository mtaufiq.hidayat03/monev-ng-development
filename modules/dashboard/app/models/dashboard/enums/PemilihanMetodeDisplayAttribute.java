package models.dashboard.enums;

import java.util.Arrays;
import java.util.List;

public enum PemilihanMetodeDisplayAttribute {
    KONSOLIDASI(
            Arrays.asList("KONSOLIDASI")
    ),
    ITEMIZE(
            Arrays.asList("ITEMIZE")
    ),
    TENDER_CEPAT(
            Arrays.asList("TENDER_CEPAT")
    ),
    REVERSE_AUCTION(
            Arrays.asList("REVERSE_AUCTION")
    );

    public List<String> attributes;

    PemilihanMetodeDisplayAttribute(List<String> attributes) {
        this.attributes = attributes;
    }
}
