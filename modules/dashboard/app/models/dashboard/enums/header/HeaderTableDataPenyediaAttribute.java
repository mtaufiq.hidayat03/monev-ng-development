package models.dashboard.enums.header;

public enum HeaderTableDataPenyediaAttribute {
    nama("Nama"),
    paket_daftar("Mendaftar"),
    paket_prakualifikasi("Lolos Prakualifikasi"),
    paket_menawar("Mengirim Penawaran"),
    paket_menang("Paket Menang"),
    kontrak_menang("Nilai Menang");

    public final String label;

    HeaderTableDataPenyediaAttribute(String label) {
        this.label = label;
    }
    public String getLabel(){
        return this.label;
    }
}
