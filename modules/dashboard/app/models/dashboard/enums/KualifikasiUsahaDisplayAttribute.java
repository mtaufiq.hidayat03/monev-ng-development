package models.dashboard.enums;

import java.util.Arrays;
import java.util.List;

public enum KualifikasiUsahaDisplayAttribute {
    SEMUA(
            Arrays.asList("SEMUA")
    ),
    KECIL(
            Arrays.asList("KECIL")
    ),
    NON_KECIL(
            Arrays.asList("NON_KECIL")
    ),
    KECIL_NON_KECIL(
            Arrays.asList("KECIL_NON_KECIL")
    ),
    BELUM_DITENTUKAN(
            Arrays.asList("BELUM_DITENTUKAN")
    );

    public List<String> attributes;

    KualifikasiUsahaDisplayAttribute(List<String> attributes) {
        this.attributes = attributes;
    }

}
