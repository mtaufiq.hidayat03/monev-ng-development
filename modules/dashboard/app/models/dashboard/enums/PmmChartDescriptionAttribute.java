package models.dashboard.enums;

import play.db.jdbc.Enumerated;

import javax.persistence.EnumType;

@Enumerated(EnumType.ORDINAL)
public enum PmmChartDescriptionAttribute {
    jumlah(0, "Grafik yang menampilkan persentase jumlah pengadaan yang dapat diselesaikan tepat pada " +
            "waktunya yang didapatkan dari hasil bagi antara jumlah pengadaan tepat waktu dibagi jumlah seluruh " +
            "pengadaan pada tahun bersangkutan (Years Based) dikali 100%  "),
    nilai(1, "Grafik yang menampilkan persentase nilai pengadaan yang dapat diselesaikan tepat pada " +
            "waktunya yang didapatkan dari hasil bagi antara total nilai pengadaan tepat waktu dibagi total nilai seluruh " +
            "pengadaan pada tahun bersangkutan (Years Based) dikali 100%  "),
    efisiensi(2, "Grafik yang menampilkan persentase efisiensi anggaran yang didapatkan dari " +
            "Nilai Pagu Pemilihan dikurangi Nilai Hasil Pemilihan dibagi Nilai Pagu Pemilihan pada tahun bersangkutan " +
            "(Years Based) dikali 100% "),
    partisipasi_penyedia(3, "Grafik yang menampilkan persentase partisipasi peserta yang didapatkan dari " +
            "hasil bagi antara Jumlah Peserta (Perusahaan) Yang Mengikuti Proses Pengadaan dibagi Jumlah Seluruh Peserta Aktif (Perusahaan) " +
            "pada tahun bersangkutan (Years Based) dikali 100% "),
    tender_dini(4, "Grafik yang menampilkan persentase nilai tender dini yang didapatkan dari hasil bagi antara " +
            "jumlah nilai tender dini dibagi dengan total nilai semua pengadaan pada tahun bersangkutan (Years Based) " +
            "dikali 100% "),
    kontrak_selesai_tepat_waktu(5, "Grafik yang menampilkan persentase kontrak yang selesai tepat pada waktunya " +
            "yang didapatkan dari hasil bagi antara total kontrak yang selesai tepat waktu dibagi dengan total semua kontrak pada " +
            "tahun bersangkutan (Years Based) dikali 100% ");

    public final Integer id;
    public final String description;

    PmmChartDescriptionAttribute(Integer id, String description) {
        this.id = id;
        this.description = description;
    }

    public static PmmChartDescriptionAttribute findById(Integer id) {
        PmmChartDescriptionAttribute jenis = null;
        if (id == null)
            return null;
        switch (id.intValue()) {
            case 0:
                jenis = jumlah;
                break;
            case 1:
                jenis = nilai;
                break;
            case 2:
                jenis = efisiensi;
                break;
            case 3:
                jenis = partisipasi_penyedia;
                break;
            case 4:
                jenis = tender_dini;
                break;
            case 5:
                jenis = kontrak_selesai_tepat_waktu;
                break;
            default:
                jenis = jumlah;
                break;
        }
        return jenis;
    }

    public Integer getId() {
        return this.id;
    }

    public String getDescription() {
        return this.description;
    }
}
