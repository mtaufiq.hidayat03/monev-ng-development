package models.dashboard.enums;

import play.db.jdbc.Enumerated;

import javax.persistence.EnumType;

@Enumerated(EnumType.ORDINAL)
public enum JenisPengadaanDisplayAttribute {
    PENGADAAN_BARANG(0,"PENGADAAN BARANG"),
    JASA_KONSULTASI_BADAN_USAHA(1, "JASA KONSULTASI BADAN USAHA"),
    PEKERJAAN_KONSTRUKSI(2, "PEKERJAAN KONSTRUKSI"),
    JASA_LAINNYA(3, "JASA LAINNYA"),
    JASA_KONSULTASI_PERSEORANGAN(4, "JASA KONSULTASI PERSEORANGAN"),
    TERINTEGRASI(5, "TERINTEGRASI");

    public final Integer id;
    public final String label;

    JenisPengadaanDisplayAttribute(Integer id, String label) {
        this.id = id;
        this.label = label;
    }

    public static JenisPengadaanDisplayAttribute findById(Integer id)
    {
        JenisPengadaanDisplayAttribute  jenis = null;
        if(id == null)
            return null;
        switch (id.intValue()) {
            case 0:
                jenis = PENGADAAN_BARANG;
                break;
            case 1:
                jenis = JASA_KONSULTASI_BADAN_USAHA;
                break;
            case 2:
                jenis = PEKERJAAN_KONSTRUKSI;
                break;
            case 3:
                jenis = JASA_LAINNYA;
                break;
            case 4:
                jenis = JASA_KONSULTASI_PERSEORANGAN;
                break;
            case 5:
                jenis = TERINTEGRASI;
                break;
            default:
                jenis = PENGADAAN_BARANG;
                break;
        }
        return jenis;
    }

    public Integer getId () {
        return this.id;
    }
    public String getLabel(){
        return this.label;
    }
}
