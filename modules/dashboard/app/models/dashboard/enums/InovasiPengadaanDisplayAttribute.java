package models.dashboard.enums;

import java.util.Arrays;
import java.util.List;

public enum InovasiPengadaanDisplayAttribute {
    PAGU(
            Arrays.asList("PAGU")
    ),
    PAKET(
            Arrays.asList("PAKET")
    );

    public List<String> attributes;

    InovasiPengadaanDisplayAttribute(List<String> attributes) {
        this.attributes = attributes;
    }
}
