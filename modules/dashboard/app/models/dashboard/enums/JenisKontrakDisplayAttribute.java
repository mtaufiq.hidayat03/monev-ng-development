package models.dashboard.enums;

import play.db.jdbc.Enumerated;
import javax.persistence.EnumType;

@Enumerated(EnumType.ORDINAL)
public enum JenisKontrakDisplayAttribute {
    SEMUA(0,"SEMUA"),
    LUMSUM(1, "LUMSUM"),
    HARGA_SATUAN(2, "HARGA SATUAN"),
    LUMSUM_HARGA_SATUAN(3, "LUMSUM HARGA SATUAN "),
    TERIMA_JADI(4, "TERIMA JADI (TURN KEY)"),
    PERSENTASE(5, "PERSENTASE"),
    TAHUN_TUNGGAL(6, "TAHUN TUNGGAL"),
    TAHUN_JAMAK(7, "TAHUN JAMAK"),
    PENGADAAN_TUNGGAL(8, "PENGADAAN TUNGGAL"),
    PENGADAAN_BERSAMA(9, "PENGADAAN BERSAMA"),
    KONTRAK_PAYUNG(10, "KONTRAK PAYUNG "),
    PEKERJAAN_TUNGGAL(11, "PENGADAAN PEKERJAAN TUNGGAL"),
    PEKERJAAN_TERINTEGRASI(12, "PENGADAAN PEKERJAAN TERINTEGRASI"),
    WAKTU_PENUGASAN(13, "WAKTU PENUGASAN"),
    BELUM_DITENTUKAN(14, "BELUM DITENTUKAN");

    public final Integer id;
    public final String label;

    JenisKontrakDisplayAttribute(Integer id, String label){
        this.id = id;
        this.label = label;
    }

    public static JenisKontrakDisplayAttribute findById(Integer id)
    {
        JenisKontrakDisplayAttribute  jenis = null;
        if(id == null)
            return null;
        switch (id.intValue()) {
            case 0:
                jenis = SEMUA;
                break;
            case 1:
                jenis = LUMSUM;
                break;
            case 2:
                jenis = HARGA_SATUAN;
                break;
            case 3:
                jenis = LUMSUM_HARGA_SATUAN;
                break;
            case 4:
                jenis = TERIMA_JADI;
                break;
            case 5:
                jenis = PERSENTASE;
                break;
            case 6:
                jenis = TAHUN_TUNGGAL;
                break;
            case 7:
                jenis = TAHUN_JAMAK;
                break;
            case 8:
                jenis = PENGADAAN_TUNGGAL;
                break;
            case 9:
                jenis = PENGADAAN_BERSAMA;
                break;
            case 10:
                jenis = KONTRAK_PAYUNG;
                break;
            case 11:
                jenis = PEKERJAAN_TUNGGAL;
                break;
            case 12:
                jenis = PEKERJAAN_TERINTEGRASI;
                break;
            case 13:
                jenis = WAKTU_PENUGASAN;
                break;
            case 14:
                jenis = BELUM_DITENTUKAN;
                break;
            default:
                jenis = SEMUA;
                break;
        }
        return jenis;
    }

    public Integer getId () {
        return this.id;
    }
    public String getLabel(){
        return this.label;
    }
}
