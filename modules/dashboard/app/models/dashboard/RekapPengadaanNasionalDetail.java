package models.dashboard;

import models.cms.profilekl.ProfileKlDataUtilContract;
import models.common.ReadOnlyModel;
import play.data.binding.As;
import play.db.jdbc.Table;
import utils.common.IndonesianNumberBinder;

import javax.persistence.Transient;

@Table(name = "rekap_pengadaan_nasional_detail", schema = "public")
public class RekapPengadaanNasionalDetail extends ReadOnlyModel implements ProfileKlDataUtilContract {

    public static final String TAG = "RekapPengadaanNasionalDetail";

    @As(binder = IndonesianNumberBinder.class)
    public long total_anggaran;
    @As(binder = IndonesianNumberBinder.class)
    public long belanja_pengadaan;
    @As(binder = IndonesianNumberBinder.class)
    public int rencana_paket;
    @As(binder = IndonesianNumberBinder.class)
    public long rencana_pagu;
    @As(binder = IndonesianNumberBinder.class)
    public int pelaksanaan_paket;
    @As(binder = IndonesianNumberBinder.class)
    public long pelaksanaan_pagu;
    @As(binder = IndonesianNumberBinder.class)
    public int hasil_paket;
    @As(binder = IndonesianNumberBinder.class)
    public long hasil_pagu;
    @As(binder = IndonesianNumberBinder.class)
    public long kontrak;
    @As(binder = IndonesianNumberBinder.class)
    public long pembayaran;
    public String kode_kldi;
    public String nama_kldi;
    public String jenis_kldi;
    public String jenis_pemerintahan;
    public Integer tahun_anggaran;

    @Transient
    public String label;

    @Transient
    public String kode;

    public Double getPersenPerencanaan() {
        return 100.0 * rencana_pagu / belanja_pengadaan;
    }

    public Double getPersenPelaksanaan() {
        return 100.0 * pelaksanaan_pagu / rencana_pagu;
    }

    public Double getPersenHasil() {
        return 100.0 * hasil_pagu / pelaksanaan_pagu;
    }

    public Double getPersenKontrak() {
        return 100.0 * kontrak / hasil_pagu;
    }

    public Double getPersenPembayaran() {
        return 100.0 * pembayaran / kontrak;
    }

    public RekapPengadaanNasionalDetail() {
    }

}
