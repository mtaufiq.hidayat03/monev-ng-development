package models.dashboard;

import models.smartreport.BaseAdjustable;
import play.db.jdbc.Table;

/**
 * @author idoej
 */
@Table(name = "rekap_advokasi", schema = "public")
public class RekapAdvokasi extends BaseAdjustable {

    public static final String TAG = "RekapAdvokasi";

    public int jumlah_advokasi;
    public long nilai_advokasi;
    public int jumlah_kasus_pengadaan;
    public long nilai_kasus_pengadaan;
    public int jumlah_kasus_ahli;
    public long nilai_kasus_ahli;
    public int jumlah_permasalahan_kontrak;
    public long nilai_permasalahan_kontrak;
    public double pengaduan_konstruksi;
    public double pengaduan_barang;
    public double pengaduan_konsultasi;
    public double pengaduan_lainnya;
}
