package models.dashboard;

import models.common.ReadOnlyModel;
import play.db.jdbc.Table;
import utils.common.DateFormatter;
import utils.common.ExtractableChartQueryResult;

import java.util.Date;

/**
 * @author idoej
 */
@Table(name = "rekap_perencanaan_bulan", schema = "public")
public class RekapPerencanaanBulan extends ReadOnlyModel implements ExtractableChartQueryResult {

    public static final String TAG = "RekapPerencanaanBulan";

    public Date bulan;
    public long rencana_pagu;


    @Override
    public String getXAxisLabel() {
        return DateFormatter.format(bulan, "MMMM yyyy");
    }
}
