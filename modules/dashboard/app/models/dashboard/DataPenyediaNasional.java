package models.dashboard;

import models.smartreport.BaseAdjustable;
import play.db.jdbc.Table;

/**
 * @author idoej
 */
@Table(name = "data_penyedia_nasional", schema = "public")
public class DataPenyediaNasional extends BaseAdjustable {

    public static final String TAG = "DataPenyediaNasional";

    public Integer terverifikasi;
    public Integer teragregasi;
    public Integer terdaftar_sikap;
    public Integer aktif;
    public Integer lolos_pra;
    public Integer menang;
}
