package models.dashboard;

import models.smartreport.BaseAdjustable;
import play.data.binding.As;
import play.db.jdbc.Table;
import umkm.models.RekapMetode;
import utils.common.ExtractableChartQueryResult;
import utils.common.IndonesianNumberBinder;

import javax.persistence.Transient;

/**
 * @author idoej
 */
@Table(name = "rekap_pemilihan_metode", schema = "public")
public class RekapPemilihanMetode extends BaseAdjustable implements ExtractableChartQueryResult, RekapMetode {

    public static final String TAG = "RekapPemilihanMetode";

    @As(binder = IndonesianNumberBinder.class)
    public long pagu_konsolidasi;
    @As(binder = IndonesianNumberBinder.class)
    public int paket_konsolidasi;
    @As(binder = IndonesianNumberBinder.class)
    public long pagu_umkm;
    @As(binder = IndonesianNumberBinder.class)
    public int paket_umkm;
    @As(binder = IndonesianNumberBinder.class)
    public long pagu_pdn;
    @As(binder = IndonesianNumberBinder.class)
    public int paket_pdn;
    @As(binder = IndonesianNumberBinder.class)
    public long pagu_itemize;
    @As(binder = IndonesianNumberBinder.class)
    public int paket_itemize;
    @As(binder = IndonesianNumberBinder.class)
    public long pagu_ulang;
    @As(binder = IndonesianNumberBinder.class)
    public int paket_ulang;
    @As(binder = IndonesianNumberBinder.class)
    public long pagu_gagal;
    @As(binder = IndonesianNumberBinder.class)
    public int paket_gagal;
    @As(binder = IndonesianNumberBinder.class)
    public long pagu_lelang_cepat;
    @As(binder = IndonesianNumberBinder.class)
    public int paket_lelang_cepat;
    @As(binder = IndonesianNumberBinder.class)
    public long pagu_reverse_auction;
    @As(binder = IndonesianNumberBinder.class)
    public int paket_reverse_auction;
    @Transient
    public int paket_total = 0;
    @Transient
    public long pagu_total = 0;

    public static RekapPemilihanMetode create(int tahun) {
        RekapPemilihanMetode rekap = new RekapPemilihanMetode();
        rekap.tahun = tahun;
        return rekap;
    }

    @Override
    public String getXAxisLabel() {
        return String.valueOf(tahun);
    }

    @Override
    public Integer getYear() {
        return tahun;
    }

    @Override
    public Long getTotalPagu() {
        return pagu_total;
    }

    @Override
    public void setTotalPagu(Long value) {
        this.pagu_total = value;
    }

    @Override
    public Integer getTotalPaket() {
        return paket_total;
    }

    @Override
    public void setTotalPaket(Integer value) {
        this.paket_total = value;
    }

    @Override
    public long getUmkmPagu() {
        return pagu_umkm;
    }

    @Override
    public long getPdnPagu() {
        return pagu_pdn;
    }

    @Override
    public long getPdnPaket() {
        return paket_pdn;
    }

    @Override
    public long getUmkmPaket() {
        return paket_umkm;
    }
}
