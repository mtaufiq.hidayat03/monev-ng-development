package models.dashboard;

import models.smartreport.BaseAdjustable;
import play.data.binding.As;
import play.db.jdbc.Table;
import utils.common.ExtractableChartQueryResult;
import utils.common.IndonesianNumberBinder;

/**
 * @author idoej
 */
@Table(name = "inovasi_pengadaan", schema = "public")
public class InovasiPengadaan extends BaseAdjustable implements ExtractableChartQueryResult {

    public static final String TAG = "InovasiPengadaan";

    @As(binder = IndonesianNumberBinder.class)
    public long pagu_dini;
    @As(binder = IndonesianNumberBinder.class)
    public int paket_dini;
    @As(binder = IndonesianNumberBinder.class)
    public long pagu_konsolidasi;
    @As(binder = IndonesianNumberBinder.class)
    public int paket_konsolidasi;
    @As(binder = IndonesianNumberBinder.class)
    public long pagu_itemize;
    @As(binder = IndonesianNumberBinder.class)
    public int paket_itemize;
    @As(binder = IndonesianNumberBinder.class)
    public long pagu_lelang_cepat;
    @As(binder = IndonesianNumberBinder.class)
    public int paket_lelang_cepat;
    @As(binder = IndonesianNumberBinder.class)
    public long pagu_reverse_auction;
    @As(binder = IndonesianNumberBinder.class)
    public int paket_reverse_auction;

    public static InovasiPengadaan create(int tahun) {
        InovasiPengadaan rekap = new InovasiPengadaan();
        rekap.tahun = tahun;
        return rekap;
    }

    @Override
    public String getXAxisLabel() {
        return String.valueOf(tahun);
    }
}
