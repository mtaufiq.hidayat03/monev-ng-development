package models.dashboard.queries;

import models.common.contracts.TablePagination;
import org.apache.commons.lang3.StringUtils;
import play.mvc.Scope;
import utils.common.QueryUtil;

import java.util.ArrayList;
import java.util.List;

public class RekapPenyediaSearchQuery implements TablePagination.ParamAble {

    public static final String TAG = "RekapPenyediaSearchQuery";

    private final String keyword;
    private final String column;
    private final String order;
    private final int page;
    private final TablePagination.Param[] params;

    public RekapPenyediaSearchQuery(Scope.Params params) {
        this.keyword = params.get("search") != null ? params.get("search") : "";
        this.column = params.get("attr") != null ? params.get("attr") : "";
        this.order = params.get("order") != null ? params.get("order") : "";
        this.page = getCurrentPage(params);
        this.params = generateParams();
    }

    private TablePagination.Param[] generateParams() {
        List<TablePagination.Param> results = new ArrayList<>();
        if (isKeywordExist()) {
            results.add(new TablePagination.Param("search", keyword));
        }
        if (isAttrExists()) {
            results.add(new TablePagination.Param("attr", column));
        }
        if (isOrderExists()) {
            results.add(new TablePagination.Param("order", order));
        }
        TablePagination.Param[] params = new TablePagination.Param[results.size()];
        return results.toArray(params);
    }

    @Override
    public TablePagination.Param[] getParams() {
        return params;
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public String getKeyword() {
        return QueryUtil.sanitize(keyword);
    }

    public boolean isAttrExists() {
        return StringUtils.isNotEmpty(getAttr());
    }

    public boolean isOrderExists() {
        return StringUtils.isNotEmpty(getOrder());
    }

    public String getAttr() {
        return column;
    }

    public String getOrder() {
        return order;
    }

}
