package models.dashboard.queries;

import models.common.contracts.TablePagination;
import models.dashboard.RekapUkpbj;
import play.mvc.Scope;
import utils.common.QueryUtil;

import java.util.ArrayList;
import java.util.List;

public class RekapUkpbjSearchQuery implements TablePagination.ParamAble {

    public static final String TAG = "RekapUkpbjSearchQuery";

    private final String keyword;
    private final RekapUkpbj.KELEMBAGAAN kelembagaan;
    private final Integer kematangan;
    private final int page;
    private final TablePagination.Param[] params;

    public RekapUkpbjSearchQuery(Scope.Params params) {
        this.keyword = params.get("search") != null ? params.get("search") : "";
        this.page = getCurrentPage(params);
        this.kelembagaan = !isNull(params, "kelembagaan") ? params.get("kelembagaan", RekapUkpbj.KELEMBAGAAN.class) : null;
        this.kematangan = !isNull(params, "kematangan") ? params.get("kematangan", Integer.class) : null;
        this.params = generateParams();
    }

    private TablePagination.Param[] generateParams() {
        List<TablePagination.Param> results = new ArrayList<>();
        if (isKeywordExist()) {
            results.add(new TablePagination.Param("search", keyword));
        }
        if (isKelembagaanExists()) {
            results.add(new TablePagination.Param("kelembagaan", kelembagaan));
        }
        if (isKematanganExists()) {
            results.add(new TablePagination.Param("kematangan", kematangan));
        }
        TablePagination.Param[] params = new TablePagination.Param[results.size()];
        return results.toArray(params);
    }

    @Override
    public TablePagination.Param[] getParams() {
        return params;
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public String getKeyword() {
        return QueryUtil.sanitize(keyword);
    }

    public RekapUkpbj.KELEMBAGAAN getKelembagaan() {
        return kelembagaan;
    }

    public boolean isKelembagaanExists() {
        return kelembagaan != null;
    }

    public Integer getKematangan() {
        return kematangan;
    }

    public boolean isKematanganExists() {
        return kematangan != null;
    }

}
