package models.dashboard.queries;

import models.common.contracts.TablePagination;
import org.apache.commons.lang3.StringUtils;
import play.mvc.Scope;
import utils.common.QueryUtil;

import java.util.ArrayList;
import java.util.List;

public class DetailLpseSearchQuery implements TablePagination.ParamAble {

    public static final String TAG = "DetailLpseSearchQuery";

    private final String keyword;
    private final Integer standarLengkap;
    private final String versiSpse;
    private final int page;
    private final TablePagination.Param[] params;

    public DetailLpseSearchQuery(Scope.Params params) {
        this.keyword = params.get("search") != null ? params.get("search") : "";
        this.standarLengkap = params.get("standar_lengkap") != null ? params.get("standar_lengkap", Integer.class) : null;
        this.versiSpse = params.get("versi_spse") != null ? params.get("versi_spse") : "";
        this.page = getCurrentPage(params);
        this.params = generateParams();
    }

    private TablePagination.Param[] generateParams() {
        List<TablePagination.Param> results = new ArrayList<>();
        if (isKeywordExist()) {
            results.add(new TablePagination.Param("search", keyword));
        }
        TablePagination.Param[] params = new TablePagination.Param[results.size()];
        return results.toArray(params);
    }

    @Override
    public TablePagination.Param[] getParams() {
        return params;
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public String getKeyword() {
        return QueryUtil.sanitize(keyword);
    }

    public Integer getStandarLengkap() {
        return standarLengkap;
    }

    public String getVersiSpse() {
        return versiSpse;
    }

    public boolean isStandarLengkapExists() {
        return standarLengkap != null;
    }

    public boolean isVersiSpseExists() {
        return !StringUtils.isEmpty(versiSpse);
    }

    public boolean isStandarLengkap() {
        return standarLengkap == 1;
    }
}
