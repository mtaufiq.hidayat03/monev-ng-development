package models.dashboard.queries;

import models.common.contracts.TablePagination;
import models.dashboard.enums.JenisKontrakDisplayAttribute;
import play.mvc.Scope;
import utils.common.QueryUtil;

import java.util.ArrayList;
import java.util.List;

public class JenisKontrakSearchQuery implements TablePagination.ParamAble {

    public static final String TAG = "JenisKontrakSearchQuery";
    private final String keyword;
    private final JenisKontrakDisplayAttribute status;
    private final Integer year;
    private final int page;
    private final TablePagination.Param[] params;

    public JenisKontrakSearchQuery(Scope.Params params) {
        this.keyword = params.get("search") != null ? params.get("search") : "";
        this.status = params.get("filter") != null ? params.get("filter", JenisKontrakDisplayAttribute.class) : null;
        this.year = getInteger(params, "year");
        this.page = getCurrentPage(params);
        this.params = generateParams();
    }

    private TablePagination.Param[] generateParams() {
        List<TablePagination.Param> results = new ArrayList<>();
        if (isKeywordExist()) {
            results.add(new TablePagination.Param("search", keyword));
        }
        if (isStatusExists()) {
            results.add(new TablePagination.Param("filter", status));
        }
        if (getYear() != null) {
            results.add(new TablePagination.Param("year", year));
        }
        TablePagination.Param[] params = new TablePagination.Param[results.size()];
        return results.toArray(params);
    }

    @Override
    public TablePagination.Param[] getParams() {
        return params;
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public String getKeyword() {
        return QueryUtil.sanitize(keyword);
    }

    public boolean isStatusExists() {
        return status != null;
    }

    public boolean isYearExist() {
        return year != null && year != 0;
    }

    public Integer getYear() {
        return year;
    }

    public String getYearAsString() {
        return isYearExist()
                ? getYear().toString()
                : "Semua Tahun";
    }

}