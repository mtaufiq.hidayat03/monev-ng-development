package models.dashboard.queries;

import models.common.contracts.TablePagination;
import models.dashboard.PaketPenyedia;
import play.mvc.Scope;
import utils.common.QueryUtil;

import java.util.ArrayList;
import java.util.List;

public class PaketPenyediaSearchQuery implements TablePagination.ParamAble {

    public static final String TAG = "PaketPenyediaSearchQuery";

    private final String keyword;
    private final PaketPenyedia.STATUS_IKUT_SERTA status;
    private final Long rekananId;
    private final int page;
    private final TablePagination.Param[] params;

    public PaketPenyediaSearchQuery(Scope.Params params) {
        this.keyword = params.get("search") != null ? params.get("search") : "";
        this.status = params.get("status") != null ? params.get("status", PaketPenyedia.STATUS_IKUT_SERTA.class) : null;
        this.rekananId = params.get("rekanan_id") != null ? params.get("rekanan_id", Long.class) : null;
        this.page = getCurrentPage(params);
        this.params = generateParams();
    }

    private TablePagination.Param[] generateParams() {
        List<TablePagination.Param> results = new ArrayList<>();
        if (isKeywordExist()) {
            results.add(new TablePagination.Param("search", keyword));
        }
        if (isStatusExists()) {
            results.add(new TablePagination.Param("status", status));
        }
        if (isRekananIdExists()) {
            results.add(new TablePagination.Param("rekanan_id", rekananId));
        }
        TablePagination.Param[] params = new TablePagination.Param[results.size()];
        return results.toArray(params);
    }

    @Override
    public TablePagination.Param[] getParams() {
        return params;
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public String getKeyword() {
        return QueryUtil.sanitize(keyword);
    }

    public boolean isStatusExists() {
        return status != null;
    }

    public PaketPenyedia.STATUS_IKUT_SERTA getStatus() {
        return status;
    }

    public boolean isRekananIdExists() {
        return rekananId != null;
    }

    public Long getRekananId() {
        return rekananId;
    }
}
