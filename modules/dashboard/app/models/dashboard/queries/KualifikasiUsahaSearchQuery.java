package models.dashboard.queries;

import models.common.contracts.TablePagination;
import models.dashboard.enums.KualifikasiUsahaDisplayAttribute;
import play.mvc.Scope;

import java.util.ArrayList;
import java.util.List;

public class KualifikasiUsahaSearchQuery implements TablePagination.ParamAble {

    public static final String TAG = "KualifikasiUsahaSearchQuery";
    private final String keyword;
    private final KualifikasiUsahaDisplayAttribute status;
    private final int page;
    private final TablePagination.Param[] params;

    public KualifikasiUsahaSearchQuery(Scope.Params params) {
        this.keyword = generateKeyword(params);
        this.status = params.get("filter") != null ? params.get("filter", KualifikasiUsahaDisplayAttribute.class) : null;
        this.page = getCurrentPage(params);
        this.params = generateParams();
    }

    private TablePagination.Param[] generateParams() {
        List<TablePagination.Param> results = new ArrayList<>();
        if (isKeywordExist()) {
            results.add(new TablePagination.Param("search", keyword));
        }
        if (isStatusExists()) {
            results.add(new TablePagination.Param("filter", status));
        }
        TablePagination.Param[] params = new TablePagination.Param[results.size()];
        return results.toArray(params);
    }

    @Override
    public TablePagination.Param[] getParams() {
        return params;
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public String getKeyword() {
        return keyword;
    }

    public boolean isStatusExists() {
        return status != null && !status.attributes.contains("SEMUA");
    }

    public KualifikasiUsahaDisplayAttribute getStatus() {
        return status;
    }

    public KualifikasiUsahaDisplayAttribute getStatusAsFilter() {
        if (getStatus() == null) {
            return KualifikasiUsahaDisplayAttribute.SEMUA;
        }
        return getStatus();
    }

}
