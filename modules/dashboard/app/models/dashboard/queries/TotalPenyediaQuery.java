package models.dashboard.queries;

import models.common.contracts.TablePagination;
import play.mvc.Scope;
import utils.common.QueryUtil;

import java.util.ArrayList;
import java.util.List;

public class TotalPenyediaQuery implements TablePagination.ParamAble {

    public static final String TAG = "TotalPenyediaQuery";
    private final String keyword;
    private final int page;
    private final TablePagination.Param[] params;

    public TotalPenyediaQuery(Scope.Params params) {
        this.keyword = params.get("search") != null ? params.get("search") : "";
        this.page = getCurrentPage(params);
        this.params = generateParams();
    }

    private TablePagination.Param[] generateParams() {
        List<TablePagination.Param> results = new ArrayList<>();
        TablePagination.Param[] params = new TablePagination.Param[results.size()];
        return results.toArray(params);
    }

    @Override
    public TablePagination.Param[] getParams() {
        return params;
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public String getKeyword() {
        return QueryUtil.sanitize(keyword);
    }
}
