package models.dashboard;

import models.common.contracts.TablePagination.Param;
import models.common.contracts.TablePagination.ParamAble;
import models.dashboard.enums.JenisPengadaanDisplayAttribute;
import org.apache.commons.lang3.StringUtils;
import play.mvc.Scope;
import utils.common.LogUtil;
import utils.common.QueryUtil;

import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 11/7/2019 1:07 PM
 */
public class SearchQuery implements ParamAble {

    public static final String TAG = "SearchQuery";

    private final String keyword;
    private final String filter;
    private final String subFilter;
    private final int page;
    private final Param[] params;
    private final String item;
    private final Integer tahun;
    private final String tipe;
    private final String anggaran;
    private final String klpd;
    private final String institution;
    private final String propinsi;

    public SearchQuery(Scope.Params params) {
        this.keyword = params.get("search") != null ? params.get("search") : "";
        this.page = getCurrentPage(params);
        this.filter = !isNull(params, "filter") ? params.get("filter", String.class) : null;
        this.subFilter = !isNull(params, "subFilter") ? params.get("subFilter", String.class) : null;
        this.tahun = !isNull(params, "tahun") ? params.get("tahun", Integer.class) : null;
        this.item = !isNull(params, "item") ? params.get("item") : null;
        this.tipe = !isNull(params, "tipe") ? params.get("tipe") : null;
        this.anggaran = !isNull(params, "anggaran") ? params.get("anggaran") : null;
        this.klpd = !isNull(params, "klpd") ? params.get("klpd") : null;
        String decoded = getString(params, generateKey("institution"));
        try {
            decoded = URLDecoder.decode(decoded, "UTF-8");
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }
        institution = decoded;
        String propinsiDecoded = getString(params, generateKey("propinsi"));
        try {
            propinsiDecoded = URLDecoder.decode(propinsiDecoded, "UTF-8");
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }
        this.propinsi = propinsiDecoded;
        this.params = generateParams();
    }

    private Param[] generateParams() {
        List<Param> results = new ArrayList<>();
        if (isKeywordExist()) {
            results.add(new Param("search", keyword));
        }
        if (isFilterExist()) {
            results.add(new Param("filter", filter));
            results.add(new Param("subFilter", subFilter));
        }
        if (tahun != null) {
            results.add(new Param("tahun", tahun));
        }
        if (item != null && !item.isEmpty()) {
            results.add(new Param("item", item));
        }
        if (tipe != null && !tipe.isEmpty()) {
            results.add(new Param("tipe", tipe));
        }
        if (anggaran != null && !anggaran.isEmpty()) {
            results.add(new Param("anggaran", anggaran));
        }
        if (klpd != null && !klpd.isEmpty()) {
            results.add(new Param("klpd", klpd));
        }
        if (institutionExist()) {
            results.add(new Param(generateKey("institution"), institution));
        }
        if (propinsiExist()) {
            results.add(new Param(generateKey("propinsi"), propinsi));
        }
        Param[] params = new Param[results.size()];
        return results.toArray(params);
    }

    public boolean institutionExist() {
        return !StringUtils.isEmpty(institution);
    }

    public boolean propinsiExist() {
        return !StringUtils.isEmpty(propinsi);
    }

    public String getInstitution() {
        return institution;
    }

    public String getPropinsi() {
        return propinsi;
    }

    @Override
    public Param[] getParams() {
        return params;
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public String getKeyword() {
        return QueryUtil.sanitize(keyword);
    }

    public boolean isFilterExist() {
        return filter != null && !filter.isEmpty();
    }

    public String getFilter() {
        return filter;
    }

    public String getSubFilter() {
        return subFilter;
    }

    public String getType() {
        for (JenisPengadaanDisplayAttribute jenisPengadaanDisplayAttribute : JenisPengadaanDisplayAttribute.values()) {
            Boolean attribute = jenisPengadaanDisplayAttribute.toString()
                    .replaceAll("_", " ").equals(subFilter);
            if (attribute) {
                return jenisPengadaanDisplayAttribute.getId().toString();
            }
        }
        return null;
    }

    public String getItem() {
        return item;
    }

    public Integer getTahun() {
        return tahun;
    }

    public String getTipe() {
        return tipe;
    }

    public String getAnggaran() {
        return anggaran;
    }

    public String getKlpd() {
        return klpd;
    }

    public boolean isPercentage() {
        return getFilter() != null && getFilter().equals(3);
    }

    public String getDefaultFilterLabel() {
        LogUtil.debug(TAG, "filter: " + getFilter());
        final String label = getFilter() != null ? getFilter() : "Pilih Filter";
        LogUtil.debug(TAG, "label: " + label);
        return label;
    }


}
