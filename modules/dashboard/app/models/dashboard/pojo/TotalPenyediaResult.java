package models.dashboard.pojo;

import models.common.contracts.TablePagination;
import models.common.contracts.TablePagination.PageAble;
import models.dashboard.RekapPenyedia;
import models.dashboard.SearchQuery;
import ppsdm.models.CertificateHolder;

import java.util.List;

public class TotalPenyediaResult implements PageAble<RekapPenyedia> {

    private List<RekapPenyedia> items;
    private final SearchQuery request;
    private int total = 0;
    private Meta meta;

    public TotalPenyediaResult(SearchQuery request) {
        this.request = request;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setItems(List<RekapPenyedia> collection) {
        this.items = collection;
    }

    @Override
    public List<RekapPenyedia> getItems() {
        return items;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public TablePagination.ParamAble getParamAble() {
        return request;
    }

    @Override
    public Class<RekapPenyedia> getReturnType() {
        return RekapPenyedia.class;
    }

    @Override
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    @Override
    public Meta getMeta() {
        return meta;
    }
}