package models.dashboard.pojo;

import models.common.contracts.TablePagination;
import models.common.contracts.TablePagination.PageAble;

import java.util.List;

/**
 * @author HanusaCloud on 11/8/2019 1:57 PM
 */
public class DetailPengadaanResult implements PageAble<DetailPengadaan> {

    private int total;
    private List<DetailPengadaan> items;
    private final TablePagination.ParamAble params;

    public DetailPengadaanResult(TablePagination.ParamAble params) {
        this.params = params;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public TablePagination.ParamAble getParamAble() {
        return params;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setItems(List<DetailPengadaan> collection) {
        this.items = collection;
    }

    @Override
    public List<DetailPengadaan> getItems() {
        return items;
    }

    @Override
    public Class<DetailPengadaan> getReturnType() {
        return DetailPengadaan.class;
    }
}
