package models.dashboard.pojo;

import models.common.contracts.TablePagination;

import java.util.List;

/**
 * @author HanusaCloud on 11/11/2019 2:28 PM
 */
public class JenisResult implements TablePagination.PageAble<DetailPengadaan> {

    private List<DetailPengadaan> items;
    private final TablePagination.ParamAble paramAble;
    private int total = 0;

    public JenisResult(TablePagination.ParamAble paramAble) {
        this.paramAble = paramAble;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public TablePagination.ParamAble getParamAble() {
        return paramAble;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setItems(List<DetailPengadaan> collection) {
        this.items = collection;
    }

    @Override
    public List<DetailPengadaan> getItems() {
        return items;
    }

    @Override
    public Class<DetailPengadaan> getReturnType() {
        return DetailPengadaan.class;
    }
}
