package models.dashboard.pojo;

import models.common.contracts.TablePagination;
import models.dashboard.RekapPengadaan;

import java.util.List;

public class RekapPengadaanResult implements TablePagination.PageAble<RekapPengadaan> {

    private List<RekapPengadaan> items;
    private final TablePagination.ParamAble paramAble;
    private int total = 0;

    public RekapPengadaanResult(TablePagination.ParamAble paramAble) {
        this.paramAble = paramAble;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public TablePagination.ParamAble getParamAble() {
        return paramAble;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setItems(List<RekapPengadaan> collection) {
        this.items = collection;
    }

    @Override
    public List<RekapPengadaan> getItems() {
        return items;
    }

    @Override
    public Class<RekapPengadaan> getReturnType() {
        return RekapPengadaan.class;
    }
}

