package models.dashboard.pojo;

import models.common.contracts.TablePagination;
import models.dashboard.PaketPenyedia;

import java.util.List;

/**
 * @author HanusaCloud on 11/25/2019 5:26 PM
 */
public class PaketPenyediaResult implements TablePagination.PageAble<PaketPenyedia> {

    private List<PaketPenyedia> items;
    private final TablePagination.ParamAble paramAble;
    private int total = 0;

    public PaketPenyediaResult(TablePagination.ParamAble paramAble) {
        this.paramAble = paramAble;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public TablePagination.ParamAble getParamAble() {
        return paramAble;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setItems(List<PaketPenyedia> collection) {
        this.items = collection;
    }

    @Override
    public List<PaketPenyedia> getItems() {
        return items;
    }

    @Override
    public Class<PaketPenyedia> getReturnType() {
        return PaketPenyedia.class;
    }
}
