package models.dashboard.pojo;

import models.common.contracts.TablePagination;
import models.dashboard.DetailLpse;

import java.util.List;

public class DetailLpseResult implements TablePagination.PageAble<DetailLpse> {

    private List<DetailLpse> items;
    private final TablePagination.ParamAble paramAble;
    private int total = 0;

    public DetailLpseResult(TablePagination.ParamAble paramAble) {
        this.paramAble = paramAble;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public TablePagination.ParamAble getParamAble() {
        return paramAble;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setItems(List<DetailLpse> collection) {
        this.items = collection;
    }

    @Override
    public List<DetailLpse> getItems() {
        return items;
    }

    @Override
    public Class<DetailLpse> getReturnType() {
        return DetailLpse.class;
    }
}

