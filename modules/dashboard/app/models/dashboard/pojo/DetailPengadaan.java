package models.dashboard.pojo;

import extensions.common.CurrencyFormatter;
import extensions.common.NumberFormatter;
import utils.common.LogUtil;

public class DetailPengadaan {

    public static final String TAG = "DetailPengadaan";

    public String kode;
    public String label;
    public int rencanaPaket = 0;
    public long rencanaPagu = 0;
    public int pelaksanaanPaket = 0;
    public long pelaksanaanPagu = 0;
    public int hasilPaket = 0;
    public long hasilPagu = 0;
    // only use this if query without group by header2
    public int paket = 0;
    public long pagu = 0;
    public String tahap;

    public double percentage = 0.0;

    public double getTotal() {
        return rencanaPagu + pelaksanaanPagu + hasilPagu;
    }

    public void setPercentageValue(double divider) {
        LogUtil.debug(TAG, "total : " + getTotal());
        final double result = (getTotal() / divider) * 100;
        LogUtil.debug(TAG, "result: " + result);
        this.percentage = result;
    }

    public String getKode() {
        return kode;
    }

    public String getHumanReadblePagu() {
        return CurrencyFormatter.formatCurrenciesJutaRupiah(pagu);
    }

    public String getPercentage() {
        return NumberFormatter.formatNumberIndonesia(percentage) + " %";
    }

    public String getFormattedLabel() {
        return getLabelValue() + " paket: " + paket + ", pagu: " + getHumanReadblePagu() + "";
    }

    public String getLabelValue() {
        return label;
    }

    public String getValue() {
        return getLabelValue();
    }


}
