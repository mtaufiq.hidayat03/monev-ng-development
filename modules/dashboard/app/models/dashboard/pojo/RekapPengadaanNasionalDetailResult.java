package models.dashboard.pojo;

import models.common.contracts.TablePagination;
import models.dashboard.RekapPengadaanNasionalDetail;

import java.util.List;

public class RekapPengadaanNasionalDetailResult implements TablePagination.PageAble<RekapPengadaanNasionalDetail> {

    private List<RekapPengadaanNasionalDetail> items;
    private final TablePagination.ParamAble paramAble;
    private int total = 0;

    public RekapPengadaanNasionalDetailResult(TablePagination.ParamAble paramAble) {
        this.paramAble = paramAble;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public TablePagination.ParamAble getParamAble() {
        return paramAble;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setItems(List<RekapPengadaanNasionalDetail> collection) {
        this.items = collection;
    }

    @Override
    public List<RekapPengadaanNasionalDetail> getItems() {
        return items;
    }

    @Override
    public Class<RekapPengadaanNasionalDetail> getReturnType() {
        return RekapPengadaanNasionalDetail.class;
    }
}
