package models.dashboard.pojo;

import models.common.contracts.TablePagination;
import models.dashboard.RekapUkpbj;

import java.util.List;

public class RekapUkpbjResult implements TablePagination.PageAble<RekapUkpbj> {

    private List<RekapUkpbj> items;
    private final TablePagination.ParamAble paramAble;
    private int total = 0;

    public RekapUkpbjResult(TablePagination.ParamAble paramAble) {
        this.paramAble = paramAble;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public TablePagination.ParamAble getParamAble() {
        return paramAble;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setItems(List<RekapUkpbj> collection) {
        this.items = collection;
    }

    @Override
    public List<RekapUkpbj> getItems() {
        return items;
    }

    @Override
    public Class<RekapUkpbj> getReturnType() {
        return RekapUkpbj.class;
    }
}

