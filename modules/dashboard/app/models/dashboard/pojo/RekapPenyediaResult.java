package models.dashboard.pojo;

import models.common.contracts.TablePagination;
import models.dashboard.RekapPenyedia;

import java.util.List;

/**
 * @author HanusaCloud on 11/25/2019 5:26 PM
 */
public class RekapPenyediaResult implements TablePagination.PageAble<RekapPenyedia> {

    private List<RekapPenyedia> items;
    private final TablePagination.ParamAble paramAble;
    private int total = 0;

    public RekapPenyediaResult(TablePagination.ParamAble paramAble) {
        this.paramAble = paramAble;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public TablePagination.ParamAble getParamAble() {
        return paramAble;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setItems(List<RekapPenyedia> collection) {
        this.items = collection;
    }

    @Override
    public List<RekapPenyedia> getItems() {
        return items;
    }

    @Override
    public Class<RekapPenyedia> getReturnType() {
        return RekapPenyedia.class;
    }
}
