package models.dashboard.pojo;

import models.common.contracts.TablePagination;
import models.dashboard.KualifikasiUsaha;

import java.util.List;

public class KualifikasiUsahaResult implements TablePagination.PageAble<KualifikasiUsaha>{

    private List<KualifikasiUsaha> items;
    private final TablePagination.ParamAble paramAble;
    private int total = 0;

    public KualifikasiUsahaResult(TablePagination.ParamAble paramAble) {
        this.paramAble = paramAble;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public TablePagination.ParamAble getParamAble() {
        return paramAble;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setItems(List<KualifikasiUsaha> collection) {
        this.items = collection;
    }

    @Override
    public List<KualifikasiUsaha> getItems() {
        return items;
    }

    @Override
    public Class<KualifikasiUsaha> getReturnType() {
        return KualifikasiUsaha.class;
    }
}
