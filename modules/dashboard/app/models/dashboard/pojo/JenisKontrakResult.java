package models.dashboard.pojo;

import models.common.contracts.TablePagination;
import models.dashboard.JenisKontrak;

import java.util.List;

public class JenisKontrakResult implements TablePagination.PageAble<JenisKontrak> {

    private List<JenisKontrak> items;
    private final TablePagination.ParamAble paramAble;
    private int total = 0;

    public JenisKontrakResult(TablePagination.ParamAble paramAble) {
        this.paramAble = paramAble;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public TablePagination.ParamAble getParamAble() {
        return paramAble;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setItems(List<JenisKontrak> collection) {
        this.items = collection;
    }

    @Override
    public List<JenisKontrak> getItems() {
        return items;
    }

    @Override
    public Class<JenisKontrak> getReturnType() {
        return JenisKontrak.class;
    }

}
