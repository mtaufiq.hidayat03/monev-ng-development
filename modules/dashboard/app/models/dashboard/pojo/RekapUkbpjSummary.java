package models.dashboard.pojo;

public class RekapUkbpjSummary {
    public long jumlahUkpbj;
    public long jumlahStruktural;
    public long jumlahAdhoc;
    public long jumlahBelumTerbentuk;
    public long jumlahKematangan9;
}
