package models.dashboard;

import models.cms.profilekl.ProfileKlData;
import models.cms.profilekl.ProfileKlDataUtilContract;
import models.cms.profilekl.ProfileKlHeader;
import models.common.ReadOnlyModel;
import play.db.jdbc.Table;

import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.List;

/**
 * @author idoej
 */
@Table(name = "rekap_pengadaan", schema = "public")
public class RekapPengadaan extends ReadOnlyModel implements ProfileKlDataUtilContract {

    public static final String TAG = "RekapPengadaan";

    public Integer tahun_anggaran;
    public String jenis_belanja;
    public String jenis_pengadaan;
    public String metode_pemilihan;
    public String sumber_dana;
    public String tipe_kldi;
    public String kode_kldi;
    public String nama_kldi;
    public String kode_satker;
    public String nama_satker;
    public long total_anggaran;
    public long belanja_pengadaan;
    public int rencana_paket;
    public long rencana_pagu;
    public int pelaksanaan_paket;
    public long pelaksanaan_pagu;
    public int hasil_paket;
    public long hasil_pagu;
    public long kontrak;
    public long pembayaran;

    @Transient
    public String label;

    @Transient
    public String kode;

    public Double getPersenPerencanaan() {
        return 100.0 * rencana_pagu / belanja_pengadaan;
    }

    public Double getPersenPelaksanaan() {
        return 100.0 * pelaksanaan_pagu / rencana_pagu;
    }

    public Double getPersenHasil() {
        return 100.0 * hasil_pagu / pelaksanaan_pagu;
    }

    public Double getPersenKontrak() {
        return 100.0 * kontrak / hasil_pagu;
    }

    public Double getPersenPembayaran() {
        return 100.0 * pembayaran / kontrak;
    }

    public RekapPengadaan() {}

    public RekapPengadaan(List<ProfileKlData> data) {
        total_anggaran = searchFor(ProfileKlHeader.TOTAL_ANGGARAN, data);
        belanja_pengadaan = searchFor(ProfileKlHeader.BELANJA_PENGADAAN, data);
        rencana_pagu = searchFor(ProfileKlHeader.PERENCANAAN, data);
        pelaksanaan_pagu = searchFor(ProfileKlHeader.PELAKSANAAN, data);
        hasil_pagu = searchFor(ProfileKlHeader.HASIL_PEMILIHAN, data);
        kontrak = searchFor(ProfileKlHeader.KONTRAK, data);
        pembayaran = searchFor(ProfileKlHeader.PEMBAYARAN, data);
    }

    @Override
    public List<ProfileKlData> getProfileData() {
        List<ProfileKlData> data = new ArrayList<>();
        data.add(new ProfileKlData(ProfileKlHeader.TOTAL_ANGGARAN, (double) total_anggaran));
        data.add(new ProfileKlData(ProfileKlHeader.BELANJA_PENGADAAN, (double) belanja_pengadaan));
        data.add(new ProfileKlData(ProfileKlHeader.PERENCANAAN, (double) rencana_pagu));
        data.add(new ProfileKlData(ProfileKlHeader.PELAKSANAAN, (double) pelaksanaan_pagu));
        data.add(new ProfileKlData(ProfileKlHeader.HASIL_PEMILIHAN, (double) hasil_pagu));
        data.add(new ProfileKlData(ProfileKlHeader.KONTRAK, (double) kontrak));
        data.add(new ProfileKlData(ProfileKlHeader.PEMBAYARAN, (double) pembayaran));
        return data;
    }
}
