package models.dashboard;

import models.common.ReadOnlyModel;
import org.apache.commons.lang3.StringUtils;
import play.db.jdbc.Table;
import utils.common.AttributeExtractable;

/**
 * @author idoej
 */
@Table(name = "rekap_lpse", schema = "public")
public class DetailLpse extends ReadOnlyModel implements AttributeExtractable {

    public static final String TAG = "DetailLpse";

    public String nama;
    public int jumlah_standar;
    public String versi_spse;
    public Float longitude;
    public Float latitude;
    public String alamat;
    public String website;

    public DetailLpse turnToValidJson() {
        nama = escapse(nama);
        alamat = escapse(alamat);
        website = escapse(website);
        return this;
    }

    public static String escapse(String jsString) {
        if (StringUtils.isEmpty(jsString)) {
            return "";
        }
        return jsString.replace("\\", "\\\\")
                .replace("\"", "\\\"")
                .replace("\b", "\\b")
                .replace("\f", "\\f")
                .replace("\n", "\\n")
                .replace("\r", "\\r")
                .replace("\t", "\\t")
                .replace("/", "\\/");
    }



}
