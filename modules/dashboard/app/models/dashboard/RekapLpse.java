package models.dashboard;

import models.common.ReadOnlyModel;
import play.db.jdbc.Table;

import java.util.Date;

/**
 * @author idoej
 */
@Table(name = "rekap_lpse", schema = "public")
public class RekapLpse extends ReadOnlyModel {

    public static final String TAG = "RekapLpse";

    public int jumlah;
    public int jumlah_standar_lengkap;
    public Date tanggal_update;
    
}
