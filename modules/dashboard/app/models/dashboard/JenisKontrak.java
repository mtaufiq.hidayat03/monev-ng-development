package models.dashboard;

import models.dashboard.enums.JenisKontrakDisplayAttribute;
import play.data.binding.As;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import utils.common.IndonesianNumberBinder;

@Table(name = "jenis_kontrak", schema = "public")
public class JenisKontrak extends BaseTable {
    @Id
    public Long id;

    public String kode_kldi;
    public String nama_kldi;
    public String kode_satker;
    public String nama_satker;
    @As(binder = IndonesianNumberBinder.class)
    public Long total_anggaran;
    @As(binder = IndonesianNumberBinder.class)
    public Long belanja_pengadaan;
    @As(binder = IndonesianNumberBinder.class)
    public Long pelaksanaan_pagu;
    @As(binder = IndonesianNumberBinder.class)
    public Long pelaksanaan_paket;
    @As(binder = IndonesianNumberBinder.class)
    public Long hasil_paket;
    @As(binder = IndonesianNumberBinder.class)
    public Long hasil_pagu;
    public JenisKontrakDisplayAttribute jenis_kontrak;
    public Integer tahun_anggaran;

}
