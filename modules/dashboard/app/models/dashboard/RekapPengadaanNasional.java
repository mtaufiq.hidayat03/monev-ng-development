package models.dashboard;

import models.smartreport.BaseAdjustable;
import org.joda.time.LocalDateTime;
import play.data.binding.As;
import play.db.jdbc.Table;
import utils.common.ExtractableChartQueryResult;
import utils.common.IndonesianNumberBinder;
import utils.common.LogUtil;

import java.util.Date;

/**
 * @author idoej
 */
@Table(name = "rekap_pengadaan_nasional", schema = "public")
public class RekapPengadaanNasional extends BaseAdjustable implements ExtractableChartQueryResult {

    public static final String TAG = "RekapPengadaanNasional";

    @As(binder = IndonesianNumberBinder.class)
    public long total_anggaran;
    @As(binder = IndonesianNumberBinder.class)
    public long belanja_pengadaan;
    @As(binder = IndonesianNumberBinder.class)
    public int rencana_paket;
    @As(binder = IndonesianNumberBinder.class)
    public long rencana_pagu;
    @As(binder = IndonesianNumberBinder.class)
    public int pelaksanaan_paket;
    @As(binder = IndonesianNumberBinder.class)
    public long pelaksanaan_pagu;
    @As(binder = IndonesianNumberBinder.class)
    public int hasil_paket;
    @As(binder = IndonesianNumberBinder.class)
    public long hasil_pagu;
    @As(binder = IndonesianNumberBinder.class)
    public long kontrak;
    @As(binder = IndonesianNumberBinder.class)
    public long pembayaran;

    public static RekapPengadaanNasional create(int tahun) {
        RekapPengadaanNasional rekap = new RekapPengadaanNasional();
        rekap.tahun = tahun;
        return rekap;
    }

    public Double getPersenPerencanaan() {
        return 100.0 * rencana_pagu / belanja_pengadaan;
    }

    public Double getPersenPelaksanaan() {
        return 100.0 * pelaksanaan_pagu / rencana_pagu;
    }

    public Double getPersenHasil() {
        return 100.0 * hasil_pagu / pelaksanaan_pagu;
    }

    public Double getPersenKontrak() {
        return 100.0 * kontrak / hasil_pagu;
    }

    public Double getPersenPembayaran() {
        return 100.0 * pembayaran / kontrak;
    }

    public boolean isAdaUpdate() {
        RekapPengadaanNasionalOriginal originalSummary = RekapPengadaanNasionalOriginal.find("tahun = ?", tahun)
                .first();

        return originalSummary != null
                && LocalDateTime.fromDateFields(tanggal_update)
                    .isBefore(LocalDateTime.fromDateFields(originalSummary.tanggal_update));
    }

    public void copy(RekapPengadaanNasional copy) {
        LogUtil.debug(TAG, copy);
        total_anggaran = copy.total_anggaran;
        belanja_pengadaan = copy.belanja_pengadaan;
        rencana_paket = copy.rencana_paket;
        rencana_pagu = copy.rencana_pagu;
        pelaksanaan_paket = copy.pelaksanaan_paket;
        pelaksanaan_pagu = copy.pelaksanaan_pagu;
        hasil_paket = copy.hasil_paket;
        hasil_pagu = copy.hasil_pagu;
        kontrak = copy.kontrak;
        pembayaran = copy.pembayaran;
        tanggal_update = new Date();
    }

    @Override
    public String getXAxisLabel() {
        return String.valueOf(tahun);
    }
}
