package models.dashboard;

import models.common.ReadOnlyModel;
import play.db.jdbc.Table;

import java.util.Date;

/**
 * @author idoej
 */
@Table(name = "rekap_pengadaan_nasional_original", schema = "public")
public class RekapPengadaanNasionalOriginal extends ReadOnlyModel {

    public static final String TAG = "RekapPengadaanNasional";

    public Integer tahun;
    public long total_anggaran;
    public long belanja_pengadaan;
    public int rencana_paket;
    public long rencana_pagu;
    public int pelaksanaan_paket;
    public long pelaksanaan_pagu;
    public int hasil_paket;
    public long hasil_pagu;
    public long kontrak;
    public long pembayaran;
    public Date tanggal_update;

    public Double getPersenPerencanaan() {
        return 100.0 * rencana_pagu / belanja_pengadaan;
    }

    public Double getPersenPelaksanaan() {
        return 100.0 * pelaksanaan_pagu / rencana_pagu;
    }

    public Double getPersenHasil() {
        return 100.0 * hasil_pagu / pelaksanaan_pagu;
    }

    public Double getPersenKontrak() {
        return 100.0 * kontrak / hasil_pagu;
    }

    public Double getPersenPembayaran() {
        return 100.0 * pembayaran / kontrak;
    }

}
