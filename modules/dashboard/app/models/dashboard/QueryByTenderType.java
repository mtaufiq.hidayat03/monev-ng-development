package models.dashboard;

import models.common.contracts.TablePagination;
import play.mvc.Scope;
import utils.common.QueryUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 11/11/2019 12:40 PM
 */
public class QueryByTenderType implements TablePagination.ParamAble {

    private final TablePagination.Param[] params;
    private final String keyword;
    private final int page;
    private final String type;
    private final String region;
    private final String kldiType;
    private final String kldi;
    private final String filter;

    public QueryByTenderType(Scope.Params params) {
        this.keyword = generateKeyword(params);
        this.type = QueryUtil.sanitize(params.get("type"));
        this.region = QueryUtil.sanitize(params.get("region"));
        this.kldiType = QueryUtil.sanitize(params.get("kldiType"));
        this.kldi = QueryUtil.sanitize(params.get("kldi"));
        this.filter = QueryUtil.sanitize(params.get("filter"));
        this.page = getCurrentPage(params);
        this.params = generateParams();
    }

    private TablePagination.Param[] generateParams() {
        List<TablePagination.Param> results = new ArrayList<>();
        if (isKeywordExist()) {
            results.add(new TablePagination.Param("search", keyword));
        }
        if (type != null && !type.isEmpty()) {
            results.add(new TablePagination.Param("type", type));
        }
        if (region != null && !region.isEmpty()) {
            results.add(new TablePagination.Param("region", region));
        }
        if (kldiType != null && !kldiType.isEmpty()) {
            results.add(new TablePagination.Param("kldiType", kldiType));
        }
        if (kldi != null && !kldi.isEmpty()) {
            results.add(new TablePagination.Param("kldi", kldi));
        }
        if (filter != null && !filter.isEmpty()) {
            results.add(new TablePagination.Param("filter", filter));
        }
        TablePagination.Param[] params = new TablePagination.Param[results.size()];
        return results.toArray(params);
    }
    @Override
    public TablePagination.Param[] getParams() {
        return this.params;
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public String getKeyword() {
        return keyword;
    }

    public boolean isTypeExist() {
        return type != null && !type.isEmpty();
    }

    public String getType() {
        return type;
    }

    public boolean isRegionExist() {
        return region != null && !region.isEmpty();
    }

    public String getRegion() {
        return region;
    }

    public boolean isKldiTypeExist() {
        return kldiType != null && !kldiType.isEmpty();
    }

    public String getKldiType() {
        return kldiType;
    }

    public boolean isKldiExist() {
        return kldi != null && !kldi.isEmpty();
    }

    public String getKldi() {
        return kldi;
    }

}
