package models.dashboard;

import constants.generated.R;
import models.common.ReadOnlyModel;
import org.apache.commons.collections4.map.HashedMap;
import play.db.jdbc.Table;
import play.mvc.Router;
import ppsdm.pojos.TotalPenyediaMeta;
import utils.common.ExtractableChartQueryResult;

import javax.persistence.Transient;
import java.util.Map;

/**
 * @author idoej
 */
@Table(name = "rekap_penyedia", schema = "public")
public class RekapPenyedia extends ReadOnlyModel implements ExtractableChartQueryResult {

    public static final String TAG = "RekapPenyedia";

    public Long rekanan_id;
    public String nama;
    public String npwp;
    public String alamat;
    public String propinsi;
    public String kabupaten;

    public Long kontrak_menang;
    public Integer paket_menang;
    public Integer paket_menawar;
    public Integer paket_prakualifikasi;
    public Integer paket_daftar;

    @Transient
    private Long total;
    @Transient
    public TotalPenyediaMeta meta;

    public Long getTotalPropinsi() {
        return total;
    }

    @Override
    public String getXAxisLabel() {
        return nama;
    }

    public String getUrlByPropinsi() {
        Map<String, Object> map = new HashedMap<>();
        map.put("propinsi", propinsi);
        return Router.reverse(R.route.dashboard_datapenyediacontroller_totalpenyedianasionalleveltwo, map).url;
    }
}
