package models.dashboard;

import models.common.contracts.TablePagination;
import models.common.contracts.TablePagination.Param;
import models.dashboard.enums.JenisPengadaanDisplayAttribute;
import play.mvc.Scope;
import utils.common.QueryUtil;

/**
 * @author HanusaCloud on 11/11/2019 12:40 PM
 */
public class SatkerQueryByTenderType implements TablePagination.ParamAble {

    public static final String TAG = "SatkerQueryByTenderType";

    private final Param[] params;
    private final String keyword;
    private final int page;
    private final String type;
    private final String region;
    private final String kldiType;
    private final String kldi;

    public SatkerQueryByTenderType(Scope.Params params, Param[] queryParams) {
        this.keyword = generateKeyword(params);
        this.type = QueryUtil.sanitize(params.get("type"));
        this.region = QueryUtil.sanitize(params.get("region"));
        this.kldiType = QueryUtil.sanitize(params.get("kldiType"));
        this.kldi = QueryUtil.sanitize(params.get("kldi"));
        this.page = getCurrentPage(params);
        this.params = combineOthers(params, queryParams);
    }

    @Override
    public Param[] getParams() {
        return this.params;
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public String getKeyword() {
        return keyword;
    }

    public String getType() {
        for (JenisPengadaanDisplayAttribute jenisPengadaanDisplayAttribute : JenisPengadaanDisplayAttribute.values()) {
            Boolean attribute = jenisPengadaanDisplayAttribute.toString()
                    .replaceAll("_", "").equals(type);
            if (attribute) {
                return jenisPengadaanDisplayAttribute.getId().toString();
            }
        }
        return null;
    }

    public String getRegion() {
        return region;
    }

    public String getKldiType() {
        return kldiType;
    }

    public boolean isKldiExist() {
        return kldi != null && !kldi.isEmpty();
    }

    public String getKldi() {
        return kldi;
    }

    @Override
    public String getKeyPrefix() {
        return "satker";
    }
}
