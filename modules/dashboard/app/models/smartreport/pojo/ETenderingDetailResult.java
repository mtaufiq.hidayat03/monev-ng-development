package models.smartreport.pojo;

import models.common.contracts.TablePagination;
import models.smartreport.ETenderingDetail;

import java.util.List;

public class ETenderingDetailResult implements TablePagination.PageAble<ETenderingDetail> {

    private List<ETenderingDetail> items;
    private final TablePagination.ParamAble paramAble;
    private int total = 0;

    public ETenderingDetailResult(TablePagination.ParamAble paramAble) {
        this.paramAble = paramAble;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public TablePagination.ParamAble getParamAble() {
        return paramAble;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setItems(List<ETenderingDetail> collection) {
        this.items = collection;
    }

    @Override
    public List<ETenderingDetail> getItems() {
        return items;
    }

    @Override
    public Class<ETenderingDetail> getReturnType() {
        return ETenderingDetail.class;
    }
}
