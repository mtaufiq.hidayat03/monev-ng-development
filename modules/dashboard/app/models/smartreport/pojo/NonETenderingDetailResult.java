package models.smartreport.pojo;

import models.common.contracts.TablePagination;
import models.smartreport.NonETenderingDetail;

import java.util.List;

public class NonETenderingDetailResult implements TablePagination.PageAble<NonETenderingDetail> {

    private List<NonETenderingDetail> items;
    private final TablePagination.ParamAble paramAble;
    private int total = 0;

    public NonETenderingDetailResult(TablePagination.ParamAble paramAble) {
        this.paramAble = paramAble;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public TablePagination.ParamAble getParamAble() {
        return paramAble;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setItems(List<NonETenderingDetail> collection) {
        this.items = collection;
    }

    @Override
    public List<NonETenderingDetail> getItems() {
        return items;
    }

    @Override
    public Class<NonETenderingDetail> getReturnType() {
        return NonETenderingDetail.class;
    }
}
