package models.smartreport.pojo;

import models.common.contracts.TablePagination;
import models.smartreport.EPurchasingDetail;

import java.util.List;

public class EPurchasingDetailResult implements TablePagination.PageAble<EPurchasingDetail> {
    private List<EPurchasingDetail> items;
    private final TablePagination.ParamAble paramAble;
    private int total = 0;

    public EPurchasingDetailResult(TablePagination.ParamAble paramAble) {
        this.paramAble = paramAble;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public TablePagination.ParamAble getParamAble() {
        return paramAble;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setItems(List<EPurchasingDetail> collection) {
        this.items = collection;
    }

    @Override
    public List<EPurchasingDetail> getItems() {
        return items;
    }

    @Override
    public Class<EPurchasingDetail> getReturnType() {
        return EPurchasingDetail.class;
    }
}
