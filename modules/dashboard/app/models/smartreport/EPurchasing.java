package models.smartreport;

import org.joda.time.LocalDateTime;
import play.data.binding.As;
import play.db.jdbc.Table;
import utils.common.ExtractableChartQueryResult;
import utils.common.IndonesianDecimalBinder;
import utils.common.IndonesianNumberBinder;

import java.util.Date;

@Table(name = "e_purchasing", schema = "smartreport")
public class EPurchasing extends BaseAdjustable implements ExtractableChartQueryResult {
    public static final String TAG = "EPurchasing";

    @As(binder = IndonesianNumberBinder.class)
    public Integer jumlah_paket = 0;
    @As(binder = IndonesianDecimalBinder.class)
    public Double nilai_transaksi = 0d;
    @As(binder = IndonesianNumberBinder.class)
    public Integer jumlah_kategori_komoditas = 0;
    @As(binder = IndonesianNumberBinder.class)
    public Integer jumlah_penyedia = 0;
    @As(binder = IndonesianNumberBinder.class)
    public Integer jumlah_produk = 0;

    @Override
    public String getXAxisLabel() {
        return String.valueOf(tahun);
    }

    public boolean isAdaUpdate() {
        EPurchasingOriginal originalSummary = EPurchasingOriginal.find("tahun = ?", tahun).first();
        return originalSummary != null
                && LocalDateTime.fromDateFields(tanggal_update)
                .isBefore(LocalDateTime.fromDateFields(originalSummary.tanggal_update));
    }

    public void copy(EPurchasing copy) {
        jumlah_paket = copy.jumlah_paket;
        nilai_transaksi = copy.nilai_transaksi;
        jumlah_kategori_komoditas = copy.jumlah_kategori_komoditas;
        jumlah_penyedia = copy.jumlah_penyedia;
        jumlah_produk = copy.jumlah_produk;
        tanggal_update = new Date();
    }

}
