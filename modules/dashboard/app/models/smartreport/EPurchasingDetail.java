package models.smartreport;

import play.data.binding.As;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Table;
import utils.common.ExtractableChartQueryResult;
import utils.common.IndonesianDecimalBinder;
import utils.common.IndonesianNumberBinder;

@Table(name = "e_purchasing_detail", schema = "smartreport")
public class EPurchasingDetail extends BaseTable implements ExtractableChartQueryResult {

    public static final String TAG = "EPurchasingDetail";

    @Id
    public Long id;
    public Integer tahun;
    public String kode_kldi;
    public String nama_kldi;
    public String kode_satker;
    public String nama_satker;
    @As(binder = IndonesianNumberBinder.class)
    public Integer jumlah_paket = 0;
    @As(binder = IndonesianDecimalBinder.class)
    public Double nilai_transaksi = 0d;
    @As(binder = IndonesianNumberBinder.class)
    public Integer jumlah_kategori_komoditas = 0;
    @As(binder = IndonesianNumberBinder.class)
    public Integer jumlah_penyedia = 0;
    @As(binder = IndonesianNumberBinder.class)
    public Integer jumlah_produk = 0;

    @Override
    public String getXAxisLabel() {
        return nama_kldi;
    }
}
