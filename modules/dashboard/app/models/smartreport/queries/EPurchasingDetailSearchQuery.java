package models.smartreport.queries;

import models.common.contracts.TablePagination;
import models.smartreport.enums.EPurchasingDisplayAttribute;
import play.mvc.Scope;
import utils.common.QueryUtil;

import java.util.ArrayList;
import java.util.List;

public class EPurchasingDetailSearchQuery implements TablePagination.ParamAble {
    public static final String TAG = "EPurchasingDetailSearchQuery";
    private final String keyword;
    private final String tahun;
    private final EPurchasingDisplayAttribute status;
    private final int page;
    private final TablePagination.Param[] params;

    public EPurchasingDetailSearchQuery(Scope.Params params) {
        this.tahun = params.get("tahun") != null ? params.get("tahun") : "";
        this.keyword = generateKeyword(params);
        this.status = params.get("filter") != null ? params.get("filter", EPurchasingDisplayAttribute.class) : null;
        this.page = getCurrentPage(params);
        this.params = generateParams();
    }

    private TablePagination.Param[] generateParams() {
        List<TablePagination.Param> results = new ArrayList<>();
        if (isTahunExists()) {
            results.add(new TablePagination.Param("tahun", tahun));
        }
        if (isKeywordExist()) {
            results.add(new TablePagination.Param("search", keyword));
        }
        if (isStatusExists()) {
            results.add(new TablePagination.Param("filter", status));
        }
        TablePagination.Param[] params = new TablePagination.Param[results.size()];
        return results.toArray(params);
    }

    @Override
    public TablePagination.Param[] getParams() {
        return params;
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public String getKeyword() {
        return QueryUtil.sanitize(keyword);
    }

    public boolean isStatusExists() {
        return status != null;
    }

    public boolean isTahunExists() {
        return tahun != null;
    }
}
