package models.smartreport;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import models.common.contracts.DataTablePagination;
import utils.common.LogUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 11/25/2019 5:26 PM
 */
public class EtenderingResult implements DataTablePagination.PageAble<ETenderingSummary> {

    private List<ETenderingSummary> items;
    private final DataTablePagination.Request request;
    private int total = 0;
    private int totalItemInOnePage = 0;
    private final String[] columns = new String[]{
            "id", "tahun", "tanggal_update"
    };
    private final List<String> searchAbleFields;

    public EtenderingResult(DataTablePagination.Request request) {
        this.request = request;
        List<String> fields = new ArrayList<>();
        fields.add("tahun::TEXT");
        fields.add("tanggal_update::TEXT");
        searchAbleFields = fields;
    }

    @Override
    public JsonObject getResultsAsJson() {
        JsonObject result = getResponseTemplate();
        JsonArray jsonArray = new JsonArray();
        if (getTotal() == 0) {
            result.add("data", jsonArray);
            return result;
        }
        int i = getNumberingStartFrom();
        for (ETenderingSummary model : getResults()) {
            JsonArray array = new JsonArray();
            array.add(i);
            array.add(model.tahun);
            array.add(model.getFormattedTanggalUpdate());
            array.add(model.isAdaUpdate() ? "Ada" : "Tidak");
            array.add(model.id);
            jsonArray.add(array);
            i++;
        }
        result.add("data", jsonArray);
        LogUtil.debug(TAG, result);
        return result;
    }

    @Override
    public List<String> extractSearchAbleFields(Field[] fields) {
        return searchAbleFields;
    }

    @Override
    public void setResult(List<ETenderingSummary> items) {
        this.items = items;
    }

    @Override
    public List<ETenderingSummary> getResults() {
        return items;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setTotalItemInOnePage(int totalItem) {
        totalItemInOnePage = totalItem;
    }

    @Override
    public int getTotalItemInOnePage() {
        return totalItemInOnePage;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public String[] getColumns() {
        return columns;
    }

    @Override
    public DataTablePagination.Request getRequest() {
        return request;
    }

    @Override
    public Class<ETenderingSummary> getReturnType() {
        return ETenderingSummary.class;
    }
}
