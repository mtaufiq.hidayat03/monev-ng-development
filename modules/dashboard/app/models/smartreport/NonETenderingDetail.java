package models.smartreport;

import models.common.ReadOnlyModel;
import play.db.jdbc.Table;
import utils.common.ExtractableChartQueryResult;

@Table(name = "non_e_tendering_detail", schema = "smartreport")
public class NonETenderingDetail extends ReadOnlyModel implements ExtractableChartQueryResult {
    public static final String TAG = "NonETenderingDetail";

    public Integer tahun;
    public String kode_lpse;
    public String nama_lpse;
    public Integer jumlah_paket;
    public Double nilai_pagu;
    public Double nilai_transaksi;

    @Override
    public String getXAxisLabel() {
        return String.valueOf(nama_lpse);
    }

}
