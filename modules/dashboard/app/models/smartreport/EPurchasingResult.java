package models.smartreport;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import models.common.contracts.DataTablePagination;
import utils.common.LogUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 11/25/2019 6:20 PM
 */
public class EPurchasingResult implements DataTablePagination.PageAble<EPurchasing> {

    private List<EPurchasing> items;
    private final DataTablePagination.Request request;
    private int total = 0;
    private int totalItemInOnePage = 0;
    private static final String[] columns;
    private static final List<String> searchAbleFields;

    static {
        searchAbleFields = new ArrayList<>();
        searchAbleFields.add("tahun::TEXT");
        searchAbleFields.add("tanggal_update::TEXT");
        columns = new String[]{
                "id", "tahun", "tanggal_update"
        };
    }

    public EPurchasingResult(DataTablePagination.Request request) {
        this.request = request;
        executeQuery();
    }

    @Override
    public List<String> extractSearchAbleFields(Field[] fields) {
        return searchAbleFields;
    }

    @Override
    public JsonObject getResultsAsJson() {
        JsonObject result = getResponseTemplate();
        JsonArray jsonArray = new JsonArray();
        if (getTotal() == 0) {
            result.add("data", jsonArray);
            return result;
        }
        int i = getNumberingStartFrom();
        for (EPurchasing model : getResults()) {
            JsonArray array = new JsonArray();
            array.add(i);
            array.add(model.tahun);
            array.add(model.getFormattedTanggalUpdate());
            array.add(model.isAdaUpdate() ? "Ada" : "Tidak");
            array.add(model.id);
            jsonArray.add(array);
            i++;
        }
        result.add("data", jsonArray);
        LogUtil.debug(TAG, result);
        return result;
    }

    @Override
    public void setResult(List<EPurchasing> items) {
        this.items = items;
    }

    @Override
    public List<EPurchasing> getResults() {
        return items;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setTotalItemInOnePage(int totalItem) {
        this.totalItemInOnePage = totalItem;
    }

    @Override
    public int getTotalItemInOnePage() {
        return totalItemInOnePage;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public String[] getColumns() {
        return columns;
    }

    @Override
    public DataTablePagination.Request getRequest() {
        return request;
    }

    @Override
    public Class<EPurchasing> getReturnType() {
        return EPurchasing.class;
    }
}
