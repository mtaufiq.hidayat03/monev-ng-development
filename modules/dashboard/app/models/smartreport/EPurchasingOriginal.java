package models.smartreport;

import play.db.jdbc.Table;

@Table(name = "e_purchasing_original", schema = "smartreport")
public class EPurchasingOriginal extends BaseAdjustable {
    public static final String TAG = "EPurchasingOriginal";

    public Integer jumlah_paket;
    public Double nilai_transaksi;
    public Integer jumlah_kategori_komoditas;
    public Integer jumlah_penyedia;
    public Integer jumlah_produk;

}
