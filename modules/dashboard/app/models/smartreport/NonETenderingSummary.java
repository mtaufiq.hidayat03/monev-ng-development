package models.smartreport;

import org.joda.time.LocalDateTime;
import play.data.binding.As;
import play.db.jdbc.Table;
import utils.common.ExtractableChartQueryResult;
import utils.common.IndonesianDecimalBinder;
import utils.common.IndonesianNumberBinder;

import java.util.Date;

@Table(name = "non_e_tendering_summary", schema = "smartreport")
public class NonETenderingSummary extends BaseAdjustable implements ExtractableChartQueryResult {
    public static final String TAG = "NonETenderingSummary";

    @As(binder = IndonesianNumberBinder.class)
    public Integer jumlah_paket;
    @As(binder = IndonesianDecimalBinder.class)
    public Double nilai_pagu;
    @As(binder = IndonesianDecimalBinder.class)
    public Double nilai_transaksi;

    @Override
    public String getXAxisLabel() {
        return String.valueOf(tahun);
    }

    public boolean isAdaUpdate() {
        NonETenderingOriginalSummary originalSummary = NonETenderingOriginalSummary
                .find("tahun = ?", tahun)
                .first();

        return originalSummary != null
                && LocalDateTime.fromDateFields(tanggal_update)
                    .isBefore(LocalDateTime.fromDateFields(originalSummary.tanggal_update));
    }

    public void copy(NonETenderingSummary copy) {
        jumlah_paket = copy.jumlah_paket;
        nilai_pagu = copy.nilai_pagu;
        nilai_transaksi = copy.nilai_transaksi;
        tanggal_update = new Date();
    }

}
