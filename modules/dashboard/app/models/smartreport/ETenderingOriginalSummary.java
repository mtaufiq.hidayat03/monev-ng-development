package models.smartreport;

import play.db.jdbc.Table;

@Table(name = "e_tendering_original_summary", schema = "smartreport")
public class ETenderingOriginalSummary extends BaseAdjustable {
    public static final String TAG = "ETenderingOriginalSummary";

    public Integer jumlah_lelang;
    public Double pagu_lelang;
    public Integer jumlah_lelang_selesai;
    public Double pagu_lelang_selesai;
    public Double nilai_hasil_lelang;
    public Double selisih_pagu_hasil_lelang;
    public Double selisih_hps_hasil_lelang;
    public Integer jumlah_ppk;
    public Integer jumlah_panitia;
    public Integer penyedia_terdaftar;
    public Integer penyedia_terverifikasi;
    public Integer penyedia_tertolak;
    public Integer penyedia_terkoreksi;
    public Integer penyedia_terblacklist;
    public Double selisih_pagu_hasil_persen;
    public Double selisih_hps_hasil_persen;
}
