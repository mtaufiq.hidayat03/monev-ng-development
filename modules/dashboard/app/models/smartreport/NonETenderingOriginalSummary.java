package models.smartreport;

import play.db.jdbc.Table;

@Table(name = "non_e_tendering_original_summary", schema = "smartreport")
public class NonETenderingOriginalSummary extends BaseAdjustable {
    public static final String TAG = "NonETenderingOriginalSummary";

    public Integer jumlah_paket;
    public Double nilai_pagu;
    public Double nilai_transaksi;

}
