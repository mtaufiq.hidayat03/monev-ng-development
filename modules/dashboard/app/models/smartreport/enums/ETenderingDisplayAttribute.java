package models.smartreport.enums;

import java.util.Arrays;
import java.util.List;

public enum ETenderingDisplayAttribute {
    JUMLAH(
            false,
            Arrays.asList("jumlah_lelang", "jumlah_lelang_selesai"),
            Arrays.asList("Jumlah Pengumuman Paket Tender/Seleksi", "Jumlah Tender/Seleksi Selesai")
    ),
    NILAI(
            true,
            Arrays.asList("pagu_lelang", "pagu_lelang_selesai", "nilai_hasil_lelang"),
            Arrays.asList("Nilai Pagu", "Nilai Pagu Selesai", "Nilai Hasil Lelang")
    ),
    SELISIH_NILAI(
            true,
            Arrays.asList("selisih_pagu_hasil_lelang", "selisih_hps_hasil_lelang"),
            Arrays.asList("Selisih Pagu dengan Hasil Lelang", "Selisih HPS dengan Hasil Lelang")
    ),
    PERSEN_SELISIH_NILAI(
            false,
            Arrays.asList("selisih_pagu_hasil_persen", "selisih_hps_hasil_persen"),
            Arrays.asList("% Selisih Pagu dengan Hasil Lelang", "% Selisih HPS dengan Hasil Lelang")
    )/*,
    PENYELENGGARA(
            false,
            Arrays.asList("jumlah_ppk", "jumlah_panitia"),
            Arrays.asList("Jumlah PPK", "Jumlah Panitia")
    ),
    PENYEDIA(
            false,
            Arrays.asList(
                    "penyedia_terdaftar",
                    "penyedia_terverifikasi",
                    "penyedia_tertolak",
                    "penyedia_terkoreksi",
                    "penyedia_terblacklist"
            ),
            Arrays.asList("Terdaftar", "Terverifikasi", "Tertolak", "Terkoreksi", "Terblacklist")
    )*/;

    public boolean money;
    public List<String> attributes;
    public List<String> attributeLabels;

    ETenderingDisplayAttribute(boolean money, List<String> attributes, List<String> attributeLabels) {
        this.money = money;
        this.attributes = attributes;
        this.attributeLabels = attributeLabels;
    }
}
