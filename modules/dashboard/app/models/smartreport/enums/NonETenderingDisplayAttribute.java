package models.smartreport.enums;

import java.util.Arrays;
import java.util.List;

public enum NonETenderingDisplayAttribute {
    JUMLAH_PAKET(
            false,
            Arrays.asList("jumlah_paket"),
            Arrays.asList("Jumlah Paket")
    ),
    NILAI_PAGU(
            true,
            Arrays.asList("nilai_pagu"),
            Arrays.asList("Nilai Pagu")
    ),
    NILAI_TRANSAKSI(
            true,
            Arrays.asList("nilai_transaksi"),
            Arrays.asList("Nilai Transaksi")
    );

    public boolean money;
    public List<String> attributes;
    public List<String> attributeLabels;

    NonETenderingDisplayAttribute(boolean money, List<String> attributes, List<String> attributeLabels) {
        this.money = money;
        this.attributes = attributes;
        this.attributeLabels = attributeLabels;
    }
}
