package models.smartreport.enums;

import java.util.Arrays;
import java.util.List;

public enum EPurchasingDisplayAttribute {
    JUMLAH_PAKET(
            false,
            Arrays.asList("jumlah_paket"),
            Arrays.asList("Jumlah Paket")
    ),
    NILAI_TRANSAKSI(
            true,
            Arrays.asList("nilai_transaksi"),
            Arrays.asList("Nilai Transaksi")
    ),
    JUMLAH_KATEGORI(
            false,
            Arrays.asList("jumlah_kategori_komoditas"),
            Arrays.asList("Jumlah Kategori Komoditas")
    ),
    JUMLAH_PENYEDIA(
            false,
            Arrays.asList("jumlah_penyedia"),
            Arrays.asList("Jumlah Penyedia Berkontrak")
    ),
    JUMLAH_PRODUK(
            false,
            Arrays.asList("jumlah_produk"),
            Arrays.asList("Jumlah Produk")
    );

    public boolean money;
    public List<String> attributes;
    public List<String> attributeLabels;

    EPurchasingDisplayAttribute(boolean money, List<String> attributes, List<String> attributeLabels) {
        this.money = money;
        this.attributes = attributes;
        this.attributeLabels = attributeLabels;
    }
}
