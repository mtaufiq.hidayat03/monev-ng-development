package models.smartreport;

import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import utils.common.DateFormatter;

import javax.persistence.MappedSuperclass;
import java.util.Date;

@MappedSuperclass
public class BaseAdjustable extends BaseTable {

    @Id
    public Long id;
    public Integer tahun;
    public Date tanggal_update;

    @Override
    protected void prePersist() {
        if (this.id == null) {
            final String sequenceName = getClassSequence() + "_id_seq";
            id = Query.find("SELECT nextval('" + sequenceName + "'::regclass)", Long.class).first();
        }

    }

    public String getClassSequence() {
        final String name = getTableName();
        return name;
    }

    public String getFormattedTanggalUpdate() {
        if (tanggal_update == null) {
            return "";
        }
        return DateFormatter.format(tanggal_update, "dd-MM-yyyy HH:mm:ss");
    }

}
