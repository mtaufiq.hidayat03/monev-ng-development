package models.smartreport;

import org.joda.time.LocalDateTime;
import play.data.binding.As;
import play.db.jdbc.Table;
import utils.common.ExtractableChartQueryResult;
import utils.common.IndonesianDecimalBinder;
import utils.common.IndonesianNumberBinder;

import java.util.Date;

@Table(name = "e_tendering_summary", schema = "smartreport")
public class ETenderingSummary extends BaseAdjustable implements ExtractableChartQueryResult {
    public static final String TAG = "ETenderingSummary";

    @As(binder = IndonesianNumberBinder.class)
    public Integer jumlah_lelang = 0;
    @As(binder = IndonesianDecimalBinder.class)
    public Double pagu_lelang = 0d;
    @As(binder = IndonesianNumberBinder.class)
    public Integer jumlah_lelang_selesai = 0;
    @As(binder = IndonesianDecimalBinder.class)
    public Double pagu_lelang_selesai = 0d;
    @As(binder = IndonesianDecimalBinder.class)
    public Double nilai_hasil_lelang = 0d;
    @As(binder = IndonesianDecimalBinder.class)
    public Double selisih_pagu_hasil_lelang = 0d;
    @As(binder = IndonesianDecimalBinder.class)
    public Double selisih_hps_hasil_lelang = 0d;
    @As(binder = IndonesianNumberBinder.class)
    public Integer jumlah_ppk = 0;
    @As(binder = IndonesianNumberBinder.class)
    public Integer jumlah_panitia = 0;
    @As(binder = IndonesianNumberBinder.class)
    public Integer penyedia_terdaftar = 0;
    @As(binder = IndonesianNumberBinder.class)
    public Integer penyedia_terverifikasi = 0;
    @As(binder = IndonesianNumberBinder.class)
    public Integer penyedia_tertolak = 0;
    @As(binder = IndonesianNumberBinder.class)
    public Integer penyedia_terkoreksi = 0;
    @As(binder = IndonesianNumberBinder.class)
    public Integer penyedia_terblacklist = 0;
    @As(binder = IndonesianDecimalBinder.class)
    public Double selisih_pagu_hasil_persen = 0d;
    @As(binder = IndonesianDecimalBinder.class)
    public Double selisih_hps_hasil_persen = 0d;

    @Override
    public String getXAxisLabel() {
        return String.valueOf(tahun);
    }

    public boolean isAdaUpdate() {
        ETenderingOriginalSummary originalSummary = ETenderingOriginalSummary.find("tahun = ?", tahun).first();
        return originalSummary != null
                && LocalDateTime.fromDateFields(tanggal_update)
                    .isBefore(LocalDateTime.fromDateFields(originalSummary.tanggal_update));
    }

    public void copy(ETenderingSummary copy) {
        jumlah_lelang = copy.jumlah_lelang;
        pagu_lelang = copy.pagu_lelang;
        jumlah_lelang_selesai = copy.jumlah_lelang_selesai;
        pagu_lelang_selesai = copy.pagu_lelang_selesai;
        nilai_hasil_lelang = copy.nilai_hasil_lelang;
        selisih_pagu_hasil_lelang = copy.selisih_pagu_hasil_lelang;
        selisih_hps_hasil_lelang = copy.selisih_hps_hasil_lelang;
        selisih_pagu_hasil_persen = 100 * selisih_pagu_hasil_lelang / pagu_lelang_selesai;
        selisih_hps_hasil_persen = 100 * selisih_hps_hasil_lelang / pagu_lelang_selesai;
        jumlah_ppk = copy.jumlah_ppk;
        jumlah_panitia = copy.jumlah_panitia;
        tanggal_update = new Date();
    }

}
