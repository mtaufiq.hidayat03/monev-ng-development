package models.smartreport;

import models.common.ReadOnlyModel;
import play.db.jdbc.Table;
import utils.common.ExtractableChartQueryResult;

@Table(name = "e_tendering_detail", schema = "smartreport")
public class ETenderingDetail extends ReadOnlyModel implements ExtractableChartQueryResult {
    public static final String TAG = "ETenderingDetail";

    public Integer tahun;
    public String kode_lpse;
    public String nama_lpse;
    public Integer jumlah_lelang;
    public Double pagu_lelang;
    public Integer jumlah_lelang_selesai;
    public Double pagu_lelang_selesai;
    public Double nilai_hasil_lelang;
    public Double selisih_pagu_hasil_lelang;
    public Double selisih_hps_hasil_lelang;
    public Integer jumlah_ppk;
    public Integer jumlah_panitia;
    public Integer penyedia_terdaftar;
    public Integer penyedia_terverifikasi;
    public Integer penyedia_tertolak;
    public Integer penyedia_terkoreksi;
    public Integer penyedia_terblacklist;
    public Double selisih_pagu_hasil_persen;
    public Double selisih_hps_hasil_persen;

    @Override
    public String getXAxisLabel() {
        return nama_lpse;
    }

}
