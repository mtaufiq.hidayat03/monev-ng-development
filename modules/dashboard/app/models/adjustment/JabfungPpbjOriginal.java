package models.adjustment;

import models.smartreport.BaseAdjustable;
import play.data.binding.As;
import play.db.jdbc.Table;
import utils.common.IndonesianNumberBinder;

import java.util.Calendar;
import java.util.Date;

@Table(name = "adjustment_jabfung_ppbj_original", schema = "jabfung")
public class JabfungPpbjOriginal extends BaseAdjustable {

    public static final String TAG = "JabdungPpbjOriginal";

    @As(binder = IndonesianNumberBinder.class)
    public Long pertama;
    @As(binder = IndonesianNumberBinder.class)
    public Long muda;
    @As(binder = IndonesianNumberBinder.class)
    public Long madya;

}
