package models.adjustment;

import models.smartreport.BaseAdjustable;
import play.data.binding.As;
import play.db.jdbc.Table;
import utils.common.IndonesianDecimalBinder;

import java.util.Calendar;
import java.util.Date;

@Table(name = "adjustment_pdn_umkm_original", schema = "public")
public class PdnUmkmOriginal extends BaseAdjustable {

    public static final String TAG = "PdnUmkmOriginal";

    @As(binder = IndonesianDecimalBinder.class)
    public Double pdn;
    @As(binder = IndonesianDecimalBinder.class)
    public Double pagu_umkm;

}
