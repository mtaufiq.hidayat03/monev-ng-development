package models.adjustment;

import models.smartreport.BaseAdjustable;
import play.data.binding.As;
import play.db.jdbc.Table;
import utils.common.IndonesianNumberBinder;

@Table(name = "adjustment_pemegang_sertifikat_kompetensi_original", schema = "jabfung")
public class PemegangSertifikatKompetensiOriginal extends BaseAdjustable {

    public static final String TAG = "PemegangSertifikatKompetensiOriginal";

    @As(binder = IndonesianNumberBinder.class)
    public Long muda;
    @As(binder = IndonesianNumberBinder.class)
    public Long madya;
    @As(binder = IndonesianNumberBinder.class)
    public Long pertama;

}