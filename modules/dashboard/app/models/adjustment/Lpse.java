package models.adjustment;

import models.smartreport.BaseAdjustable;
import play.data.binding.As;
import play.db.jdbc.Table;
import utils.common.IndonesianNumberBinder;

import java.util.Calendar;
import java.util.Date;

@Table(name = "adjustment_lpse", schema = "public")
public class Lpse extends BaseAdjustable {

    public static final String TAG = "Lpse";

    @As(binder = IndonesianNumberBinder.class)
    public Long total;
    @As(binder = IndonesianNumberBinder.class)
    public Long jumlah_highlight_lpse;
    @As(binder = IndonesianNumberBinder.class)
    public Long standar;

    public void copy(Lpse copy) {
        total = copy.total;
        jumlah_highlight_lpse = copy.jumlah_highlight_lpse;
        standar = copy.standar;
        tanggal_update = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(tanggal_update);
        tahun = calendar.get(calendar.YEAR);
    }
}
