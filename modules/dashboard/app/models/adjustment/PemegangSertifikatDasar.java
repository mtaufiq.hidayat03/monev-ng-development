package models.adjustment;

import models.smartreport.BaseAdjustable;
import play.data.binding.As;
import play.db.jdbc.Table;
import utils.common.IndonesianNumberBinder;

import java.util.Calendar;
import java.util.Date;

@Table(name = "adjustment_sertifikat_dasar", schema = "jabfung")
public class PemegangSertifikatDasar extends BaseAdjustable {

    public static final String TAG = "PemegangSertifikatDasar";

    @As(binder = IndonesianNumberBinder.class)
    public Long total;

    public void copy(PemegangSertifikatDasar copy) {
        total = copy.total;
        tanggal_update = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(tanggal_update);
        tahun = calendar.get(calendar.YEAR);
    }

}