package models.adjustment;

import models.smartreport.BaseAdjustable;
import play.data.binding.As;
import play.db.jdbc.Table;
import utils.common.IndonesianNumberBinder;

@Table(name = "adjustment_lpse_original", schema = "public")
public class LpseOriginal extends BaseAdjustable {

    public static final String TAG = "Lpse";

    @As(binder = IndonesianNumberBinder.class)
    public Long total;
    @As(binder = IndonesianNumberBinder.class)
    public Long jumlah_highlight_lpse;
    @As(binder = IndonesianNumberBinder.class)
    public Long standar;

}
