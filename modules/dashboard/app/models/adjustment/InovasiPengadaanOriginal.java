package models.adjustment;

import models.smartreport.BaseAdjustable;
import play.data.binding.As;
import play.db.jdbc.Table;
import utils.common.IndonesianDecimalBinder;
import utils.common.IndonesianNumberBinder;

import java.util.Calendar;
import java.util.Date;

@Table(name = "adjustment_inovasi_pengadaan_original", schema = "public")
public class InovasiPengadaanOriginal extends BaseAdjustable {

    public static final String TAG = "InovasiPengadaanOriginal";

    @As(binder = IndonesianDecimalBinder.class)
    public Double lelang_dini;
    @As(binder = IndonesianDecimalBinder.class)
    public Double konsolidasi;
    @As(binder = IndonesianDecimalBinder.class)
    public Double tender_cepat;
    @As(binder = IndonesianDecimalBinder.class)
    public Double kontrak_payung;
    @As(binder = IndonesianNumberBinder.class)
    public Long tender_itemized;

}
