package models.adjustment;

import models.smartreport.BaseAdjustable;
import play.data.binding.As;
import play.db.jdbc.Table;
import utils.common.IndonesianNumberBinder;

@Table(name = "adjustment_sertifikat_dasar_original", schema = "jabfung")
public class PemegangSertifikatDasarOriginal extends BaseAdjustable {

    public static final String TAG = "PemegangSertifikatDasarOriginal";

    @As(binder = IndonesianNumberBinder.class)
    public Long total;

}