package models.adjustment;

import models.smartreport.BaseAdjustable;
import play.data.binding.As;
import play.db.jdbc.Table;
import utils.common.IndonesianNumberBinder;

import java.util.Calendar;
import java.util.Date;

@Table(name = "adjustment_pemegang_sertifikat_kompetensi", schema = "jabfung")
public class PemegangSertifikatKompetensi extends BaseAdjustable {

    public static final String TAG = "PemegangSertifikatKompetensi";

    @As(binder = IndonesianNumberBinder.class)
    public Long pertama;
    @As(binder = IndonesianNumberBinder.class)
    public Long muda;
    @As(binder = IndonesianNumberBinder.class)
    public Long madya;

    public void copy(PemegangSertifikatKompetensi copy) {
        pertama = copy.pertama;
        muda = copy.muda;
        madya = copy.madya;
        tanggal_update = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(tanggal_update);
        tahun = calendar.get(calendar.YEAR);
    }

    public Long getTotal() {
        return pertama + muda + madya;
    }

}