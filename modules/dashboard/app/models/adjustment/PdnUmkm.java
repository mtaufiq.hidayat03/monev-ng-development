package models.adjustment;

import models.smartreport.BaseAdjustable;
import play.data.binding.As;
import play.db.jdbc.Table;
import utils.common.IndonesianDecimalBinder;

import java.util.Calendar;
import java.util.Date;

@Table(name = "adjustment_pdn_umkm", schema = "public")
public class PdnUmkm extends BaseAdjustable {

    public static final String TAG = "PdnUmkm";

    @As(binder = IndonesianDecimalBinder.class)
    public Double pdn;
    @As(binder = IndonesianDecimalBinder.class)
    public Double pagu_umkm;

    public void copy(PdnUmkm copy) {
        pdn = copy.pdn;
        pagu_umkm = copy.pagu_umkm;
        tanggal_update = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(tanggal_update);
        tahun = calendar.get(calendar.YEAR);
    }
}
