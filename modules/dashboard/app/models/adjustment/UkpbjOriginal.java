package models.adjustment;

import models.smartreport.BaseAdjustable;
import play.data.binding.As;
import play.db.jdbc.Table;
import utils.common.IndonesianNumberBinder;

import java.util.Calendar;
import java.util.Date;

@Table(name = "adjustment_ukpbj_original", schema = "public")
public class UkpbjOriginal extends BaseAdjustable {

    public static final String TAG = "UkpbjOriginal";

    @As(binder = IndonesianNumberBinder.class)
    public Long struktural;
    @As(binder = IndonesianNumberBinder.class)
    public Long adhoc;
    @As(binder = IndonesianNumberBinder.class)
    public Long belum_terbentuk;
    @As(binder = IndonesianNumberBinder.class)
    public Long sembilan_kematangan;

}
