package models.adjustment;

import models.smartreport.BaseAdjustable;
import play.data.binding.As;
import play.db.jdbc.Table;
import utils.common.IndonesianNumberBinder;

import java.util.Calendar;
import java.util.Date;

@Table(name = "adjustment_advokasi", schema = "public")
public class Advokasi extends BaseAdjustable {

    public static final String TAG = "Advokasi";

    @As(binder = IndonesianNumberBinder.class)
    public Long layanan_advokasi;
    @As(binder = IndonesianNumberBinder.class)
    public Long kasus_pengadaan;
    @As(binder = IndonesianNumberBinder.class)
    public Long permasalahan_dilayani;

    public void copy(Advokasi copy) {
        layanan_advokasi = copy.layanan_advokasi;
        kasus_pengadaan = copy.kasus_pengadaan;
        permasalahan_dilayani = copy.permasalahan_dilayani;
        tanggal_update = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(tanggal_update);
        tahun = calendar.get(calendar.YEAR);
    }

}
