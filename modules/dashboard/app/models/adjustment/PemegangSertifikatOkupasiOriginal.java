package models.adjustment;

import models.smartreport.BaseAdjustable;
import play.data.binding.As;
import play.db.jdbc.Table;
import utils.common.IndonesianNumberBinder;

@Table(name = "adjustment_pemegang_sertifikat_okupasi_original", schema = "jabfung")
public class PemegangSertifikatOkupasiOriginal extends BaseAdjustable {

    public static final String TAG = "PemegangSertifikatOkupasiOriginal";

    @As(binder = IndonesianNumberBinder.class)
    public Long pokja_pemilihan;
    @As(binder = IndonesianNumberBinder.class)
    public Long pejabat_pengadaan;
    @As(binder = IndonesianNumberBinder.class)
    public Long pejabat_pembuat_komitmen;

}
