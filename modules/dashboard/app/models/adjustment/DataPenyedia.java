package models.adjustment;

import models.smartreport.BaseAdjustable;
import play.data.binding.As;
import play.db.jdbc.Table;
import utils.common.IndonesianNumberBinder;

import java.util.Calendar;
import java.util.Date;

@Table(name = "adjustment_data_penyedia", schema = "public")
public class DataPenyedia extends BaseAdjustable {

    public static final String TAG = "DataPenyedia";

    @As(binder = IndonesianNumberBinder.class)
    public Long terverifikasi;
    @As(binder = IndonesianNumberBinder.class)
    public Long terdaftar_sikap;
    @As(binder = IndonesianNumberBinder.class)
    public Long aktif;
    @As(binder = IndonesianNumberBinder.class)
    public Long menang;

    public void copy(DataPenyedia copy) {
        terverifikasi = copy.terverifikasi;
        terdaftar_sikap = copy.terdaftar_sikap;
        aktif = copy.aktif;
        menang = copy.menang;
        tanggal_update = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(tanggal_update);
        tahun = calendar.get(calendar.YEAR);
    }
}
