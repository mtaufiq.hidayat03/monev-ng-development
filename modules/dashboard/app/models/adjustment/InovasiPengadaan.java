package models.adjustment;

import models.smartreport.BaseAdjustable;
import play.data.binding.As;
import play.db.jdbc.Table;
import utils.common.IndonesianDecimalBinder;

import java.util.Calendar;
import java.util.Date;

@Table(name = "adjustment_inovasi_pengadaan", schema = "public")
public class InovasiPengadaan extends BaseAdjustable {

    public static final String TAG = "InovasiPengadaan";

    @As(binder = IndonesianDecimalBinder.class)
    public Double lelang_dini;
    @As(binder = IndonesianDecimalBinder.class)
    public Double konsolidasi;
    @As(binder = IndonesianDecimalBinder.class)
    public Double tender_cepat;
    @As(binder = IndonesianDecimalBinder.class)
    public Double kontrak_payung;
    @As(binder = IndonesianDecimalBinder.class)
    public Double tender_itemized;

    public void copy(InovasiPengadaan copy) {
        lelang_dini = copy.lelang_dini;
        konsolidasi = copy.konsolidasi;
        tender_cepat = copy.tender_cepat;
        kontrak_payung = copy.kontrak_payung;
        tender_itemized = copy.tender_itemized;
        tanggal_update = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(tanggal_update);
        tahun = calendar.get(calendar.YEAR);
    }
}
