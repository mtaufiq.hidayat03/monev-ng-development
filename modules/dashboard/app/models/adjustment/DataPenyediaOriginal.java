package models.adjustment;

import models.smartreport.BaseAdjustable;
import play.data.binding.As;
import play.db.jdbc.Table;
import utils.common.IndonesianNumberBinder;

import java.util.Calendar;
import java.util.Date;

@Table(name = "adjustment_data_penyedia_original", schema = "public")
public class DataPenyediaOriginal extends BaseAdjustable {

    public static final String TAG = "DataPenyedia";

    @As(binder = IndonesianNumberBinder.class)
    public Long terverifikasi;
    @As(binder = IndonesianNumberBinder.class)
    public Long terdaftar_sikap;
    @As(binder = IndonesianNumberBinder.class)
    public Long aktif;
    @As(binder = IndonesianNumberBinder.class)
    public Long menang;

}
