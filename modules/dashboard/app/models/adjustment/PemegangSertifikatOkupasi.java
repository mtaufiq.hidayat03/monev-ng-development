package models.adjustment;

import models.smartreport.BaseAdjustable;
import play.data.binding.As;
import play.db.jdbc.Table;
import utils.common.IndonesianNumberBinder;

import java.util.Calendar;
import java.util.Date;

@Table(name = "adjustment_pemegang_sertifikat_okupasi", schema = "jabfung")
public class PemegangSertifikatOkupasi extends BaseAdjustable {

    public static final String TAG = "PemegangSertifikatOkupasi";

    @As(binder = IndonesianNumberBinder.class)
    public Long pokja_pemilihan;
    @As(binder = IndonesianNumberBinder.class)
    public Long pejabat_pengadaan;
    @As(binder = IndonesianNumberBinder.class)
    public Long pejabat_pembuat_komitmen;

    public void copy(PemegangSertifikatOkupasi copy) {
        pokja_pemilihan = copy.pokja_pemilihan;
        pejabat_pengadaan = copy.pejabat_pengadaan;
        pejabat_pembuat_komitmen = copy.pejabat_pembuat_komitmen;
        tanggal_update = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(tanggal_update);
        tahun = calendar.get(calendar.YEAR);
    }

    public Long getTotal() {
        return pokja_pemilihan + pejabat_pengadaan + pejabat_pembuat_komitmen;
    }
}
