package models.adjustment;

import models.smartreport.BaseAdjustable;
import play.data.binding.As;
import play.db.jdbc.Table;
import utils.common.IndonesianNumberBinder;

import java.util.Calendar;
import java.util.Date;

@Table(name = "adjustment_ukpbj", schema = "public")
public class Ukpbj extends BaseAdjustable {

    public static final String TAG = "Ukpbj";

    @As(binder = IndonesianNumberBinder.class)
    public Long struktural;
    @As(binder = IndonesianNumberBinder.class)
    public Long adhoc;
    @As(binder = IndonesianNumberBinder.class)
    public Long belum_terbentuk;
    @As(binder = IndonesianNumberBinder.class)
    public Long sembilan_kematangan;

    public void copy(Ukpbj copy) {
        struktural = copy.struktural;
        adhoc = copy.adhoc;
        belum_terbentuk = copy.belum_terbentuk;
        sembilan_kematangan = copy.sembilan_kematangan;
        tanggal_update = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(tanggal_update);
        tahun = calendar.get(calendar.YEAR);
    }

    public Long getTotal() {
        return struktural + adhoc + belum_terbentuk;
    }
}
