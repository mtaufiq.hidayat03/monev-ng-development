package models.adjustment.enums;

import java.util.Arrays;
import java.util.List;

public enum AdjustmentDisplayAttribute {
    PEMEGANG_SERTIFIKAT_KOMPETENSI_JABFUNG(
            Arrays.asList("pemegang_sertifikat_kompetensi_jabfung"),
            Arrays.asList("Pemegang Sertifikat Kompetensi Jabfung")
    ),
    PEMEGANG_SERTIFIKAT_OKUPASI(
            Arrays.asList("pemegang_sertifikat_okupasi"),
            Arrays.asList("Pemegang Sertifikat Okupasi")
    ),
    PEMEGANG_SERTIFIKAT_DASAR(
            Arrays.asList("pemegang_sertifikat_dasar"),
            Arrays.asList("Pemegang Sertifikat Dasar")
    ),
    ADVOKASI(
            Arrays.asList("advokasi"),
            Arrays.asList("Advokasi")
    ),
    UKPBJ(
            Arrays.asList("ukpbj"),
            Arrays.asList("UKPBJ")
    ),
    JABFUNG_PPBJ(
            Arrays.asList("jabfung_ppbj"),
            Arrays.asList("JABFUNG PPBJ")
    ),
    LPSE(
            Arrays.asList("lpse"),
            Arrays.asList("LPSE")
    ),
    DATA_PENYEDIA(
            Arrays.asList("data_penyedia"),
            Arrays.asList("DATA PENYEDIA")
    ),
    INOVASI_PENGADAAN(
            Arrays.asList("inovasi_pengadaan"),
            Arrays.asList("INOVASI PENGADAAN")
    ),
    PDN_DAN_PERAN_SERTA_USAHA_KECIL(
            Arrays.asList("pdn_dan_peran_serta_usaha_kecil"),
            Arrays.asList("PDN & PERAN SERTA USAHA KECIL")
    )
    ;

    public List<String> attributes;
    public List<String> attributeLabels;

    AdjustmentDisplayAttribute(List<String> attributes, List<String> attributeLabels) {
        this.attributes = attributes;
        this.attributeLabels = attributeLabels;
    }

}
