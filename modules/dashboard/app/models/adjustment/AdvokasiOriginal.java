package models.adjustment;

import models.smartreport.BaseAdjustable;
import play.data.binding.As;
import play.db.jdbc.Table;
import utils.common.IndonesianNumberBinder;

@Table(name = "adjustment_advokasi_original", schema = "public")
public class AdvokasiOriginal extends BaseAdjustable {

    public static final String TAG = "AdvokasiOriginal";

    @As(binder = IndonesianNumberBinder.class)
    public Long layanan_advokasi;
    @As(binder = IndonesianNumberBinder.class)
    public Long kasus_pengadaan;
    @As(binder = IndonesianNumberBinder.class)
    public Long permasalahan_dilayani;

}
