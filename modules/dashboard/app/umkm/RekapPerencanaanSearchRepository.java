package umkm;

import umkm.models.PerencanaanMetodeDetail;

/**
 * @author HanusaCloud on 1/15/2020 10:16 AM
 */
public class RekapPerencanaanSearchRepository implements RekapMetodeSearchRepository<PerencanaanMetodeDetail> {

    @Override
    public Class<PerencanaanMetodeDetail> getReturnType() {
        return PerencanaanMetodeDetail.class;
    }

}
