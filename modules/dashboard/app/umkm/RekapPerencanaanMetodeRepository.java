package umkm;

import models.common.contracts.Chartable;
import models.dashboard.RekapPengadaanNasional;
import umkm.models.RekapPerencanaanMetode;
import umkm.models.SearchQuery;
import utils.common.ChartBuilder;

import java.time.Year;
import java.util.Arrays;
import java.util.List;

public class RekapPerencanaanMetodeRepository implements BaseRekapPerencanaanRepository<RekapPerencanaanMetode> {

    public static final String TAG = "RekapPerencanaanMetodeRepository";

    private static final RekapPerencanaanMetodeRepository repository = new RekapPerencanaanMetodeRepository();

    public static RekapPerencanaanMetodeRepository getInstance() {
        return repository;
    }

    public RekapPerencanaanMetode getLatest() {
        RekapPerencanaanMetode rekap = RekapPerencanaanMetode.find("1=1 order by tahun desc").first();
        if (rekap != null) {
            RekapPengadaanNasional rekapNasional = RekapPengadaanNasional.find("tahun = ?", rekap.tahun).first();
            if (rekapNasional != null) {
                rekap.paket_total = rekapNasional.rencana_paket;
                rekap.pagu_total = rekapNasional.rencana_pagu;
            }
        }
        return rekap;
    }

    public Chartable chartKonsolidasi() {
        // ambil label seriesnya
        List<String> seriesLabels = Arrays.asList("Kondolidasi", "Non Kondolidasi");
        // query
        RekapPerencanaanMetode rekap = getLatest();
        if (rekap == null) {
            rekap = new RekapPerencanaanMetode();
        }

        List<Number> data = Arrays.asList(rekap.pagu_konsolidasi,
                rekap.pagu_total- rekap.pagu_konsolidasi);
        // build and return chart
        return ChartBuilder.buildDonut("Pagu Kondolidasi", seriesLabels, data);
    }

    public List<RekapPerencanaanMetode> list10Years() {
        int tenYearAgo = Year.now().getValue() - 10;
        return RekapPerencanaanMetode.find("tahun > ? order by tahun desc", tenYearAgo).fetch();
    }

    @Override
    public Class<RekapPerencanaanMetode> getReturnType() {
        return RekapPerencanaanMetode.class;
    }

    public interface MetodeDetailsContract {

        String getQuery(SearchQuery searchQuery);

    }

}
