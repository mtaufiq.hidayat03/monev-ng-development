package umkm.models;

import play.db.jdbc.Table;

/**
 * @author HanusaCloud on 1/14/2020 9:58 AM
 */
@Table(name = "rekap_perencanaan_metode_detail", schema = "public")
public class PerencanaanMetodeDetail implements RekapMetodeDetail {

    public String kode;
    public String nama;
    public Double pagu_pdn;
    public Double paket_pdn;
    public Double pagu_umkm;
    public Double paket_umkm;

    @Override
    public String getKode() {
        return kode;
    }

    @Override
    public String getnNama() {
        return nama;
    }

    @Override
    public Double getPaguPdn() {
        return pagu_pdn;
    }

    @Override
    public Double getPaketPdn() {
        return paket_pdn;
    }

    @Override
    public Double getPaguUmkm() {
        return pagu_umkm;
    }

    @Override
    public Double getPaketUmkm() {
        return paket_umkm;
    }
}
