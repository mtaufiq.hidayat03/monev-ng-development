package umkm.models;

import models.dashboard.RekapPengadaanNasional;
import models.smartreport.BaseAdjustable;
import play.data.binding.As;
import play.db.jdbc.Table;
import utils.common.ExtractableChartQueryResult;
import utils.common.IndonesianNumberBinder;

import javax.persistence.Transient;

/**
 * @author idoej
 */
@Table(name = "rekap_perencanaan_metode", schema = "public")
public class RekapPerencanaanMetode extends BaseAdjustable implements ExtractableChartQueryResult, RekapMetode {

    public static final String TAG = "RekapPerencanaanMetode";

    @As(binder = IndonesianNumberBinder.class)
    public long pagu_konsolidasi;
    @As(binder = IndonesianNumberBinder.class)
    public int paket_konsolidasi;
    @As(binder = IndonesianNumberBinder.class)
    public long pagu_umkm;
    @As(binder = IndonesianNumberBinder.class)
    public int paket_umkm;
    @As(binder = IndonesianNumberBinder.class)
    public long pagu_pdn;
    @As(binder = IndonesianNumberBinder.class)
    public int paket_pdn;
    @Transient
    public int paket_total = 0;
    @Transient
    public long pagu_total = 0;

    public RekapPerencanaanMetode() {}

    public RekapPerencanaanMetode(RekapPengadaanNasional model) {
        this.paket_total = model.rencana_paket;
        this.pagu_total = model.rencana_pagu;
        this.tahun = model.tahun;
    }

    public static RekapPerencanaanMetode create(int tahun) {
        RekapPerencanaanMetode rekap = new RekapPerencanaanMetode();
        rekap.tahun = tahun;
        return rekap;
    }

    @Override
    public String getXAxisLabel() {
        return String.valueOf(tahun);
    }

    @Override
    public long getUmkmPagu() {
        return pagu_umkm;
    }

    @Override
    public long getPdnPagu() {
        return pagu_pdn;
    }

    @Override
    public long getPdnPaket() {
        return paket_pdn;
    }

    @Override
    public long getUmkmPaket() {
        return paket_umkm;
    }

    @Override
    public Integer getYear() {
        return tahun;
    }

    @Override
    public Long getTotalPagu() {
        return pagu_total;
    }

    @Override
    public void setTotalPagu(Long value) {
        this.pagu_total = value;
    }

    @Override
    public Integer getTotalPaket() {
        return paket_total;
    }

    @Override
    public void setTotalPaket(Integer value) {
        this.paket_total = value;
    }
}
