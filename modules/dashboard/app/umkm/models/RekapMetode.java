package umkm.models;

/**
 * @author HanusaCloud on 1/14/2020 1:46 PM
 */
public interface RekapMetode {

    Integer getYear();

    Long getTotalPagu();
    void setTotalPagu(Long value);
    Integer getTotalPaket();
    void setTotalPaket(Integer value);

    long getUmkmPagu();
    long getPdnPagu();
    long getPdnPaket();
    long getUmkmPaket();

    default long getNonUmkmPagu() {
        return getTotalPagu() > 0 ? getTotalPagu() - getUmkmPagu() : 0;
    }

    default long getNonPdnPagu() {
        return getTotalPagu() > 0 ? getTotalPagu() - getPdnPagu() : 0;
    }

}
