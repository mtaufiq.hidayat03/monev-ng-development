package umkm.models;

import org.apache.commons.lang3.StringUtils;
import play.mvc.Scope;

import java.util.ArrayList;
import java.util.List;

import static models.common.contracts.TablePagination.Param;
import static models.common.contracts.TablePagination.ParamAble;

/**
 * @author HanusaCloud on 1/14/2020 9:42 AM
 */
public class SearchQuery implements ParamAble {

    private Param[] params;
    private final String keyword;
    private final Integer year;
    private final int page;
    private final String kldi;

    public SearchQuery(Scope.Params scopeParams) {
        keyword = generateKeyword(scopeParams);
        year = getInteger(scopeParams, "year");
        page = getCurrentPage(scopeParams);
        kldi = getString(scopeParams, "kldi");
        List<Param> tempParamsList = new ArrayList<>();
        if (isYearExist()) {
            tempParamsList.add(new Param("year", year));
        }
        if (isKldiExist()) {
            tempParamsList.add(new Param("kldi", kldi));
        }
        Param[] tempParams = new Param[tempParamsList.size()];
        this.params = tempParamsList.toArray(tempParams);
    }

    @Override
    public Param[] getParams() {
        return params;
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public String getKeyword() {
        return keyword;
    }

    public boolean isYearExist() {
        return year != null && year > 0;
    }

    public Integer getYear() {
        return year;
    }

    public boolean isKldiExist() {
        return !StringUtils.isEmpty(kldi);
    }

    public String getKldi() {
        return kldi;
    }

    public String getYearAsString() {
        return isYearExist() ? getYear().toString() : "Semua Tahun";
    }

}
