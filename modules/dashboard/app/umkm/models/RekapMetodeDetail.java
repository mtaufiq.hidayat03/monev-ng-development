package umkm.models;

/**
 * @author HanusaCloud on 1/14/2020 2:10 PM
 */
public interface RekapMetodeDetail {

    String getKode();
    String getnNama();
    Double getPaguPdn();
    Double getPaketPdn();
    Double getPaguUmkm();
    Double getPaketUmkm();

}
