package umkm.models;

import play.db.jdbc.Table;

/**
 * @author HanusaCloud on 1/15/2020 11:00 AM
 */
@Table(name = "rekap_pemilihan_metode_detail", schema = "public")
public class PemilihanMetodeDetail implements RekapMetodeDetail {

    public String kode;
    public String nama;
    public Double pagu_pdn;
    public Double paket_pdn;
    public Double pagu_umkm;
    public Double paket_umkm;

    @Override
    public String getKode() {
        return kode;
    }

    @Override
    public String getnNama() {
        return nama;
    }

    @Override
    public Double getPaguPdn() {
        return pagu_pdn;
    }

    @Override
    public Double getPaketPdn() {
        return paket_pdn;
    }

    @Override
    public Double getPaguUmkm() {
        return pagu_umkm;
    }

    @Override
    public Double getPaketUmkm() {
        return paket_umkm;
    }

}
