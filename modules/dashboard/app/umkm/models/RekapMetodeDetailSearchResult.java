package umkm.models;

import models.common.contracts.TablePagination;

import java.util.List;

/**
 * @author HanusaCloud on 1/14/2020 9:54 AM
 */
public class RekapMetodeDetailSearchResult implements TablePagination.PageAble<RekapMetodeDetail> {

    private List<RekapMetodeDetail> collection;
    private final SearchQuery paramAble;
    private Integer total;

    public RekapMetodeDetailSearchResult(SearchQuery searchQuery) {
        paramAble = searchQuery;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public TablePagination.ParamAble getParamAble() {
        return paramAble;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setItems(List<RekapMetodeDetail> collection) {
        this.collection = collection;
    }

    @Override
    public List<RekapMetodeDetail> getItems() {
        return collection;
    }

    @Override
    public Class<RekapMetodeDetail> getReturnType() {
        return RekapMetodeDetail.class;
    }
}
