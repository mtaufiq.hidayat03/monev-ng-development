package umkm;

import models.common.contracts.QueryClassGenerator;
import play.db.jdbc.Query;
import umkm.models.RekapMetodeDetailSearchResult;
import umkm.models.SearchQuery;
import utils.common.LogUtil;

/**
 * @author HanusaCloud on 1/15/2020 10:10 AM
 */
public interface RekapMetodeSearchRepository<T> extends QueryClassGenerator {

    String TAG = "RekapMetodeSearchRepository";

    Class<T> getReturnType();

    default RekapMetodeDetailSearchResult getMetodeDetailsByKldi(SearchQuery searchQuery) {
        LogUtil.debug(TAG, "get by kldi");
        return getMetodeDetails(searchQuery, e -> {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT kode_kldi AS kode, " +
                    " nama_kldi AS nama," +
                    " SUM(paket_umkm) AS paket_umkm," +
                    " SUM(pagu_umkm) AS pagu_umkm," +
                    " SUM(paket_pdn) AS paket_pdn," +
                    " SUM(pagu_pdn) AS pagu_pdn" +
                    " FROM ").append(getTableName(getReturnType().getAnnotations()));
            sb.append(" WHERE 1=1");
            if (e.isKeywordExist()) {
                sb.append(" AND nama_kldi ILIKE '%").append(e.getKeyword()).append("%'");
            }
            if (e.isYearExist()) {
                sb.append(" AND tahun = '").append(e.getYear()).append("'");
            }
            sb.append(" GROUP BY kode_kldi, nama_kldi");
            return sb.toString();
        });
    }

    default RekapMetodeDetailSearchResult getMetodeDetailsBySatker(SearchQuery searchQuery) {
        return getMetodeDetails(searchQuery, e -> {
            StringBuilder sb = new StringBuilder();
            sb.append("SELECT kode_satker AS kode, " +
                    " nama_satker AS nama," +
                    " SUM(paket_umkm) AS paket_umkm," +
                    " SUM(pagu_umkm) AS pagu_umkm," +
                    " SUM(paket_pdn) AS paket_pdn," +
                    " SUM(pagu_pdn) AS pagu_pdn" +
                    " FROM ").append(getTableName(getReturnType().getAnnotations()));
            sb.append(" WHERE 1=1");
            if (e.isKeywordExist()) {
                sb.append(" AND nama_satker ILIKE '%").append(e.getKeyword()).append("%'");
            }
            if (e.isYearExist()) {
                sb.append(" AND tahun = '").append(e.getYear()).append("'");
            }
            if (e.isKldiExist()) {
                sb.append(" AND kode_kldi = '").append(e.getKldi()).append("'");
            }
            sb.append(" GROUP BY kode_satker, nama_satker");
            return sb.toString();
        });
    }

    default RekapMetodeDetailSearchResult getMetodeDetails(
            SearchQuery searchQuery,
            RekapPerencanaanMetodeRepository.MetodeDetailsContract contract
    ) {
        RekapMetodeDetailSearchResult result = new RekapMetodeDetailSearchResult(searchQuery);
        final String query = contract.getQuery(searchQuery);
        result.generateTotal(query);
        if (!result.isEmpty()) {
            result.setItems(Query.find(result.getLimitOffsetQuery(query), getReturnType()).fetch());
        }
        return result;
    }

}
