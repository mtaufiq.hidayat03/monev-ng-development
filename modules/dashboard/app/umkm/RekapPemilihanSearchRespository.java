package umkm;

import umkm.models.PemilihanMetodeDetail;

/**
 * @author HanusaCloud on 1/15/2020 11:03 AM
 */
public class RekapPemilihanSearchRespository implements RekapMetodeSearchRepository<PemilihanMetodeDetail> {

    @Override
    public Class<PemilihanMetodeDetail> getReturnType() {
        return PemilihanMetodeDetail.class;
    }

}
