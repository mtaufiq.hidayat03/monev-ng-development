package umkm;

import models.common.contracts.Chartable;
import models.common.contracts.QueryClassGenerator;
import models.dashboard.RekapPengadaanNasional;
import play.db.jdbc.Query;
import umkm.models.RekapMetode;
import umkm.models.SearchQuery;
import utils.common.LogUtil;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author HanusaCloud on 1/14/2020 2:18 PM
 */
public interface BaseRekapPerencanaanRepository<T extends RekapMetode> extends QueryClassGenerator {

    String BASE_TAG = "BaseRekapPerencanaanRepository";

    Class<T> getReturnType();

    default List<RekapMetode> getLatests(SearchQuery searchQuery) {
        StringBuilder sb = new StringBuilder();
        final Object[] result = getBareSelectQuery(getReturnType().getAnnotations());
        LogUtil.debug(BASE_TAG, result);
        sb.append(result[0]);
        sb.append("WHERE 1=1");
        if (searchQuery.isYearExist()) {
            sb.append(" AND tahun = '").append(searchQuery.getYear()).append("' ");
        }
        sb.append(" ORDER by tahun ASC");
        List<RekapMetode> results = Query.find(sb.toString(), getReturnType()).fetch();
        return getLatests(results);
    }

    default List<RekapMetode> getLatests(List<RekapMetode> rekaps) {
        if (!rekaps.isEmpty()) {
            return rekaps.stream().map(e -> {
                RekapPengadaanNasional rekapNasional = RekapPengadaanNasional.find("tahun = ?", e.getYear()).first();
                if (rekapNasional != null) {
                    e.setTotalPaket(rekapNasional.rencana_paket);
                    e.setTotalPagu(rekapNasional.rencana_pagu);
                }
                return e;
            }).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    default Chartable chartUmkm(SearchQuery searchQuery) {
        // ambil label seriesnya
        Chartable chartable = new Chartable();
        List<RekapMetode> rekaps = getLatests(searchQuery);
        //chartable.labels = new LinkedHashSet<>(seriesLabels);
        chartable.labels = rekaps.stream()
                .map(RekapMetode::getYear)
                .collect(Collectors.toCollection(LinkedHashSet::new));
        chartable.datasets.add(new Chartable.ChartItem(
                "UMKM",
                rekaps.stream().map(RekapMetode::getUmkmPagu).collect(Collectors.toList()),
                1));
        chartable.datasets.add(new Chartable.ChartItem(
                "Non UMKM",
                rekaps.stream().map(RekapMetode::getNonUmkmPagu).collect(Collectors.toList()),
                2));
        return chartable;
    }

    default Chartable chartPdn(SearchQuery searchQuery) {
        Chartable chartable = new Chartable();
        List<RekapMetode> rekaps = getLatests(searchQuery);
        chartable.labels = rekaps.stream()
                .map(RekapMetode::getYear)
                .collect(Collectors.toCollection(LinkedHashSet::new));
        chartable.datasets.add(new Chartable.ChartItem(
                "PDN",
                rekaps.stream().map(RekapMetode::getPdnPagu).collect(Collectors.toList()),
                1));
        chartable.datasets.add(new Chartable.ChartItem(
                "Non PDN",
                rekaps.stream().map(RekapMetode::getNonPdnPagu).collect(Collectors.toList()),
                2));
        return chartable;
    }

}
