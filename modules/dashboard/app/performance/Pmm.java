package performance;

import models.common.ReadOnlyModel;
import play.db.jdbc.Table;

/**
 * @author HanusaCloud on 12/21/2019 12:12 PM
 */
@Table(name = "pmm", schema = "public")
public class Pmm extends ReadOnlyModel {

    public static final String TAG = "Pmm";

    public String kode_kldi;
    public String nama_kldi;
    public Integer tahun;
    public Integer bulan;
    public Long jumlah_tepat_waktu;
    public Long jumlah_total;
    public Long nilai_tepat_waktu;
    public Long nilai_total;
    public Long pagu_pemilihan;
    public Long hasil_pemilihan;
    public Long jumlah_peserta;
    public Long nilai_tender_dini;
    public Long total_serah_terima_tepat_waktu;
    public Long total_serah_terima;

}
