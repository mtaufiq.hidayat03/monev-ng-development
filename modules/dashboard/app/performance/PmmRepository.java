package performance;

import models.common.contracts.Chartable;
import models.dashboard.enums.PmmChartDescriptionAttribute;
import org.apache.commons.lang3.StringUtils;
import permission.Kldi;
import play.db.jdbc.Query;
import repositories.user.management.KldiRepository;
import utils.common.LogUtil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author HanusaCloud on 12/21/2019 2:04 PM
 */
public class PmmRepository {

    public static final String TAG = "PmmRepository";

    private static final PmmRepository repository = new PmmRepository();

    public static PmmRepository getInstance() {
        return repository;
    }

    public List<Pmm> getAllKldi(int year) {
        return Query.find("SELECT nama_kldi, kode_kldi " +
                "FROM \"public\".\"pmm\" " +
                "WHERE (tahun >= ? OR tahun <= ?)" +
                "GROUP BY kode_kldi, nama_kldi;", Pmm.class, (year - 4), year).fetch();
    }

    public Object[] getAggregationQueryBy(String kldi, Integer year) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT " +
                "CASE WHEN SUM(p.jumlah_total) > 0 THEN ((SUM(p.jumlah_tepat_waktu) / SUM(p.jumlah_total)) * 100 ) " +
                "ELSE 0 END AS jumlah_percentage, " +
                "CASE WHEN SUM(p.nilai_total) > 0 THEN ((SUM(p.nilai_tepat_waktu) / SUM(p.nilai_total)) * 100 ) " +
                "ELSE 0 END AS nilai_percentage, " +
                "CASE WHEN SUM(p.pagu_pemilihan) > 0 THEN (((SUM(p.pagu_pemilihan) - " +
                "SUM(p.hasil_pemilihan)) / SUM(p.pagu_pemilihan)) * 100) " +
                "ELSE  0 END AS efficiency, " +
                "CASE WHEN SUM(p.jumlah_total) > 0 THEN ((SUM(p.jumlah_peserta)/ SUM(p.jumlah_total)) * 100) " +
                "ELSE 0 END AS partisipasi_percentage, " +
                "CASE WHEN SUM(p.nilai_total) > 0 THEN ((SUM(p.nilai_tender_dini)/ SUM(p.nilai_total)) * 100) " +
                "ELSE 0 END AS tender_dini_percentage, " +
                "CASE WHEN SUM(p.total_serah_terima) > 0 THEN ((SUM(p.total_serah_terima_tepat_waktu)/ SUM(p.total_serah_terima)) * 100) " +
                "ELSE 0 END AS kontrak_selesai_percentage, " +
                "p.tahun " +
                "FROM \"public\".\"pmm\" p ");
        sb.append("WHERE (p.tahun >= ? OR p.tahun <= ?) ");
        List<Object> params = new ArrayList<>();
        params.add((year - 4));
        params.add(year);
        if (!StringUtils.isEmpty(kldi)) {
            sb.append("AND p.kode_kldi = ? ");
            params.add(kldi);
        }
        sb.append("GROUP BY p.tahun ");
        if (!StringUtils.isEmpty(kldi)) {
            sb.append(", p.kode_kldi ");
        }
        sb.append("ORDER BY p.tahun ASC");
        final String query = sb.toString();
        LogUtil.debug(TAG, query);
        LogUtil.debug(TAG, params);
        return new Object[]{query, params};
    }

    public List<PmmAggregation> getAggregationBy(String kldi, Integer year) {
        LogUtil.debug(TAG, "get aggregation by kldi and year");
        Object[] aggregationQuery = getAggregationQueryBy(kldi, year);
        String query = (String) aggregationQuery[0];
        List<Object> params = (List<Object>) aggregationQuery[1];
        return Query.find(query, PmmAggregation.class, params.toArray()).fetch();
    }

    public Chartable getChartAbleAggregationBy(String kldi, Integer year, String type) {
        Chartable chartable = new Chartable();
        List<PmmAggregation> pmms = getAggregationBy(kldi, year);
        LogUtil.debug(TAG, pmms);
        int i = 1;
        List<Number> values = new ArrayList<>();
        for (PmmAggregation pmm : pmms) {
            values.add(pmm.getPercentageByKey(type).longValue());
            chartable.labels.add(pmm.tahun.toString());
            i++;
        }
        chartable.datasets.add(new Chartable.ChartItem("", values, i));
        return chartable;
    }

    public String getChartDescription(String kldi, String type) {
        String desc = "";
        List<PmmChartDescriptionAttribute> descriptions =
                Arrays.asList(PmmChartDescriptionAttribute.values());
        for (int i = 0; i < descriptions.size(); i++) {
            if (type.equals(descriptions.get(i).toString().replaceAll("_", " "))) {
                desc = descriptions.get(i).getDescription();
            }
        }
        if (kldi != null && !kldi.equals("")) {
            Kldi model = new KldiRepository().getById(kldi);
            desc += "di " + model.nama_instansi;
        }
        return desc;
    }

}
