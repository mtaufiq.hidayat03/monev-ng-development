package performance;

/**
 * @author HanusaCloud on 12/21/2019 2:32 PM
 */
public class PmmAggregation {

    public Double jumlah_percentage;
    public Double nilai_percentage;
    public Double efficiency;
    public Double partisipasi_percentage;
    public Double tender_dini_percentage;
    public Double kontrak_selesai_percentage;
    public Integer tahun;

    public Double getPercentageByKey(String value) {
        if (value.equals("jumlah")) {
            return jumlah_percentage;
        } else if (value.equals("nilai")) {
            return nilai_percentage;
        } else if (value.equals("efisiensi")) {
            return efficiency;
        } else if (value.equals("partisipasi penyedia")) {
            return partisipasi_percentage;
        } else if (value.equals("tender dini")) {
            return tender_dini_percentage;
        } else if (value.equals("kontrak selesai tepat waktu")) {
            return kontrak_selesai_percentage;
        }
        return 0D;
    }

}
