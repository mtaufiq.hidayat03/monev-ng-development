package controllers.dashboard;

import constants.generated.R;
import controllers.common.HtmlBaseController;
import models.common.contracts.Chartable;
import models.dashboard.DetailLpse;
import models.dashboard.pojo.DetailLpseResult;
import models.dashboard.queries.DetailLpseSearchQuery;
import utils.common.LogUtil;

import java.util.stream.Collectors;

public class RekapLpseController extends HtmlBaseController {

    public static final String TAG = "RekapLpseController";


    public static void index() {
        DetailLpseSearchQuery query = new DetailLpseSearchQuery(params);
        DetailLpseResult result = rekapLpseRepository.listDetail(query);
        Chartable standardisasiChart = rekapLpseRepository.standardisasiChart();
        Chartable versiSpseChart = rekapLpseRepository.versiSpseChart();
        LogUtil.debug(TAG, result);
        injectFilter();
        renderArgs.put("maps", rekapLpseRepository.getAllForMapBy(query)
                .stream()
                .map(DetailLpse::turnToValidJson)
                .collect(Collectors.toList())
        );
        renderTemplate(R.view.dashboard_lpse_index, result, query, standardisasiChart, versiSpseChart);
    }

    private static boolean injectFilter() {
        renderArgs.put("versiSpses", rekapLpseRepository.distinctVersiSpse());
        return true;
    }
}
