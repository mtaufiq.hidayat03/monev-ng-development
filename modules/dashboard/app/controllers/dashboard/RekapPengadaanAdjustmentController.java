package controllers.dashboard;

import constants.generated.R;
import controllers.user.management.BaseAdminController;
import models.dashboard.RekapPengadaanNasional;
import models.dashboard.RekapPengadaanNasionalOriginal;

import java.time.Year;

public class RekapPengadaanAdjustmentController extends BaseAdminController {

    public static void index() {

        renderTemplate(R.view.dashboard_rekap_pengadaan_adjustment_index);
    }

    public static void create() {
        int thisYear = Year.now().getValue();
        if (rekapPengadaanRepository.isExistsTahun(thisYear)) {
            flashError("rekap_pengadaan.data.exists");
            index();
        }
        RekapPengadaanNasional summary = RekapPengadaanNasional.create(thisYear);
        RekapPengadaanNasionalOriginal original = rekapPengadaanRepository.getOriginalByYear(summary.tahun);
        renderArgs.put("summary", summary);
        renderArgs.put("original", original);
        renderArgs.put("tahun", summary.tahun);

        renderTemplate(R.view.dashboard_rekap_pengadaan_adjustment_edit);
    }

    public static void adjust(Long id) {
        RekapPengadaanNasional summary = rekapPengadaanRepository.getNasional(id);
        RekapPengadaanNasionalOriginal original = rekapPengadaanRepository.getOriginalByYear(summary.tahun);
        renderArgs.put("summary", summary);
        renderArgs.put("original", original);
        renderArgs.put("tahun", summary.tahun);

        renderTemplate(R.view.dashboard_rekap_pengadaan_adjustment_edit);
    }

    public static void submit(Long id, RekapPengadaanNasional summaryEdit) {
        RekapPengadaanNasional summary = id == null
                ? RekapPengadaanNasional.create(Year.now().getValue())
                : rekapPengadaanRepository.getNasional(id);
        summary.copy(summaryEdit);
        summary.save();

        flashSuccess("rekap_pengadaan.adjust.success");
        index();

    }

}
