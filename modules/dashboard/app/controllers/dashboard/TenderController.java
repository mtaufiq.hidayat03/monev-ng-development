package controllers.dashboard;

import controllers.common.BaseController;
import models.dashboard.BelanjaPengadaan;
import repositories.dashboard.BelanjaPengadaanRepository;
import utils.common.JsonUtil;
import utils.common.LogUtil;

import java.util.List;

/**
 * @author HanusaCloud on 9/12/2019 1:43 PM
 */
public class TenderController extends BaseController {

    public static final String TAG = "TenderController";

    public static void belanjaPengadaan() {
        List<BelanjaPengadaan> results = BelanjaPengadaanRepository.getReport();
        LogUtil.debug(TAG, results);
        renderArgs.put("data", JsonUtil.toJson(results));
        renderTemplate("dashboard/belanja-pengadaan.html");
    }

    public static void toPdf() {
        renderTemplate("dashboard/to-pdf.html");
    }

}
