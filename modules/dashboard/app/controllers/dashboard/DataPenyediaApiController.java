package controllers.dashboard;

import constants.generated.R;
import controllers.common.HtmlBaseController;
import models.dashboard.queries.PaketPenyediaSearchQuery;

public class DataPenyediaApiController extends HtmlBaseController {

    public static void dataPenyedia() {
        renderArgs.put("result", rekapPenyediaRepository.listPenyediaStatus(new PaketPenyediaSearchQuery(params)));
        renderArgs.put("status", params.get("status"));
        renderTemplate(R.view.dashboard_lain_paket_penyedia);
    }

}
