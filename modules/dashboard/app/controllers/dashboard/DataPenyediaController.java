package controllers.dashboard;

import constants.generated.R;
import controllers.common.HtmlBaseController;
import models.dashboard.SearchQuery;
import models.dashboard.enums.DataPenyediaDisplayAttribute;
import models.dashboard.enums.header.HeaderTableDataPenyediaAttribute;
import models.dashboard.queries.RekapPenyediaSearchQuery;
import play.mvc.Before;

import java.util.Arrays;
import java.util.List;

public class DataPenyediaController extends HtmlBaseController {

    public static final String TAG = "DataPenyediaController";
    public static final List<String> menuLabels = Arrays.asList(
            "Statistik Keikutsertaan" ,
            "Sebaran Per Wilayah"
    );
    public static final List<String> menuUrls = Arrays.asList(
            R.route.dashboard_datapenyediacontroller_datapenyedia,
            R.route.dashboard_datapenyediacontroller_totalpenyedianasional
    );

    @Before
    public static void injectMenu() {
        renderArgs.put("menuLabels" , menuLabels);
        renderArgs.put("menuUrls" , menuUrls);
        renderArgs.put("currentAction" , request.action);
    }

    public static void index() {
        dataPenyedia();
    }

    public static void dataPenyedia() {
        DataPenyediaDisplayAttribute filter = params.get("filter", DataPenyediaDisplayAttribute.class);
        if (params.get("filter") == null) {
            filter = DataPenyediaDisplayAttribute.NILAI_MENANG_TERBANYAK;
        }
        renderArgs.put("filter", filter);
        renderArgs.put("filters", DataPenyediaDisplayAttribute.values());
        renderArgs.put("template" , R.view.dashboard_lain_data_penyedia);
        if (filter == DataPenyediaDisplayAttribute.NILAI_MENANG_TERBANYAK) {
            renderArgs.put("kontrakChart", rekapPenyediaRepository.kontrakTerbanyakChart());
        }
        if (filter == DataPenyediaDisplayAttribute.PAKET_MENANG_TERBANYAK) {
            renderArgs.put("paketChart", rekapPenyediaRepository.paketTerbanyakChart());
        }
        renderArgs.put("result" , rekapPenyediaRepository.list(new RekapPenyediaSearchQuery(params)));
        renderArgs.put("header" , HeaderTableDataPenyediaAttribute.values());
        renderTemplate(R.view.dashboard_lain_index);
    }

    public static void totalPenyediaNasional() {
        renderArgs.put("template" , R.view.dashboard_lain_total_penyedia_nasional);
        renderArgs.put("result" , totalPenyediaRepository.levelOne(new SearchQuery(params)));
        renderArgs.put("search" , params.get("search"));
        renderTemplate(R.view.dashboard_lain_index);
    }

    public static void totalPenyediaNasionalLevelTwo() {
        renderArgs.put("template" , R.view.dashboard_lain_total_penyedia_nasional_level_two);
        renderArgs.put("result" , totalPenyediaRepository.levelTwo(new SearchQuery(params)));
        renderArgs.put("currentAction" , R.route.dashboard_datapenyediacontroller_totalpenyedianasional); // hacky
        renderTemplate(R.view.dashboard_lain_index);
    }

}
