package controllers.dashboard;

import constants.generated.R;
import controllers.common.HtmlBaseController;
import models.dashboard.RekapSerahTerima;
import org.apache.commons.lang3.StringUtils;
import performance.Pmm;
import play.mvc.Before;
import utils.common.IdUtils;

import java.time.Year;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class InformasiLainController extends HtmlBaseController {

    public static final String TAG = "InformasiLainController";
    public static final List<String> menuLabels = Arrays.asList(
            "RUP Per Bulan",
            "Rekap Serah Terima",
            "Tender/Seleksi Gagal & Ulang",
            "PMM"
    );
    public static final List<String> menuUrls = Arrays.asList(
            R.route.dashboard_informasilaincontroller_perencanaanperbulan,
            R.route.dashboard_informasilaincontroller_serahterima,
            R.route.dashboard_informasilaincontroller_lelanggagal,
            R.route.dashboard_informasilaincontroller_pmm
    );


    private static final Map<String, String> PMM_HEADERS;

    static {
        PMM_HEADERS = new LinkedHashMap<>();
        PMM_HEADERS.put("Jumlah", "Proses Pengadaan Selesai Tepat Waktu - Jumlah");
        PMM_HEADERS.put("Nilai", "Proses Pengadaan Selesai Tepat Waktu - Nilai");
        PMM_HEADERS.put("Efisiensi", "Efisiensi");
        PMM_HEADERS.put("Partisipasi Penyedia", "Partisipasi Penyedia Barang/Jasa");
        PMM_HEADERS.put("Tender Dini", "Persentase Tender Dini");
        PMM_HEADERS.put("Kontrak Selesai Tepat Waktu", "Persentase Kontrak Selesai Tepat Waktu");
    }

    @Before
    public static void injectMenu() {
        renderArgs.put("menuLabels", menuLabels);
        renderArgs.put("menuUrls", menuUrls);
        renderArgs.put("currentAction", request.action);
    }

    public static void index() {
        perencanaanPerBulan();
    }

    public static void perencanaanPerBulan() {
        renderArgs.put("template", R.view.dashboard_lain_trend_pengumuman);
        renderArgs.put("rekapPerencanaanPerBulan", rekapPengadaanRepository.perencanaan1TahunChart());
        renderTemplate(R.view.dashboard_lain_index);
    }

    public static void serahTerima() {
        String year = params.get("year");
        RekapSerahTerima rekapSerahTerima = rekapSerahTerimaRepository.rekapSerahTerima();

        int thisYear = rekapSerahTerima.tahun_anggaran;
        if (year != null && IdUtils.isNumeric(year)) {
            thisYear = Integer.parseInt(year);
        }
        renderArgs.put("template", R.view.dashboard_lain_serah_terima);
        renderArgs.put("yearNow", thisYear);
        renderArgs.put("listYears", rekapSerahTerimaRepository.listYears());
        renderArgs.put("jenisBelanjaChart", rekapSerahTerimaRepository.chartJenisBelanja(thisYear, true));
        renderArgs.put("jenisPengadaanChart", rekapSerahTerimaRepository.chartJenisPengadaan(thisYear, true));
        renderArgs.put("metodePemilihanChart", rekapSerahTerimaRepository.chartMetodePemilihan(thisYear, true));
        renderArgs.put("jenisBelanjaChartPaket", rekapSerahTerimaRepository.chartJenisBelanja(thisYear, false));
        renderArgs.put("jenisPengadaanChartPaket", rekapSerahTerimaRepository.chartJenisPengadaan(thisYear, false));
        renderArgs.put("metodePemilihanChartPaket", rekapSerahTerimaRepository.chartMetodePemilihan(thisYear, false));
        renderArgs.put("yearlyChart", rekapSerahTerimaRepository.chartPerTahun());
        renderTemplate(R.view.dashboard_lain_index);
    }

    public static void lelangGagal() {
        renderArgs.put("template", R.view.dashboard_lain_gagal_ulang);
        renderArgs.put("gagalChart", rekapPemilihanMetodeRepository.chartGagalUlang());
        renderTemplate(R.view.dashboard_lain_index);
    }

    public static void pmm() {
        String kldi = params.get("kldi");
        final Integer year = Year.now().getValue();
        String type = params.get("type");
        if (StringUtils.isEmpty(type)) {
            type = "Jumlah";
        }
        List<Pmm> kldis = pmmRepository.getAllKldi(year);
        renderArgs.put("template", R.view.dashboard_lain_pmm_index);
        renderArgs.put("pmmTypes", PMM_HEADERS.keySet().toArray());
        renderArgs.put("type", type);
        renderArgs.put("chartTitle", PMM_HEADERS.get(type));
        renderArgs.put("pmmChart", pmmRepository.getChartAbleAggregationBy(kldi, year, type.toLowerCase()));
        renderArgs.put("chartDesc", pmmRepository.getChartDescription(kldi, type.toLowerCase()));
        renderArgs.put("kldis", kldis);
        renderArgs.put("kldi", kldi);
        if (!StringUtils.isEmpty(kldi) && !kldis.isEmpty()) {
            String kldiName = "";
            for (Pmm pmm : kldis) {
                if (kldi.equals(pmm.kode_kldi)) {
                    kldiName = pmm.nama_kldi;
                }
            }
            renderArgs.put("kldiName", kldiName);
        }
        renderTemplate(R.view.dashboard_lain_index);
    }

}
