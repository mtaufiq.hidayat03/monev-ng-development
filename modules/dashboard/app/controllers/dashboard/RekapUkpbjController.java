package controllers.dashboard;

import constants.generated.R;
import controllers.common.HtmlBaseController;
import models.common.contracts.Chartable;
import models.dashboard.RekapUkpbj;
import models.dashboard.pojo.RekapUkpbjResult;
import models.dashboard.queries.RekapUkpbjSearchQuery;
import utils.common.LogUtil;

import java.util.Arrays;

public class RekapUkpbjController extends HtmlBaseController {

    public static final String TAG = "RekapUkpbjController";


    public static void index() {
        RekapUkpbjSearchQuery query = new RekapUkpbjSearchQuery(params);
        RekapUkpbjResult result = rekapUkpbjRepository.list(query);
        Chartable kelembagaanChart = rekapUkpbjRepository.kelembagaanChart();
        Chartable kematanganChart = rekapUkpbjRepository.kematanganChart();
        Chartable perKldiChart = rekapUkpbjRepository.perKldiChart();
        LogUtil.debug(TAG, result);
        injectFilter();
        renderTemplate(R.view.dashboard_ukpbj_index, result, query, kelembagaanChart, kematanganChart, perKldiChart);
    }

    private static boolean injectFilter() {
//        renderArgs.put("kematangan", params.get("kematangan"));
//        renderArgs.put("kelembagaan", params.get("kelembagaan"));
        renderArgs.put("kelembagaans", RekapUkpbj.KELEMBAGAAN.values());
        renderArgs.put("kematangans", Arrays.asList(0,1,2,3,4,5,6,7,8,9));
        return true;
    }
}
