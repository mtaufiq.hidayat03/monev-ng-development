package controllers.dashboard;

import constants.generated.R;
import controllers.common.HtmlBaseController;
import models.dashboard.SearchQuery;
import ppsdm.CertificateHolderRepository;
import ppsdm.OccupationalCertificateRepository;
import utils.common.LogUtil;

public class OccupationalCertificateController extends HtmlBaseController {

    private static final OccupationalCertificateRepository repo = OccupationalCertificateRepository.getInstance();

    public static void levelOne() {
        renderArgs.put("result", repo.levelOne(new SearchQuery(params)));
        renderArgs.put("chartable", repo.levelOneChart(new SearchQuery(params)));
        renderTemplate(R.view.dashboard_jabfung_occupation_index);
    }

    public static void levelTwo(String kodeKldi, String satker, String namaKldi) {
        renderArgs.put("result", repo.levelTwo(new SearchQuery(params), kodeKldi, satker));
        renderArgs.put("satker", satker);
        renderArgs.put("kldi", namaKldi);
        renderTemplate(R.view.dashboard_jabfung_occupation_index_level_two);
    }
}
