package controllers.dashboard;

import constants.generated.R;
import controllers.common.HtmlBaseController;
import models.dashboard.enums.KualifikasiUsahaChartDisplayAttribute;
import models.dashboard.enums.KualifikasiUsahaDisplayAttribute;
import models.dashboard.queries.KualifikasiUsahaSearchQuery;
import play.mvc.Before;
import umkm.RekapPemilihanSearchRespository;
import umkm.RekapPerencanaanSearchRepository;
import umkm.models.SearchQuery;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class UmkmController extends HtmlBaseController {

    private static final RekapPerencanaanSearchRepository perencanaanSearchRepo = new RekapPerencanaanSearchRepository();
    private static final RekapPemilihanSearchRespository pemilihanSearchRepo = new RekapPemilihanSearchRespository();


    public static final String TAG = "UmkmController";
    public static final List<String> menuLabels = Arrays.asList(
            "Perencanaan UMKM dan PDN",
            "Pelaksanaan UMKM dan PDN",
            "Kualifikasi Usaha"
    );
    public static final List<String> menuUrls = Arrays.asList(
            R.route.dashboard_umkmcontroller_rencanametode,
            R.route.dashboard_umkmcontroller_pelaksanaanmetode,
            R.route.dashboard_umkmcontroller_kualifikasiusaha
    );

    @Before
    public static void injectMenu() {
        renderArgs.put("menuLabels", menuLabels);
        renderArgs.put("menuUrls", menuUrls);
        renderArgs.put("currentAction", request.action);
    }

    public static void index() {
        rencanaMetode();
    }

    public static void rencanaMetode() {
        SearchQuery searchQuery = new SearchQuery(params);
        renderArgs.put("template", R.view.dashboard_lain_rencana_metode);
        renderArgs.put("umkmChart", rekapPerencanaanMetodeRepository.chartUmkm(searchQuery));
        renderArgs.put("pdnChart", rekapPerencanaanMetodeRepository.chartPdn(searchQuery));
        renderArgs.put("years", rekapPerencanaanMetodeRepository.list10Years()
                .stream()
                .map(e -> e.tahun)
                .collect(Collectors.toList())
        );
        if (searchQuery.isKldiExist()) {
            renderArgs.put("searchResult", perencanaanSearchRepo.getMetodeDetailsBySatker(searchQuery));
        } else {
            renderArgs.put("searchResult", perencanaanSearchRepo.getMetodeDetailsByKldi(searchQuery));
        }
        renderTemplate(R.view.dashboard_lain_index);
    }

    public static void pelaksanaanMetode() {
        SearchQuery searchQuery = new SearchQuery(params);
        renderArgs.put("template", R.view.dashboard_lain_rekap_metode);
        renderArgs.put("umkmChart", rekapPemilihanMetodeRepository.chartUmkm(searchQuery));
        renderArgs.put("pdnChart", rekapPemilihanMetodeRepository.chartPdn(searchQuery));
        renderArgs.put("years", rekapPerencanaanMetodeRepository.list10Years()
                .stream()
                .map(e -> e.tahun)
                .collect(Collectors.toList())
        );
        if (searchQuery.isKldiExist()) {
            renderArgs.put("searchResult", pemilihanSearchRepo.getMetodeDetailsBySatker(searchQuery));
        } else {
            renderArgs.put("searchResult", pemilihanSearchRepo.getMetodeDetailsByKldi(searchQuery));
        }
        renderTemplate(R.view.dashboard_lain_index);
    }

    public static void kualifikasiUsaha() {
        Boolean isMoney = false;
        KualifikasiUsahaChartDisplayAttribute filter = params.get("chart",
                KualifikasiUsahaChartDisplayAttribute.class);
        if (params.get("chart") != null) {
            for (KualifikasiUsahaChartDisplayAttribute kualifikasiUsahaChartDisplayAttribute :
                    KualifikasiUsahaChartDisplayAttribute.values()) {
                if (params.get("chart").equals(kualifikasiUsahaChartDisplayAttribute.toString())) {
                    filter = kualifikasiUsahaChartDisplayAttribute;
                    isMoney = kualifikasiUsahaChartDisplayAttribute.isMoney();
                } else {
                    filter = null;
                }
            }
        }
        if (filter == null || params.get("chart") == null) {
            filter = KualifikasiUsahaChartDisplayAttribute.PAGU;
            isMoney = KualifikasiUsahaChartDisplayAttribute.PAGU.isMoney();
        }
        renderArgs.put("isMoney", isMoney);
        renderArgs.put("chart", filter);
        renderArgs.put("charts", KualifikasiUsahaChartDisplayAttribute.values());
        renderArgs.put("template", R.view.dashboard_lain_kualifikasi_usaha);
        renderArgs.put("kecilChart", kualifikasiUsahaRepository.kecilChart(filter.toString()));
        renderArgs.put("nonKecilChart", kualifikasiUsahaRepository.nonKecilChart(filter.toString()));
        renderArgs.put("kecilNonKecilChart", kualifikasiUsahaRepository.kecilNonKecilChart(filter.toString()));
        renderArgs.put("filters", KualifikasiUsahaDisplayAttribute.values());
        renderArgs.put("result", kualifikasiUsahaRepository.list(new KualifikasiUsahaSearchQuery(params)));
        renderTemplate(R.view.dashboard_lain_index);
    }

}
