package controllers.dashboard.procurementprofile;

import controllers.common.ApiBaseController;
import models.common.contracts.DataTableRequest;
import permission.Access;
import permission.Can;
import procurementprofile.ProfileDataTable;
import utils.common.LogUtil;

/**
 * @author HanusaCloud on 9/26/2019 11:11 AM
 */
public class ProfileApiController extends ApiBaseController {

    public static final String TAG = "ProfileApiController";

    @Can({Access.ADMIN_KL})
    public static void listJson() {
        LogUtil.debug(TAG, new  DataTableRequest(request));
        renderJSON(new ProfileDataTable(new DataTableRequest(request)).getResultsAsJson());
    }

}
