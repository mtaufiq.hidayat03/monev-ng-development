package controllers.dashboard.procurementprofile;

import constants.generated.R;
import controllers.user.management.BaseAdminController;
import models.common.contracts.CreationContract;
import org.apache.commons.lang3.StringUtils;
import permission.Can;
import play.Play;
import procurementprofile.models.ProcurementData;
import procurementprofile.models.ProcurementDataOriginal;
import procurementprofile.models.ProcurementProfile;
import procurementprofile.models.ProfileSection;
import utils.common.LogUtil;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Random;

import static permission.Access.*;
import static procurementprofile.models.ProcurementProfile.PublicationStatus.PUBLISHED;
import static procurementprofile.models.ProcurementProfile.PublicationStatus.DRAFT;

/**
 * @author HanusaCloud on 9/26/2019 10:47 AM
 */
public class ProfileController extends BaseAdminController {

    public static final String TAG = "ProfileController";

    private static List<Integer> PAGES = ProfileSection.getPages();

    @Can({ADMIN_KL, ADMIN_KONTEN})
    public static void index() {
        renderTemplate(R.view.dashboard_profile_list);
    }

    public static void insert() {
        ProcurementProfile model = new ProcurementProfile();
        model.mined_at = new Timestamp(System.currentTimeMillis());
        model.withDataOriginal();
        renderArgs.put("model", model);
        LogUtil.debug(TAG, PAGES);
        renderArgs.put("pages", PAGES);
        renderTemplate(R.view.dashboard_profile_input);
    }

    @Can({SUPER_ADMIN})
    public static void delete(Long id) {
        notFoundIfNull(id);
        ProcurementProfile profile = ProcurementProfile.findById(id);
        notFoundIfNull(profile);
        profile.deleteModel();
        flashSuccess(R.translation.procurement_profile_delete_success);
        index();
    }

    @Can({SUPER_ADMIN})
    public static void adjust(Long id) {
        notFoundIfNull(id);
        ProcurementProfile model = ProcurementProfile.findById(id);
        notFoundIfNull(model);
        model.withData();
        renderArgs.put("model", model);
        LogUtil.debug(TAG, PAGES);
        renderArgs.put("pages", PAGES);
        renderTemplate(R.view.dashboard_profile_input);
    }

    public static void storeAsDraft(Long id, ProcurementData[] list) {
        if (!store(id, list, DRAFT)) {
            flashError(R.translation.form_save_failed);
        }
        index();
    }

    public static void storeAsPublished(Long id, ProcurementData[] list) {
        if (!store(id, list, PUBLISHED)) {
            flashError(R.translation.form_save_failed);
        }
        index();
    }

    private static boolean store(Long id, ProcurementData[] list, ProcurementProfile.PublicationStatus status) {
        LogUtil.debug(TAG, list);
        ProcurementProfile profile;
        if (id == null) {
            profile = new ProcurementProfile();
            profile.mined_at = new Timestamp(System.currentTimeMillis());
            profile.title = "Profil Pengadaan " + new SimpleDateFormat(CreationContract.DATE_FORMAT)
                    .format(profile.mined_at);
        } else {
            profile = ProcurementProfile.findById(id);
        }
        profile.status = status;
        profile.saveModel();
        Integer latestVersion = ProcurementData.getLatestVersion(profile.id);
        LogUtil.debug(TAG, list);
        for (ProcurementData data : list) {
            data.procurement_id = profile.id;
            data.flag = latestVersion + 1;
            data.section = ProfileSection.valueOf(data.header.toUpperCase()).getGroup();
            data.save();
        }
        return true;
    }

    @Can({SUPER_ADMIN})
    public static void dummy() {
        if (Play.mode.isProd()) {
            index();
        }
        ProcurementProfile model = new ProcurementProfile();
        model.title = "Test procurement profile: " + System.currentTimeMillis();
        model.status = PUBLISHED;
        model.mined_at = new Timestamp(System.currentTimeMillis());
        model.saveModel();
        for (ProfileSection section : ProfileSection.values()) {
            new ProcurementData(model.id, 900L, section).save();
        }
        index();
    }

    @Can({SUPER_ADMIN})
    public static void dummyOriginal(String key) {
        if (StringUtils.isEmpty(key) || !key.equals("Pv2UBSbGtmhB3l4p6pan")) {
            index();
        }
        LogUtil.debug(TAG, "generate dummy original");
        for (ProfileSection section : ProfileSection.values()) {
            long LOWER_RANGE = 10; //assign lower range value
            long UPPER_RANGE = 10000000; //assign upper range value
            Random random = new Random();
            ProcurementDataOriginal original = new ProcurementDataOriginal(section);
            if (section.isText()) {
                original.value_text = "Test Text " + section.getName();
            } else {
                original.value = LOWER_RANGE +
                        (long)(random.nextDouble()*(UPPER_RANGE - LOWER_RANGE));
            }
            original.save();
        }
        index();
    }

}
