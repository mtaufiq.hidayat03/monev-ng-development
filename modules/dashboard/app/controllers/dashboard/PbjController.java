package controllers.dashboard;

import constants.generated.R;
import controllers.common.HtmlBaseController;
import models.dashboard.SearchQuery;
import ppsdm.JabfungRepository;

/**
 * @author HanusaCloud on 11/8/2019 9:09 AM
 */
public class PbjController extends HtmlBaseController {

    private static final JabfungRepository repo = JabfungRepository.getInstance();

    public static void levelOne() {
        renderArgs.put("result", repo.levelOne(new SearchQuery(params)));
        renderArgs.put("chartable", repo.levelOneChart(new SearchQuery(params)));
        renderTemplate(R.view.dashboard_jabfung_pbj_index);
    }

    public static void levelTwo(String kode_kldi, String satker, String nama_kldi) {
        renderArgs.put("result", repo.levelTwo(new SearchQuery(params), kode_kldi, satker));
        renderArgs.put("satker", satker);
        renderArgs.put("kldi", nama_kldi);
        renderTemplate(R.view.dashboard_jabfung_pbj_index_level_two);
    }

}
