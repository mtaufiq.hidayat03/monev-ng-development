package controllers.dashboard;

import constants.generated.R;
import controllers.common.HtmlBaseController;
import models.dashboard.enums.InovasiPengadaanDisplayAttribute;
import models.dashboard.enums.JenisKontrakDisplayAttribute;
import models.dashboard.enums.PemilihanMetodeDisplayAttribute;
import models.dashboard.queries.JenisKontrakSearchQuery;
import play.mvc.Before;

import java.util.Arrays;
import java.util.List;

public class InovasiPengadaanController extends HtmlBaseController {

    public static final String TAG = "InovasiPengadaanController";
    public static final List<String> menuLabels = Arrays.asList(
            "Inovasi Pengadaan",
            "Jenis Kontrak",
            "Perencanaan",
            "Pemilihan"
    );
    public static final List<String> menuUrls = Arrays.asList(
            R.route.dashboard_inovasipengadaancontroller_utama,
            R.route.dashboard_inovasipengadaancontroller_jeniskontrak,
            R.route.dashboard_inovasipengadaancontroller_rencanametode,
            R.route.dashboard_inovasipengadaancontroller_pelaksanaanmetode
    );

    @Before
    public static void injectMenu() {
        renderArgs.put("menuLabels", menuLabels);
        renderArgs.put("menuUrls", menuUrls);
        renderArgs.put("currentAction", request.action);
    }

    public static void index() {
        utama();
    }

    public static void utama() {
        InovasiPengadaanDisplayAttribute filter = params.get("filter", InovasiPengadaanDisplayAttribute.class);
        if (params.get("filter") == null) {
            filter = InovasiPengadaanDisplayAttribute.PAGU;
        }
        renderArgs.put("filter", filter);
        renderArgs.put("filters", InovasiPengadaanDisplayAttribute.values());
        renderArgs.put("template", R.view.dashboard_lain_inovasi);
        if (filter == InovasiPengadaanDisplayAttribute.PAGU) {
            renderArgs.put("chartPagu", inovasiPengadaanRepository.chartPagu());
        }
        if (filter == InovasiPengadaanDisplayAttribute.PAKET) {
            renderArgs.put("chartPaket", inovasiPengadaanRepository.chartPaket());
        }
        renderTemplate(R.view.dashboard_lain_index);
    }

    public static void rencanaMetode() {
        renderArgs.put("template", R.view.dashboard_lain_inovasi_perencanaan);
        renderArgs.put("konsolidasiChart", rekapPerencanaanMetodeRepository.chartKonsolidasi());
        renderArgs.put("result", rekapPerencanaanMetodeRepository.list10Years());
        renderTemplate(R.view.dashboard_lain_index);
    }

    public static void pelaksanaanMetode() {
        PemilihanMetodeDisplayAttribute filter = params.get("filter", PemilihanMetodeDisplayAttribute.class);
        if (params.get("filter") == null) {
            filter = PemilihanMetodeDisplayAttribute.KONSOLIDASI;
        }
        renderArgs.put("filter", filter);
        renderArgs.put("filters", PemilihanMetodeDisplayAttribute.values());
        renderArgs.put("template", R.view.dashboard_lain_inovasi_pemilihan);
        if (filter == PemilihanMetodeDisplayAttribute.KONSOLIDASI) {
            renderArgs.put("konsolidasiChart", rekapPemilihanMetodeRepository.chartKonsolidasi());
        }
        if (filter == PemilihanMetodeDisplayAttribute.ITEMIZE) {
            renderArgs.put("itemizeChart", rekapPemilihanMetodeRepository.chartItemize());
        }
        if (filter == PemilihanMetodeDisplayAttribute.TENDER_CEPAT) {
            renderArgs.put("expressChart", rekapPemilihanMetodeRepository.chartExpress());
        }
        if (filter == PemilihanMetodeDisplayAttribute.REVERSE_AUCTION) {
            renderArgs.put("reverseChart", rekapPemilihanMetodeRepository.chartReverse());
        }
        renderTemplate(R.view.dashboard_lain_index);
    }

    public static void jenisKontrak() {
        final JenisKontrakSearchQuery searchQuery = new JenisKontrakSearchQuery(params);
        renderArgs.put("template", R.view.dashboard_lain_jenis_kontrak);
        renderArgs.put("filters", JenisKontrakDisplayAttribute.values());
        JenisKontrakDisplayAttribute filter = params.get("filter", JenisKontrakDisplayAttribute.class);
        if (params.get("filter") == null) {
            filter = JenisKontrakDisplayAttribute.SEMUA;
        }
        renderArgs.put("filter", filter);
        renderArgs.put("result", jenisKontrakRepository.list(searchQuery, filter));
        renderArgs.put("years", jenisKontrakRepository.getYears());
        renderArgs.put("searchQuery", searchQuery);
        renderTemplate(R.view.dashboard_lain_index);
    }

}
