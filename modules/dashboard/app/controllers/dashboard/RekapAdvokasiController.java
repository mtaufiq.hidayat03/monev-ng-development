package controllers.dashboard;

import constants.generated.R;
import controllers.common.HtmlBaseController;
import models.common.contracts.Chartable;
import models.dashboard.RekapAdvokasi;

import java.util.List;

public class RekapAdvokasiController extends HtmlBaseController {

    public static final String TAG = "RekapUkpbjController";


    public static void index() {
        Chartable chart = rekapAdvokasiRepository.chart();
        List<RekapAdvokasi> result = rekapAdvokasiRepository.list();
        renderTemplate(R.view.dashboard_advokasi_index, chart, result);
    }

}
