package controllers.dashboard;

import controllers.common.ApiBaseController;
import models.common.contracts.DataTableRequest;
import permission.Access;
import permission.Can;
import utils.common.LogUtil;

public class RekapPengadaanAdjustmentApiController extends ApiBaseController {

    public static final String TAG = "RekapPengadaanAdjustmentApiController";

    @Can({Access.SUPER_ADMIN})
    public static void list() {
        DataTableRequest dataTableRequest = new DataTableRequest(request);
        LogUtil.debug(TAG, dataTableRequest);
        renderJSON(rekapPengadaanRepository.getRekapPengadaanResult(dataTableRequest).getResultsAsJson());
    }

    @Can({Access.SUPER_ADMIN})
    public static void get(Long id) {
        renderJSON(rekapPengadaanRepository.getNasional(id));
    }

}
