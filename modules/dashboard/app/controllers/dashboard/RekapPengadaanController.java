package controllers.dashboard;

import constants.generated.R;
import controllers.common.HtmlBaseController;
import models.dashboard.KldiQueryByTenderType;
import models.dashboard.QueryByTenderType;
import models.dashboard.SatkerQueryByTenderType;
import models.dashboard.SearchQuery;
import models.dashboard.pojo.JenisResult;
import models.dashboard.pojo.RekapPengadaanNasionalDetailResult;
import models.dashboard.pojo.RekapPengadaanResult;
import utils.DashboardLOVHelper;
import utils.common.LogUtil;

import java.time.Year;
import java.util.List;

public class RekapPengadaanController extends HtmlBaseController {

    public static final String TAG = "RekapPengadaanController";

    public static final String[] FILTERS = {
            "Jenis Belanja",
            "Metode Pemilihan",
            "Jenis Pengadaan"
    };
    public static final String[] FILTER_IDS = {
            "jenisBelanja",
            "metodePemilihan",
            "jenisPengadaan"
    };
    public static final List[] SUB_FILTERS = {
            DashboardLOVHelper.listJenisBelanja(),
            DashboardLOVHelper.listMetodePemilihan(),
            DashboardLOVHelper.listJenisPengadaan()
    };

    public static void index(Integer tahun) {
        if (tahun == null || tahun == 0) {
            List<String> tahuns = DashboardLOVHelper.listTahunAnggaran();
            tahun = tahuns.isEmpty() ? Year.now().getValue() : Integer.parseInt(tahuns.get(tahuns.size()-1));
        }
        params.put("tahun", tahun.toString());
        RekapPengadaanNasionalDetailResult result = rekapPengadaanRepository.listRootNasionalDetail(new SearchQuery(params));
        LogUtil.debug(TAG, result);
        injectFilter();
        renderTemplate(R.view.dashboard_rekap_pengadaan_index, result, tahun);
    }

    private static boolean injectFilter() {
       /*  renderArgs.put("filter", params.get("filter"));
        renderArgs.put("subFilter", params.get("subFilter"));
        renderArgs.put("filters", FILTERS);
        renderArgs.put("filterIds", FILTER_IDS);
        renderArgs.put("subFilters", SUB_FILTERS); */
        renderArgs.put("tahuns", DashboardLOVHelper.listTahunAnggaran());
        return true;
    }

    public static void anggaran(int tahun, String item) {
        String anggaran = item;
        params.put("anggaran", anggaran);
        RekapPengadaanNasionalDetailResult result = rekapPengadaanRepository.listAnggaranNasionalDetail(new SearchQuery(params));
        LogUtil.debug(TAG, result);
        injectFilter();
        renderTemplate(R.view.dashboard_rekap_pengadaan_anggaran, result, tahun, anggaran);
    }

    public static void tipe(int tahun, String anggaran, String item) {
        String tipe = item;
        params.put("tipe", tipe);
        RekapPengadaanNasionalDetailResult result = rekapPengadaanRepository.listKlpdNasionalDetail(new SearchQuery(params));
        LogUtil.debug(TAG, result);
        injectFilter();
        renderTemplate(R.view.dashboard_rekap_pengadaan_tipe, result, tahun, anggaran, tipe);
    }

    public static void klpd(int tahun, String anggaran, String tipe, String item) {
        String klpd = item;
        params.put("klpd", klpd);
        RekapPengadaanResult result = rekapPengadaanRepository.listSatker(new SearchQuery(params));
        LogUtil.debug(TAG, result);
        injectFilter();
        renderTemplate(R.view.dashboard_rekap_pengadaan_klpd, result, tahun, anggaran, tipe, klpd);
    }

    public static void byTenderTypeIndex() {
        QueryByTenderType query = new QueryByTenderType(params);
        injectFilter();
        renderArgs.put("paramAble", query);
        renderArgs.put("tenderType", rekapPengadaanRepository.getTenderTypes(query));
        if (query.isTypeExist()) {
            renderArgs.put("region", rekapPengadaanRepository.getByRegion(query));
        }
        if (query.isRegionExist()) {
            renderArgs.put("kldiType", rekapPengadaanRepository.getKldiType(query));
        }
        if (query.isKldiTypeExist()) {
            KldiQueryByTenderType kldiQuery = new KldiQueryByTenderType(params, query.getParams());
            JenisResult result = rekapPengadaanRepository.getKldiList(kldiQuery);
            renderArgs.put("kldi", result);
        }
        if (query.isKldiExist()) {
            SatkerQueryByTenderType satkerQueryByTenderType = new SatkerQueryByTenderType(params, query.getParams());
            renderArgs.put("satker", rekapPengadaanRepository.getSatkers(satkerQueryByTenderType));
        }
        renderTemplate(R.view.dashboard_tendertype_index);
    }

}
