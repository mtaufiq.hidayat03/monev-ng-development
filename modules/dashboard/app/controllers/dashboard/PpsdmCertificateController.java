package controllers.dashboard;

import constants.generated.R;
import controllers.common.HtmlBaseController;
import models.common.contracts.Chartable;
import models.dashboard.SearchQuery;
import ppsdm.CertificateHolderRepository;
import ppsdm.pojos.CertificateHolderSearchResult;

/**
 * @author HanusaCloud on 11/7/2019 12:02 PM
 */
public class PpsdmCertificateController extends HtmlBaseController {

    public static final String TAG = "PpsdmCertificateController";

    private static final CertificateHolderRepository repository = CertificateHolderRepository.getInstance();

    public static void levelOne() {
        CertificateHolderSearchResult result = repository.levelOne(new SearchQuery(params));
        Chartable chartable = repository.levelOneChart(new SearchQuery(params));
        renderArgs.put("result", result);
        renderArgs.put("chartable", chartable);
        renderTemplate(R.view.dashboard_jabfung_certificate_index);
    }

    public static void levelTwo(String kodeKldi, String satker, String namaKldi) {
        CertificateHolderSearchResult result = repository.levelTwo(new SearchQuery(params), kodeKldi, satker);
        renderArgs.put("result", result);
        renderArgs.put("satker", satker);
        renderArgs.put("kldi", namaKldi);
        renderTemplate(R.view.dashboard_jabfung_certificate_index_level_two);
    }

}
