package controllers.dashboard;

import constants.generated.R;
import controllers.common.HtmlBaseController;
import models.dashboard.SearchQuery;

public class BasicLevelCertificateHolderController extends HtmlBaseController {

    public static final String TAG = "BasicLevelCertificateHolderController";

    public static void levelOne() {
        renderArgs.put("result", basicLevelCertificateHolderRepository.levelOne(new SearchQuery(params)));
        renderArgs.put("chartable", basicLevelCertificateHolderRepository.levelOneChart(new SearchQuery(params)));
        renderTemplate(R.view.dashboard_jabfung_basic_index);
    }

    public static void levelTwo(String kode_kldi, String satker, String nama_kldi) {
        renderArgs.put("result", basicLevelCertificateHolderRepository.levelTwo(new SearchQuery(params), kode_kldi, satker));
        renderArgs.put("satker", satker);
        renderArgs.put("kldi", nama_kldi);
        renderTemplate(R.view.dashboard_jabfung_basic_index_level_two);
    }
}
