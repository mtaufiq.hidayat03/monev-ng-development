package controllers.smartreport;

import controllers.common.ApiBaseController;
import models.common.contracts.DataTableRequest;
import models.smartreport.EPurchasingResult;
import models.smartreport.NonETenderingResult;
import permission.Access;
import permission.Can;
import utils.common.LogUtil;

public class SmartReportAdjustmentApiController extends ApiBaseController {

    public static final String TAG = "SmartReportAdjustmentApiController";

    @Can({Access.SUPER_ADMIN})
    public static void eTendering() {
        DataTableRequest dataTableRequest = new DataTableRequest(request);
        LogUtil.debug(TAG, dataTableRequest);
        renderJSON(eTenderingSummaryRepository.getEtenderingResult(dataTableRequest).getResultsAsJson());
    }

    @Can({Access.SUPER_ADMIN})
    public static void eTenderingView(Long id) {
        renderJSON(eTenderingSummaryRepository.get(id));
    }

    @Can({Access.SUPER_ADMIN})
    public static void ePurchasing() {
        renderJSON(new EPurchasingResult(new DataTableRequest(request)).getResultsAsJson());
    }

    @Can({Access.SUPER_ADMIN})
    public static void ePurchasingView(Long id) {
        renderJSON(ePurchasingRepository.get(id));
    }

    @Can({Access.SUPER_ADMIN})
    public static void nonETendering() {
        renderJSON(new NonETenderingResult(new DataTableRequest(request)).getResultsAsJson());
    }

    @Can({Access.SUPER_ADMIN})
    public static void nonETenderingView(Long id) {
        renderJSON(nonETenderingSummaryRepository.get(id));
    }

}
