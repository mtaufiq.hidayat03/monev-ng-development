package controllers.smartreport;

import constants.generated.R;
import controllers.user.management.BaseAdminController;
import models.smartreport.*;

public class SmartReportAdjustmentController extends BaseAdminController {

    public static void eTendering() {

        renderTemplate(R.view.dashboard_smart_report_adjustment_e_tendering);
    }

    public static void eTenderingAdjust(Long id) {
        ETenderingSummary summary = eTenderingSummaryRepository.get(id);
        ETenderingOriginalSummary original = eTenderingSummaryRepository.getOriginalByYear(summary.tahun);
        renderArgs.put("summary", summary);
        renderArgs.put("original", original);
        renderArgs.put("tahun", summary.tahun);

        renderTemplate(R.view.dashboard_smart_report_adjustment_e_tendering_edit);
    }

    public static void eTenderingSubmit(Long id, ETenderingSummary summaryEdit) {
        ETenderingSummary summary = eTenderingSummaryRepository.get(id);
        summary.copy(summaryEdit);
        summary.save();

        flashSuccess("smartreport.adjust.success");
        eTendering();

    }

    public static void ePurchasing() {
        renderArgs.put("result", ePurchasingRepository.listLast10Years());

        renderTemplate(R.view.dashboard_smart_report_adjustment_e_purchasing);
    }

    public static void ePurchasingAdjust(Long id) {
        EPurchasing summary = ePurchasingRepository.get(id);
        EPurchasingOriginal original = ePurchasingRepository.getOriginalByYear(summary.tahun);
        renderArgs.put("summary", summary);
        renderArgs.put("original", original);
        renderArgs.put("tahun", summary.tahun);

        renderTemplate(R.view.dashboard_smart_report_adjustment_e_purchasing_edit);
    }

    public static void ePurchasingSubmit(Long id, EPurchasing summaryEdit) {
        EPurchasing summary = ePurchasingRepository.get(id);
        summary.copy(summaryEdit);
        summary.save();

        flashSuccess("smartreport.adjust.success");
        ePurchasing();

    }

    public static void nonETendering() {
        renderArgs.put("result", nonETenderingSummaryRepository.listLast10Years());

        renderTemplate(R.view.dashboard_smart_report_adjustment_non_e_tendering);
    }

    public static void nonETenderingAdjust(Long id) {
        NonETenderingSummary summary = nonETenderingSummaryRepository.get(id);
        NonETenderingOriginalSummary original = nonETenderingSummaryRepository.getOriginalByYear(summary.tahun);
        renderArgs.put("summary", summary);
        renderArgs.put("original", original);
        renderArgs.put("tahun", summary.tahun);

        renderTemplate(R.view.dashboard_smart_report_adjustment_non_e_tendering_edit);
    }

    public static void nonETenderingSubmit(Long id, NonETenderingSummary summaryEdit) {
        NonETenderingSummary summary = nonETenderingSummaryRepository.get(id);
        summary.copy(summaryEdit);
        summary.save();

        flashSuccess("smartreport.adjust.success");
        nonETendering();

    }
}
