package controllers.smartreport;

import constants.generated.R;
import controllers.common.HtmlBaseController;
import models.common.contracts.Chartable;
import models.smartreport.enums.EPurchasingDisplayAttribute;
import models.smartreport.enums.ETenderingDisplayAttribute;
import models.smartreport.enums.NonETenderingDisplayAttribute;
import models.smartreport.queries.EPurchasingDetailSearchQuery;
import models.smartreport.queries.ETenderingDetailSearchQuery;
import models.smartreport.queries.NonETenderingDetailSearchQuery;

import java.util.Arrays;
import java.util.List;

public class SmartReportController extends HtmlBaseController {

    public static void index() {
        String report = params.get("report");
        if (report == null || report.equals("e-tendering")) {
            eTendering();
        }

        if (report.equals("e-purchasing")) {
            ePurchasing();
        }

        if (report.equals("non-e-tendering")) {
            nonETendering();
        }
    }

    private static void renderArgs(List result,
                                   Chartable chartable,
                                   List<String> attributes,
                                   List<String> attributeLabels,
                                   List filters,
                                   Object filter, String report) {
        renderArgs.put("result", result);
        renderArgs.put("chartable", chartable);
        renderArgs.put("attributes", attributes);
        renderArgs.put("labels", attributeLabels);
        renderArgs.put("filters", filters);
        renderArgs.put("filter", filter);
        renderArgs.put("report", report);
    }

    public static void eTendering() {
        ETenderingDisplayAttribute filter = params.get("filter", ETenderingDisplayAttribute.class);
        if (params.get("filter") == null) {
            filter = ETenderingDisplayAttribute.JUMLAH;
        }
        renderArgs(eTenderingSummaryRepository.listLast10Years(),
                eTenderingSummaryRepository.chart(filter),
                filter.attributes,
                filter.attributeLabels,
                Arrays.asList(ETenderingDisplayAttribute.values()),
                filter,
                "e-tendering");

        renderTemplate(R.view.dashboard_smart_report_e_tendering);
    }

    public static void eTenderingDetail(ETenderingDisplayAttribute filter, int tahun) {
        if (filter == null) {
            filter = ETenderingDisplayAttribute.JUMLAH;
        }
        renderArgs.put("result", eTenderingDetailRepository.list(new ETenderingDetailSearchQuery(params), tahun, filter));
        renderArgs.put("chartable", eTenderingDetailRepository.chart(tahun, filter));
        renderArgs.put("attributes", filter.attributes);
        renderArgs.put("labels", filter.attributeLabels);
        renderArgs.put("filters", ETenderingDisplayAttribute.values());
        renderArgs.put("filter", filter);
        renderArgs.put("report", "e-tendering");
        renderArgs.put("tahun", tahun);
        renderTemplate(R.view.dashboard_smart_report_e_tendering_detail);
    }

    public static void ePurchasing() {
        EPurchasingDisplayAttribute filter = params.get("filter", EPurchasingDisplayAttribute.class);
        if (params.get("filter") == null) {
            filter = EPurchasingDisplayAttribute.JUMLAH_PAKET;
        }
        renderArgs(ePurchasingRepository.listLast10Years(),
                ePurchasingRepository.chart(filter),
                filter.attributes,
                filter.attributeLabels,
                Arrays.asList(EPurchasingDisplayAttribute.values()),
                filter,
                "e-purchasing");
        renderTemplate(R.view.dashboard_smart_report_e_purchasing);
    }

    public static void ePurchasingDetail(EPurchasingDisplayAttribute filter, int tahun) {
        if (filter == null) {
            filter = EPurchasingDisplayAttribute.JUMLAH_PAKET;
        }
        renderArgs.put("result", ePurchasingDetailRepository.list(new EPurchasingDetailSearchQuery(params), tahun, filter));
        renderArgs.put("chartable", ePurchasingDetailRepository.chart(tahun, filter));
        renderArgs.put("attributes", filter.attributes);
        renderArgs.put("labels", filter.attributeLabels);
        renderArgs.put("filters", EPurchasingDisplayAttribute.values());
        renderArgs.put("filter", filter);
        renderArgs.put("report", "e-purchasing");
        renderArgs.put("tahun", tahun);
        renderTemplate(R.view.dashboard_smart_report_e_purchasing_detail);
    }

    public static void nonETendering() {
        NonETenderingDisplayAttribute filter = params.get("filter", NonETenderingDisplayAttribute.class);
        if (params.get("filter") == null) {
            filter = NonETenderingDisplayAttribute.JUMLAH_PAKET;
        }
        renderArgs(nonETenderingSummaryRepository.listLast10Years(),
                nonETenderingSummaryRepository.chart(filter),
                filter.attributes,
                filter.attributeLabels,
                Arrays.asList(NonETenderingDisplayAttribute.values()),
                filter,
                "non-e-tendering");
        renderArgs.put("result", nonETenderingSummaryRepository.listLast10Years());
        renderArgs.put("chartable", nonETenderingSummaryRepository.chart(filter));
        renderArgs.put("attributes", filter.attributes);
        renderArgs.put("labels", filter.attributeLabels);
        renderArgs.put("filters", NonETenderingDisplayAttribute.values());
        renderTemplate(R.view.dashboard_smart_report_non_e_tendering);
    }

    public static void nonETenderingDetail(NonETenderingDisplayAttribute filter, int tahun) {
        if (filter == null) {
            filter = NonETenderingDisplayAttribute.JUMLAH_PAKET;
        }
        renderArgs.put("result", nonETenderingDetailRepository.list(new NonETenderingDetailSearchQuery(params), tahun, filter));
        renderArgs.put("chartable", nonETenderingDetailRepository.chart(tahun, filter));
        renderArgs.put("attributes", filter.attributes);
        renderArgs.put("labels", filter.attributeLabels);
        renderArgs.put("filters", NonETenderingDisplayAttribute.values());
        renderArgs.put("filter", filter);
        renderArgs.put("report", "non-e-tendering");
        renderArgs.put("tahun", tahun);
        renderTemplate(R.view.dashboard_smart_report_non_e_tendering_detail);
    }
}
