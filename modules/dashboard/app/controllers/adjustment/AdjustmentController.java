package controllers.adjustment;

import constants.generated.R;
import controllers.user.management.BaseAdminController;
import models.adjustment.*;
import models.adjustment.enums.AdjustmentDisplayAttribute;
import permission.Can;
import utils.common.LogUtil;

import static permission.Access.SUPER_ADMIN;

public class AdjustmentController extends BaseAdminController {


    @Can({SUPER_ADMIN})
    public static void index() {
        String filter = params.get("filter");
        LogUtil.debug(TAG, AdjustmentDisplayAttribute.PEMEGANG_SERTIFIKAT_OKUPASI);
        if (filter == null ||
                filter.equals(AdjustmentDisplayAttribute.PEMEGANG_SERTIFIKAT_KOMPETENSI_JABFUNG.toString())) {
            sertifikatKompetensiJabfung();
        }

        if (filter.equals(AdjustmentDisplayAttribute.PEMEGANG_SERTIFIKAT_OKUPASI.toString())) {
            sertifikatKompetensiOkupasi();
        }

        if (filter.equals(AdjustmentDisplayAttribute.PEMEGANG_SERTIFIKAT_DASAR.toString())) {
            sertifikatKompetensiDasar();
        }

        if (filter.equals(AdjustmentDisplayAttribute.ADVOKASI.toString())) {
            advokasi();
        }

        if (filter.equals(AdjustmentDisplayAttribute.UKPBJ.toString())) {
            ukpbj();
        }

        if (filter.equals(AdjustmentDisplayAttribute.JABFUNG_PPBJ.toString())) {
            jabfungPpbj();
        }

        if (filter.equals(AdjustmentDisplayAttribute.LPSE.toString())) {
            lpse();
        }

        if (filter.equals(AdjustmentDisplayAttribute.DATA_PENYEDIA.toString())) {
            dataPenyedia();
        }

        if (filter.equals(AdjustmentDisplayAttribute.INOVASI_PENGADAAN.toString())) {
            inovasiPengadaan();
        }

        if (filter.equals(AdjustmentDisplayAttribute.PDN_DAN_PERAN_SERTA_USAHA_KECIL.toString())) {
            pdnPeranSertaUsahaKecil();
        }
    }

    @Can({SUPER_ADMIN})
    public static void sertifikatKompetensiJabfung() {
        AdjustmentDisplayAttribute filter = AdjustmentDisplayAttribute.PEMEGANG_SERTIFIKAT_KOMPETENSI_JABFUNG;
        PemegangSertifikatKompetensi summary = adjustmentRepository.getSummaryPemegangSertifikatKompetensi();
        PemegangSertifikatKompetensiOriginal original =
                adjustmentRepository.getOriginalPemegangSertifikatKompetensi();
        renderArgs.put("filters", AdjustmentDisplayAttribute.values());
        renderArgs.put("summary", summary);
        renderArgs.put("original", original);
        renderTemplate(R.view.adjustment_pemegang_sertifikat_kompetensi, filter);
    }

    @Can({SUPER_ADMIN})
    public static void sertifikatKompetensiJabfungSubmit(PemegangSertifikatKompetensi summaryEdit) {
        PemegangSertifikatKompetensi summary = adjustmentRepository.getSummaryPemegangSertifikatKompetensi();
        if (summary == null) {
            summary = new PemegangSertifikatKompetensi();
        }
        summary.copy(summaryEdit);
        summary.save();
        flashSuccess("adjustment.adjust.success");
        sertifikatKompetensiJabfung();
    }

    @Can({SUPER_ADMIN})
    public static void sertifikatKompetensiOkupasi() {
        AdjustmentDisplayAttribute filter = AdjustmentDisplayAttribute.PEMEGANG_SERTIFIKAT_OKUPASI;
        PemegangSertifikatOkupasi summary = adjustmentRepository.getSummaryPemegangSertifikatOkupasi();
        PemegangSertifikatOkupasiOriginal original =
                adjustmentRepository.getOriginalPemegangSertifikatOkupasi();
        renderArgs.put("filters", AdjustmentDisplayAttribute.values());
        renderArgs.put("summary", summary);
        renderArgs.put("original", original);
        renderTemplate(R.view.adjustment_pemegang_sertifikat_okupasi, filter);
    }

    @Can({SUPER_ADMIN})
    public static void sertifikatKompetensiOkupasiSubmit(PemegangSertifikatOkupasi summaryEdit) {
        PemegangSertifikatOkupasi summary = adjustmentRepository.getSummaryPemegangSertifikatOkupasi();
        if (summary == null) {
            summary = new PemegangSertifikatOkupasi();
        }
        summary.copy(summaryEdit);
        summary.save();
        flashSuccess("adjustment.adjust.success");
        sertifikatKompetensiOkupasi();
    }

    @Can({SUPER_ADMIN})
    public static void sertifikatKompetensiDasar() {
        AdjustmentDisplayAttribute filter = AdjustmentDisplayAttribute.PEMEGANG_SERTIFIKAT_DASAR;
        PemegangSertifikatDasar summary = adjustmentRepository.getSummaryPemegangSertifikatDasar();
        PemegangSertifikatDasarOriginal original =
                adjustmentRepository.getOriginalPemegangSertifikatDasar();
        renderArgs.put("filters", AdjustmentDisplayAttribute.values());
        renderArgs.put("summary", summary);
        renderArgs.put("original", original);
        renderTemplate(R.view.adjustment_pemegang_sertifikat_dasar, filter);
    }

    @Can({SUPER_ADMIN})
    public static void sertifikatKompetensiDasarSubmit(PemegangSertifikatDasar summaryEdit) {
        PemegangSertifikatDasar summary = adjustmentRepository.getSummaryPemegangSertifikatDasar();
        if (summary == null) {
            summary = new PemegangSertifikatDasar();
        }
        summary.copy(summaryEdit);
        summary.save();
        flashSuccess("adjustment.adjust.success");
        sertifikatKompetensiDasar();
    }

    @Can({SUPER_ADMIN})
    public static void advokasi() {
        AdjustmentDisplayAttribute filter = AdjustmentDisplayAttribute.ADVOKASI;
        Advokasi summary = adjustmentRepository.getSummaryAdvokasi();
        AdvokasiOriginal original =
                adjustmentRepository.getOriginalAdvokasi();
        renderArgs.put("filters", AdjustmentDisplayAttribute.values());
        renderArgs.put("summary", summary);
        renderArgs.put("original", original);
        renderTemplate(R.view.adjustment_advokasi, filter);
    }

    @Can({SUPER_ADMIN})
    public static void advokasiSubmit(Advokasi summaryEdit) {
        Advokasi summary = adjustmentRepository.getSummaryAdvokasi();
        if (summary == null) {
            summary = new Advokasi();
        }
        summary.copy(summaryEdit);
        summary.save();
        flashSuccess("adjustment.adjust.success");
        advokasi();
    }

    @Can({SUPER_ADMIN})
    public static void ukpbj() {
        AdjustmentDisplayAttribute filter = AdjustmentDisplayAttribute.UKPBJ;
        Ukpbj summary = adjustmentRepository.getSummaryUkpbj();
        UkpbjOriginal original =
                adjustmentRepository.getOriginalUkpbj();
        renderArgs.put("filters", AdjustmentDisplayAttribute.values());
        renderArgs.put("summary", summary);
        renderArgs.put("original", original);
        renderTemplate(R.view.adjustment_ukpbj, filter);
    }

    @Can({SUPER_ADMIN})
    public static void ukpbjSubmit(Ukpbj summaryEdit) {
        Ukpbj summary = adjustmentRepository.getSummaryUkpbj();
        if (summary == null) {
            summary = new Ukpbj();
        }
        summary.copy(summaryEdit);
        summary.save();
        flashSuccess("adjustment.adjust.success");
        ukpbj();
    }

    @Can({SUPER_ADMIN})
    public static void jabfungPpbj() {
        AdjustmentDisplayAttribute filter = AdjustmentDisplayAttribute.JABFUNG_PPBJ;
        JabfungPpbj summary = adjustmentRepository.getSummaryJabfungPpbj();
        JabfungPpbjOriginal original =
                adjustmentRepository.getOriginalJabfungPpbj();
        renderArgs.put("filters", AdjustmentDisplayAttribute.values());
        renderArgs.put("summary", summary);
        renderArgs.put("original", original);
        renderTemplate(R.view.adjustment_jabfung_ppbj, filter);
    }

    @Can({SUPER_ADMIN})
    public static void jabfungPpbjSubmit(JabfungPpbj summaryEdit) {
        JabfungPpbj summary = adjustmentRepository.getSummaryJabfungPpbj();
        if (summary == null) {
            summary = new JabfungPpbj();
        }
        summary.copy(summaryEdit);
        summary.save();
        flashSuccess("adjustment.adjust.success");
        jabfungPpbj();
    }

    @Can({SUPER_ADMIN})
    public static void lpse() {
        AdjustmentDisplayAttribute filter = AdjustmentDisplayAttribute.LPSE;
        Lpse summary = adjustmentRepository.getSummaryLpse();
        LpseOriginal original =
                adjustmentRepository.getOriginalLpse();
        renderArgs.put("filters", AdjustmentDisplayAttribute.values());
        renderArgs.put("summary", summary);
        renderArgs.put("original", original);
        renderTemplate(R.view.adjustment_lpse, filter);
    }

    @Can({SUPER_ADMIN})
    public static void lpseSubmit(Lpse summaryEdit) {
        Lpse summary = adjustmentRepository.getSummaryLpse();
        if (summary == null) {
            summary = new Lpse();
        }
        summary.copy(summaryEdit);
        summary.save();
        flashSuccess("adjustment.adjust.success");
        lpse();
    }

    @Can({SUPER_ADMIN})
    public static void dataPenyedia() {
        AdjustmentDisplayAttribute filter = AdjustmentDisplayAttribute.DATA_PENYEDIA;
        DataPenyedia summary = adjustmentRepository.getSummaryDataPenyedia();
        DataPenyediaOriginal original =
                adjustmentRepository.getOriginalDataPenyedia();
        renderArgs.put("filters", AdjustmentDisplayAttribute.values());
        renderArgs.put("summary", summary);
        renderArgs.put("original", original);
        renderTemplate(R.view.adjustment_data_penyedia, filter);
    }

    @Can({SUPER_ADMIN})
    public static void dataPenyediaSubmit(DataPenyedia summaryEdit) {
        DataPenyedia summary = adjustmentRepository.getSummaryDataPenyedia();
        if (summary == null) {
            summary = new DataPenyedia();
        }
        summary.copy(summaryEdit);
        summary.save();
        flashSuccess("adjustment.adjust.success");
        dataPenyedia();
    }

    @Can({SUPER_ADMIN})
    public static void inovasiPengadaan() {
        AdjustmentDisplayAttribute filter = AdjustmentDisplayAttribute.INOVASI_PENGADAAN;
        InovasiPengadaan summary = adjustmentRepository.getSummaryInovasiPengadaan();
        InovasiPengadaanOriginal original =
                adjustmentRepository.getOriginalInovasiPengadaan();
        renderArgs.put("filters", AdjustmentDisplayAttribute.values());
        renderArgs.put("summary", summary);
        renderArgs.put("original", original);
        renderTemplate(R.view.adjustment_inovasi_pengadaan, filter);
    }

    @Can({SUPER_ADMIN})
    public static void inovasiPengadaanSubmit(InovasiPengadaan summaryEdit) {
        InovasiPengadaan summary = adjustmentRepository.getSummaryInovasiPengadaan();
        if (summary == null) {
            summary = new InovasiPengadaan();
        }
        summary.copy(summaryEdit);
        summary.save();
        flashSuccess("adjustment.adjust.success");
        inovasiPengadaan();
    }

    @Can({SUPER_ADMIN})
    public static void pdnPeranSertaUsahaKecil() {
        AdjustmentDisplayAttribute filter = AdjustmentDisplayAttribute.PDN_DAN_PERAN_SERTA_USAHA_KECIL;
        PdnUmkm summary = adjustmentRepository.getSummaryPdnPeranSertaUsahaKecil();
        PdnUmkmOriginal original =
                adjustmentRepository.getOriginalPdnPeranSertaUsahaKecil();
        renderArgs.put("filters", AdjustmentDisplayAttribute.values());
        renderArgs.put("summary", summary);
        renderArgs.put("original", original);
        renderTemplate(R.view.adjustment_pdn_peran_serta_usaha_kecil, filter);
    }

    @Can({SUPER_ADMIN})
    public static void pdnPeranSertaUsahaKecilSubmit(PdnUmkm summaryEdit) {
        PdnUmkm summary = adjustmentRepository.getSummaryPdnPeranSertaUsahaKecil();
        if (summary == null) {
            summary = new PdnUmkm();
        }
        summary.copy(summaryEdit);
        summary.save();
        flashSuccess("adjustment.adjust.success");
        pdnPeranSertaUsahaKecil();
    }

}
