package services.search;

import repositories.searchcontent.GeneralSearchRepository;
import repositories.searchcontent.KeywordRecorderRepository;
import utils.common.LogUtil;

/**
 * @author HanusaCloud on 11/15/2019 4:02 PM
 */
public class ElasticIndexingService {

    public static final String TAG = "IndexingService";

    private static final ElasticIndexingService service = new ElasticIndexingService();

    private static final GeneralSearchRepository repository = GeneralSearchRepository.getInstance();
    private static final KeywordRecorderRepository keywordRepository = KeywordRecorderRepository.getInstance();

    public static ElasticIndexingService getInstance() {
        return service;
    }

    public void reMapping() {
        LogUtil.debug(TAG, "start remapping");
        repository.reIndexing();
        keywordRepository.initMapping();
    }

}
