package services.search;

import models.searchcontent.ElasticRequest;
import repositories.cms.ContentRepository;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author HanusaCloud on 1/7/2020 11:24 AM
 */
public class ContentIndexingService implements PagingIndexer {

    private static final ContentRepository contentRepository = ContentRepository.getInstance();

    private final Long total;

    public ContentIndexingService() {
        total = contentRepository.count();
        startIndexing();
    }

    @Override
    public Long getTotal() {
        return total;
    }

    @Override
    public Integer getPerPage() {
        return 50;
    }

    @Override
    public List<ElasticRequest> getRequests(Long offset, Integer limit) {
        return contentRepository.getContents(offset, limit)
                .stream()
                .map(ElasticRequest::new)
                .collect(Collectors.toList());
    }
}
