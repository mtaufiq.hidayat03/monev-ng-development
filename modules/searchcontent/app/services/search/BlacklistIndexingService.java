package services.search;

import models.Blacklist;
import models.searchcontent.ElasticRequest;
import repositories.core.BlacklistRepository;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author HanusaCloud on 1/7/2020 11:17 AM
 */
public class BlacklistIndexingService implements PagingIndexer {

    private static final BlacklistRepository blacklistRepository = BlacklistRepository.getInstance();

    private final Long total;

    public BlacklistIndexingService() {
        total = blacklistRepository.count();
        startIndexing();
    }

    @Override
    public Long getTotal() {
        return total;
    }

    @Override
    public Integer getPerPage() {
        return 50;
    }

    @Override
    public List<ElasticRequest> getRequests(Long offset, Integer limit) {
        return blacklistRepository.paginate(offset, limit)
                .stream()
                .map(Blacklist.Data::new)
                .map(ElasticRequest::new)
                .collect(Collectors.toList());
    }
}
