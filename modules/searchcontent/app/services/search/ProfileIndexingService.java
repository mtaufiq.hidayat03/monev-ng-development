package services.search;

import models.searchcontent.ElasticRequest;
import procurementprofile.ProfileRepository;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author HanusaCloud on 1/7/2020 11:29 AM
 */
public class ProfileIndexingService implements PagingIndexer {

    private static final ProfileRepository repository = ProfileRepository.getInstance();

    private final Long total;

    public ProfileIndexingService() {
        total = repository.count();
        startIndexing();
    }

    @Override
    public Long getTotal() {
        return total;
    }

    @Override
    public Integer getPerPage() {
        return 10;
    }

    @Override
    public List<ElasticRequest> getRequests(Long offset, Integer limit) {
        return repository.getProfiles(offset, limit)
                .stream()
                .map(ElasticRequest::new)
                .collect(Collectors.toList());
    }
}
