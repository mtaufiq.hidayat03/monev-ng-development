package services.search;

import models.searchcontent.ElasticRequest;
import repositories.searchcontent.GeneralSearchRepository;
import utils.common.LogUtil;

import java.util.List;

/**
 * @author HanusaCloud on 1/7/2020 11:20 AM
 */
public interface PagingIndexer {

    String TAG = "PagingIndexer";

    Long getTotal();
    List<ElasticRequest> getRequests(Long offset, Integer limit);

    default Integer getPerPage() {
        return 70;
    }

    default Integer getTotalPage() {
        LogUtil.debug(TAG, "total: " + getTotal());
        final int totalPage = (int) Math.ceil(getTotal() * 1.0 / getPerPage() * 1.0);
        LogUtil.debug(TAG, "total page: " + totalPage);
        return totalPage;
    }

    default void startIndexing() {
        if (getTotal() == null || getTotal() == 0) {
            LogUtil.debug(TAG, "Oops total is empty");
            return;
        }
        for (int i = 1; i <= getTotalPage(); i++) {
            List<ElasticRequest> requests = getRequests((i - 1) * getTotal(), getPerPage());
            try {
                GeneralSearchRepository.getInstance().bulk(requests);
            } catch (Exception e) {
                LogUtil.error(TAG, e);
            }
        }
    }

}
