package jobs.searchcontent;

import play.jobs.Job;
import services.search.ProfileIndexingService;
import utils.common.LogUtil;

/**
 * @author HanusaCloud on 1/7/2020 8:59 AM
 */
public class ElasticProcurementJob extends Job {

    public static final String TAG = "ElasticProcurementJob";

    @Override
    public void doJob() throws Exception {
        super.doJob();
        LogUtil.debug(TAG, "elastic procurement is running");
        new ProfileIndexingService();
    }
}
