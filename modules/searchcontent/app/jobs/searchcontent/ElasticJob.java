package jobs.searchcontent;

import play.jobs.Job;
import play.jobs.On;
import play.libs.F;
import utils.common.LogUtil;

/**
 * @author HanusaCloud on 1/7/2020 8:59 AM
 */
@On("0 0 23 * * ?")
public class ElasticJob extends Job {

    public static final String TAG = "ElasticJob";

    @Override
    public void doJob() throws Exception {
        super.doJob();
        LogUtil.debug(TAG, "elastic search job activated");
        F.Promise.waitAll(
                new ElasticContentJob().now(),
                new ElasticProcurementJob().now(),
                new ElasticBlacklistJob().now()
        );
    }
}
