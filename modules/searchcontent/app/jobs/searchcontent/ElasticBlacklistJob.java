package jobs.searchcontent;

import play.jobs.Job;
import services.search.BlacklistIndexingService;
import utils.common.LogUtil;

/**
 * @author HanusaCloud on 1/7/2020 8:58 AM
 */
public class ElasticBlacklistJob extends Job {

    public static final String TAG = "ElasticBlacklistJob";

    @Override
    public void doJob() throws Exception {
        super.doJob();
        LogUtil.debug(TAG, "elastic blacklist is running");
        new BlacklistIndexingService();
    }
}
