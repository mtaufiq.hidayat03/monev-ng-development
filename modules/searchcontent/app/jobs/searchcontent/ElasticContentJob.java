package jobs.searchcontent;

import play.jobs.Job;
import services.search.ContentIndexingService;
import utils.common.LogUtil;

/**
 * @author HanusaCloud on 11/14/2019 3:53 PM
 */
public class ElasticContentJob extends Job {

    public static final String TAG = "ElasticContentJob";

    @Override
    public void doJob() throws Exception {
        super.doJob();
        LogUtil.debug(TAG, "Elastic content job is running");
        new ContentIndexingService();
    }

}
