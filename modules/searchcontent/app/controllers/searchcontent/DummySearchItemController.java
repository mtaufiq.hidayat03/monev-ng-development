package controllers.searchcontent;

import constants.generated.R;
import controllers.common.HtmlBaseController;
import jobs.searchcontent.ElasticJob;
import models.searchcontent.SearchQueryParams;
import models.user.management.CurrentUser;
import models.user.management.contracts.UserContract;
import permission.Access;
import permission.Can;
import repositories.searchcontent.GeneralSearchRepository;
import services.search.ElasticIndexingService;

/**
 * @author HanusaCloud on 11/6/2019 2:45 PM
 */
public class DummySearchItemController extends HtmlBaseController {

    private static final GeneralSearchRepository repository = GeneralSearchRepository.getInstance();

    public static void search() {
        CurrentUser currentUser = getCurrentUser();
        UserContract user = null;
        if (currentUser != null && currentUser.user != null) {
            user = currentUser.user;
        }
        renderArgs.put("result", repository.search(new SearchQueryParams(params), user));
        renderTemplate(R.view.searchcontent_elastic_result);
    }

    @Can({Access.SUPER_ADMIN})
    public static void reIndexing() {
        try {
            new ElasticJob().doJob();
        } catch (Exception e) {
            e.printStackTrace();
        }
        ok();
    }

    @Can({Access.SUPER_ADMIN})
    public static void reMapping() {
        try {
            ElasticIndexingService.getInstance().reMapping();
        } catch (Exception e) {
            e.printStackTrace();
        }
        ok();
    }

}
