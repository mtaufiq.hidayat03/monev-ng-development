package controllers.searchcontent;

import constants.generated.R;
import controllers.common.HtmlBaseController;
import models.common.contracts.Chartable;
import models.searchcontent.BlacklistSearchQueryParam;
import models.searchcontent.ElasticSearchResult;
import repositories.searchcontent.AggregateResult;
import repositories.searchcontent.BlacklistElasticRepository;
import repositories.searchcontent.KeywordRecorderRepository;
import utils.common.LogUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 11/12/2019 2:32 PM
 */
public class BlacklistSearchController extends HtmlBaseController {

    public static final String TAG = "BlacklistSearchController";
    private static final BlacklistElasticRepository repository = BlacklistElasticRepository.getInstance();

    public static final String[] FILTERS = {
            "Penyedia Berdasarkan Pelanggaran",
            "Penyedia Berdasarkan Domisili",
            "Penyedia Berdasarkan Penayangan",
            "Berdasarkan Jenis Pengadaan",
            "Berdasarkan Penyedia Ikut Tender/Seleksi"
    };
    public static final String[] DOMICILES = {
            "Nasional",
            "Provinsi"
    };

    public static void index() {
        BlacklistSearchQueryParam queryParam = new BlacklistSearchQueryParam(params);
        LogUtil.debug(TAG, queryParam);
        if (queryParam.isProvinceFilter() || queryParam.isNationalFilter()) {
            List<AggregateResult> provinces = repository.getProvinces(queryParam);
            renderArgs.put("provinces", provinces);
            if (queryParam.isProvinceExist()) {
                LogUtil.debug(TAG, "get domiciles aggs");
                renderArgs.put("domicileAggs", repository.getDistricts(queryParam));
            }
            if (queryParam.isNationalFilter()) {
                List<AggregateResult> domicileAggs = new ArrayList<>(provinces);
                domicileAggs.sort((o1, o2) -> (int)(o2.count - o1.count));
                renderArgs.put("domicileAggs",
                        provinces.size() >= 5
                                ? domicileAggs.subList(0, 5)
                                : domicileAggs
                );
            }
        }
        if (queryParam.isItDomicile()) {
            renderArgs.put("domiciles", DOMICILES);
        }
        if (queryParam.isItPublishedDate()) {
            LogUtil.debug(TAG, "get publihed date aggs");
            renderArgs.put("years", repository.extractYear(queryParam));
            Chartable byDate = repository.getPublishedDateAggregation(queryParam);
            LogUtil.debug(TAG, byDate);
            renderArgs.put("publishedDateAggs", byDate);
        }
        if (queryParam.isViolation()) {
            LogUtil.debug(TAG, "get violation aggs");
            renderArgs.put("violationAggs", repository.getViolationAggragation(queryParam));
        }
        if (queryParam.isProcurementType()) {
           injectProcurement(queryParam);
        }
        ElasticSearchResult searchResult = repository.blacklistSearch(queryParam);
        KeywordRecorderRepository.getInstance().record(
                new KeywordRecorderRepository.RecordKeyword(
                        queryParam.getKeyword(),
                        getUserId().toString(),
                        getPlaySessionCookie(),
                        searchResult.getTotal(),
                        searchResult.getQueryString()
                )
        );
        renderArgs.put("filters", FILTERS);
        renderArgs.put("queryParam", queryParam);
        renderArgs.put("result", searchResult);
        renderTemplate(R.view.searchcontent_blacklist_search);
    }

    private static boolean injectProcurement(BlacklistSearchQueryParam queryParam) {
        List<AggregateResult> procurementResults = repository.extractProcurementType(queryParam);
        renderArgs.put("procurementFilters", procurementResults);
        return true;
    }

}
