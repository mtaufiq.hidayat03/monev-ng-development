package controllers.searchcontent;

import constants.generated.R;
import controllers.common.HtmlBaseController;
import models.searchcontent.ElasticSearchResult;
import models.searchcontent.SearchQueryParams;
import models.user.management.CurrentUser;
import models.user.management.contracts.UserContract;
import repositories.searchcontent.GeneralSearchRepository;
import repositories.searchcontent.KeywordRecorderRepository;

/**
 * @author HanusaCloud on 11/7/2019 9:07 AM
 */
public class SearchController extends HtmlBaseController {

    public static void index() {
        CurrentUser currentUser = getCurrentUser();
        UserContract user = null;
        if (currentUser != null && currentUser.user != null) {
            user = currentUser.user;
        }
        SearchQueryParams searchQueryParams = new SearchQueryParams(params);
        ElasticSearchResult searchResult = GeneralSearchRepository.getInstance()
                .search(searchQueryParams, user);
        KeywordRecorderRepository.getInstance().record(
                new KeywordRecorderRepository.RecordKeyword(
                        searchQueryParams.getKeyword(),
                        getUserId().toString(),
                        getPlaySessionCookie(),
                        searchResult.getTotal(),
                        searchResult.getQueryString()
                )
        );
        renderArgs.put("result", searchResult);
        renderTemplate(R.view.searchcontent_elastic_result);
    }

}
