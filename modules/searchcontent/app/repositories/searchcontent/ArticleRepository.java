package repositories.searchcontent;

import models.cms.Content;
import models.cms.ContentType;
import models.searchcontent.ElasticRequest;
import models.searchcontent.ElasticSearchResult;
import models.searchcontent.SearchQueryParams;
import models.searchcontent.SearchResultItem;
import models.user.management.contracts.UserContract;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import play.cache.Cache;
import utils.common.LogUtil;

import java.util.List;

/**
 * @author HanusaCloud on 11/28/2019 3:47 PM
 */
public class ArticleRepository extends ElasticRepository {

    public static final String TAG = "ArticleRepository";

    private static final String LATEST_CONTENTS_KEY = "latest_contents";

    private static final ArticleRepository repository = new ArticleRepository();

    public static ArticleRepository getInstance() {
        return repository;
    }

    private BoolQueryBuilder getBoolQuery(UserContract user) {
        BoolQueryBuilder boolQueryBuilder = getBoolQueryBuilder(user);
        boolQueryBuilder.filter(QueryBuilders.termsQuery("model_type.keyword",
                ContentType.ARTIKEL_UMUM,
                ContentType.PROFILE_KLPD
                )
        );
        return boolQueryBuilder;
    }

    public List<SearchResultItem> getLatest(UserContract user) {
        LogUtil.debug(TAG, "get latest general articles");
        List<SearchResultItem> contents;
        if ((contents = (List<SearchResultItem>) Cache.get(LATEST_CONTENTS_KEY)) != null) {
            return contents;
        }
        SearchRequest request = new SearchRequest(MONEV_INDEX);
        BoolQueryBuilder boolQueryBuilder = getBoolQuery(user);
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        boolQueryBuilder.must(QueryBuilders.matchAllQuery());
        sourceBuilder.sort(DATE_DESC);
        sourceBuilder.query(boolQueryBuilder);
        sourceBuilder.from(0);
        sourceBuilder.size(5);
        sourceBuilder.timeout(SEARCH_DEFAULT_TIMEOUT);
        request.source(sourceBuilder);
        contents = processingSearch(request, null);
        if (!contents.isEmpty()) {
            Cache.set(LATEST_CONTENTS_KEY, contents, "2h");
        }
        return contents;
    }

    public void deleteLatestContentsCache() {
        Cache.delete(LATEST_CONTENTS_KEY);
    }

    public ElasticSearchResult generalArticleSearch(SearchQueryParams params, UserContract user) {
        ElasticSearchResult result = new ElasticSearchResult(params);
        if (result.exceedPageLimitation()) {
            LogUtil.debug(TAG, "exceed page limitation.. exit");
            return result;
        }
        SearchRequest request = new SearchRequest(MONEV_INDEX);
        BoolQueryBuilder boolQueryBuilder = getBoolQuery(user);
        SearchSourceBuilder sourceBuilder = getBaseSourceBuilder(params);
        if (params.isKeywordExist()) {
            boolQueryBuilder.must(getMultiMatch(params.getKeyword(), CONTENT_FIELDS));
        } else {
            boolQueryBuilder.must(QueryBuilders.matchAllQuery());
            sourceBuilder.sort(DATE_DESC);
        }
        sourceBuilder.query(boolQueryBuilder);
        sourceBuilder.from(result.getOffset());
        sourceBuilder.size(result.getLimit());
        sourceBuilder.timeout(SEARCH_DEFAULT_TIMEOUT);
        request.source(sourceBuilder);
        result.setItems(processingSearch(request, result));
        return result;
    }

    public List<SearchResultItem> populars(UserContract user) {
        LogUtil.debug(TAG, "get 5 most popular");
        final String key = "populars_general_article";
        List<SearchResultItem> saved;
        LogUtil.debug(TAG, "check for cache");
        if ((saved = (List<SearchResultItem>) Cache.get(key)) != null) {
            LogUtil.debug(TAG, "cache exists");
            return saved;
        }
        LogUtil.debug(TAG, "cache not exist");
        SearchRequest request = new SearchRequest(MONEV_INDEX);
        BoolQueryBuilder boolQueryBuilder = getBoolQuery(user);
        boolQueryBuilder.filter(QueryBuilders.rangeQuery("payload.viewer").gte(1));
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(boolQueryBuilder);
        sourceBuilder.size(5);
        sourceBuilder.sort("payload.viewer", SortOrder.DESC);
        sourceBuilder.timeout(SEARCH_DEFAULT_TIMEOUT);
        request.source(sourceBuilder);
        saved = processingSearch(request, null);
        LogUtil.debug(TAG, "cache populars ");
        if (!saved.isEmpty()) {
            Cache.set(key, saved, "12h");
        }
        return saved;
    }

    public boolean storeToElastic(Content model) {
        LogUtil.debug(TAG, "send changes to elastic");
        if (model == null || model.getContentId() == null || model.getContentId() == 0) {
            LogUtil.debug(TAG, "Oops.. it seems content id is null");
            return false;
        }
        return upsert(new ElasticRequest(model));
    }

}
