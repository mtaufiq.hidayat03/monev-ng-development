package repositories.searchcontent;

import com.google.gson.annotations.SerializedName;
import models.cms.Content;
import models.common.contracts.CreationContract;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.BucketOrder;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramInterval;
import org.elasticsearch.search.aggregations.bucket.histogram.Histogram;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.ParsedCardinality;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import repositories.cms.ContentRepository;
import utils.common.LogUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author HanusaCloud on 11/28/2019 10:24 AM
 */
public class PopularContentRepository extends ElasticRepository {

    public static final String TAG = "PopularContentRepository";

    public static String INDEX = MONEV_INDEX + "-popular";

    private static final PopularContentRepository repository = new PopularContentRepository();

    public static PopularContentRepository getInstance() {
        return repository;
    }

    public void popularIndexRequest(PopularContent popularContent) {
        LogUtil.debug(TAG, "generate index request");
        IndexRequest request = new IndexRequest(INDEX);
        request.source(gson.toJson(popularContent), XContentType.JSON);
        ElasticClient.client.indexAsync(
                request,
                RequestOptions.DEFAULT,
                new ActionListener<IndexResponse>() {
            @Override
            public void onResponse(IndexResponse indexResponse) {
                LogUtil.debug(TAG, "index status: " + (indexResponse.status() == RestStatus.OK));
            }

            @Override
            public void onFailure(Exception e) {
                LogUtil.error(TAG, e);
            }
        });
    }

    public List<PopularContent> getPopularContents(String ...id) {
        LogUtil.debug(TAG, "get populars value or total viewer");
        SearchRequest request = new SearchRequest(INDEX);
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        AggregationBuilder userCardinality = AggregationBuilders.cardinality("by-user")
                .field("user_id.keyword");
        AggregationBuilder createdHistogram = AggregationBuilders.dateHistogram("by-created")
                .field("created_at")
                .fixedInterval(DateHistogramInterval.minutes(5))
                .minDocCount(1L)
                .subAggregation(userCardinality)
                .order(BucketOrder.aggregation("by-user", false));
        AggregationBuilder aggregationBuilder = AggregationBuilders
                .terms("by-content")
                .field("content_id.keyword")
                .order(BucketOrder.aggregation("_count", false))
                .subAggregation(createdHistogram)
                .size(5);
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.must(QueryBuilders.termsQuery("content_id", id));
        sourceBuilder.query(boolQueryBuilder);
        sourceBuilder.aggregation(aggregationBuilder);
        sourceBuilder.size(0);
        sourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        request.source(sourceBuilder);
        LogUtil.debug(TAG, request);
        List<PopularContent> popularContents = new ArrayList<>();
        try {
            SearchResponse response = ElasticClient.client.search(request, RequestOptions.DEFAULT);
            Terms terms = response.getAggregations().get("by-content");
            for (Terms.Bucket bucket : terms.getBuckets()) {
                PopularContent model = new PopularContent();
                model.contentId = bucket.getKey().toString();
                LogUtil.debug(TAG, "content id: " + model.contentId);
                Histogram dateTerms = bucket.getAggregations().get("by-created");
                if (dateTerms != null) {
                    for (Histogram.Bucket bucketDate : dateTerms.getBuckets()) {
                        LogUtil.debug(TAG, "key: " + bucketDate.getKey().toString());
                        ParsedCardinality userTerms = bucketDate.getAggregations().get("by-user");
                        if (userTerms == null) {
                            continue;
                        }
                        model.viewer += userTerms.value();
                        LogUtil.debug(TAG, "value: " + model.viewer);
                    }
                    popularContents.add(model);
                }

            }
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }
        LogUtil.debug(TAG, popularContents);
        return popularContents;
    }

    public static class PopularContent {

        @SerializedName("content_id")
        public String contentId;
        public String cookie;
        @SerializedName("created_at")
        public Date createdAt = new Date();
        @SerializedName("content_type")
        public String type;
        @SerializedName("user_id")
        public String userId;
        @SerializedName("minutes_grouper")
        public String minutesGrouper;

        public transient double viewer;
        public Content article;

        public PopularContent() {}

        public PopularContent(
                String contentId,
                String cookie,
                String type,
                String userId
        ) {
            this.contentId = contentId;
            this.cookie = cookie;
            this.type = type;
            this.userId = userId;
            this.minutesGrouper = new SimpleDateFormat(CreationContract.MISSING_SECONDS).format(createdAt);
        }

        public Content getGeneralArticle() {
            if (article == null && !StringUtils.isEmpty(contentId)) {
                article = ContentRepository.getByContent(Long.valueOf(contentId));
            }
            return article;
        }

        public Long getViewerIn() {
            return new Double(viewer).longValue();
        }

    }

}
