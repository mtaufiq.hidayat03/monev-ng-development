package repositories.searchcontent;

import models.common.contracts.CreationContract;
import utils.common.LogUtil;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author HanusaCloud on 11/13/2019 8:47 AM
 */
public class AggregateResult implements Serializable {

    public static final String TAG = "AggregateResult";

    public final String name;
    public final long count;

    public AggregateResult(String name, long count) {
        this.name = name;
        this.count = count;
    }

    public List<Number> getCountAsList() {
        List<Number> results = new ArrayList<>();
        results.add(count);
        return results;
    }

    public long getCount() {
        return count;
    }

    public String getName() {
        return name;
    }

    public String generateAsDateString(long value) {
        try {
            final Date date = new Date(value);
            return new SimpleDateFormat(CreationContract.DATE_FORMAT).format(date);
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            return "";
        }
    }

}
