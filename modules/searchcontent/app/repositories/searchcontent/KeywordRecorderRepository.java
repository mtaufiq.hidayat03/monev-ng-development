package repositories.searchcontent;

import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.BucketOrder;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import play.cache.Cache;
import utils.common.LogUtil;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author HanusaCloud on 12/17/2019 8:47 AM
 */
public class KeywordRecorderRepository extends ElasticRepository {

    public static final String TAG = "KeywordRecorderRepository";

    private static final String RECORDER_INDEX = "keyword-recorder-" + MONEV_INDEX;
    private static final String CACHE_KEY = "most-searched-keywords";

    private static final KeywordRecorderRepository repository = new KeywordRecorderRepository();

    public static KeywordRecorderRepository getInstance() {
        return repository;
    }

    public void initMapping() {
        LogUtil.debug(TAG, "create mapping");
        Mapping[] mappings = {
                new Mapping("searched", "text"),
                new Mapping("searched_keyword", "keyword"),
                new Mapping("user", "text"),
                new Mapping("session", "text"),
                new Mapping("session_keyword", "keyword"),
                new Mapping("created_at", "date"),
                new Mapping("query_string", "keyword")
        };
        mapping(RECORDER_INDEX, mappings);
    }

    public void record(RecordKeyword data) {
        if (!data.allowToBeRecorded()) {
            LogUtil.debug(TAG, "keyword does not allow to be recorded.. exit");
            return;
        }
        LogUtil.debug(TAG, "record keyword: " + data.searched);
        IndexRequest request = new IndexRequest(RECORDER_INDEX);
        LogUtil.debug(TAG, "build index request");
        request.source(gson.toJson(data), XContentType.JSON);
        try {
            ElasticClient.client.index(request, RequestOptions.DEFAULT);
        } catch (Exception e) {
            LogUtil.warn(TAG, "Oops failed to record keyword");
            LogUtil.error(TAG, e);
        }
    }

    public List<String> getMostSearchedKeywords() {
        LogUtil.debug(TAG, "get most searched keywords");
        List<String> results;
        LogUtil.debug(TAG, "check for cache by key: " + CACHE_KEY);
        if ((results = (List<String>) Cache.get(CACHE_KEY)) != null) {
            LogUtil.debug(TAG, "cache exists");
            return results;
        }
        LogUtil.debug(TAG, "cache not exists or expired, generate new cache");
        SearchRequest request = new SearchRequest(RECORDER_INDEX);
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.filter(
                QueryBuilders.rangeQuery("total").gt(0)
        );
        boolQueryBuilder.filter(
                new BoolQueryBuilder().must(QueryBuilders.existsQuery("searched"))
        );
        LogUtil.debug(TAG, "generate terms aggregation by most searched keywords");
        AggregationBuilder aggregationBuilder = AggregationBuilders
                .terms("by-keyword")
                .field("searched_keyword")
                .order(BucketOrder.aggregation("_count", false))
                .size(5);
        sourceBuilder.query(boolQueryBuilder);
        sourceBuilder.aggregation(aggregationBuilder);
        sourceBuilder.size(0);
        sourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        request.searchType(SearchType.DFS_QUERY_THEN_FETCH);
        request.source(sourceBuilder);
        results = new ArrayList<>();
        try {
            SearchResponse response = ElasticClient.client.search(request, RequestOptions.DEFAULT);
            Terms terms = response.getAggregations().get("by-keyword");
            for (Terms.Bucket bucket : terms.getBuckets()) {
                final String key = bucket.getKeyAsString();
                if (!StringUtils.isEmpty(key)) {
                    results.add(bucket.getKeyAsString());
                }
            }
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }
        LogUtil.debug(TAG, "most searched: ", results);
        if (!results.isEmpty()) {
            LogUtil.debug(TAG, "cache results for 5 minutes");
            Cache.set(CACHE_KEY, results, "5mn");
        }
        return results;
    }

    public static class RecordKeyword {

        public static final String TAG = "RecordKeyword";

        public final String searched;
        @SerializedName("searched_keyword")
        public final String searchedKeyword;
        public final String user;
        public final String session;
        @SerializedName("session_keyword")
        public final String sessionKeyword;
        @SerializedName("created_at")
        public final Date createdAt;
        public final Integer total;
        @SerializedName("query_string")
        public final String queryString;

        public RecordKeyword(String keyword,
                             String user,
                             String session,
                             Integer total,
                             String queryString
        ) {
            this.searched = keyword;
            this.searchedKeyword = this.searched;
            this.user = user;
            this.session = session;
            this.sessionKeyword = this.session;
            this.createdAt = new Date();
            this.total = total;
            this.queryString = queryString;
        }

        public String sanitizeSearched() {
            return searched.replaceAll("[\\W]+", "");
        }

        public String getCacheKey() {
            return session + "_" + sanitizeSearched();
        }

        public boolean allowToBeRecorded() {
            if (StringUtils.isEmpty(searched)) {
                LogUtil.debug(TAG, "keyword empty no need to record");
                return false;
            }
            if (sanitizeSearched().length() <= 3) {
                LogUtil.debug(TAG, "Oops.. sanitize keyword length lte 3, exits.");
                return false;
            }
            final Boolean result = (Boolean) Cache.get(getCacheKey());
             if (result == null) {
                 Cache.set(getCacheKey(), true, "3mn");
                 return true;
             }
             return false;
        }

    }

}
