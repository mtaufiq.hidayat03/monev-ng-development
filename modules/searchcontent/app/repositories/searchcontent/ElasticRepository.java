package repositories.searchcontent;

import com.google.gson.*;
import models.cms.ContentPermission;
import models.cms.ContentStatus;
import models.common.contracts.CreationContract;
import models.common.contracts.TablePagination;
import models.searchcontent.ElasticRequest;
import models.searchcontent.SearchResultItem;
import models.user.management.contracts.UserContract;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.indices.GetIndexTemplatesRequest;
import org.elasticsearch.client.indices.GetIndexTemplatesResponse;
import org.elasticsearch.client.indices.PutIndexTemplateRequest;
import org.elasticsearch.common.unit.Fuzziness;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.*;
import org.elasticsearch.rest.RestStatus;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.*;
import play.Play;
import utils.common.LogUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author HanusaCloud on 11/6/2019 10:28 AM
 */
public class ElasticRepository {

    public static final String TAG = "ElasticRepository";

    public static final String MONEV_INDEX = Play.configuration.getProperty("es_index");
    protected static final Map<String, Float> ALL_FIELDS;
    protected static final Map<String, Float> CONTENT_FIELDS;
    protected static final Map<String, Float>  BLACKLIST_FIELDS;
    protected static final TimeValue SEARCH_DEFAULT_TIMEOUT = new TimeValue(60, TimeUnit.SECONDS);

    public static final String[] SORTS = new String[]{"Relevance", "Date Ascending", "Date Descending"};
    private static final ScoreSortBuilder RELEVANCE = new ScoreSortBuilder().order(SortOrder.DESC);
    private static final FieldSortBuilder DATE_ASC = SortBuilders.fieldSort("model_created_at").order(SortOrder.ASC);
    protected static final FieldSortBuilder DATE_DESC = SortBuilders.fieldSort("model_created_at").order(SortOrder.DESC);

    private static final SortBuilder[] SORT_BUILDERS = new SortBuilder[]{
            RELEVANCE,
            DATE_ASC,
            DATE_DESC
    };

    public static final Mapping[] MAPPINGS = new Mapping[]{
            new Mapping("model_created_at", "date", true),
            new Mapping("payload.pelanggaran.published_date", "date", true),
            new Mapping("payload.pelanggaran.started", "date", true),
            new Mapping("payload.pelanggaran.expired_date", "date", true),
            new Mapping("payload.pelanggaran.withdrawal_date", "date", true),
            new Mapping("payload.dibuat", "date"),
            new Mapping("payload.created_at", "date"),
            new Mapping("payload.deleted_at", "date")
    };

    static {
        ALL_FIELDS = new HashMap<>();
        CONTENT_FIELDS = new HashMap<>();
        CONTENT_FIELDS.put("model_type", 0.3f);
        CONTENT_FIELDS.put("payload.title", 1.2f);
        CONTENT_FIELDS.put("payload.content", 1.0f);
        CONTENT_FIELDS.put("payload.kldi_name", 0.8f);
        CONTENT_FIELDS.put("payload.vision", 0.6f);
        CONTENT_FIELDS.put("payload.mission", 0.6f);
        ALL_FIELDS.putAll(CONTENT_FIELDS);

        BLACKLIST_FIELDS = new HashMap<>();
        BLACKLIST_FIELDS.put("payload.kldi_id", 0.3f);
        BLACKLIST_FIELDS.put("payload.alamat", 0.7f);
        BLACKLIST_FIELDS.put("payload.pelanggaran.detil_pelanggaran.deskripsi", 0.9f);
        BLACKLIST_FIELDS.put("payload.pelanggaran.detil_pelanggaran.jenis_pelanggaran", 0.9f);
        BLACKLIST_FIELDS.put("payload.provinsi.nama", 0.6f);
        BLACKLIST_FIELDS.put("payload.kabupaten.nama", 0.6f);
        BLACKLIST_FIELDS.put("payload.sk", 0.4f);
        BLACKLIST_FIELDS.put("payload.pelanggaran.sk", 0.4f);
        BLACKLIST_FIELDS.put("payload.nama_penyedia", 1.2f);
        BLACKLIST_FIELDS.put("payload.npwp", 0.2f);
        BLACKLIST_FIELDS.put("payload.satuan_kerja.nama", 0.6f);
        BLACKLIST_FIELDS.put("payload.status", 0.6f);
        BLACKLIST_FIELDS.put("payload.nama_pengadaan", 0.6f);
        BLACKLIST_FIELDS.put("payload.jenis_pengadaan", 0.6f);
        ALL_FIELDS.putAll(BLACKLIST_FIELDS);
        //CONTENT_FIELDS.put("payload.data.value", 0.5f);
        ALL_FIELDS.put("payload.data.header", 0.9f);
     //   PROFILE_FIELDS.put("payload.data.pagu", 0.5f);

    }

    private static ExclusionStrategy strategy = new ExclusionStrategy() {
        @Override
        public boolean shouldSkipField(FieldAttributes field) {
            if (field.getName().equals("created_at")
                    || field.getName().equals("updated_at")
                    || field.getName().equals("created_by")
                    || field.getName().equals("updated_by")) {
                return true;
            }
            return false;
        }

        @Override
        public boolean shouldSkipClass(Class<?> clazz) {
            return false;
        }
    };

    protected static Gson gson = new GsonBuilder()
            //.addSerializationExclusionStrategy(strategy)
            .setDateFormat(CreationContract.STANDARD_FORMAT)
            .create();

    public IndexRequest toIndexRequest(ElasticRequest payload) {
        LogUtil.debug(TAG, "generate index request");
        IndexRequest request = new IndexRequest(ElasticRepository.MONEV_INDEX);
        request.id(payload.payload.getGeneratedId());
        request.source(gson.toJson(payload), XContentType.JSON);
        return request;
    }

    public UpdateRequest toUpdateRequest(ElasticRequest payload) {
        LogUtil.debug(TAG, "generate update request");
        UpdateRequest request = new UpdateRequest(ElasticRepository.MONEV_INDEX, payload.payload.getGeneratedId());
        request.doc(gson.toJson(payload), XContentType.JSON);
        request.upsert(toIndexRequest(payload));
        return request;
    }

    public boolean upsert(ElasticRequest request) {
        if (request == null) {
            LogUtil.debug(TAG, "Oops.. it seems request is null");
            return false;
        }
        LogUtil.debug(TAG, "doing single upsert");
        List<ElasticRequest> requests = new ArrayList<>();
        requests.add(request);
        try {
             return bulk(requests);
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            return false;
        }
    }

    public boolean bulk(List<ElasticRequest> requests) throws IOException {
        if (requests.isEmpty()) {
            LogUtil.debug(TAG, "it seems requests is empty");
            return false;
        }
        BulkRequest bulkRequest = new BulkRequest();
        for (ElasticRequest request : requests) {
            bulkRequest.add(toUpdateRequest(request));
        }
        BulkResponse bulkResponse = ElasticClient.client.bulk(bulkRequest, RequestOptions.DEFAULT);
        LogUtil.debug(TAG, "bulk status: " + bulkResponse.status().name());
        return bulkResponse.status() == RestStatus.OK;
    }

    public void save(UpdateRequest request) throws IOException {
        UpdateResponse response = ElasticClient.client.update(request, RequestOptions.DEFAULT);
        LogUtil.debug(TAG, response);
    }

    public boolean deleteDocument(ElasticRequest model) {
        final String documentId = model.payload.getGeneratedId();
        LogUtil.debug(TAG, "delete document by id: " + documentId);
        DeleteRequest request = new DeleteRequest(MONEV_INDEX, documentId);
        try {
            DeleteResponse response = ElasticClient.client.delete(request, RequestOptions.DEFAULT);
            LogUtil.debug(TAG, "is result OK: " + (response.status() == RestStatus.OK));
            return true;
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            return false;
        }
    }

    public void deleteIndex() {
        LogUtil.debug(TAG, "delete index: " + MONEV_INDEX);
        DeleteIndexRequest request = new DeleteIndexRequest(MONEV_INDEX);
        request.timeout(TimeValue.timeValueMinutes(2));
        request.timeout("2m");
        try {
            AcknowledgedResponse response = ElasticClient.client.indices().delete(request, RequestOptions.DEFAULT);
            LogUtil.debug(TAG, "response status isAcknowledged: " + response.isAcknowledged());
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }
    }

    public Boolean isMappingExist(String mappingKey) {
        GetIndexTemplatesRequest request = new GetIndexTemplatesRequest(mappingKey);
        try {
            GetIndexTemplatesResponse response = ElasticClient.client
                                                .indices()
                                                .getIndexTemplate(request, RequestOptions.DEFAULT);
            return response != null
                    && response.getIndexTemplates() != null;
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            return false;
        }
    }

    protected List<SearchResultItem> processingSearch(SearchRequest request, TablePagination.PageAble result) {
        LogUtil.debug(TAG, "process result result items, return list of items");
        List<SearchResultItem> items = new ArrayList<>();
        try {
            SearchResponse response = ElasticClient.client.search(request, RequestOptions.DEFAULT);
            if (result != null) {
                result.setTotal((int) response.getHits().getTotalHits().value);
            }
            for (SearchHit searchHit : response.getHits().getHits()) {
                items.add(new SearchResultItem(searchHit));
            }
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }
        return items;
    }

    protected MultiMatchQueryBuilder getMultiMatch(String keyword, Map<String, Float> fields) {
        LogUtil.debug(TAG, "generate base multi match query");
        return QueryBuilders.multiMatchQuery(keyword)
                .fields(fields)
                .operator(Operator.AND)
                .type(MultiMatchQueryBuilder.Type.BEST_FIELDS)
                .tieBreaker(0.5f)
                .fuzziness(Fuzziness.AUTO)
                .maxExpansions(4)
                .lenient(false)
                .prefixLength(3);
    }

    protected List<MatchQueryBuilder> getFuzzies(String keyword, Map<String, Float> fields) {
        List<MatchQueryBuilder> matchQueryBuilders = new ArrayList<>();
        for (Map.Entry<String, Float> entry : fields.entrySet()) {
            if (entry.getValue() > 0.9f) {
                matchQueryBuilders.add(QueryBuilders.matchQuery(entry.getKey(), keyword)
                        .fuzziness(3)
                        .boost(entry.getValue())
                        .maxExpansions(70)
                        .operator(Operator.AND)
                        .prefixLength(2)
                );
            }
        }
        return matchQueryBuilders;
    }

    public JsonObject generateTemplateMapping(Mapping[] mappings) {
        JsonObject jsonObject = new JsonObject();
        JsonObject properties = new JsonObject();
        for (Mapping mapping : mappings) {
            properties.add(mapping.name, mapping.getProperties());
        }
        jsonObject.add("properties", properties);
        return jsonObject;
    }

    public void mapping(String index, Mapping[] mappings) {
        LogUtil.debug(TAG, "load mapping template");
        List<String> patterns = new ArrayList<>();
        patterns.add(index + "*");
        PutIndexTemplateRequest request = new PutIndexTemplateRequest(index);
        request.patterns(patterns);
        request.mapping(generateTemplateMapping(mappings).toString(), XContentType.JSON);
        try {
            AcknowledgedResponse putTemplateResponse = ElasticClient.client.indices()
                    .putTemplate(request, RequestOptions.DEFAULT);
            LogUtil.debug(TAG, "is request acknowledged: " + putTemplateResponse.isAcknowledged());
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }
    }

    public void reIndexing() {
        deleteIndex();
        mapping(MONEV_INDEX, MAPPINGS);
    }

    protected BoolQueryBuilder getBoolQueryBuilder(UserContract user) {
        LogUtil.debug(TAG, "get default bool query with permission filter");
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.filter(QueryBuilders.termsQuery(
                "payload.publishing_status.keyword",
                getPublishingStatus(user)).boost(0f));
        boolQueryBuilder.filter(QueryBuilders.termsQuery(
                "payload.content_permission.keyword",
                getPermission(user)).boost(0f));
        boolQueryBuilder.should(QueryBuilders.boolQuery()
                .mustNot(QueryBuilders.existsQuery(
                        "payload.content_permission"
                ).boost(0f))
                .boost(0f));
        return boolQueryBuilder;
    }

    protected List<String> getPermission(UserContract user) {
        LogUtil.debug(TAG, "produce permission based on user");
        List<String> permissions = new ArrayList<>();
        permissions.add(ContentPermission.PUBLIC.toLowerCase());
        if (user != null) {
            permissions.add(ContentPermission.INTERNAL.toLowerCase());
            if (user.isSuperAdmin()) {
                permissions.add(ContentPermission.PRIVATE.toLowerCase());
            } else {
                permissions.add(user.getUserId().toString());
            }
        }
        return permissions;
    }

    protected List<String> getPublishingStatus(UserContract user) {
        List<String> permissions = new ArrayList<>();
        permissions.add(ContentStatus.PUBLISH.name);
        if (user == null) {
            return permissions;
        }
//        if (user.isSuperAdmin()) {
//            permissions.add(ContentStatus.STOCK_REVIEW.name);
//        } else {
//            permissions.add(ContentStatus.DRAFT.name);
//        }
//        permissions.add(user.getUserId().toString());
        return permissions;
    }

    protected SearchSourceBuilder getBaseSourceBuilder(TablePagination.ParamAble paramAble) {
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        Integer sort = paramAble.getSort();
        if (sort < 0 || sort >= SORT_BUILDERS.length) {
            sort = 0;
        }
        if (sort== 0) {
            if (paramAble.isKeywordExist()) {
                sourceBuilder.sort(RELEVANCE);
            }
            sourceBuilder.sort(DATE_DESC);
            return sourceBuilder;
        }
        sourceBuilder.sort(SORT_BUILDERS[sort]);
        return sourceBuilder;
    }

    public static class Mapping {

        public final String name;
        public final String type;
        public final boolean nullAble;

        public Mapping(String name, String type, boolean nullAble) {
            this.name = name;
            this.type = type;
            this.nullAble = nullAble;
        }

        public Mapping(String name, String type) {
            this.name = name;
            this.type = type;
            this.nullAble = false;
        }

        public JsonObject getProperties() {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("type", type);
            if (nullAble) {
                jsonObject.addProperty("null_value", "NULL");
            }
            return jsonObject;
        }

    }

}
