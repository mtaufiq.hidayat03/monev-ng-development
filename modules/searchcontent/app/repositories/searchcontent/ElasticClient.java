package repositories.searchcontent;

import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import play.Play;

/**
 * @author HanusaCloud on 11/13/2019 8:38 AM
 */
public class ElasticClient {

    public static RestHighLevelClient client = new RestHighLevelClient(
        RestClient.builder(HttpHost.create(Play.configuration.getProperty("es_host")))
            .setHttpClientConfigCallback(httpAsyncClientBuilder -> {
                final CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
                credentialsProvider.setCredentials(AuthScope.ANY,
                        new UsernamePasswordCredentials(
                                Play.configuration.getProperty("es_user"),
                                Play.configuration.getProperty("es_pass")
                        ));
                return httpAsyncClientBuilder.setDefaultCredentialsProvider(credentialsProvider);
            })
    );

}
