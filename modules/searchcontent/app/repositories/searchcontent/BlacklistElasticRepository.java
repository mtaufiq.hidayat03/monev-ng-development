package repositories.searchcontent;

import models.cms.ContentType;
import models.common.contracts.Chartable;
import models.searchcontent.BlacklistSearchQueryParam;
import models.searchcontent.ElasticSearchResult;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.common.unit.TimeValue;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.BucketOrder;
import org.elasticsearch.search.aggregations.bucket.histogram.DateHistogramInterval;
import org.elasticsearch.search.aggregations.bucket.histogram.Histogram;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.aggregations.metrics.ValueCount;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import utils.common.LogUtil;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static models.common.contracts.CreationContract.*;
import static models.searchcontent.BlacklistSearchQueryParam.INJUNCTION_KEY_FIELDS;
import static models.searchcontent.BlacklistSearchQueryParam.INJUNCTION_MAP;

/**
 * @author HanusaCloud on 11/13/2019 8:39 AM
 */
public class BlacklistElasticRepository extends ElasticRepository {

    public static final String TAG = "BlacklistElasticRepository";
    private static final BlacklistElasticRepository repository = new BlacklistElasticRepository();

    public static BlacklistElasticRepository getInstance() {
        return repository;
    }

    public List<AggregateResult> getDistricts(BlacklistSearchQueryParam param) {
        return getDomicileFromBlacklist(param,"kabupaten", 5);
    }

    public List<AggregateResult> getProvinces(BlacklistSearchQueryParam param) {
        return getDomicileFromBlacklist(param, "provinsi", 1000);
    }

    private MultiMatchQueryBuilder getBlacklistMultiMatch(String keyword) {
        return getMultiMatch(keyword, BLACKLIST_FIELDS);
    }

    private List<AggregateResult> getDomicileFromBlacklist(
            BlacklistSearchQueryParam param,
            String key,
            int total) {
        List<AggregateResult> regions = new ArrayList<>();
        SearchRequest request = new SearchRequest(MONEV_INDEX);
        SearchSourceBuilder sourceBuilder = getBaseSourceBuilder(param);
        AggregationBuilder aggregationBuilder = AggregationBuilders
                .terms(key)
                .field("payload." + key + ".nama.keyword")
                .order(BucketOrder.key(true))
                .size(total);
        BoolQueryBuilder boolQueryBuilder = getBoolQuery();
        if (param.isKeywordExist()) {
            boolQueryBuilder.must(getBlacklistMultiMatch(param.getKeyword()));
        }
        if (param.isProvinceExist() && !key.equals("provinsi")) {
            boolQueryBuilder.must(QueryBuilders.termQuery("payload.provinsi.nama.keyword", param.getProvince()));
        }
        sourceBuilder.query(boolQueryBuilder);
        sourceBuilder.aggregation(aggregationBuilder);
        sourceBuilder.size(0);
        sourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        request.source(sourceBuilder);
        LogUtil.debug(TAG, request);
        try {
            SearchResponse response = ElasticClient.client.search(request, RequestOptions.DEFAULT);
            Terms terms = response.getAggregations().get(key);
            for (Terms.Bucket bucket : terms.getBuckets()) {
                regions.add(new AggregateResult(bucket.getKey().toString(), bucket.getDocCount()));
            }
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }
        LogUtil.debug(TAG, regions);
        return regions;
    }

    public List<Integer> extractYear(BlacklistSearchQueryParam params) {
        LogUtil.debug(TAG, "get filter years");
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        for (String field : INJUNCTION_KEY_FIELDS) {
            AggregationBuilder agg = AggregationBuilders.dateHistogram(field)
                    .field("payload.pelanggaran." + field)
                    .calendarInterval(DateHistogramInterval.MONTH)
                    .minDocCount(1);
            sourceBuilder.aggregation(agg);
        }
        BoolQueryBuilder boolQueryBuilder = getBoolQuery();
        if (params.isKeywordExist()) {
            boolQueryBuilder.must(getBlacklistMultiMatch(params.getKeyword()));
        }
        sourceBuilder.query(boolQueryBuilder);
        sourceBuilder.size(0);
        sourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        Set<Integer> years = new HashSet<>();
        try {
            SearchResponse response = getResponse(sourceBuilder);
            for (String keyField : INJUNCTION_KEY_FIELDS) {
                Histogram terms = response.getAggregations().get(keyField);
                for (Histogram.Bucket bucket : terms.getBuckets()) {
                    final Date date = new Date().from(((ZonedDateTime) bucket.getKey()).toInstant());
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(date);
                    years.add(calendar.get(Calendar.YEAR));
                }
            }
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }
        LogUtil.debug(TAG, years);
        List<Integer> results = new ArrayList<>(years);
        results.sort(new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o2 - o1;
            }
        });
        return results;
    }

    public List<AggregateResult> extractProcurementType(BlacklistSearchQueryParam params) {
        LogUtil.debug(TAG, "get filter procurement type");
        List<AggregateResult> results = getTermsAggregation(
                params,
                "payload.jenis_pengadaan.keyword",
                "procurement");
        return results;
    }

    public List<AggregateResult> dateInjunctionAggregation(BlacklistSearchQueryParam params, String key) {
        LogUtil.debug(TAG, "get date aggregation by key: " + key);
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        AggregationBuilder agg = AggregationBuilders.dateHistogram(key)
                .field("payload.pelanggaran." + key)
                .calendarInterval(params.isYearExist()
                        ? DateHistogramInterval.MONTH
                        : DateHistogramInterval.YEAR
                )
                .minDocCount(1);
        sourceBuilder.aggregation(agg);
        BoolQueryBuilder boolQueryBuilder = getBoolQuery();
        if (params.isKeywordExist()) {
            boolQueryBuilder.must(getBlacklistMultiMatch(params.getKeyword()));
        }
        if (params.isYearExist()) {
            boolQueryBuilder.filter(
                    getYearRangeBuilder(
                            params,
                            "payload.pelanggaran." + key
                    )
            );
        }
        sourceBuilder.query(boolQueryBuilder);
        sourceBuilder.size(0);
        sourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        List<AggregateResult> results = new ArrayList<>();
        try {
            SearchResponse response = getResponse(sourceBuilder);
            final String format = params.isYearExist() ? MONTH_FORMAT : YEAR_FORMAT;
            Histogram terms = response.getAggregations().get(key);
            for (Histogram.Bucket bucket : terms.getBuckets()) {
                final Date date = new Date().from(((ZonedDateTime) bucket.getKey()).toInstant());
                final String name = new SimpleDateFormat(format).format(date);
                results.add(new AggregateResult(name, bucket.getDocCount()));
            }
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }
        LogUtil.debug(TAG, results);
        return results;
    }

    public Chartable getPublishedDateAggregation(BlacklistSearchQueryParam params) {
        LogUtil.debug(TAG, "get aggregation by published_date");
        Chartable result = new Chartable();
        try {
            int i = 1;
            for (Map.Entry<String, String> entry: INJUNCTION_MAP.entrySet()) {
                if (params.isInjunctionExist()
                        && !entry.getKey().equals(params.injunctionKeyField())) {
                    continue;
                }
                List<AggregateResult> aggregateResults = dateInjunctionAggregation(params, entry.getKey());
                List<Number> values = new ArrayList<>();
                for (AggregateResult aggResult : aggregateResults) {
                    values.add(aggResult.getCount());
                    result.labels.add(aggResult.getName());
                }
                result.datasets.add(new Chartable.ChartItem(entry.getValue(), values, i));
                i++;
            }
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }
        LogUtil.debug(TAG, result);
        return result;
    }

    public ElasticSearchResult blacklistSearch(BlacklistSearchQueryParam params) {
        ElasticSearchResult result = new ElasticSearchResult(params);
        if (result.exceedPageLimitation()) {
            LogUtil.debug(TAG, "exceed page limitation.. exit");
            return result;
        }
        SearchRequest request = new SearchRequest(MONEV_INDEX);
        BoolQueryBuilder boolQueryBuilder = getBoolQuery();
        if (params.isKeywordExist()) {
            boolQueryBuilder.must(getBlacklistMultiMatch(params.getKeyword()));
        } else {
            boolQueryBuilder.must(QueryBuilders.matchAllQuery());
        }
        if (params.isProvinceExist()) {
            boolQueryBuilder.filter(
                    QueryBuilders.termQuery(
                            "payload.provinsi.nama.keyword",
                            params.getProvince()));
        }
        if (params.isProcurementExist()) {
            boolQueryBuilder.filter(QueryBuilders.termQuery(
                    "payload.jenis_pengadaan.keyword",
                    params.getProcurement())
            );
        }
        if (params.isParticipateInProcurement()) {
            boolQueryBuilder.filter(
                    QueryBuilders.existsQuery(
                            "payload.participate_in_procurement"
                    )
            );
        }
        if (params.isYearExist()) {
            boolQueryBuilder.filter(
                    getYearRangeBuilder(params, "payload.pelanggaran.published_date")
            );
        }
        if (params.isInjunctionExist()) {
            boolQueryBuilder.filter(
                    QueryBuilders.existsQuery("payload.pelanggaran." + params.injunctionKeyField())
            );
        }
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        sourceBuilder.query(boolQueryBuilder);
        sourceBuilder.from(result.getOffset());
        sourceBuilder.size(result.getLimit());
        sourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        request.source(sourceBuilder);
        result.setItems(processingSearch(request, result));
        return result;
    }

    public List<AggregateResult> getViolationAggragation(BlacklistSearchQueryParam param) {
        return getTermsAggregation(
                param,
                "payload.pelanggaran.detil_pelanggaran.jenis_pelanggaran.keyword",
                "violation"
        );
    }

    public List<AggregateResult> getStatusAggregation(BlacklistSearchQueryParam param) {
        return getTermsAggregation(param, "payload.status.keyword", "status");
    }

    private RangeQueryBuilder getYearRangeBuilder(BlacklistSearchQueryParam params, String name) {
        return QueryBuilders.rangeQuery(name)
                .gte(params.getYear() + "-01-01T00:00:00")
                .lt(params.getYear() + "-12-31T00:00:00");
    }

    private List<AggregateResult> getTermsAggregation(
            BlacklistSearchQueryParam param,
            String field,
            String key
    ) {
        LogUtil.debug(TAG, "get aggregation by field: " + field + " with its key: " + key);
        List<AggregateResult> results = new ArrayList<>();
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        AggregationBuilder aggregationBuilder = AggregationBuilders
                .terms(key)
                .field(field)
                .order(BucketOrder.count(false))
                .size(5);
        BoolQueryBuilder boolQueryBuilder = getBoolQuery();
        if (param.isKeywordExist()) {
            boolQueryBuilder.must(getBlacklistMultiMatch(param.getKeyword()));
        }
        sourceBuilder.query(boolQueryBuilder);
        sourceBuilder.aggregation(aggregationBuilder);
        sourceBuilder.size(0);
        sourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        try {
            SearchResponse response = getResponse(sourceBuilder);
            Terms terms = response.getAggregations().get(key);
            for (Terms.Bucket bucket : terms.getBuckets()) {
                results.add(new AggregateResult(
                        bucket.getKey().toString(),
                        bucket.getDocCount()
                        )
                );
            }
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }
        LogUtil.debug(TAG, results);
        return results;
    }

    public Long getTotalByKldiAggragation(String kldiId) {
        LogUtil.debug(TAG, "get total by kldi id: " + kldiId);
        List<AggregateResult> regions = new ArrayList<>();
        SearchSourceBuilder sourceBuilder = new SearchSourceBuilder();
        AggregationBuilder aggregationBuilder = AggregationBuilders
                .count("by-kldi")
                .field("payload.kldi_id.keyword");
        BoolQueryBuilder boolQueryBuilder = getBoolQuery();
        boolQueryBuilder.filter(QueryBuilders.termQuery("payload.kldi_id.keyword", kldiId));
        sourceBuilder.query(boolQueryBuilder);
        sourceBuilder.aggregation(aggregationBuilder);
        sourceBuilder.size(0);
        sourceBuilder.timeout(new TimeValue(60, TimeUnit.SECONDS));
        try {
            SearchResponse response = getResponse(sourceBuilder);
            ValueCount terms = response.getAggregations().get("by-kldi");
            LogUtil.debug(TAG, "blacklist by kldi: " + kldiId+ " has total: " + terms.getValue());
            return terms.getValue();
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }
        LogUtil.debug(TAG, regions);
        return 0L;
    }

    private BoolQueryBuilder getBoolQuery() {
        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.filter(QueryBuilders.termQuery("model_type.keyword", ContentType.BLACKLIST));
        return boolQueryBuilder;
    }

    private SearchResponse getResponse(SearchSourceBuilder searchSource) throws IOException {
        SearchRequest request = new SearchRequest(MONEV_INDEX);
        request.searchType(SearchType.DFS_QUERY_THEN_FETCH);
        request.source(searchSource);
        return ElasticClient.client.search(request, RequestOptions.DEFAULT);
    }

}
