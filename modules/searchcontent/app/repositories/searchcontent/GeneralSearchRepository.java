package repositories.searchcontent;

import models.searchcontent.ElasticSearchResult;
import models.searchcontent.SearchQueryParams;
import models.user.management.contracts.UserContract;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import utils.common.LogUtil;

/**
 * @author HanusaCloud on 11/13/2019 8:40 AM
 */
public class GeneralSearchRepository extends ElasticRepository {

    public static final String TAG = "GeneralSearchRepository";
    private static final GeneralSearchRepository repository = new GeneralSearchRepository();

    public static GeneralSearchRepository getInstance() {
        return repository;
    }

    public ElasticSearchResult search(SearchQueryParams params, UserContract user) {
        ElasticSearchResult result = new ElasticSearchResult(params);
        if (result.exceedPageLimitation()) {
            LogUtil.debug(TAG, "exceed page limitation.. exit");
            return result;
        }
        SearchRequest request = new SearchRequest(MONEV_INDEX);
        request.searchType(SearchType.DFS_QUERY_THEN_FETCH);
        BoolQueryBuilder boolQueryBuilder = getBoolQueryBuilder(user);
        if (params.isKeywordExist()) {
            boolQueryBuilder.must(getMultiMatch(params.getKeyword(), ALL_FIELDS));
        }
        SearchSourceBuilder sourceBuilder = getBaseSourceBuilder(params);
        sourceBuilder.query(boolQueryBuilder);
        sourceBuilder.from(result.getOffset());
        sourceBuilder.size(result.getLimit());
        request.source(sourceBuilder);
        LogUtil.debug(TAG, request);
        result.setItems(processingSearch(request, result));
        return result;
    }

}
