package models.searchcontent;

import models.common.contracts.TablePagination;

import java.util.List;

/**
 * @author HanusaCloud on 11/12/2019 10:45 AM
 */
public class ElasticSearchResult implements TablePagination.PageAble<SearchResultItem> {

    private int total;
    private List<SearchResultItem> items;
    private final TablePagination.ParamAble paramAble;
    private Meta meta;

    public ElasticSearchResult(TablePagination.ParamAble paramAble) {
        this.paramAble = paramAble;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public TablePagination.ParamAble getParamAble() {
        return paramAble;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setItems(List<SearchResultItem> collection) {
        this.items = collection;
    }

    @Override
    public List<SearchResultItem> getItems() {
        return items;
    }

    @Override
    public Class<SearchResultItem> getReturnType() {
        return SearchResultItem.class;
    }

    @Override
    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    @Override
    public Meta getMeta() {
        return meta;
    }

    @Override
    public int getLimit() {
        return 12;
    }
}
