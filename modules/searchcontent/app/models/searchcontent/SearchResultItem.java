package models.searchcontent;

import models.Blacklist;
import models.cms.Content;
import models.cms.contracts.ContentTypeContract;
import models.common.contracts.CreationContract;
import models.searchcontent.contracts.ElasticItemDetail;
import org.elasticsearch.search.SearchHit;
import procurementprofile.models.ProcurementProfile;
import utils.common.JsonUtil;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Map;

/**
 * @author HanusaCloud on 11/6/2019 4:19 PM
 */
public class SearchResultItem implements Serializable, ContentTypeContract {

    private final Integer id;
    private final String type;
    private final String url;
    private final String createdAt;
    private final ElasticItemDetail detail;

    public SearchResultItem(SearchHit hit) {
        Map<String, Object> source = hit.getSourceAsMap();
        id = (Integer) source.get("model_id");
        type = (String) source.get("model_type");
        url = (String) source.get("model_url");
        this.createdAt = CreationContract.dateFormat((String) source.get("model_created_at"));
        final Object object = source.get("payload");
        final String objectJson = JsonUtil.toJson(object);
        this.detail = JsonUtil.fromJson(objectJson, (Type) getClassType());
    }

    private Class getClassType() {
        if (isBlacklist()) {
            return Blacklist.Data.class;
        }
        if (isProcurementProfile()) {
            return ProcurementProfile.class;
        }
        return Content.class;
    }

    public Integer getId() {
        return id;
    }

    public ElasticItemDetail getDetail() {
        return detail;
    }

    public String getType() {
        return type;
    }

    public String getUrl() {
        return url != null ? url : "";
    }

    public String getMeta() {
        if (getDetail().getMeta().isEmpty()) {
            return "Tayang pada " + createdAt;
        }
        return getDetail().getMeta();
    }

    public String toJson() {
        return JsonUtil.toJson(detail);
    }

    public String getTypeAsColorCode() {
        if (isBlacklist()) {
            return "black";
        } else if (isTableau()) {
            return "grey";
        } else if (isGeneralArticle()) {
            return "teal";
        } else if (isProcurementProfile()) {
            return "blue";
        } else {
            return "green";
        }
    }

    @Override
    public String getContentType() {
        return getType();
    }

}
