package models.searchcontent;

import models.searchcontent.contracts.ElasticRequestPayloadContract;
import org.apache.commons.lang3.StringUtils;

/**
 * @author HanusaCloud on 11/6/2019 9:18 AM
 */
public class ElasticRequest<T extends ElasticRequestPayloadContract> {

    public final Long model_id;
    public final String model_type;
    public final String model_url;
    public final String model_created_at;
    public final T payload;

    public ElasticRequest(T payload) {
        this.payload = payload;
        model_id = payload.getModelId();
        model_type = payload.getModelType();
        model_url = payload.getModelUrl();
        final String created = payload.getStandardFormattedCreatedAt();
        model_created_at = StringUtils.isEmpty(created) ? null : created;
        payload.setModelRequirements();
    }

}
