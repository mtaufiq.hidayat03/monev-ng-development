package models.searchcontent.contracts;

import models.common.contracts.CreationContract;

/**
 * @author HanusaCloud on 11/6/2019 9:25 AM
 */
public interface ElasticRequestPayloadContract extends CreationContract {

    String CREATED_AT = "yyyy-MM-ddTHH:mm:ss";

    Long getModelId();
    String getModelType();
    String getModelUrl();
    void setModelRequirements();

    default String getGeneratedId() {
        return getModelId() + "" + getModelType().replaceAll("\\s", "").toLowerCase();
    }

}
