package models.searchcontent.contracts;

/**
 * @author HanusaCloud on 11/8/2019 4:38 PM
 */
public interface ElasticItemDetail {

    String getDetailTitle();
    String getDetailContent();
    String getMeta();
    String getPermission();

    default String getImageUrl() {
        return "";
    }

    default String getDetailDownloadFileUrl() {
        return "";
    }

}
