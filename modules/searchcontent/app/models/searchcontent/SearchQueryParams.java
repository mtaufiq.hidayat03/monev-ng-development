package models.searchcontent;

import models.common.contracts.TablePagination;
import models.common.contracts.TablePagination.Param;
import play.mvc.Scope;
import repositories.searchcontent.ElasticRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 11/12/2019 10:20 AM
 */
public class SearchQueryParams implements TablePagination.ParamAble {

    private final Param[] params;
    private final int page;
    private final String keyword;
    private final String contentType;
    private final Integer sort;

    public SearchQueryParams(Scope.Params params) {
        page = getCurrentPage(params);
        keyword = generateKeyword(params);
        sort = getInteger(params, getSortKey(), isKeywordExist() ? 0 : 2);
        contentType = params.get("contentType");
        List<Param> results = new ArrayList<>();
        if (isKeywordExist()) {
            results.add(new Param(getSearchKey(), keyword));
        }
        if (contentTypeExist()) {
            results.add(new Param("contentType", contentType));
        }
        if (sort != null) {
            results.add(new Param(getSortKey(), sort));
        }
        Param[] arr = new Param[results.size()];
        this.params = results.toArray(arr);
    }

    @Override
    public Param[] getParams() {
        return params;
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public String getKeyword() {
        return keyword;
    }

    public boolean contentTypeExist() {
        return getContentType() != null && !getContentType().isEmpty();
    }

    public String getContentType() {
        return contentType;
    }

    public Integer getSort() {
        return sort;
    }

    public String getSortLabel() {
        if (getSort() == null) {
            return "";
        }
        return ElasticRepository.SORTS[getSort()];
    }

}
