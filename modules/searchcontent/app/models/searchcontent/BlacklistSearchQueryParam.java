package models.searchcontent;

import controllers.searchcontent.BlacklistSearchController;
import models.common.contracts.TablePagination;
import models.common.contracts.TablePagination.Param;
import org.apache.commons.lang3.StringUtils;
import play.mvc.Scope;

import java.util.*;

/**
 * @author HanusaCloud on 11/12/2019 5:32 PM
 */
public class BlacklistSearchQueryParam implements TablePagination.ParamAble {

    public static final String[] INJUNCTION_KEY_FIELDS = {"published_date", "withdrawal_date"};
    public static final Map<String, String> INJUNCTION_MAP;

    static {
        INJUNCTION_MAP = new LinkedHashMap<>();
        INJUNCTION_MAP.put(INJUNCTION_KEY_FIELDS[0], "Penayangan");
        INJUNCTION_MAP.put(INJUNCTION_KEY_FIELDS[1], "Penurunan");
    }

    private final Param[] params;
    private final int page;
    private final String keyword;
    private final Integer domicile;
    private final String province;
    private final Integer filter;
    private final Integer year;
    private final String procurement;
    private final Integer injunction;
    private final String status;

    public BlacklistSearchQueryParam(Scope.Params params) {
        keyword = generateKeyword(params);
        page = getCurrentPage(params);
        province = params.get("province");
        filter = getInteger(params, "filter", 2);
        domicile = getInteger(params, "domicile", isItDomicile() ? 0 : null);
        year = getInteger(params, "year", 0);
        procurement = getString(params, "procurement", isProcurementType() ? "Semua Jenis" : null);
        injunction = getInteger(params, "injunction", -1);
        status = getString(params, "status");
        List<Param> results = new ArrayList<>();
        if (isKeywordExist()) {
            results.add(new Param("search", keyword));
        }
        if (isFilterExist()) {
            results.add(new Param("filter", filter));
        }
        if (year != null) {
            results.add(new Param("year", year));
        }
        if (isProcurementExist()) {
            results.add(new Param("procurement", procurement));
        }
        if (isInjunctionExist()) {
            results.add(new Param("injunction", injunction));
        }
        if (isStatusExists()) {
            results.add(new Param("status", status));
        }
        Param[] arr = new Param[results.size()];
        this.params = results.toArray(arr);
    }

    @Override
    public Param[] getParams() {
        return params;
    }

    @Override
    public int getPage() {
        return page;
    }

    @Override
    public String getKeyword() {
        return keyword;
    }

    public String getProvince() {
        return province;
    }

    public Integer getFilterDomicile() {
        return domicile;
    }

    public boolean isFilterDomicileExist() {
        return domicile != null;
    }

    public boolean isProvinceFilter() {
        return isFilterDomicileExist() && getFilterDomicile() == 1;
    }

    public boolean isNationalFilter() {
        return isFilterDomicileExist() && getFilterDomicile() == 0;
    }

    public boolean isFilterExist() {
        return filter != null;
    }

    public Integer getFilter() {
        return filter;
    }

    public boolean isItPublishedDate() {
        return isFilterExist() && getFilter() == 2;
    }

    public boolean isViolation() {
        return isFilterExist() && getFilter() == 0;
    }

    public boolean isItDomicile() {
        return isFilterExist() && getFilter() == 1;
    }

    public boolean isProcurementType() {
        return isFilterExist() && getFilter() == 3;
    }

    public boolean isProvinceExist() {
        return !StringUtils.isEmpty(province);
    }

    public boolean isProcurementExist() {
        return !StringUtils.isEmpty(procurement) && !procurement.equals("Semua Jenis");
    }

    public boolean isParticipateInProcurement() {
        return isFilterExist() && getFilter() == 4;
    }

    public String getYearAsString() {
        return year == null || year == 0 ? "Semua Tahun" : year.toString();
    }

    public String getInjunctionAsString() {
        return getInjunction() == null
                || getInjunction() == -1
                    ? "Semua"
                    : INJUNCTION_MAP.get(injunctionKeyField());
    }

    public Integer getInjunction() {
        return injunction < 0
                || injunction >= INJUNCTION_MAP.size()
                    ? -1
                    : injunction;
    }

    public String getProcurement() {
        return procurement;
    }

    public Integer getYear() {
        return year;
    }

    public boolean isYearExist() {
        return year != null && year > 0;
    }

    public boolean isInjunctionExist() {
        return injunction != null && injunction > -1 && injunction < INJUNCTION_MAP.size();
    }

    public String injunctionKeyField() {
        return isInjunctionExist() ? INJUNCTION_KEY_FIELDS[getInjunction()] : "";
    }

    public Collection<String> getInjunctionLabels() {
        return INJUNCTION_MAP.values();
    }

    public String getDomicileLabel() {
        if (!isFilterDomicileExist()
                || getFilterDomicile() > BlacklistSearchController.DOMICILES.length) {
            return "";
        }
        return BlacklistSearchController.DOMICILES[getFilterDomicile()];
    }

    public String getFilterLabel() {
        if (!isFilterExist()) {
            return  "";
        }
        return BlacklistSearchController.FILTERS[getFilter()];
    }

    public String getStatus() {
        return status;
    }

    public boolean isStatusExists() {
        return !StringUtils.isEmpty(status);
    }
}
