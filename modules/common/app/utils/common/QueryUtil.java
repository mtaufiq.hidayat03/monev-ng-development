package utils.common;

/**
 * @author HanusaCloud on 1/14/2020 10:30 AM
 */
public class QueryUtil {

    public static String sanitize(String value) {
        if (value == null || value.isEmpty()) {
            return "";
        }

        return value.replaceAll("'", "''")
                .replace("\\", "\\\\")
                .replaceAll("\b","\\b")
                .replaceAll("\n","\\n")
                .replaceAll("\r", "\\r")
                .replaceAll("\t", "\\t")
                .replaceAll("\\x1A", "\\Z")
                .replaceAll("\\x00", "\\0")
                .replaceAll("'", "\\'")
                .replaceAll("\"", "\\\"");
    }

    public static String searchLikeQuery(String[] columns, String keyword) {
        final String sanitizedKeyword = sanitize(keyword);
        StringBuilder sb = new StringBuilder();
        String glue = "";
        sb.append("(");
        for (String column : columns) {
            sb.append(glue)
                    .append(column)
                    .append(" ILIKE ").append("'%").append(sanitizedKeyword).append("%'");
            glue = " OR ";
        }
        sb.append(")");
        return sb.toString();
    }

}