package utils.common;

import io.sentry.SentryClient;
import io.sentry.SentryClientFactory;
import io.sentry.context.Context;
import io.sentry.event.BreadcrumbBuilder;
import io.sentry.event.UserBuilder;
import models.user.management.CurrentUser;
import models.user.management.contracts.UserContract;
import org.apache.commons.lang3.StringUtils;
import play.Play;

import java.util.Date;

/**
 * @author HanusaCloud on 9/9/2019 2:53 PM
 */
public class SentryUtil {
    
    public static final String TAG = "SentryUtil";

    private static SentryClient sentryClient;
    private static final String TYPE_UNCAUGHT = "UNCAUGHT";
    private static final String TYPE_HANDLED = "HANDLED";

    static  {
        LogUtil.debug(TAG, "init sentry util");
        final String dsn = Play.configuration.getProperty("sentry.dsn");
        LogUtil.debug(TAG, "sentry dsn: " + dsn);
        if (!StringUtils.isEmpty(dsn)) {
            sentryClient = SentryClientFactory.sentryClient(dsn);
        }
    }

    public static void uncaught(Throwable t) {
        catchError(t, TYPE_UNCAUGHT);
    }

    public static void catchError(Throwable t) {
        catchError(t, TYPE_HANDLED);
    }

    private static void catchError(Throwable e, String type) {
        if (sentryClient == null) {
            LogUtil.debug(TAG, "Oops.. you don't configure your sentry");
            return;
        }
        try {
            LogUtil.debug(TAG, "send error to sentry.io, type: " + type);
            final CurrentUser currentUser = CurrentUser.getInstance();
            final UserContract user = currentUser == null ? null : currentUser.user;

            Context context = sentryClient.getContext();
            // Record a breadcrumb in the current context. By default the last 100 breadcrumbs are kept.
            final String env = Play.mode.isDev() ? "Development" : "Production";
            context.recordBreadcrumb(new BreadcrumbBuilder()
                    .setCategory(env)
                    .setMessage("Catch unhandled error")
                    .setTimestamp(new Date())
                    .build());
            context.addExtra("environment", env);
            context.addTag("environment", env);
            final String name = user == null ? "non-authorized-user" : user.getName();
            context.addTag("user", name);
            context.addTag("type", type);
            // Set the user in the current context.
            final String emailAddress = user == null ? "non-authorized-user" : user.getEmail();
            final long userId = user == null ? 0 : user.getUserId();
            context.setUser(new UserBuilder()
                    .setEmail(emailAddress)
                    .setUsername(name)
                    .setId(String.valueOf(userId))
                    .build());
            sentryClient.sendException(e);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
