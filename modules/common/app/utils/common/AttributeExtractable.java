package utils.common;


import com.google.gson.internal.Primitives;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

public interface AttributeExtractable {

    default Number getNumber(String attribute) {
        try {
            Class<?> clazz = getClass();
            if (attribute.endsWith("()")) {
                Method m = clazz.getDeclaredMethod(attribute.replaceAll("\\(\\)", ""));
                return (Number) m.invoke(this);
            } else {
                Field f = clazz.getDeclaredField(attribute);
                return (Number) f.get(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    default <T> T get(String attribute, Class<T> classOfT) {
        try {
            Class<?> clazz = getClass();
            if (attribute.endsWith("()")) {
                Method m = clazz.getDeclaredMethod(attribute.replaceAll("\\(\\)", ""));
                return Primitives.wrap(classOfT).cast(m.invoke(this));
            } else {
                Field f = clazz.getDeclaredField(attribute);
                return Primitives.wrap(classOfT).cast(f.get(this));
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
