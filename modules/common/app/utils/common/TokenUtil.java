package utils.common;

import play.libs.Codec;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * @author HanusaCloud on 9/18/2019 4:03 PM
 */
public class TokenUtil {

    public static final String TAG = "TokenUtil";

    public static String md5Digest(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("md5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            LogUtil.error(TAG, e);
        }
        return null;
    }

    public static String sha256(String value) {
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.update(value.getBytes(StandardCharsets.UTF_8));
            byte[] digested = messageDigest.digest();
            return Codec.byteToHexString(digested);
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            return null;
        }
    }

    public static String generateMd5Token(String value) {
        LogUtil.debug(TAG, "generate md5 token");
        UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date dt = new Date();
        String currentDateTimeString = dateFormat.format(dt);
        final String result = md5Digest(value + currentDateTimeString + randomUUIDString);
        LogUtil.debug(TAG, "token: " + result);
        return result;
    }

}
