package utils.common;

import models.cms.ContentFolder;

import java.io.File;

public class DirectoryUtil {
    private static final String TAG = "DirectoryUtil";

    public static void createDir(String path) {
        LogUtil.debug(TAG, "Path :"+path);
        File file =  new File(ContentFolder.APP_PATH+path);
        if (!file.exists() && file.mkdirs()) {
            LogUtil.debug(TAG, "directory is created");
        }
    }

}
