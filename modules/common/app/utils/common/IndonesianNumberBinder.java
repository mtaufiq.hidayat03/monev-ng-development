package utils.common;

import org.apache.commons.lang3.StringUtils;
import play.data.binding.TypeBinder;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

public class IndonesianNumberBinder implements TypeBinder<Number> {
    @Override
    public Number bind(String name, Annotation[] annotations, String value, Class actualClass, Type genericType) throws Exception {
        if(StringUtils.isEmpty(value))
            return null;
        value = value.replaceAll("\\.", "");
        // check using contains instead of equals, because getTypeName usually returns className with packageName
        if (genericType.getTypeName().toLowerCase().contains("long")) {
            return Long.parseLong(value);
        }
        return Integer.parseInt(value);
    }
}
