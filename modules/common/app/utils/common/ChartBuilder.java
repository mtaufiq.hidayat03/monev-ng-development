package utils.common;

import models.common.contracts.Chartable;
import org.joda.time.LocalDate;

import java.util.*;

public class ChartBuilder {

    public static Chartable buildTimeSeries(LocalDate start, LocalDate end, List<String> seriesLabels, List<Chartable.ChartQueryResult> result) {
        // ambil label x axis
        // karena ini based on tanggal, kita langsung loop saja tanggal/bulan awal sampai tanggal/bulan akhir
        Map<String, Integer> monthIndexes = new TreeMap<>();
        Chartable chartable = new Chartable();
        LocalDate tmpTime = start;
        int labelIndex = 0;
        while (tmpTime.getYear() < end.getYear() || tmpTime.getMonthOfYear() < end.getMonthOfYear()) {
            String monthFormatted = DateFormatter.format(tmpTime.toDate(), "MMMM yy");
            chartable.labels.add(monthFormatted);
            monthIndexes.put(monthFormatted, labelIndex);
            tmpTime = tmpTime.plusMonths(1);
            ++labelIndex;
        }

        // sediakan tempat untuk menampung data series, dan setiap titik sumbu x kita isi dengan 0
        int seriesIndex = 0;
        for (String seriesLabel: seriesLabels) {
            chartable.datasets.add(new Chartable.ChartItem(seriesLabel, new ArrayList<>(Collections.nCopies(chartable.labels.size(), 0)), ++seriesIndex));
        }

        // masukkan data ke series
        for (Chartable.ChartQueryResult item: result) {
            int monthIndex = monthIndexes.get(item.timeLabel);
            chartable.put(item.seriesLabel, item.data, monthIndex);
        }

        return chartable;
    }

    public static Chartable buildYearSeries(int start, int end, List<String> attributes, List<String> attributeLabels, List<? extends ExtractableChartQueryResult> data) {
        // ambil label x axis
        Map<String, Integer> yearIndexes = new TreeMap<>();
        Chartable chartable = new Chartable();
        int labelIndex = 0;
        for (int y=start; y<=end; y++) {
            String yearString = String.valueOf(y);
            chartable.labels.add(yearString);
            yearIndexes.put(yearString, labelIndex);
            ++labelIndex;
        }

        // sediakan tempat untuk menampung data series, dan setiap titik sumbu x kita isi dengan 0
        int seriesIndex = 0;
        for (String seriesLabel: attributeLabels) {
            chartable.datasets.add(new Chartable.ChartItem(seriesLabel, new ArrayList<>(Collections.nCopies(chartable.labels.size(), 0)), ++seriesIndex));
        }

        // masukkan data ke series
        for (ExtractableChartQueryResult item: data) {
            int yearIndex = yearIndexes.get(item.getXAxisLabel());
            for (int i=0; i<attributes.size(); i++) {
                String attribute = attributes.get(i);
                String attributeLabel = attributeLabels.get(i);
                chartable.put(attributeLabel, item.getNumber(attribute), yearIndex);
            }
        }

        return chartable;
    }

    public static Chartable buildNmostBar(List<String> attributes, List<String> attributeLabels, List<? extends ExtractableChartQueryResult> data, int max) {
        Chartable chartable = new Chartable();
        if (data != null) {
            for (int i = 0; i < data.size() && i < max; i++) {
                ExtractableChartQueryResult item = data.get(i);
                chartable.labels.add(item.getXAxisLabel());
                for (int j = 0; j < attributes.size(); j++) {
                    String attribute = attributes.get(j);
                    String attributeLabel = attributeLabels.get(j);
                    chartable.add(attributeLabel, item.getNumber(attribute));
                }
            }
        }

        return chartable;
    }

    public static Chartable buildPie(String chartTitle, List<String> attributeLabels, List<Number> data) {
        Chartable chartable = new Chartable();
        chartable.labels = new LinkedHashSet<>(attributeLabels);
        chartable.datasets.add(new Chartable.ChartItem(chartTitle, data, 1));

        return chartable;
    }

    public static Chartable buildDonut(String chartTitle, List<String> attributeLabels, List<Number> data) {
        return buildPie(chartTitle, attributeLabels, data);
    }
}
