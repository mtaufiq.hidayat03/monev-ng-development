package utils.common;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author HanusaCloud on 9/18/2019 4:25 PM
 */
public class MailUtil {

    public static final String TAG = "MailUtil";

    public static Timestamp expiredDate()  {
        LogUtil.debug(TAG, "generate expiration date");
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR, 24);
        java.util.Date expiredDate = cal.getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        String expired_date = dateFormat.format(expiredDate);
        return java.sql.Timestamp.valueOf(expired_date);
    }

}
