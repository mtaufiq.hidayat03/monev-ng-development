package utils.common;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 12/21/2019 8:48 AM
 */
public class IdUtils {

    public static final String TAG = "IdUtils";

    public static List<Long> getNumericalsFrom(String stringValue) {
        LogUtil.debug(TAG, "get id from: " + stringValue);
        List<Long> results = new ArrayList<>();
        if (StringUtils.isEmpty(stringValue)) {
            LogUtil.debug(TAG, "Oops.. value is empty");
            return results;
        }
        try {
            String[] values = stringValue.split(",");
            if (values.length <= 0) {
                return results;
            }
            for (String value : values) {
                if (StringUtils.isEmpty(value)) {
                    continue;
                }
                String cleaned = value.replaceAll("\\s", "");
                if (StringUtils.isNumeric(cleaned)) {
                    results.add(Long.valueOf(cleaned));
                }
            }
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }
        return results;
    }

    public static String turnIdsToIns(List ids) {
        if (ids == null || ids.isEmpty()) {
            return "";
        }
        StringBuilder sb = new StringBuilder();
        String glue = "";
        for (Object value : ids) {
            sb.append(glue).append("'").append(value).append("'");
            glue = ", ";
        }
        return sb.toString();
    }

    public static boolean isNumeric(String str) {
        for (char c : str.toCharArray()) {
            if (!Character.isDigit(c)) return false;
        }
        return true;
    }

}
