package utils.common;

public interface ExtractableChartQueryResult extends AttributeExtractable {
    String getXAxisLabel();
}
