package utils.common;

import org.apache.commons.lang3.StringUtils;
import play.data.binding.TypeBinder;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

public class IndonesianDecimalBinder implements TypeBinder<Double> {
    @Override
    public Double bind(String name, Annotation[] annotations, String value, Class actualClass, Type genericType) throws Exception {
        if(StringUtils.isEmpty(value))
            return null;
        value = value.replaceAll("\\.", "");
        value = value.replaceAll(",", ".");
        return Double.valueOf(value);
    }
}
