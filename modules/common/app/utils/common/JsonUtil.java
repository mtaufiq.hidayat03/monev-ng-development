package utils.common;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author HanusaCloud on 8/26/2019 1:17 PM
 */
public class JsonUtil {

    private static final Gson gson = new Gson();
    private static final Gson gsonId = new GsonBuilder()
            .registerTypeAdapter(Date.class,
                    (JsonDeserializer<Date>) (jsonElement, type, jsonDeserializationContext) -> {
        DateFormat longDf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        DateFormat shortDf = new SimpleDateFormat("dd-MM-yyyy");
        try {
            return longDf.parse(jsonElement.getAsString());
        } catch (ParseException e) {
            try {
                return shortDf.parse(jsonElement.getAsString());
            } catch (ParseException ex) {
                return null;
            }
        }
    }).create();

    public static String toJson(final Object src) {
        if (src == null) {
            return toJson(JsonNull.INSTANCE);
        }
        return gson.toJson(src, src.getClass());
    }

    public String toJson(final Object src, Type typeOfSrc) {
        return gson.toJson(src, typeOfSrc);
    }

    public static <T> T fromJson(final String json, Class<T> classOfT) {
        return gson.fromJson(json, classOfT);
    }

    public static <T> T fromJson(final String json, Type typeOfT) throws JsonIOException, JsonSyntaxException {
        return gson.fromJson(json, typeOfT);
    }

    public static <T> T fromJson(final JsonElement json, Class<T> classOfT) {
        return gson.fromJson(json, classOfT);
    }


    /******* ID ********/
    public static String toJsonId(final Object src) {
        if (src == null) {
            return toJson(JsonNull.INSTANCE);
        }
        return gsonId.toJson(src, src.getClass());
    }

    public String toJsonId(final Object src, Type typeOfSrc) {
        return gsonId.toJson(src, typeOfSrc);
    }

    public static <T> T fromJsonId(final String json, Class<T> classOfT) {
        return gsonId.fromJson(json, classOfT);
    }

    public static <T> T fromJsonId(final String json, Type typeOfT) throws JsonIOException, JsonSyntaxException {
        return gsonId.fromJson(json, typeOfT);
    }

    public static <T> T fromJsonId(final JsonElement json, Class<T> classOfT) {
        return gsonId.fromJson(json, classOfT);
    }

}
