package utils.common;

/**
 * @author HanusaCloud on 9/30/2019 10:54 AM
 */
public class FormatterUtil {

    public static final long THOUSAND = 1000L;
    public static final long MILLION = 1000000L;
    public static final long BILLION = 1000000000L;
    public static final long TRILLION = 1000000000000L;

    public static String currencyReadableFormatter(double value) {
        double devident = 1;
        String symbol = "";
        if (value > THOUSAND && value < MILLION) {
            devident = 1000.0;
            symbol = "Rb";
        } else if (value >= MILLION && value < BILLION) {
            devident = 1000000.0;
            symbol = "Jt";
        } else if (value >= BILLION && value < TRILLION) {
            devident = 1000000000.0;
            symbol = "M";
        } else if (value >= TRILLION) {
            devident = 1000000000000.0;
            symbol = "T";
        }
        return String.format("%.0f " + symbol, value / devident);
    }

}
