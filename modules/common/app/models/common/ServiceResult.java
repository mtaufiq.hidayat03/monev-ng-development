package models.common;

import play.i18n.Messages;
import utils.common.JsonUtil;

/**
 * @author HanusaCloud on 9/17/2019 4:36 PM
 */
public class ServiceResult<T> {

    public final String message;
    public final boolean status;
    public final T payload;

    public ServiceResult(boolean status, String message, T payload) {
        this.message = generateMessage(message);
        this.status = status;
        this.payload = payload;
    }

    public ServiceResult(String message) {
        this.status = false;
        this.message = generateMessage(message);
        this.payload = null;
    }

    public ServiceResult(boolean status, String message) {
        this.message = generateMessage(message);
        this.status = status;
        this.payload = null;
    }

    public ServiceResult(String message, T payload) {
        this.message = generateMessage(message);
        this.status = false;
        this.payload = payload;
    }

    private String generateMessage(String value) {
        String result = "";
        if (!(result = Messages.get(value)).equals(value)) {
            return result;
        }
        return value;
    }

    public String toJson() {
        return JsonUtil.toJson(this);
    }

}
