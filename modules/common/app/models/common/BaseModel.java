package models.common;

import models.user.management.CurrentUser;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import utils.common.LogUtil;

import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.sql.Timestamp;

/**
 * @author HanusaCloud on 8/26/2019 5:14 PM
 */
@MappedSuperclass
public class BaseModel extends BaseTable implements Serializable {

    public static final String BASE_TAG = "BaseModel";

    @Id
    public Long id;

    public Timestamp created_at;
    public Long created_by;
    protected Timestamp updated_at;
    protected Long updated_by;
    protected Timestamp deleted_at;
    protected Long deleted_by;

   /* public Long created_by() {
        CurrentUser session = CurrentUser.getInstance();
        Long currentUser;
        currentUser = session.user.getUserId();
        return currentUser;
    } */

    @Override
    protected void prePersist() {
        CurrentUser session = CurrentUser.getInstance();
        Long currentUser;
        if (session == null
                || session.user == null
                || (currentUser = session.user.getUserId()) == null) {
            currentUser = -999L;
        }

        if (this.id == null) {
            LogUtil.debug(BASE_TAG, "id null set created_*");
            created_at = new Timestamp(System.currentTimeMillis());
            created_by = currentUser;
        }

        if (this.id != null) {
            LogUtil.debug(BASE_TAG, "id is not null set updated_*");
            updated_at = new Timestamp(System.currentTimeMillis());
            updated_by = currentUser;
        }
        LogUtil.debug(BASE_TAG, "current user: " + currentUser);
        if (this.id == null) {
            final String sequenceName = getClassSequence() + "_id_seq";
            id = Query.find("SELECT nextval('" + sequenceName + "'::regclass)", Long.class).first();
        }

    }

    protected void setDeleted() {
        deleted_at = new Timestamp(System.currentTimeMillis());
        CurrentUser session = CurrentUser.getInstance();
        if (session == null
                || session.user == null
                || (deleted_by = session.user.getUserId()) == null) {
            deleted_by = -999L;
        }
        LogUtil.debug(BASE_TAG, "deleted_by: " + deleted_by);
    }

    public void saveModel() {
        final long result = save();
        if (this.id == null) {
            this.id = result;
        }
    }

    public void deleteModel() {
        setDeleted();
        saveModel();
    }

    public String getClassSequence() {
        final String name = getTableName();
        return name.contains(".") ? name.substring(name.indexOf(".") + 1) : name;
    }

}
