package models.common.contracts;

/**
 * @author HanusaCloud on 8/27/2019 10:57 AM
 */
public interface FormContract {

    String CREATE = "create";
    String UPDATE = "update";

    String getAction();

    default boolean isCreatedMode() {
        return getAction().equalsIgnoreCase(CREATE);
    }
    default boolean isEditedMode() {
        return getAction().equalsIgnoreCase(UPDATE);
    }

}