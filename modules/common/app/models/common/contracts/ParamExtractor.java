package models.common.contracts;

import play.mvc.Scope;
import utils.common.LogUtil;
import utils.common.QueryUtil;

/**
 * @author HanusaCloud on 1/4/2020 3:08 PM
 */
public interface ParamExtractor {

    String TAG = "ParamExtractor";

    default String generateKey(String key) {
        final String result = getKeyPrefix() == null || getKeyPrefix().isEmpty()
                ? key : getKeyPrefix() + "-" + key;
        LogUtil.debug(TAG, "generated key: " + result);
        return result;
    }

    default String getKeyPrefix() {
        return "";
    }

    default Integer getInteger(Scope.Params params, String key, Integer defaultValue) {
        final Integer value = params.get(key, Integer.class);
        return value == null ? defaultValue : value;
    }

    default Integer getInteger(Scope.Params params, String key) {
        return getInteger(params, key, null);
    }

    default String getString(Scope.Params params, String key) {
        return getString(params, key, "");
    }

    default String getString(Scope.Params params, String key, String dafaultValue) {
        final String value = params.get(key, String.class);
        return value == null ? dafaultValue : QueryUtil.sanitize(value);
    }

    default boolean isNull(Scope.Params params, String key) {
        return params.get(key) == null;
    }

}
