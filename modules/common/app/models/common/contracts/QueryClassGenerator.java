package models.common.contracts;

import models.common.contracts.annotations.SearchAble;
import models.common.contracts.annotations.SoftDelete;
import org.apache.commons.lang3.StringUtils;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import utils.common.LogUtil;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 1/14/2020 2:25 PM
 */
public interface QueryClassGenerator {

    String QUERY_TAG = "QueryClassGenerator";

    default void setTotal(int total) {

    }

    default int getTotal() {
        return 0;
    }

    default boolean isEmpty() {
        return getTotal() == 0;
    }

    default Object[] getBareSelectQuery(Annotation[] annotations) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        List<String> columns = extractColumns();
        sb.append(generateColumnQuery(columns));
        sb.append(" FROM ");
        Boolean isSoftDeleteExist = false;
        for (Annotation annotation : annotations) {
            if (annotation instanceof Table) {
                if (!StringUtils.isEmpty(((Table) annotation).schema())) {
                    sb.append(((Table) annotation).schema())
                            .append(".");
                }
                sb.append(((Table) annotation).name());
                continue;
            }
            if (annotation instanceof SoftDelete) {
                isSoftDeleteExist = true;
            }
        }
        sb.append(" ");
        return new Object[]{sb.toString(), isSoftDeleteExist};
    }

    default String getTableName(Annotation[] annotations) {
        StringBuilder sb = new StringBuilder();
        for (Annotation annotation : annotations) {
            if (annotation instanceof Table) {
                if (!StringUtils.isEmpty(((Table) annotation).schema())) {
                    sb.append(((Table) annotation).schema())
                            .append(".");
                }
                sb.append(((Table) annotation).name());
            }
        }
        return sb.toString();
    }

    default List<String> extractColumns() {
        return null;
    }

    default String generateColumnQuery(List<String> columns) {
        StringBuilder sb = new StringBuilder();
        if (columns != null && !columns.isEmpty()) {
            String glue = "";
            for (String s : columns) {
                sb.append(glue).append(s);
                glue = ", ";
            }
        } else {
            sb.append("*");
        }
        return sb.toString();
    }

    default void generateTotal(String query) {
        final int total = Query.count("SELECT COUNT(*) FROM (" + query + ") p", Integer.class);
        LogUtil.debug(QUERY_TAG, "total: " + total);
        setTotal(total);
    }

    default List<String> extractSearchAbleFields(Field[] fields) {
        List<String> results = new ArrayList<>();
        for (Field field : fields) {
            for (Annotation annotation : field.getAnnotations()) {
                if (annotation instanceof SearchAble) {
                    results.add(field.getName());
                }
            }
        }
        return results;
    }

    default String generateSearchableQuery(List<String> searchAbleFields, String keyword) {
        StringBuilder sb = new StringBuilder();
        if (!searchAbleFields.isEmpty()) {
            sb.append(" (");
            String glue = "";
            for (String s : searchAbleFields) {
                sb.append(glue)
                        .append(s)
                        .append(" ILIKE ")
                        .append("'%").append(keyword).append("%'");
                glue = " OR ";
            }
            sb.append(")");
        }
        return sb.toString();
    }

}
