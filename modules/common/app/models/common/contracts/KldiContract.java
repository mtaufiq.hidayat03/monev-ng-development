package models.common.contracts;

import models.Satker;
import org.apache.commons.lang3.StringUtils;
import permission.Kldi;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author HanusaCloud on 11/20/2019 1:44 PM
 */
public interface KldiContract {

    String getKldi();

    default Kldi getKldiData() {
        return Kldi.findById(getKldi());
    }

    default boolean isItMyKldi(String id) {
        return !StringUtils.isEmpty(getKldi()) && getKldi().equals(id);
    }

    default List<Satker> getSatkers() {
        return Satker.getSatkersBy(getKldi());
    }

    default List<String> extractSatkerNames() {
        return getSatkers()
                .stream()
                .map(Satker::getNama)
                .collect(Collectors.toList());
    }

}
