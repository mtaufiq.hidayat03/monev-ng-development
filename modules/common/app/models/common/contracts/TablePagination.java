package models.common.contracts;

import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import play.db.jdbc.Query;
import play.mvc.Scope;
import utils.common.LogUtil;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * @author HanusaCloud on 11/7/2019 1:01 PM
 */
public interface TablePagination {

    interface ParamAble extends ParamExtractor {

        String TAG = "ParamAble";

        Param[] getParams();

        int getPage();
        String getKeyword();

        default String getPageKey() {
            return generateKey("page");
        }

        default String getSearchKey() {
            return generateKey("search");
        }

        default String getSortKey() {
            return generateKey("sort");
        }

        default Param getParamByKey(String key) {
            if (getParams() == null || getParams().length == 0) {
                return null;
            }
            for (Param param : getParams()) {
                if (param.getKey().equals(key) && param.valueExist()) {
                    return param;
                }
            }
            return null;
        }

        default String[] getKeywords() {
            return isKeywordExist() ? getKeyword().split("\\s") : new String[]{};
        }

        default boolean isParamByKeyExist(String key) {
            final Param param = getParamByKey(key);
            return param != null;
        }

        default boolean isKeywordExist() {
            return !StringUtils.isEmpty(getKeyword());
        }

        default Integer getCurrentPage(Scope.Params params) {
            return getInteger(params, getPageKey(), 1);
        }

        default String generateKeyword(Scope.Params params) {
            return getString(params, getSearchKey());
        }

        default List<Param> getOthersSpecificParams(Scope.Params params) {
            List<Param> results = new ArrayList<>();
            for (Map.Entry<String, String[]> entry : params.data.entrySet()) {
                if (entry.getKey().contains("page")
                        && !entry.getKey().equals(getPageKey())
                        && entry.getValue().length > 0) {
                    results.add(new Param(entry.getKey(), entry.getValue()[0]));
                }
            }
            return results;
        }

        default Param[] combineOthers(Scope.Params params, Param[] queryParams) {
            List<Param> paramList = new ArrayList<>();
            Collections.addAll(paramList, queryParams);
            paramList.addAll(getOthersSpecificParams(params));
            Param[] paramArray = new Param[paramList.size()];
            return paramList.toArray(paramArray);
        }

        default Map<String, Object> getParamMap() {
            LogUtil.debug(TAG, getParams());
            Map<String, Object> map = new HashedMap<>();
            for (Param param : getParams()) {
                map.put(param.getKey(), param.getValue());
            }
            return map;
        }

        default Integer getSort() {
            return 0;
        }

    }

    /**
     * pageable T means that T as a model and not as a collection,
     * because well... pageable means contain more than one item hence the name pageable*/
    interface PageAble<T> extends QueryClassGenerator {

        String TAG = "PageAble";

        int MAX_SIZE_PER_PAGE = 24;
        int MAX_PAGE_ITEM_DISPLAY = 10;

        ParamAble getParamAble();

        void setItems(List<T> collection);
        List<T> getItems();

        Class<T> getReturnType();

        default String prefixPage() {
            return "";
        }

        default T getFirst() {
            if (getTotal() == 0) {
                return null;
            }
            return getItems().get(0);
        }

        //only for elasticsearch
        default boolean exceedPageLimitation() {
            return (getLimit() * getCurPage()) > 10000;
        }

        default int getCurPage() {
            return getParamAble() != null ? getParamAble().getPage() : 0;
        }

        default Param[] getParams() {
            return getParamAble().getParams();
        }

        default int getTotalCurrentPage() {
            return getItems() != null ? getItems().size() : 0;
        }

        default String getPrevLink() {
            return this.getLink() + getParamAble().getPageKey() + "=" + (getCurPage() - 1);
        }

        default String getNextLink() {
            return this.getLink() + getParamAble().getPageKey() + "=" + (getCurPage() + 1);
        }

        default boolean isPrevious() {
            return this.getCurPage() != 1;
        }

        default int getLimit() {
            return MAX_PAGE_ITEM_DISPLAY;
        }

        default boolean isNext() {
            return this.getCurPage() * getLimit() < getTotal();
        }

        default int getOffset() {
            final int offset = getCurPage() > 1 ? (getCurPage() - 1) * getLimit() : 0;
            LogUtil.debug(TAG, "offset: " + offset);
            return offset;
        }

        default int getShow() {
            return getCurPage() == 1 ? 1 : getOffset() + 1;
        }

        default int getPageCount() {
            return calculatePageCount();
        }

        default String getLink() {
            return getQueryString();
        }

        default String getQueryString() {
            if (getParams() != null) {
                String link = "?" + generateQuery();
                if (!link.contains("?")) {
                    link += "?";
                } else if (!link.endsWith("&") && !link.endsWith("?")) {
                    link += "&";
                }
                return link;
            }
            return "";
        }

        default int[] getPageNumbers() {
            int[] pageNumbers = new int[getPageCount()];
            for (int i = 0; i < pageNumbers.length; i++) {
                pageNumbers[i] = i + 1;
            }
            return pageNumbers;
        }

        default int getFrom() {
            return getTotalCurrentPage() < getLimit() ? getTotal() : getLimit() * getCurPage();
        }

        default void generateTotal(String query, Object[] params) {
            final int total = Query.count("SELECT COUNT(*) FROM (" + query + ") p", Integer.class, params);
            setTotal(total);
        }

        default int calculatePageCount() {
            return  (int) Math.ceil(getTotal() * 1.0 / getLimit() * 1.0);
        }

        default String generateQuery() {
            StringBuilder result = new StringBuilder();
            String glue = "";
            for (Param param : getParamAble().getParams()) {
                if (!param.getValue().equals(getParamAble().getPageKey())
                        && param.getValue() != null) {
                    String encodedName = param.getKey();
                    String value = String.valueOf(param.getValue());
                    result.append(glue).append(encodedName).append("=").append(encode(value));
                    glue = "&";
                }
            }
            return result.toString();
        }

        default String encode(String content) {
            try {
                return URLEncoder.encode(content, "ISO-8859-1");
            } catch (Exception var3) {
                throw new IllegalArgumentException(var3);
            }
        }

        default String getLimitOffsetQuery(String query) {
            return query + " LIMIT " + getLimit() + " OFFSET " + getOffset();
        }

        default void executeQuery(String query) {
            LogUtil.debug(TAG, query);
            generateTotal(query);
            if (!isEmpty()) {
                setItems(Query.find(getLimitOffsetQuery(query), getReturnType()).fetch());
            }
            generateMeta();
        }

        default void executeQuery(String query, Object[] params) {
            generateTotal(query, params);
            if (!isEmpty()) {
                setItems(Query.find(getLimitOffsetQuery(query), getReturnType(), params).fetch());
            }
            generateMeta();
        }

        default void generateMeta() {
            setMeta(new Meta(this));
        }

        default void setMeta(Meta meta) {

        }

        default Meta getMeta() {
            return new Meta(this);
        }

        class Meta {

            private final int page;
            private final int total;
            private final int show;
            private final int from;
            private final String keyword;

            public Meta(PageAble pageAble) {
                this.total = pageAble.getTotal();
                this.show = pageAble.getShow();
                this.from = pageAble.getFrom();
                this.page = pageAble.getCurPage();
                String keyword = null;
                for (Param param : pageAble.getParams()) {
                    if (param.getKey().contains("keyword")
                            || param.getKey().contains("search")) {
                        keyword = String.valueOf(param.getValue());
                    }
                }
                this.keyword = keyword;
            }

            public String getKeyword() {
                return keyword;
            }
        }

    }

    public static class Param {

        private final String key;
        private final Object value;

        public Param(String key, Object value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }

        public Object getValue() {
            return value;
        }

        public boolean valueExist() {
            return getValue() != null && !ObjectUtils.isEmpty(getValue());
        }

    }

}
