package models.common.contracts;

/**
 * @author HanusaCloud on 11/20/2019 1:46 PM
 */
public interface PublishingStatusContract {

    Integer getStatus();

    default boolean statusExists() {
        return getStatus() != null;
    }

    default boolean isDraft() {
        return containStatus(0);
    }

    default boolean isPublish() {
        return containStatus(2);
    }

    default boolean isStockReview() {
        return containStatus(1);
    }

    default boolean containStatus(Integer id) {
        return statusExists() && getStatus().equals(id);
    }

}
