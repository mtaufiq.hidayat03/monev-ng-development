package models.common.contracts;

import utils.common.LogUtil;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.Date;

/**
 * @author HanusaCloud on 11/6/2019 5:31 PM
 */
public interface CreationContract {

    String TAG = "CreationContract";

    String STANDARD_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSZZ";
    String DATE_FORMAT = "dd-MM-yyyy";
    String STANDARD_NON_ISO = "yyyy-MM-dd HH:mm:ss";
    String MISSING_SECONDS = "yyyy-MM-dd HH:mm";
    String STANDARD_READABLE_FORMAT = "MMMM dd, YYYY";
    String STANDARD_TIME_READABLE_FORMAT = "MMMM dd, YYY HH:mm";
    String YEAR_FORMAT = "yyyy";
    String MONTH_FORMAT = "MMM";

    Timestamp getCreatedAt();

    default String getStandardFormattedCreatedAt() {
        if (getCreatedAt() == null) {
            return "";
        }
        return new SimpleDateFormat(STANDARD_FORMAT).format(getCreatedAt());
    }

    default String getCreatedAsDateOnly() {
        if (getCreatedAt() == null) {
            return "";
        }
        return new SimpleDateFormat(DATE_FORMAT).format(getCreatedAt());
    }

    default String getYearFromCreatedAt() {
        if (getCreatedAt() == null) {
            return Year.now().toString();
        }
        return new SimpleDateFormat("yyyy").format(getCreatedAt());
    }

    default String getCreatedAsReadable() {
        if (getCreatedAt() == null) {
            return "";
        }
        return new SimpleDateFormat(STANDARD_READABLE_FORMAT).format(getCreatedAt());
    }

    static String dateFormat(String stringDate) {
        LogUtil.debug(TAG, stringDate);
        if (stringDate == null || stringDate.isEmpty()) {
            return "";
        }
        try {
            final Date date = new SimpleDateFormat(STANDARD_FORMAT).parse(stringDate);
            final String result = new SimpleDateFormat(DATE_FORMAT).format(date);
            LogUtil.debug(TAG, "date format result: " + result);
            return result;
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            return "";
        }
    }

    default Long getCreatedBy() {
        return null;
    }

}
