package models.common.contracts;

import models.cms.ContentPermission;
import org.apache.commons.lang3.StringUtils;

/**
 * @author HanusaCloud on 11/18/2019 4:54 PM
 */
public interface PermissionContract {

    String getPermission();

    default void setPermissions(String[] permissions) {

    }

    default boolean isItMyPermission(String value) {
        return !StringUtils.isEmpty(getPermission()) && getPermission().equalsIgnoreCase(value);
    }

    default boolean isPrivate() {
        return !StringUtils.isEmpty(getPermission()) && getPermission().equalsIgnoreCase(ContentPermission.PRIVATE);
    }

    default boolean isPublic() {
        return !StringUtils.isEmpty(getPermission()) && getPermission().equalsIgnoreCase(ContentPermission.PUBLIC);
    }

    default boolean isInternal() {
        return !StringUtils.isEmpty(getPermission()) && getPermission().equalsIgnoreCase(ContentPermission.INTERNAL);
    }

}
