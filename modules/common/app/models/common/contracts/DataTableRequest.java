package models.common.contracts;

import play.mvc.Http;
import utils.common.QueryUtil;

import java.util.Map;

/**
 * @author HanusaCloud on 9/27/2019 8:38 AM
 */
public class DataTableRequest implements DataTablePagination.Request {

    public static final String TAG = "DataTableRequest";

    private final int start;
    private final int length;
    private final int draw;
    private final String search;
    private final String order;
    private final int columnIndex;
    private final String orderDirection;

    public DataTableRequest(Http.Request request) {
        String keyword = "";
        int startParam = 0;
        int lengthParam = 0;
        int drawParam = 0;
        String orderDirection = "asc";
        String orderColumn = "created_at";
        int columnIndex = 0;
        for (Map.Entry<String, String[]> entry : request.params.data.entrySet()) {
            final String key = entry.getKey();
            final String[] values = entry.getValue();
            if (key.equals("search[value]")) {
                keyword = values.length == 0 ? "" :values[0];
                continue;
            }
            if (key.equals("start")) {
                startParam = Integer.valueOf((values.length == 0 ? "0" : values[0]));
                continue;
            }
            if (key.equals("length")) {
                lengthParam = Integer.valueOf((values.length == 0 ? "10" : values[0]));
                continue;
            }
            if (key.equals("draw")) {
                drawParam = Integer.valueOf((values.length == 0 ? "0" : values[0]));
                continue;
            }
            if (key.equals("order[0][column]")) {
                columnIndex = Integer.valueOf((values.length == 0 ? "0" : values[0]));
                continue;
            }
            if (key.equals("order[0][dir]")) {
                orderDirection = values.length == 0 ? "" :values[0];
            }
        }
        this.start = startParam;
        this.length = lengthParam;
        this.search = QueryUtil.sanitize(keyword);
        this.draw = drawParam;
        this.order = orderColumn + " " + orderDirection;
        this.columnIndex = columnIndex;
        this.orderDirection = orderDirection;
    }


    @Override
    public int getStart() {
        return start;
    }

    @Override
    public int getLength() {
        return length;
    }

    @Override
    public String getSearch() {
        return search;
    }

    @Override
    public int getDraw() {
        return draw;
    }

    @Override
    public String getOrderBy() {
        return "created_at DESC";
    }

    @Override
    public int getOrderColumnIndex() {
        return columnIndex;
    }

    @Override
    public String getOrderDirection() {
        return orderDirection;
    }

}
