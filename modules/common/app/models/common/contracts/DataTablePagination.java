package models.common.contracts;

import com.google.gson.JsonObject;
import play.db.jdbc.Query;
import utils.common.LogUtil;

import java.util.List;

/**
 * @author HanusaCloud on 10/29/2019 4:43 PM
 */
public interface DataTablePagination {

    interface Request {

        int getStart();
        int getLength();
        String getSearch();
        int getDraw();
        String getOrderBy();
        int getOrderColumnIndex();
        String getOrderDirection();

        default boolean isSearchAble() {
            return getSearch() != null && !getSearch().isEmpty();
        }

    }

    interface PageAble<T> extends QueryClassGenerator {

        String TAG = "PageAble";

        JsonObject getResultsAsJson();
        void setResult(List<T> items);
        List<T> getResults();
        void setTotalItemInOnePage(int totalItem);
        int getTotalItemInOnePage();
        String[] getColumns();

        Request getRequest();

        Class<T> getReturnType();

        default String softDelete() {
            return "deleted_at is NULL";
        }

        default int getLimit() {
            return getRequest().getLength();
        }

        default int getOffset() {
            return getRequest().getStart();
        }

        default int getNumberingStartFrom() {
            return getOffset() + 1;
        }

        default void executeQuery(String query) {
            generateTotal(query);
            LogUtil.debug(TAG, query);
            if (!isEmpty()) {
                final String sql = query + " LIMIT " + getLimit() + " OFFSET " + getOffset();
                List<T> results = Query.find(sql, getReturnType()).fetch();
                LogUtil.debug(TAG, sql);
                setResult(results);
                setTotalItemInOnePage(results.size());
            }
        }

        default void modifiedQuery(String query) {
            StringBuilder sb = new StringBuilder();
            sb.append(query);
            final Object[] result = getBareSelectQuery(getReturnType().getAnnotations());
            boolean isSoftDeleteExist = (boolean) result[1];
            if (isSoftDeleteExist) {
                sb.append(" AND ").append(softDelete());
            }

            if (getRequest().isSearchAble()) {
                List<String> searchAbleFields = extractSearchAbleFields(getReturnType().getFields());
                sb.append(" AND ");
                sb.append(generateSearchableQuery(searchAbleFields, getRequest().getSearch()));
            }
            sb.append(" ORDER BY ").append(generateOrderSql());
            final String sql = sb.toString();
            executeQuery(sql);
        }

        default void executeQuery() {
            final String query = generateQueryFromClass();
            executeQuery(query);
        }

        default String generateQueryFromClass() {
            LogUtil.debug(TAG, "generate select query from class");
            StringBuilder sb = new StringBuilder();
            final Object[] result = getBareSelectQuery(getReturnType().getAnnotations());
            boolean isSoftDeleteExist = (boolean) result[1];
            sb.append(result[0]);
            sb.append(" WHERE 1=1");
            if (isSoftDeleteExist) {
                sb.append(" AND ").append(softDelete());
            }

            if (getRequest().isSearchAble()) {
                List<String> searchAbleFields = extractSearchAbleFields(getReturnType().getFields());
                sb.append(" AND ");
                sb.append(generateSearchableQuery(searchAbleFields, getRequest().getSearch()));
            }

            sb.append(" ORDER BY ").append(generateOrderSql());
            final String sql = sb.toString();
            LogUtil.debug(TAG, "generated sql: " + sql);
            return sql;
        }

        default String generateOrderSql() {
            final String column = getColumns()[getRequest().getOrderColumnIndex()];
            return column + " " + getRequest().getOrderDirection().toUpperCase();
        }

        default JsonObject getResponseTemplate() {
            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("draw", getRequest().getDraw());
            jsonObject.addProperty("recordsTotal", getTotal());
            jsonObject.addProperty("iTotalDisplayRecords", getTotal());
            jsonObject.addProperty("recordsFiltered", getTotal());
            return jsonObject;
        }

    }

}
