package models.common.contracts;

import java.util.*;

public class Chartable {
    public Set<Object> labels = new LinkedHashSet<>();
    public List<ChartItem> datasets = new ArrayList<>();

    public static class ChartItem {
        public int number;
        public String label;
        public List<Number> data = new ArrayList<>();
        public ChartItem(String label, int number) {
            this.label = label;
            this.number = number;
        }

        public ChartItem(String label, List<Number> data, int number) {
            this.label = label;
            this.data = data;
            this.number = number;
        }

        public ChartItem(String label, Number data, int number) {
            this.label = label;
            this.data.add(data);
            this.number = number;
        }

    }

    public static class ChartQueryResult {
        public String seriesLabel;
        public String timeLabel;
        public Number data;
    }

    public void add(String seriesLabel, Number data) {
        boolean exists = false;
        for (ChartItem item: datasets) {
            if (item.label.equals(seriesLabel)) {
                item.data.add(data);
                exists = true;
                break;
            }
        }

        if (!exists) {
            datasets.add(new ChartItem(seriesLabel, new ArrayList<>(Arrays.asList(data)), datasets.size()));
        }
    }

    public void put(String seriesLabel, Number data, int index) {
        for (ChartItem item: datasets) {
            if (item.label.equals(seriesLabel)) {
                item.data.set(index, data);
                break;
            }
        }
    }

    public LinkedHashMap<String, Long> getTotalValue() {
        LinkedHashMap<String, Long> results = new LinkedHashMap<>();
        for (ChartItem item : datasets) {
            results.put(item.label, item.data.stream().mapToLong(e -> (long) e).sum());
        }
        return results;
    }

}
