package models.common;

import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;

import javax.persistence.MappedSuperclass;
import java.io.Serializable;

/**
 * @author HanusaCloud on 8/26/2019 5:14 PM
 */
@MappedSuperclass
public class ReadOnlyModel extends BaseTable implements Serializable {

    public static final String BASE_TAG = "ReadOnlyModel";

    @Id
    public Long id;
}
