package notifiers;

import models.user.management.Token;
import models.user.management.User;
import play.mvc.Mailer;
import repositories.user.management.UserRepository;
import services.cms.user.management.UserService;
import utils.common.LogUtil;
import utils.common.MailUtil;
import utils.common.TokenUtil;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class Mails extends Mailer {

    private static final String TAG = "Mails";
    private static final String host ="http://monev-ng.cloud.javan.co.id";
    private static UserService userService = UserService.getInstance();

    public static boolean sendEmail(String email) {
        try {
            String token = TokenUtil.generateMd5Token(email);
            LogUtil.debug(TAG, "token to be sent: " + token);
            Timestamp timestamp = MailUtil.expiredDate();
            String url = host+"/user/create-user/"+token;
            addRecipient(email+" <"+email+">");
            setFrom("LKPP <humas@lkpp.go.id>");
            setSubject("Lengkapi Registrasi User Anda");
            if (send(email, url).get()) {
                userService.storeTokenUser(email, token, timestamp, Token.Type.REGISTRATION);
                LogUtil.debug(TAG, "sukses kirim email");
                LogUtil.debug(TAG, "Registrasi user public telah berhasil dilakukan");
                return true;
            }
            LogUtil.debug(TAG, "gagal kirim email");
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }
        return false;
    }

    public static boolean sendEmailForgotPassword(String email) {
        try {
            String token = TokenUtil.generateMd5Token(email);
            LogUtil.debug(TAG, "token to be sent: " + token);
            Timestamp timestamp = MailUtil.expiredDate();
            String url = host+"/user/forgot-password/"+token;
            addRecipient(email+" <"+email+">");
            setFrom("LKPP <humas@lkpp.go.id>");
            setSubject("Permohonan Reset Password");
            if (send(email, url).get()) {
                userService.storeTokenUser(email, token, timestamp, Token.Type.FORGOT_PASSWORD);
                LogUtil.debug(TAG, "sukses kirim email");
                LogUtil.debug(TAG, "Kirim email reset password telah berhasil dilakukan");
                return true;
            }
            LogUtil.debug(TAG, "gagal kirim email");
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }
        return false;
    }


    public static void sendEmailApproveAdminKl(Long id) {
        User model = new UserRepository().getUser(id);
        String email = model.email;
        String notif = "Pengajuan anda untuk menjadi admin K/L/P/D telah disetujui oleh super admin";
        addRecipient(email+" <"+email+">");
        setFrom("LKPP <humas@lkpp.go.id>");
        setSubject("Pengajuan admin K/L/P/D");
        try {
            if(sendAndWait(email, notif)) {
                LogUtil.debug(TAG, "sukses kirim email approve admin K/L/P/D");
            }
            LogUtil.debug(TAG, "gagal kirim email approve admin K/L/P/D");
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }
    }

    public static void sendEmailRejectAdminKl(Long id) {
        User model = new UserRepository().getUser(id);
        String email = model.email;
        String notif = "Pengajuan anda untuk menjadi admin K/L/P/D ditolak oleh super admin";
        addRecipient(email+" <"+email+">");
        setFrom("LKPP <humas@lkpp.go.id>");
        setSubject("Pengajuan admin K/L/P/D");
        try {
            if(sendAndWait(email, notif)) {
                LogUtil.debug(TAG, "sukses kirim email reject admin K/L/P/D");
            }
            LogUtil.debug(TAG, "gagal kirim email reject admin K/L/P/D");
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }
    }

    public static String sendEmailAdminKonten(String email){
        String hasil = null;
        String token = TokenUtil.generateMd5Token(email);
        Timestamp timestamp = MailUtil.expiredDate();
        String url = host+"/user/create-admin/"+token;
        addRecipient(email+" <"+email+">");
        setFrom("LKPP <humas@lkpp.go.id>");
        setSubject("Lengkapi Registrasi Admin K/L/P/D anda");
        try {
            if (sendAndWait(email, url)) {
                userService.storeTokenAdmin(email, token, timestamp, Token.Type.REGISTRATION);
                LogUtil.debug(TAG, "Registrasi User admin K/L/P/D telah berhasil dilakukan.");
                LogUtil.debug(TAG, "sukses kirim email");
                hasil = "sukses";
            }
            LogUtil.debug(TAG, "gagal kirim email");
            hasil = "gagal";
        } catch (Exception e) {
           LogUtil.error(TAG, e);
        }
        return hasil;
    }

    public static void sendMail(String email, String url, String subject) {
        UUID uuid = UUID.randomUUID();
        String randomUUIDString = uuid.toString();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date dt = new Date();
        String currentDateTimeString = dateFormat.format(dt);
        String mytoken = email+currentDateTimeString+randomUUIDString;
        String token = TokenUtil.md5Digest(mytoken);
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.HOUR, 24);
        Date expiredDate = cal.getTime();
        String expired_date = dateFormat.format(expiredDate);
        Timestamp timestamp = Timestamp.valueOf(expired_date);
        url = url+token;
        addRecipient(email+" <"+email+">");
        setFrom("LKPP <humas@lkpp.go.id>");
        setSubject(subject);
        try {
            if (send(email, url).get()) {
                userService.storeTokenAdmin(email, token, timestamp, Token.Type.REGISTRATION);
                LogUtil.debug(TAG, "Registrasi telah berhasil dilakukan.");
                LogUtil.debug(TAG, "sukses kirim email");
            }
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }
    }

}