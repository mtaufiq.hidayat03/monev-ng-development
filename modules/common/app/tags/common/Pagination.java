package tags.common;

import groovy.lang.Closure;
import models.common.contracts.TablePagination.PageAble;
import play.templates.FastTags;
import play.templates.GroovyTemplate;

import java.io.PrintWriter;
import java.util.Map;

import static models.common.contracts.TablePagination.PageAble.MAX_PAGE_ITEM_DISPLAY;

/**
 * @author HanusaCloud on 11/7/2019 12:58 PM
 */
public class Pagination extends FastTags {

    public static final String TAG = "Pagination";

    public static void _pagination(
            Map<?, ?> args,
            Closure body,
            PrintWriter out,
            GroovyTemplate.ExecutableTemplate template,
            int fromLine) {
        PageAble page = (PageAble) args.get("arg");
        String baseUrl = args.get("url") == null ? "" : (String)args.get("url");
        if (page == null) {
            out.write("");
            return;
        }
        StringBuilder content = new StringBuilder();
        if (page.getTotal() > 0) {
            int from=1, to=1;
            content.append("<div class=\"ui two column grid stackable\">");
            content.append("<div class=\"ui left floated five wide column middle aligned\">");
                    content.append(page.getShow())
                    .append(" sampai ")
                    .append(page.getFrom())
                    .append(" dari ")
                    .append(page.getTotal());
            content.append("</div>");
            if (page.getLimit() < page.getTotal()) {
                content.append("<div class=\"ui right aligned eleven wide column wrapper-component-pagination\">");
                content.append("<div class=\"ui pagination menu\">");
                content.append("<a class=\"item\" href=\"")
                        .append(page.isPrevious() ? baseUrl + page.getPrevLink() : "javascript:void(0)")
                        .append("\"><i class=\"chevron left icon\"></i></a>");
                int start, size;
                if (page.getPageCount() <= MAX_PAGE_ITEM_DISPLAY) {
                    start = 1;
                    size = page.getPageCount();
                } else {
                    if (page.getCurPage() <= MAX_PAGE_ITEM_DISPLAY - MAX_PAGE_ITEM_DISPLAY / 2) {
                        start = 1;
                    } else if (page.getCurPage() >= page.getPageCount() - MAX_PAGE_ITEM_DISPLAY / 2) {
                        start = page.getPageCount() - MAX_PAGE_ITEM_DISPLAY + 1;
                    } else {
                        start = page.getCurPage() - MAX_PAGE_ITEM_DISPLAY / 2;
                    }
                    size = MAX_PAGE_ITEM_DISPLAY;
                }
                from = start;
                final String link = page.getLink();
                for (int i = 0; i < size; i++) {
                    content.append("<a class=\"").append(((start + i) == page.getCurPage() ? "active" : ""))
                            .append(" item\" href=\"")
                            .append(baseUrl + link)
                            .append(page.getParamAble().getPageKey())
                            .append("=").append((start + i))
                            .append("\">");
                    content.append((start + i));
                    content.append("</a>");
                }
                to = start + (size-1);
                content.append("<a class=\"item\" href=\"")
                        .append(page.isNext() ? baseUrl + page.getNextLink() : "javascript:void(0)")
                        .append("\"><i class=\"chevron right icon\"></i></a>");
                content.append("</div>");
                content.append("</div>");

            }
            content.append("</div><br />");
        }
        out.print(content.toString());
    }

}
