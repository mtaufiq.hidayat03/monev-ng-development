package tags.common;

import groovy.lang.Closure;
import play.mvc.Router;
import play.templates.FastTags;
import play.templates.GroovyTemplate;
import utils.common.LogUtil;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * @author HanusaCloud on 9/24/2019 4:43 PM
 */
public class RouterTag extends FastTags {

    public static final String TAG = "RouterTag";

    public static void _url(
            Map<?, ?> args,
            Closure body,
            PrintWriter out,
            GroovyTemplate.ExecutableTemplate template,
            int fromLine
    ) {
        LogUtil.debug(TAG, args);
        final String action = (String) args.get("arg");
        args.remove("arg");
        Map<String, Object> map = new HashMap<>();
        map.putAll((Map<? extends String, ?>) args);
        final String url = Router.reverse(action, map).url;
        LogUtil.debug(TAG, url);
        out.print(url);
    }


}
