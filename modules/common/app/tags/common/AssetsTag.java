package tags.common;

import groovy.lang.Closure;
import play.mvc.Router;
import play.templates.FastTags;
import play.templates.GroovyTemplate;
import utils.common.LogUtil;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

/**
 * @author HanusaCloud on 10/9/2019 8:28 AM
 */
public class AssetsTag extends FastTags {

    public static void _assets(
            Map<?, ?> args,
            Closure body,
            PrintWriter out,
            GroovyTemplate.ExecutableTemplate template,
            int fromLine
    ) {
        final String action = (String) args.get("arg");
        out.print(action);
    }



}
