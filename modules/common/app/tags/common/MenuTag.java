package tags.common;

import constants.generated.R;
import models.Configuration;
import models.user.management.CurrentUser;
import play.mvc.Router;

import java.util.ArrayList;
import java.util.List;

public class MenuTag {

    private static final SideBarMenu USERS = new SideBarMenu(
            "User",
            new SideBarMenu[]{new SideBarMenu(
                    "Manajemen User",
                    "controllers.user.management.UserController.index")
            }, "fa fa-user-circle");

    private static final SideBarMenu CONTENT = new SideBarMenu(
            "Konten",
            new SideBarMenu[]{new SideBarMenu(
                    "Manajemen Konten",
                    R.route.cms_contentcontroller_index)
            }, "fa fa-file");

    private static final SideBarMenu SUPER_CONTENT = new SideBarMenu(
            "Konten",
            new SideBarMenu[]{new SideBarMenu(
                    "Manajemen Konten",
                    R.route.cms_contentcontroller_index),
                    new SideBarMenu(
                            "Manajemen Library Tableau",
                            R.route.cms_tableau_tableaucontroller_index)
            }, "fa fa-file");

    private static final SideBarMenu PREDICTION = new SideBarMenu(
            "Prediksi",
            new SideBarMenu[]{
                    new SideBarMenu(
                            "Tender/Seleksi",
                            R.route.core_tenderclassificationcontroller_search)
            }, "fa fa-superpowers");

    private static final SideBarMenu QUESTION = new SideBarMenu(
            "Pertanyaan",
            new SideBarMenu[]{
                    new SideBarMenu(
                            "Manajemen Pertanyaan",
                            R.route.questionanswers_questioncontroller_index)
            }, "fa fa-question");

    private static final SideBarMenu PROFILES = new SideBarMenu(
            "Adjustment",
            new SideBarMenu[]{
                    new SideBarMenu(
                            "Profil Pengadaan",
                            R.route.dashboard_procurementprofile_profilecontroller_index
                    ),
                    new SideBarMenu(
                            "Rekap Pengadaan",
                            R.route.dashboard_rekappengadaanadjustmentcontroller_index
                    ),
                    new SideBarMenu("Smart Report",
                            new SideBarMenu[]{
                                    new SideBarMenu(
                                            "e-Tendering",
                                            R.route.smartreport_smartreportadjustmentcontroller_etendering
                                    ),
                                    new SideBarMenu(
                                            "e-Purchasing",
                                            R.route.smartreport_smartreportadjustmentcontroller_epurchasing
                                    ),
                                    new SideBarMenu(
                                            "Non e-Tendering",
                                            R.route.smartreport_smartreportadjustmentcontroller_nonetendering
                                    )
                            }, null
                    ),
                    new SideBarMenu(
                            "Informasi Lainnya",
                            R.route.adjustment_adjustmentcontroller_index
                    )
            }, "fa fa-list");

    public static List<SideBarMenu> getList() {
        List<SideBarMenu> langMenuList = new ArrayList<>();
        CurrentUser currentUser = CurrentUser.getInstance();
        if (!currentUser.user.isAdminKonten()) {
            langMenuList.add(USERS);
        }
        if (currentUser.user.isSuperAdmin()) {
            langMenuList.add(PROFILES);
            langMenuList.add(SUPER_CONTENT);
        } else {
            langMenuList.add(CONTENT);
        }
        if (currentUser.user.isSuperAdmin()
                || (currentUser.user.isAdminKl() && Configuration.isTenderForKlpdEnabled())) {
            langMenuList.add(PREDICTION);
        }
        if (currentUser.user.isSuperAdmin()) {
            langMenuList.add(QUESTION);
        }
        return langMenuList;
    }

    public static class SideBarMenu {

        public final String header;
        public final SideBarMenu[] items;
        public final String icons;
        public final String url;

        private SideBarMenu(String header, String action) {
            this.header = header;
            this.icons = null;
            this.items = null;
            this.url = Router.reverse(action).url;
        }

        private SideBarMenu(String header, SideBarMenu[] items, String icons) {
            this.header = header;
            this.items = items;
            this.icons = icons;
            this.url = null;
        }

        public boolean isHasItem() {
            return items != null && items.length > 0;
        }

    }

//    public static class SideBarMenuItem {
//
//        public final String name;
//        public final String url;
//
//        public SideBarMenuItem(String name, String action) {
//            this.name = name;
//            this.url = Router.reverse(action).url;
//        }
//
//    }

}
