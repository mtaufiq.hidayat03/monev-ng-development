package extensions.common;

import play.templates.JavaExtensions;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class CurrencyFormatter extends JavaExtensions {
    /**
     * Indonesia Locale
     */
    public static final Locale indonesia = new Locale("ID", "id");

    /**
     * Fungsi {@code formatCurrenciesJuta} digunakan untuk format angka ke dalam bentuk (Juta,Milyar,Trilyun)
     *
     * @param number nilai yang diformat
     * @return nilai {@code number} yang sudah terformat dalam bentuk [nilai][jt/M/T]
     */
    public static String formatCurrenciesJuta(Number number) {
        if (number == null)
            return "";
        String hasil = "";
        String sufix = "";
        if (number != null) {
            double num = number.doubleValue();
            double juta = num / 1E6;
            double milyar = num / 1E9;
            double trilyun = num / 1E12;
            // pakai juta atau milyar?
            if (juta < 1) {
                sufix = " Rb";
                num /= 1E3;
            } else if (milyar < 1) {
                sufix = " Jt";
                num /= 1E6;
            } else if (milyar >= 1 && trilyun < 1) {
                sufix = " M";
                num /= 1E9;
            } else {
                sufix = " T";
                num /= 1E12;
            }
            NumberFormat format = new DecimalFormat("###,###,###,###,###,##0.00");
            hasil = format.format(num);
            // hasil = "Rp " + hasil; //tanpa Rp
            hasil = hasil.replaceAll(",", "_");
            hasil = hasil.replaceAll("\\.", ",");
            hasil = hasil.replaceAll("_", "\\.");

        }
        return hasil + sufix;
    }

    /**
     * Fungsi {@code formatCurrenciesJutaRupiah} digunakan untuk format angka kedalam bentuk [Rp. ][nilai][jt/M/T].
     *
     * @param number nilai yang diformat
     * @return nilai {@code number} yang sudah terformat dalam bentuk [Rp. ][nilai][jt/M/T]
     */
    public static String formatCurrenciesJutaRupiah(Number number) {
        if (number == null)
            return "";
        String hasil = formatCurrenciesJuta(number);
        return "Rp " + hasil;
    }

    /**
     * Fungsi {@code formatCurrencyRupiah} digunakan untuk format angka kedalam bentuk Rupiah
     *
     * @param number nilai yang diformat
     * @return nilai {@code number} yang sudah terformat dalam bentuk Rp. [nilai]
     */
    public static String formatCurrencyRupiah(Number number) {
        String hasil = "";
        if (number != null) {
            NumberFormat format = NumberFormat.getCurrencyInstance(indonesia);
            hasil = format.format(number).replaceAll("Rp", "Rp ");
        }
        return hasil;
    }
}
