package extensions.common;

import models.common.contracts.CreationContract;
import play.templates.JavaExtensions;
import utils.common.LogUtil;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author HanusaCloud on 11/22/2019 3:53 PM
 */
public class DateFormatter extends JavaExtensions {

    public static final String TAG = "DateFormatter";

    public static String standardDate(String stringDate) {
        try {
            LogUtil.debug(TAG, "date String: " + stringDate);
            final Date date = new SimpleDateFormat(CreationContract.STANDARD_NON_ISO).parse(stringDate);
            return new SimpleDateFormat(CreationContract.DATE_FORMAT).format(date);
        } catch (Exception e) {
            return stringDate;
        }
    }

}
