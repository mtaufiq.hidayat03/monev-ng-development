package extensions.common;

import play.templates.JavaExtensions;

/**
 * @author HanusaCloud on 11/27/2019 11:24 AM
 */
public class LabelToColorCode extends JavaExtensions {

    public static String predictionToColorCode(String value) {
        if (value.equalsIgnoreCase("berhasil")) {
            return "badge badge-success";
        }
        if (value.equalsIgnoreCase("gagal")) {
            return "badge badge-danger";
        }
        return "black";
    }

}
