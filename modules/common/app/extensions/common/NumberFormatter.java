package extensions.common;

import play.templates.JavaExtensions;

import java.text.NumberFormat;
import java.util.Locale;

public class NumberFormatter extends JavaExtensions {
    /**
     * Indonesia Locale
     */
    public static final Locale indonesia = new Locale("ID", "id");

    /**
     * Fungsi {@code formatNumberIndonesia} digunakan untuk format angka ke dalam format Indonesia
     *
     * @param number nilai yang diformat
     * @return nilai {@code number} yang sudah terformat dalam format Indonesia
     */
    public static String formatNumberIndonesia(Number number) {
        return NumberFormat.getInstance(indonesia).format(number);
    }

    /**
     * Fungsi {@code formatNumberIndonesia} digunakan untuk format angka ke dalam format Indonesia
     *
     * @param number nilai yang diformat
     * @return nilai {@code number} yang sudah terformat dalam format Indonesia
     */
    public static String formatPersenIndonesia(Number number) {
        NumberFormat nf = NumberFormat.getInstance(indonesia);
        nf.setMaximumFractionDigits(2);
        nf.setMinimumFractionDigits(0);
        return nf.format(number);
    }

}
