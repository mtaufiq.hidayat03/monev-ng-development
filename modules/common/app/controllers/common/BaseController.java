package controllers.common;

import models.user.management.CurrentUser;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import performance.PmmRepository;
import permission.Can;
import play.Play;
import play.i18n.Lang;
import play.i18n.Messages;
import play.mvc.Before;
import play.mvc.Catch;
import play.mvc.Controller;
import play.mvc.Http;
import ppsdm.BasicLevelCertificateHolderRepository;
import repositories.adjustment.AdjustmentRepository;
import repositories.cms.ContentRepository;
import repositories.core.BlacklistRepository;
import repositories.core.QuestionAnswers.AnswerRepository;
import repositories.core.QuestionAnswers.QuestionRepository;
import repositories.dashboard.*;
import repositories.smartreport.*;
import repositories.user.management.UserRepository;
import services.cms.ContentService;
import services.cms.user.management.UserService;
import services.core.BlacklistService;
import services.core.QuestionService;
import umkm.RekapPerencanaanMetodeRepository;
import utils.common.LogUtil;
import utils.common.SentryUtil;

/**
 * @author HanusaCloud on 8/27/2019 10:39 AM
 */
public class BaseController extends Controller {

    public static final String BASE_TAG = "BaseController";

    private static boolean firstRequest = true;

    public static CurrentUser getCurrentUser(){
        return CurrentUser.getInstance();
    }

    private static ThreadLocal<StopWatch> tl=new ThreadLocal<>();

    protected static UserService userService = UserService.getInstance();

    protected static QuestionService questionService = QuestionService.getInstance();

    protected static QuestionRepository questionRepository = QuestionRepository.getInstance();

    protected static AnswerRepository answerRepository = AnswerRepository.getInstance();

    protected static BlacklistService blacklistService = BlacklistService.getInstance();

    protected static BlacklistRepository blacklistRepository = BlacklistRepository.getInstance();

    protected static ContentService contentService = ContentService.getInstance();

    protected static ContentRepository contentRepository = ContentRepository.getInstance();

    protected static RekapPengadaanRepository rekapPengadaanRepository = RekapPengadaanRepository.getInstance();

    protected static ETenderingSummaryRepository eTenderingSummaryRepository = ETenderingSummaryRepository.getInstance();

    protected static ETenderingDetailRepository eTenderingDetailRepository = ETenderingDetailRepository.getInstance();

    protected static EPurchasingRepository ePurchasingRepository = EPurchasingRepository.getInstance();

    protected static EPurchasingDetailRepository ePurchasingDetailRepository = EPurchasingDetailRepository.getInstance();

    protected static NonETenderingSummaryRepository nonETenderingSummaryRepository = NonETenderingSummaryRepository.getInstance();

    protected static NonETenderingDetailRepository nonETenderingDetailRepository = NonETenderingDetailRepository.getInstance();

    protected static RekapUkpbjRepository rekapUkpbjRepository = RekapUkpbjRepository.getInstance();

    protected static RekapAdvokasiRepository rekapAdvokasiRepository = RekapAdvokasiRepository.getInstance();

    protected static RekapLpseRepository rekapLpseRepository = RekapLpseRepository.getInstance();

    protected static RekapPenyediaRepository rekapPenyediaRepository = RekapPenyediaRepository.getInstance();

    protected static RekapPerencanaanMetodeRepository rekapPerencanaanMetodeRepository = RekapPerencanaanMetodeRepository.getInstance();

    protected static RekapPemilihanMetodeRepository rekapPemilihanMetodeRepository = RekapPemilihanMetodeRepository.getInstance();

    protected static KualifikasiUsahaRepository kualifikasiUsahaRepository = KualifikasiUsahaRepository.getInstance();

    protected static JenisKontrakRepository jenisKontrakRepository = JenisKontrakRepository.getInstance();

    protected static TotalPenyediaRepository totalPenyediaRepository = TotalPenyediaRepository.getInstance();

    protected static InovasiPengadaanRepository inovasiPengadaanRepository = InovasiPengadaanRepository.getInstance();

    protected static RekapSerahTerimaRepository rekapSerahTerimaRepository = RekapSerahTerimaRepository.getInstance();

    protected static AdjustmentRepository adjustmentRepository = AdjustmentRepository.getInstance();

    protected static BasicLevelCertificateHolderRepository basicLevelCertificateHolderRepository = BasicLevelCertificateHolderRepository.getInstance();

    protected static PmmRepository pmmRepository = PmmRepository.getInstance();

    protected static UserRepository userRepository = UserRepository.getInstance();

    //untuk melakukan bypass autentifikasi login yaitu beforeAll() pada aplikasi tambahkan /*
    @Before(unless = {"AuthController.logout"})
    protected static void beforeAll()
    {
        String language = params.get("language");
        if(language != null){
            Lang.set(language);
        }

        tl.set(new StopWatch());
        tl.get().start();
        firstRequest = false;

        Can allow = getActionAnnotation(Can.class);
        CurrentUser currentUser = getCurrentUser();
       // LogUtil.debug(TAG, currentUser);
        if (allow != null) { // cek otoritas terhadap fungsi yang dijalankan
            boolean allowed = false;
            String user_session = "UNAUTHORIZED_USER";
            if (currentUser != null) {
                allowed = currentUser.allowed(allow.value());
            }

            if (!allowed) {
                String message = Messages.get("not-authorized");
                if (StringUtils.isEmpty(user_session))
                    message += " karena <b>belum login atau session telah habis</b>.";
                flash("message", message);
                redirect("/");
            }
            if (currentUser != null) {
                if (Play.mode.isDev()) {
                    LogUtil.debug(BASE_TAG, currentUser);
                }
                renderArgs.put("user", currentUser.user);
            }
        } else {
            LogUtil.debug(BASE_TAG, "user does not have the role");
        }
    }
    // tambahkan */ pada akhir fungsi beforeAll()

    @Catch({Throwable.class})
    public static void globalCatcher(Throwable t) {
        LogUtil.debug(BASE_TAG, "global catcher is caught");
        SentryUtil.uncaught(t);
    }

    protected static String getPlaySessionCookie() {
        LogUtil.debug(BASE_TAG, "get play session cookie");
        Http.Cookie cookie;
        if ((cookie = request.cookies.get("PLAY_SESSION")) != null
                && !StringUtils.isEmpty(cookie.value)) {
            return cookie.value;
        }
        return "";
    }

    protected static Long getUserId() {
        return getCurrentUser() != null
                && getCurrentUser().user != null
                ? getCurrentUser().getUserId()
                : 0;
    }

}
