package controllers.common;

import constants.generated.R;
import models.user.management.CurrentUser;
import models.user.management.contracts.UserContract;
import permission.Kldi;
import play.data.validation.Error;
import play.i18n.Messages;
import play.mvc.Before;
import repositories.user.management.KldiRepository;
import utils.common.LogUtil;

import java.util.List;
import java.util.regex.Pattern;

/**
 * @author HanusaCloud on 9/24/2019 4:10 PM
 */
public class HtmlBaseController extends BaseController {

    public static final String TAG = "HtmlBaseController";

    private static Pattern MESSAGE_KEY_PATTERN = Pattern.compile("[\\w(.|\\-)]+");

    @Before()
    public static void injectVariables() {
        CurrentUser currentUser = getCurrentUser();
        if (currentUser != null) {
            UserContract user = currentUser.user;
            renderArgs.put("user", user);
            if (user.isRegiseredUser()) {
                List<Kldi> daftarkl = KldiRepository.getInstance().getListKl();
                renderArgs.put("daftarkl", daftarkl);
            }
        }
        renderArgs.put("translation", R.translation.class);
        renderArgs.put("route", R.route.class);
    }

    private static boolean isMessageKey(String key) {
        return !key.contains(" ") && MESSAGE_KEY_PATTERN.matcher(key).matches();
    }

    protected static String flashError(String value) {
        String message = value;
        if (isMessageKey(value)) {
            message = Messages.get(value);
        }
        flash.error(message);
        return message;
    }

    protected static String flashSuccess(String value) {
        String message = value;
        if (isMessageKey(value)) {
            message = Messages.get(value);
        }
        flash.success(message);
        return message;
    }

    protected static String printMessage() {
        StringBuilder sb = new StringBuilder();
        if (!validation.errorsMap().isEmpty()) {
            for (List<Error> error : validation.errorsMap().values()) {
                LogUtil.debug(TAG, error);
            }
        }
        return sb.toString();
    }

}
