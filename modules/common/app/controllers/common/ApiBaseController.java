package controllers.common;

import models.common.ServiceResult;
import play.mvc.Before;

/**
 * @author HanusaCloud on 9/24/2019 4:17 PM
 */
public class ApiBaseController extends BaseController {

    @Before
    public static void injectJson() {
        if (getCurrentUser() == null) {
            renderJSON(new ServiceResult(false, "Unauthorized"));
        }
    }

}
