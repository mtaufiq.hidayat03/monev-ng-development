var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var cleanCss = require('gulp-clean-css');
var merge = require('merge-stream');
var concat = require('gulp-concat');
var glob = require('glob');
var header = require('gulp-header');
var streamqueue = require('streamqueue');
var debug = require('gulp-debug');
const readline = require('readline');
const fs = require('fs');
const path = require('path');


var bootstrapPath = './core/resources/vendors/bootstraps/app.scss';
var modulePath = './modules/**/resources/*.scss';
var corePath = './core/resources/scss/*.scss';
var landingPath = './modules/usermanagement/resources/landing.scss';

var adminPath = './modules/usermanagement/resources/admin-custom.scss';
var adminModulePath = './modules/**/resources/*admin.scss';

const XLSX = require('xlsx')

const translationPattern = new RegExp(/([\w(\.|\-)]+)(?=\=)/, 'g');
const routePattern = new RegExp(/[\s\t][a-zA-Z_0-9\.{1,}]+/, 'g');
const routeStaticPattern = new RegExp(/staticDir.*/, 'g');

const assetsPattern = new RegExp(/(script|link|img)(.*?)(src|href)(.*?)(?<=\")(.*?)(?=\")/, 'g')
const assetsReplacementPattern = new RegExp(/(src|href)=\"(.*?)\"/, 'g')

const columnNumberPattern = new RegExp(/(?<column>[\D]+?)(?<number>[\d]+)/, 'g')

const actionPattern = /[\@\{\}\']/g

const urlTagPattern = new RegExp(/\#\{url route\.(?<name>[a-zA-Z\_]+)(\,|\s|)(.*?)\/\}/, 'g')

gulp.task('build-css', function() {
    console.log('start building css')
    var bootstrap = gulp.src([
            bootstrapPath,
        ], { allowEmpty: true })
        .pipe(sass().on('error', sass.logError))

    var core = gulp.src([
            corePath
        ], { allowEmpty: true })
        .pipe(header("@import './global_variables';"))
        .pipe(sass().on('error', sass.logError))

    var modules = gulp.src([modulePath])
        .pipe(header("@import '../../../core/resources/scss/global_variables';"))
        .pipe(sass().on('error', sass.logError))

    return streamqueue({ objectMode: true }, bootstrap, core, modules)
        .pipe(cleanCss())
        .pipe(concat('app.min.css'))
        .pipe(gulp.dest('./core/public/stylesheets/'));
});

async function generateTranslationTemplate() {
    var fileArray = [];
    var paths = fileArray.concat(
        glob.sync('./core/conf/messages'),
        glob.sync('./modules/*/conf/messages')
    )
    var translations = {}
    var translationPaths = []
    paths.forEach(item => {
        var lines = fs.readFileSync(item, 'utf-8')
            .split('\n')
        for (const line of lines) {
            if (line.includes('@')) {
                var result = line.split('=')[1]
                var path = item.replace('messages', '') + result.replace('./', '').replace(/(\s|\r\n|\r)/, '')
                translationPaths.push(path)
            }
        }
    })
    translationPaths.forEach(item => {
        var lines = fs.readFileSync(item, 'utf-8')
            .split('\n')
        var ln = 1
        for (const line of lines) {
            if (line.includes('#') || line == '') {
                continue
            }
            while (match = translationPattern.exec(line)) {
                if (match) {
                    const result = match[0]
                    if (translations[result] == undefined) {
                        translations[result] = { key: result, total: 0, source: [] }
                    }
                    translations[result]['total'] += 1
                    if (translations[result]['total'] > 1) {
                        translations[result]['source'].push({ path: item, line: ln })
                    }
                }
            }
            ln++
        }
    })

    var template = ""
    template += "\tpublic static final class translation {\n\n"
    var duplicated = []
    for (const item in translations) {
        const duplication = translations[item]
        if (duplication.total > 1) {
            duplicated.push(duplication)
        }
        template += "\t\tpublic static final String " + item.replace(/(\.|\-)/g, '_').toLowerCase() + " = " + "\"" + item + "\"" + ";\n"
    }
    template += "\n\t}\n\n"
    if (duplicated.length > 0) {
        console.log('translation key duplication detected')
        console.log(JSON.stringify(duplicated));
    }
    return template

}

function getRoutes() {
    var fileArray = glob.sync('./modules/*/conf/routes');
    var paths = fileArray.concat(glob.sync('./core/conf/routes'))
    var routes = {}
    paths.forEach(path => {
        if (routes[path] == undefined) {
            routes[path] = []
        }
        const array = fs.readFileSync(path, 'utf-8')
            .split('\n')
        for (const line of array) {
            if (line.includes('#') || line.includes('*')) {
                continue
            }
			if (line.includes('staticDir')) {
				while (match = routeStaticPattern.exec(line)) {
					if (match) {
						const result = match[0]
						if (!isNaN(result)) {
							continue
						}
						const resultStatic = result.replace(/(\s|\s\t|\r\n|\n)/g, '').replace(/(\/|\:)/g, '.')
						routes[path].push(resultStatic)
					}
				}
				continue
			}
            while (match = routePattern.exec(line)) {
                if (match) {
                    const result = match[0]
                    if (!isNaN(result)) {
                        continue
                    }
                    routes[path].push(result.replace(/(\s|\s\t|\r\n|\n)/g, ''))
                }
            }
        }
    })
    return routes
}

function actionRouteToJavaName(item) {
    return item.replace(/(\.|\-)/g, '_').toLowerCase()
}

async function generateRouteTemplate() {
    const routes = getRoutes()
    var template = ""
    template += "\tpublic static final class route {\n\n"
    for (const key in routes) {
        const items = routes[key]
        if (items.length == 0) {
            continue
        }
        template += "\t\t//" + key + "\n"
        routes[key].forEach(item => {
            template += "\t\tpublic static final String " + actionRouteToJavaName(item) + " = " + "\"" + item + "\"" + ";\n"
        });
    }
    template += "\n\t}\n\n"
    return template
}

async function validateAssets(replaceAssets) {
    console.log('validate assets');
    var paths = getTemplates()
    var results = []
    for (const path of paths) {
        var stringContent = fs.readFileSync(path, 'utf-8')
        const array = stringContent.split('\n')
        var ln = 0
        var replaceContents = []
        for (const line of array) {
            ln++
            if (line.includes('<!--') 
                || line.includes('$') 
                || !((line.includes('script') && line.includes('src'))
                || (line.includes('link') && line.includes('href')) 
                || (line.includes('img') && line.includes('src')))
                || line.includes('http')) {
                continue
            }
            
            while (match = assetsPattern.exec(line)) {
                if (match == undefined || match == null) {
                    continue
                } 
                var link = match[5].replace(actionPattern,'').replace(/.*public/g, './core/public')
                if (!fs.existsSync(link)) {
                    results.push({inview: path, assets: link, linenumber: ln})
                }
            }

            if (!replaceAssets) {
                continue
            }

            if (line.includes('#{assets')) {
                continue
            }
            
            while (match = assetsReplacementPattern.exec(line)) {
                if (match == undefined || match == null) {
                    continue
                }
                var assetsUrl = match[2]
                var withAssetsTag = assetsUrl.replace(actionPattern, '')
                if (withAssetsTag.charAt(0) != '/') {
                    withAssetsTag = "/" + withAssetsTag
                }                
                withAssetsTag = "#{assets '" + withAssetsTag + "' /}"
                if (stringContent.includes(assetsUrl)) {
                    replaceContents.push({path: path, linenumber: ln, from: assetsUrl, to: withAssetsTag})
                    stringContent = stringContent.replace(assetsUrl, withAssetsTag)
                }
            }
        }
        if (replaceContents.length > 0) {
            console.log('asset url is being replaced: ' + JSON.stringify(replaceContents));
            fs.writeFile(path, stringContent, 'utf8', function (err) {
                if (err) return console.log(err);
            });
        }
    }
    if (results.length > 0) {
        console.log('total missing assets ' + results.length);
        throw new Error('Oops.. missing assets: ' + JSON.stringify(results))
    }
}

async function generateJsTemplate() {
    console.log('generate js template')
    var paths = glob.sync('./core/public/**/*.min.js')
    var template = ""
    template += "\tpublic static final class js {\n\n"
    for (const item of paths) {
        if (item.includes('messages') 
            || item.includes('lang') 
            || item.includes('i18n')
            || item.includes('locales')
            || item.includes('locale')
            || item.includes('localization')) {
            continue
        }
        var name = item.replace(/\.\/core/g, '')
        console.log(name);
        
        var arrName = name.split('/');
        var varName = arrName[arrName.length - 1].replace(/[\-\.\_\s]/g, '_').toLowerCase()
        template += "\t\tpublic static final String " + varName + " = " + "\"" + name + "\"" + ";\n"
    }
    template += "\n\t}\n\n"
    return template
}

function getTemplates() {
    var fileArray = glob.sync('./modules/*/app/views/**/*.html');
    return fileArray.concat(glob.sync('./core/app/views/**/*.html'))
}

async function generateViewTemplate() {
    var paths = getTemplates()
    var template = ""
    template += "\tpublic static final class view {\n\n"
    paths.forEach(item => {
        var name = item.split('views')[1].replace(/^\//g, '')
        var varName = name.replace(/[\/\-]/g, '_').replace('.html', '').toLowerCase()
        template += "\t\tpublic static final String " + varName + " = " + "\"" + name + "\"" + ";\n"
    })
    template += "\n\t}\n\n"
    return template
}

gulp.task('validate-replace', function() {
    return validateAssets(true)
})

gulp.task('validate-assets', function() {
    return validateAssets(false)
})

gulp.task('generate-r', function() {
    console.log('generate r.java')
    const dir = './core/app/constants/generated/'
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir, { recursive: true });
    }
    return Promise.all([
            generateRouteTemplate(),
            generateTranslationTemplate(),
            generateViewTemplate()
        ])
        .then(([route, translation, view]) => {
            var template = "package constants.generated;\n"
            template += "\n"
            template += "/**\n* DO NOT CHANGE THIS CLASS MANUALLY, IT IS AN AUTO GENERATED CLASS.\n */\n"
            template += "public final class R {\n"
            template += "\n"
            template += route + translation + view
            template += "\n}"
            fs.writeFile(dir + '/R.java', template, function(err) {
                if (err) throw err;
                console.log('Saved!');
            })
            return Promise.resolve(true)
        })
})

gulp.task('watch-r', function() {
    gulp.watch([
        './core/conf/messages',
        './core/conf/translations/*',
        './modules/*/app/views/**/*.html',
        './core/app/views/**/*.html',
        './modules/*/conf/routes',
        './modules/*/conf/translations/*',
        './core/conf/routes',
    ], gulp.series('generate-r'));
});


gulp.task('build-css-landing', function() {
    console.log('start building css')
    var landing = gulp.src([
            landingPath,
        ], { allowEmpty: true })
        .pipe(sass().on('error', sass.logError))

    return streamqueue({ objectMode: true }, landing)
        .pipe(cleanCss())
        .pipe(concat('landing.min.css'))
        .pipe(gulp.dest('./core/public/stylesheets/'));
});

gulp.task('build-css-admin', function() {
    console.log('start building css')
    var admin = gulp.src([
            adminPath, adminModulePath
        ], { allowEmpty: true })
        .pipe(sass().on('error', sass.logError))

    return streamqueue({ objectMode: true }, admin)
        .pipe(cleanCss())
        .pipe(concat('admin-custom.min.css'))
        .pipe(gulp.dest('./core/public/stylesheets/'));
});

gulp.task('watch-css', function() {
    gulp.watch([bootstrapPath, corePath, modulePath], gulp.series('build-css'));
    gulp.watch([landingPath], gulp.series('build-css-landing'));
    gulp.watch([adminPath], gulp.series('build-css-admin'));
});

gulp.task('build-fa', function() {
    console.log('start building css')
    return merge(gulp.src([
                './resources/vendors/font-awesome-4.7.0/scss/font-awesome.scss',
            ], { allowEmpty: true })
            .pipe(sass().on('error', sass.logError)))
        .pipe(cleanCss())
        .pipe(concat('font-awesome.min.css'))
        .pipe(gulp.dest('./public/stylesheets/'));
});

gulp.task('pack-js', function() {
    console.log('pack js')
    return gulp.src([
            './resources/javascripts/d3/d3.min.js'
        ], { allowEmpty: true })
        .pipe(gulp.dest('./public/javascripts/'));
});

gulp.task('read-xlsx', function() {
    console.log('read xls')
    return generateProfileSection()
})

let extractColumnNumber = (key) => {
    var params = {}
    while (matches = columnNumberPattern.exec(key)) {
        params = {
            column: matches.groups['column'],
            rownumber: matches.groups['number']
        }
    }
    return params
}

function readSheetProfileSection() {
    var workbook = XLSX.readFile("Mapping Monev NG.xlsx")
    let sheet = workbook.Sheets['Profil Pengadaan']
    var header = {}
    var data = {}
    var currentNumber = 0
    var currentPage = ""
    var currentProfile = ""
    var results = []
    var i = 25
    for (let key in sheet) {
        let row = sheet[key]
        let extracted = extractColumnNumber(key)
        if (extracted.rownumber == 1) {
            header[extracted.column] = (row.v)
            continue
        }
        if (currentNumber != extracted.rownumber) {
            results.push(data)
            data = {}
            i++
        }
        currentNumber = extracted.rownumber 
        if (key.includes("A")) {
            currentPage = row.v
        }
        if (key.includes("B")) {
            currentProfile = row.v
        }
        data[header[extracted.column]] = row.v
        data['Halaman'] = currentPage
        data['procurement_profiles'] = currentProfile
        data['order'] = i
    }
    results.sort((e1, e2) => e1.Halaman - e2.Halaman)
    return results
}

async function generateProfileSection() {
    const results = readSheetProfileSection()    
    let capitalized = (value) => {
        if (value) {
            let toCapitalized = value.toString()
            return toCapitalized.split(" ")
                .map(e => e.charAt(0).toUpperCase() + e.slice(1))
                .join(" ")
        }
        return value
    }
    let startEndAutoGeneratedPattern = new RegExp(/(?<=\/\/START\sAUTO\sGENERATED)([\w\W]+?)(?=\/\/END\sOF\sAUTO\sGENERATED)/, 'gm')
    
    var template = '\n\t'
    var glue = ""
    var indent = ""
    var i = 0;
    const totalInArray = results.length
    for (let data of results) {
        i++
        if (Object.keys(data).length == 0) {
            continue
        }
        let name = data['procurement_data'].replace(" ", "")
        let section = typeof data['section'] === 'undefined' ? 'unknown' :  data['section']
        let page = data["Halaman"]
        glue = totalInArray == i ? ";" : ","
        let extra = data['Extra'] && data['Extra'] !== '' ? ',\n\t\t"' + data['Extra'] + '"' : ''
        template += indent + name.toUpperCase() + '(\n\t\t"' + name + 
                '",\n\t\t"' + section + 
                '",\n\t\t"' + capitalized(data['header']) + 
                '",\n\t\t' + data['order'] + 
                ',\n\t\t' + page +
                extra + 
                '\n\t)' + glue + '\n'
        indent = "\t"
    }
    let fileArray = glob.sync('./modules/**/ProfileSection.java')
    fileArray.forEach(e => {
        var lines = fs.readFileSync(e, 'utf-8')
        let replaced = lines.replace(startEndAutoGeneratedPattern, template)
        fs.writeFile(e, replaced, 'utf8', function (err) {
            if (err) return console.log(err)
        });
    })
}

function getXlsPagePattern() {
    let workbook = XLSX.readFile("Mapping Monev NG.xlsx")
    let sheet = workbook.Sheets['Sheet4']
    let results = []
    let data = {}
    let header = {}
    let currentNumber = 0
    let currentPage = ""
    let currentProfile = ""
    let i = 0
    for (let key in sheet) {
        let row = sheet[key]
        let extracted = extractColumnNumber(key)
        if (currentNumber != extracted.rownumber) {
            results.push(data)
            data = {}
            i++
        }
        if (extracted.rownumber == 1) {
            header[extracted.column] = (row.v)
            continue
        }
        currentNumber = extracted.rownumber 
        data[header[extracted.column]] = row.v
    }
    return results
}

gulp.task('validate-route-In-html', function() {
    console.log('read xls')
    return validateRouteInHtml()
})

async function validateRouteInHtml() {
    const routes = getRoutes()

    let existsInRoutes = (name) => {

        for (let key in routes) {            
            for (let route of routes[key]) {
                if (name === actionRouteToJavaName(route)) {
                    return true
                }
            }
        }
        return false
    }

    console.log('validate routes in html');
    let paths = getTemplates()
    var nonExistance = []
    for (let path of paths) {

        const array = fs.readFileSync(path, 'utf-8')
            .split('\n')
        var data = {
            file: path,
            invalids: []
        }
        var linenumber = 1
        for (const line of array) {
            while (match = urlTagPattern.exec(line)) {
                let name = match.groups['name']
                if (name && !existsInRoutes(name)) {
                    data.invalids.push({
                        name: name,
                        line: linenumber
                    })
                }
            }
            linenumber++
        }
        if (data.invalids.length > 0) {
            nonExistance.push(data)
        }
    }
    if (nonExistance.length != 0) {
        throw new Error('route not exist check this: ' + JSON.stringify(nonExistance))
    }
    return true
}

gulp.task('generate-db-view-xls', function() {
    console.log('generate db view from xls ')
    return createQueryFromXls()
})

async function createQueryFromXls() {
    const dir = './core/db/generated/view'
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir, { recursive: true });
    }
    let sections = readSheetProfileSection()    
    let map = {}
    let pages = []
    for (let element of sections) {
        if (Object.keys(element).length == 0) {
            continue
        }
        let page = element['Halaman']
        if (typeof page === 'undefined') {
            continue
        }
        page = page.toString()
        if (!pages.includes(page)) {
            pages.push(page.toString())
        }
        if (!Object.keys(map).includes(page)) {
            map[page] = []
        }
        map[page].push(element)
    }
    let bashTemplate = '#!/bin/bash'
    for (let index of pages) {
        let arr = map[index]
        
        let template = ''
        let separates = separateBySection(arr)
        for (let s in separates) {            
            template += generateSqlViewTemplate(index, separates[s], s === 'general' ? "" : "_"+ s)
        }
        let filename = 'page-'+ index + '-view.sql'
        bashTemplate += '\npsql "postgresql://postgres:postgres@10.0.0.129:5433/monev_ng" < ./' + filename
        fs.writeFile(dir + '/' + filename, template, function(err) {
            if (err) throw err;
            console.log('Saved!');
        })
    }
    fs.writeFile(dir + '/execute-views', bashTemplate, function(err) {
        if (err) throw err;
        console.log('Saved!');
    })
    return true
}

function separateBySection(arr) {
    let data = {
        general: []
    }
    for (let a of arr) {
        if (a['view'] && a['view'] === 'separate-by-section') {
            if (typeof data[a['section']] === 'undefined') {
                data[a['section']] = []
            }
            data[a['section']].push(a)
        } else {
            data.general.push(a)
        }
    }
    return data
}

function generateSqlViewTemplate(index, arr, addition) {
    let template = "\nDROP VIEW IF EXISTS \"public\".\"page_" + index + addition + "\";\n\n"
        + "CREATE OR REPLACE VIEW \"public\".\"page_" + index + addition + "\" AS \n"
        + "SELECT pd.procurement_id, pd.flag,"
    let glue = ''
    arr.sort((e1, e2) => e1.order - e2.order)
    for (let a of arr) {
        let name = a['procurement_data']
        if (a['query'] && a['query'] === 'subquery-label') {
            template += glue + "\n" + "( " +
                "\n\tSELECT value_text FROM \"public\".\"procurement_data\" " +
                "\n\tWHERE procurement_id = pd.procurement_id " +
                "\n\tAND flag = pd.flag " +
                "\n\tAND \"header\" = '" + name + "' " +
                "\n) AS " + name 
        } else {
            template += glue + "\n" + "sum(" +
                "\n\t\CASE" +
                "\n\tWHEN ((pd.header)::text = '" + name + "'::text) THEN pd.value" +
                "\n\tELSE 0" +
                "\n\tEND" +
                "\n) AS " + name
        }
        glue = ", "
    }
    template += "\nFROM \"public\".\"procurement_data\" pd "
    + "\nGROUP BY pd.procurement_id, pd.flag;\n"
    return template
}


gulp.task('merge-sql', function() {
    return gulp.src(['./**/*.sql','!./**/app.sql','!./**/*-data.sql'])
      .pipe(concat('app.sql'))
      .pipe(gulp.dest('./core/db/'))
  })


gulp.task('modify-modules-in-buildxml', function() {
    let template = ''
    let files = []
    let i = 1
    for (let file in files) {
        let filename = path.basename(file)
        template += '\t<loadfile property="module' + i + '" srcfile="${app.path}/modules/' + filename + '" />\n' +
		'\t<copy todir="${spse.target.dir}/modules/' + filename + '" includeemptydirs="false">\n' +
		'\t\t<fileset dir="${module' + i + '}" excludes="${filesExclude}" />\n' +
        '\t</copy>\n'
        i++
    }
    console.log(template)
})