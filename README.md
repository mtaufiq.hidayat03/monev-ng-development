MONEV-NG

### Prerequisite
1. Java programming language.
2. Javascript.
3. HTML and CSS/SCSS.
4. Posgresql.

### Requirement
1. [Play 1.4.2c](https://gitlab.lkpp.go.id/play-framework/play/tree/play-1.4.2c)
2. Postgres 10.x
3. Java 1.8.xxx dengan JCE
4. Bootstrap 4
5. Nodejs V10.X and NPM
6. JQuery
7. SCSS
8. Memcahed

### Running Project
1. Clone project.
2. Clone play-lkpp , checkout `play-lkpp-no-jpa-netty4`.
3. cd core.
4. rename conf/application.conf.example to conf/application.conf, and set http.port, db connection according to your environment
5. run `play deps --clearcache`.
6. run `play idealize` for Intellij IDEA.
7. cd ..
8. run `npm install`, if you haven't done this before, either way skip it.
9. run `npm run init-project`, it will generate css and `R.java`.
10. run `play run core`.

### Play Framework
This app is built on top of old java framework, you won't find your prefered magics or utilities here, because we don't have (write)access to the framework maintained by LKPP, if somehow you need utility script to ease the development phase, you have to use NodeJs, check `package.json` and `gulp.js` for more technical examples or create your own script.

### Easy Running Project Using Utility Script
1. remove suffix `.example` from `utility.example or utility.bat.example` choose the file according to your OS, NOTE: learn windows batch file .bat or linux .sh file by yourself.
2. open `utility` file set `PLAY` variable according to your environment example: `../play/play`
3. run utility script by `./utility <command>`
4. command `init`: initialize app for the first time, `deps`: update application's dependency, `build-css`: build css using gulp-sass, `run`: run play application, `watch`: set gulp to watch over scss.files and generate new css.

### Development Notes
1. DO NOT use `System.out.println`.
2. Logger usage is a must for easy debugging and documentation, see `LogUtil.java`.
3. Modularized apps, for example cms feature and user management.
4. Git master branch is for production only.
5. Git each feature or issue must have each own git branch. 
6. Development branch is used only for development.
7. Each feature and issue must be subjected to code review and then merge through merge-request feature to development branch.
8. Git naming convention `features/<feature-name>`, must be in kebab case example: `features/register-user`.
9. Use MVC with addition of Service class, for logic.
10. A table must be audit able for example having these (`created_at`, `created_by`, `updated_at`, `updated_by`, `deleted_at`, `deleted_by`) except for pivot table.
11. Table sequence must have table_name with suffix `_id_seq` for example: `user_id_seq`.
12. DO NOT forget to click `Ctrl + alt + o` to optimized imports every time.
13. Every route must be registered in `routes` file.
14. Each module has their own scss style in resource/app.scss, all resource will be bundled into one css file public/stylesheet/app.min.css.
15. Use SCSS for manageable stylesheet and build css using this command `npm run build-css`.
16. API respond must have different BaseController for API.
17. Modularization: go to modules and run `play new-module <module name>`, go to core/, add dependency to core/conf/dependency.yml, and `play deps`.
18. Use memcached if you want to keep your session alive, you don't need to re-login every time you restart the app.
19. Every database migration has to be documented inside each module, check usermanagement/db/user.sql for an example.
20. DO NOT add stylesheets/css onto this file `./core/public/stylesheets/app.min.css`, because it will be overwriten every time you run `npm run build-css`.
21. If possible use local assets like css and js, DO NOT use assets hosted by different services.
22. RESTful routes example: DO NOT do this `/monev-ng/user/createuser`, but you do this instead `/monev-ng/user/create` or `/monev-ng/user/create-admin`.
23. For easy java auto coding style, click `Ctrl + alt + l`, follows default Intellij IDEA style.
24. The used of Intellij IDEA live template: https://www.jetbrains.com/help/idea/using-live-templates.html is highly recommended.
25. Method name must be in CamelCase example: DO NOT do this `register_user`, do this instead `registerUser`.
26. Variable must be in camelCase example: DO NOT do this `email_address`, do this instead `emailAddress`. But if it's a model's properties you can use snake_case, because Play doesn't call `@Column` annotation so you can't rename the properties.
27. Java ERROR EXCEPTION MUST BE HANDLED see `LogUtil.java` for more information, for javascript part show error message if possible.
28. Template naming convention example: DO NOT do this `modaladminkl`, do this instead `modal-admin-kl` it's easier to read.
29. Because there's a chance when this app is deployed in production using `proxy_pass` every url must be in relative path.
30. Every message except log message must be registered in `/translation/<message_file>` file and import in `/conf/messages`, check `usermanagement/conf/messages` for example.
31. Check `R.java` file, every time you change either of these files `messages, routes, *.html` run this code `npm run generate-r` or for automatically generated you can use `npm run watch-r`, YOU HAVE TO USE `R.java` avoiding hard-coded resource such as messages or route.
32. If there's an error related to `Missing R.java class` run this command `npm run generate-r`.
33. Extending appropriate `controller`, HTML based content uses `HtmlBaseController` and Api based content uses `ApiBaseController`.
34. Run this command `npm run validate-assets` to validate your assets.
35. Open `package.json` to see available utility commands.
36. DO NOT USE `if` WITHOUT CURLY BRACES(unless you know what you're doing).
37. DO NOT LEAVE `catch` block EMPTY.
38. The used of automatic testing is highly encouraged(TDD.. even better), check `core/test` for unit test examples or `ui-tests` for ui test.
39. Use extension method in form of `java default method` inside interface to reduce code duplication and `SOC`.

### Why bother to use R.java?
This concept is used by Android, it's basically adding another layer to your design-pattern, 
so that you can access your resources such as translations, routes, html templates using static class, 
imagine you have to access translation by its key, let's say `message.hellow_word` it's easy right? indeed, 
but.. if you have to use that translation in more than one class or method, how can you make sure that they are the same key?
R.java is designed to help you minimize that error by simply calling the object like this `R.translation.message_hello_word`, 
it is guaranteed to return the same key, plus it does compile time checking by default and can be added to check template checking as well.

### Production Deployment
1. Run this script first `npm run deployment`
2. Renama `build.example.xml` to `build.xml` change the value of `source.home` according to your env
2. Build app using ANT `<ant dir>/bin/ant build.xml`, you can download ANT from here https://ant.apache.org/bindownload.cgi
3. Rename `application.example.conf` to `application.conf`
4. Open `application.conf` modify db connection, elastic, jasper, upload dir, prediction.*
5. Run bash script `runner start`

### Updating Procurement Profile Mappings
1. Download latest sheet from https://docs.google.com/spreadsheets/d/1pz4LKm5WZCDxyVZ-1QRnVMfjZmyI4X0vgpYU7wscFv4/edit#gid=309822542.
2. Place it at root dir with name `Mapping Monev NG.xlsx` (overwrite).
3. Run this command `npm run profile-section-view`.
4. Push to repo.
 
### Resource
Play https://www.playframework.com/documentation/1.4.x

Cheat sheets https://www.playframework.com/documentation/1.4.x/cheatsheet/templates

SCSS https://sass-lang.com/

Bootstrap https://getbootstrap.com/docs/4.0/getting-started/introduction/


