import { dialogAccept, totalSearchedItem, searchContentByTitle } from "./content-utils";

export const contentDeletion = () => {
    
    beforeAll(async () => {
        await Promise.all([
            page.goto('http://localhost:9300/cms', {waitUntil: 'networkidle0'})
        ])
    })

    it('should be able to search content', async() => {
        let typedSearch: string = await searchContentByTitle('Test Content Automation Updated')
        await expect(typedSearch).toMatch('Test Content Automation Updated')
        await expect(await totalSearchedItem()).not.toBe(0)
    }, 30000)

    it('should be able to delete content', async () => {
        await page.click('#table-content > tbody > tr:nth-child(1) td div.btn-group button')
        await page.on('dialog', dialogAccept)
        await page.click('#table-content > tbody > tr:nth-child(1) td div.btn-group ul li a.delete')
        await page.waitFor(2000)
        await page.removeListener('dialog', dialogAccept)
    }, 20000)

    it('should return empty when searching "Test Content Automation Updated"', async () => {
        await searchContentByTitle('Test Content Automation Updated')
        await expect(await totalSearchedItem()).toBe(0)
    }, 10000)

}