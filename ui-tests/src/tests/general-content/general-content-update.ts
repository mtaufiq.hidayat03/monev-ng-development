import { inputContentTitle, searchContentByTitle, totalSearchedItem, checkHeaderExists, setContentDetail, changePublicationStatus, storeContent, setAccessPageTo, storeAccess, dialogAccept, uploadFileTo, getSelectedAccess } from "./content-utils"

export const contentModification = () => {

    beforeAll(async () => {
        await Promise.all([
            page.goto('http://localhost:9300/cms', {waitUntil: 'networkidle0'})
        ])
    })

    it('should be able to search "Test Content Automation"', async () => {
        await searchContentByTitle("Test Content Automation")
        await expect(await totalSearchedItem()).not.toBe(0)
    }, 15000)

    it('should be able to get to update page', async () => {
        await page.click('#table-content > tbody > tr:nth-child(1) td div.btn-group button')
        await page.click('#table-content > tbody > tr:nth-child(1) td div.btn-group ul li a.update-content')
        await page.waitFor(2000)
        await expect(await checkHeaderExists('h1.input-content')).toBe(true)
    }, 15000)

    it("should be able to change content's title to 'Test Content Automation Updated'", async () => {
        await page.click('input#title', {clickCount: 3})
        await expect(await inputContentTitle('Test Content Automation Updated')).toMatch('Test Content Automation Updated')
    }, 15000)

    it('should be able to update banner file', async () => {
        let fileName: string = await uploadFileTo(
            'input#uploadfile', 
            "D:/web/projects/monev-ng/ui-tests/sample/sample.png"
            )
        await expect(fileName).toBe('sample.png')
    }, 30000)

    it('should be able to delete old document file', async () => {
        await page.on('dialog', dialogAccept)
        await page.click('.divuploadExtra .delete_file:nth-child(1) .del_file_uploadExtra')
        let addedFiles: any = await page.evaluate((): any => {
            let result: NodeList = document.querySelectorAll('.divuploadExtra .delete_file input.uploadExtra')
            return result.length ?? 0
        })
        await expect(addedFiles).toEqual(0)
        await page.removeListener('dialog', dialogAccept)
    }, 15000)

    it('should be able to add document number one', async () => {
        await page.waitForSelector('#attachMoreExtra')
        await page.click('#attachMoreExtra')
        let firstFile: string = await uploadFileTo(
            '.divuploadExtra .delete_file:nth-child(1) input.uploadExtra', 
            "D:/web/projects/monev-ng/ui-tests/sample/sample-updated.pdf"
            )
        await expect(firstFile).toBe("sample-updated.pdf")
    }, 15000)

    it('should be able to add document number two', async () => {
        await page.waitForSelector('#attachMoreExtra')
        await page.click('#attachMoreExtra')
        let firstFile: string = await uploadFileTo(
            '.divuploadExtra .delete_file:nth-child(2) input.uploadExtra', 
            "D:/web/projects/monev-ng/ui-tests/sample/sample-updated1.pdf"
            )
        await expect(firstFile).toBe("sample-updated1.pdf")
    }, 15000)

    it('should be able to update content to "New updated detail content"', async () => {
        await page.click('div.note-editable', {clickCount: 3})
        let typedContent: string = await setContentDetail("New updated detail content")
        await expect(typedContent).toContain("New updated detail content")
    }, 15000)

    it('should be able to update status to PUBLISH', async () => {
        let selectedStatus: string = await changePublicationStatus("2")
        await expect(selectedStatus).toBe("2")
    }, 15000)

    it('should be able to store updated data', async () => {
        let contentId: number = await storeContent()
        await expect(contentId).not.toBe(0)
    }, 15000)

    it('should be PRIVATE access', async () => {
        let selectedAccess = await getSelectedAccess()
        await expect(selectedAccess).toBe('private')
    }, 15000)

    it('should be able to change access to INTERNAL', async () => {
        await expect(await setAccessPageTo('internal')).toBe('internal')
    }, 15000)

    it('should be able to store access', async () => {
        await storeAccess()
    }, 15000)

    it('should be able to see content list', async () => {
        await expect(await checkHeaderExists('h1.content-management')).toBe(true)
    })

}