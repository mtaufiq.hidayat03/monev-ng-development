import { setAccessPageTo, dialogAccept, inputContentTitle, totalRequiredMessages, storeContent, checkHeaderExists, changePublicationStatus, setContentDetail, uploadFileTo } from "./content-utils";

export const contentCreation = () => {
    
    beforeAll(async () => {
        await Promise.all([
            page.goto('http://localhost:9300/users', {waitUntil: 'networkidle0'})
        ])
    })

    it('should be able to get to cms', async () => {
        await page.evaluate((): void => {
            let result: any = document.querySelector('nav.page-sidebar.shadow-nav')
            result.style.left = 0
        })
        await page.click('ul.menu-items.scroll-content li.menus:nth-child(3)')
        await page.waitForSelector('ul.menu-items.scroll-content li.menus:nth-child(3) ul.sub-menu > li:nth-child(1) > a')
        await page.click('ul.menu-items.scroll-content li.menus:nth-child(3) ul.sub-menu > li:nth-child(1) > a')
        await page.waitFor(2000)
        await expect(await checkHeaderExists('h1.content-management')).toBe(true)
    }, 30000)

    it('should be able to go to create content', async () => {
        await page.click('button.btn-teal-monev.dropdown-toggle')
        await page.click('ul.dropdown-menu li:nth-child(3) a')
        await page.waitFor(3000)
        await expect(await checkHeaderExists('h1.input-content')).toBe(true)
    }, 30000)

    it('should show required messages', async () => {
        let totalRequired: number = await totalRequiredMessages()
        await expect(totalRequired).toBe(3)
    })

    it('should be able to type content title', async () => {
        await expect(await inputContentTitle("Test Content Automation")).toMatch("Test Content Automation")
    }, 30000)

    it('should not be able to upload banner file using pdf file', async () => {
        let fileName: string = await uploadFileTo(
            'input#uploadfile', 
            "D:/web/projects/monev-ng/ui-tests/sample/sample.pdf",
            true
            )
        await expect(fileName).toBe('')
    }, 30000)

    it('should be able to upload banner file using image file', async () => {
        let fileName: string = await uploadFileTo(
            'input#uploadfile', 
            "D:/web/projects/monev-ng/ui-tests/sample/sample.png"
            )
        await expect(fileName).toBe('sample.png')
    }, 30000)

    it('should not be able to upload extras using image file', async () => {
        let fileName: string = await uploadFileTo(
            'input.uploadExtra', 
            "D:/web/projects/monev-ng/ui-tests/sample/sample.png",
            true
            )
        await expect(fileName).toBe('')
    }, 30000)

    it('should be able to upload extras using document file', async () => {
        let fileName: string = await uploadFileTo(
            'input.uploadExtra', 
            "D:/web/projects/monev-ng/ui-tests/sample/sample.pdf"
            )
        await expect(fileName).toBe('sample.pdf')
    })

    it('should be able to upload more extras', async () => {
        await page.waitForSelector('#attachMoreExtra')
        await page.click('#attachMoreExtra')
        let fileName: string = await uploadFileTo(
            '.divuploadExtra .delete_file:nth-child(2) input.uploadExtra', 
            "D:/web/projects/monev-ng/ui-tests/sample/sample.pdf"
            )
        await expect(fileName).toBe("sample.pdf")
    })

    it('should be able to delete extra file', async () => {
        await page.on('dialog', dialogAccept)
        await page.click('.divuploadExtra .delete_file:nth-child(2) .del_file_uploadExtra')
        let addedFiles: any = await page.evaluate((): any => {
            let result: NodeList = document.querySelectorAll('.delete_file input.uploadExtra')
            return result != null ? result.length : 0
        })
        await expect(addedFiles).toEqual(0)
        await page.removeListener('dialog', dialogAccept)
    }, 30000)

    it('should be able to type content detail', async () => {
        await expect(await setContentDetail("Test Content Automation detail")).toContain("Test Content Automation detail")
    }, 30000)

    it('should be able to pick publication as DRAFT', async () => {
        await expect(await changePublicationStatus('0')).toMatch('0')
    })

    it('should be able to store content', async () => {
        let contentId: number = await storeContent()
        await expect(contentId).not.toBe(0)
    })

    it('should be able to set access to PUBLIC', async () => {
        let aksesString: string = await setAccessPageTo('public')
        await expect(aksesString).toMatch('public')
    })

    it('should be able to set access to INTERNAL', async () => {
        let aksesString: string = await setAccessPageTo('internal')
        await expect(aksesString).toMatch('internal')
    }, 7000)

    it('should be able to set access to PRIVATE and set users', async () => {
        let aksesString: string = await setAccessPageTo('private')
        await expect(aksesString).toMatch('private')
        await page.evaluate(() => {
            let node: any = document.querySelector('div.ui.fluid.multiple.search.selection.dropdown')
            if (node) {
                node.click()
            }
        })
        await page.waitFor(500)
        await page.waitForSelector('div.menu.transition.visible')
        await page.waitFor(700)
        await page.click('div.menu.transition.visible div:nth-child(1)')
        await page.waitFor(800)
        await page.click('div.menu.transition.visible div:nth-child(4)')
        await page.waitFor(800)
        await page.click('.title-access-page')
        let totalUser: number = await page.evaluate((): number => {
            let nodes: NodeList = document.querySelectorAll('div.ui.fluid.multiple.search.selection.dropdown a.ui.label')
            return nodes?.length ?? 0
        })
        await expect(totalUser).toEqual(2)
    }, 10000)

    it('should be able to store access content', async () => {
        await page.evaluate(() => {
            let node: HTMLElement | null = document.querySelector('button[type="submit"].button-save')
            if (node) {
                node.click()
            }
        })
        await page.waitFor(2000)
    }, 10000)

    it('should be able to see content list', async () => {
        await expect(await checkHeaderExists('h1.content-management')).toBe(true)
    })

}