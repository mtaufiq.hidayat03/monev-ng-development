import { contentCreation } from './general-content'
import { contentDeletion } from './general-content-delete';
import { contentModification } from './general-content-update';
import { contentPreview } from './general-content-view';

describe('Content Creation', contentCreation)
describe('Content Modification', contentModification)
describe('Content Preview', contentPreview)
describe('Content Deletion', contentDeletion)