import { Dialog } from "puppeteer"

const searchContentByTitle = async (keyword: string): Promise<string> => {
    await page.type('.dataTables_filter input[type="search"]', keyword, {delay: 200})
    let typedKeyword: string = await page.evaluate((): string => {
        let element: HTMLInputElement | null = document.querySelector('.dataTables_filter input[type="search"]')
        return element?.value ?? ''
    })
    await page.keyboard.press('Enter')
    await page.waitFor(3000)
    return typedKeyword
}

const checkHeaderExists = async (selector: string): Promise<boolean> => {
    return page.evaluate((selector: string): boolean => {
        let result: HTMLLIElement | null = document.querySelector(selector)
        return result != null
    }, selector)
}

const totalSearchedItem = async (): Promise<number> => {
    let totalContent: number = await page.evaluate((): number => {
        let nodes: NodeList | null = document.querySelectorAll('#table-content > tbody > tr td:not(.dataTables_empty)')
        return nodes.length ?? 0
    })
    return totalContent
}

const changePublicationStatus = async (keyword: string): Promise<string> => {
    await page.waitForSelector('select[name="model.publish"]')
    await page.select('select[name="model.publish"]', keyword)
    return page.evaluate((): string => {
        let node: HTMLSelectElement | null = document.querySelector('select[name="model.publish"]')
        return node?.value ?? ''
    })
}

const setAccessPageTo = async (keyword: string): Promise<string> => {
    await page.click(`input[type="radio"][value="${keyword.toLowerCase()}"]`)
    return getSelectedAccess()
}

const getSelectedAccess = async (): Promise<string> => {
    return await page.evaluate((): string => {
        let result: HTMLInputElement | null = document.querySelector('input[type="radio"]:checked')
        return result?.value ?? ''
    })
}

const dialogAccept = async (dialog: Dialog) => {
    return dialog.accept()
}

const totalRequiredMessages = async (): Promise<number> => {
    await page.click('a#save_button')
    let totalRequired: number = await page.evaluate((): number => {
        let result: NodeList = document.querySelectorAll('.has-error')
        return result.length ?? 0
    })
    return totalRequired
}

const inputContentTitle = async (title: string): Promise<string> => {
    await page.type('input#title', title, {delay: 200})
    let typedTitle: string = await page.evaluate((): string => {
        let result: HTMLInputElement | null = document.querySelector('input#title')
        return result?.value || ''
    })
    return typedTitle
}

const storeContent = async (): Promise<number> => {
    await page.click('a#save_button')
    await page.waitFor(2000)
    let contentId: number = await page.evaluate((): number => {
        let node: Element | null = document.querySelector('form#inputArtikelUmum input[name="model.id"]')
        return node != null ? parseInt(node.getAttribute('value') || '0') : 0
    })
    return contentId
}

const setContentDetail = async (content: string): Promise<string> => {
    await page.type('div.note-editable', content, {delay: 200})
    let typedContent: string = await page.evaluate((): string => {
        let result: any = document.querySelector('textarea#content')
        return result != null ? result.value : ''
    })
    return typedContent
}

const storeAccess = async () => {
    await page.waitForSelector('button[type="submit"].button-save')
    await page.click('button[type="submit"].button-save')
    await page.waitFor(5000)
}

const extractFileNameFrom = async (selector: string): Promise<string> => {
    return page.evaluate((selector: string): string => {
        let result: any = document.querySelector(selector)
        if (result == null) {
            return ''
        }
        let file: any = result.files[0]
        if (file == null) {
            return ''
        }
        return file.name
    }, selector)
}

const uploadFileTo = async (selector: string, filePath: string, acceptDialog: boolean = false): Promise<string> => {
    if (acceptDialog) {
        await page.on('dialog', dialogAccept)
    }
    let [fileChooser] = await Promise.all([
        page.waitForFileChooser(),
        page.click(selector)
    ])
    await fileChooser.accept([filePath])
    await page.waitFor(1000)
    if (acceptDialog) {
        await page.removeListener('dialog', dialogAccept)
    }
    return extractFileNameFrom(selector)
}

export { 
    searchContentByTitle, 
    setAccessPageTo, 
    totalSearchedItem, 
    changePublicationStatus, 
    dialogAccept,
    inputContentTitle,
    totalRequiredMessages,
    storeContent,
    checkHeaderExists,
    setContentDetail,
    storeAccess,
    extractFileNameFrom,
    uploadFileTo,
    getSelectedAccess
}
