import { searchContentByTitle, totalSearchedItem } from "./content-utils"

export const contentPreview = () => {

    beforeAll(async () => {
        await Promise.all([
            page.goto('http://localhost:9300/cms', {waitUntil: 'networkidle0'})
        ])
    })

    it('should be able to search "Test Content Automation Updated"', async () => {
        await searchContentByTitle("Test Content Automation Updated")
        await expect(await totalSearchedItem()).not.toBe(0)
    }, 15000)

    it('should be able to get to update page', async () => {
        await page.click('#table-content > tbody > tr:nth-child(1) td div.btn-group button')
        await page.click('#table-content > tbody > tr:nth-child(1) td div.btn-group ul li a.preview-content')
        await page.waitFor(2000)
    }, 15000)

    it('should be the right title "Test Content Automation Updated"', async () => {
        let contentTitle: string = await page.evaluate((): string => {
            let element: HTMLElement | null = document.querySelector('html body div.pusher section.content-article-new div.ui.container div.ui.grid.stackable div.ten.wide.column div.title-content')
            return element?.innerHTML ?? ''
        })
        await expect(contentTitle).toBe('Test Content Automation Updated')
    })

    it('should have 2 files[sample-updated.pdf, sample-updated1.pdf]', async () => {
        let fileNames: string[] = await page.evaluate((): string[] => {
            let elements: NodeListOf<HTMLElement> = document.querySelectorAll('.extra-files div div div div.file-download')
            let names: string[] = []
            if (elements) {
                elements.forEach((e: HTMLElement) => {
                    names.push(e.innerHTML)
                })
            }
            return names
        })
        await expect(fileNames[0]).toBe("sample-updated.pdf")
        await expect(fileNames[1]).toBe("sample-updated1.pdf")
    })

}