const puppeteer = require('puppeteer')
const { setup: setupPuppeteer } = require('jest-environment-puppeteer')

module.exports = async function globalSetup() {
    await setupPuppeteer();
    // Open a new page
    const browser = await puppeteer.connect({
        browserWSEndpoint: process.env.PUPPETEER_WS_ENDPOINT,
    })
    const page = await browser.newPage()

    await Promise.all([
        page.goto('http://localhost:9300/blacklist', {waitUntil: 'networkidle0'})
    ])
    // Log in a user
    await page.click('a.btn-modal-login')
    await page.type('input.user', 'superuser@gmail.com', {delay: 200})
    let user = await page.evaluate(() => {
        let result = document.querySelector('input.user')
        return result != null ? result.value : ''
    })
    await page.type('.auth-credential-pass-input input.pass', '12345678', {delay: 200})
    let pass = await page.evaluate(() => {
        let result = document.querySelector('.auth-credential-pass-input input.pass')
        return result != null ? result.value : ''
    })
    await page.click('#submit_data')
    await page.waitForNavigation({waitUntil: 'networkidle0'})
  };
  