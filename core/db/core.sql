DROP SEQUENCE IF EXISTS "public"."blacklist_id_seq";
CREATE SEQUENCE "public"."blacklist_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

create table public.blacklist (
  "id" bigint NOT NULL DEFAULT nextval('blacklist_id_seq'::regclass),
  "json_data" text,
    "rekanan_id" bigint ,
    "sk" character varying(255),
    "npwp" character varying(255),
    "alamat" character varying(255),
    "tanggal_tayang" timestamp(6),
    "tanggal_turun_tayang" timestamp(6),
    "tanggal_berakhir" timestamp(6),
    "nama_propinsi" character varying(255),
    "id_propinsi" character varying(50),
    "nama_kabupaten" character varying(255),
    "id_kabupaten" character varying(50),
    "nama_penyedia" character varying(255),
    "kode_kldi" character varying(50),
    "nama_kldi" character varying(255),
    "kode_satker" character varying(50),
    "nama_satker" character varying(255),
    "status" character varying(255),
    "deskripsi" text,
    "jenis" character varying(255),
    "jenis_pengadaan" character varying(255),
    "id_pengadaan" character varying(255),
    "nama_pengadaan" character varying(255)
)
;

ALTER TABLE "public"."blacklist" ADD CONSTRAINT "blacklist._pkey" PRIMARY KEY ("id");

DROP SEQUENCE IF EXISTS "public"."blacklist_ikut_pengadaan_id_seq";
CREATE SEQUENCE "public"."blacklist_ikut_pengadaan_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

create table public.blacklist_ikut_pengadaan (
  "id" BIGINT NOT NULL DEFAULT nextval('blacklist_ikut_pengadaan_id_seq'::regclass),
  "id_pengadaan" bigint,
  "rkn_id" bigint,
  "nama_pengadaan" character varying(255)
)
;

ALTER TABLE "public"."blacklist_ikut_pengadaan" ADD CONSTRAINT "blacklist_ikut_lelang._pkey" PRIMARY KEY ("id");

CREATE TABLE "public"."satker" (
"id" int4,
"idsatker" varchar(255) COLLATE "default",
"idkldi" varchar(255) COLLATE "default",
"nama" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;
ALTER TABLE "public"."satker" ADD CONSTRAINT "satker._pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."configuration_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."configuration" (
  "id" int4 NOT NULL DEFAULT nextval('configuration_id_seq'::regclass),
  "key" varchar(255) COLLATE "default",
  "config_value" varchar(255) COLLATE "default",
  "created_at" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
  "created_by" bigint,
  "updated_at" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
  "updated_by" bigint,
  "deleted_at" timestamp(6) DEFAULT NULL,
  "deleted_by" bigint
)
WITH (OIDS=FALSE)
;
ALTER TABLE "public"."configuration" ADD CONSTRAINT "configuration._pkey" PRIMARY KEY ("id");
INSERT INTO "public"."configuration" ("key", "config_value", "created_at", "created_by", "updated_at", "updated_by", "deleted_at", "deleted_by") VALUES ('tender_classification', 'false', '2019-12-04 18:10:36.035075', NULL, '2019-12-05 08:55:49.948', '1', NULL, NULL);




