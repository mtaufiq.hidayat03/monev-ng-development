
DROP VIEW IF EXISTS "public"."page_13";

CREATE OR REPLACE VIEW "public"."page_13" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'ulp_ukpbj'::text) THEN pd.value
	ELSE 0
	END
) AS ulp_ukpbj, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ulp_ukpbj_permanen_struktural'::text) THEN pd.value
	ELSE 0
	END
) AS ulp_ukpbj_permanen_struktural, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ulp_ukpbj_adhoc'::text) THEN pd.value
	ELSE 0
	END
) AS ulp_ukpbj_adhoc, 
sum(
	CASE
	WHEN ((pd.header)::text = 'kl_ulp_ukpbj_permanen_struktural'::text) THEN pd.value
	ELSE 0
	END
) AS kl_ulp_ukpbj_permanen_struktural, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pd_ulp_ukpbj_permanen_struktural'::text) THEN pd.value
	ELSE 0
	END
) AS pd_ulp_ukpbj_permanen_struktural, 
sum(
	CASE
	WHEN ((pd.header)::text = 'kl_ulp_ukpbj_adhoc'::text) THEN pd.value
	ELSE 0
	END
) AS kl_ulp_ukpbj_adhoc, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pd_ulp_ukpbj_adhoc'::text) THEN pd.value
	ELSE 0
	END
) AS pd_ulp_ukpbj_adhoc, 
sum(
	CASE
	WHEN ((pd.header)::text = 'layanan_pengadaan_elektronik'::text) THEN pd.value
	ELSE 0
	END
) AS layanan_pengadaan_elektronik, 
sum(
	CASE
	WHEN ((pd.header)::text = 'lpse_terstandar'::text) THEN pd.value
	ELSE 0
	END
) AS lpse_terstandar, 
sum(
	CASE
	WHEN ((pd.header)::text = 'lpse_terinstal_spse4'::text) THEN pd.value
	ELSE 0
	END
) AS lpse_terinstal_spse4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'lpse_17standar'::text) THEN pd.value
	ELSE 0
	END
) AS lpse_17standar, 
sum(
	CASE
	WHEN ((pd.header)::text = 'lpse_pengguna_spse4'::text) THEN pd.value
	ELSE 0
	END
) AS lpse_pengguna_spse4
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;
