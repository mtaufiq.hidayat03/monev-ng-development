
DROP VIEW IF EXISTS "public"."page_27";

CREATE OR REPLACE VIEW "public"."page_27" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_pagu_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS p27_pagu_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_pagu_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS p27_pagu_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_pagu_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS p27_pagu_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_pagu_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS p27_pagu_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_pagu_kecil_3'::text) THEN pd.value
	ELSE 0
	END
) AS p27_pagu_kecil_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_pagu_non_3'::text) THEN pd.value
	ELSE 0
	END
) AS p27_pagu_non_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_pagu_kecil_4'::text) THEN pd.value
	ELSE 0
	END
) AS p27_pagu_kecil_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_pagu_non_4'::text) THEN pd.value
	ELSE 0
	END
) AS p27_pagu_non_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_pagu_kecil_5'::text) THEN pd.value
	ELSE 0
	END
) AS p27_pagu_kecil_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_pagu_non_5'::text) THEN pd.value
	ELSE 0
	END
) AS p27_pagu_non_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_paket_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS p27_paket_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_paket_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS p27_paket_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_paket_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS p27_paket_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_paket_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS p27_paket_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_paket_kecil_3'::text) THEN pd.value
	ELSE 0
	END
) AS p27_paket_kecil_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_paket_non_3'::text) THEN pd.value
	ELSE 0
	END
) AS p27_paket_non_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_paket_kecil_4'::text) THEN pd.value
	ELSE 0
	END
) AS p27_paket_kecil_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_paket_non_4'::text) THEN pd.value
	ELSE 0
	END
) AS p27_paket_non_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_paket_kecil_5'::text) THEN pd.value
	ELSE 0
	END
) AS p27_paket_kecil_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_paket_non_5'::text) THEN pd.value
	ELSE 0
	END
) AS p27_paket_non_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_pagu_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_pagu_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_pagu_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_pagu_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_pagu_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_pagu_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_pagu_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_pagu_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_pagu_kecil_3'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_pagu_kecil_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_pagu_non_3'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_pagu_non_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_pagu_kecil_4'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_pagu_kecil_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_pagu_non_4'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_pagu_non_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_pagu_kecil_5'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_pagu_kecil_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_pagu_non_5'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_pagu_non_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_paket_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_paket_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_paket_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_paket_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_paket_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_paket_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_paket_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_paket_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_paket_kecil_3'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_paket_kecil_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_paket_non_3'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_paket_non_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_paket_kecil_4'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_paket_kecil_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_paket_non_4'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_paket_non_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_paket_kecil_5'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_paket_kecil_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_paket_non_5'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_paket_non_5
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;
