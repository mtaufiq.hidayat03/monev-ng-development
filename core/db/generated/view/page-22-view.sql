
DROP VIEW IF EXISTS "public"."page_22";

CREATE OR REPLACE VIEW "public"."page_22" AS 
SELECT pd.procurement_id, pd.flag,
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptt_item_name_1' 
) AS ptt_item_name_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptt_item_value_1'::text) THEN pd.value
	ELSE 0
	END
) AS ptt_item_value_1, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptt_item_name_2' 
) AS ptt_item_name_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptt_item_value_2'::text) THEN pd.value
	ELSE 0
	END
) AS ptt_item_value_2, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptt_item_name_3' 
) AS ptt_item_name_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptt_item_value_3'::text) THEN pd.value
	ELSE 0
	END
) AS ptt_item_value_3, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptt_item_name_4' 
) AS ptt_item_name_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptt_item_value_4'::text) THEN pd.value
	ELSE 0
	END
) AS ptt_item_value_4, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptt_item_name_5' 
) AS ptt_item_name_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptt_item_value_5'::text) THEN pd.value
	ELSE 0
	END
) AS ptt_item_value_5, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptt_item_name_6' 
) AS ptt_item_name_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptt_item_value_6'::text) THEN pd.value
	ELSE 0
	END
) AS ptt_item_value_6, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptt_item_name_7' 
) AS ptt_item_name_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptt_item_value_7'::text) THEN pd.value
	ELSE 0
	END
) AS ptt_item_value_7, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptt_item_name_8' 
) AS ptt_item_name_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptt_item_value_8'::text) THEN pd.value
	ELSE 0
	END
) AS ptt_item_value_8, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptt_item_name_9' 
) AS ptt_item_name_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptt_item_value_9'::text) THEN pd.value
	ELSE 0
	END
) AS ptt_item_value_9, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptt_item_name_10' 
) AS ptt_item_name_10, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptt_item_value_10'::text) THEN pd.value
	ELSE 0
	END
) AS ptt_item_value_10
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;
