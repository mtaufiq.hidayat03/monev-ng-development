
DROP VIEW IF EXISTS "public"."page_6";

CREATE OR REPLACE VIEW "public"."page_6" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'bklp'::text) THEN pd.value
	ELSE 0
	END
) AS bklp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'harga_belanja_pengadaan'::text) THEN pd.value
	ELSE 0
	END
) AS harga_belanja_pengadaan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'persentase_belanja_pengadaan'::text) THEN pd.value
	ELSE 0
	END
) AS persentase_belanja_pengadaan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p6_ulp_ukpbj'::text) THEN pd.value
	ELSE 0
	END
) AS p6_ulp_ukpbj, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ukpbk_permanen_struktural'::text) THEN pd.value
	ELSE 0
	END
) AS ukpbk_permanen_struktural, 
sum(
	CASE
	WHEN ((pd.header)::text = 'lpse_1'::text) THEN pd.value
	ELSE 0
	END
) AS lpse_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'lpse_2'::text) THEN pd.value
	ELSE 0
	END
) AS lpse_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'lpse_standar'::text) THEN pd.value
	ELSE 0
	END
) AS lpse_standar
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;
