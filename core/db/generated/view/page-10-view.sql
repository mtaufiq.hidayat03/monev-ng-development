
DROP VIEW IF EXISTS "public"."page_10";

CREATE OR REPLACE VIEW "public"."page_10" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'harga_nilai_pesanan'::text) THEN pd.value
	ELSE 0
	END
) AS harga_nilai_pesanan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_harga_pesanan'::text) THEN pd.value
	ELSE 0
	END
) AS paket_harga_pesanan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'komoditas_harga_pesanan'::text) THEN pd.value
	ELSE 0
	END
) AS komoditas_harga_pesanan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'produk_harga_pesanan'::text) THEN pd.value
	ELSE 0
	END
) AS produk_harga_pesanan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_harga_pesanan'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_harga_pesanan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'komoditas_nasional'::text) THEN pd.value
	ELSE 0
	END
) AS komoditas_nasional, 
sum(
	CASE
	WHEN ((pd.header)::text = 'produk_nasional'::text) THEN pd.value
	ELSE 0
	END
) AS produk_nasional, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_nasional'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_nasional, 
sum(
	CASE
	WHEN ((pd.header)::text = 'kementrian_sektoral_lokal'::text) THEN pd.value
	ELSE 0
	END
) AS kementrian_sektoral_lokal, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pemda_sektoral_lokal'::text) THEN pd.value
	ELSE 0
	END
) AS pemda_sektoral_lokal, 
sum(
	CASE
	WHEN ((pd.header)::text = 'komoditas_sektoral_lokal'::text) THEN pd.value
	ELSE 0
	END
) AS komoditas_sektoral_lokal, 
sum(
	CASE
	WHEN ((pd.header)::text = 'produk_sektoral_lokal'::text) THEN pd.value
	ELSE 0
	END
) AS produk_sektoral_lokal, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_sektoral_lokal'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_sektoral_lokal, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_1'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_2'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_3'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_4'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_5'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_6'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_7'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_8'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_9'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_10'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_10, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_11'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_11, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_12'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_12, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_13'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_13, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_14'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_14, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_name_1'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_name_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_paket_1'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_paket_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_harga_1'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_harga_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_name_2'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_name_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_paket_2'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_paket_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_harga_2'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_harga_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_name_3'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_name_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_paket_3'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_paket_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_harga_3'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_harga_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_name_4'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_name_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_paket_4'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_paket_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_harga_4'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_harga_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_name_5'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_name_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_paket_5'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_paket_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_harga_5'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_harga_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_name_6'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_name_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_paket_6'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_paket_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_harga_6'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_harga_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_name_7'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_name_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_paket_7'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_paket_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_harga_7'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_harga_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_name_8'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_name_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_paket_8'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_paket_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_harga_8'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_harga_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_name_9'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_name_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_paket_9'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_paket_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_harga_9'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_harga_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_name_10'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_name_10, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_paket_10'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_paket_10, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_harga_10'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_harga_10
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;
