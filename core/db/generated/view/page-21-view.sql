
DROP VIEW IF EXISTS "public"."page_21";

CREATE OR REPLACE VIEW "public"."page_21" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_1'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_2'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_3'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_3'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_3'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_4'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_4'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_4'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_5'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_5'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_5'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_6'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_6'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_6'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_7'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_7'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_7'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_8'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_8'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_8'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_9'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_9'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_9'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_10'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_10, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_10'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_10, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_10'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_10, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_11'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_11, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_11'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_11, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_11'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_11, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_12'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_12, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_12'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_12, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_12'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_12, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_13'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_13, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_13'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_13, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_13'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_13, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_14'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_14, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_14'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_14, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_14'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_14, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_15'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_15, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_15'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_15, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_15'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_15
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;
