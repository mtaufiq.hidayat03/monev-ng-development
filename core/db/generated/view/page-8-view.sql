
DROP VIEW IF EXISTS "public"."page_8";

CREATE OR REPLACE VIEW "public"."page_8" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'bp_2018'::text) THEN pd.value
	ELSE 0
	END
) AS bp_2018, 
sum(
	CASE
	WHEN ((pd.header)::text = 'perencanaan_pengadaan'::text) THEN pd.value
	ELSE 0
	END
) AS perencanaan_pengadaan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'persentase_penyedia'::text) THEN pd.value
	ELSE 0
	END
) AS persentase_penyedia, 
sum(
	CASE
	WHEN ((pd.header)::text = 'harga_penyedia'::text) THEN pd.value
	ELSE 0
	END
) AS harga_penyedia, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_penyedia'::text) THEN pd.value
	ELSE 0
	END
) AS paket_penyedia, 
sum(
	CASE
	WHEN ((pd.header)::text = 'persentase_swakelola'::text) THEN pd.value
	ELSE 0
	END
) AS persentase_swakelola, 
sum(
	CASE
	WHEN ((pd.header)::text = 'harga_swakelola'::text) THEN pd.value
	ELSE 0
	END
) AS harga_swakelola, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_swakelola'::text) THEN pd.value
	ELSE 0
	END
) AS paket_swakelola, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jp_pk_harga'::text) THEN pd.value
	ELSE 0
	END
) AS jp_pk_harga, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jp_pk_paket'::text) THEN pd.value
	ELSE 0
	END
) AS jp_pk_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jp_barang_harga'::text) THEN pd.value
	ELSE 0
	END
) AS jp_barang_harga, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jp_barang_paket'::text) THEN pd.value
	ELSE 0
	END
) AS jp_barang_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jp_jk_harga'::text) THEN pd.value
	ELSE 0
	END
) AS jp_jk_harga, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jp_jk_paket'::text) THEN pd.value
	ELSE 0
	END
) AS jp_jk_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jp_jl_harga'::text) THEN pd.value
	ELSE 0
	END
) AS jp_jl_harga, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jp_jl_paket'::text) THEN pd.value
	ELSE 0
	END
) AS jp_jl_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pengadaan_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS pengadaan_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pengadaan_paket'::text) THEN pd.value
	ELSE 0
	END
) AS pengadaan_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penunjukan_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS penunjukan_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penunjukan_paket'::text) THEN pd.value
	ELSE 0
	END
) AS penunjukan_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'purchasing_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS purchasing_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'purchasing_paket'::text) THEN pd.value
	ELSE 0
	END
) AS purchasing_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'tender_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS tender_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'tender_paket'::text) THEN pd.value
	ELSE 0
	END
) AS tender_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'tender_cepat_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS tender_cepat_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'tender_cepat_paket'::text) THEN pd.value
	ELSE 0
	END
) AS tender_cepat_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'seleksi_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS seleksi_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'seleksi_paket'::text) THEN pd.value
	ELSE 0
	END
) AS seleksi_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'sdk_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS sdk_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'sdk_paket'::text) THEN pd.value
	ELSE 0
	END
) AS sdk_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'lainnya_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS lainnya_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'lainnya_paket'::text) THEN pd.value
	ELSE 0
	END
) AS lainnya_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_1'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_2'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_3'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_4'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_5'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_6'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_7'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_8'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_9'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_10'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_10, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_11'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_11, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_12'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_12, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_13'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_13, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_14'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_14, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_15'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_15, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_16'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_16, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_17'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_17
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;
