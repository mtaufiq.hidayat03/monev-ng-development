
DROP VIEW IF EXISTS "public"."page_16";

CREATE OR REPLACE VIEW "public"."page_16" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'jumlah_layanan_advokasi'::text) THEN pd.value
	ELSE 0
	END
) AS jumlah_layanan_advokasi, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jla_wilayah1'::text) THEN pd.value
	ELSE 0
	END
) AS jla_wilayah1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jla_presentase_wilayah1'::text) THEN pd.value
	ELSE 0
	END
) AS jla_presentase_wilayah1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jla_wilayah2'::text) THEN pd.value
	ELSE 0
	END
) AS jla_wilayah2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jla_presentase_wilayah2'::text) THEN pd.value
	ELSE 0
	END
) AS jla_presentase_wilayah2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'nptla_wilayah1'::text) THEN pd.value
	ELSE 0
	END
) AS nptla_wilayah1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'nptla_presentase_wilayah1'::text) THEN pd.value
	ELSE 0
	END
) AS nptla_presentase_wilayah1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'nptla_wilayah2'::text) THEN pd.value
	ELSE 0
	END
) AS nptla_wilayah2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'nptla_presentase_wilayah2'::text) THEN pd.value
	ELSE 0
	END
) AS nptla_presentase_wilayah2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pengaduan_paket'::text) THEN pd.value
	ELSE 0
	END
) AS pengaduan_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'total_pagu_pengaduan'::text) THEN pd.value
	ELSE 0
	END
) AS total_pagu_pengaduan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pekerjaan_konstruksi'::text) THEN pd.value
	ELSE 0
	END
) AS pekerjaan_konstruksi, 
sum(
	CASE
	WHEN ((pd.header)::text = 'barang_aduan'::text) THEN pd.value
	ELSE 0
	END
) AS barang_aduan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jasa_konsultasi_aduan'::text) THEN pd.value
	ELSE 0
	END
) AS jasa_konsultasi_aduan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jasa_lainnya_aduan'::text) THEN pd.value
	ELSE 0
	END
) AS jasa_lainnya_aduan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'prakontrak_pengaduan'::text) THEN pd.value
	ELSE 0
	END
) AS prakontrak_pengaduan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pascakontrak_pengaduan'::text) THEN pd.value
	ELSE 0
	END
) AS pascakontrak_pengaduan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'kasus_pengadaan'::text) THEN pd.value
	ELSE 0
	END
) AS kasus_pengadaan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'total_pagu_kasus'::text) THEN pd.value
	ELSE 0
	END
) AS total_pagu_kasus, 
sum(
	CASE
	WHEN ((pd.header)::text = 'persentase_penyidikan'::text) THEN pd.value
	ELSE 0
	END
) AS persentase_penyidikan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'persentase_persidangan'::text) THEN pd.value
	ELSE 0
	END
) AS persentase_persidangan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'persentase_penyelidikan'::text) THEN pd.value
	ELSE 0
	END
) AS persentase_penyelidikan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pemohon_kepolisian'::text) THEN pd.value
	ELSE 0
	END
) AS pemohon_kepolisian, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pemohon_kejaksaan'::text) THEN pd.value
	ELSE 0
	END
) AS pemohon_kejaksaan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pemohon_kpk'::text) THEN pd.value
	ELSE 0
	END
) AS pemohon_kpk, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pemohon_kppu'::text) THEN pd.value
	ELSE 0
	END
) AS pemohon_kppu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pemohon_ombudsman'::text) THEN pd.value
	ELSE 0
	END
) AS pemohon_ombudsman, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pemohon_lainnya'::text) THEN pd.value
	ELSE 0
	END
) AS pemohon_lainnya
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;
