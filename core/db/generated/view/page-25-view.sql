
DROP VIEW IF EXISTS "public"."page_25";

CREATE OR REPLACE VIEW "public"."page_25" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_blacklist_hps_kurang_200jt'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_blacklist_hps_kurang_200jt, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_blacklist_hps_200jt'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_blacklist_hps_200jt, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_blacklist_hps_2m'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_blacklist_hps_2m, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_blacklist_hps_5m'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_blacklist_hps_5m, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_blacklist_hps_50m'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_blacklist_hps_50m, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_blacklist_hps_lebih_50m'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_blacklist_hps_lebih_50m, 
sum(
	CASE
	WHEN ((pd.header)::text = 'total_pelanggaran_1'::text) THEN pd.value
	ELSE 0
	END
) AS total_pelanggaran_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'deskripsi_pelanggaran_1'::text) THEN pd.value
	ELSE 0
	END
) AS deskripsi_pelanggaran_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'total_pelanggaran_2'::text) THEN pd.value
	ELSE 0
	END
) AS total_pelanggaran_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'deskripsi_pelanggaran_2'::text) THEN pd.value
	ELSE 0
	END
) AS deskripsi_pelanggaran_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'total_pelanggaran_3'::text) THEN pd.value
	ELSE 0
	END
) AS total_pelanggaran_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'deskripsi_pelanggaran_3'::text) THEN pd.value
	ELSE 0
	END
) AS deskripsi_pelanggaran_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'page25_bln_1'::text) THEN pd.value
	ELSE 0
	END
) AS page25_bln_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'page25_bln_2'::text) THEN pd.value
	ELSE 0
	END
) AS page25_bln_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'page25_bln_3'::text) THEN pd.value
	ELSE 0
	END
) AS page25_bln_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'page25_bln_4'::text) THEN pd.value
	ELSE 0
	END
) AS page25_bln_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'page25_bln_5'::text) THEN pd.value
	ELSE 0
	END
) AS page25_bln_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'page25_bln_6'::text) THEN pd.value
	ELSE 0
	END
) AS page25_bln_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'page25_bln_7'::text) THEN pd.value
	ELSE 0
	END
) AS page25_bln_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'page25_bln_8'::text) THEN pd.value
	ELSE 0
	END
) AS page25_bln_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'page25_bln_9'::text) THEN pd.value
	ELSE 0
	END
) AS page25_bln_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'page25_bln_10'::text) THEN pd.value
	ELSE 0
	END
) AS page25_bln_10, 
sum(
	CASE
	WHEN ((pd.header)::text = 'page25_bln_11'::text) THEN pd.value
	ELSE 0
	END
) AS page25_bln_11, 
sum(
	CASE
	WHEN ((pd.header)::text = 'page25_bln_12'::text) THEN pd.value
	ELSE 0
	END
) AS page25_bln_12
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;
