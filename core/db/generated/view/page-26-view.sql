
DROP VIEW IF EXISTS "public"."page_26";

CREATE OR REPLACE VIEW "public"."page_26" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'tahun'::text) THEN pd.value
	ELSE 0
	END
) AS tahun, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket'::text) THEN pd.value
	ELSE 0
	END
) AS paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pagu'::text) THEN pd.value
	ELSE 0
	END
) AS pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'hari'::text) THEN pd.value
	ELSE 0
	END
) AS hari, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_2'::text) THEN pd.value
	ELSE 0
	END
) AS paket_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pagu_2'::text) THEN pd.value
	ELSE 0
	END
) AS pagu_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'hari_2'::text) THEN pd.value
	ELSE 0
	END
) AS hari_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pagu_t_1'::text) THEN pd.value
	ELSE 0
	END
) AS pagu_t_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pagu_t_2'::text) THEN pd.value
	ELSE 0
	END
) AS pagu_t_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pagu_t_3'::text) THEN pd.value
	ELSE 0
	END
) AS pagu_t_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pagu_t_4'::text) THEN pd.value
	ELSE 0
	END
) AS pagu_t_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_t_1'::text) THEN pd.value
	ELSE 0
	END
) AS paket_t_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_t_2'::text) THEN pd.value
	ELSE 0
	END
) AS paket_t_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_t_3'::text) THEN pd.value
	ELSE 0
	END
) AS paket_t_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_t_4'::text) THEN pd.value
	ELSE 0
	END
) AS paket_t_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'selesai_1'::text) THEN pd.value
	ELSE 0
	END
) AS selesai_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'gagal_1'::text) THEN pd.value
	ELSE 0
	END
) AS gagal_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'selesai_2'::text) THEN pd.value
	ELSE 0
	END
) AS selesai_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'gagal_2'::text) THEN pd.value
	ELSE 0
	END
) AS gagal_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'selesai_3'::text) THEN pd.value
	ELSE 0
	END
) AS selesai_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'gagal_3'::text) THEN pd.value
	ELSE 0
	END
) AS gagal_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'selesai_4'::text) THEN pd.value
	ELSE 0
	END
) AS selesai_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'gagal_4'::text) THEN pd.value
	ELSE 0
	END
) AS gagal_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'selesai_5'::text) THEN pd.value
	ELSE 0
	END
) AS selesai_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'gagal_5'::text) THEN pd.value
	ELSE 0
	END
) AS gagal_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'alasan_1'::text) THEN pd.value
	ELSE 0
	END
) AS alasan_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'alasan_2'::text) THEN pd.value
	ELSE 0
	END
) AS alasan_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'alasan_3'::text) THEN pd.value
	ELSE 0
	END
) AS alasan_3
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;
