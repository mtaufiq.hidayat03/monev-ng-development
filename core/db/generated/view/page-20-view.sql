
DROP VIEW IF EXISTS "public"."page_20";

CREATE OR REPLACE VIEW "public"."page_20" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_1'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_1'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_2'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_2'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_3'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_3'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_4'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_4'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_5'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_5'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_6'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_6'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_7'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_7'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_8'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_8'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_9'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_9'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_10'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_10, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_10'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_10, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_11'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_11, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_11'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_11, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_12'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_12, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_12'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_12, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_13'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_13, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_13'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_13, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_14'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_14, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_14'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_14, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_15'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_15, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_15'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_15, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_16'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_16, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_16'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_16, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_17'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_17, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_17'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_17, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_18'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_18, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_18'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_18, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_19'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_19, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_19'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_19, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_20'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_20, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_20'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_20, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_21'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_21, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_21'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_21, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_22'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_22, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_22'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_22, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_23'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_23, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_23'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_23, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_24'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_24, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_24'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_24, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_25'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_25, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_25'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_25, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_26'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_26, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_26'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_26, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_27'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_27, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_27'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_27, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_28'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_28, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_28'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_28, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_29'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_29, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_29'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_29, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_30'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_30, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_30'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_30, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_31'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_31, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_31'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_31, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_32'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_32, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_32'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_32, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_33'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_33, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_33'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_33, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_34'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_34, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_34'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_34
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;
