
DROP VIEW IF EXISTS "public"."page_7";

CREATE OR REPLACE VIEW "public"."page_7" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'ipp_ld_paket'::text) THEN pd.value
	ELSE 0
	END
) AS ipp_ld_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ipp_ld_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS ipp_ld_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ipp_lsc_paket'::text) THEN pd.value
	ELSE 0
	END
) AS ipp_lsc_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ipp_lsc_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS ipp_lsc_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ipp_li_paket'::text) THEN pd.value
	ELSE 0
	END
) AS ipp_li_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ipp_li_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS ipp_li_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ipp_konsolidasi_paket'::text) THEN pd.value
	ELSE 0
	END
) AS ipp_konsolidasi_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ipp_konsolidasi_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS ipp_konsolidasi_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ipp_payung_paket'::text) THEN pd.value
	ELSE 0
	END
) AS ipp_payung_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ipp_payung_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS ipp_payung_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ipp_jamak_paket'::text) THEN pd.value
	ELSE 0
	END
) AS ipp_jamak_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ipp_jamak_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS ipp_jamak_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'psuk_rup_paket'::text) THEN pd.value
	ELSE 0
	END
) AS psuk_rup_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'psuk_rup_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS psuk_rup_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'psuk_realisasi_paket'::text) THEN pd.value
	ELSE 0
	END
) AS psuk_realisasi_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'psuk_realisasi_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS psuk_realisasi_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'persentase_rp'::text) THEN pd.value
	ELSE 0
	END
) AS persentase_rp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'harga_rp'::text) THEN pd.value
	ELSE 0
	END
) AS harga_rp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'swakelola_rp'::text) THEN pd.value
	ELSE 0
	END
) AS swakelola_rp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'tendering_rp'::text) THEN pd.value
	ELSE 0
	END
) AS tendering_rp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'purchasing_rp'::text) THEN pd.value
	ELSE 0
	END
) AS purchasing_rp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pengadaan_langsung_rp'::text) THEN pd.value
	ELSE 0
	END
) AS pengadaan_langsung_rp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penunjukan_langsung_rp'::text) THEN pd.value
	ELSE 0
	END
) AS penunjukan_langsung_rp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'sayembara_kontes_rp'::text) THEN pd.value
	ELSE 0
	END
) AS sayembara_kontes_rp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'lainnya_rp'::text) THEN pd.value
	ELSE 0
	END
) AS lainnya_rp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pagu_pengumuman_et'::text) THEN pd.value
	ELSE 0
	END
) AS pagu_pengumuman_et, 
sum(
	CASE
	WHEN ((pd.header)::text = 'hasil_tender_et'::text) THEN pd.value
	ELSE 0
	END
) AS hasil_tender_et, 
sum(
	CASE
	WHEN ((pd.header)::text = 'nilai_harga_ep'::text) THEN pd.value
	ELSE 0
	END
) AS nilai_harga_ep, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ppb_dasar'::text) THEN pd.value
	ELSE 0
	END
) AS ppb_dasar, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ppbk'::text) THEN pd.value
	ELSE 0
	END
) AS ppbk, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ppbk_jf'::text) THEN pd.value
	ELSE 0
	END
) AS ppbk_jf, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pd_spse'::text) THEN pd.value
	ELSE 0
	END
) AS pd_spse, 
sum(
	CASE
	WHEN ((pd.header)::text = 'sanksi_df'::text) THEN pd.value
	ELSE 0
	END
) AS sanksi_df, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ph_layanan_advokasi'::text) THEN pd.value
	ELSE 0
	END
) AS ph_layanan_advokasi, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ph_layanan_advokasi_value'::text) THEN pd.value
	ELSE 0
	END
) AS ph_layanan_advokasi_value, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ph_kasus_pengadaan'::text) THEN pd.value
	ELSE 0
	END
) AS ph_kasus_pengadaan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ph_nilai_kasus'::text) THEN pd.value
	ELSE 0
	END
) AS ph_nilai_kasus, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ph_kontrak'::text) THEN pd.value
	ELSE 0
	END
) AS ph_kontrak, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ph_nilai_kontrak'::text) THEN pd.value
	ELSE 0
	END
) AS ph_nilai_kontrak
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;
