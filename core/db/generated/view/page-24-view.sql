
DROP VIEW IF EXISTS "public"."page_24";

CREATE OR REPLACE VIEW "public"."page_24" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'kena_sanksi_blacklist_1'::text) THEN pd.value
	ELSE 0
	END
) AS kena_sanksi_blacklist_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pekerjaan_konstruksi_blacklist'::text) THEN pd.value
	ELSE 0
	END
) AS pekerjaan_konstruksi_blacklist, 
sum(
	CASE
	WHEN ((pd.header)::text = 'barang_blacklist'::text) THEN pd.value
	ELSE 0
	END
) AS barang_blacklist, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jasa_konsultasi_blacklist'::text) THEN pd.value
	ELSE 0
	END
) AS jasa_konsultasi_blacklist
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;
