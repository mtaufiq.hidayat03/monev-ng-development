
DROP VIEW IF EXISTS "public"."page_35";

CREATE OR REPLACE VIEW "public"."page_35" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'ptf_barang_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS ptf_barang_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptf_barang_paket'::text) THEN pd.value
	ELSE 0
	END
) AS ptf_barang_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptf_pekerjaan_konstruksi_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS ptf_pekerjaan_konstruksi_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptf_pekerjaan_konstruksi_paket'::text) THEN pd.value
	ELSE 0
	END
) AS ptf_pekerjaan_konstruksi_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptf_jasa_konsultansi_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS ptf_jasa_konsultansi_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptf_jasa_konsultansi_paket'::text) THEN pd.value
	ELSE 0
	END
) AS ptf_jasa_konsultansi_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptf_jasa_lainnya_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS ptf_jasa_lainnya_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptf_jasa_lainnya_paket'::text) THEN pd.value
	ELSE 0
	END
) AS ptf_jasa_lainnya_paket
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;
