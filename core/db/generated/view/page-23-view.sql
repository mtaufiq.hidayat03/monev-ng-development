
DROP VIEW IF EXISTS "public"."page_23";

CREATE OR REPLACE VIEW "public"."page_23" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'sipil_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS sipil_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'sipil_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS sipil_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'sipil_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS sipil_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'sipil_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS sipil_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'bangunan_gedung_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS bangunan_gedung_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'bangunan_gedung_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS bangunan_gedung_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'bangunan_gedung_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS bangunan_gedung_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'bangunan_gedung_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS bangunan_gedung_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'engineer_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS engineer_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'engineer_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS engineer_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'engineer_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS engineer_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'engineer_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS engineer_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'elektrikal_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS elektrikal_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'elektrikal_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS elektrikal_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'elektrikal_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS elektrikal_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'elektrikal_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS elektrikal_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'konsultan_spesialis_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS konsultan_spesialis_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'konsultan_spesialis_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS konsultan_spesialis_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'konsultan_spesialis_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS konsultan_spesialis_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'konsultan_spesialis_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS konsultan_spesialis_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pelaksana_spesialis_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS pelaksana_spesialis_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pelaksana_spesialis_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS pelaksana_spesialis_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pelaksana_spesialis_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS pelaksana_spesialis_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pelaksana_spesialis_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS pelaksana_spesialis_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penataan_ruang_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS penataan_ruang_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penataan_ruang_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS penataan_ruang_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penataan_ruang_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS penataan_ruang_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penataan_ruang_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS penataan_ruang_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'arsitektur_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS arsitektur_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'arsitektur_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS arsitektur_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'arsitektur_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS arsitektur_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'arsitektur_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS arsitektur_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'instalasi_mekanik_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS instalasi_mekanik_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'instalasi_mekanik_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS instalasi_mekanik_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'instalasi_mekanik_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS instalasi_mekanik_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'instalasi_mekanik_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS instalasi_mekanik_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'konsultansi_lainnya_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS konsultansi_lainnya_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'konsultansi_lainnya_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS konsultansi_lainnya_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'konsultansi_lainnya_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS konsultansi_lainnya_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'konsultansi_lainnya_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS konsultansi_lainnya_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pelaksana_lainnya_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS pelaksana_lainnya_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pelaksana_lainnya_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS pelaksana_lainnya_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pelaksana_lainnya_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS pelaksana_lainnya_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pelaksana_lainnya_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS pelaksana_lainnya_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'terintegrasi_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS terintegrasi_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'terintegrasi_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS terintegrasi_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'terintegrasi_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS terintegrasi_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'terintegrasi_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS terintegrasi_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pelaksana_keterampilan_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS pelaksana_keterampilan_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pelaksana_keterampilan_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS pelaksana_keterampilan_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pelaksana_keterampilan_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS pelaksana_keterampilan_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pelaksana_keterampilan_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS pelaksana_keterampilan_non_2
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;

DROP VIEW IF EXISTS "public"."page_23_p23_first_list";

CREATE OR REPLACE VIEW "public"."page_23_p23_first_list" AS 
SELECT pd.procurement_id, pd.flag,
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptwentythree_first_name_1' 
) AS ptwentythree_first_name_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwentythree_first_nilai_1'::text) THEN pd.value
	ELSE 0
	END
) AS ptwentythree_first_nilai_1, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptwentythree_first_name_2' 
) AS ptwentythree_first_name_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwentythree_first_nilai_2'::text) THEN pd.value
	ELSE 0
	END
) AS ptwentythree_first_nilai_2, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptwentythree_first_name_3' 
) AS ptwentythree_first_name_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwentythree_first_nilai_3'::text) THEN pd.value
	ELSE 0
	END
) AS ptwentythree_first_nilai_3, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptwentythree_first_name_4' 
) AS ptwentythree_first_name_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwentythree_first_nilai_4'::text) THEN pd.value
	ELSE 0
	END
) AS ptwentythree_first_nilai_4, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptwentythree_first_name_5' 
) AS ptwentythree_first_name_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwentythree_first_nilai_5'::text) THEN pd.value
	ELSE 0
	END
) AS ptwentythree_first_nilai_5, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptwentythree_first_name_6' 
) AS ptwentythree_first_name_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwentythree_first_nilai_6'::text) THEN pd.value
	ELSE 0
	END
) AS ptwentythree_first_nilai_6, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptwentythree_first_name_7' 
) AS ptwentythree_first_name_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwentythree_first_nilai_7'::text) THEN pd.value
	ELSE 0
	END
) AS ptwentythree_first_nilai_7, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptwentythree_first_name_8' 
) AS ptwentythree_first_name_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwentythree_first_nilai_8'::text) THEN pd.value
	ELSE 0
	END
) AS ptwentythree_first_nilai_8, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptwentythree_first_name_9' 
) AS ptwentythree_first_name_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwentythree_first_nilai_9'::text) THEN pd.value
	ELSE 0
	END
) AS ptwentythree_first_nilai_9, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptwentythree_first_name_10' 
) AS ptwentythree_first_name_10, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwentythree_first_nilai_10'::text) THEN pd.value
	ELSE 0
	END
) AS ptwentythree_first_nilai_10
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;
