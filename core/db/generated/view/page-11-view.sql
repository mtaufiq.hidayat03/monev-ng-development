
DROP VIEW IF EXISTS "public"."page_11";

CREATE OR REPLACE VIEW "public"."page_11" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'st_januari'::text) THEN pd.value
	ELSE 0
	END
) AS st_januari, 
sum(
	CASE
	WHEN ((pd.header)::text = 'srsp_januari'::text) THEN pd.value
	ELSE 0
	END
) AS srsp_januari, 
sum(
	CASE
	WHEN ((pd.header)::text = 'st_februari'::text) THEN pd.value
	ELSE 0
	END
) AS st_februari, 
sum(
	CASE
	WHEN ((pd.header)::text = 'srsp_februari'::text) THEN pd.value
	ELSE 0
	END
) AS srsp_februari, 
sum(
	CASE
	WHEN ((pd.header)::text = 'st_maret'::text) THEN pd.value
	ELSE 0
	END
) AS st_maret, 
sum(
	CASE
	WHEN ((pd.header)::text = 'srsp_maret'::text) THEN pd.value
	ELSE 0
	END
) AS srsp_maret, 
sum(
	CASE
	WHEN ((pd.header)::text = 'st_april'::text) THEN pd.value
	ELSE 0
	END
) AS st_april, 
sum(
	CASE
	WHEN ((pd.header)::text = 'srsp_april'::text) THEN pd.value
	ELSE 0
	END
) AS srsp_april, 
sum(
	CASE
	WHEN ((pd.header)::text = 'st_mei'::text) THEN pd.value
	ELSE 0
	END
) AS st_mei, 
sum(
	CASE
	WHEN ((pd.header)::text = 'srsp_mei'::text) THEN pd.value
	ELSE 0
	END
) AS srsp_mei, 
sum(
	CASE
	WHEN ((pd.header)::text = 'st_juni'::text) THEN pd.value
	ELSE 0
	END
) AS st_juni, 
sum(
	CASE
	WHEN ((pd.header)::text = 'srsp_juni'::text) THEN pd.value
	ELSE 0
	END
) AS srsp_juni, 
sum(
	CASE
	WHEN ((pd.header)::text = 'st_juli'::text) THEN pd.value
	ELSE 0
	END
) AS st_juli, 
sum(
	CASE
	WHEN ((pd.header)::text = 'srsp_juli'::text) THEN pd.value
	ELSE 0
	END
) AS srsp_juli, 
sum(
	CASE
	WHEN ((pd.header)::text = 'st_agustus'::text) THEN pd.value
	ELSE 0
	END
) AS st_agustus, 
sum(
	CASE
	WHEN ((pd.header)::text = 'srsp_agustus'::text) THEN pd.value
	ELSE 0
	END
) AS srsp_agustus, 
sum(
	CASE
	WHEN ((pd.header)::text = 'st_september'::text) THEN pd.value
	ELSE 0
	END
) AS st_september, 
sum(
	CASE
	WHEN ((pd.header)::text = 'srsp_september'::text) THEN pd.value
	ELSE 0
	END
) AS srsp_september, 
sum(
	CASE
	WHEN ((pd.header)::text = 'st_oktober'::text) THEN pd.value
	ELSE 0
	END
) AS st_oktober, 
sum(
	CASE
	WHEN ((pd.header)::text = 'srsp_oktober'::text) THEN pd.value
	ELSE 0
	END
) AS srsp_oktober, 
sum(
	CASE
	WHEN ((pd.header)::text = 'st_november'::text) THEN pd.value
	ELSE 0
	END
) AS st_november, 
sum(
	CASE
	WHEN ((pd.header)::text = 'srsp_november'::text) THEN pd.value
	ELSE 0
	END
) AS srsp_november, 
sum(
	CASE
	WHEN ((pd.header)::text = 'st_desember'::text) THEN pd.value
	ELSE 0
	END
) AS st_desember, 
sum(
	CASE
	WHEN ((pd.header)::text = 'srsp_desember'::text) THEN pd.value
	ELSE 0
	END
) AS srsp_desember, 
sum(
	CASE
	WHEN ((pd.header)::text = 'target_januari'::text) THEN pd.value
	ELSE 0
	END
) AS target_januari, 
sum(
	CASE
	WHEN ((pd.header)::text = 'realisasi_januari'::text) THEN pd.value
	ELSE 0
	END
) AS realisasi_januari, 
sum(
	CASE
	WHEN ((pd.header)::text = 'target_februari'::text) THEN pd.value
	ELSE 0
	END
) AS target_februari, 
sum(
	CASE
	WHEN ((pd.header)::text = 'realisasi_februari'::text) THEN pd.value
	ELSE 0
	END
) AS realisasi_februari, 
sum(
	CASE
	WHEN ((pd.header)::text = 'target_maret'::text) THEN pd.value
	ELSE 0
	END
) AS target_maret, 
sum(
	CASE
	WHEN ((pd.header)::text = 'realisasi_maret'::text) THEN pd.value
	ELSE 0
	END
) AS realisasi_maret, 
sum(
	CASE
	WHEN ((pd.header)::text = 'target_april'::text) THEN pd.value
	ELSE 0
	END
) AS target_april, 
sum(
	CASE
	WHEN ((pd.header)::text = 'realisasi_april'::text) THEN pd.value
	ELSE 0
	END
) AS realisasi_april, 
sum(
	CASE
	WHEN ((pd.header)::text = 'target_mei'::text) THEN pd.value
	ELSE 0
	END
) AS target_mei, 
sum(
	CASE
	WHEN ((pd.header)::text = 'realisasi_mei'::text) THEN pd.value
	ELSE 0
	END
) AS realisasi_mei, 
sum(
	CASE
	WHEN ((pd.header)::text = 'target_juni'::text) THEN pd.value
	ELSE 0
	END
) AS target_juni, 
sum(
	CASE
	WHEN ((pd.header)::text = 'realisasi_juni'::text) THEN pd.value
	ELSE 0
	END
) AS realisasi_juni, 
sum(
	CASE
	WHEN ((pd.header)::text = 'target_juli'::text) THEN pd.value
	ELSE 0
	END
) AS target_juli, 
sum(
	CASE
	WHEN ((pd.header)::text = 'realisasi_juli'::text) THEN pd.value
	ELSE 0
	END
) AS realisasi_juli, 
sum(
	CASE
	WHEN ((pd.header)::text = 'target_agustus'::text) THEN pd.value
	ELSE 0
	END
) AS target_agustus, 
sum(
	CASE
	WHEN ((pd.header)::text = 'realisasi_agustus'::text) THEN pd.value
	ELSE 0
	END
) AS realisasi_agustus, 
sum(
	CASE
	WHEN ((pd.header)::text = 'target_september'::text) THEN pd.value
	ELSE 0
	END
) AS target_september, 
sum(
	CASE
	WHEN ((pd.header)::text = 'realisasi_september'::text) THEN pd.value
	ELSE 0
	END
) AS realisasi_september, 
sum(
	CASE
	WHEN ((pd.header)::text = 'target_oktober'::text) THEN pd.value
	ELSE 0
	END
) AS target_oktober, 
sum(
	CASE
	WHEN ((pd.header)::text = 'realisasi_oktober'::text) THEN pd.value
	ELSE 0
	END
) AS realisasi_oktober, 
sum(
	CASE
	WHEN ((pd.header)::text = 'target_november'::text) THEN pd.value
	ELSE 0
	END
) AS target_november, 
sum(
	CASE
	WHEN ((pd.header)::text = 'realisasi_november'::text) THEN pd.value
	ELSE 0
	END
) AS realisasi_november, 
sum(
	CASE
	WHEN ((pd.header)::text = 'target_desember'::text) THEN pd.value
	ELSE 0
	END
) AS target_desember, 
sum(
	CASE
	WHEN ((pd.header)::text = 'realisasi_desember'::text) THEN pd.value
	ELSE 0
	END
) AS realisasi_desember
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;
