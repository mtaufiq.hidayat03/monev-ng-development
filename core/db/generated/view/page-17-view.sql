
DROP VIEW IF EXISTS "public"."page_17";

CREATE OR REPLACE VIEW "public"."page_17" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'permasalahan_kontrak'::text) THEN pd.value
	ELSE 0
	END
) AS permasalahan_kontrak, 
sum(
	CASE
	WHEN ((pd.header)::text = 'nilai_pengadaan'::text) THEN pd.value
	ELSE 0
	END
) AS nilai_pengadaan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pemutusan_kontrak'::text) THEN pd.value
	ELSE 0
	END
) AS pemutusan_kontrak, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pemberian_kesempatan'::text) THEN pd.value
	ELSE 0
	END
) AS pemberian_kesempatan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'perubahan_kontrak'::text) THEN pd.value
	ELSE 0
	END
) AS perubahan_kontrak, 
sum(
	CASE
	WHEN ((pd.header)::text = 'uang_muka'::text) THEN pd.value
	ELSE 0
	END
) AS uang_muka, 
sum(
	CASE
	WHEN ((pd.header)::text = 'daftar_hitam'::text) THEN pd.value
	ELSE 0
	END
) AS daftar_hitam, 
sum(
	CASE
	WHEN ((pd.header)::text = 'keadaan_kahar'::text) THEN pd.value
	ELSE 0
	END
) AS keadaan_kahar, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyesuaian_harga'::text) THEN pd.value
	ELSE 0
	END
) AS penyesuaian_harga, 
sum(
	CASE
	WHEN ((pd.header)::text = 'permasalahan_lainnya'::text) THEN pd.value
	ELSE 0
	END
) AS permasalahan_lainnya, 
sum(
	CASE
	WHEN ((pd.header)::text = 'putusan_sidang_pbjp'::text) THEN pd.value
	ELSE 0
	END
) AS putusan_sidang_pbjp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'kasus_pbjp_barang'::text) THEN pd.value
	ELSE 0
	END
) AS kasus_pbjp_barang, 
sum(
	CASE
	WHEN ((pd.header)::text = 'kasus_pbjp_konstruksi'::text) THEN pd.value
	ELSE 0
	END
) AS kasus_pbjp_konstruksi, 
sum(
	CASE
	WHEN ((pd.header)::text = 'kasus_pbjp_konsultasi'::text) THEN pd.value
	ELSE 0
	END
) AS kasus_pbjp_konsultasi, 
sum(
	CASE
	WHEN ((pd.header)::text = 'kasus_jasa_lainnya'::text) THEN pd.value
	ELSE 0
	END
) AS kasus_jasa_lainnya, 
sum(
	CASE
	WHEN ((pd.header)::text = 'perkara_pidana'::text) THEN pd.value
	ELSE 0
	END
) AS perkara_pidana, 
sum(
	CASE
	WHEN ((pd.header)::text = 'perkara_persaingan_usaha'::text) THEN pd.value
	ELSE 0
	END
) AS perkara_persaingan_usaha, 
sum(
	CASE
	WHEN ((pd.header)::text = 'perkara_perdata'::text) THEN pd.value
	ELSE 0
	END
) AS perkara_perdata, 
sum(
	CASE
	WHEN ((pd.header)::text = 'perkara_tun'::text) THEN pd.value
	ELSE 0
	END
) AS perkara_tun
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;
