
DROP VIEW IF EXISTS "public"."page_36";

CREATE OR REPLACE VIEW "public"."page_36" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_pengadaan_langsung_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS pts_pengadaan_langsung_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_pengadaan_langsung_paket'::text) THEN pd.value
	ELSE 0
	END
) AS pts_pengadaan_langsung_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_epurchasing_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS pts_epurchasing_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_epurchasing_paket'::text) THEN pd.value
	ELSE 0
	END
) AS pts_epurchasing_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_penunjukan_langsung_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS pts_penunjukan_langsung_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_penunjukan_langsung_paket'::text) THEN pd.value
	ELSE 0
	END
) AS pts_penunjukan_langsung_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_tender_lelang_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS pts_tender_lelang_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_tender_lelang_paket'::text) THEN pd.value
	ELSE 0
	END
) AS pts_tender_lelang_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_pemilihan_langsung_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS pts_pemilihan_langsung_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_pemilihan_langsung_paket'::text) THEN pd.value
	ELSE 0
	END
) AS pts_pemilihan_langsung_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_seleksi_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS pts_seleksi_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_seleksi_paket'::text) THEN pd.value
	ELSE 0
	END
) AS pts_seleksi_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_tender_lelang_cepat_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS pts_tender_lelang_cepat_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_tender_lelang_cepat_paket'::text) THEN pd.value
	ELSE 0
	END
) AS pts_tender_lelang_cepat_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_kontes_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS pts_kontes_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_kontes_paket'::text) THEN pd.value
	ELSE 0
	END
) AS pts_kontes_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_sayembara_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS pts_sayembara_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_sayembara_paket'::text) THEN pd.value
	ELSE 0
	END
) AS pts_sayembara_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_lainnya_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS pts_lainnya_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_lainnya_paket'::text) THEN pd.value
	ELSE 0
	END
) AS pts_lainnya_paket
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;
