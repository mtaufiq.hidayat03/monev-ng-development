
DROP VIEW IF EXISTS "public"."page_15";

CREATE OR REPLACE VIEW "public"."page_15" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'total_peserta_diklat'::text) THEN pd.value
	ELSE 0
	END
) AS total_peserta_diklat, 
sum(
	CASE
	WHEN ((pd.header)::text = 'diklat_pembentukan'::text) THEN pd.value
	ELSE 0
	END
) AS diklat_pembentukan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'diklat_jabfung_pbj_pertama'::text) THEN pd.value
	ELSE 0
	END
) AS diklat_jabfung_pbj_pertama, 
sum(
	CASE
	WHEN ((pd.header)::text = 'diklat_jabfung_muda'::text) THEN pd.value
	ELSE 0
	END
) AS diklat_jabfung_muda, 
sum(
	CASE
	WHEN ((pd.header)::text = 'diklat_jabfung_pbj_muda'::text) THEN pd.value
	ELSE 0
	END
) AS diklat_jabfung_pbj_muda, 
sum(
	CASE
	WHEN ((pd.header)::text = 'lembaga_pendidikan_pelatihan_terakreditasi'::text) THEN pd.value
	ELSE 0
	END
) AS lembaga_pendidikan_pelatihan_terakreditasi, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyelenggara_ujian_sert_dasar'::text) THEN pd.value
	ELSE 0
	END
) AS penyelenggara_ujian_sert_dasar, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyelenggara_ujian_sert_komp'::text) THEN pd.value
	ELSE 0
	END
) AS penyelenggara_ujian_sert_komp
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;
