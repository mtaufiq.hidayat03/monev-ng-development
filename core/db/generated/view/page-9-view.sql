
DROP VIEW IF EXISTS "public"."page_9";

CREATE OR REPLACE VIEW "public"."page_9" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'harga_pagu_pengumuman'::text) THEN pd.value
	ELSE 0
	END
) AS harga_pagu_pengumuman, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_pagu_pengumuman'::text) THEN pd.value
	ELSE 0
	END
) AS paket_pagu_pengumuman, 
sum(
	CASE
	WHEN ((pd.header)::text = 'harga_hps_pp'::text) THEN pd.value
	ELSE 0
	END
) AS harga_hps_pp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_hps_pp'::text) THEN pd.value
	ELSE 0
	END
) AS paket_hps_pp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'harga_hasil_tender'::text) THEN pd.value
	ELSE 0
	END
) AS harga_hasil_tender, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_hasil_tender'::text) THEN pd.value
	ELSE 0
	END
) AS paket_hasil_tender, 
sum(
	CASE
	WHEN ((pd.header)::text = 'harga_pengadaan_barang'::text) THEN pd.value
	ELSE 0
	END
) AS harga_pengadaan_barang, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_pengadaan_barang'::text) THEN pd.value
	ELSE 0
	END
) AS paket_pengadaan_barang, 
sum(
	CASE
	WHEN ((pd.header)::text = 'harga_barang_jp'::text) THEN pd.value
	ELSE 0
	END
) AS harga_barang_jp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_barang_jp'::text) THEN pd.value
	ELSE 0
	END
) AS paket_barang_jp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'harga_jkbu_jp'::text) THEN pd.value
	ELSE 0
	END
) AS harga_jkbu_jp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_jkbu_jp'::text) THEN pd.value
	ELSE 0
	END
) AS paket_jkbu_jp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'harga_jkp_jp'::text) THEN pd.value
	ELSE 0
	END
) AS harga_jkp_jp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_jkp_jp'::text) THEN pd.value
	ELSE 0
	END
) AS paket_jkp_jp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'harga_jl_jp'::text) THEN pd.value
	ELSE 0
	END
) AS harga_jl_jp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_jl_jp'::text) THEN pd.value
	ELSE 0
	END
) AS paket_jl_jp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_1'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_2'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_3'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_4'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_5'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_6'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_7'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_8'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_9'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_10'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_10, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_11'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_11, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_12'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_12, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_13'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_13, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_14'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_14, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_15'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_15, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_16'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_16, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_17'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_17, 
sum(
	CASE
	WHEN ((pd.header)::text = 'mpt_hps'::text) THEN pd.value
	ELSE 0
	END
) AS mpt_hps, 
sum(
	CASE
	WHEN ((pd.header)::text = 'mpt_paket'::text) THEN pd.value
	ELSE 0
	END
) AS mpt_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'mptc_hps'::text) THEN pd.value
	ELSE 0
	END
) AS mptc_hps, 
sum(
	CASE
	WHEN ((pd.header)::text = 'mptc_paket'::text) THEN pd.value
	ELSE 0
	END
) AS mptc_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'mps_hps'::text) THEN pd.value
	ELSE 0
	END
) AS mps_hps, 
sum(
	CASE
	WHEN ((pd.header)::text = 'mps_paket'::text) THEN pd.value
	ELSE 0
	END
) AS mps_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'mpc_hps'::text) THEN pd.value
	ELSE 0
	END
) AS mpc_hps, 
sum(
	CASE
	WHEN ((pd.header)::text = 'mpc_paket'::text) THEN pd.value
	ELSE 0
	END
) AS mpc_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'selisih_pagu_pengumuman'::text) THEN pd.value
	ELSE 0
	END
) AS selisih_pagu_pengumuman
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;
