
DROP VIEW IF EXISTS "public"."page_19";

CREATE OR REPLACE VIEW "public"."page_19" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'ptds_item_1'::text) THEN pd.value
	ELSE 0
	END
) AS ptds_item_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptds_item_2'::text) THEN pd.value
	ELSE 0
	END
) AS ptds_item_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptds_item_3'::text) THEN pd.value
	ELSE 0
	END
) AS ptds_item_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptds_item_4'::text) THEN pd.value
	ELSE 0
	END
) AS ptds_item_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptds_item_5'::text) THEN pd.value
	ELSE 0
	END
) AS ptds_item_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_teragregasi_lpse'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_teragregasi_lpse, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_dlm_sikap'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_dlm_sikap, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_lolos_prakualifikasi'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_lolos_prakualifikasi, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_aktif_ikut_lelang'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_aktif_ikut_lelang, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_menang'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_menang
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;
