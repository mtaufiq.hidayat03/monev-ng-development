
DROP VIEW IF EXISTS "public"."page_14";

CREATE OR REPLACE VIEW "public"."page_14" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'penjabat_fungsional'::text) THEN pd.value
	ELSE 0
	END
) AS penjabat_fungsional, 
sum(
	CASE
	WHEN ((pd.header)::text = 'mpjf_inpassing'::text) THEN pd.value
	ELSE 0
	END
) AS mpjf_inpassing, 
sum(
	CASE
	WHEN ((pd.header)::text = 'mpjf_perpindahan'::text) THEN pd.value
	ELSE 0
	END
) AS mpjf_perpindahan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'mpjf_pengangkatan_pertama'::text) THEN pd.value
	ELSE 0
	END
) AS mpjf_pengangkatan_pertama, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pf_wanita'::text) THEN pd.value
	ELSE 0
	END
) AS pf_wanita, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pf_pria'::text) THEN pd.value
	ELSE 0
	END
) AS pf_pria, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pf_pertama'::text) THEN pd.value
	ELSE 0
	END
) AS pf_pertama, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pf_fungsional_muda'::text) THEN pd.value
	ELSE 0
	END
) AS pf_fungsional_muda, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pf_fungsional_madya'::text) THEN pd.value
	ELSE 0
	END
) AS pf_fungsional_madya, 
sum(
	CASE
	WHEN ((pd.header)::text = 'sjf_aktif'::text) THEN pd.value
	ELSE 0
	END
) AS sjf_aktif, 
sum(
	CASE
	WHEN ((pd.header)::text = 'sjf_pembebasan_sementara'::text) THEN pd.value
	ELSE 0
	END
) AS sjf_pembebasan_sementara, 
sum(
	CASE
	WHEN ((pd.header)::text = 'sjf_pensiun'::text) THEN pd.value
	ELSE 0
	END
) AS sjf_pensiun, 
sum(
	CASE
	WHEN ((pd.header)::text = 'sjf_wafat'::text) THEN pd.value
	ELSE 0
	END
) AS sjf_wafat, 
sum(
	CASE
	WHEN ((pd.header)::text = 'td_dasar_pbjp'::text) THEN pd.value
	ELSE 0
	END
) AS td_dasar_pbjp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'td_fasilitator_pbjp'::text) THEN pd.value
	ELSE 0
	END
) AS td_fasilitator_pbjp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'kpf_muda'::text) THEN pd.value
	ELSE 0
	END
) AS kpf_muda, 
sum(
	CASE
	WHEN ((pd.header)::text = 'kpf_madya'::text) THEN pd.value
	ELSE 0
	END
) AS kpf_madya, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ko_ppk_pbj'::text) THEN pd.value
	ELSE 0
	END
) AS ko_ppk_pbj, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ko_pokja_pemilihan'::text) THEN pd.value
	ELSE 0
	END
) AS ko_pokja_pemilihan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ko_pp'::text) THEN pd.value
	ELSE 0
	END
) AS ko_pp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'k_assesor_kompetensi'::text) THEN pd.value
	ELSE 0
	END
) AS k_assesor_kompetensi, 
sum(
	CASE
	WHEN ((pd.header)::text = 'kfk_pbj'::text) THEN pd.value
	ELSE 0
	END
) AS kfk_pbj, 
sum(
	CASE
	WHEN ((pd.header)::text = 'keterangan_ahli'::text) THEN pd.value
	ELSE 0
	END
) AS keterangan_ahli, 
sum(
	CASE
	WHEN ((pd.header)::text = 'mediator'::text) THEN pd.value
	ELSE 0
	END
) AS mediator, 
sum(
	CASE
	WHEN ((pd.header)::text = 'arbiter'::text) THEN pd.value
	ELSE 0
	END
) AS arbiter
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;
