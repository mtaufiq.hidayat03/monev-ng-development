DROP SEQUENCE IF EXISTS "public"."blacklist_id_seq";
CREATE SEQUENCE "public"."blacklist_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

create table public.blacklist (
  "id" bigint NOT NULL DEFAULT nextval('blacklist_id_seq'::regclass),
  "json_data" text,
    "rekanan_id" bigint ,
    "sk" character varying(255),
    "npwp" character varying(255),
    "alamat" character varying(255),
    "tanggal_tayang" timestamp(6),
    "tanggal_turun_tayang" timestamp(6),
    "tanggal_berakhir" timestamp(6),
    "nama_propinsi" character varying(255),
    "id_propinsi" character varying(50),
    "nama_kabupaten" character varying(255),
    "id_kabupaten" character varying(50),
    "nama_penyedia" character varying(255),
    "kode_kldi" character varying(50),
    "nama_kldi" character varying(255),
    "kode_satker" character varying(50),
    "nama_satker" character varying(255),
    "status" character varying(255),
    "deskripsi" text,
    "jenis" character varying(255),
    "jenis_pengadaan" character varying(255),
    "id_pengadaan" character varying(255),
    "nama_pengadaan" character varying(255)
)
;

ALTER TABLE "public"."blacklist" ADD CONSTRAINT "blacklist._pkey" PRIMARY KEY ("id");

DROP SEQUENCE IF EXISTS "public"."blacklist_ikut_pengadaan_id_seq";
CREATE SEQUENCE "public"."blacklist_ikut_pengadaan_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

create table public.blacklist_ikut_pengadaan (
  "id" BIGINT NOT NULL DEFAULT nextval('blacklist_ikut_pengadaan_id_seq'::regclass),
  "id_pengadaan" bigint,
  "rkn_id" bigint,
  "nama_pengadaan" character varying(255)
)
;

ALTER TABLE "public"."blacklist_ikut_pengadaan" ADD CONSTRAINT "blacklist_ikut_lelang._pkey" PRIMARY KEY ("id");

CREATE TABLE "public"."satker" (
"id" int4,
"idsatker" varchar(255) COLLATE "default",
"idkldi" varchar(255) COLLATE "default",
"nama" varchar(255) COLLATE "default"
)
WITH (OIDS=FALSE)

;
ALTER TABLE "public"."satker" ADD CONSTRAINT "satker._pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."configuration_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."configuration" (
  "id" int4 NOT NULL DEFAULT nextval('configuration_id_seq'::regclass),
  "key" varchar(255) COLLATE "default",
  "config_value" varchar(255) COLLATE "default",
  "created_at" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
  "created_by" bigint,
  "updated_at" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
  "updated_by" bigint,
  "deleted_at" timestamp(6) DEFAULT NULL,
  "deleted_by" bigint
)
WITH (OIDS=FALSE)
;
ALTER TABLE "public"."configuration" ADD CONSTRAINT "configuration._pkey" PRIMARY KEY ("id");
INSERT INTO "public"."configuration" ("key", "config_value", "created_at", "created_by", "updated_at", "updated_by", "deleted_at", "deleted_by") VALUES ('tender_classification', 'false', '2019-12-04 18:10:36.035075', NULL, '2019-12-05 08:55:49.948', '1', NULL, NULL);





-- ----------------------------
-- Sequence structure for akses_content_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."akses_content_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for akses_content
-- ----------------------------
CREATE TABLE "public"."akses_content" (
  "id" bigint NOT NULL DEFAULT nextval('akses_content_id_seq'::regclass),
  "user_id" bigint,
  "content_id" bigint,
  "profilkl_id" bigint
)
;

ALTER TABLE "public"."akses_content" ADD CONSTRAINT "akses_content_pkey" PRIMARY KEY ("id");


-- ----------------------------
-- Sequence structure for contents_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."contents_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for contents
-- ----------------------------
CREATE TABLE "public"."contents" (
  "id" bigint NOT NULL DEFAULT nextval('contents_id_seq'::regclass),
  "title" character varying(255),
  "vision" character varying(255),
  "mission" character varying(255),
  "content" text,
  "regulation" json,
  "mou" json,
  "featured_image" character varying(255),
  "type" character varying(255),
  "kldi_id" character varying(255),
  "publish" integer,
  "created_by" integer,
  "created_at" timestamp(6),
  "updated_by" integer,
  "updated_at" timestamp(6),
  "deleted_by" integer,
  "deleted_at" timestamp(6),
  "permission" character(20),
  "free_content" text,
  "alamat_url" text
)
;

-- ----------------------------
-- Primary Key structure for table contents
-- ----------------------------
ALTER TABLE "public"."contents" ADD CONSTRAINT "contents_pkey" PRIMARY KEY ("id");


CREATE SEQUENCE "public"."tableau_library_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."tableau_library" (
"id" int8 DEFAULT nextval('tableau_library_id_seq'::regclass) NOT NULL,
"title" varchar(255) COLLATE "default",
"description" text COLLATE "default",
"available_for_klpd" bool,
"url" text COLLATE "default",
"created_by" int4,
"created_at" timestamp(6),
"updated_by" int4,
"updated_at" timestamp(6),
"deleted_by" int4,
"deleted_at" timestamp(6),
"kldi_id" varchar(30) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of tableau_library
-- ----------------------------
INSERT INTO "public"."tableau_library" VALUES ('5', 'Belanja Pengadaan Daerah Kabupaten', 'belanja pengadaan kabupaten', 't', 'https://monevtab.lkpp.go.id/views/MonevPusat/BelanjaPengadaanKabKota?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-06 14:36:43.829', '1', '2019-12-06 13:15:12.522', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('7', 'Belanja Pengadaan (Nasional)', 'Belanja Pengadaan nasional', 't', 'https://monevtab.lkpp.go.id/views/MonevPusat/BelanjaPengadaanNasional?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-07 09:22:32.339', '1', '2019-11-14 17:29:06.091', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('8', 'klasifikasi penyedia nasional', 'klasifikasi penyedia nasional', 'f', 'https://monevtab.lkpp.go.id/#/views/RencanaBelanja/2_2KlasifikasiPenyedia?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-07 14:12:57.283', '1', '2019-11-07 14:19:58.203', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('9', 'Jenis Belanja Pengadaan Pusat', 'jenis belanja pengadaan pusat', 't', 'https://monevtab.lkpp.go.id/views/MonevPusat/JenisBelanjaPengadaanPusat?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-07 14:19:31.35', '1', '2019-11-07 14:22:29.904', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('10', 'jenis pengadaan - nasional', '[perencanaan] Belanja - Rencana Pemilihan/ Pelaksanaan Jenis Pengadaan', 'f', 'https://monevtab.lkpp.go.id/#/views/RencanaBelanja/2_3JenisPengadaan?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-07 16:24:03.89', '1', '2019-11-13 13:56:28.718', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('11', 'Hasil Pemilihan Nasional', 'hasil pemilihan - nasional', 't', 'https://monevtab.lkpp.go.id/#/views/HasilPemilihan/DashboardPaket-NilaiPusatDaerah?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-13 15:28:18.268', '1', '2019-11-14 16:03:19.933', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('12', 'Belanja - Rencana Pemilihan/ Pelaksanaan Klasifikasi Penyedia', 'Belanja - Rencana Pemilihan/ Pelaksanaan Klasifikasi Penyedia', 't', 'https://monevtab.lkpp.go.id/#/views/RencanaBelanja/2_2KlasifikasiPenyedia?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-13 15:48:40.45', '1', '2019-11-14 16:48:10.952', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('13', 'Belanja - Rencana Pemilihan/ Pelaksanaan Jenis Pengadaan', 'Belanja - Rencana Pemilihan/ Pelaksanaan Jenis Pengadaan', 't', 'https://monevtab.lkpp.go.id/#/views/RencanaBelanja/2_7JenisPengKontrak?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-13 15:57:52.561', '1', '2019-11-14 16:48:37.474', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('14', 'Belanja - Rencana Pemilihan/ Pelaksanaan Grafik Spasial', 'Belanja - Rencana Pemilihan/ Pelaksanaan Grafik Spasial', 't', 'https://monevtab.lkpp.go.id/#/views/Perencanaan-GrafikSpasial/2_4Belanja-Nasional?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-13 16:11:16.249', '1', '2019-11-14 16:47:30.461', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('15', 'Belanja - Rencana Kontrak Total', 'Belanja - Rencana Kontrak Total', 't', 'https://monevtab.lkpp.go.id/#/views/RencanaBelanja/2_5RencanaKontrak?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-13 16:32:36.802', '1', '2019-11-14 16:49:10.829', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('16', 'Belanja - Rencana Kontrak Kelola Penyedia', 'Belanja - Rencana Kontrak Kelola Penyedia', 't', 'https://monevtab.lkpp.go.id/views/RencanaBelanja/2_6KlasPenydKontrak?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-13 16:41:45.196', '1', '2019-11-14 17:02:12.392', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('17', 'Belanja - Rencana Kontrak Jenis Pengadaan', 'Belanja - Rencana Kontrak Jenis Pengadaan', 't', 'https://monevtab.lkpp.go.id/views/RencanaBelanja/2_3JenisPengadaan?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-13 17:08:25.063', '1', '2019-11-14 16:56:10.43', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('18', 'Belanja - Rencana Kontrak Grafik Spasial', 'Belanja - Rencana Kontrak Grafik Spasial', 't', 'https://monevtab.lkpp.go.id/views/Perencanaan-GrafikSpasial/2_4Belanja-Nasional?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-13 17:17:26.157', '1', '2019-11-14 16:44:14.849', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('19', 'Kegiatan - Rencana Pemilihan/Pelaksanaan Total', 'Kegiatan - Rencana Pemilihan/Pelaksanaan Total', 'f', 'https://monevtab.lkpp.go.id/views/RencanaKegiatan/2_9RencanaKegiatanPusat?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-13 17:42:54.966', '1', '2019-11-14 16:37:07.526', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('20', 'Kegiatan - Rencana Pemilihan/Pelaksanaan Klasifikasi Kegiatan', 'Kegiatan - Rencana Pemilihan/Pelaksanaan Klasifikasi Kegiatan', 'f', 'https://monevtab.lkpp.go.id/#/views/RencanaKegiatan/2_14KlasPenyKegKontrakPusat?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-13 17:52:56.803', null, null, null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('21', 'Hasil Pemilihan Nasional (Sample)', 'Hasil Pemilihan Nasional', 't', 'https://monevtab.lkpp.go.id/views/HasilPemilihan/DashboardPaket-NilaiPusatDaerah?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-14 12:18:42.403', '1', '2019-11-14 12:52:29.067', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('22', 'Kegiatan - Rencana Pemilihan/Pelaksanaan Grafik Spasial', 'Kegiatan - Rencana Pemilihan/Pelaksanaan Grafik Spasial', 't', 'https://monevtab.lkpp.go.id/views/Perencanaan-GrafikSpasial/2_12Keg-Nas?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-14 12:38:45.359', '1', '2019-11-14 16:43:08.74', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('25', 'Kegiatan Rencana Kontrak Grafik Spasial', 'Kegiatan Rencana Kontrak Grafik Spasial', 'f', 'https://monevtab.lkpp.go.id/#/views/Perencanaan-GrafikSpasial/2_4Belanja-Nasional?:iid=3', '1', '2019-11-14 13:05:39.806', null, null, null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('26', 'HPS Pemilihan Penyedia', 'HPS Pemilihan Penyedia', 'f', 'https://monevtab.lkpp.go.id/#/views/HasilPemilihan/DashboardPaket-NilaiPusatDaerah?:iid=1', '1', '2019-11-14 13:09:09.414', null, null, null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('27', 'Waktu Pengumuman Mulai Lelang (berdasarkan Paket)', 'Waktu Pengumuman Mulai Lelang (berdasarkan Paket)', 'f', 'https://monevtab.lkpp.go.id/#/views/1DistribusiLelang/1_1DistribusiJumlahLelang?:iid=2', '1', '2019-11-14 13:11:54.584', null, null, null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('28', 'Belanja - Rencana Pengadaan', 'Belanja - Rencana Pengadaan', 't', 'https://monevtab.lkpp.go.id/views/RencanaBelanja/2_1RencanaPengadaanPusatNasional?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', null, '1', '2019-11-14 13:39:18.777', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('29', 'Hasil Lelang', 'Hasil Lelang', 'f', 'https://monevtab.lkpp.go.id/#/views/1DistribusiLelang/1_2DistribusiNilaiLelang?:iid=4', '1', '2019-11-14 13:40:08.923', null, null, null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('31', 'Kegiatan - Rencana Kontrak', 'Kegiatan - Rencana Kontrak', 't', 'https://monevtab.lkpp.go.id/views/RencanaKegiatan/2_13RencanaPengKegKontrakPusat?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', null, '1', '2019-11-14 17:01:51.884', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('32', 'Kegiatan - Rencana Kontrak Jenis Pengadaaan', 'Kegiatan - Rencana Kontrak Jenis Pengadaaan', 't', 'https://monevtab.lkpp.go.id/views/RencanaKegiatan/2_15JenisPengadaanKegiatanKontrakPusat?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', null, null, '1', '2019-11-14 16:43:11.588', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('33', 'Kegiatan - Rencana Pemilihan Jenis Pengadaan', 'Kegiatan - Rencana Pemilihan Jenis Pengadaan', 't', 'https://monevtab.lkpp.go.id/views/RencanaKegiatan/2_11JenisPengadaanKegiatanPusat?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', null, null, '1', '2019-11-14 16:46:57.551', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('34', 'Belanja Pengadaan KL', 'Belanja Pengadaan KL', 'f', 'https://monevtab.lkpp.go.id/views/MonevPusat/BelanjaPengadaanKL?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', null, null, '1', '2019-11-14 16:04:46.531', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('35', 'Belanja Pengadaan Terbesar Pusat', 'Belanja Pengadaan Terbesar Pusat', 't', 'https://monevtab.lkpp.go.id/views/BelanjaTerbesar/10BelanjaPengadaanTerbesarPusat?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', null, null, '1', '2019-11-14 17:01:25.08', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('36', 'Kegiatan - Klafisikasi Penyedia Kegiatan', 'Kegiatan - Klafisikasi Penyedia Kegiatan', 'f', 'https://monevtab.lkpp.go.id/views/RencanaKegiatan/2_10KlasifikasiPenyediaKegiatanPusat?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', null, null, '1', '2019-11-14 16:41:10.252', null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('39', 'Trend', 'Trend', 'f', 'https://monevtab.lkpp.go.id/views/RencanaKegiatan/2_9RencanaKegiatanKL?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-24 23:42:46.142', null, null, null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('40', 'Hasil Pemilihan Paket Nilai Pusat Daerah', 'Hasil Pemilihan Paket Nilai Pusat Daerah', 't', 'https://monevtab.lkpp.go.id/views/HasilPemilihan/DashboardPaket-NilaiPusatDaerahasli?iframeSizedToWindow=true&:embed=y&:showAppBanner=false&:display_count=no&:showVizHome=no', '1', '2019-11-26 05:24:22.423', null, null, null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('41', 'Belanja Pengadaan Daerah Kabupaten', 'belanja pengadaan kabupaten', 't', 'https://s.id/UTScitra', '1', '2019-11-30 16:37:04.507', null, null, null, null, null);
INSERT INTO "public"."tableau_library" VALUES ('42', 'belanja negara', 'belanja cuyy', 't', 'https://monevtab.lkpp.go.id/views/MonevPusat/BelanjaPengadaanNasional?iframeSizedToWindow=true&:embe...', '1', '2019-12-03 17:36:06.532', null, null, null, null, null);

-- ----------------------------
-- Alter Sequences Owned By
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table tableau_library
-- ----------------------------
ALTER TABLE "public"."tableau_library" ADD PRIMARY KEY ("id");

SELECT setval('"public"."tableau_library_id_seq"', 50, true);

CREATE SEQUENCE "public"."tableau_library_parameters_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."tableau_library_parameters" (
"id" int8 DEFAULT nextval('tableau_library_parameters_id_seq'::regclass) NOT NULL,
"label" varchar(255) COLLATE "default",
"value" varchar COLLATE "default",
"library_id" int8,
"created_by" int4,
"created_at" timestamp(6),
"updated_by" int4,
"updated_at" timestamp(6),
"deleted_by" int4,
"deleted_at" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of tableau_library_parameters
-- ----------------------------
INSERT INTO "public"."tableau_library_parameters" VALUES ('8', 'YEAR(published_date)', '2018,2017,2016', '6', '1', '2019-11-07 01:54:08.014', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('13', 'tahun anggaran', '2017,2018,2019', '8', '1', '2019-11-07 14:19:58.206', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('14', 'tahun_anggaran', '2017,2018', '9', '1', '2019-11-07 14:22:29.905', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('19', 'Choose Years (copy)', '2017,2018,2016,2013', '10', '1', '2019-11-13 13:56:28.72', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('32', 'Choose Years (copy)', '2018', '20', '1', '2019-11-13 17:52:56.898', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('40', 'Choose Years (copy)', '2017', '23', '1', '2019-11-14 12:47:04.689', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('41', 'Select a View', 'Paket,Nilai', '21', '1', '2019-11-14 12:52:29.069', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('42', 'Choose Years (copy)', '2018', '24', '1', '2019-11-14 12:55:18.749', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('43', 'tahun anggaran', '2017', '25', '1', '2019-11-14 13:05:39.809', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('44', 'Choose Years (copy)', '2013,2014,2015,2016,2017,2018', '28', '1', '2019-11-14 13:39:18.782', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('45', 'Lelang Option', 'perbandingan perbulan', '29', '1', '2019-11-14 13:40:08.928', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('47', 'Select a View', 'nilai,Paket', '11', '1', '2019-11-14 16:03:19.935', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('48', 'Choose Years (copy)', '2013,2014,2015,2017,2018', '34', '1', '2019-11-14 16:04:46.532', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('54', 'Choose Years (copy)', '2013,2014,2015,2016,2017,2018', '19', '1', '2019-11-14 16:37:07.527', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('56', 'Choose Years (copy)', '2013,2014,2015,2016,2017,2018', '36', '1', '2019-11-14 16:41:10.253', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('57', 'Tahun Anggaran', '2013,2014,2015,2016,2017', '22', '1', '2019-11-14 16:43:08.742', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('58', 'Choose Years (copy)', '2013,2014,2015,2016,2017,2018', '32', '1', '2019-11-14 16:43:12.266', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('59', 'Tahun Anggaran', '2013,2014,2015,2016,2017', '18', '1', '2019-11-14 16:44:14.85', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('60', 'Choose Years (copy)', '2013,2014,2015,2016,2017,2018', '33', '1', '2019-11-14 16:46:57.552', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('61', 'Tahun Anggaran', '2013,2014,2015,2016,2017', '14', '1', '2019-11-14 16:47:30.464', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('62', 'Choose Years (copy)', '2013,2014,2015,2016,2017,2018', '12', '1', '2019-11-14 16:48:10.953', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('63', 'Choose Years (copy)', '2013,2015,2014,2016,2017,2018', '13', '1', '2019-11-14 16:48:37.476', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('64', 'Choose Years (copy)', '2013,2014,2015,2016,2017,2018', '15', '1', '2019-11-14 16:49:10.83', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('66', 'Choose Years (copy)', '2013,2014,2015,2016,2017,2018', '17', '1', '2019-11-14 16:56:10.431', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('67', 'Tahun Anggaran', '2012,2013,2014,2015,2017', '35', '1', '2019-11-14 17:01:25.08', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('68', 'Choose Years (copy)', '2013,2014,2015,2016,2017,2018', '31', '1', '2019-11-14 17:01:51.885', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('69', 'Choose Years (copy)', '2013,2014,2015,2016,2017,2018', '16', '1', '2019-11-14 17:02:12.393', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('70', 'Tahun Anggaran', '2012, 2013,2014,2015,2017,2018', '7', '1', '2019-11-14 17:29:06.094', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('73', 'tahun anggaran', '2013,2014,2015,2016,2017', '41', '1', '2019-11-30 16:37:04.514', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('74', 'tahun', '2018,2019', '42', '1', '2019-12-03 17:36:06.556', null, null, null, null);
INSERT INTO "public"."tableau_library_parameters" VALUES ('76', 'tahun anggaran', '2013,2014,2015,2016,2017,2018', '5', '1', '2019-12-06 13:15:12.528', null, null, null, null);

-- ----------------------------
-- Alter Sequences Owned By
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table tableau_library_parameters
-- ----------------------------
ALTER TABLE "public"."tableau_library_parameters" ADD PRIMARY KEY ("id");

SELECT setval('"public"."tableau_library_parameters_id_seq"', 80, true);

ALTER TABLE "public"."contents" ADD COLUMN "tableau_library_id" bigint;

CREATE TABLE "public"."content_tableau_parameters" (
  "content_id" bigint,
  "tableau_param_id" bigint,
  "value" text
)
;

-- ----------------------------
-- Primary Key structure for table contents
-- ----------------------------
ALTER TABLE "public"."content_tableau_parameters" ADD CONSTRAINT "content_tableau_parameters_pkey" PRIMARY KEY ("content_id", "tableau_param_id");

CREATE SEQUENCE "public"."profile_kl_data_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."profile_kl_data" (
  "id" bigint NOT NULL DEFAULT nextval('profile_kl_data_id_seq'::regclass),
  "content_id" bigint NOT NULL,
  "header" character varying(250),
  "value" NUMERIC NULL,
  "created_by" integer,
  "created_at" timestamp(6)
)
;
ALTER TABLE "public"."profile_kl_data" ADD CONSTRAINT "profile_kl_data_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."procurement_profiles_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."procurement_profiles" (
  "id" bigint NOT NULL DEFAULT nextval('procurement_profiles_id_seq'::regclass),
  "title" character varying(255),
  "kldi_id" character varying(30),
  "status" character varying(20),
  "mined_at" timestamp(6),
  "adjusted_by" integer,
  "adjusted_at" timestamp(6),
  "created_by" integer,
  "created_at" timestamp(6),
  "updated_by" integer,
  "updated_at" timestamp(6),
  "deleted_by" integer,
  "deleted_at" timestamp(6)
)
;
ALTER TABLE "public"."procurement_profiles" ADD CONSTRAINT "procurement_profiles_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."procurement_data_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."procurement_data" (
  "id" bigint NOT NULL DEFAULT nextval('procurement_data_id_seq'::regclass),
  "procurement_id" bigint NOT NULL,
  "header" character varying(250),
  "flag" INT NOT NULL,
  "value" NUMERIC NULL,
  "pagu" NUMERIC NULL,
  "value_text" TEXT NULL,
  "extra" TEXT NULL,
  "section" character varying(250),
  "created_by" integer,
  "created_at" timestamp(6)
)
;
ALTER TABLE "public"."procurement_data" ADD CONSTRAINT "procurement_data_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."procurement_data_original_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."procurement_data_original" (
  "id" bigint NOT NULL DEFAULT nextval('procurement_data_original_id_seq'::regclass),
  "header" character varying(250),
  "value" NUMERIC NULL,
  "pagu" NUMERIC NULL,
  "value_text" TEXT NULL,
  "created_by" integer,
  "created_at" timestamp(6)
)
;
ALTER TABLE "public"."procurement_data_original" ADD CONSTRAINT "procurement_data_original_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."rekap_pengadaan_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."rekap_pengadaan" (
  "id" bigint NOT NULL DEFAULT nextval('rekap_pengadaan_id_seq'::regclass),
  "tahun_anggaran" integer NOT NULL,
  "jenis_belanja" character varying(50),
  "jenis_pengadaan" character varying(50),
  "metode_pemilihan" character varying(50),
  "sumber_dana" character varying(50),
  "tipe_kldi" character varying(50),
  "kode_kldi" character varying(50),
  "nama_kldi" character varying(255),
  "kode_satker" character varying(50),
  "nama_satker" character varying(255),
  "total_anggaran" bigint NOT NULL,
  "belanja_pengadaan" bigint NOT NULL,
  "rencana_pagu" bigint NOT NULL,
  "rencana_paket" integer NOT NULL,
  "pelaksanaan_pagu" bigint NOT NULL,
  "pelaksanaan_paket" integer NOT NULL,
  "hasil_paket" integer NOT NULL,
  "hasil_pagu" bigint NOT NULL,
  "kontrak" bigint NOT NULL,
  "pembayaran" bigint NOT NULL
);
ALTER TABLE "public"."rekap_pengadaan" ADD CONSTRAINT "rekap_pengadaan_pkey" PRIMARY KEY ("id");
CREATE INDEX ON "public"."rekap_pengadaan" ((lower(sumber_dana)));
CREATE INDEX ON "public"."rekap_pengadaan" ((lower(tipe_kldi)));
CREATE INDEX ON "public"."rekap_pengadaan" ((lower(nama_kldi)));
CREATE INDEX ON "public"."rekap_pengadaan" ((lower(nama_satker)));
CREATE INDEX ON "public"."rekap_pengadaan" (kode_kldi);
CREATE INDEX ON "public"."rekap_pengadaan" (kode_satker);
CREATE INDEX ON "public"."rekap_pengadaan" (jenis_belanja);
CREATE INDEX ON "public"."rekap_pengadaan" (jenis_pengadaan);
CREATE INDEX ON "public"."rekap_pengadaan" (metode_pemilihan);

CREATE SEQUENCE "public"."rekap_pengadaan_nasional_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."rekap_pengadaan_nasional" (
  "id" bigint NOT NULL DEFAULT nextval('rekap_pengadaan_nasional_id_seq'::regclass),
  "tahun_anggaran" integer NOT NULL,
  "total_anggaran" bigint NOT NULL,
  "belanja_pengadaan" bigint NOT NULL,
  "rencana_pagu" bigint NOT NULL,
  "rencana_paket" integer NOT NULL,
  "pelaksanaan_pagu" bigint NOT NULL,
  "pelaksanaan_paket" integer NOT NULL,
  "hasil_paket" integer NOT NULL,
  "hasil_pagu" bigint NOT NULL,
  "kontrak" bigint NOT NULL,
  "pembayaran" bigint NOT NULL
);
ALTER TABLE "public"."rekap_pengadaan_nasional" ADD CONSTRAINT "rekap_pengadaan_nasional_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."rekap_pengadaan_nasional_original_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."rekap_pengadaan_nasional_original" (
  "id" bigint NOT NULL DEFAULT nextval('rekap_pengadaan_nasional_original_id_seq'::regclass),
  "tahun_anggaran" integer NOT NULL,
  "total_anggaran" bigint NOT NULL,
  "belanja_pengadaan" bigint NOT NULL,
  "rencana_pagu" bigint NOT NULL,
  "rencana_paket" integer NOT NULL,
  "pelaksanaan_pagu" bigint NOT NULL,
  "pelaksanaan_paket" integer NOT NULL,
  "hasil_paket" integer NOT NULL,
  "hasil_pagu" bigint NOT NULL,
  "kontrak" bigint NOT NULL,
  "pembayaran" bigint NOT NULL
);
ALTER TABLE "public"."rekap_pengadaan_nasional_original" ADD CONSTRAINT "rekap_pengadaan_nasional_original_pkey" PRIMARY KEY ("id");

ALTER TABLE "public"."rekap_pengadaan_nasional" ADD COLUMN tanggal_update timestamp(6);
ALTER TABLE "public"."rekap_pengadaan_nasional_original" ADD COLUMN tanggal_update timestamp(6);

ALTER TABLE "public"."rekap_pengadaan_nasional" rename COLUMN tahun_anggaran to tahun;
ALTER TABLE "public"."rekap_pengadaan_nasional_original" rename COLUMN tahun_anggaran to tahun;

CREATE SEQUENCE "public"."rekap_ukpbj_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."rekap_ukpbj" (
  "id" bigint NOT NULL DEFAULT nextval('rekap_ukpbj_id_seq'::regclass),
  "nama" character varying(255),
  "manajemen_pengadaan" character varying(10),
  "manajemen_penyedia" character varying(10),
  "manajemen_kinerja" character varying(10),
  "manajemen_resiko" character varying(10),
  "organisasi" character varying(10),
  "tugas_fungsi" character varying(10),
  "perencanaan_sdm_pengadaan" character varying(10),
  "pengembangan_sdm_pengadaan" character varying(10),
  "sistem_informasi" character varying(10),
  "kelembagaan" character varying(20)
);
ALTER TABLE "public"."rekap_ukpbj" ADD CONSTRAINT "rekap_ukpbj_pkey" PRIMARY KEY ("id");
ALTER TABLE "public"."rekap_ukpbj" ADD COLUMN kematangan integer not null;
ALTER TABLE "public"."rekap_ukpbj" ADD COLUMN kode_kldi character varying(50) not null;
ALTER TABLE "public"."rekap_ukpbj" ADD COLUMN nama_kldi character varying(255) not null;

CREATE SEQUENCE "public"."rekap_advokasi_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."rekap_advokasi" (
  "id" bigint NOT NULL DEFAULT nextval('rekap_advokasi_id_seq'::regclass),
  "jumlah_advokasi" integer not null,
  "nilai_advokasi" bigint not null,
  "jumlah_kasus_pengadaan" integer not null,
  "nilai_kasus_pengadaan" bigint not null,
  "jumlah_kasus_ahli" integer not null,
  "nilai_kasus_ahli" bigint not null,
  "jumlah_permasalahan_kontrak" integer not null,
  "nilai_permasalahan_kontrak" bigint not null,
  pengaduan_konstruksi numeric not null,
  pengaduan_barang numeric not null,
  pengaduan_konsultasi numeric not null,
  pengaduan_lainnya numeric not null,
  tanggal_update timestamp(6),
  tahun integer not null
);
ALTER TABLE "public"."rekap_advokasi" ADD CONSTRAINT "rekap_advokasi_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."rekap_lpse_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."rekap_lpse" (
  "id" bigint NOT NULL DEFAULT nextval('rekap_lpse_id_seq'::regclass),
  jumlah integer not null,
  jumlah_standar_lengkap integer not null,
  tanggal_update timestamp(6)
);
ALTER TABLE "public"."rekap_lpse" ADD CONSTRAINT "rekap_lpse_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."detail_lpse_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."detail_lpse" (
"id" int8 DEFAULT nextval('detail_lpse_id_seq'::regclass) NOT NULL,
"nama" varchar(255) COLLATE "default",
"jumlah_standar" int4 NOT NULL,
"versi_spse" varchar(20) COLLATE "default",
"longitude" double precision,
"latitude" double precision,
"alamat" text COLLATE "default",
"website" text COLLATE "default"
)
WITH (OIDS=FALSE)

;
ALTER TABLE "public"."detail_lpse" ADD CONSTRAINT "detail_lpse_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."rekap_perencanaan_bulan_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."rekap_perencanaan_bulan" (
  "id" bigint NOT NULL DEFAULT nextval('rekap_perencanaan_bulan_id_seq'::regclass),
  "bulan" timestamp(6),
  rencana_pagu bigint not null
);
ALTER TABLE "public"."rekap_perencanaan_bulan" ADD CONSTRAINT "rekap_perencanaan_bulan_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."rekap_penyedia_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."rekap_penyedia" (
  "id" bigint NOT NULL DEFAULT nextval('rekap_penyedia_id_seq'::regclass),
  rekanan_id bigint not null,
  nama character varying (255),
  npwp character varying (100),
  alamat text,
  propinsi character varying (100),
  kabupaten character varying (100),
  kontrak_menang bigint not null default 0,
  paket_menang bigint not null default 0,
  paket_menawar bigint not null default 0,
  paket_prakualifikasi bigint not null default 0,
  paket_daftar bigint not null default 0
);
ALTER TABLE "public"."rekap_penyedia" ADD CONSTRAINT "rekap_penyedia_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."paket_penyedia_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."paket_penyedia" (
  "id" bigint NOT NULL DEFAULT nextval('paket_penyedia_id_seq'::regclass),
  rekanan_id bigint not null,
  "id_paket" bigint NOT NULL,
  "id_lelang" bigint NOT NULL,
  nama_paket character varying (255),
  pagu bigint not null default 0,
  status character varying (100),
  "kualifikasi_penyedia" character varying(100)
);
ALTER TABLE "public"."paket_penyedia" ADD CONSTRAINT "paket_penyedia_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."rekap_perencanaan_metode_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."rekap_perencanaan_metode" (
  "id" bigint NOT NULL DEFAULT nextval('rekap_perencanaan_metode_id_seq'::regclass),
  "paket_konsolidasi" integer NOT NULL,
  "pagu_konsolidasi" bigint NOT NULL,
  "paket_umkm" integer NOT NULL,
  "pagu_umkm" bigint NOT NULL,
  "paket_pdn" integer NOT NULL,
  "pagu_pdn" bigint NOT NULL,
  tanggal_update timestamp(6),
  tahun integer
);
ALTER TABLE "public"."rekap_perencanaan_metode" ADD CONSTRAINT "rekap_perencanaan_metode_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."rekap_perencanaan_metode_detail_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."rekap_perencanaan_metode_detail" (
  "id" bigint NOT NULL DEFAULT nextval('rekap_perencanaan_metode_detail_id_seq'::regclass),
  "kode_kldi" character varying(50),
  "nama_kldi" character varying(255),
  "kode_satker" character varying(50),
  "nama_satker" character varying(255),
  "paket_konsolidasi" integer NOT NULL,
  "pagu_konsolidasi" bigint NOT NULL,
  "paket_umkm" integer NOT NULL,
  "pagu_umkm" bigint NOT NULL,
  "paket_pdn" integer NOT NULL,
  "pagu_pdn" bigint NOT NULL,
  tanggal_update timestamp(6),
  tahun integer
);
ALTER TABLE "public"."rekap_perencanaan_metode_detail" ADD CONSTRAINT "rekap_perencanaan_metode_detail_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."rekap_pemilihan_metode_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."rekap_pemilihan_metode" (
  "id" bigint NOT NULL DEFAULT nextval('rekap_pemilihan_metode_id_seq'::regclass),
  "paket_konsolidasi" integer NOT NULL,
  "pagu_konsolidasi" bigint NOT NULL,
  "paket_umkm" integer NOT NULL,
  "pagu_umkm" bigint NOT NULL,
  "paket_pdn" integer NOT NULL,
  "pagu_pdn" bigint NOT NULL,
  "paket_itemize" integer NOT NULL,
  "pagu_itemize" bigint NOT NULL,
  "paket_ulang" integer NOT NULL,
  "pagu_ulang" bigint NOT NULL,
  "paket_gagal" integer NOT NULL,
  "pagu_gagal" bigint NOT NULL,
  "paket_lelang_cepat" integer NOT NULL,
  "pagu_lelang_cepat" bigint NOT NULL,
  "paket_reverse_auction" integer NOT NULL,
  "pagu_reverse_auction" bigint NOT NULL,
  tanggal_update timestamp(6),
  tahun integer
);
ALTER TABLE "public"."rekap_pemilihan_metode" ADD CONSTRAINT "rekap_pemilihan_metode_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."rekap_pemilihan_metode_detail_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."rekap_pemilihan_metode_detail" (
  "id" bigint NOT NULL DEFAULT nextval('rekap_pemilihan_metode_detail_id_seq'::regclass),
  "kode_kldi" character varying(50),
  "nama_kldi" character varying(255),
  "kode_satker" character varying(50),
  "nama_satker" character varying(255),
  "paket_konsolidasi" integer NOT NULL,
  "pagu_konsolidasi" bigint NOT NULL,
  "paket_umkm" integer NOT NULL,
  "pagu_umkm" bigint NOT NULL,
  "paket_pdn" integer NOT NULL,
  "pagu_pdn" bigint NOT NULL,
  "paket_itemize" integer NOT NULL,
  "pagu_itemize" bigint NOT NULL,
  "paket_ulang" integer NOT NULL,
  "pagu_ulang" bigint NOT NULL,
  "paket_gagal" integer NOT NULL,
  "pagu_gagal" bigint NOT NULL,
  "paket_reverse_auction" integer NOT NULL,
  "pagu_reverse_auction" bigint NOT NULL,
  tanggal_update timestamp(6),
  tahun integer
);
ALTER TABLE "public"."rekap_pemilihan_metode_detail" ADD CONSTRAINT "rekap_pemilihan_metode_detail_pkey" PRIMARY KEY ("id");



CREATE SEQUENCE "public"."jenis_kontrak_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for jenis_kontrak
-- ----------------------------
CREATE TABLE "public"."jenis_kontrak" (
  "id" int8 NOT NULL DEFAULT nextval('jenis_kontrak_id_seq'::regclass),
  "kode_kldi" varchar(50) COLLATE "pg_catalog"."default",
  "nama_kldi" varchar(255) COLLATE "pg_catalog"."default",
  "kode_satker" varchar(50) COLLATE "pg_catalog"."default",
  "nama_satker" varchar(255) COLLATE "pg_catalog"."default",
  "total_anggaran" int8 NOT NULL,
  "belanja_pengadaan" int8 NOT NULL,
  "pelaksanaan_pagu" int8 NOT NULL,
  "pelaksanaan_paket" int4 NOT NULL,
  "hasil_paket" int4 NOT NULL,
  "hasil_pagu" int8 NOT NULL,
  "jenis_kontrak" int4,
  "tahun_anggaran" int4
)
;

-- ----------------------------
-- Primary Key structure for table jenis_kontrak
-- ----------------------------
ALTER TABLE "public"."jenis_kontrak" ADD CONSTRAINT "jenis_kontrak_pkey" PRIMARY KEY ("id");


CREATE SEQUENCE "public"."kualifikasi_usaha_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for kualifikasi_usaha
-- ----------------------------
CREATE TABLE "public"."kualifikasi_usaha" (
  "id" int8 NOT NULL DEFAULT nextval('kualifikasi_usaha_id_seq'::regclass),
  "kode_kldi" varchar(50) COLLATE "pg_catalog"."default",
  "nama_kldi" varchar(255) COLLATE "pg_catalog"."default",
  "kode_satker" varchar(50) COLLATE "pg_catalog"."default",
  "nama_satker" varchar(255) COLLATE "pg_catalog"."default",
  "total_anggaran" int8 NOT NULL,
  "belanja_pengadaan" int8 NOT NULL,
  "pelaksanaan_pagu" int8 NOT NULL,
  "pelaksanaan_paket" int4 NOT NULL,
  "hasil_paket" int4 NOT NULL,
  "hasil_pagu" int8 NOT NULL,
  "kualifikasi_usaha" varchar(100) COLLATE "pg_catalog"."default",
  "tahun" int4
)
;

-- ----------------------------
-- Primary Key structure for table kualifikasi_usaha
-- ----------------------------
ALTER TABLE "public"."kualifikasi_usaha" ADD CONSTRAINT "kualifikasi_usaha_pkey" PRIMARY KEY ("id");


CREATE SEQUENCE "public"."pembayaran_amel_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for pembayaran_amel
-- ----------------------------
CREATE TABLE "public"."pembayaran_amel" (
  "id" int8 NOT NULL DEFAULT nextval('pembayaran_amel_id_seq'::regclass),
  "kode_satker" varchar(50) COLLATE "pg_catalog"."default",
  "jenis_pengadaan" varchar(255) COLLATE "pg_catalog"."default",
  "pembayaran" int8,
  "tahun" int4
)
;

-- ----------------------------
-- Primary Key structure for table pembayaran_amel
-- ----------------------------
ALTER TABLE "public"."pembayaran_amel" ADD CONSTRAINT "pembayaran_amel_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Sequence for data_penyedia_nasional
-- ----------------------------
CREATE SEQUENCE "public"."data_penyedia_nasional_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for data_penyedia_nasional
-- ----------------------------
CREATE TABLE "public"."data_penyedia_nasional" (
  "id" int8 NOT NULL DEFAULT nextval('data_penyedia_nasional_id_seq'::regclass),
  "terverifikasi" int4 NOT NULL,
  "teragregasi" int4 NOT NULL,
  "terdaftar_sikap" int4 NOT NULL,
  "aktif" int4 NOT NULL,
  "lolos_pra" int4 NOT NULL,
  "menang" int4 NOT NULL,
  tanggal_update timestamp(6),
  tahun integer
)
;

-- ----------------------------
-- Primary Key structure for table data_penyedia_nasional
-- ----------------------------
ALTER TABLE "public"."data_penyedia_nasional" ADD CONSTRAINT "data_penyedia_nasional_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Sequence for inovasi_pengadaan
-- ----------------------------
CREATE SEQUENCE "public"."inovasi_pengadaan_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for inovasi_pengadaan
-- ----------------------------
CREATE TABLE "public"."inovasi_pengadaan" (
  "id" int8 NOT NULL DEFAULT nextval('inovasi_pengadaan_id_seq'::regclass),
  "pagu_dini" bigint NOT NULL,
  "paket_dini" integer NOT NULL,
  "pagu_konsolidasi" bigint NOT NULL,
  "paket_konsolidasi" integer NOT NULL,
  "pagu_itemize" bigint NOT NULL,
  "paket_itemize" integer NOT NULL,
  "pagu_lelang_cepat" bigint NOT NULL,
  "paket_lelang_cepat" integer NOT NULL,
  "pagu_reverse_auction" bigint NOT NULL,
  "paket_reverse_auction" integer NOT NULL,
  tanggal_update timestamp(6),
  tahun integer
)
;

-- ----------------------------
-- Primary Key structure for table inovasi_pengadaan
-- ----------------------------
ALTER TABLE "public"."inovasi_pengadaan" ADD CONSTRAINT "inovasi_pengadaan_pkey" PRIMARY KEY ("id");


CREATE SEQUENCE "public"."rekap_serah_terima_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;
CREATE TABLE "public"."rekap_serah_terima" (
  "id" bigint NOT NULL DEFAULT nextval('rekap_serah_terima_id_seq'::regclass),
  "tahun_anggaran" integer NOT NULL,
  "jenis_belanja" character varying(50),
  "jenis_pengadaan" character varying(50),
  "metode_pemilihan" character varying(50),
  "sumber_dana" character varying(50),
  "tipe_kldi" character varying(50),
  "kode_kldi" character varying(50),
  "nama_kldi" character varying(255),
  "kode_satker" character varying(50),
  "nama_satker" character varying(255),
  "paket" integer NOT NULL,
  "pagu" bigint NOT NULL
);
ALTER TABLE "public"."rekap_serah_terima" ADD CONSTRAINT "rekap_serah_terima_pkey" PRIMARY KEY ("id");
CREATE INDEX ON "public"."rekap_serah_terima" ((lower(sumber_dana)));
CREATE INDEX ON "public"."rekap_serah_terima" ((lower(tipe_kldi)));
CREATE INDEX ON "public"."rekap_serah_terima" ((lower(nama_kldi)));
CREATE INDEX ON "public"."rekap_serah_terima" ((lower(nama_satker)));
CREATE INDEX ON "public"."rekap_serah_terima" (kode_kldi);
CREATE INDEX ON "public"."rekap_serah_terima" (kode_satker);
CREATE INDEX ON "public"."rekap_serah_terima" (jenis_belanja);
CREATE INDEX ON "public"."rekap_serah_terima" (jenis_pengadaan);
CREATE INDEX ON "public"."rekap_serah_terima" (metode_pemilihan);

-- ----------------------------
-- Table structure for adjsutment advokasi original and summary
-- ----------------------------

CREATE SEQUENCE "public"."adjustment_advokasi_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."adjustment_advokasi" (
"id" int8 NOT NULL DEFAULT nextval('adjustment_advokasi_id_seq'::regclass),
"layanan_advokasi" int4,
"kasus_pengadaan" int4,
"permasalahan_dilayani" int4,
"tanggal_update" timestamp(6),
"tahun" int4
);

ALTER TABLE "public"."adjustment_advokasi" ADD CONSTRAINT "adjustment_advokasi_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."adjustment_advokasi_original_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."adjustment_advokasi_original" (
"id" int8 NOT NULL DEFAULT nextval('adjustment_advokasi_original_id_seq'::regclass),
"layanan_advokasi" int4,
"kasus_pengadaan" int4,
"permasalahan_dilayani" int4,
"tanggal_update" timestamp(6),
"tahun" int4
);

ALTER TABLE "public"."adjustment_advokasi_original" ADD CONSTRAINT "adjustment_advokasi_original_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Table structure for adjustment ukpbj original and summary
-- ----------------------------

CREATE SEQUENCE "public"."adjustment_ukpbj_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."adjustment_ukpbj" (
"id" int8 NOT NULL DEFAULT nextval('adjustment_ukpbj_id_seq'::regclass),
"struktural" int4,
"adhoc" int4,
"belum_terbentuk" int4,
"sembilan_kematangan" int4,
"tanggal_update" timestamp(6),
"tahun" int4
);

ALTER TABLE "public"."adjustment_ukpbj" ADD CONSTRAINT "adjustment_ukpbj_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."adjustment_ukpbj_original_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."adjustment_ukpbj_original" (
"id" int8 NOT NULL DEFAULT nextval('adjustment_ukpbj_original_id_seq'::regclass),
"struktural" int4,
"adhoc" int4,
"belum_terbentuk" int4,
"sembilan_kematangan" int4,
"tanggal_update" timestamp(6),
"tahun" int4
);

ALTER TABLE "public"."adjustment_ukpbj_original" ADD CONSTRAINT "adjustment_ukpbj_original_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Table structure for adjustment lpse original and summary
-- ----------------------------

CREATE SEQUENCE "public"."adjustment_lpse_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."adjustment_lpse" (
"id" int8 NOT NULL DEFAULT nextval('adjustment_lpse_id_seq'::regclass),
"total" int4,
"jumlah_highlight_lpse" int4,
"standar" int4,
"tanggal_update" timestamp(6),
"tahun" int4
);

ALTER TABLE "public"."adjustment_lpse" ADD CONSTRAINT "adjustment_lpse_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."adjustment_lpse_original_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."adjustment_lpse_original" (
"id" int8 NOT NULL DEFAULT nextval('adjustment_lpse_original_id_seq'::regclass),
"total" int4,
"jumlah_highlight_lpse" int4,
"standar" int4,
"tanggal_update" timestamp(6),
"tahun" int4
);

ALTER TABLE "public"."adjustment_lpse_original" ADD CONSTRAINT "adjustment_lpse_original_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Table structure for adjustment data penyedia original and summary
-- ----------------------------

CREATE SEQUENCE "public"."adjustment_data_penyedia_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."adjustment_data_penyedia" (
"id" int8 NOT NULL DEFAULT nextval('adjustment_data_penyedia_id_seq'::regclass),
"terverifikasi" int8,
"terdaftar_sikap" int8,
"aktif" int8,
"menang" int8,
"tanggal_update" timestamp(6),
"tahun" int4
);

ALTER TABLE "public"."adjustment_data_penyedia" ADD CONSTRAINT "adjustment_data_penyedia_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."adjustment_data_penyedia_original_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."adjustment_data_penyedia_original" (
"id" int8 NOT NULL DEFAULT nextval('adjustment_data_penyedia_original_id_seq'::regclass),
"terverifikasi" int8,
"terdaftar_sikap" int8,
"aktif" int8,
"menang" int8,
"tanggal_update" timestamp(6),
"tahun" int4
);

ALTER TABLE "public"."adjustment_data_penyedia_original" ADD CONSTRAINT "adjustment_data_penyedia_original_pkey" PRIMARY KEY ("id");


-- ----------------------------
-- Table structure for adjustment inovasi pengadaan original and summary
-- ----------------------------

CREATE SEQUENCE "public"."adjustment_inovasi_pengadaan_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."adjustment_inovasi_pengadaan" (
"id" int8 NOT NULL DEFAULT nextval('adjustment_inovasi_pengadaan_id_seq'::regclass),
"lelang_dini" float8,
"konsolidasi" float8,
"tender_cepat" float8,
"kontrak_payung" float8,
"tender_itemized" float8,
"tanggal_update" timestamp(6),
"tahun" int4
);

ALTER TABLE "public"."adjustment_inovasi_pengadaan" ADD CONSTRAINT "adjustment_inovasi_pengadaan_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."adjustment_inovasi_pengadaan_original_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."adjustment_inovasi_pengadaan_original" (
"id" int8 NOT NULL DEFAULT nextval('adjustment_inovasi_pengadaan_original_id_seq'::regclass),
"lelang_dini" float8,
"konsolidasi" float8,
"tender_cepat" float8,
"kontrak_payung" float8,
"tender_itemized" float8,
"tanggal_update" timestamp(6),
"tahun" int4
);

ALTER TABLE "public"."adjustment_inovasi_pengadaan_original" ADD CONSTRAINT "adjustment_inovasi_pengadaan_original_pkey" PRIMARY KEY ("id");


-- ----------------------------
-- Table structure for adjustment pdn umkm original and summary
-- ----------------------------

CREATE SEQUENCE "public"."adjustment_pdn_umkm_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."adjustment_pdn_umkm" (
"id" int8 NOT NULL DEFAULT nextval('adjustment_pdn_umkm_id_seq'::regclass),
"pdn" float8,
"pagu_umkm" float8,
"tanggal_update" timestamp(6),
"tahun" int4
);

ALTER TABLE "public"."adjustment_pdn_umkm" ADD CONSTRAINT "adjustment_pdn_umkm_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE "public"."adjustment_pdn_umkm_original_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."adjustment_pdn_umkm_original" (
"id" int8 NOT NULL DEFAULT nextval('adjustment_pdn_umkm_original_id_seq'::regclass),
"pdn" float8,
"pagu_umkm" float8,
"tanggal_update" timestamp(6),
"tahun" int4
);

ALTER TABLE "public"."adjustment_pdn_umkm_original" ADD CONSTRAINT "adjustment_pdn_umkm_original_pkey" PRIMARY KEY ("id");


CREATE SEQUENCE "public"."pmm_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

CREATE TABLE "public"."pmm" (
    "id" bigint NOT NULL DEFAULT nextval('pmm_id_seq'::regclass),
    "kode_kldi" character varying(50),
    "nama_kldi" character varying(255),
    "tahun" int4,
    "bulan" int4,
    "jumlah_tepat_waktu" NUMERIC NULL,
    "jumlah_total" NUMERIC NULL,
    "nilai_tepat_waktu" NUMERIC NULL,
    "nilai_total" NUMERIC NULL,
    "pagu_pemilihan" NUMERIC NULL,
    "hasil_pemilihan" NUMERIC NULL
);

ALTER TABLE "public"."pmm" ADD CONSTRAINT "pmm_pkey" PRIMARY KEY ("id");

ALTER TABLE "public"."pembayaran_amel" ADD COLUMN created_at timestamp(6);
ALTER TABLE "public"."pembayaran_amel" ADD COLUMN source_repo numeric;

CREATE TABLE "public"."rekap_pengadaan_nasional_detail" (
"total_anggaran" int8 NOT NULL,
"belanja_pengadaan" int8 NOT NULL,
"rencana_pagu" int8 NOT NULL,
"rencana_paket" int8 NOT NULL,
"pelaksanaan_pagu" int8 NOT NULL,
"pelaksanaan_paket" int8 NOT NULL,
"hasil_paket" int8 NOT NULL,
"hasil_pagu" int8 NOT NULL,
"kontrak" int8 NOT NULL,
"pembayaran" int8 NOT NULL,
"kode_kldi" varchar(255) COLLATE "default",
"nama_kldi" text COLLATE "default",
"jenis_kldi" varchar(255) COLLATE "default",
"jenis_pemerintahan" varchar(255) COLLATE "default",
"tahun_anggaran" int4
)
WITH (OIDS=FALSE)

;
/*
Navicat PGSQL Data Transfer

Source Server         : Ubuntu-ui-psql
Source Server Version : 101000
Source Host           : 192.168.120.30:5432
Source Database       : monev_ng
Source Schema         : dwh_procurement

Target Server Type    : PGSQL
Target Server Version : 101000
File Encoding         : 65001

Date: 2019-12-11 14:55:39
*/

CREATE SCHEMA IF NOT EXISTS dwh_procurement;

-- ----------------------------
-- Table structure for check_paket_id
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."check_paket_id";
CREATE TABLE "dwh_procurement"."check_paket_id" (
"kode_paket" int4,
"nama" text COLLATE "default",
"hasil" text COLLATE "default",
"kode_paket_bl" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for cpi_report_01
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."cpi_report_01";
CREATE TABLE "dwh_procurement"."cpi_report_01" (
"tahun_anggaran" int2,
"kode_paket" numeric(19),
"kode_paket_rup" numeric(19),
"kode_lelang" numeric(19),
"kode_kldi" varchar(20) COLLATE "default",
"nama_lpse" varchar(255) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"jenis_kldi" varchar(255) COLLATE "default",
"nama_satker" varchar(200) COLLATE "default",
"nama_kabupaten" varchar(80) COLLATE "default",
"nama_propinsi" varchar(80) COLLATE "default",
"sumber_dana" text COLLATE "default",
"jenis_pengadaan" varchar(80) COLLATE "default",
"metode_pemilihan" text COLLATE "default",
"nama_paket" text COLLATE "default",
"versi_lelang" int2,
"kode_rekening" text COLLATE "default",
"nilai_pagu" float8,
"nilai_hps" numeric(20,2),
"nilai_anggaran" float8,
"tgl_lelang_dibuat" timestamp(6),
"tgl_umum_prakualifikasi" timestamp(6),
"tgl_umum_pascakualifikasi" timestamp(6),
"tgl_sppbj" timestamp(6),
"tgl_ttd_kontrak" timestamp(6),
"tgl_buka_dok_penawaran" timestamp(6),
"tgl_rencana_awal_pengadaan" timestamp(6),
"tgl_rencana_akhir_pengadaan" timestamp(6),
"tgl_umum_pemenang" timestamp(6),
"psr_harga" float8,
"psr_harga_terkoreksi" float8,
"jenis_belanja" text COLLATE "default",
"tgl_dok_lelang" timestamp(6),
"tgl_upload_dok_penawaran" timestamp(6),
"kode_lpse" int4,
"tgl_umum_pemenang_awal" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for cpi_report_01_a
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."cpi_report_01_a";
CREATE TABLE "dwh_procurement"."cpi_report_01_a" (
"tahun_anggaran" float8,
"kode_lpse" int4,
"nama_lpse" varchar(255) COLLATE "default",
"kode_kldi" text COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"kode_kldi_rup" varchar(255) COLLATE "default",
"nama_satker" text COLLATE "default",
"nama_satker_rup" varchar(200) COLLATE "default",
"nama_kabupaten" varchar(80) COLLATE "default",
"nama_propinsi" varchar(80) COLLATE "default",
"jenis_kldi" varchar(255) COLLATE "default",
"kode_paket" numeric(19),
"kode_paket_rup" numeric(19),
"nama_paket" text COLLATE "default",
"nama_paket_rup" text COLLATE "default",
"jenis_pengadaan" varchar(80) COLLATE "default",
"metode_pemilihan" text COLLATE "default",
"sumber_dana" text COLLATE "default",
"kode_lelang" numeric(19),
"versi_lelang" int2,
"kode_rekening" text COLLATE "default",
"kode_rekening_rup" varchar(200) COLLATE "default",
"nilai_pagu" float8,
"nilai_hps" float8,
"nilai_anggaran" float8,
"tgl_lelang_dibuat" timestamp(6),
"tgl_umum_prakualifikasi" timestamp(6),
"tgl_umum_pascakualifikasi" timestamp(6),
"tgl_sppbj" timestamp(6),
"tgl_ttd_kontrak" timestamp(6),
"tgl_buka_dok_penawaran" timestamp(6),
"tgl_rencana_awal_pengadaan" timestamp(6),
"tgl_rencana_akhir_pengadaan" timestamp(6),
"tgl_umum_pemenang" timestamp(6),
"tgl_umum_pemenang_awal" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for cpi_report_02
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."cpi_report_02";
CREATE TABLE "dwh_procurement"."cpi_report_02" (
"tahun_anggaran" int2,
"kode_kldi" varchar(20) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"jenis_kldi" varchar(255) COLLATE "default",
"sumber_dana" text COLLATE "default",
"nama_kabupaten" varchar(80) COLLATE "default",
"nama_propinsi" varchar(80) COLLATE "default",
"kode_lpse" int4,
"nama_lpse" varchar(255) COLLATE "default",
"kode_paket" numeric(19),
"nama_paket" varchar(5000) COLLATE "default",
"kode_paket_rup" numeric(19),
"nama_paket_rup" text COLLATE "default",
"versi_lelang" float8,
"kode_lelang" numeric(19),
"kode_peserta" numeric(19),
"nama_penyedia" varchar(100) COLLATE "default",
"npwp_penyedia" varchar(50) COLLATE "default",
"is_pemenang" int2,
"ev_admin" int4,
"ev_teknis" int4,
"ev_harga" int4,
"ev_kualifikasi" int4,
"harga_penawaran" float8,
"harga_terkoreksi" float8,
"nama_satker" varchar(200) COLLATE "default",
"nama_satker_rup" varchar(200) COLLATE "default",
"kls_id" varchar(2) COLLATE "default",
"klasifikasi_usaha" varchar(80) COLLATE "default",
"metode_pemilihan" text COLLATE "default",
"metode_kualifikasi" varchar(50) COLLATE "default",
"jenis_pengadaan" varchar(80) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for cpi_report_03
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."cpi_report_03";
CREATE TABLE "dwh_procurement"."cpi_report_03" (
"tahun_anggaran" int2,
"tgl_sanggah" timestamp(6),
"tgl_jawab_sanggah" timestamp(6),
"thp_nama" varchar(200) COLLATE "default",
"kode_lpse" int4,
"nama_lpse" varchar(255) COLLATE "default",
"kode_kldi" varchar(20) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"jenis_kldi" varchar(255) COLLATE "default",
"nama_kabupaten" varchar(80) COLLATE "default",
"nama_propinsi" varchar(80) COLLATE "default",
"kode_lelang" numeric(19),
"nama_paket" varchar(500) COLLATE "default",
"sumber_dana" text COLLATE "default",
"kode_rekening" text COLLATE "default",
"kode_penyedia" numeric(19),
"nama_penyedia" varchar(100) COLLATE "default",
"npwp_penyedia" varchar(50) COLLATE "default",
"nilai_pagu" numeric(20,2),
"jenis_pengadaan" varchar(80) COLLATE "default",
"metode_pemilihan" varchar(255) COLLATE "default",
"kode_paket" numeric(19)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for cpi_report_03_sg
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."cpi_report_03_sg";
CREATE TABLE "dwh_procurement"."cpi_report_03_sg" (
"repo_id" int4,
"lls_id" numeric(19),
"pkt_id" numeric(19),
"pkt_nama" varchar(500) COLLATE "default",
"min" float8,
"pagu" numeric(20,2),
"lls_versi_lelang" int2,
"eva_versi" int4
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for cpi_report_04
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."cpi_report_04";
CREATE TABLE "dwh_procurement"."cpi_report_04" (
"kode_paket" int4,
"id_swakelola" int8,
"nama_jenis" varchar(255) COLLATE "default",
"nama_paket" text COLLATE "default",
"jenis_pengadaan" varchar(40) COLLATE "default",
"satuan_kerja" varchar(255) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"tipe_kldi" varchar(255) COLLATE "default",
"kabupaten" varchar(80) COLLATE "default",
"propinsi" varchar(80) COLLATE "default",
"tahun_anggaran" int4,
"nilai_pagu" int8,
"metode_pemilihan" varchar(40) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for dm_fraud_address
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."dm_fraud_address";
CREATE TABLE "dwh_procurement"."dm_fraud_address" (
"nama_penyedia" text COLLATE "default",
"rkn_alamat" varchar(1000) COLLATE "default",
"alamat_penyedia" text COLLATE "default",
"rkn_kodepos" varchar(8) COLLATE "default",
"rkn_telepon" varchar(50) COLLATE "default",
"rkn_fax" varchar(50) COLLATE "default",
"rkn_mobile_phone" varchar(50) COLLATE "default",
"rkn_npwp" varchar(50) COLLATE "default",
"rkn_pkp" varchar(100) COLLATE "default",
"rkn_statcabang" varchar(1) COLLATE "default",
"rkn_email" varchar(1000) COLLATE "default",
"rkn_website" varchar(40) COLLATE "default",
"rkn_almtpusat" varchar(1000) COLLATE "default",
"rkn_telppusat" varchar(50) COLLATE "default",
"rkn_faxpusat" varchar(50) COLLATE "default",
"rkn_emailpusat" varchar(40) COLLATE "default",
"rkn_namauser" varchar(50) COLLATE "default",
"rkn_isactive" int2,
"key_penyedia" text COLLATE "default",
"key_alamat" text COLLATE "default",
"key_npwp" text COLLATE "default",
"key_phone" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for dm_klasifikasipenyedia
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."dm_klasifikasipenyedia";
CREATE TABLE "dwh_procurement"."dm_klasifikasipenyedia" (
"nama_penyedia" varchar(200) COLLATE "default",
"npwp_penyedia" varchar(200) COLLATE "default",
"lokasi_penyedia_kabupaten" varchar(80) COLLATE "default",
"lokasi_penyedia_provinsi" varchar(80) COLLATE "default",
"tipe" varchar(10) COLLATE "default",
"kode_jenis_usaha" varchar(10) COLLATE "default",
"klasifikasi" varchar(200) COLLATE "default",
"kategori" varchar(200) COLLATE "default",
"golongan_pokok" varchar(250) COLLATE "default",
"golongan" varchar(200) COLLATE "default",
"sub_golongan" varchar(250) COLLATE "default",
"kelompok" varchar(200) COLLATE "default",
"verified" bool,
"tahun" int2,
"update_date" timestamp(6),
"status_adp" bool
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for dm_profilpenyedia
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."dm_profilpenyedia";
CREATE TABLE "dwh_procurement"."dm_profilpenyedia" (
"tahun_anggaran" int2,
"nama_penyedia" text COLLATE "default",
"npwp_penyedia" text COLLATE "default",
"lolos_evaluasi_thp0" int8,
"lolos_evaluasi_thp1" int8,
"lolos_evaluasi_thp2" int8,
"lolos_evaluasi_thp3" int8,
"lolos_evaluasi_thp4" int8,
"jumlah_paket" int8,
"jumlah_ikut_lelang" int8,
"jumlah_ikut_peserta" int8,
"jumlah_menang" int8,
"total_perolehan" numeric(22,2),
"lokasi_lelang_kabupaten" varchar(80) COLLATE "default",
"lokasi_lelang_provinsi" varchar(80) COLLATE "default",
"repo_lelang" int4,
"lokasi_penyedia_kabupaten" varchar(80) COLLATE "default",
"lokasi_penyedia_provinsi" varchar(80) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for dm_rekappengadaan
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."dm_rekappengadaan";
CREATE TABLE "dwh_procurement"."dm_rekappengadaan" (
"tahun_anggaran" int2,
"jenis_kldi" varchar(255) COLLATE "default",
"id_kldi" varchar(255) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"nama_propinsi" varchar(80) COLLATE "default",
"nama_kabupaten" varchar(80) COLLATE "default",
"jumlah_paket_penyedia" int8,
"jumlah_kegiatan_swakelola" int8,
"jumlah_kegiatan_penyedia_swakelola" int8,
"jumlah_paket_lelang" int8,
"jumlah_paket_lelang_selesai" int8,
"jumlah_paket_ekatalog" int8,
"anggaran_sismontepra" numeric(22,2),
"pagu_paket_penyedia" numeric(22,2),
"pagu_kegiatan_swakelola" numeric(22,2),
"pagu_kegiatan_penyedia_swakelola" numeric(22,2),
"dekon_tp" numeric(22,2),
"anggaran_pegawai_penyedia" numeric(22,2),
"anggaran_pegawai_swakelola" numeric(22,2),
"pagu_lelang" numeric(22,2),
"pagu_lelang_selesai" numeric(22,2),
"nilai_hasil_lelang" numeric(22,2),
"nilai_hps" numeric(22,2),
"pagu_ekatalog" numeric(22,2),
"update_date" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for dm_rekappengadaanperbulan
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."dm_rekappengadaanperbulan";
CREATE TABLE "dwh_procurement"."dm_rekappengadaanperbulan" (
"tahun_anggaran" int2,
"jenis_kldi" varchar(255) COLLATE "default",
"id_kldi" varchar(255) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"periode" timestamp(6),
"nama_propinsi" varchar(80) COLLATE "default",
"nama_kabupaten" varchar(80) COLLATE "default",
"jumlah_paket_penyedia" int8,
"jumlah_kegiatan_swakelola" int8,
"jumlah_kegiatan_penyedia_swakelola" int8,
"jumlah_paket_lelang" int8,
"jumlah_paket_lelang_selesai" int8,
"jumlah_paket_ekatalog" int8,
"pagu_paket_penyedia" numeric(22,2),
"pagu_kegiatan_swakelola" numeric(22,2),
"pagu_kegiatan_penyedia_swakelola" numeric(22,2),
"pagu_lelang" numeric(22,2),
"pagu_lelang_selesai" numeric(22,2),
"nilai_hasil_lelang" numeric(22,2),
"nilai_hps" numeric(22,2),
"pagu_ekatalog" numeric(22,2),
"update_date" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for dm_rekappengadaansatker
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."dm_rekappengadaansatker";
CREATE TABLE "dwh_procurement"."dm_rekappengadaansatker" (
"tahun_anggaran" int2,
"id_satker" varchar(500) COLLATE "default",
"nama_satuan_kerja" varchar(500) COLLATE "default",
"jenis_kldi" varchar(500) COLLATE "default",
"id_kldi" varchar(500) COLLATE "default",
"nama_kldi" varchar(500) COLLATE "default",
"nama_propinsi" varchar(100) COLLATE "default",
"nama_kabupaten" varchar(100) COLLATE "default",
"jumlah_paket_penyedia" int8,
"jumlah_kegiatan_swakelola" int8,
"jumlah_kegiatan_penyedia_swakelola" int8,
"jumlah_paket_lelang" int8,
"jumlah_paket_lelang_selesai" int8,
"jumlah_paket_ekatalog" int8,
"anggaran_sismontepra" numeric(22,2),
"pagu_paket_penyedia" numeric(22,2),
"pagu_kegiatan_swakelola" numeric(22,2),
"pagu_kegiatan_penyedia_swakelola" numeric(22,2),
"dekon_tp" numeric(22,2),
"anggaran_pegawai_penyedia" numeric(22,2),
"anggaran_pegawai_swakelola" numeric(22,2),
"pagu_lelang" numeric(22,2),
"pagu_lelang_selesai" numeric(22,2),
"nilai_hasil_lelang" numeric(22,2),
"nilai_hps" numeric(22,2),
"pagu_ekatalog" numeric(22,2),
"update_date" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for dm_rekappengadaansatker_lama
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."dm_rekappengadaansatker_lama";
CREATE TABLE "dwh_procurement"."dm_rekappengadaansatker_lama" (
"tahun_anggaran" int2,
"nama_kldi" varchar(500) COLLATE "default",
"jenis_kldi" varchar(500) COLLATE "default",
"id_satker" varchar(500) COLLATE "default",
"nama_satuan_kerja" varchar(500) COLLATE "default",
"nama_propinsi" varchar(100) COLLATE "default",
"nama_kabupaten" varchar(100) COLLATE "default",
"jumlah_paket_penyedia" int8,
"jumlah_kegiatan_swakelola" int8,
"jumlah_kegiatan_penyedia_swakelola" int8,
"anggaran_sirup" numeric(22,2),
"pagu_paket_penyedia" numeric(22,2),
"pagu_kegiatan_swakelola" numeric(22,2),
"pagu_kegiatan_penyedia_swakelola" numeric(22,2),
"dekon_tp" numeric(22,2),
"anggaran_pegawai_penyedia" numeric(22,2),
"anggaran_pegawai_swakelola" numeric(22,2),
"update_date" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for dm_riwayatlelang
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."dm_riwayatlelang";
CREATE TABLE "dwh_procurement"."dm_riwayatlelang" (
"repo_id" int4,
"pkt_id" numeric(19),
"tahun_anggaran" int2,
"nama_paket" text COLLATE "default",
"pkt_pagu" numeric(22,2),
"pkt_hps" numeric(22,2),
"tanggal_lelang_dibuat" timestamp(6),
"versi_lelang" int2,
"status_lelang" int2,
"alasan_lelang_diulang" text COLLATE "default",
"alasan_lelang_ditutup" text COLLATE "default",
"jumlah_peserta_lelang" int8,
"evaluasi0_lulus" int2,
"evaluasi1_lulus" int2,
"evaluasi2_lulus" int2,
"evaluasi3_lulus" int2,
"evaluasi4_lulus" int2,
"harga_terendah" numeric(22,2),
"harga_tertinggi" numeric(22,2),
"harga_hasil_lelang" numeric(22,2),
"harga_rata_rata" numeric(22,2),
"ada_pemenang" int2,
"nama_pemenang" text COLLATE "default",
"npwp_pemenang" text COLLATE "default",
"nama_satker" text COLLATE "default",
"nama_lpse" varchar(500) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"lelang_selesai" int2
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for epurchasing
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."epurchasing";
CREATE TABLE "dwh_procurement"."epurchasing" (
"tahun_anggaran" int2,
"kode_paket" numeric(19),
"konversi_harga_total" float8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for fraud_report_02
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."fraud_report_02";
CREATE TABLE "dwh_procurement"."fraud_report_02" (
"tahun_anggaran" int2,
"kode_kldi" varchar(20) COLLATE "default",
"kode_lpse" int4,
"nama_lpse" varchar(255) COLLATE "default",
"sumber_dana" text COLLATE "default",
"kode_paket" numeric(19),
"nama_paket" varchar(500) COLLATE "default",
"kode_paket_rup" numeric(19),
"nama_paket_rup" text COLLATE "default",
"kode_rekening" text COLLATE "default",
"versi_lelang" float8,
"kode_lelang" numeric(19),
"kode_peserta" numeric(19),
"kode_penyedia" numeric(19),
"nama_penyedia" varchar(100) COLLATE "default",
"npwp_penyedia" varchar(40) COLLATE "default",
"rkn_alamat" varchar(1000) COLLATE "default",
"rkn_email" varchar(1000) COLLATE "default",
"rkn_fax" varchar(50) COLLATE "default",
"rkn_mobile_phone" varchar(50) COLLATE "default",
"rkn_telepon" varchar(50) COLLATE "default",
"rkn_tgl_daftar" timestamp(6),
"rkn_tgl_setuju" timestamp(6),
"rkn_website" varchar(40) COLLATE "default",
"is_pemenang" int2,
"ev_admin" int4,
"ev_teknis" int4,
"ev_harga" int4,
"ev_kualifikasi" int4,
"nilai_pagu" numeric(52,2),
"harga_penawaran" float8,
"harga_terkoreksi" float8,
"nama_satker" varchar(200) COLLATE "default",
"nama_satker_rup" varchar(200) COLLATE "default",
"kls_id" varchar(2) COLLATE "default",
"klasifikasi_usaha" varchar(80) COLLATE "default",
"metode_pemilihan" text COLLATE "default",
"metode_kualifikasi" varchar(50) COLLATE "default",
"nilai_anggaran" float8,
"pkt_hps" numeric(20,2),
"tgl_umum_pemenang" timestamp(6),
"tgl_buka_dok_penawaran" timestamp(6),
"tgl_dok_lelang" timestamp(6),
"tgl_sppbj" timestamp(6),
"tgl_ttd_kontrak" timestamp(6),
"tgl_upload_dok_penawaran" timestamp(6),
"nama_kldi" varchar(255) COLLATE "default",
"jenis_kldi" varchar(255) COLLATE "default",
"nama_kabupaten" varchar(80) COLLATE "default",
"nama_propinsi" varchar(80) COLLATE "default",
"jenis_pengadaan" varchar(80) COLLATE "default",
"tgl_umum_pascakualifikasi" timestamp(6),
"tgl_umum_prakualifikasi" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for fraud_report_02a
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."fraud_report_02a";
CREATE TABLE "dwh_procurement"."fraud_report_02a" (
"tahun_anggaran" int2,
"kode_kldi" varchar(20) COLLATE "default",
"kode_lpse" int4,
"nama_lpse" varchar(255) COLLATE "default",
"sumber_dana" text COLLATE "default",
"kode_paket" numeric(19),
"nama_paket" varchar(500) COLLATE "default",
"kode_paket_rup" numeric(19),
"nama_paket_rup" text COLLATE "default",
"kode_rekening" text COLLATE "default",
"versi_lelang" float8,
"kode_lelang" numeric(19),
"kode_peserta" numeric(19),
"kode_penyedia" numeric(19),
"nama_penyedia" varchar(100) COLLATE "default",
"npwp_penyedia" varchar(40) COLLATE "default",
"rkn_alamat" varchar(1000) COLLATE "default",
"rkn_email" varchar(1000) COLLATE "default",
"rkn_fax" varchar(50) COLLATE "default",
"rkn_mobile_phone" varchar(50) COLLATE "default",
"rkn_telepon" varchar(50) COLLATE "default",
"rkn_tgl_daftar" timestamp(6),
"rkn_tgl_setuju" timestamp(6),
"rkn_website" varchar(40) COLLATE "default",
"is_pemenang" int2,
"ev_admin" int4,
"ev_teknis" int4,
"ev_harga" int4,
"ev_kualifikasi" int4,
"nilai_pagu" numeric(54,2),
"harga_penawaran" float8,
"harga_terkoreksi" float8,
"nama_satker" varchar(200) COLLATE "default",
"nama_satker_rup" varchar(200) COLLATE "default",
"kls_id" varchar(2) COLLATE "default",
"klasifikasi_usaha" varchar(80) COLLATE "default",
"metode_pemilihan" text COLLATE "default",
"metode_kualifikasi" varchar(50) COLLATE "default",
"nilai_anggaran" float8,
"pkt_hps" numeric(22,2),
"tgl_umum_pemenang" timestamp(6),
"tgl_buka_dok_penawaran" timestamp(6),
"tgl_dok_lelang" timestamp(6),
"tgl_sppbj" timestamp(6),
"tgl_ttd_kontrak" timestamp(6),
"tgl_upload_dok_penawaran" timestamp(6),
"nama_kldi" varchar(255) COLLATE "default",
"jenis_kldi" varchar(255) COLLATE "default",
"nama_kabupaten" varchar(80) COLLATE "default",
"nama_propinsi" varchar(80) COLLATE "default",
"jenis_pengadaan" varchar(80) COLLATE "default",
"tgl_umum_pascakualifikasi" timestamp(6),
"tgl_umum_prakualifikasi" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for jenis_pengadaan
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."jenis_pengadaan";
CREATE TABLE "dwh_procurement"."jenis_pengadaan" (
"id" int4 NOT NULL,
"jenis" varchar(40) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for master_pemilihan
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."master_pemilihan";
CREATE TABLE "dwh_procurement"."master_pemilihan" (
"no" int4 NOT NULL,
"mtd_pemilihan" varchar(30) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for monev_ng_report_01a
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."monev_ng_report_01a";
CREATE TABLE "dwh_procurement"."monev_ng_report_01a" (
"tahun_anggaran" int4,
"header1" varchar(50) COLLATE "default",
"header2" varchar(50) COLLATE "default",
"jenis_penyedia" varchar(50) COLLATE "default",
"systems" varchar(50) COLLATE "default",
"fields" varchar(255) COLLATE "default",
"tipe_kldi" varchar(255) COLLATE "default",
"kode_kldi" varchar(255) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"id" int4,
"paket" int4,
"nilai" numeric(22,2),
"dekon_flag" varchar(50) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for monev_ng_report_01a_satker
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."monev_ng_report_01a_satker";
CREATE TABLE "dwh_procurement"."monev_ng_report_01a_satker" (
"tahun_anggaran" int4,
"header1" varchar(50) COLLATE "default",
"header2" varchar(50) COLLATE "default",
"jenis_penyedia" varchar(50) COLLATE "default",
"systems" varchar(50) COLLATE "default",
"fields" varchar(255) COLLATE "default",
"tipe_kldi" varchar(255) COLLATE "default",
"kode_kldi" varchar(255) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"id" int4,
"paket" int4,
"nilai" numeric(22,2),
"dekon_flag" varchar(50) COLLATE "default",
"satker" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for monev_ng_report_01b
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."monev_ng_report_01b";
CREATE TABLE "dwh_procurement"."monev_ng_report_01b" (
"tahun_anggaran" int4,
"header1" varchar(50) COLLATE "default",
"header2" varchar(50) COLLATE "default",
"jenis_belanja" varchar(50) COLLATE "default",
"jenis_penyedia" varchar(50) COLLATE "default",
"systems" varchar(50) COLLATE "default",
"fields" varchar(255) COLLATE "default",
"tipe_kldi" varchar(255) COLLATE "default",
"kode_kldi" varchar(255) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"id" int4,
"paket" numeric(20),
"nilai" numeric(22,2)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for monev_ng_report_01b_satker
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."monev_ng_report_01b_satker";
CREATE TABLE "dwh_procurement"."monev_ng_report_01b_satker" (
"tahun_anggaran" int4,
"header1" varchar(50) COLLATE "default",
"header2" varchar(50) COLLATE "default",
"jenis_belanja" varchar(50) COLLATE "default",
"jenis_penyedia" varchar(50) COLLATE "default",
"systems" varchar(50) COLLATE "default",
"fields" varchar(255) COLLATE "default",
"tipe_kldi" varchar(255) COLLATE "default",
"kode_kldi" varchar(255) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"id" int4,
"paket" numeric(20),
"nilai" numeric(22,2),
"satker" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for monev_ng_report_01c
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."monev_ng_report_01c";
CREATE TABLE "dwh_procurement"."monev_ng_report_01c" (
"tahun_anggaran" int4,
"pusat_daerah" varchar(50) COLLATE "default",
"tanggal" timestamp(6),
"jenis_penyedia" varchar(50) COLLATE "default",
"jenis_swakelola" varchar(50) COLLATE "default",
"jenis_pengadaan" varchar(40) COLLATE "default",
"kelola_penyedia" text COLLATE "default",
"tipe_kldi" varchar(255) COLLATE "default",
"kode_kldi" varchar(255) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"paket" int4,
"nilai" numeric(22,2),
"tanggal_awal_pengadaan" timestamp(6),
"tanggal_akhir_pengadaan" timestamp(6),
"tanggal_awal_pekerjaan" timestamp(6),
"tanggal_akhir_pekerjaan" timestamp(6),
"satker" varchar(255) COLLATE "default",
"tanggal_pengumuman" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for monev_ng_report_02
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."monev_ng_report_02";
CREATE TABLE "dwh_procurement"."monev_ng_report_02" (
"id" int4,
"alamat" varchar(255) COLLATE "default",
"additional_address" varchar(255) COLLATE "default",
"alasan_terdaftar" int4,
"correspondence_email" varchar(255) COLLATE "default",
"correspondence_id" varchar(255) COLLATE "default",
"correspondence_name" varchar(255) COLLATE "default",
"correspondence_phone" varchar(255) COLLATE "default",
"description" text COLLATE "default",
"direktur" varchar(255) COLLATE "default",
"instansi" varchar(255) COLLATE "default",
"kab_kota" int4,
"keterangan" varchar(65535) COLLATE "default",
"kode_lelang" numeric(19),
"nama_jenis" text COLLATE "default",
"nama_penyedia" varchar(255) COLLATE "default",
"npwp" varchar(255) COLLATE "default",
"propinsi_id" int4,
"kabupaten_penetapan" varchar(80) COLLATE "default",
"propinsi_penetapan" varchar(80) COLLATE "default",
"kabupaten_domisili" varchar(80) COLLATE "default",
"propinsi_domisili" varchar(80) COLLATE "default",
"created_at" timestamp(6),
"deleted_at" timestamp(6),
"start_date" timestamp(6),
"expired_date" timestamp(6),
"tgl_umum_pemenang" timestamp(6),
"violation" float8,
"is_pemenang" int2,
"tahun_anggaran" varchar(20) COLLATE "default",
"nama_paket" varchar(500) COLLATE "default",
"kode_rup" numeric(19),
"nama_kldi" varchar(255) COLLATE "default",
"nama_satker" varchar(200) COLLATE "default",
"jenis_pengadaan" varchar(80) COLLATE "default",
"metode_pemilihan" text COLLATE "default",
"nilai_pagu" float8,
"nilai_hps" numeric(22,2),
"tgl_lelang_dibuat" timestamp(6),
"publish" varchar(25) COLLATE "default",
"publish_date" timestamp(6),
"sk" varchar(255) COLLATE "default",
"update_at_nama_jenis" timestamp(6),
"hps_inaproc" int8,
"pagu_inaproc" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for monev_ng_report_03
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."monev_ng_report_03";
CREATE TABLE "dwh_procurement"."monev_ng_report_03" (
"id" int4,
"alamat" varchar(255) COLLATE "default",
"additional_address" varchar(255) COLLATE "default",
"alasan_terdaftar" int4,
"correspondence_email" varchar(255) COLLATE "default",
"correspondence_id" varchar(255) COLLATE "default",
"correspondence_name" varchar(255) COLLATE "default",
"correspondence_phone" varchar(255) COLLATE "default",
"direktur" varchar(255) COLLATE "default",
"instansi" varchar(255) COLLATE "default",
"kab_kota" int4,
"keterangan" varchar(65535) COLLATE "default",
"kode_lelang" numeric(19),
"nama_penyedia" varchar(255) COLLATE "default",
"npwp" varchar(255) COLLATE "default",
"propinsi_id" int4,
"kabupaten_penetapan" varchar(80) COLLATE "default",
"propinsi_penetapan" varchar(80) COLLATE "default",
"kabupaten_domisili" varchar(80) COLLATE "default",
"propinsi_domisili" varchar(80) COLLATE "default",
"created_at" timestamp(6),
"deleted_at" timestamp(6),
"start_date" timestamp(6),
"expired_date" timestamp(6),
"tgl_umum_pemenang" timestamp(6),
"violation" float8,
"is_pemenang" int2,
"tahun_anggaran" varchar(20) COLLATE "default",
"nama_paket" varchar(500) COLLATE "default",
"kode_rup" numeric(19),
"nama_kldi" varchar(255) COLLATE "default",
"nama_satker" varchar(200) COLLATE "default",
"jenis_pengadaan" varchar(80) COLLATE "default",
"metode_pemilihan" text COLLATE "default",
"tgl_lelang_dibuat" timestamp(6),
"publish" varchar(25) COLLATE "default",
"description" text COLLATE "default",
"nama_jenis" text COLLATE "default",
"publish_date" timestamp(6),
"sk" varchar(255) COLLATE "default",
"update_at_nama_jenis" timestamp(6),
"hps_inaproc" int8,
"pagu_inaproc" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for monev_ng_report_04
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."monev_ng_report_04";
CREATE TABLE "dwh_procurement"."monev_ng_report_04" (
"kode_kldi" varchar(6) COLLATE "default",
"nama_kldi" varchar(256) COLLATE "default",
"jenis_kldi" varchar(256) COLLATE "default",
"kode_satker" int8,
"nama_satker" varchar(200) COLLATE "default",
"komoditas_id" int8,
"nama_komoditas" varchar(100) COLLATE "default",
"sub_komoditas" varchar(300) COLLATE "default",
"sub_kategori" varchar(300) COLLATE "default",
"no_produk" varchar(200) COLLATE "default",
"nama_produk" varchar(200) COLLATE "default",
"konversi_harga_total" numeric(22,2),
"nilai_paket" numeric(22,2),
"nama_penyedia" varchar(100) COLLATE "default",
"status_paket" varchar(100) COLLATE "default",
"kuantitas" numeric(25,5),
"no_paket" varchar(50) COLLATE "default",
"rup_id" int8,
"nama_kabupaten" varchar(80) COLLATE "default",
"nama_provinsi" varchar(80) COLLATE "default",
"nama_paket" varchar(1000) COLLATE "default",
"id_produk" int4,
"npwp_penyedia" varchar(100) COLLATE "default",
"tanggal_buat_paket" timestamp(6),
"tanggal_selesai" timestamp(6),
"tanggal_awal_pengadaan" timestamp(6),
"active" int2,
"apakah_berlaku" int2,
"tgl_masa_berlaku_selesai" timestamp(6),
"paket_id" int8,
"tahun_anggaran" int4,
"jenis_pengadaan" varchar(40) COLLATE "default",
"tanggal_akhir_pengadaan" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for monev_ng_report_06
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."monev_ng_report_06";
CREATE TABLE "dwh_procurement"."monev_ng_report_06" (
"tahun_anggaran" int4,
"title" varchar(50) COLLATE "default",
"tanggal" timestamp(6),
"tipe_kldi" varchar(255) COLLATE "default",
"kode_kldi" varchar(255) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"paket" int4,
"nilai" numeric(22,2),
"nama_satker" text COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for proc_indicator
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."proc_indicator";
CREATE TABLE "dwh_procurement"."proc_indicator" (
"tahun_anggaran" int2,
"kode_paket" numeric(19),
"kode_paket_rup" numeric(19),
"kode_lelang" numeric(19),
"kode_kldi" text COLLATE "default",
"nama_lpse" varchar(255) COLLATE "default",
"nama_kldi" varchar(255) COLLATE "default",
"jenis_kldi" varchar(255) COLLATE "default",
"nama_satker" varchar(200) COLLATE "default",
"nama_agency" varchar(80) COLLATE "default",
"nama_kabupaten" varchar(80) COLLATE "default",
"nama_propinsi" varchar(80) COLLATE "default",
"sumber_dana" text COLLATE "default",
"jenis_pengadaan" varchar(80) COLLATE "default",
"metode_pemilihan" text COLLATE "default",
"jenis_belanja" text COLLATE "default",
"metode_kualifikasi" varchar(50) COLLATE "default",
"nama_paket" text COLLATE "default",
"versi_lelang" int2,
"kode_rekening" text COLLATE "default",
"kode_lpse" int4,
"nilai_pagu" numeric(20,2),
"nilai_hps" numeric(20,2),
"nilai_anggaran" float8,
"tgl_lelang_dibuat" timestamp(6),
"tgl_pengumuman" timestamp(6),
"tgl_umum_prakualifikasi" timestamp(6),
"tgl_umum_pascakualifikasi" timestamp(6),
"tgl_sppbj" timestamp(6),
"tgl_ttd_kontrak" timestamp(6),
"tgl_buka_dok_penawaran" timestamp(6),
"tgl_rencana_awal_pengadaan" timestamp(6),
"tgl_rencana_akhir_pengadaan" timestamp(6),
"tgl_umum_pemenang" timestamp(6),
"psr_harga" float8,
"psr_harga_terkoreksi" float8,
"tgl_dok_lelang" timestamp(6),
"tgl_upload_dok_penawaran" timestamp(6),
"jml_peserta" int8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for proc_indicator2
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."proc_indicator2";
CREATE TABLE "dwh_procurement"."proc_indicator2" (
"tahun" int4,
"no_paket" varchar(50) COLLATE "default",
"rup_id" int8,
"tipe_kldi" varchar(256) COLLATE "default",
"kode_kldi" varchar(6) COLLATE "default",
"nama_satuan_kerja" varchar(200) COLLATE "default",
"nama_kldi" varchar(256) COLLATE "default",
"nama_komoditas" varchar(100) COLLATE "default",
"nama_kabupaten" varchar(80) COLLATE "default",
"nama_provinsi" varchar(80) COLLATE "default",
"nama_paket" varchar(1000) COLLATE "default",
"nilai_paket" numeric(22,2),
"nama_penyedia" varchar(100) COLLATE "default",
"npwp_penyedia" varchar(100) COLLATE "default",
"tanggal_buat_paket" timestamp(6),
"tanggal_selesai" timestamp(6),
"tanggal_awal_pengadaan" timestamp(6)
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for same_address
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."same_address";
CREATE TABLE "dwh_procurement"."same_address" (
"rkn_nama_ref" varchar(255) COLLATE "default",
"rkn_nama" varchar(255) COLLATE "default",
"rkn_npwp" varchar(50) COLLATE "default",
"rkn_id" numeric(19),
"rkn_email" varchar(1000) COLLATE "default",
"rkn_telepon" varchar(50) COLLATE "default",
"rkn_alamat" varchar(4000) COLLATE "default",
"nama_similarity" float8,
"rkn_alamat_sim" varchar(4000) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for same_email
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."same_email";
CREATE TABLE "dwh_procurement"."same_email" (
"rkn_nama_ref" varchar(255) COLLATE "default",
"rkn_nama" varchar(255) COLLATE "default",
"rkn_npwp" varchar(50) COLLATE "default",
"rkn_id" numeric(19),
"rkn_email" varchar(1000) COLLATE "default",
"rkn_alamat" varchar(4000) COLLATE "default",
"nama_similarity" float8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for same_npwp
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."same_npwp";
CREATE TABLE "dwh_procurement"."same_npwp" (
"rkn_nama" varchar(255) COLLATE "default",
"rkn_npwp" varchar(50) COLLATE "default",
"rkn_id" numeric(19),
"src" int4,
"nama_similarity" float8,
"rkn_nama_ref" varchar(255) COLLATE "default",
"rkn_email" varchar(1000) COLLATE "default",
"rkn_alamat" varchar(4000) COLLATE "default",
"rkn_telepon" varchar(50) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for same_phone
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."same_phone";
CREATE TABLE "dwh_procurement"."same_phone" (
"rkn_nama_ref" varchar(255) COLLATE "default",
"rkn_nama" varchar(255) COLLATE "default",
"rkn_npwp" varchar(50) COLLATE "default",
"rkn_id" numeric(19),
"rkn_email" varchar(1000) COLLATE "default",
"rkn_alamat" varchar(4000) COLLATE "default",
"rkn_telepon" varchar(50) COLLATE "default",
"nama_similarity" float8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for similar_email
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."similar_email";
CREATE TABLE "dwh_procurement"."similar_email" (
"src" int4,
"rkn_id" numeric(19),
"rkn_id_1" numeric(19),
"rkn_npwp" varchar(50) COLLATE "default",
"rkn_npwp_1" varchar(50) COLLATE "default",
"rkn_email" varchar(1000) COLLATE "default",
"rkn_email_1" varchar(1000) COLLATE "default",
"email_similarity" float8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for similar_email_uq
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."similar_email_uq";
CREATE TABLE "dwh_procurement"."similar_email_uq" (
"src" int4,
"rkn_id" numeric(19),
"rkn_id_1" numeric(19),
"rkn_npwp" varchar(50) COLLATE "default",
"rkn_npwp_1" varchar(50) COLLATE "default",
"rkn_email" varchar(1000) COLLATE "default",
"rkn_email_1" varchar(1000) COLLATE "default",
"email_similarity" float8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for similar_name
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."similar_name";
CREATE TABLE "dwh_procurement"."similar_name" (
"src" int4,
"rkn_id" numeric(19),
"rkn_id_1" numeric(19),
"rkn_npwp" varchar(50) COLLATE "default",
"rkn_npwp_1" varchar(50) COLLATE "default",
"rkn_nama" varchar(100) COLLATE "default",
"rkn_nama_1" varchar(100) COLLATE "default",
"nama_similarity" float8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for similar_name_uniq
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."similar_name_uniq";
CREATE TABLE "dwh_procurement"."similar_name_uniq" (
"src" int4,
"rkn_id" numeric(19),
"rkn_id_1" numeric(19),
"rkn_npwp" varchar(50) COLLATE "default",
"rkn_npwp_1" varchar(50) COLLATE "default",
"rkn_nama" varchar(100) COLLATE "default",
"rkn_nama_1" varchar(100) COLLATE "default",
"nama_similarity" float8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for similar_name_uq
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."similar_name_uq";
CREATE TABLE "dwh_procurement"."similar_name_uq" (
"src" int4,
"rkn_id" numeric(19),
"rkn_id_1" numeric(19),
"rkn_npwp" varchar(50) COLLATE "default",
"rkn_npwp_1" varchar(50) COLLATE "default",
"rkn_nama" varchar(100) COLLATE "default",
"rkn_nama_1" varchar(100) COLLATE "default",
"nama_similarity" float8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for similar_phone
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."similar_phone";
CREATE TABLE "dwh_procurement"."similar_phone" (
"src" int4,
"rkn_id" numeric(19),
"rkn_id_1" numeric(19),
"rkn_npwp" varchar(50) COLLATE "default",
"rkn_npwp_1" varchar(50) COLLATE "default",
"rkn_telepon" varchar(50) COLLATE "default",
"rkn_telepon_1" varchar(50) COLLATE "default",
"phone_similarity" float8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Table structure for similar_phone_uq
-- ----------------------------
DROP TABLE IF EXISTS "dwh_procurement"."similar_phone_uq";
CREATE TABLE "dwh_procurement"."similar_phone_uq" (
"src" int4,
"rkn_id" numeric(19),
"rkn_id_1" numeric(19),
"rkn_npwp" varchar(50) COLLATE "default",
"rkn_npwp_1" varchar(50) COLLATE "default",
"rkn_telepon" varchar(50) COLLATE "default",
"rkn_telepon_1" varchar(50) COLLATE "default",
"phone_similarity" float8
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------

-- ----------------------------
-- Indexes structure for table fraud_report_02
-- ----------------------------
CREATE INDEX "idx_fraud_report_02_1" ON "dwh_procurement"."fraud_report_02" USING btree ("nama_kldi", "nama_propinsi");
CREATE INDEX "idx_fraud_report_02_2" ON "dwh_procurement"."fraud_report_02" USING btree ("nama_kldi");
CREATE INDEX "idx_fraud_report_02_3" ON "dwh_procurement"."fraud_report_02" USING btree ("nama_penyedia");

-- ----------------------------
-- Primary Key structure for table master_pemilihan
-- ----------------------------
ALTER TABLE "dwh_procurement"."master_pemilihan" ADD PRIMARY KEY ("no");

--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6 (Debian 10.6-1.pgdg80+1)
-- Dumped by pg_dump version 10.11 (Ubuntu 10.11-1.pgdg16.04+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'SQL_ASCII';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: jabfung; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA jabfung;


ALTER SCHEMA jabfung OWNER TO postgres;

--
-- Name: adjustment_jabfung_ppbj_id_seq; Type: SEQUENCE; Schema: jabfung; Owner: postgres
--

CREATE SEQUENCE jabfung.adjustment_jabfung_ppbj_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE jabfung.adjustment_jabfung_ppbj_id_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: adjustment_jabfung_ppbj; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.adjustment_jabfung_ppbj (
    id bigint DEFAULT nextval('jabfung.adjustment_jabfung_ppbj_id_seq'::regclass) NOT NULL,
    pertama integer,
    muda integer,
    madya integer,
    tanggal_update timestamp(6) without time zone,
    tahun integer
);


ALTER TABLE jabfung.adjustment_jabfung_ppbj OWNER TO postgres;

--
-- Name: adjustment_jabfung_ppbj_original_id_seq; Type: SEQUENCE; Schema: jabfung; Owner: postgres
--

CREATE SEQUENCE jabfung.adjustment_jabfung_ppbj_original_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE jabfung.adjustment_jabfung_ppbj_original_id_seq OWNER TO postgres;

--
-- Name: adjustment_jabfung_ppbj_original; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.adjustment_jabfung_ppbj_original (
    id bigint DEFAULT nextval('jabfung.adjustment_jabfung_ppbj_original_id_seq'::regclass) NOT NULL,
    pertama integer,
    muda integer,
    madya integer,
    tanggal_update timestamp(6) without time zone,
    tahun integer
);


ALTER TABLE jabfung.adjustment_jabfung_ppbj_original OWNER TO postgres;

--
-- Name: adjustment_pemegang_sertifikat_kompetensi_id_seq; Type: SEQUENCE; Schema: jabfung; Owner: postgres
--

CREATE SEQUENCE jabfung.adjustment_pemegang_sertifikat_kompetensi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE jabfung.adjustment_pemegang_sertifikat_kompetensi_id_seq OWNER TO postgres;

--
-- Name: adjustment_pemegang_sertifikat_kompetensi; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.adjustment_pemegang_sertifikat_kompetensi (
    id bigint DEFAULT nextval('jabfung.adjustment_pemegang_sertifikat_kompetensi_id_seq'::regclass) NOT NULL,
    muda integer,
    madya integer,
    pertama integer,
    tanggal_update timestamp(6) without time zone,
    tahun integer
);


ALTER TABLE jabfung.adjustment_pemegang_sertifikat_kompetensi OWNER TO postgres;

--
-- Name: adjustment_pemegang_sertifikat_kompetensi_original_id_seq; Type: SEQUENCE; Schema: jabfung; Owner: postgres
--

CREATE SEQUENCE jabfung.adjustment_pemegang_sertifikat_kompetensi_original_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE jabfung.adjustment_pemegang_sertifikat_kompetensi_original_id_seq OWNER TO postgres;

--
-- Name: adjustment_pemegang_sertifikat_kompetensi_original; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.adjustment_pemegang_sertifikat_kompetensi_original (
    id bigint DEFAULT nextval('jabfung.adjustment_pemegang_sertifikat_kompetensi_original_id_seq'::regclass) NOT NULL,
    muda integer,
    madya integer,
    pertama integer,
    tanggal_update timestamp(6) without time zone,
    tahun integer
);


ALTER TABLE jabfung.adjustment_pemegang_sertifikat_kompetensi_original OWNER TO postgres;

--
-- Name: adjustment_pemegang_sertifikat_okupasi_id_seq; Type: SEQUENCE; Schema: jabfung; Owner: postgres
--

CREATE SEQUENCE jabfung.adjustment_pemegang_sertifikat_okupasi_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE jabfung.adjustment_pemegang_sertifikat_okupasi_id_seq OWNER TO postgres;

--
-- Name: adjustment_pemegang_sertifikat_okupasi; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.adjustment_pemegang_sertifikat_okupasi (
    id bigint DEFAULT nextval('jabfung.adjustment_pemegang_sertifikat_okupasi_id_seq'::regclass) NOT NULL,
    pokja_pemilihan integer,
    pejabat_pengadaan integer,
    pejabat_pembuat_komitmen integer,
    tanggal_update timestamp(6) without time zone,
    tahun integer
);


ALTER TABLE jabfung.adjustment_pemegang_sertifikat_okupasi OWNER TO postgres;

--
-- Name: adjustment_pemegang_sertifikat_okupasi_original_id_seq; Type: SEQUENCE; Schema: jabfung; Owner: postgres
--

CREATE SEQUENCE jabfung.adjustment_pemegang_sertifikat_okupasi_original_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE jabfung.adjustment_pemegang_sertifikat_okupasi_original_id_seq OWNER TO postgres;

--
-- Name: adjustment_pemegang_sertifikat_okupasi_original; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.adjustment_pemegang_sertifikat_okupasi_original (
    id bigint DEFAULT nextval('jabfung.adjustment_pemegang_sertifikat_okupasi_original_id_seq'::regclass) NOT NULL,
    pokja_pemilihan integer,
    pejabat_pengadaan integer,
    pejabat_pembuat_komitmen integer,
    tanggal_update timestamp(6) without time zone,
    tahun integer
);


ALTER TABLE jabfung.adjustment_pemegang_sertifikat_okupasi_original OWNER TO postgres;

--
-- Name: adjustment_sertifikat_dasar_id_seq; Type: SEQUENCE; Schema: jabfung; Owner: postgres
--

CREATE SEQUENCE jabfung.adjustment_sertifikat_dasar_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE jabfung.adjustment_sertifikat_dasar_id_seq OWNER TO postgres;

--
-- Name: adjustment_sertifikat_dasar; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.adjustment_sertifikat_dasar (
    id bigint DEFAULT nextval('jabfung.adjustment_sertifikat_dasar_id_seq'::regclass) NOT NULL,
    total integer,
    tanggal_update timestamp(6) without time zone,
    tahun integer
);


ALTER TABLE jabfung.adjustment_sertifikat_dasar OWNER TO postgres;

--
-- Name: adjustment_sertifikat_dasar_original_id_seq; Type: SEQUENCE; Schema: jabfung; Owner: postgres
--

CREATE SEQUENCE jabfung.adjustment_sertifikat_dasar_original_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE jabfung.adjustment_sertifikat_dasar_original_id_seq OWNER TO postgres;

--
-- Name: adjustment_sertifikat_dasar_original; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.adjustment_sertifikat_dasar_original (
    id bigint DEFAULT nextval('jabfung.adjustment_sertifikat_dasar_original_id_seq'::regclass) NOT NULL,
    total integer,
    tanggal_update timestamp(6) without time zone,
    tahun integer
);


ALTER TABLE jabfung.adjustment_sertifikat_dasar_original OWNER TO postgres;

--
-- Name: jabfung_pbj; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.jabfung_pbj (
    id integer NOT NULL,
    certificate_number character varying(255),
    name character varying(255),
    nip character varying(255),
    institution_id character varying(255),
    institution_name character varying(255),
    satker_id character varying(255),
    satker_name character varying(255),
    issued_date timestamp(6) without time zone,
    level character varying(255),
    rank character varying(255)
);


ALTER TABLE jabfung.jabfung_pbj OWNER TO postgres;

--
-- Name: jabfung_pbj_lama; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.jabfung_pbj_lama (
    id integer,
    nama character varying(255),
    jenjang character varying(255),
    satker character varying(255),
    pangkat_gol character varying(255),
    issued_date timestamp(6) without time zone
);


ALTER TABLE jabfung.jabfung_pbj_lama OWNER TO postgres;

--
-- Name: pemegang_sertifikat; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.pemegang_sertifikat (
    id integer NOT NULL,
    certificate_number character varying(255),
    name character varying(255),
    nip character varying(255),
    institution_id character varying(255),
    institution_name character varying(255),
    satker_id character varying(255),
    satker_name character varying(255),
    issued_date timestamp(6) without time zone,
    rank character varying(255)
);


ALTER TABLE jabfung.pemegang_sertifikat OWNER TO postgres;

--
-- Name: pemegang_sertifikat_lama; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.pemegang_sertifikat_lama (
    id integer,
    certificate_number character varying(255),
    name character varying(255),
    nip character varying(255),
    institution character varying(255),
    issued_date timestamp(6) without time zone,
    jenjang character varying(255)
);


ALTER TABLE jabfung.pemegang_sertifikat_lama OWNER TO postgres;

--
-- Name: sertifikat_kompetensi; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.sertifikat_kompetensi (
    id integer,
    certificate_number character varying(255),
    name character varying(255),
    nip character varying(255),
    institution_id character varying(255),
    institution_name character varying(255),
    satker_id character varying(255),
    satker_name character varying(255),
    issued_date timestamp(6) without time zone,
    level character varying(255),
    rank character varying(255)
);


ALTER TABLE jabfung.sertifikat_kompetensi OWNER TO postgres;

--
-- Name: sertifikat_okupasi; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.sertifikat_okupasi (
    id integer NOT NULL,
    certificate_number character varying(255),
    name character varying(255),
    nip character varying(255),
    institution_id character varying(255),
    institution_name character varying(255),
    satker_id character varying(255),
    satker_name character varying(255),
    issued_date timestamp(6) without time zone,
    type character varying(255),
    rank character varying(255)
);


ALTER TABLE jabfung.sertifikat_okupasi OWNER TO postgres;

--
-- Name: sertifikat_okupasi_lama; Type: TABLE; Schema: jabfung; Owner: postgres
--

CREATE TABLE jabfung.sertifikat_okupasi_lama (
    id integer,
    certificate_number character varying(255),
    name character varying(255),
    nip character varying(255),
    institution_id character varying(255),
    institution_name character varying(255),
    satker_id character varying(255),
    satker_name character varying(255),
    issued_date timestamp(6) without time zone,
    type character varying(255)
);


ALTER TABLE jabfung.sertifikat_okupasi_lama OWNER TO postgres;

--
-- Name: adjustment_jabfung_ppbj_original adjustment_jabfung_ppbj_original_pkey; Type: CONSTRAINT; Schema: jabfung; Owner: postgres
--

ALTER TABLE ONLY jabfung.adjustment_jabfung_ppbj_original
    ADD CONSTRAINT adjustment_jabfung_ppbj_original_pkey PRIMARY KEY (id);


--
-- Name: adjustment_jabfung_ppbj adjustment_jabfung_ppbj_pkey; Type: CONSTRAINT; Schema: jabfung; Owner: postgres
--

ALTER TABLE ONLY jabfung.adjustment_jabfung_ppbj
    ADD CONSTRAINT adjustment_jabfung_ppbj_pkey PRIMARY KEY (id);


--
-- Name: adjustment_pemegang_sertifikat_kompetensi_original adjustment_pemegang_sertifikat_kompetensi_original_pkey; Type: CONSTRAINT; Schema: jabfung; Owner: postgres
--

ALTER TABLE ONLY jabfung.adjustment_pemegang_sertifikat_kompetensi_original
    ADD CONSTRAINT adjustment_pemegang_sertifikat_kompetensi_original_pkey PRIMARY KEY (id);


--
-- Name: adjustment_pemegang_sertifikat_kompetensi adjustment_pemegang_sertifikat_kompetensi_pkey; Type: CONSTRAINT; Schema: jabfung; Owner: postgres
--

ALTER TABLE ONLY jabfung.adjustment_pemegang_sertifikat_kompetensi
    ADD CONSTRAINT adjustment_pemegang_sertifikat_kompetensi_pkey PRIMARY KEY (id);


--
-- Name: adjustment_pemegang_sertifikat_okupasi_original adjustment_pemegang_sertifikat_okupasi_original_pkey; Type: CONSTRAINT; Schema: jabfung; Owner: postgres
--

ALTER TABLE ONLY jabfung.adjustment_pemegang_sertifikat_okupasi_original
    ADD CONSTRAINT adjustment_pemegang_sertifikat_okupasi_original_pkey PRIMARY KEY (id);


--
-- Name: adjustment_pemegang_sertifikat_okupasi adjustment_pemegang_sertifikat_okupasi_pkey; Type: CONSTRAINT; Schema: jabfung; Owner: postgres
--

ALTER TABLE ONLY jabfung.adjustment_pemegang_sertifikat_okupasi
    ADD CONSTRAINT adjustment_pemegang_sertifikat_okupasi_pkey PRIMARY KEY (id);


--
-- Name: adjustment_sertifikat_dasar_original adjustment_sertifikat_dasar_original_pkey; Type: CONSTRAINT; Schema: jabfung; Owner: postgres
--

ALTER TABLE ONLY jabfung.adjustment_sertifikat_dasar_original
    ADD CONSTRAINT adjustment_sertifikat_dasar_original_pkey PRIMARY KEY (id);


--
-- Name: adjustment_sertifikat_dasar adjustment_sertifikat_dasar_pkey; Type: CONSTRAINT; Schema: jabfung; Owner: postgres
--

ALTER TABLE ONLY jabfung.adjustment_sertifikat_dasar
    ADD CONSTRAINT adjustment_sertifikat_dasar_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--


CREATE SCHEMA IF NOT EXISTS smartreport;

CREATE SEQUENCE smartreport.e_tendering_summary_id_seq
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

create table smartreport.e_tendering_summary (
  id bigint NOT NULL DEFAULT nextval('smartreport.e_tendering_summary_id_seq'::regclass),
  tahun integer NOT NULL,
  jumlah_lelang integer,
  pagu_lelang double precision,
  jumlah_lelang_selesai integer,
  pagu_lelang_selesai double precision,
  nilai_hasil_lelang double precision,
  selisih_pagu_hasil_lelang double precision,
  selisih_hps_hasil_lelang double precision,
  jumlah_ppk integer,
  jumlah_panitia integer,
  penyedia_terblacklist integer,
  penyedia_terdaftar integer,
  penyedia_tertolak integer,
  penyedia_terverifikasi integer,
  penyedia_terkoreksi integer
);

ALTER TABLE smartreport.e_tendering_summary ADD CONSTRAINT "smartreport.e_tendering_summary_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE smartreport.e_tendering_detail_id_seq
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

create table smartreport.e_tendering_detail (
  id bigint NOT NULL DEFAULT nextval('smartreport.e_tendering_detail_id_seq'::regclass),
  nama_lpse character varying (255),
  kode_lpse numeric,
  tahun integer NOT NULL,
  jumlah_lelang integer,
  pagu_lelang double precision,
  jumlah_lelang_selesai integer,
  pagu_lelang_selesai double precision,
  nilai_hasil_lelang double precision,
  selisih_pagu_hasil_lelang double precision,
  selisih_hps_hasil_lelang double precision,
  jumlah_ppk integer,
  jumlah_panitia integer,
  penyedia_terblacklist integer,
  penyedia_terdaftar integer,
  penyedia_tertolak integer,
  penyedia_terverifikasi integer,
  penyedia_terkoreksi integer
);

ALTER TABLE smartreport.e_tendering_detail ADD CONSTRAINT "smartreport.e_tendering_detail_pkey" PRIMARY KEY ("id");


alter table smartreport.e_tendering_summary
  add column selisih_pagu_hasil_persen double precision,
  add column selisih_hps_hasil_persen double precision;

alter table smartreport.e_tendering_detail
  add column selisih_pagu_hasil_persen double precision,
  add column selisih_hps_hasil_persen double precision;


CREATE SEQUENCE smartreport.e_purchasing_id_seq
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

create table smartreport.e_purchasing (
  id bigint NOT NULL DEFAULT nextval('smartreport.e_purchasing_id_seq'::regclass),
  tahun integer NOT NULL,
  jumlah_paket integer,
  nilai_transaksi double precision,
  jumlah_kategori_komoditas integer,
  jumlah_penyedia integer,
  jumlah_produk integer
);

ALTER TABLE smartreport.e_purchasing ADD CONSTRAINT "smartreport.e_purchasing_pkey" PRIMARY KEY ("id");


CREATE SEQUENCE smartreport.non_e_tendering_summary_id_seq
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

create table smartreport.non_e_tendering_summary (
  id bigint NOT NULL DEFAULT nextval('smartreport.non_e_tendering_summary_id_seq'::regclass),
  tahun integer NOT NULL,
  jumlah_paket integer,
  nilai_pagu double precision,
  nilai_transaksi double precision
);

ALTER TABLE smartreport.non_e_tendering_summary ADD CONSTRAINT "smartreport.non_e_tendering_summary_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE smartreport.non_e_tendering_detail_id_seq
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

create table smartreport.non_e_tendering_detail (
  id bigint NOT NULL DEFAULT nextval('smartreport.non_e_tendering_detail_id_seq'::regclass),
  nama_lpse character varying (255),
  kode_lpse numeric,
  tahun integer NOT NULL,
  jumlah_paket integer,
  nilai_pagu double precision,
  nilai_transaksi double precision
);

ALTER TABLE smartreport.non_e_tendering_detail ADD CONSTRAINT "smartreport.non_e_tendering_detail_pkey" PRIMARY KEY ("id");

CREATE SEQUENCE smartreport.e_tendering_original_summary_id_seq
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

create table smartreport.e_tendering_original_summary (
  id bigint NOT NULL DEFAULT nextval('smartreport.e_tendering_original_summary_id_seq'::regclass),
  tahun integer NOT NULL,
  jumlah_lelang integer,
  pagu_lelang double precision,
  jumlah_lelang_selesai integer,
  pagu_lelang_selesai double precision,
  nilai_hasil_lelang double precision,
  selisih_pagu_hasil_lelang double precision,
  selisih_hps_hasil_lelang double precision,
  jumlah_ppk integer,
  jumlah_panitia integer,
  penyedia_terblacklist integer,
  penyedia_terdaftar integer,
  penyedia_tertolak integer,
  penyedia_terverifikasi integer,
  penyedia_terkoreksi integer
);

ALTER TABLE smartreport.e_tendering_original_summary ADD CONSTRAINT "smartreport.e_tendering_original_summary_pkey" PRIMARY KEY ("id");

alter table smartreport.e_tendering_original_summary
  add column selisih_pagu_hasil_persen double precision,
  add column selisih_hps_hasil_persen double precision;

ALTER TABLE smartreport.e_tendering_summary ADD COLUMN tanggal_update timestamp(6);
ALTER TABLE smartreport.e_tendering_original_summary ADD COLUMN tanggal_update timestamp(6);

CREATE SEQUENCE smartreport.e_purchasing_original_id_seq
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

create table smartreport.e_purchasing_original (
  id bigint NOT NULL DEFAULT nextval('smartreport.e_purchasing_original_id_seq'::regclass),
  tahun integer NOT NULL,
  jumlah_paket integer,
  nilai_transaksi double precision,
  jumlah_kategori_komoditas integer,
  jumlah_penyedia integer,
  jumlah_produk integer
);

ALTER TABLE smartreport.e_purchasing_original ADD CONSTRAINT "smartreport.e_purchasing_original_pkey" PRIMARY KEY ("id");

ALTER TABLE smartreport.e_purchasing ADD COLUMN tanggal_update timestamp(6);
ALTER TABLE smartreport.e_purchasing_original ADD COLUMN tanggal_update timestamp(6);

CREATE SEQUENCE smartreport.non_e_tendering_original_summary_id_seq
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

create table smartreport.non_e_tendering_original_summary (
  id bigint NOT NULL DEFAULT nextval('smartreport.non_e_tendering_original_summary_id_seq'::regclass),
  tahun integer NOT NULL,
  jumlah_paket integer,
  nilai_pagu double precision,
  nilai_transaksi double precision
);

ALTER TABLE smartreport.non_e_tendering_original_summary ADD CONSTRAINT "smartreport.non_e_tendering_original_summary_pkey" PRIMARY KEY ("id");

ALTER TABLE smartreport.non_e_tendering_summary ADD COLUMN tanggal_update timestamp(6);
ALTER TABLE smartreport.non_e_tendering_original_summary ADD COLUMN tanggal_update timestamp(6);


CREATE SEQUENCE "smartreport"."e_purchasing_detail_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for e_purchasing
-- ----------------------------
CREATE TABLE "smartreport"."e_purchasing_detail" (
  "id" int8 NOT NULL DEFAULT nextval('"smartreport".e_purchasing_detail_id_seq'::regclass),
  "tahun" int4 NOT NULL,
  "kode_kldi" character varying(50),
  "nama_kldi" character varying(255),
  "kode_satker" character varying(50),
  "nama_satker" character varying(255),
  "jumlah_paket" int4,
  "nilai_transaksi" float8,
  "jumlah_kategori_komoditas" int4,
  "jumlah_penyedia" int4,
  "jumlah_produk" int4
)
;

-- ----------------------------
-- Primary Key structure for table e_purchasing
-- ----------------------------
ALTER TABLE "smartreport"."e_purchasing_detail" ADD CONSTRAINT "smartreport.e_purchasing_detail_pkey" PRIMARY KEY ("id");



-- ----------------------------
-- Sequence structure for role_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."role_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for role_permission_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."role_permission_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;


-- ----------------------------
-- Sequence structure for token_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."token_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for user_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."user_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Sequence structure for user_role_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."user_role_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for kldi
-- ----------------------------

CREATE TABLE "public"."kldi" (
  "id" varchar(30) NOT NULL COLLATE "pg_catalog"."default",
  "nama_instansi" varchar(255) COLLATE "pg_catalog"."default",
  "alamat" varchar(255) COLLATE "pg_catalog"."default",
  "jenis" varchar(255) COLLATE "pg_catalog"."default"
)
;

ALTER TABLE "public"."kldi" ADD CONSTRAINT "kldi_id" PRIMARY KEY ("id");

-- ----------------------------
-- Table structure for permission
-- ----------------------------

-- ----------------------------
-- Sequence structure for permission_id_seq
-- ----------------------------
CREATE SEQUENCE "public"."permission_id_seq"
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;


CREATE TABLE "public"."permission" (
  "id" int4 NOT NULL DEFAULT nextval('public.permission_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO "public"."permission" VALUES (1, 'superPermission');
INSERT INTO "public"."permission" VALUES (2, 'adminUserPermission');
INSERT INTO "public"."permission" VALUES (3, 'adminUserInputPermission');
INSERT INTO "public"."permission" VALUES (4, 'endUserPermission');
INSERT INTO "public"."permission" VALUES (5, 'contentPermission');
INSERT INTO "public"."permission" VALUES (6, 'adminContentPermission');
INSERT INTO "public"."permission" VALUES (7, 'adminContentInputPermission');

-- ----------------------------
-- Table structure for role
-- ----------------------------
CREATE TABLE "public"."role" (
  "id" int4 NOT NULL DEFAULT nextval('public.role_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default",
  "desc" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
  "created_by" int4,
  "updated_at" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
  "updated_by" int4,
  "deleted_at" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
  "deleted_by" int4
)
;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO "public"."role" VALUES (1, 'Super Admin', 'this is super admin', '2019-08-28 10:28:51.129933', NULL, '2019-08-28 10:28:51.129933', NULL, NULL, NULL);
INSERT INTO "public"."role" VALUES (2, 'Admin K/L', 'this is Admin K/L', '2019-08-28 10:28:51.131777', NULL, '2019-08-28 10:28:51.131777', NULL, NULL, NULL);
INSERT INTO "public"."role" VALUES (3, 'Admin Konten', 'this is admin konten', '2019-08-28 10:28:51.132852', NULL, '2019-08-28 10:28:51.132852', NULL, NULL, NULL);
INSERT INTO "public"."role" VALUES (4, 'Public User', 'this is public user', '2019-08-28 10:28:51.133711', NULL, '2019-08-28 10:28:51.133711', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for role_permission
-- ----------------------------
CREATE TABLE "public"."role_permission" (
  "id" int4 NOT NULL DEFAULT nextval('public.role_permission_id_seq'::regclass),
  "role_id" int4,
  "permission_id" int4
)
;

-- ----------------------------
-- Records of role_permission
-- ----------------------------
INSERT INTO "public"."role_permission" VALUES (2, 2, 2);
INSERT INTO "public"."role_permission" VALUES (1, 1, 1);
INSERT INTO "public"."role_permission" VALUES (3, 2, 3);
INSERT INTO "public"."role_permission" VALUES (4, 1, 2);
INSERT INTO "public"."role_permission" VALUES (5, 3, 5);
INSERT INTO "public"."role_permission" VALUES (6, 3, 6);
INSERT INTO "public"."role_permission" VALUES (7, 3, 7);
INSERT INTO "public"."role_permission" VALUES (8, 4, 4);

-- ----------------------------
-- Table structure for token
-- ----------------------------
CREATE TABLE "public"."token" (
  "id" int4 NOT NULL DEFAULT nextval('public.token_id_seq'::regclass),
  "email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "token" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "expired_date" timestamp(6),
  "created_by" int4,
  "created_at" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
  "updated_by" int4,
  "updated_at" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
  "deleted_by" int4,
  "deleted_at" timestamp(6) DEFAULT CURRENT_TIMESTAMP
)
;

-- ----------------------------
-- Table structure for user
-- ----------------------------
CREATE TABLE "public"."user" (
  "id" int4 NOT NULL DEFAULT nextval('public.user_id_seq'::regclass),
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "email" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "password" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "phone" varchar(32) COLLATE "pg_catalog"."default" NOT NULL,
  "created_by" int4,
  "created_at" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
  "updated_by" int4,
  "updated_at" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
  "deleted_by" int4,
  "deleted_at" timestamp(6) DEFAULT CURRENT_TIMESTAMP,
  "npwp" varchar(30) COLLATE "pg_catalog"."default",
  "nip" varchar(255) COLLATE "pg_catalog"."default",
  "approve" int4,
  "upload_file" varchar(255) COLLATE "pg_catalog"."default",
  "kldi_id" char(20) COLLATE "pg_catalog"."default",
  "avatar" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
CREATE TABLE "public"."user_role" (
  "id" int4 NOT NULL DEFAULT nextval('public.user_role_id_seq'::regclass),
  "user_id" int4,
  "role_id" int4
)
;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO "public"."user_role" VALUES (1, 1, 1);
INSERT INTO "public"."user_role" VALUES (2, 2, 1);

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------

ALTER SEQUENCE "public"."permission_id_seq"
OWNED BY "public"."permission"."id";
SELECT setval('"public"."permission_id_seq"', 2, false);
ALTER SEQUENCE "public"."role_id_seq"
OWNED BY "public"."role"."id";
SELECT setval('"public"."role_id_seq"', 2, false);
ALTER SEQUENCE "public"."role_permission_id_seq"
OWNED BY "public"."role_permission"."id";
SELECT setval('"public"."role_permission_id_seq"', 2, false);
SELECT setval('"public"."token_id_seq"', 19, true);
ALTER SEQUENCE "public"."user_id_seq"
OWNED BY "public"."user"."id";
SELECT setval('"public"."user_id_seq"', 11, true);
ALTER SEQUENCE "public"."user_role_id_seq"
OWNED BY "public"."user_role"."id";
SELECT setval('"public"."user_role_id_seq"', 7, true);

-- ----------------------------
-- Remove HTML Tags to String for Landing Page Content
-- ----------------------------
CREATE OR REPLACE FUNCTION strip_tags(TEXT) RETURNS TEXT AS $$
    SELECT regexp_replace($1, '<[^>]*>', '', 'g')
$$ LANGUAGE SQL;

-- ----------------------------
-- Primary Key structure for table permission
-- ----------------------------
ALTER TABLE "public"."permission" ADD CONSTRAINT "permission_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table role
-- ----------------------------
ALTER TABLE "public"."role" ADD CONSTRAINT "role_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table role_permission
-- ----------------------------
ALTER TABLE "public"."role_permission" ADD CONSTRAINT "role_permission_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Uniques structure for table token
-- ----------------------------
ALTER TABLE "public"."token" ADD CONSTRAINT "token_email_uindex" UNIQUE ("email");

-- ----------------------------
-- Primary Key structure for table token
-- ----------------------------
ALTER TABLE "public"."token" ADD CONSTRAINT "token_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table user
-- ----------------------------
CREATE UNIQUE INDEX "user_email_uindex" ON "public"."user" USING btree (
  "email" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table user
-- ----------------------------
ALTER TABLE "public"."user" ADD CONSTRAINT "user_pkey" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table user_role
-- ----------------------------
ALTER TABLE "public"."user_role" ADD CONSTRAINT "user_role_pkey" PRIMARY KEY ("id");

ALTER TABLE "public"."role" RENAME COLUMN "desc" TO "description";

ALTER TABLE "public"."token" ADD COLUMN "token_type" character varying(20) DEFAULT 'REGISTRATION';

-- ----------------------------
-- Drop Uniques structure for table token
-- ----------------------------
ALTER TABLE "public"."token" DROP CONSTRAINT "token_email_uindex";

INSERT INTO "public"."user" VALUES ('1', 'superUser', 'superuser@gmail.com', '1579e2eb6e13b9db210285344ec640737acf0eabd14c0dabd8ee2fb8be2136a2', '123456', '1', '2019-09-02 15:13:10.796', null, null, null, null, null, null, '4', null, null);
SELECT setval('"public"."user_id_seq"', 10, true);
SELECT setval('"public"."user_role_id_seq"', 10, true);




DROP VIEW IF EXISTS "public"."page_10";

CREATE OR REPLACE VIEW "public"."page_10" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'harga_nilai_pesanan'::text) THEN pd.value
	ELSE 0
	END
) AS harga_nilai_pesanan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_harga_pesanan'::text) THEN pd.value
	ELSE 0
	END
) AS paket_harga_pesanan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'komoditas_harga_pesanan'::text) THEN pd.value
	ELSE 0
	END
) AS komoditas_harga_pesanan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'produk_harga_pesanan'::text) THEN pd.value
	ELSE 0
	END
) AS produk_harga_pesanan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_harga_pesanan'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_harga_pesanan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'komoditas_nasional'::text) THEN pd.value
	ELSE 0
	END
) AS komoditas_nasional, 
sum(
	CASE
	WHEN ((pd.header)::text = 'produk_nasional'::text) THEN pd.value
	ELSE 0
	END
) AS produk_nasional, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_nasional'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_nasional, 
sum(
	CASE
	WHEN ((pd.header)::text = 'kementrian_sektoral_lokal'::text) THEN pd.value
	ELSE 0
	END
) AS kementrian_sektoral_lokal, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pemda_sektoral_lokal'::text) THEN pd.value
	ELSE 0
	END
) AS pemda_sektoral_lokal, 
sum(
	CASE
	WHEN ((pd.header)::text = 'komoditas_sektoral_lokal'::text) THEN pd.value
	ELSE 0
	END
) AS komoditas_sektoral_lokal, 
sum(
	CASE
	WHEN ((pd.header)::text = 'produk_sektoral_lokal'::text) THEN pd.value
	ELSE 0
	END
) AS produk_sektoral_lokal, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_sektoral_lokal'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_sektoral_lokal, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_1'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_2'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_3'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_4'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_5'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_6'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_7'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_8'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_9'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_10'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_10, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_11'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_11, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_12'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_12, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_13'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_13, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wmp_list_item_14'::text) THEN pd.value
	ELSE 0
	END
) AS wmp_list_item_14, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_name_1'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_name_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_paket_1'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_paket_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_harga_1'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_harga_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_name_2'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_name_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_paket_2'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_paket_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_harga_2'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_harga_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_name_3'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_name_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_paket_3'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_paket_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_harga_3'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_harga_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_name_4'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_name_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_paket_4'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_paket_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_harga_4'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_harga_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_name_5'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_name_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_paket_5'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_paket_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_harga_5'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_harga_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_name_6'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_name_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_paket_6'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_paket_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_harga_6'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_harga_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_name_7'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_name_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_paket_7'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_paket_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_harga_7'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_harga_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_name_8'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_name_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_paket_8'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_paket_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_harga_8'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_harga_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_name_9'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_name_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_paket_9'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_paket_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_harga_9'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_harga_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_name_10'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_name_10, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_paket_10'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_paket_10, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pten_komoditas_harga_10'::text) THEN pd.value
	ELSE 0
	END
) AS pten_komoditas_harga_10
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;


DROP VIEW IF EXISTS "public"."page_11";

CREATE OR REPLACE VIEW "public"."page_11" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'st_januari'::text) THEN pd.value
	ELSE 0
	END
) AS st_januari, 
sum(
	CASE
	WHEN ((pd.header)::text = 'srsp_januari'::text) THEN pd.value
	ELSE 0
	END
) AS srsp_januari, 
sum(
	CASE
	WHEN ((pd.header)::text = 'st_februari'::text) THEN pd.value
	ELSE 0
	END
) AS st_februari, 
sum(
	CASE
	WHEN ((pd.header)::text = 'srsp_februari'::text) THEN pd.value
	ELSE 0
	END
) AS srsp_februari, 
sum(
	CASE
	WHEN ((pd.header)::text = 'st_maret'::text) THEN pd.value
	ELSE 0
	END
) AS st_maret, 
sum(
	CASE
	WHEN ((pd.header)::text = 'srsp_maret'::text) THEN pd.value
	ELSE 0
	END
) AS srsp_maret, 
sum(
	CASE
	WHEN ((pd.header)::text = 'st_april'::text) THEN pd.value
	ELSE 0
	END
) AS st_april, 
sum(
	CASE
	WHEN ((pd.header)::text = 'srsp_april'::text) THEN pd.value
	ELSE 0
	END
) AS srsp_april, 
sum(
	CASE
	WHEN ((pd.header)::text = 'st_mei'::text) THEN pd.value
	ELSE 0
	END
) AS st_mei, 
sum(
	CASE
	WHEN ((pd.header)::text = 'srsp_mei'::text) THEN pd.value
	ELSE 0
	END
) AS srsp_mei, 
sum(
	CASE
	WHEN ((pd.header)::text = 'st_juni'::text) THEN pd.value
	ELSE 0
	END
) AS st_juni, 
sum(
	CASE
	WHEN ((pd.header)::text = 'srsp_juni'::text) THEN pd.value
	ELSE 0
	END
) AS srsp_juni, 
sum(
	CASE
	WHEN ((pd.header)::text = 'st_juli'::text) THEN pd.value
	ELSE 0
	END
) AS st_juli, 
sum(
	CASE
	WHEN ((pd.header)::text = 'srsp_juli'::text) THEN pd.value
	ELSE 0
	END
) AS srsp_juli, 
sum(
	CASE
	WHEN ((pd.header)::text = 'st_agustus'::text) THEN pd.value
	ELSE 0
	END
) AS st_agustus, 
sum(
	CASE
	WHEN ((pd.header)::text = 'srsp_agustus'::text) THEN pd.value
	ELSE 0
	END
) AS srsp_agustus, 
sum(
	CASE
	WHEN ((pd.header)::text = 'st_september'::text) THEN pd.value
	ELSE 0
	END
) AS st_september, 
sum(
	CASE
	WHEN ((pd.header)::text = 'srsp_september'::text) THEN pd.value
	ELSE 0
	END
) AS srsp_september, 
sum(
	CASE
	WHEN ((pd.header)::text = 'st_oktober'::text) THEN pd.value
	ELSE 0
	END
) AS st_oktober, 
sum(
	CASE
	WHEN ((pd.header)::text = 'srsp_oktober'::text) THEN pd.value
	ELSE 0
	END
) AS srsp_oktober, 
sum(
	CASE
	WHEN ((pd.header)::text = 'st_november'::text) THEN pd.value
	ELSE 0
	END
) AS st_november, 
sum(
	CASE
	WHEN ((pd.header)::text = 'srsp_november'::text) THEN pd.value
	ELSE 0
	END
) AS srsp_november, 
sum(
	CASE
	WHEN ((pd.header)::text = 'st_desember'::text) THEN pd.value
	ELSE 0
	END
) AS st_desember, 
sum(
	CASE
	WHEN ((pd.header)::text = 'srsp_desember'::text) THEN pd.value
	ELSE 0
	END
) AS srsp_desember, 
sum(
	CASE
	WHEN ((pd.header)::text = 'target_januari'::text) THEN pd.value
	ELSE 0
	END
) AS target_januari, 
sum(
	CASE
	WHEN ((pd.header)::text = 'realisasi_januari'::text) THEN pd.value
	ELSE 0
	END
) AS realisasi_januari, 
sum(
	CASE
	WHEN ((pd.header)::text = 'target_februari'::text) THEN pd.value
	ELSE 0
	END
) AS target_februari, 
sum(
	CASE
	WHEN ((pd.header)::text = 'realisasi_februari'::text) THEN pd.value
	ELSE 0
	END
) AS realisasi_februari, 
sum(
	CASE
	WHEN ((pd.header)::text = 'target_maret'::text) THEN pd.value
	ELSE 0
	END
) AS target_maret, 
sum(
	CASE
	WHEN ((pd.header)::text = 'realisasi_maret'::text) THEN pd.value
	ELSE 0
	END
) AS realisasi_maret, 
sum(
	CASE
	WHEN ((pd.header)::text = 'target_april'::text) THEN pd.value
	ELSE 0
	END
) AS target_april, 
sum(
	CASE
	WHEN ((pd.header)::text = 'realisasi_april'::text) THEN pd.value
	ELSE 0
	END
) AS realisasi_april, 
sum(
	CASE
	WHEN ((pd.header)::text = 'target_mei'::text) THEN pd.value
	ELSE 0
	END
) AS target_mei, 
sum(
	CASE
	WHEN ((pd.header)::text = 'realisasi_mei'::text) THEN pd.value
	ELSE 0
	END
) AS realisasi_mei, 
sum(
	CASE
	WHEN ((pd.header)::text = 'target_juni'::text) THEN pd.value
	ELSE 0
	END
) AS target_juni, 
sum(
	CASE
	WHEN ((pd.header)::text = 'realisasi_juni'::text) THEN pd.value
	ELSE 0
	END
) AS realisasi_juni, 
sum(
	CASE
	WHEN ((pd.header)::text = 'target_juli'::text) THEN pd.value
	ELSE 0
	END
) AS target_juli, 
sum(
	CASE
	WHEN ((pd.header)::text = 'realisasi_juli'::text) THEN pd.value
	ELSE 0
	END
) AS realisasi_juli, 
sum(
	CASE
	WHEN ((pd.header)::text = 'target_agustus'::text) THEN pd.value
	ELSE 0
	END
) AS target_agustus, 
sum(
	CASE
	WHEN ((pd.header)::text = 'realisasi_agustus'::text) THEN pd.value
	ELSE 0
	END
) AS realisasi_agustus, 
sum(
	CASE
	WHEN ((pd.header)::text = 'target_september'::text) THEN pd.value
	ELSE 0
	END
) AS target_september, 
sum(
	CASE
	WHEN ((pd.header)::text = 'realisasi_september'::text) THEN pd.value
	ELSE 0
	END
) AS realisasi_september, 
sum(
	CASE
	WHEN ((pd.header)::text = 'target_oktober'::text) THEN pd.value
	ELSE 0
	END
) AS target_oktober, 
sum(
	CASE
	WHEN ((pd.header)::text = 'realisasi_oktober'::text) THEN pd.value
	ELSE 0
	END
) AS realisasi_oktober, 
sum(
	CASE
	WHEN ((pd.header)::text = 'target_november'::text) THEN pd.value
	ELSE 0
	END
) AS target_november, 
sum(
	CASE
	WHEN ((pd.header)::text = 'realisasi_november'::text) THEN pd.value
	ELSE 0
	END
) AS realisasi_november, 
sum(
	CASE
	WHEN ((pd.header)::text = 'target_desember'::text) THEN pd.value
	ELSE 0
	END
) AS target_desember, 
sum(
	CASE
	WHEN ((pd.header)::text = 'realisasi_desember'::text) THEN pd.value
	ELSE 0
	END
) AS realisasi_desember
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;


DROP VIEW IF EXISTS "public"."page_13";

CREATE OR REPLACE VIEW "public"."page_13" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'ulp_ukpbj'::text) THEN pd.value
	ELSE 0
	END
) AS ulp_ukpbj, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ulp_ukpbj_permanen_struktural'::text) THEN pd.value
	ELSE 0
	END
) AS ulp_ukpbj_permanen_struktural, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ulp_ukpbj_adhoc'::text) THEN pd.value
	ELSE 0
	END
) AS ulp_ukpbj_adhoc, 
sum(
	CASE
	WHEN ((pd.header)::text = 'kl_ulp_ukpbj_permanen_struktural'::text) THEN pd.value
	ELSE 0
	END
) AS kl_ulp_ukpbj_permanen_struktural, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pd_ulp_ukpbj_permanen_struktural'::text) THEN pd.value
	ELSE 0
	END
) AS pd_ulp_ukpbj_permanen_struktural, 
sum(
	CASE
	WHEN ((pd.header)::text = 'kl_ulp_ukpbj_adhoc'::text) THEN pd.value
	ELSE 0
	END
) AS kl_ulp_ukpbj_adhoc, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pd_ulp_ukpbj_adhoc'::text) THEN pd.value
	ELSE 0
	END
) AS pd_ulp_ukpbj_adhoc, 
sum(
	CASE
	WHEN ((pd.header)::text = 'layanan_pengadaan_elektronik'::text) THEN pd.value
	ELSE 0
	END
) AS layanan_pengadaan_elektronik, 
sum(
	CASE
	WHEN ((pd.header)::text = 'lpse_terstandar'::text) THEN pd.value
	ELSE 0
	END
) AS lpse_terstandar, 
sum(
	CASE
	WHEN ((pd.header)::text = 'lpse_terinstal_spse4'::text) THEN pd.value
	ELSE 0
	END
) AS lpse_terinstal_spse4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'lpse_17standar'::text) THEN pd.value
	ELSE 0
	END
) AS lpse_17standar, 
sum(
	CASE
	WHEN ((pd.header)::text = 'lpse_pengguna_spse4'::text) THEN pd.value
	ELSE 0
	END
) AS lpse_pengguna_spse4
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;


DROP VIEW IF EXISTS "public"."page_14";

CREATE OR REPLACE VIEW "public"."page_14" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'penjabat_fungsional'::text) THEN pd.value
	ELSE 0
	END
) AS penjabat_fungsional, 
sum(
	CASE
	WHEN ((pd.header)::text = 'mpjf_inpassing'::text) THEN pd.value
	ELSE 0
	END
) AS mpjf_inpassing, 
sum(
	CASE
	WHEN ((pd.header)::text = 'mpjf_perpindahan'::text) THEN pd.value
	ELSE 0
	END
) AS mpjf_perpindahan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'mpjf_pengangkatan_pertama'::text) THEN pd.value
	ELSE 0
	END
) AS mpjf_pengangkatan_pertama, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pf_wanita'::text) THEN pd.value
	ELSE 0
	END
) AS pf_wanita, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pf_pria'::text) THEN pd.value
	ELSE 0
	END
) AS pf_pria, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pf_pertama'::text) THEN pd.value
	ELSE 0
	END
) AS pf_pertama, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pf_fungsional_muda'::text) THEN pd.value
	ELSE 0
	END
) AS pf_fungsional_muda, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pf_fungsional_madya'::text) THEN pd.value
	ELSE 0
	END
) AS pf_fungsional_madya, 
sum(
	CASE
	WHEN ((pd.header)::text = 'sjf_aktif'::text) THEN pd.value
	ELSE 0
	END
) AS sjf_aktif, 
sum(
	CASE
	WHEN ((pd.header)::text = 'sjf_pembebasan_sementara'::text) THEN pd.value
	ELSE 0
	END
) AS sjf_pembebasan_sementara, 
sum(
	CASE
	WHEN ((pd.header)::text = 'sjf_pensiun'::text) THEN pd.value
	ELSE 0
	END
) AS sjf_pensiun, 
sum(
	CASE
	WHEN ((pd.header)::text = 'sjf_wafat'::text) THEN pd.value
	ELSE 0
	END
) AS sjf_wafat, 
sum(
	CASE
	WHEN ((pd.header)::text = 'td_dasar_pbjp'::text) THEN pd.value
	ELSE 0
	END
) AS td_dasar_pbjp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'td_fasilitator_pbjp'::text) THEN pd.value
	ELSE 0
	END
) AS td_fasilitator_pbjp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'kpf_muda'::text) THEN pd.value
	ELSE 0
	END
) AS kpf_muda, 
sum(
	CASE
	WHEN ((pd.header)::text = 'kpf_madya'::text) THEN pd.value
	ELSE 0
	END
) AS kpf_madya, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ko_ppk_pbj'::text) THEN pd.value
	ELSE 0
	END
) AS ko_ppk_pbj, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ko_pokja_pemilihan'::text) THEN pd.value
	ELSE 0
	END
) AS ko_pokja_pemilihan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ko_pp'::text) THEN pd.value
	ELSE 0
	END
) AS ko_pp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'k_assesor_kompetensi'::text) THEN pd.value
	ELSE 0
	END
) AS k_assesor_kompetensi, 
sum(
	CASE
	WHEN ((pd.header)::text = 'kfk_pbj'::text) THEN pd.value
	ELSE 0
	END
) AS kfk_pbj, 
sum(
	CASE
	WHEN ((pd.header)::text = 'keterangan_ahli'::text) THEN pd.value
	ELSE 0
	END
) AS keterangan_ahli, 
sum(
	CASE
	WHEN ((pd.header)::text = 'mediator'::text) THEN pd.value
	ELSE 0
	END
) AS mediator, 
sum(
	CASE
	WHEN ((pd.header)::text = 'arbiter'::text) THEN pd.value
	ELSE 0
	END
) AS arbiter
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;


DROP VIEW IF EXISTS "public"."page_15";

CREATE OR REPLACE VIEW "public"."page_15" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'total_peserta_diklat'::text) THEN pd.value
	ELSE 0
	END
) AS total_peserta_diklat, 
sum(
	CASE
	WHEN ((pd.header)::text = 'diklat_pembentukan'::text) THEN pd.value
	ELSE 0
	END
) AS diklat_pembentukan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'diklat_jabfung_pbj_pertama'::text) THEN pd.value
	ELSE 0
	END
) AS diklat_jabfung_pbj_pertama, 
sum(
	CASE
	WHEN ((pd.header)::text = 'diklat_jabfung_muda'::text) THEN pd.value
	ELSE 0
	END
) AS diklat_jabfung_muda, 
sum(
	CASE
	WHEN ((pd.header)::text = 'diklat_jabfung_pbj_muda'::text) THEN pd.value
	ELSE 0
	END
) AS diklat_jabfung_pbj_muda, 
sum(
	CASE
	WHEN ((pd.header)::text = 'lembaga_pendidikan_pelatihan_terakreditasi'::text) THEN pd.value
	ELSE 0
	END
) AS lembaga_pendidikan_pelatihan_terakreditasi, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyelenggara_ujian_sert_dasar'::text) THEN pd.value
	ELSE 0
	END
) AS penyelenggara_ujian_sert_dasar, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyelenggara_ujian_sert_komp'::text) THEN pd.value
	ELSE 0
	END
) AS penyelenggara_ujian_sert_komp
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;


DROP VIEW IF EXISTS "public"."page_16";

CREATE OR REPLACE VIEW "public"."page_16" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'jumlah_layanan_advokasi'::text) THEN pd.value
	ELSE 0
	END
) AS jumlah_layanan_advokasi, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jla_wilayah1'::text) THEN pd.value
	ELSE 0
	END
) AS jla_wilayah1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jla_presentase_wilayah1'::text) THEN pd.value
	ELSE 0
	END
) AS jla_presentase_wilayah1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jla_wilayah2'::text) THEN pd.value
	ELSE 0
	END
) AS jla_wilayah2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jla_presentase_wilayah2'::text) THEN pd.value
	ELSE 0
	END
) AS jla_presentase_wilayah2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'nptla_wilayah1'::text) THEN pd.value
	ELSE 0
	END
) AS nptla_wilayah1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'nptla_presentase_wilayah1'::text) THEN pd.value
	ELSE 0
	END
) AS nptla_presentase_wilayah1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'nptla_wilayah2'::text) THEN pd.value
	ELSE 0
	END
) AS nptla_wilayah2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'nptla_presentase_wilayah2'::text) THEN pd.value
	ELSE 0
	END
) AS nptla_presentase_wilayah2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pengaduan_paket'::text) THEN pd.value
	ELSE 0
	END
) AS pengaduan_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'total_pagu_pengaduan'::text) THEN pd.value
	ELSE 0
	END
) AS total_pagu_pengaduan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pekerjaan_konstruksi'::text) THEN pd.value
	ELSE 0
	END
) AS pekerjaan_konstruksi, 
sum(
	CASE
	WHEN ((pd.header)::text = 'barang_aduan'::text) THEN pd.value
	ELSE 0
	END
) AS barang_aduan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jasa_konsultasi_aduan'::text) THEN pd.value
	ELSE 0
	END
) AS jasa_konsultasi_aduan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jasa_lainnya_aduan'::text) THEN pd.value
	ELSE 0
	END
) AS jasa_lainnya_aduan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'prakontrak_pengaduan'::text) THEN pd.value
	ELSE 0
	END
) AS prakontrak_pengaduan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pascakontrak_pengaduan'::text) THEN pd.value
	ELSE 0
	END
) AS pascakontrak_pengaduan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'kasus_pengadaan'::text) THEN pd.value
	ELSE 0
	END
) AS kasus_pengadaan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'total_pagu_kasus'::text) THEN pd.value
	ELSE 0
	END
) AS total_pagu_kasus, 
sum(
	CASE
	WHEN ((pd.header)::text = 'persentase_penyidikan'::text) THEN pd.value
	ELSE 0
	END
) AS persentase_penyidikan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'persentase_persidangan'::text) THEN pd.value
	ELSE 0
	END
) AS persentase_persidangan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'persentase_penyelidikan'::text) THEN pd.value
	ELSE 0
	END
) AS persentase_penyelidikan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pemohon_kepolisian'::text) THEN pd.value
	ELSE 0
	END
) AS pemohon_kepolisian, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pemohon_kejaksaan'::text) THEN pd.value
	ELSE 0
	END
) AS pemohon_kejaksaan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pemohon_kpk'::text) THEN pd.value
	ELSE 0
	END
) AS pemohon_kpk, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pemohon_kppu'::text) THEN pd.value
	ELSE 0
	END
) AS pemohon_kppu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pemohon_ombudsman'::text) THEN pd.value
	ELSE 0
	END
) AS pemohon_ombudsman, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pemohon_lainnya'::text) THEN pd.value
	ELSE 0
	END
) AS pemohon_lainnya
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;


DROP VIEW IF EXISTS "public"."page_17";

CREATE OR REPLACE VIEW "public"."page_17" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'permasalahan_kontrak'::text) THEN pd.value
	ELSE 0
	END
) AS permasalahan_kontrak, 
sum(
	CASE
	WHEN ((pd.header)::text = 'nilai_pengadaan'::text) THEN pd.value
	ELSE 0
	END
) AS nilai_pengadaan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pemutusan_kontrak'::text) THEN pd.value
	ELSE 0
	END
) AS pemutusan_kontrak, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pemberian_kesempatan'::text) THEN pd.value
	ELSE 0
	END
) AS pemberian_kesempatan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'perubahan_kontrak'::text) THEN pd.value
	ELSE 0
	END
) AS perubahan_kontrak, 
sum(
	CASE
	WHEN ((pd.header)::text = 'uang_muka'::text) THEN pd.value
	ELSE 0
	END
) AS uang_muka, 
sum(
	CASE
	WHEN ((pd.header)::text = 'daftar_hitam'::text) THEN pd.value
	ELSE 0
	END
) AS daftar_hitam, 
sum(
	CASE
	WHEN ((pd.header)::text = 'keadaan_kahar'::text) THEN pd.value
	ELSE 0
	END
) AS keadaan_kahar, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyesuaian_harga'::text) THEN pd.value
	ELSE 0
	END
) AS penyesuaian_harga, 
sum(
	CASE
	WHEN ((pd.header)::text = 'permasalahan_lainnya'::text) THEN pd.value
	ELSE 0
	END
) AS permasalahan_lainnya, 
sum(
	CASE
	WHEN ((pd.header)::text = 'putusan_sidang_pbjp'::text) THEN pd.value
	ELSE 0
	END
) AS putusan_sidang_pbjp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'kasus_pbjp_barang'::text) THEN pd.value
	ELSE 0
	END
) AS kasus_pbjp_barang, 
sum(
	CASE
	WHEN ((pd.header)::text = 'kasus_pbjp_konstruksi'::text) THEN pd.value
	ELSE 0
	END
) AS kasus_pbjp_konstruksi, 
sum(
	CASE
	WHEN ((pd.header)::text = 'kasus_pbjp_konsultasi'::text) THEN pd.value
	ELSE 0
	END
) AS kasus_pbjp_konsultasi, 
sum(
	CASE
	WHEN ((pd.header)::text = 'kasus_jasa_lainnya'::text) THEN pd.value
	ELSE 0
	END
) AS kasus_jasa_lainnya, 
sum(
	CASE
	WHEN ((pd.header)::text = 'perkara_pidana'::text) THEN pd.value
	ELSE 0
	END
) AS perkara_pidana, 
sum(
	CASE
	WHEN ((pd.header)::text = 'perkara_persaingan_usaha'::text) THEN pd.value
	ELSE 0
	END
) AS perkara_persaingan_usaha, 
sum(
	CASE
	WHEN ((pd.header)::text = 'perkara_perdata'::text) THEN pd.value
	ELSE 0
	END
) AS perkara_perdata, 
sum(
	CASE
	WHEN ((pd.header)::text = 'perkara_tun'::text) THEN pd.value
	ELSE 0
	END
) AS perkara_tun
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;


DROP VIEW IF EXISTS "public"."page_19";

CREATE OR REPLACE VIEW "public"."page_19" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'ptds_item_1'::text) THEN pd.value
	ELSE 0
	END
) AS ptds_item_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptds_item_2'::text) THEN pd.value
	ELSE 0
	END
) AS ptds_item_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptds_item_3'::text) THEN pd.value
	ELSE 0
	END
) AS ptds_item_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptds_item_4'::text) THEN pd.value
	ELSE 0
	END
) AS ptds_item_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptds_item_5'::text) THEN pd.value
	ELSE 0
	END
) AS ptds_item_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_teragregasi_lpse'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_teragregasi_lpse, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_dlm_sikap'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_dlm_sikap, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_lolos_prakualifikasi'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_lolos_prakualifikasi, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_aktif_ikut_lelang'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_aktif_ikut_lelang, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_menang'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_menang
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;


DROP VIEW IF EXISTS "public"."page_20";

CREATE OR REPLACE VIEW "public"."page_20" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_1'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_1'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_2'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_2'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_3'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_3'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_4'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_4'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_5'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_5'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_6'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_6'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_7'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_7'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_8'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_8'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_9'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_9'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_10'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_10, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_10'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_10, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_11'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_11, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_11'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_11, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_12'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_12, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_12'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_12, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_13'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_13, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_13'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_13, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_14'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_14, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_14'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_14, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_15'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_15, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_15'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_15, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_16'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_16, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_16'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_16, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_17'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_17, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_17'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_17, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_18'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_18, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_18'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_18, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_19'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_19, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_19'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_19, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_20'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_20, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_20'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_20, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_21'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_21, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_21'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_21, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_22'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_22, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_22'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_22, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_23'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_23, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_23'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_23, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_24'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_24, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_24'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_24, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_25'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_25, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_25'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_25, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_26'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_26, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_26'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_26, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_27'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_27, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_27'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_27, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_28'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_28, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_28'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_28, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_29'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_29, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_29'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_29, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_30'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_30, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_30'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_30, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_31'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_31, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_31'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_31, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_32'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_32, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_32'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_32, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_33'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_33, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_33'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_33, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_name_34'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_name_34, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwenty_item_value_34'::text) THEN pd.value
	ELSE 0
	END
) AS ptwenty_item_value_34
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;


DROP VIEW IF EXISTS "public"."page_21";

CREATE OR REPLACE VIEW "public"."page_21" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_1'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_2'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_3'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_3'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_3'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_4'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_4'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_4'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_5'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_5'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_5'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_6'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_6'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_6'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_7'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_7'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_7'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_8'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_8'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_8'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_9'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_9'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_9'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_10'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_10, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_10'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_10, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_10'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_10, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_11'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_11, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_11'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_11, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_11'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_11, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_12'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_12, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_12'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_12, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_12'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_12, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_13'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_13, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_13'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_13, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_13'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_13, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_14'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_14, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_14'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_14, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_14'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_14, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_name_15'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_name_15, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_kecil_15'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_kecil_15, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pbk_item_non_15'::text) THEN pd.value
	ELSE 0
	END
) AS pbk_item_non_15
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;


DROP VIEW IF EXISTS "public"."page_22";

CREATE OR REPLACE VIEW "public"."page_22" AS 
SELECT pd.procurement_id, pd.flag,
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptt_item_name_1' 
) AS ptt_item_name_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptt_item_value_1'::text) THEN pd.value
	ELSE 0
	END
) AS ptt_item_value_1, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptt_item_name_2' 
) AS ptt_item_name_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptt_item_value_2'::text) THEN pd.value
	ELSE 0
	END
) AS ptt_item_value_2, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptt_item_name_3' 
) AS ptt_item_name_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptt_item_value_3'::text) THEN pd.value
	ELSE 0
	END
) AS ptt_item_value_3, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptt_item_name_4' 
) AS ptt_item_name_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptt_item_value_4'::text) THEN pd.value
	ELSE 0
	END
) AS ptt_item_value_4, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptt_item_name_5' 
) AS ptt_item_name_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptt_item_value_5'::text) THEN pd.value
	ELSE 0
	END
) AS ptt_item_value_5, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptt_item_name_6' 
) AS ptt_item_name_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptt_item_value_6'::text) THEN pd.value
	ELSE 0
	END
) AS ptt_item_value_6, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptt_item_name_7' 
) AS ptt_item_name_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptt_item_value_7'::text) THEN pd.value
	ELSE 0
	END
) AS ptt_item_value_7, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptt_item_name_8' 
) AS ptt_item_name_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptt_item_value_8'::text) THEN pd.value
	ELSE 0
	END
) AS ptt_item_value_8, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptt_item_name_9' 
) AS ptt_item_name_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptt_item_value_9'::text) THEN pd.value
	ELSE 0
	END
) AS ptt_item_value_9, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptt_item_name_10' 
) AS ptt_item_name_10, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptt_item_value_10'::text) THEN pd.value
	ELSE 0
	END
) AS ptt_item_value_10
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;


DROP VIEW IF EXISTS "public"."page_23";

CREATE OR REPLACE VIEW "public"."page_23" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'sipil_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS sipil_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'sipil_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS sipil_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'sipil_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS sipil_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'sipil_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS sipil_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'bangunan_gedung_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS bangunan_gedung_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'bangunan_gedung_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS bangunan_gedung_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'bangunan_gedung_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS bangunan_gedung_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'bangunan_gedung_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS bangunan_gedung_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'engineer_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS engineer_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'engineer_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS engineer_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'engineer_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS engineer_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'engineer_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS engineer_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'elektrikal_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS elektrikal_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'elektrikal_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS elektrikal_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'elektrikal_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS elektrikal_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'elektrikal_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS elektrikal_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'konsultan_spesialis_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS konsultan_spesialis_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'konsultan_spesialis_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS konsultan_spesialis_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'konsultan_spesialis_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS konsultan_spesialis_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'konsultan_spesialis_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS konsultan_spesialis_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pelaksana_spesialis_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS pelaksana_spesialis_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pelaksana_spesialis_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS pelaksana_spesialis_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pelaksana_spesialis_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS pelaksana_spesialis_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pelaksana_spesialis_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS pelaksana_spesialis_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penataan_ruang_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS penataan_ruang_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penataan_ruang_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS penataan_ruang_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penataan_ruang_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS penataan_ruang_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penataan_ruang_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS penataan_ruang_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'arsitektur_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS arsitektur_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'arsitektur_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS arsitektur_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'arsitektur_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS arsitektur_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'arsitektur_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS arsitektur_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'instalasi_mekanik_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS instalasi_mekanik_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'instalasi_mekanik_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS instalasi_mekanik_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'instalasi_mekanik_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS instalasi_mekanik_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'instalasi_mekanik_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS instalasi_mekanik_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'konsultansi_lainnya_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS konsultansi_lainnya_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'konsultansi_lainnya_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS konsultansi_lainnya_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'konsultansi_lainnya_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS konsultansi_lainnya_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'konsultansi_lainnya_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS konsultansi_lainnya_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pelaksana_lainnya_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS pelaksana_lainnya_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pelaksana_lainnya_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS pelaksana_lainnya_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pelaksana_lainnya_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS pelaksana_lainnya_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pelaksana_lainnya_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS pelaksana_lainnya_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'terintegrasi_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS terintegrasi_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'terintegrasi_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS terintegrasi_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'terintegrasi_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS terintegrasi_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'terintegrasi_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS terintegrasi_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pelaksana_keterampilan_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS pelaksana_keterampilan_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pelaksana_keterampilan_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS pelaksana_keterampilan_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pelaksana_keterampilan_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS pelaksana_keterampilan_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pelaksana_keterampilan_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS pelaksana_keterampilan_non_2
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;

DROP VIEW IF EXISTS "public"."page_23_p23_first_list";

CREATE OR REPLACE VIEW "public"."page_23_p23_first_list" AS 
SELECT pd.procurement_id, pd.flag,
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptwentythree_first_name_1' 
) AS ptwentythree_first_name_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwentythree_first_nilai_1'::text) THEN pd.value
	ELSE 0
	END
) AS ptwentythree_first_nilai_1, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptwentythree_first_name_2' 
) AS ptwentythree_first_name_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwentythree_first_nilai_2'::text) THEN pd.value
	ELSE 0
	END
) AS ptwentythree_first_nilai_2, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptwentythree_first_name_3' 
) AS ptwentythree_first_name_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwentythree_first_nilai_3'::text) THEN pd.value
	ELSE 0
	END
) AS ptwentythree_first_nilai_3, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptwentythree_first_name_4' 
) AS ptwentythree_first_name_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwentythree_first_nilai_4'::text) THEN pd.value
	ELSE 0
	END
) AS ptwentythree_first_nilai_4, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptwentythree_first_name_5' 
) AS ptwentythree_first_name_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwentythree_first_nilai_5'::text) THEN pd.value
	ELSE 0
	END
) AS ptwentythree_first_nilai_5, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptwentythree_first_name_6' 
) AS ptwentythree_first_name_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwentythree_first_nilai_6'::text) THEN pd.value
	ELSE 0
	END
) AS ptwentythree_first_nilai_6, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptwentythree_first_name_7' 
) AS ptwentythree_first_name_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwentythree_first_nilai_7'::text) THEN pd.value
	ELSE 0
	END
) AS ptwentythree_first_nilai_7, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptwentythree_first_name_8' 
) AS ptwentythree_first_name_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwentythree_first_nilai_8'::text) THEN pd.value
	ELSE 0
	END
) AS ptwentythree_first_nilai_8, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptwentythree_first_name_9' 
) AS ptwentythree_first_name_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwentythree_first_nilai_9'::text) THEN pd.value
	ELSE 0
	END
) AS ptwentythree_first_nilai_9, 
( 
	SELECT value_text FROM "public"."procurement_data" 
	WHERE procurement_id = pd.procurement_id 
	AND flag = pd.flag 
	AND "header" = 'ptwentythree_first_name_10' 
) AS ptwentythree_first_name_10, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptwentythree_first_nilai_10'::text) THEN pd.value
	ELSE 0
	END
) AS ptwentythree_first_nilai_10
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;


DROP VIEW IF EXISTS "public"."page_24";

CREATE OR REPLACE VIEW "public"."page_24" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'kena_sanksi_blacklist_1'::text) THEN pd.value
	ELSE 0
	END
) AS kena_sanksi_blacklist_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pekerjaan_konstruksi_blacklist'::text) THEN pd.value
	ELSE 0
	END
) AS pekerjaan_konstruksi_blacklist, 
sum(
	CASE
	WHEN ((pd.header)::text = 'barang_blacklist'::text) THEN pd.value
	ELSE 0
	END
) AS barang_blacklist, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jasa_konsultasi_blacklist'::text) THEN pd.value
	ELSE 0
	END
) AS jasa_konsultasi_blacklist
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;


DROP VIEW IF EXISTS "public"."page_25";

CREATE OR REPLACE VIEW "public"."page_25" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_blacklist_hps_kurang_200jt'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_blacklist_hps_kurang_200jt, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_blacklist_hps_200jt'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_blacklist_hps_200jt, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_blacklist_hps_2m'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_blacklist_hps_2m, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_blacklist_hps_5m'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_blacklist_hps_5m, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_blacklist_hps_50m'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_blacklist_hps_50m, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penyedia_blacklist_hps_lebih_50m'::text) THEN pd.value
	ELSE 0
	END
) AS penyedia_blacklist_hps_lebih_50m, 
sum(
	CASE
	WHEN ((pd.header)::text = 'total_pelanggaran_1'::text) THEN pd.value
	ELSE 0
	END
) AS total_pelanggaran_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'deskripsi_pelanggaran_1'::text) THEN pd.value
	ELSE 0
	END
) AS deskripsi_pelanggaran_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'total_pelanggaran_2'::text) THEN pd.value
	ELSE 0
	END
) AS total_pelanggaran_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'deskripsi_pelanggaran_2'::text) THEN pd.value
	ELSE 0
	END
) AS deskripsi_pelanggaran_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'total_pelanggaran_3'::text) THEN pd.value
	ELSE 0
	END
) AS total_pelanggaran_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'deskripsi_pelanggaran_3'::text) THEN pd.value
	ELSE 0
	END
) AS deskripsi_pelanggaran_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'page25_bln_1'::text) THEN pd.value
	ELSE 0
	END
) AS page25_bln_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'page25_bln_2'::text) THEN pd.value
	ELSE 0
	END
) AS page25_bln_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'page25_bln_3'::text) THEN pd.value
	ELSE 0
	END
) AS page25_bln_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'page25_bln_4'::text) THEN pd.value
	ELSE 0
	END
) AS page25_bln_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'page25_bln_5'::text) THEN pd.value
	ELSE 0
	END
) AS page25_bln_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'page25_bln_6'::text) THEN pd.value
	ELSE 0
	END
) AS page25_bln_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'page25_bln_7'::text) THEN pd.value
	ELSE 0
	END
) AS page25_bln_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'page25_bln_8'::text) THEN pd.value
	ELSE 0
	END
) AS page25_bln_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'page25_bln_9'::text) THEN pd.value
	ELSE 0
	END
) AS page25_bln_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'page25_bln_10'::text) THEN pd.value
	ELSE 0
	END
) AS page25_bln_10, 
sum(
	CASE
	WHEN ((pd.header)::text = 'page25_bln_11'::text) THEN pd.value
	ELSE 0
	END
) AS page25_bln_11, 
sum(
	CASE
	WHEN ((pd.header)::text = 'page25_bln_12'::text) THEN pd.value
	ELSE 0
	END
) AS page25_bln_12
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;


DROP VIEW IF EXISTS "public"."page_26";

CREATE OR REPLACE VIEW "public"."page_26" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'tahun'::text) THEN pd.value
	ELSE 0
	END
) AS tahun, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket'::text) THEN pd.value
	ELSE 0
	END
) AS paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pagu'::text) THEN pd.value
	ELSE 0
	END
) AS pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'hari'::text) THEN pd.value
	ELSE 0
	END
) AS hari, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_2'::text) THEN pd.value
	ELSE 0
	END
) AS paket_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pagu_2'::text) THEN pd.value
	ELSE 0
	END
) AS pagu_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'hari_2'::text) THEN pd.value
	ELSE 0
	END
) AS hari_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pagu_t_1'::text) THEN pd.value
	ELSE 0
	END
) AS pagu_t_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pagu_t_2'::text) THEN pd.value
	ELSE 0
	END
) AS pagu_t_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pagu_t_3'::text) THEN pd.value
	ELSE 0
	END
) AS pagu_t_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pagu_t_4'::text) THEN pd.value
	ELSE 0
	END
) AS pagu_t_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_t_1'::text) THEN pd.value
	ELSE 0
	END
) AS paket_t_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_t_2'::text) THEN pd.value
	ELSE 0
	END
) AS paket_t_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_t_3'::text) THEN pd.value
	ELSE 0
	END
) AS paket_t_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_t_4'::text) THEN pd.value
	ELSE 0
	END
) AS paket_t_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'selesai_1'::text) THEN pd.value
	ELSE 0
	END
) AS selesai_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'gagal_1'::text) THEN pd.value
	ELSE 0
	END
) AS gagal_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'selesai_2'::text) THEN pd.value
	ELSE 0
	END
) AS selesai_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'gagal_2'::text) THEN pd.value
	ELSE 0
	END
) AS gagal_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'selesai_3'::text) THEN pd.value
	ELSE 0
	END
) AS selesai_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'gagal_3'::text) THEN pd.value
	ELSE 0
	END
) AS gagal_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'selesai_4'::text) THEN pd.value
	ELSE 0
	END
) AS selesai_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'gagal_4'::text) THEN pd.value
	ELSE 0
	END
) AS gagal_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'selesai_5'::text) THEN pd.value
	ELSE 0
	END
) AS selesai_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'gagal_5'::text) THEN pd.value
	ELSE 0
	END
) AS gagal_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'alasan_1'::text) THEN pd.value
	ELSE 0
	END
) AS alasan_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'alasan_2'::text) THEN pd.value
	ELSE 0
	END
) AS alasan_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'alasan_3'::text) THEN pd.value
	ELSE 0
	END
) AS alasan_3
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;


DROP VIEW IF EXISTS "public"."page_27";

CREATE OR REPLACE VIEW "public"."page_27" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_pagu_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS p27_pagu_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_pagu_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS p27_pagu_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_pagu_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS p27_pagu_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_pagu_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS p27_pagu_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_pagu_kecil_3'::text) THEN pd.value
	ELSE 0
	END
) AS p27_pagu_kecil_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_pagu_non_3'::text) THEN pd.value
	ELSE 0
	END
) AS p27_pagu_non_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_pagu_kecil_4'::text) THEN pd.value
	ELSE 0
	END
) AS p27_pagu_kecil_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_pagu_non_4'::text) THEN pd.value
	ELSE 0
	END
) AS p27_pagu_non_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_pagu_kecil_5'::text) THEN pd.value
	ELSE 0
	END
) AS p27_pagu_kecil_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_pagu_non_5'::text) THEN pd.value
	ELSE 0
	END
) AS p27_pagu_non_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_paket_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS p27_paket_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_paket_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS p27_paket_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_paket_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS p27_paket_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_paket_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS p27_paket_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_paket_kecil_3'::text) THEN pd.value
	ELSE 0
	END
) AS p27_paket_kecil_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_paket_non_3'::text) THEN pd.value
	ELSE 0
	END
) AS p27_paket_non_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_paket_kecil_4'::text) THEN pd.value
	ELSE 0
	END
) AS p27_paket_kecil_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_paket_non_4'::text) THEN pd.value
	ELSE 0
	END
) AS p27_paket_non_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_paket_kecil_5'::text) THEN pd.value
	ELSE 0
	END
) AS p27_paket_kecil_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_paket_non_5'::text) THEN pd.value
	ELSE 0
	END
) AS p27_paket_non_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_pagu_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_pagu_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_pagu_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_pagu_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_pagu_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_pagu_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_pagu_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_pagu_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_pagu_kecil_3'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_pagu_kecil_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_pagu_non_3'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_pagu_non_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_pagu_kecil_4'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_pagu_kecil_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_pagu_non_4'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_pagu_non_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_pagu_kecil_5'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_pagu_kecil_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_pagu_non_5'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_pagu_non_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_paket_kecil_1'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_paket_kecil_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_paket_non_1'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_paket_non_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_paket_kecil_2'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_paket_kecil_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_paket_non_2'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_paket_non_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_paket_kecil_3'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_paket_kecil_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_paket_non_3'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_paket_non_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_paket_kecil_4'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_paket_kecil_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_paket_non_4'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_paket_non_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_paket_kecil_5'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_paket_kecil_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p27_tender_paket_non_5'::text) THEN pd.value
	ELSE 0
	END
) AS p27_tender_paket_non_5
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;


DROP VIEW IF EXISTS "public"."page_35";

CREATE OR REPLACE VIEW "public"."page_35" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'ptf_barang_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS ptf_barang_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptf_barang_paket'::text) THEN pd.value
	ELSE 0
	END
) AS ptf_barang_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptf_pekerjaan_konstruksi_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS ptf_pekerjaan_konstruksi_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptf_pekerjaan_konstruksi_paket'::text) THEN pd.value
	ELSE 0
	END
) AS ptf_pekerjaan_konstruksi_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptf_jasa_konsultansi_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS ptf_jasa_konsultansi_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptf_jasa_konsultansi_paket'::text) THEN pd.value
	ELSE 0
	END
) AS ptf_jasa_konsultansi_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptf_jasa_lainnya_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS ptf_jasa_lainnya_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ptf_jasa_lainnya_paket'::text) THEN pd.value
	ELSE 0
	END
) AS ptf_jasa_lainnya_paket
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;


DROP VIEW IF EXISTS "public"."page_36";

CREATE OR REPLACE VIEW "public"."page_36" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_pengadaan_langsung_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS pts_pengadaan_langsung_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_pengadaan_langsung_paket'::text) THEN pd.value
	ELSE 0
	END
) AS pts_pengadaan_langsung_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_epurchasing_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS pts_epurchasing_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_epurchasing_paket'::text) THEN pd.value
	ELSE 0
	END
) AS pts_epurchasing_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_penunjukan_langsung_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS pts_penunjukan_langsung_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_penunjukan_langsung_paket'::text) THEN pd.value
	ELSE 0
	END
) AS pts_penunjukan_langsung_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_tender_lelang_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS pts_tender_lelang_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_tender_lelang_paket'::text) THEN pd.value
	ELSE 0
	END
) AS pts_tender_lelang_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_pemilihan_langsung_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS pts_pemilihan_langsung_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_pemilihan_langsung_paket'::text) THEN pd.value
	ELSE 0
	END
) AS pts_pemilihan_langsung_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_seleksi_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS pts_seleksi_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_seleksi_paket'::text) THEN pd.value
	ELSE 0
	END
) AS pts_seleksi_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_tender_lelang_cepat_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS pts_tender_lelang_cepat_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_tender_lelang_cepat_paket'::text) THEN pd.value
	ELSE 0
	END
) AS pts_tender_lelang_cepat_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_kontes_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS pts_kontes_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_kontes_paket'::text) THEN pd.value
	ELSE 0
	END
) AS pts_kontes_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_sayembara_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS pts_sayembara_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_sayembara_paket'::text) THEN pd.value
	ELSE 0
	END
) AS pts_sayembara_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_lainnya_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS pts_lainnya_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pts_lainnya_paket'::text) THEN pd.value
	ELSE 0
	END
) AS pts_lainnya_paket
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;


DROP VIEW IF EXISTS "public"."page_6";

CREATE OR REPLACE VIEW "public"."page_6" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'bklp'::text) THEN pd.value
	ELSE 0
	END
) AS bklp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'harga_belanja_pengadaan'::text) THEN pd.value
	ELSE 0
	END
) AS harga_belanja_pengadaan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'persentase_belanja_pengadaan'::text) THEN pd.value
	ELSE 0
	END
) AS persentase_belanja_pengadaan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'p6_ulp_ukpbj'::text) THEN pd.value
	ELSE 0
	END
) AS p6_ulp_ukpbj, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ukpbk_permanen_struktural'::text) THEN pd.value
	ELSE 0
	END
) AS ukpbk_permanen_struktural, 
sum(
	CASE
	WHEN ((pd.header)::text = 'lpse_1'::text) THEN pd.value
	ELSE 0
	END
) AS lpse_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'lpse_2'::text) THEN pd.value
	ELSE 0
	END
) AS lpse_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'lpse_standar'::text) THEN pd.value
	ELSE 0
	END
) AS lpse_standar
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;


DROP VIEW IF EXISTS "public"."page_7";

CREATE OR REPLACE VIEW "public"."page_7" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'ipp_ld_paket'::text) THEN pd.value
	ELSE 0
	END
) AS ipp_ld_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ipp_ld_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS ipp_ld_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ipp_lsc_paket'::text) THEN pd.value
	ELSE 0
	END
) AS ipp_lsc_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ipp_lsc_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS ipp_lsc_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ipp_li_paket'::text) THEN pd.value
	ELSE 0
	END
) AS ipp_li_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ipp_li_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS ipp_li_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ipp_konsolidasi_paket'::text) THEN pd.value
	ELSE 0
	END
) AS ipp_konsolidasi_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ipp_konsolidasi_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS ipp_konsolidasi_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ipp_payung_paket'::text) THEN pd.value
	ELSE 0
	END
) AS ipp_payung_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ipp_payung_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS ipp_payung_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ipp_jamak_paket'::text) THEN pd.value
	ELSE 0
	END
) AS ipp_jamak_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ipp_jamak_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS ipp_jamak_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'psuk_rup_paket'::text) THEN pd.value
	ELSE 0
	END
) AS psuk_rup_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'psuk_rup_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS psuk_rup_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'psuk_realisasi_paket'::text) THEN pd.value
	ELSE 0
	END
) AS psuk_realisasi_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'psuk_realisasi_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS psuk_realisasi_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'persentase_rp'::text) THEN pd.value
	ELSE 0
	END
) AS persentase_rp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'harga_rp'::text) THEN pd.value
	ELSE 0
	END
) AS harga_rp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'swakelola_rp'::text) THEN pd.value
	ELSE 0
	END
) AS swakelola_rp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'tendering_rp'::text) THEN pd.value
	ELSE 0
	END
) AS tendering_rp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'purchasing_rp'::text) THEN pd.value
	ELSE 0
	END
) AS purchasing_rp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pengadaan_langsung_rp'::text) THEN pd.value
	ELSE 0
	END
) AS pengadaan_langsung_rp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penunjukan_langsung_rp'::text) THEN pd.value
	ELSE 0
	END
) AS penunjukan_langsung_rp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'sayembara_kontes_rp'::text) THEN pd.value
	ELSE 0
	END
) AS sayembara_kontes_rp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'lainnya_rp'::text) THEN pd.value
	ELSE 0
	END
) AS lainnya_rp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pagu_pengumuman_et'::text) THEN pd.value
	ELSE 0
	END
) AS pagu_pengumuman_et, 
sum(
	CASE
	WHEN ((pd.header)::text = 'hasil_tender_et'::text) THEN pd.value
	ELSE 0
	END
) AS hasil_tender_et, 
sum(
	CASE
	WHEN ((pd.header)::text = 'nilai_harga_ep'::text) THEN pd.value
	ELSE 0
	END
) AS nilai_harga_ep, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ppb_dasar'::text) THEN pd.value
	ELSE 0
	END
) AS ppb_dasar, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ppbk'::text) THEN pd.value
	ELSE 0
	END
) AS ppbk, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ppbk_jf'::text) THEN pd.value
	ELSE 0
	END
) AS ppbk_jf, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pd_spse'::text) THEN pd.value
	ELSE 0
	END
) AS pd_spse, 
sum(
	CASE
	WHEN ((pd.header)::text = 'sanksi_df'::text) THEN pd.value
	ELSE 0
	END
) AS sanksi_df, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ph_layanan_advokasi'::text) THEN pd.value
	ELSE 0
	END
) AS ph_layanan_advokasi, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ph_layanan_advokasi_value'::text) THEN pd.value
	ELSE 0
	END
) AS ph_layanan_advokasi_value, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ph_kasus_pengadaan'::text) THEN pd.value
	ELSE 0
	END
) AS ph_kasus_pengadaan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ph_nilai_kasus'::text) THEN pd.value
	ELSE 0
	END
) AS ph_nilai_kasus, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ph_kontrak'::text) THEN pd.value
	ELSE 0
	END
) AS ph_kontrak, 
sum(
	CASE
	WHEN ((pd.header)::text = 'ph_nilai_kontrak'::text) THEN pd.value
	ELSE 0
	END
) AS ph_nilai_kontrak
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;


DROP VIEW IF EXISTS "public"."page_8";

CREATE OR REPLACE VIEW "public"."page_8" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'bp_2018'::text) THEN pd.value
	ELSE 0
	END
) AS bp_2018, 
sum(
	CASE
	WHEN ((pd.header)::text = 'perencanaan_pengadaan'::text) THEN pd.value
	ELSE 0
	END
) AS perencanaan_pengadaan, 
sum(
	CASE
	WHEN ((pd.header)::text = 'persentase_penyedia'::text) THEN pd.value
	ELSE 0
	END
) AS persentase_penyedia, 
sum(
	CASE
	WHEN ((pd.header)::text = 'harga_penyedia'::text) THEN pd.value
	ELSE 0
	END
) AS harga_penyedia, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_penyedia'::text) THEN pd.value
	ELSE 0
	END
) AS paket_penyedia, 
sum(
	CASE
	WHEN ((pd.header)::text = 'persentase_swakelola'::text) THEN pd.value
	ELSE 0
	END
) AS persentase_swakelola, 
sum(
	CASE
	WHEN ((pd.header)::text = 'harga_swakelola'::text) THEN pd.value
	ELSE 0
	END
) AS harga_swakelola, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_swakelola'::text) THEN pd.value
	ELSE 0
	END
) AS paket_swakelola, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jp_pk_harga'::text) THEN pd.value
	ELSE 0
	END
) AS jp_pk_harga, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jp_pk_paket'::text) THEN pd.value
	ELSE 0
	END
) AS jp_pk_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jp_barang_harga'::text) THEN pd.value
	ELSE 0
	END
) AS jp_barang_harga, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jp_barang_paket'::text) THEN pd.value
	ELSE 0
	END
) AS jp_barang_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jp_jk_harga'::text) THEN pd.value
	ELSE 0
	END
) AS jp_jk_harga, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jp_jk_paket'::text) THEN pd.value
	ELSE 0
	END
) AS jp_jk_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jp_jl_harga'::text) THEN pd.value
	ELSE 0
	END
) AS jp_jl_harga, 
sum(
	CASE
	WHEN ((pd.header)::text = 'jp_jl_paket'::text) THEN pd.value
	ELSE 0
	END
) AS jp_jl_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pengadaan_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS pengadaan_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'pengadaan_paket'::text) THEN pd.value
	ELSE 0
	END
) AS pengadaan_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penunjukan_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS penunjukan_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'penunjukan_paket'::text) THEN pd.value
	ELSE 0
	END
) AS penunjukan_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'purchasing_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS purchasing_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'purchasing_paket'::text) THEN pd.value
	ELSE 0
	END
) AS purchasing_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'tender_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS tender_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'tender_paket'::text) THEN pd.value
	ELSE 0
	END
) AS tender_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'tender_cepat_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS tender_cepat_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'tender_cepat_paket'::text) THEN pd.value
	ELSE 0
	END
) AS tender_cepat_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'seleksi_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS seleksi_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'seleksi_paket'::text) THEN pd.value
	ELSE 0
	END
) AS seleksi_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'sdk_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS sdk_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'sdk_paket'::text) THEN pd.value
	ELSE 0
	END
) AS sdk_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'lainnya_pagu'::text) THEN pd.value
	ELSE 0
	END
) AS lainnya_pagu, 
sum(
	CASE
	WHEN ((pd.header)::text = 'lainnya_paket'::text) THEN pd.value
	ELSE 0
	END
) AS lainnya_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_1'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_2'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_3'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_4'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_5'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_6'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_7'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_8'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_9'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_10'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_10, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_11'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_11, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_12'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_12, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_13'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_13, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_14'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_14, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_15'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_15, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_16'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_16, 
sum(
	CASE
	WHEN ((pd.header)::text = 'rwpp_list_item_17'::text) THEN pd.value
	ELSE 0
	END
) AS rwpp_list_item_17
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;


DROP VIEW IF EXISTS "public"."page_9";

CREATE OR REPLACE VIEW "public"."page_9" AS 
SELECT pd.procurement_id, pd.flag,
sum(
	CASE
	WHEN ((pd.header)::text = 'harga_pagu_pengumuman'::text) THEN pd.value
	ELSE 0
	END
) AS harga_pagu_pengumuman, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_pagu_pengumuman'::text) THEN pd.value
	ELSE 0
	END
) AS paket_pagu_pengumuman, 
sum(
	CASE
	WHEN ((pd.header)::text = 'harga_hps_pp'::text) THEN pd.value
	ELSE 0
	END
) AS harga_hps_pp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_hps_pp'::text) THEN pd.value
	ELSE 0
	END
) AS paket_hps_pp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'harga_hasil_tender'::text) THEN pd.value
	ELSE 0
	END
) AS harga_hasil_tender, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_hasil_tender'::text) THEN pd.value
	ELSE 0
	END
) AS paket_hasil_tender, 
sum(
	CASE
	WHEN ((pd.header)::text = 'harga_pengadaan_barang'::text) THEN pd.value
	ELSE 0
	END
) AS harga_pengadaan_barang, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_pengadaan_barang'::text) THEN pd.value
	ELSE 0
	END
) AS paket_pengadaan_barang, 
sum(
	CASE
	WHEN ((pd.header)::text = 'harga_barang_jp'::text) THEN pd.value
	ELSE 0
	END
) AS harga_barang_jp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_barang_jp'::text) THEN pd.value
	ELSE 0
	END
) AS paket_barang_jp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'harga_jkbu_jp'::text) THEN pd.value
	ELSE 0
	END
) AS harga_jkbu_jp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_jkbu_jp'::text) THEN pd.value
	ELSE 0
	END
) AS paket_jkbu_jp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'harga_jkp_jp'::text) THEN pd.value
	ELSE 0
	END
) AS harga_jkp_jp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_jkp_jp'::text) THEN pd.value
	ELSE 0
	END
) AS paket_jkp_jp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'harga_jl_jp'::text) THEN pd.value
	ELSE 0
	END
) AS harga_jl_jp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'paket_jl_jp'::text) THEN pd.value
	ELSE 0
	END
) AS paket_jl_jp, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_1'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_1, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_2'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_2, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_3'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_3, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_4'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_4, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_5'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_5, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_6'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_6, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_7'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_7, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_8'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_8, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_9'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_9, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_10'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_10, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_11'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_11, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_12'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_12, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_13'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_13, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_14'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_14, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_15'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_15, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_16'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_16, 
sum(
	CASE
	WHEN ((pd.header)::text = 'wpml_list_item_17'::text) THEN pd.value
	ELSE 0
	END
) AS wpml_list_item_17, 
sum(
	CASE
	WHEN ((pd.header)::text = 'mpt_hps'::text) THEN pd.value
	ELSE 0
	END
) AS mpt_hps, 
sum(
	CASE
	WHEN ((pd.header)::text = 'mpt_paket'::text) THEN pd.value
	ELSE 0
	END
) AS mpt_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'mptc_hps'::text) THEN pd.value
	ELSE 0
	END
) AS mptc_hps, 
sum(
	CASE
	WHEN ((pd.header)::text = 'mptc_paket'::text) THEN pd.value
	ELSE 0
	END
) AS mptc_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'mps_hps'::text) THEN pd.value
	ELSE 0
	END
) AS mps_hps, 
sum(
	CASE
	WHEN ((pd.header)::text = 'mps_paket'::text) THEN pd.value
	ELSE 0
	END
) AS mps_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'mpc_hps'::text) THEN pd.value
	ELSE 0
	END
) AS mpc_hps, 
sum(
	CASE
	WHEN ((pd.header)::text = 'mpc_paket'::text) THEN pd.value
	ELSE 0
	END
) AS mpc_paket, 
sum(
	CASE
	WHEN ((pd.header)::text = 'selisih_pagu_pengumuman'::text) THEN pd.value
	ELSE 0
	END
) AS selisih_pagu_pengumuman
FROM "public"."procurement_data" pd 
GROUP BY pd.procurement_id, pd.flag;
