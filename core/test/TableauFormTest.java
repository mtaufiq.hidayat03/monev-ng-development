import models.cms.VisualisasiTableauForm;
import org.junit.Test;
import play.test.UnitTest;
import tableau.library.models.TableauLibrary;

import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.text.IsEmptyString.isEmptyOrNullString;

/**
 * @author HanusaCloud on 12/19/2019 4:34 PM
 */
public class TableauFormTest extends UnitTest {

    VisualisasiTableauForm model = new VisualisasiTableauForm();

    @Test
    public void shouldReturnEmptyOnNullOrEmptyTableauUrl() {
        assertThat(model.generateQueryString(new TableauLibrary()), is(isEmptyOrNullString()));
    }

    @Test
    public void shouldReturnEmptyOnNullTableau() {
        assertThat(model.generateQueryString(null), is(isEmptyOrNullString()));
    }

    @Test
    public void shouldReturnEmptyOnSizeMissMatchArray() {
        model.paramValues = new String[] {"", ""};
        model.paramKeys = new String[] {"name"};
        assertThat(model.generateQueryString(new TableauLibrary()), is(isEmptyOrNullString()));
    }

    @Test
    public void shouldReturnGeneratedQueryString() {
        model.paramKeys = new String[] {"name"};
        model.paramValues = new String[] {"asdffe"};
        String queryString = model.generateQueryString(new TableauLibrary());
        assertEquals("?name=asdffe", queryString);
    }

    @Test
    public void testShouldReturnNonEmptyMap() {
        model.alamatUrl = "?name=value&key=test&asc=34jksanfk";
        Map<String, String> result = model.extractQueryString();
        assertNotNull(result);
        assertEquals(3, result.size());
        assertEquals("value", result.get("name"));
        assertEquals("34jksanfk", result.get("asc"));
    }

    @Test
    public void testShouldReturnEmptyMap() {
        model.alamatUrl = "name=test&df=rubber";
        Map<String, String> result = model.extractQueryString();
        assertNotNull(result);
        assertEquals(0, result.size());
        assertNull(result.get("df"));
    }

}
