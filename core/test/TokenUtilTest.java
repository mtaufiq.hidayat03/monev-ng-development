import org.junit.Test;
import play.test.UnitTest;
import utils.common.TokenUtil;

/**
 * @author HanusaCloud on 12/26/2019 8:23 AM
 */
public class TokenUtilTest extends UnitTest {

    @Test
    public void shouldReturnEncryptedString() {
        String result = TokenUtil.sha256("LkPP2o19!@#");
        assertNotNull(result);
        assertEquals(result, "1579e2eb6e13b9db210285344ec640737acf0eabd14c0dabd8ee2fb8be2136a2");
    }

}
