import models.cms.*;
import models.common.ServiceResult;
import models.user.management.User;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import play.test.UnitTest;
import repositories.cms.AksesContentRepository;
import repositories.cms.ContentRepository;
import repositories.searchcontent.ArticleRepository;
import repositories.user.management.UserRepository;
import services.cms.ContentService;

import static org.mockito.Mockito.*;

/**
 * @author HanusaCloud on 1/8/2020 11:38 AM
 */
public class StoreContentAccessTest extends UnitTest {

    @InjectMocks
    ContentService mockedService;
    @Mock
    ContentRepository mockedRepo;
    @Mock
    UserRepository userRepository;
    @Mock
    AksesContentRepository aksesContentRepository;
    @Spy
    ArticleRepository elasticRepo;

    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    AccessForm model = new AccessForm();

    @Before
    public void createMock() {
        model.id = 1L;
    }

    @Test
    public void shouldReturnFailOnMissingParams() {
        AccessForm model = new AccessForm();
        ServiceResult result = mockedService.storeAccessContent(model);
        assertFalse(result.status);
    }

    @Test
    public void shouldReturnFailOnPrivatePermissionWithEmptySelectedUser() {
        model.akses = ContentPermission.PRIVATE;
        ServiceResult result = mockedService.storeAccessContent(model);
        assertFalse(result.status);
    }

    @Test
    public void shouldFailOnNullContent() {
        model.akses = ContentPermission.PRIVATE;
        model.userSelected = "34,3425,5";
        when(mockedRepo.getContent(1L)).thenReturn(null);
        ServiceResult result = mockedService.storeAccessContent(model);
        assertFalse(result.status);
        assertEquals("Content tidak ditemukan!", result.message);
    }

    @Test
    public void shouldReturnSuccess() {
        Content content = spy(new Content(1L));
        content.publish = ContentStatus.PUBLISH.id;
        content.type = ContentType.ARTIKEL_UMUM;
        content.permission = ContentPermission.PUBLIC;
        model.akses = ContentPermission.PRIVATE;
        model.userSelected = "2";
        when(mockedRepo.getContent(1L)).thenReturn(content);
        when(aksesContentRepository.detachAksesContent(1L)).thenReturn(1);
        when(userRepository.getUser(2L)).thenReturn(new User(2L));
        doNothing().when(content).saveModel();
        doReturn(true).when(elasticRepo).storeToElastic(content);
        ServiceResult<Content> result = mockedService.storeAccessContent(model);
        assertEquals(Long.valueOf(1), content.getContentId());
        assertTrue(result.status);
        assertEquals("Konten berhasil disimpan.", result.message);
        assertNotNull(result.payload);
        assertEquals(ContentPermission.PRIVATE.toLowerCase(), result.payload.permission);
        assertEquals(Long.valueOf(1), result.payload.getAccessContents().get(0).content_id);
        assertEquals(Long.valueOf(2), result.payload.getAccessContents().get(0).user_id);
        assertTrue(result.payload.isPrivate());
    }

}
