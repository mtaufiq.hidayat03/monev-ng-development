import org.junit.Test;
import play.libs.URLs;
import play.test.UnitTest;
import utils.cms.TableauUtils;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.text.IsEmptyString.isEmptyOrNullString;

/**
 * @author HanusaCloud on 12/20/2019 10:52 AM
 */
public class TableauUtilsTest extends UnitTest {


    @Test
    public void shouldReturnNonTokenizedUrl() {
        TableauUtils.TableauUrl tableauUrl = TableauUtils.reConstructTableauUrl(
                "https://monevtab.lkpp.go.id/#/views/Perencanaan-GrafikSpasial/2_4Belanja-Nasional?:iid=3",
                "?test=try&df=light",
                "Test Kldi",
                false,
                null,
                "https://monevtab.lkpp.go.id");
        assertNotNull(tableauUrl);
        assertThat(tableauUrl.tokenized, isEmptyOrNullString());
        assertThat(tableauUrl.original, containsString("Test+Kldi"));
        assertThat(tableauUrl.original, containsString("test=try"));
        assertThat(tableauUrl.original, containsString(
                "https://monevtab.lkpp.go.id/#/views/Perencanaan-GrafikSpasial/2_4Belanja-Nasional"
                )
        );
        tableauUrl = TableauUtils.reConstructTableauUrl(
                "https://monevtab.lkpp.go.id/#/views/Perencanaan-GrafikSpasial/2_4Belanja-Nasional?:iid=3",
                "",
                "Test Kldi",
                false,
                null,
                "https://monevtab.lkpp.go.id");
        assertNotNull(tableauUrl);
    }

    @Test
    public void shouldReturnEmptyUrl() {
        TableauUtils.TableauUrl tableauUrl = TableauUtils.reConstructTableauUrl(
                "https://monevtab.lkpp.go.id/views/Perencanaan-GrafikSpasial/2_4Belanja-Nasional",
                null,
                "",
                false,
                null,
                "https://monevtab.lkpp.go.id");
        assertNull(tableauUrl);
    }

    @Test
    public void shouldReturnWithoutKldi() {
        TableauUtils.TableauUrl tableauUrl = TableauUtils.reConstructTableauUrl(
                "https://monevtab.lkpp.go.id/views/Perencanaan-GrafikSpasial/2_4Belanja-Nasional",
                "asdfawe",
                "",
                false,
                null,
                "https://monevtab.lkpp.go.id");
        assertNotNull(tableauUrl);
        assertThat(tableauUrl.original, not(containsString("kldi")));
    }

    @Test
    public void shouldReturnTokennizedUrl() {
        TableauUtils.TableauUrl tableauUrl = TableauUtils.reConstructTableauUrl(
                "https://monevtab.lkpp.go.id/views/Perencanaan-GrafikSpasial/2_4Belanja-Nasional",
                "asdfawe",
                "",
                true,
                null,
                "https://monevtab.lkpp.go.id");
        assertNotNull(tableauUrl);
        assertThat(tableauUrl.tokenized, isEmptyOrNullString());
        tableauUrl = TableauUtils.reConstructTableauUrl(
                "https://monevtab.lkpp.go.id/views/Perencanaan-GrafikSpasial/2_4Belanja-Nasional",
                "asdfawe",
                "",
                true,
                "asdfawe009dfs",
                "https://monevtab.lkpp.go.id");
        assertNotNull(tableauUrl);
        assertThat(tableauUrl.tokenized, not(isEmptyOrNullString()));
        assertThat(tableauUrl.tokenized, containsString(
                "https://monevtab.lkpp.go.id/trusted/" + URLs.encodePart("asdfawe009dfs"))
        );
    }

    @Test
    public void shouldExtractAfterHash() {
        String hashed = TableauUtils.extractUrlSegmentWithoutHash(
                "https://monevtab.lkpp.go.id",
                "https://monevtab.lkpp.go.id"
        );
        assertEquals("", hashed);
        hashed = TableauUtils.extractUrlSegmentWithoutHash(
                "https://monevtab.lkpp.go.id",
                "https://monevtab.lkpp.go.id/views/Perencanaan-GrafikSpasial/2_4Belanja-Nasional"
        );
        assertEquals("/views/Perencanaan-GrafikSpasial/2_4Belanja-Nasional", hashed);
        hashed = TableauUtils.extractUrlSegmentWithoutHash(
                "https://monevtab.lkpp.go.id",
                "https://monevtab.lkpp.go.id/views/Perencanaan-GrafikSpasial/2_4Belanja-Nasional/asdfe"
        );
        assertEquals("/views/Perencanaan-GrafikSpasial/2_4Belanja-Nasional/asdfe", hashed);
        hashed = TableauUtils.extractUrlSegmentWithoutHash(
                "https://monevtab.lkpp.go.id",
                "https://monevtab.lkpp.go.id/views/Perencanaan-GrafikSpasial/2_4Belanja-Nasional/asdfe?asdf=324&asdf="
        );
        assertEquals("/views/Perencanaan-GrafikSpasial/2_4Belanja-Nasional/asdfe", hashed);
        hashed = TableauUtils.extractUrlSegmentWithoutHash(
                "https://monevtab.lkpp.go.id",
                "https://monevtab.lkpp.go.id/#/views/Perencanaan-GrafikSpasial/2_4Belanja-Nasional"
        );
        assertEquals("/views/Perencanaan-GrafikSpasial/2_4Belanja-Nasional", hashed);
        hashed = TableauUtils.extractUrlSegmentWithoutHash(
                "https://monevtab.lkpp.go.id",
                "https://monevtab.lkpp.go.id/#/views/Perencanaan-GrafikSpasial/2_4Belanja-Nasional?asdf=3sadf&mon=9405"
        );
        assertEquals("/views/Perencanaan-GrafikSpasial/2_4Belanja-Nasional", hashed);
    }

}
