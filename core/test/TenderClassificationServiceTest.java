import org.junit.Test;
import play.test.UnitTest;
import services.core.TenderClassificationService;

import java.util.ArrayList;
import java.util.Arrays;

import static org.hamcrest.CoreMatchers.containsString;

/**
 * @author HanusaCloud on 12/26/2019 10:50 AM
 */
public class TenderClassificationServiceTest extends UnitTest {

    TenderClassificationService service = new TenderClassificationService();

    @Test
    public void shouldReturnLikeQuery() {
        String query = service.generateQuery(null, "asdfasdf awe");
        assertThat(query, containsString("ILIKE '%asdfasdf awe%'"));
        query = service.generateQuery(new ArrayList<>(), "test keyword");
        assertThat(query, containsString("ILIKE '%test keyword%'"));
    }

    @Test
    public void shouldReturnQueryByIds() {
        String query = service.generateQuery(Arrays.asList(123L, 3453L), "asdfasdf asdfwae");
        assertThat(query, containsString("IN ('123', '3453')"));
    }

    @Test
    public void shouldReturnFalseOnImproperString() {
        assertFalse(service.isKeywordValid(""));
        assertFalse(service.isKeywordValid("345"));
        assertFalse(service.isKeywordValid("   345   "));
    }

    @Test
    public void shouldReturnSuccessOnProperString() {
        assertTrue(service.isKeywordValid("   344, 4234, 5435"));
        assertTrue(service.isKeywordValid("  kasd fklasdfawoieh asdf awedf "));
    }

}
