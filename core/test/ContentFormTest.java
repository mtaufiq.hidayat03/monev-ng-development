import models.cms.ContentForm;
import models.common.contracts.FormContract;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import play.test.UnitTest;
import utils.cms.UploadedFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;

/**
 * @author HanusaCloud on 12/19/2019 1:40 PM
 */
public class ContentFormTest extends UnitTest {

    ContentForm model = new ContentForm();

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Test
    public void testAction() {
        Assert.assertTrue(model.isCreatedMode());
    }

    @Test
    public void shouldNotAllowToSaveOnCreate() {
        model.action = FormContract.CREATE;
        model.content = "test content";
        model.publish = 1;
        assertFalse(model.allowToStore());
    }

    @Test
    public void shouldAllowToSaveOnCreate() throws IOException {
        model.action = FormContract.CREATE;
        model.title = "test";
        model.content = "test content";
        model.publish = 1;
        model.uploadfile = temporaryFolder.newFile("test-new-file-on-create.txt");
        assertTrue(model.allowToStore());
    }

    @Test
    public void shouldNotAllowToSaveOnUpdate() {
        model.action = FormContract.UPDATE;
        model.title = "test";
        model.content = "test content";
        model.publish = 1;
        assertFalse(model.allowToStore());
    }

    @Test
    public void shouldAllowToSaveOnUpdate() {
        model.id = 1L;
        model.action = FormContract.UPDATE;
        model.title = "test";
        model.content = "test content";
        model.publish = 1;
        assertTrue(model.allowToStore());
    }

    @Test
    public void testUploadExtraFiles() throws IOException {
        model.uploadExtra = new ArrayList<>();
        model.uploadExtra.add(temporaryFolder.newFile("sample.doc"));
        model.uploadExtra.add(temporaryFolder.newFile("sample-1.doc"));
        List<UploadedFile> uploadedExtras = model.uploadMultipleFiles(null, model.uploadExtra);
        Assert.assertEquals(model.uploadExtra.size(), uploadedExtras.size());
        assertEquals("sample.doc", uploadedExtras.get(0).getOriginalName());
        assertEquals("sample-1.doc", uploadedExtras.get(1).getOriginalName());
    }

    @Test
    public void shouldBeAbleToDeleteFile() throws IOException {
        List<File> files = new ArrayList<>();
        files.add(temporaryFolder.newFile("sample.doc"));
        files.add(temporaryFolder.newFile("sample-1.doc"));
        model.uploadedExtras = model.uploadMultipleFiles(null, files);
        model.deletedFiles = new String[] {model.uploadedExtras.get(0).getNewFileName()};
        List<UploadedFile> uploadedDeletedExtras = model.uploadMultipleFiles(model.uploadedExtras, null);
        assertEquals(1, uploadedDeletedExtras.size());
        Assert.assertEquals("sample-1.doc", uploadedDeletedExtras.get(0).getOriginalName());
    }

    @Test
    public void shouldBeAbleToUpdateExtraFile() throws IOException {
        List<File> files = new ArrayList<>();
        files.add(temporaryFolder.newFile("sample.doc"));
        files.add(temporaryFolder.newFile("sample-1.doc"));
        model.uploadedExtras = model.uploadMultipleFiles(null, files);
        model.deletedFiles = new String[] {model.uploadedExtras.get(0).getNewFileName()};
        model.uploadExtra = new ArrayList<>();
        model.uploadExtra.add(temporaryFolder.newFile("new-sample-file.doc"));
        List<UploadedFile> currentFie = model.deleteByFiltering(model.uploadedExtras);
        List<UploadedFile> uploadedDeletedExtras = model.uploadMultipleFiles(currentFie, model.uploadExtra);
        Assert.assertEquals(2, uploadedDeletedExtras.size());
        Assert.assertEquals("new-sample-file.doc", uploadedDeletedExtras.get(0).getOriginalName());
        Assert.assertEquals("sample-1.doc", uploadedDeletedExtras.get(1).getOriginalName());
    }

    @Test
    public void shouldFailToUploadFeaturedImageOnNUll() {
        model.uploadfile = null;
        model.featuredImage = null;
        UploadedFile emptyImage = model.uploadFeaturedImage();
        Assert.assertNull(emptyImage);
    }

    @Test
    public void testUploadFeaturedImage() throws IOException {
        model.uploadfile = temporaryFolder.newFile("sample.png");
        model.uploadedFeaturedImage = model.uploadFeaturedImage();
        Assert.assertNotNull(model.uploadedFeaturedImage);
        Assert.assertEquals("sample.png", model.uploadedFeaturedImage.getOriginalName());
        Assert.assertTrue(model.uploadedFeaturedImage.fileExist());
        Assert.assertThat(model.featuredImage, containsString("sample.png"));
    }

    @Test
    public void shouldUpdateFeaturedImage() throws IOException {
        model.uploadfile = temporaryFolder.newFile("new2-sample-image.png");
        model.uploadFeaturedImage();
        model.uploadfile = temporaryFolder.newFile("new-sample-image.png");
        model.uploadFeaturedImage();
        Assert.assertThat(model.featuredImage, containsString("new-sample-image.png"));
    }

}
