import org.junit.Test;
import play.test.UnitTest;
import procurementprofile.models.ProcurementData;
import procurementprofile.models.ProfileSection;

import java.time.LocalDate;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.text.IsEmptyString.isEmptyOrNullString;

/**
 * @author HanusaCloud on 12/20/2019 1:37 PM
 */
public class ProcurementDataTest extends UnitTest {

    ProcurementData model = new ProcurementData();

    @Test
    public void shouldGetNumber() {
        int result =  model.getNumberFromAndDecreaseByOne(null);
        assertEquals(0, result);
        result = model.getNumberFromAndDecreaseByOne("asdf_1");
        assertEquals(0, result);
        result = model.getNumberFromAndDecreaseByOne("asdf_3_asdf_2_2");
        assertEquals(1, result);
    }

    @Test
    public void shouldReturnYear() {
        model.profileSection = ProfileSection.PTDS_ITEM_1;
        int year = model.getCalculatedYear(2019);
        assertEquals(2019, year);
        model.profileSection = ProfileSection.PTDS_ITEM_2;
        year = model.getCalculatedYear(2019);
        assertEquals(2018, year);
    }

    @Test
    public void shouldReturnMonthAndYear() {
        LocalDate localDate = LocalDate.of(2019, 12, 1);
        model.profileSection = ProfileSection.RWPP_LIST_ITEM_1;
        String month = model.getCalculatedMonth(localDate);
        assertEquals("Dec 2019", month);
        model.profileSection = ProfileSection.RWPP_LIST_ITEM_2;
        month = model.getCalculatedMonth(localDate);
        assertEquals("Nov 2019", month);
        model.profileSection = ProfileSection.RWPP_LIST_ITEM_14;
        month = model.getCalculatedMonth(localDate);
        assertEquals("Nov 2018", month);
    }

    @Test
    public void shouldBeEmptyExtra() {
        model.profileSection = ProfileSection.P6_ULP_UKPBJ;
        model.generateExtra();
        assertThat(model.extra, is(isEmptyOrNullString()));
    }

}
