import com.google.gson.JsonObject;
import models.cms.Content;
import models.cms.ContentForm;
import models.cms.ContentPermission;
import models.cms.ContentStatus;
import models.common.ServiceResult;
import models.common.contracts.FormContract;
import models.user.management.CurrentUser;
import models.user.management.User;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import permission.Access;
import play.test.UnitTest;
import repositories.cms.ContentRepository;
import repositories.searchcontent.ArticleRepository;
import services.cms.ContentService;

import java.io.IOException;
import java.util.ArrayList;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;

/**
 * @author HanusaCloud on 1/7/2020 3:04 PM
 */
public class GeneralContentTest extends UnitTest {

    @InjectMocks
    ContentService mockedService;
    @Mock
    ContentRepository mockedRepo;
    @Spy
    ArticleRepository elasticRepository;
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();
    CurrentUser currentUser = new CurrentUser();
    ContentForm model = new ContentForm();

    @Before
    public void createMock() throws IOException {
        User user = new User();
        user.id = 1L;
        user.role_id = (int) Access.SUPER_ADMIN.getId();
        currentUser.user = user;
        model.action = FormContract.CREATE;
        model.title = "this is just test title";
        model.content = "this is just test content";
        model.publish = ContentStatus.PUBLISH.id;
        model.kldiId = "KLDI";
        model.uploadfile = temporaryFolder.newFile("test-file.txt");
        model.uploadExtra = new ArrayList<>();
        model.uploadExtra.add(temporaryFolder.newFile("test-extra-file.txt"));
    }

    @Test
    public void shouldReturnContent() {
        Content content = new Content();
        content.id = 1L;
        content.title = "Mocked content";
        when(mockedRepo.getContent(1L)).thenReturn(content);
        assertEquals("Mocked content", mockedRepo.getContent(1L).title);
    }

    @Test
    public void shouldFailOnNullContent() {
        model.id = 1L;
        when(mockedRepo.getContent(1L)).thenReturn(null);
        ServiceResult<JsonObject> result = mockedService.storeGeneralArticle(model, currentUser);
        assertFalse(result.status);
        assertEquals("Content tidak ditemukan!", result.message);
    }

    @Test
    public void shouldReturnSuccessOnCreate() {
        Content spiedContent = spy(new Content());
        model.permission = ContentPermission.INTERNAL.toLowerCase();
        when(mockedRepo.getNewContent()).thenReturn(spiedContent);
        doNothing().when(spiedContent).saveModel();
        doReturn(true).when(elasticRepository).storeToElastic(spiedContent);
        ServiceResult<JsonObject> result = mockedService.storeGeneralArticle(model, currentUser);
        assertTrue(result.status);
        assertEquals(ContentPermission.INTERNAL.toLowerCase(), spiedContent.permission);
        assertThat(spiedContent.featured_image, not(isEmptyOrNullString()));
        assertTrue(spiedContent.getFeaturedImageFile().fileExist());
        assertThat(spiedContent.free_content, containsString("test-extra-file.txt"));
        assertEquals("KLDI", spiedContent.kldi_id);
    }

}
