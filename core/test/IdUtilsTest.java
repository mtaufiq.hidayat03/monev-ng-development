import org.junit.Test;
import play.test.UnitTest;
import utils.common.IdUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 12/21/2019 8:43 AM
 */
public class IdUtilsTest extends UnitTest {

    @Test
    public void shouldOnlyReturnNumericalFromString() {
        List<Long> results = IdUtils.getNumericalsFrom(
                "123,  324,43.432,   34,adsf 89usdaf jnkdf, 3244, 905 234 , asdf 324, 324 asdfawe sde, \"234,dfa\""
        );
        assertArrayEquals(new Long[]{123L, 324L, 34L, 3244L, 905234L}, results.toArray());
    }

    @Test
    public void shouldReturnStringLongIds() {
        List<Long> ids = new ArrayList<>();
        ids.add(123L);
        ids.add(324L);
        assertEquals("'123', '324'",IdUtils.turnIdsToIns(ids));
    }

    @Test
    public void shouldReturnStringIntegerIds() {
        List<Integer> ids = new ArrayList<>();
        ids.add(123);
        ids.add(324);
        assertEquals("'123', '324'",IdUtils.turnIdsToIns(ids));
    }

}
