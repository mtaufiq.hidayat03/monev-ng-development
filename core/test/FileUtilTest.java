import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import play.test.UnitTest;
import utils.cms.FileUtil;
import utils.cms.UploadedFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.text.IsEmptyString.isEmptyOrNullString;

/**
 * @author HanusaCloud on 12/19/2019 11:16 AM
 */
public class FileUtilTest extends UnitTest {

    @Rule
    public TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Test
    public void testShouldReturnUploadedFileObject() throws IOException {
        File testFile = temporaryFolder.newFile("sample.png");
        Assert.assertTrue(testFile.exists());
        UploadedFile uploadedFile = FileUtil.uploadImageFile(testFile);
        Assert.assertNotNull(uploadedFile);
        Assert.assertEquals(uploadedFile.originalName, "sample.png");
        Assert.assertThat(uploadedFile.getPath(), not(isEmptyOrNullString()));
        Assert.assertTrue(uploadedFile.fileExist());
    }

    @Test
    public void testShouldReturnNullOnNonExistenceFile() throws IOException {
        File testFile = temporaryFolder.newFile("sample.jpeg");
        testFile.delete();
        UploadedFile uploadedFile = FileUtil.uploadImageFile(testFile);
        Assert.assertNull(uploadedFile);
    }

    @Test
    public void testFileType() throws IOException {
        File testFile = temporaryFolder.newFile("sample.jpeg");
        UploadedFile uploadedFile = FileUtil.uploadImageFile(testFile);
        Assert.assertEquals("jpeg", uploadedFile.getFileExtension());
        Assert.assertTrue(uploadedFile.isImage());
        File testDocumentFile = temporaryFolder.newFile("sample-doc-test.pdf");
        UploadedFile docFile = FileUtil.uploadImageFile(testDocumentFile);
        Assert.assertEquals("pdf", docFile.getFileExtension());
        Assert.assertTrue(docFile.isDocument());
    }

    @Test
    public void testDocumentUrl() throws IOException {
        File testFile = temporaryFolder.newFile("sample-doc.pdf");
        UploadedFile uploadedFile = FileUtil.uploadImageFile(testFile);
        Assert.assertThat(uploadedFile.getUrl(), containsString("document"));
    }

    @Test
    public void testImageUrl() throws IOException {
        File testFile = temporaryFolder.newFile("sample-image.png");
        UploadedFile uploadedFile = FileUtil.uploadImageFile(testFile);
        Assert.assertThat(uploadedFile.getUrl(), containsString("picture"));
    }

    @Test
    public void uploadMultipleFile() throws IOException {
        List<File> files = new ArrayList<>();
        List<UploadedFile> uploadedFiles = FileUtil.uploadMultipleFiles(files);
        Assert.assertEquals(0, uploadedFiles.size());
        files.add(temporaryFolder.newFile("sample-doc.pdf"));
        files.add(temporaryFolder.newFile("sample-doc1.doc"));
        uploadedFiles = FileUtil.uploadMultipleFiles(files);
        Assert.assertEquals(uploadedFiles.size(), 2);
        Assert.assertEquals(uploadedFiles.get(0).getOriginalName(), "sample-doc.pdf");
        Assert.assertThat(uploadedFiles.get(0).getPath(), not(isEmptyOrNullString()));
        Assert.assertTrue(uploadedFiles.get(0).fileExist());
        Assert.assertEquals(uploadedFiles.get(1).getOriginalName(), "sample-doc1.doc");
        Assert.assertThat(uploadedFiles.get(1).getPath(), not(isEmptyOrNullString()));
        Assert.assertTrue(uploadedFiles.get(1).fileExist());
    }

    @Test
    public void deleteFile() throws IOException {
        List<File> files = new ArrayList<>();
        files.add(temporaryFolder.newFile("sample-doc.pdf"));
        files.add(temporaryFolder.newFile("sample-doc1.doc"));
        List<UploadedFile> uploadedFiles = FileUtil.uploadMultipleFiles(files);
        Assert.assertEquals(files.size(), uploadedFiles.size());
        FileUtil.deleteFile(uploadedFiles.get(0).getPath());
        Assert.assertFalse(uploadedFiles.get(0).fileExist());
        FileUtil.deleteFile(uploadedFiles.get(1).getPath());
        Assert.assertFalse(uploadedFiles.get(1).fileExist());
    }

    @Test
    public void shouldReturnTrueOnValidFileSize() {
        assertTrue(FileUtil.compareFileSize(1000 * 1000, FileUtil.DOCUMENT_MAX_FILE_SIZE));
    }

    @Test
    public void shouldReturnFalseOnInValidFileSize() {
        assertFalse(FileUtil.compareFileSize((5000 * 1000) + 1, FileUtil.DOCUMENT_MAX_FILE_SIZE));
        assertFalse(FileUtil.compareFileSize(1000 * 10000, FileUtil.DOCUMENT_MAX_FILE_SIZE));
    }

}
