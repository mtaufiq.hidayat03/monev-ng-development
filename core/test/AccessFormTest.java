import models.cms.AccessForm;
import org.hamcrest.core.Is;
import org.hamcrest.core.IsNot;
import org.junit.Assert;
import org.junit.Test;
import play.test.UnitTest;

/**
 * @author HanusaCloud on 12/19/2019 9:54 AM
 */
public class AccessFormTest extends UnitTest {

    AccessForm model = new AccessForm();

    @Test
    public void testAction() {
        Assert.assertTrue(model.isCreatedMode());
        model.action = "update";
        Assert.assertEquals("update", model.getAction());
        Assert.assertNotEquals("create", model.getAction());
        Assert.assertTrue(model.isEditedMode());
    }

    @Test
    public void testAccessForPublicPermission() {
        model.akses = "PUBLIC";
        model.userSelected = "1,23,4,54";
        assertTrue(model.isPublic());
        Assert.assertTrue(model.isItMyPermission("public"));
        Assert.assertFalse(model.isItMyPermission("private"));
        Assert.assertThat(model.getUserId().toArray(), IsNot.not(new Long[]{1L, 23L, 4L, 54L}));
        Assert.assertThat(model.getUserId().toArray(), Is.is(new Long[]{}));
    }

    @Test
    public void testAccessForInternalPermission() {
        model.akses = "INTERNAL";
        model.userSelected = "1,23,4,54";
        assertTrue(model.isInternal());
        Assert.assertTrue(model.isItMyPermission("internal"));
        Assert.assertFalse(model.isItMyPermission("private"));
        Assert.assertThat(model.getUserId().toArray(), IsNot.not(new Long[]{1L, 23L, 4L, 54L}));
        Assert.assertThat(model.getUserId().toArray(), Is.is(new Long[]{}));
    }

    @Test
    public void testAccessForPrivatePermission() {
        model.akses = "PRIVATE";
        Assert.assertEquals("PRIVATE", model.getPermission());
        Assert.assertTrue(model.isPrivate());
        Assert.assertFalse(model.isPublic());
        model.userSelected = "1,23,4,54";
        Assert.assertTrue(model.userSeletedExist());
        Assert.assertArrayEquals( new Long[]{1L, 23L, 4L, 54L}, model.getUserId().toArray());
        model.userSelected = "1,23,4,5,4";
        Assert.assertArrayEquals( new Long[]{1L, 23L, 4L, 5L, 4L}, model.getUserId().toArray());
        model.userSelected = null;
        Assert.assertFalse(model.userSeletedExist());
        Assert.assertEquals(0, model.getUserId().size());
        model.userSelected = "";
        Assert.assertFalse(model.userSeletedExist());
        Assert.assertEquals(0, model.getUserId().size());
    }

    @Test
    public void shouldReturnEmptyOnNonNumericString() {
        model.userSelected = "this is just a test, for collecting numerical value.";
        model.akses = "PRIVATE";
        Assert.assertArrayEquals(new Long[]{}, model.getUserId().toArray());
    }

    @Test
    public void shouldOnlyReturnNumericFromImproperString() {
        model.userSelected = "1,4,, 0, sadf, daf.,, 10, 5.6,0, 23iond";
        model.akses = "PRIVATE";
        Assert.assertArrayEquals(new Long[]{1L, 4L, 0L, 10L, 0L}, model.getUserId().toArray());
    }

}
