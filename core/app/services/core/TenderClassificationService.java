package services.core;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import org.apache.commons.lang3.StringUtils;
import play.Play;
import utils.common.IdUtils;
import utils.common.LogUtil;
import weka.classifiers.Classifier;
import weka.core.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * @author HanusaCloud on 11/18/2019 10:29 AM
 */
public class TenderClassificationService {

    public static final String TAG = "TenderClassificationService";

    private static final TenderClassificationService service = new TenderClassificationService();

    public static TenderClassificationService getInstance() {
        return service;
    }

    private static String PREDICTION_SOURCE = Play.configuration.getProperty("prediction.source");
    private static String PREDICTION_SOURCE_DB = Play.configuration.getProperty("prediction.source.db");
    private static String PREDICTION_SOURCE_USERNAME = Play.configuration.getProperty("prediction.source.username");
    private static String PREDICTION_SOURCE_PASSWORD = Play.configuration.getProperty("prediction.source.password");


    private static final Classifier classifier;

    static {
        Classifier cls;
        try {
            InputStream inputStream  = new FileInputStream(new File(Play.configuration.getProperty("weka.model")));
            cls = (Classifier) SerializationHelper.read(inputStream);
            inputStream.close();
        } catch (Exception e) {
            cls = null;
            LogUtil.error(TAG, e);
        }
        classifier = cls;
    }

    private final static String[] STARTER_COLUMNS = new String[]{
            "pkt_pagu",
            "pkt_hps",
            "versi_lelang",
            "jumlah_peserta_lelang",
            "evaluasi0_lulus"
    };

    private final static String[] ALLOW_TO_BE_ADDED_COLUMNS = new String[]{
            "tahun_anggaran",
            "nama_lpse",
            "nama_kldi",
            "nama_satker",
            "pkt_hps",
            "pkt_pagu",
            "nama_paket",
            "pkt_id",
            "versi_lelang",
            "lelang_selesai",
            "status_lelang",
            "ada_pemenang",
            "tanggal_lelang_dibuat",
            "repo_id"
    };


    private static String generateSelectQuery(String[] columns) {
        StringBuilder sb = new StringBuilder();
        String glue = "";
        sb.append("SELECT ");
        for (String s : columns) {
            sb.append(glue).append(s);
            glue = ", ";
        }
        sb.append(" FROM dwh_procurement.dm_riwayatlelang ");
        return sb.toString();
    }

    private final static String STARTER_QUERY = generateSelectQuery(STARTER_COLUMNS);
    private final static ArrayList<Attribute> STARTER_ATTRIBUTES = generateAttributes(STARTER_COLUMNS);

    public static ArrayList<Attribute> generateAttributes(String[] attributes) {
        ArrayList<Attribute> results = new ArrayList<>();
        for (String s : attributes) {
            results.add(new Attribute(s));
        }
        List<String> classValue = new ArrayList<>();
        classValue.add("0");
        classValue.add("1");
        results.add(new Attribute("target", classValue));
        return results;
    }

    private static boolean containInColumns(String key, String[] columns) {
        for (String s : columns) {
            if (s.equals(key)) {
                return true;
            }
        }
        return false;
    }

    public boolean isKeywordValid(String search) {
        final boolean result = !StringUtils.isEmpty(search)
                && search.replaceAll("\\s", "").length() > 3;
            LogUtil.debug(TAG, "Oops.. keyword is less than 3: " + result );
        return result;
    }

    private JsonObject failedReturn(String message) {
        JsonObject results = new JsonObject();
        results.addProperty("status", false);
        results.addProperty("message", message);
        return results;
    }

    public JsonObject classify(String keyword) {
        JsonObject results = new JsonObject();
        if (!isKeywordValid(keyword)) {
            return failedReturn("Kata kunci harus lebih dari 3 karakter!");
        }
        List<Map<String, Object>> rawData = getData(
                generateQuery(IdUtils.getNumericalsFrom(keyword), keyword)
        );
        if (rawData.size() > 10) {
            LogUtil.debug(TAG, "Oops.. search results more than 10 items");
            return failedReturn("Hasil pencarian lebih dari 10, ubah kata kunci atau dapat menggunakan Id paket agar lebih akurat!");
        }
        final boolean moreItems = rawData.size() > 1;
        JsonArray jsonArray = new JsonArray();
        results.addProperty("search", keyword);
        try {
            for (Map<String, Object> map : rawData) {
                JsonObject jsonObject = processing(map);
                if (!moreItems) {
                    results.add("item", jsonObject);
                } else {
                    jsonArray.add(jsonObject);
                }
            }
            if (moreItems) {
                results.add("items", jsonArray);
            }
            results.addProperty("status", true);
            results.addProperty("message", "success");
            return results;
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            results.addProperty("status", false);
            results.addProperty("message", "Oops.. something's wrong");
            return results;
        }
    }

    public JsonObject processing(Map<String, Object> map) throws Exception {
        Instances instances = new Instances("procurement", STARTER_ATTRIBUTES, 1);
        JsonObject jsonObject = new JsonObject();
        int i = 0;
        double[] data = new double[STARTER_COLUMNS.length + 1];
        for (Map.Entry<String, Object> entry : map.entrySet()) {
            if (containInColumns(entry.getKey(), ALLOW_TO_BE_ADDED_COLUMNS)) {
                jsonObject.addProperty(entry.getKey(), String.valueOf(entry.getValue()));
            }
            if (containInColumns(entry.getKey(), STARTER_COLUMNS)) {
                data[i] = Double.valueOf(entry.getValue().toString());
                i++;
            }
        }
        instances.setClassIndex(instances.numAttributes() - 1);
        instances.add(new DenseInstance(1.0, data));
        Instance instance = instances.instance(0);
        final Double result = classifier.classifyInstance(instance);
        jsonObject.addProperty("status", result);
        jsonObject.addProperty("predicted_status_text", result == 1 ? "Berhasil" : "Gagal");
        String actualStatus = "Masih Berlangsung";
        final double winnerCount = jsonObject.get("ada_pemenang").getAsDouble();
        final double finished = jsonObject.get("lelang_selesai").getAsDouble();
        if (winnerCount > 0 && finished == 1) {
            actualStatus = "Berhasil";
        } else if (winnerCount == 0 && finished  <= 1) {
            actualStatus = "Gagal";
        }
        jsonObject.addProperty("actual_status_text", actualStatus);
        return jsonObject;
    }

    public String generateQuery(List<Long> ids, String keyword) {
        String query = "SELECT * FROM " + PREDICTION_SOURCE_DB + " dr " +
                "where dr.versi_lelang = (SELECT MAX(rl.versi_lelang) " +
                "FROM " + PREDICTION_SOURCE_DB + " rl " +
                "WHERE rl.pkt_id = dr.pkt_id)";
        if (ids != null && !ids.isEmpty()) {
            query += " AND dr.pkt_id IN (" + IdUtils.turnIdsToIns(ids) + ") ";
            return query;
        }
        if (!StringUtils.isEmpty(keyword)) {
            query += " AND dr.nama_paket ILIKE '%" + keyword + "%'";
        }
        return query;
    }

    public List<Map<String, Object>> getData(String query) {
        List<Map<String, Object>> results = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(PREDICTION_SOURCE
                , PREDICTION_SOURCE_USERNAME, PREDICTION_SOURCE_PASSWORD)) {
            LogUtil.debug(TAG, "query: " + query);
            ResultSet resultSet = connection.createStatement()
                    .executeQuery(query);
            while (resultSet.next()) {
                Map<String, Object> map = new LinkedHashMap<>();
                ResultSetMetaData meta = resultSet.getMetaData();
                final int columnCount = meta.getColumnCount();
                for (int i = 1; i <= columnCount; i++) {
                    final Object object = resultSet.getString(i);
                    final String key = meta.getColumnName(i).toLowerCase();
                    if (object != null) {
                        map.put(key, object);
                    }
                }
                results.add(map);
            }
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }
        return results;
    }

    private static class QueryResult {

        public final String name;
        public final Map<String, Object> rawData;

        public QueryResult(String name, Map<String, Object> rawData) {
            this.name = name;
            this.rawData = rawData;
        }

    }

}
