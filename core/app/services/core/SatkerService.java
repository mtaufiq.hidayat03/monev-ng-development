package services.core;

import com.google.gson.reflect.TypeToken;
import models.Satker;
import play.db.jdbc.Query;
import play.libs.WS;
import play.mvc.Http;
import utils.common.JsonUtil;
import utils.common.LogUtil;

import java.util.List;

/**
 * @author HanusaCloud on 12/12/2019 1:22 PM
 */
public class SatkerService {

    public static final String TAG = "SatkerService";

    public static void syncSatker() {
        LogUtil.debug(TAG, "sync satker");
        try (WS.WSRequest request = WS.url("https://sirup.lkpp.go.id/sirup/service/daftarSatkerByKLDI")) {
            WS.HttpResponse response = request.get();
            LogUtil.debug(TAG, response.getHeaders());
            LogUtil.debug(TAG, "http status: " + response.getStatus());
            if (response.getStatus() != Http.StatusCode.OK) {
                LogUtil.debug(TAG, "Oops error");
                return;
            }
            List<Satker> satkers = JsonUtil.fromJson(
                    response.getString(),
                    new TypeToken<List<Satker>>(){}.getType());
            LogUtil.debug(TAG, "total satker: " + satkers.size());
            final int truncateResult = Query.update("TRUNCATE public.satker;");
            LogUtil.debug(TAG, "truncate result: " + truncateResult);
            Satker.batchInsert(satkers);
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }
    }

}