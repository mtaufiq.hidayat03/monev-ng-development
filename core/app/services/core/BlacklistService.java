package services.core;

import com.google.gson.JsonArray;
import models.Blacklist;
import models.common.contracts.DataTableRequest;
import play.db.jdbc.Query;
import repositories.core.BlacklistRepository;
import utils.common.JsonUtil;
import utils.common.LogUtil;

import java.util.*;
import java.util.stream.Collectors;

public class BlacklistService {
    private static final String TAG = "BlacklistService";
    private BlacklistRepository repository = BlacklistRepository.getInstance();
    private static final BlacklistService blacklistService = new BlacklistService();

    public static BlacklistService getInstance() {
        return blacklistService;
    }

    public Map generateJson(DataTableRequest dataTableRequest) {
        List<Blacklist> blacklists = repository.list(dataTableRequest);
        LogUtil.debug(TAG, blacklists);
        Map result = new HashMap();
        Integer draw = dataTableRequest.getDraw();
        JsonArray drawArray = new JsonArray();
        drawArray.add(draw);
        Long iFilteredTotal = repository.count();
        JsonArray iFilteredTotalArray = new JsonArray();
        iFilteredTotalArray.add(iFilteredTotal);
        result.put("draw", drawArray);
        result.put("iTotalDisplayRecords", iFilteredTotalArray);
        result.put("aaData", JsonUtil.toJsonId(blacklists));
        LogUtil.debug(TAG, result);

        return result;
    }

    private static final String[] JENIS = {"Barang", "Jasa", "Konstruksi", "Lainnya"};

    private static final String[] STATUS = {"AKTIF", "NON-AKTIF"};

    public String getRandomly(String[] arr) {
        final int length = arr.length;
        final int index = new Random().nextInt(length);
        return arr[index > length ? length - 1 : index];
    }

    public void flattenData() {
        List<Blacklist> blacklists = Blacklist.findAll();
        final int truncateResult = Query.update("TRUNCATE public.blacklist;");
        LogUtil.debug(TAG, "truncate result: " + truncateResult);
        List<Blacklist> results = blacklists.stream()
                .map(Blacklist::getDetail)
                .flatMap(e -> {
                    e.setModelRequirements();
                    List<Blacklist> newData = new ArrayList<>();
                    for (Blacklist.Pelanggaran pelanggaran : e.pelanggaran) {
                        Blacklist blacklist = new Blacklist(e, pelanggaran);
                        blacklist.jenis_pengadaan = getRandomly(JENIS);
                        blacklist.nama_pengadaan = "Dummy Pengadaan " + blacklist.jenis_pengadaan;
                        blacklist.id_pengadaan = String.valueOf(Math.abs(new Random().nextInt()));
                        blacklist.status = getRandomly(STATUS);
                        blacklist.save();
                        newData.add(blacklist);
                    }
                    return newData.stream();
                })
                .collect(Collectors.toList());
        LogUtil.debug(TAG, results);
/*
        final String QUERY = "INSERT INTO public.blacklist (id," +
                "rekanan_id," +
                "sk," +
                "npwp," +
                "alamat," +
                "tanggal_tayang," +
                "tanggal_turun_tayang," +
                "tanggal_berakhir," +
                "nama_propinsi," +
                "id_propinsi," +
                "nama_kabupaten," +
                "id_kabupaten," +
                "kode_kldi," +
                "nama_kldi," +
                "nama_satker," +
                "kode_satker," +
                "status," +
                "deskripsi," +
                "jenis) " +
                "VALUES (:id," +
                ":rekanan_id," +
                ":sk," +
                ":npwp," +
                ":alamat," +
                ":tanggal_tayang," +
                ":tanggal_turun_tayang," +
                ":tanggal_berakhir," +
                ":nama_propinsi," +
                ":id_propinsi," +
                ":nama_kabupaten," +
                ":id_kabupaten," +
                ":kode_kldi," +
                ":nama_kldi," +
                ":nama_satker," +
                ":kode_satker," +
                ":status," +
                ":deskripsi," +
                ":jenis)";
        play.db.jdbc.Query.bindUpdateAll(new play.db.jdbc.QueryBuilder(QUERY), Collections.singletonList(results));*/
    }

}
