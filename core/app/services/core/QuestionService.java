package services.core;

import models.QuestionAnswers.Question;
import models.QuestionAnswers.QuestionForm;
import repositories.core.QuestionAnswers.QuestionRepository;
import repositories.user.management.UserRepository;

public class QuestionService {

    private static final String TAG = "QuestionService";
    private QuestionRepository repository = QuestionRepository.getInstance();
    private static final QuestionService instance = new QuestionService();
    private static final UserRepository repo = new UserRepository();

    public static QuestionService getInstance() {
        return instance;
    }

    public void store(QuestionForm questionForm) {
        Question model = new Question();
        model.setQuestionForm(questionForm);
        model.saveModel();
    }

    public boolean deleteQuestion(Long questionId) {
        Question model = repository.getQuestion(questionId);
        if (model != null) {
            model.deleteModel();
            return true;
        }
        return false;
    }

}