package models;

import models.common.ReadOnlyModel;
import play.cache.Cache;
import play.db.jdbc.Table;
import utils.common.LogUtil;

import java.io.Serializable;
import java.util.List;

/**
 * @author HanusaCloud on 11/28/2019 9:42 AM
 */
@Table(name = "satker", schema = "public")
public class Satker extends ReadOnlyModel implements Serializable {

    public static final String TAG = "Satker";

    private static final String BATCH_SQL_INSERT = "INSERT INTO public.satker (id, idSatker, idKldi, nama) " +
            "VALUES (:id, :idSatker, :idKldi, :nama)";

    public String nama;
    public String idKldi;
    public String idSatker;

    public static Satker getBy(Long id) {
        final String key = TAG + "_" + id;
        Satker model;
        LogUtil.debug(TAG, "check cache by key: " + key);
        if ((model = (Satker) Cache.get(key)) != null) {
            LogUtil.debug(TAG, "cache exists");
            return model;
        }
        model = Satker.findById(id);
        LogUtil.debug(TAG, "save to cache by key: " + key);
        Cache.set(key, model);
        return model;
    }

    public static List<Satker> getSatkersBy(String kldiId) {
        LogUtil.debug(TAG, "get satkers by kldiId: " + kldiId);
        List<Satker> results;
        final String key = "satkers_by_kldi_" + kldiId;
        LogUtil.debug(TAG, "check cache");
        if ((results = (List<Satker>) Cache.get(key)) != null) {
            LogUtil.debug(TAG, "cache exist");
            return results;
        }
        LogUtil.debug(TAG, "cache not exist");
        results = Satker.find("idKldi = ?", kldiId).fetch();
        if (!results.isEmpty()) {
            Cache.set(key, results, "5h");
        }
        return results;
    }

    public String getNama() {
        return nama;
    }

    public static void batchInsert(List data) {
        play.db.jdbc.Query.bindUpdateAll(new play.db.jdbc.QueryBuilder(BATCH_SQL_INSERT), data);
    }

}
