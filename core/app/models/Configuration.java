package models;

import models.common.BaseModel;
import org.apache.commons.lang3.StringUtils;
import play.db.jdbc.Enumerated;
import play.db.jdbc.Table;
import utils.common.LogUtil;

import javax.persistence.EnumType;

/**
 * @author HanusaCloud on 12/4/2019 5:14 PM
 */
@Table(name = "configuration", schema = "public")
public class Configuration extends BaseModel {

    public static final String TAG = "Configuration";

    @Enumerated(EnumType.ORDINAL)
    public static enum ConfigKeys {

        TENDER_CLASSIFICATION("tender_classification","Tampilkan prediksi lelang");

        private final String name;
        private final String label;


        ConfigKeys(String name, String label) {
            this.name = name;
            this.label = label;
        }

        @Override
        public String toString() {
            return name;
        }

        public String getLabel() {
            return label;
        }

        public String getName() {
            return name;
        }
    }

    public String key;
    public String config_value;

    public boolean getAsBoolean() {
        return !StringUtils.isEmpty(config_value)
                && config_value.equalsIgnoreCase("true");
    }

    public static Configuration getTenderClassificationConfig() {
        return Configuration.find(
                "key = ?",
                ConfigKeys.TENDER_CLASSIFICATION.getName()
        ).first();
    }

    public static boolean isTenderForKlpdEnabled() {
        LogUtil.debug(TAG, "check is prediction enabled for klpd");
        final Configuration model = getTenderClassificationConfig();
        final boolean status = model != null && model.getAsBoolean();
        LogUtil.debug(TAG, "is prediction enabled: " + status);
        return status;
    }

}
