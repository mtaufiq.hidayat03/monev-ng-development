package models;

import models.cms.ContentPermission;
import models.cms.ContentStatus;
import models.cms.ContentType;
import models.common.contracts.PermissionContract;
import models.searchcontent.contracts.ElasticItemDetail;
import models.searchcontent.contracts.ElasticRequestPayloadContract;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.utils.URIBuilder;
import permission.Kldi;
import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;
import repositories.user.management.KldiRepository;
import utils.common.JsonUtil;
import utils.common.LogUtil;

import javax.persistence.Transient;
import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Table(name = "blacklist", schema = "public")
public class Blacklist extends BaseTable implements Serializable {

    @Id
    public Long id;
    public Long rekanan_id;
    public String nama_penyedia;
    public String sk;
    public String npwp;
    public String alamat;
    public Timestamp tanggal_tayang;
    public Timestamp tanggal_turun_tayang;
    public Timestamp tanggal_berakhir;
    public String nama_propinsi;
    public String id_propinsi;
    public String nama_kabupaten;
    public String id_kabupaten;
    public String kode_kldi;
    public String nama_kldi;
    public String nama_satker;
    public String kode_satker;
    public String status;
    public String deskripsi;
    public String jenis;
    public String nama_pengadaan;
    public String id_pengadaan;
    public String jenis_pengadaan;

    public String json_data;
    @Transient
    public Data data;
    @Transient
    public Long blacklist_id;

    public Blacklist() {}

    public Blacklist(Data data, Pelanggaran pelanggaran) {
        this.npwp = data.npwp;
        this.alamat = data.alamat;
        this.nama_satker = data.satuan_kerja.nama;
        this.kode_satker = data.satuan_kerja.id.toString();
        this.kode_kldi = data.kldi_id;
        this.nama_kldi = data.kldi_name;
        this.id_kabupaten = data.kabupaten.id.toString();
        this.nama_kabupaten = data.kabupaten.nama;
        this.nama_propinsi = data.provinsi.nama;
        this.id_propinsi = data.provinsi.id.toString();
        this.blacklist_id = data.id;
        this.rekanan_id = !StringUtils.isEmpty(data.rkn_id) ?  Long.valueOf(data.rkn_id) : 0;
        this.json_data = "{}";
        this.nama_penyedia = data.nama_penyedia;
        if (pelanggaran != null && pelanggaran.detil_pelanggaran != null) {
            this.sk = pelanggaran.sk;
            this.deskripsi = pelanggaran.detil_pelanggaran.deskripsi;
            this.jenis = pelanggaran.detil_pelanggaran.jenis_pelanggaran;
            this.tanggal_tayang = new Timestamp(pelanggaran.published_date.getTime());
            this.tanggal_berakhir = new Timestamp(pelanggaran.dummyExpired().getTime());
        }
        this.id = Query.find("SELECT nextval('blacklist_id_seq'::regclass)", Long.class).first();
    }

    public Data getDetail() {
        if (data == null) {
            data = JsonUtil.fromJsonId(json_data, Data.class);
        }
        return data;
    }

    public static class Data implements Serializable, 
            ElasticRequestPayloadContract, ElasticItemDetail, PermissionContract {

        private static String DATE_FORMAT = "dd MMM yyyy";
        
        public static final String TAG = "Data";

        public Long id;
        public String sk;
        public String npwp;
        public String alamat;
        public Date dibuat;
        public Region provinsi;
        public Region kabupaten;
        public List<Pelanggaran> pelanggaran;
        public String nama_penyedia;
        public Satker satuan_kerja;
        public String kldi_id;
        public String kldi_name;
        public String rkn_id;
        public String status;
        public String nama_pengadaan;
        public String id_pengadaan;
        public String jenis_pengadaan;

        @Transient
        public String[] content_permission;
        @Transient
        public String[] publishing_status = new String[]{ContentStatus.PUBLISH.name};
        public List<BlacklistPenyediaIkutLelang> participate_in_procurement;

        public Data() {}

        public Data(Blacklist blacklist) {
            id = blacklist.id;
            sk = blacklist.sk;
            npwp = blacklist.npwp;
            alamat = blacklist.alamat;
            pelanggaran = new ArrayList<>();
            pelanggaran.add(new Pelanggaran(blacklist));
            nama_penyedia = blacklist.nama_penyedia;
            rkn_id = blacklist.rekanan_id.toString();
            kldi_name = blacklist.nama_kldi;
            kldi_id = blacklist.kode_kldi;
            status = blacklist.status;
            satuan_kerja = new Satker();
            satuan_kerja.nama = blacklist.nama_satker;
            satuan_kerja.idSatker = blacklist.kode_satker;
            kabupaten = new Region();
            kabupaten.nama = blacklist.nama_kabupaten;
            if (blacklist.id_kabupaten != null) {
                kabupaten.id = Long.valueOf(blacklist.id_kabupaten);
            }
            provinsi = new Region();
            if (blacklist.id_propinsi != null) {
                provinsi.id = Long.valueOf(blacklist.id_propinsi);
            }
            provinsi.nama = blacklist.nama_propinsi;
            if (!StringUtils.isEmpty(blacklist.id_pengadaan)) {
                id_pengadaan = blacklist.id_pengadaan;
            }
            if (!StringUtils.isEmpty(blacklist.nama_pengadaan)) {
                nama_pengadaan = blacklist.nama_pengadaan;
            }
            if (!StringUtils.isEmpty(blacklist.jenis_pengadaan)) {
                jenis_pengadaan = blacklist.jenis_pengadaan;
            }
            dibuat = blacklist.tanggal_tayang;
        }

        @Override
        public Long getModelId() {
            return id;
        }

        @Override
        public String getModelType() {
            return ContentType.BLACKLIST;
        }

        @Override
        public String getModelUrl() {
            if (id == null || StringUtils.isEmpty(nama_penyedia)) {
                return "";
            }
            try {
                URIBuilder builder = new URIBuilder();
                builder.addParameter("id", String.valueOf(id));
                builder.addParameter("search", nama_penyedia);
                String queryString = builder.build().toString();
                //return Router.reverse(R.route.searchcontent_blacklistsearchcontroller_index, new HashMap<>()).url
                return "/blacklist" + queryString;
            } catch (Exception e) {
                LogUtil.error(TAG, e);
                return "";
            }
        }

        @Override
        public void setModelRequirements() {
            if (!StringUtils.isEmpty(rkn_id)) {
                participate_in_procurement = BlacklistPenyediaIkutLelang.getBy(rkn_id);
            }
            content_permission = new String[]{ContentPermission.PUBLIC.toLowerCase()};
            if (satuan_kerja == null || satuan_kerja.id == null) {
                return;
            }
            Satker satuanKerja;
            Kldi kldi;
            final boolean kldiEmpty = StringUtils.isEmpty(kldi_name) || StringUtils.isEmpty(kldi_id);
            if (kldiEmpty && (satuanKerja = Satker.getBy(satuan_kerja.id)) != null
                    && (kldi = KldiRepository.getInstance().getById(satuanKerja.idKldi)) != null) {
                kldi_name = kldi.nama_instansi;
                kldi_id = kldi.id;
            }
        }

        @Override
        public Timestamp getCreatedAt() {
            if (dibuat == null) {
                return null;
            }
            return Timestamp.from(dibuat.toInstant());
        }

        @Override
        public String getDetailTitle() {
            return nama_penyedia + "<div> " + npwp + "</div>";
        }

        @Override
        public String getDetailContent() {
            String district = "";
            if (kabupaten != null) {
                district = kabupaten.nama;
            }
            String province = "";
            if (provinsi != null) {
                province = ", " + provinsi.nama;
            }
            return alamat + "<br />" + district + province;
        }

        @Override
        public String getMeta() {
            if (pelanggaran != null && !pelanggaran.isEmpty()) {
                Pelanggaran violation = pelanggaran.get(0);
                final String publishedDate = violation.published_date != null
                        ? new SimpleDateFormat(DATE_FORMAT).format(violation.published_date) : "";
                return publishedDate;
            }
            return "";
        }

        @Override
        public String getPermission() {
            return ContentPermission.PUBLIC;
        }
    }

    public static class Pelanggaran {
        public String sk;
        public Date started;
        public Date published_date;
        public Date expired_date;
        public Date withdrawal_date;
        public Detail detil_pelanggaran;

        public Pelanggaran() {}

        public Pelanggaran(Blacklist blacklist) {
            sk = blacklist.sk;
            published_date = blacklist.tanggal_tayang;
            withdrawal_date = blacklist.tanggal_turun_tayang;
            expired_date = blacklist.tanggal_berakhir;
            detil_pelanggaran = new Detail();
            detil_pelanggaran.deskripsi = blacklist.deskripsi;
            detil_pelanggaran.jenis_pelanggaran = blacklist.jenis;
        }

        public static class Detail {
            public String dibuat;
            public String deskripsi;
            public String jenis_pelanggaran;
        }

        public Date dummyExpired() {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(published_date);
            calendar.add(Calendar.YEAR, 1);
            return calendar.getTime();
        }

    }

    public static class Region {
        public Long id;
        public String nama;

    }

}
