package models.QuestionAnswers.contracts;

import org.apache.commons.lang3.StringUtils;

public interface QuestionContract {

    String getQuestion();

    default String cleanContentFromTags(String value) {
        if (StringUtils.isEmpty(value)) {
            return value;
        }
        return value.replaceAll("\\<.*?\\>", "")
                .replaceAll("&nbsp;", " ");
    }

    default String cleanAndAbbreviateContent() {
        return cleanContentFromTags(getQuestion());
    }

}
