package models.QuestionAnswers;

import models.QuestionAnswers.contracts.QuestionContract;
import models.common.BaseModel;
import models.common.contracts.annotations.SearchAble;
import models.common.contracts.annotations.SoftDelete;
import play.db.jdbc.Table;

@Table(name = "question", schema = "public")
@SoftDelete
public class Question extends BaseModel implements QuestionContract {

    @SearchAble
    public String pertanyaan;

    public Integer status;

    public Question() {}

    public void setQuestionForm (QuestionForm model) {
        this.pertanyaan = model.pertanyaan;
        this.status = model.status;
    }

    @Override
    public String getQuestion() {
        return pertanyaan;
    }
}
