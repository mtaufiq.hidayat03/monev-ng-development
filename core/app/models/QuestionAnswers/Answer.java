package models.QuestionAnswers;

import models.common.BaseModel;
import permission.Kldi;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

@Table(name = "question_answer", schema = "public")
public class Answer extends BaseModel {

    private static final String TAG = "Answer";

    public Integer pertanyaan_id;
    public String jawaban;
    public String kldi_id;

    public Answer() {
    }

    public void setAnswerForm(AnswerForm model) {
        this.pertanyaan_id = Integer.parseInt(model.pertanyaanId);
        this.jawaban = model.jawaban;
        this.kldi_id = model.kldiId;
    }

    public String getNameKldi(String kldi) {
        Kldi model = Query.find("SELECT nama_instansi FROM kldi where id='" + kldi + "'", Kldi.class).first();
        return model.nama_instansi;
    }

    public String setFormattingDate(Timestamp date) {
        String newFormatDate = new SimpleDateFormat("dd MMMM yyyy").format(date);
        return newFormatDate;
    }
}
