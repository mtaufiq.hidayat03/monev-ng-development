package models.QuestionAnswers.pojos;

import models.QuestionAnswers.Answer;
import models.common.contracts.TablePagination;
import models.dashboard.SearchQuery;

import java.util.List;

public class AnswerResult implements TablePagination.PageAble<Answer> {
    private List<Answer> items;
    private final SearchQuery request;
    private int total = 0;
    private TablePagination.PageAble.Meta meta;

    public AnswerResult(SearchQuery request) {
        this.request = request;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setItems(List<Answer> collection) {
        this.items = collection;
    }

    @Override
    public List<Answer> getItems() {
        return items;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public TablePagination.ParamAble getParamAble() {
        return request;
    }

    @Override
    public Class<Answer> getReturnType() {
        return Answer.class;
    }

    @Override
    public void setMeta(TablePagination.PageAble.Meta meta) {
        this.meta = meta;
    }

    @Override
    public TablePagination.PageAble.Meta getMeta() {
        return meta;
    }
}
