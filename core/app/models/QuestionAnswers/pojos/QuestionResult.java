package models.QuestionAnswers.pojos;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import constants.generated.R;
import models.QuestionAnswers.Question;
import models.common.contracts.DataTablePagination;
import models.user.management.User;
import play.mvc.Router;
import repositories.user.management.UserRepository;
import utils.common.LogUtil;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static models.QuestionAnswers.QuestionStatus.DRAFT;
import static models.QuestionAnswers.QuestionStatus.PUBLISH;

public class QuestionResult implements DataTablePagination.PageAble<Question> {

    private List<Question> items;
    private final DataTablePagination.Request request;
    private int total = 0;
    private int totalItemInOnePage = 0;
    private final String[] columns = new String[]{
            "id", "pertanyaan", "created_by", "created_at", "status"
    };
    private final List<String> searchAbleFields;
    private static final UserRepository repo = new UserRepository();

    public QuestionResult(DataTablePagination.Request request) {
        this.request = request;
        List<String> fields = new ArrayList<>();
        fields.add("pertanyaan::TEXT");
        fields.add("status::TEXT");
        searchAbleFields = fields;
    }

    @Override
    public JsonObject getResultsAsJson() {
        JsonObject result = getResponseTemplate();
        JsonArray jsonArray = new JsonArray();
        if (getTotal() == 0) {
            result.add("data", jsonArray);
            return result;
        }
        int i = getNumberingStartFrom();
        for (Question question : getResults()) {
            JsonArray array = new JsonArray();
            User model = repo.getCreator(question.created_by);
            Map<String, Object> params = new HashMap<>();
            params.put("id", question.id);
            String deleteUrl = Router.reverse(R.route.questionanswers_questioncontroller_delete, params).url;
            String responUrl = Router.reverse(R.route.questionanswers_questioncontroller_responquestion, params).url;
            String responLabel = "Lihat Response";
            String addAction = "<li><a href='" + responUrl + "'>" + responLabel + "</a></li>";
            String addActionStatus = "";
            if (!question.status.equals(DRAFT.getQuestionStatusId())) {
                String deactivateLabel = "Atur Sebagai Draft";
                String deactivatedUrl = Router.reverse(R.route.questionanswers_questioncontroller_draft, params).url;
                addActionStatus += "<li class=\"divider\"></li>" +
                        "<li><a href='" + deactivatedUrl + "'>" + deactivateLabel + "</a></li>";
            }
            if (!question.status.equals(PUBLISH.getQuestionStatusId())) {
                String activateLabel = "Atur Sebagai Publish";
                String activatedUrl = Router.reverse(R.route.questionanswers_questioncontroller_publish, params).url;
                addActionStatus += "<li class=\"divider\"></li>" +
                        "<li><a href='" + activatedUrl + "'>" + activateLabel + "</a></li>";
            }
            array.add(i);
            array.add(question.pertanyaan);
            array.add(model.name);
            array.add(new SimpleDateFormat("dd-MM-yyyy").format(question.created_at));
            array.add(question.status.equals(PUBLISH.getQuestionStatusId()) ? "PUBLISH" : "DRAFT");
            array.add("<div class='btn-group'>" +
                    "<button type='button' class='btn btn-success " +
                    "dropdown-toggle' data-toggle='dropdown' aria-expanded='false'>" +
                    "Action <span class='sr-only'>Toggle Dropdown</span></button>" +
                    "<ul class='dropdown-menu' role='menu'>" +
                    addAction + addActionStatus +
                    "<li class=\"divider\"></li>" +
                    "<li><a href='" + deleteUrl + "' class='delete'>Delete</a></li>" +
                    "</ul></div>");
            jsonArray.add(array);
            i++;
        }
        result.add("data", jsonArray);
        LogUtil.debug(TAG, result);
        return result;
    }

    @Override
    public List<String> extractSearchAbleFields(Field[] fields) {
        return searchAbleFields;
    }

    @Override
    public void setResult(List<Question> items) {
        this.items = items;
    }

    @Override
    public List<Question> getResults() {
        return items;
    }

    @Override
    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public void setTotalItemInOnePage(int totalItem) {
        totalItemInOnePage = totalItem;
    }

    @Override
    public int getTotalItemInOnePage() {
        return totalItemInOnePage;
    }

    @Override
    public int getTotal() {
        return total;
    }

    @Override
    public String[] getColumns() {
        return columns;
    }

    @Override
    public DataTablePagination.Request getRequest() {
        return request;
    }

    @Override
    public Class<Question> getReturnType() {
        return Question.class;
    }
}
