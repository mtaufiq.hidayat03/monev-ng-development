package models.QuestionAnswers;

import models.common.contracts.FormContract;
import models.common.contracts.PublishingStatusContract;
import play.data.validation.Required;

public class QuestionForm implements FormContract,
        PublishingStatusContract {

    public static final String TAG = "QuestionForm";

    public String action = "create";
    public Long id;
    @Required
    public String pertanyaan;
    @Required
    public Integer status;

    @Override
    public String getAction() {
        return action;
    }

    public QuestionForm() {
    }

    public QuestionForm(Question model) {
        this.pertanyaan = model.pertanyaan;
        this.status = model.status;
    }

    @Override
    public Integer getStatus() {
        return status;
    }
}
