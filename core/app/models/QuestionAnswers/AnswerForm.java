package models.QuestionAnswers;

import models.common.contracts.FormContract;
import play.data.validation.Required;

public class AnswerForm implements FormContract {

    public static final String TAG = "AnswerForm";
    public String action = "create";
    public Long id;
    @Required
    public String jawaban;
    @Required
    public String kldiId;
    @Required
    public String pertanyaanId;

    @Override
    public String getAction() {
        return action;
    }

    public AnswerForm() {
    }

    public AnswerForm(Answer model) {
        this.jawaban = model.jawaban;
        this.kldiId = model.kldi_id;
        this.pertanyaanId = Integer.toString(model.pertanyaan_id);
    }
}
