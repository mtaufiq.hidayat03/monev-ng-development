package models.QuestionAnswers;

import play.db.jdbc.Enumerated;

import javax.persistence.EnumType;
import java.util.ArrayList;
import java.util.List;

@Enumerated(EnumType.ORDINAL)
public enum QuestionStatus {

    DRAFT(0,"DRAFT","PERTANYAAN TIDAK AKTIF DAN TIDAK DITAMPILKAN"),
    PUBLISH(2,"PUBLISH","PERTANYAAN AKTIF DAN DITAMPILKAN");

    public final Integer id;
    public final String name;
    public final String desc;

    QuestionStatus(Integer id, String name, String desc) {
        this.id = id;
        this.name = name;
        this.desc = desc;
    }

    public Integer getQuestionStatusId() {
        return id;
    }

    public static QuestionStatus fromId(int id) {
        switch (id) {
            case 0:
                return QuestionStatus.DRAFT;
            case 1:
                return QuestionStatus.PUBLISH;
            default:
                return null;
        }
    }
    public static List<QuestionStatus> getQuestionStatus() {
        List<QuestionStatus> myStatus = new ArrayList<>();
        myStatus.add(DRAFT);
        myStatus.add(PUBLISH);
        return myStatus;
    }
}
