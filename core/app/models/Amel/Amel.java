package models.Amel;

import play.db.jdbc.BaseTable;
import play.db.jdbc.Id;
import play.db.jdbc.Query;
import play.db.jdbc.Table;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

@Table(name = "pembayaran_amel", schema = "public")
public class Amel extends BaseTable implements Serializable {

    private static String BATCH_QUERY = "INSERT INTO pembayaran_amel " +
            "(kode_satker, jenis_pengadaan, pembayaran, tahun, created_at, source_repo) " +
            "VALUES (:kode_satker, :jenis_pengadaan, :pembayaran, :tahun, :created_at, :source_repo)";

    @Id
    public Long id;
    public String kode_satker;
    public String jenis_pengadaan;
    public Double pembayaran;
    public Integer tahun;
    public Integer source_repo;
    public Timestamp created_at;

    @Override
    protected void prePersist() {
        super.prePersist();
        id = Query.find("SELECT nextval('pembayaran_amel_id_seq'::regclass)", Long.class).first();

    }

    public Integer getSourceRepo() {
        return this.source_repo;
    }

    public String getKodeSatker() {
        return kode_satker;
    }

    public static void batchInsert(List data) {
        play.db.jdbc.Query.bindUpdateAll(new play.db.jdbc.QueryBuilder(BATCH_QUERY), data);
    }

}
