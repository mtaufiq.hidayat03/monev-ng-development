package models;

import models.common.ReadOnlyModel;
import org.apache.commons.lang3.StringUtils;
import play.db.jdbc.Table;
import utils.common.LogUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @author HanusaCloud on 12/14/2019 7:05 PM
 */
@Table(name = "blacklist_ikut_pengadaan", schema = "public")
public class BlacklistPenyediaIkutLelang extends ReadOnlyModel {

    public static final String TAG = "BlacklistPenyediaIkutLelang";

    public Long id_pengadaan;
    public Long rkn_id;
    public String nama_pengadaan;

    public static List<BlacklistPenyediaIkutLelang> getBy(String rknId) {
        LogUtil.debug(TAG, "get procurment participation by vendor: " + rknId);
        if (StringUtils.isEmpty(rknId) || rknId.equals("0")) {
            return new ArrayList<>();
        }
        final List<BlacklistPenyediaIkutLelang> results = BlacklistPenyediaIkutLelang.find(
                "rkn_id = ?", Long.valueOf(rknId)
        ).fetch();
        LogUtil.debug(TAG, "total: " + results.size());
        return results;
    }

}
