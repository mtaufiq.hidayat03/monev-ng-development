package repositories.core;

import models.Blacklist;
import models.common.contracts.DataTableRequest;
import play.db.jdbc.Query;
import utils.common.LogUtil;

import java.util.List;

public class BlacklistRepository {
    public static final String TAG = "BlacklistRepository";
    private static final BlacklistRepository repository = new BlacklistRepository();

    public static BlacklistRepository getInstance() {
        return repository;
    }

    public List<Blacklist> list(DataTableRequest dataTableRequest) {
        Integer start = dataTableRequest.getStart();
        Integer length = dataTableRequest.getLength();
        return Blacklist.find("1=1 order by id desc LIMIT "+length+" OFFSET "+start).fetch();
    }

    public List<Blacklist> list() {
        return Blacklist.findAll();
    }

    public List<Blacklist> paginate(Long offset, Integer limit) {
        LogUtil.debug(TAG, "offset: " + offset + " limit: " + limit);
        return Blacklist.find("1=1 ORDER BY id ASC LIMIT ? OFFSET ?", limit, offset).fetch();
    }

    public Long count() {
        return Query.count("SELECT count(*) FROM public.blacklist");
    }

    public Blacklist get(Long id) {
        return Blacklist.find("id =?", id).first();
    }
}
