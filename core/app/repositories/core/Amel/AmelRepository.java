package repositories.core.Amel;

import controllers.common.BaseController;
import models.Amel.Amel;
import play.db.jdbc.Query;
import utils.common.IdUtils;
import utils.common.LogUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class AmelRepository extends BaseController {

    public static final String TAG = "AmelRepository";
    public static final AmelRepository instance = new AmelRepository();

    public static AmelRepository getInstance() {
        return instance;
    }

    public List<Amel> getDataAmel() {
        return Amel.find("1=1").fetch();
    }

    public Boolean deleteBy(Set<String> satkers) {
        LogUtil.debug(TAG, "delete by satkers");
        List<String> ids = new ArrayList<>(satkers);
        Query.update("DELETE FROM pembayaran_amel WHERE kode_satker IN ("+ IdUtils.turnIdsToIns(ids) + ")");
        return true;
    }

    public Boolean isDataAmelExist(String kode_satker, String jenis_pengadaan, Integer tahun) {
        Integer count = (int) Query.count("SELECT count(*) FROM pembayaran_amel WHERE " +
                "kode_satker='" + kode_satker + "' AND jenis_pengadaan='" + jenis_pengadaan + "' AND tahun='" + tahun + "'");
        if (count != 0) {
            return true;
        }
        return false;
    }
}
