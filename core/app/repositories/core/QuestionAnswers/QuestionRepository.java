package repositories.core.QuestionAnswers;

import controllers.common.BaseController;
import models.QuestionAnswers.Question;
import models.QuestionAnswers.pojos.QuestionResult;
import models.common.contracts.DataTablePagination;
import models.user.management.contracts.UserContract;
import play.db.jdbc.Query;
import utils.common.LogUtil;

import java.util.List;

public class QuestionRepository extends BaseController {

    public static final String TAG = "QuestionRepository";
    public static final QuestionRepository instance = new QuestionRepository();

    public static QuestionRepository getInstance() {
        return instance;
    }

    public Question getQuestion(Long id) {
        final Question model = Question.find("id =? AND deleted_at IS NULL", id).first();
        return model;
    }

    public Integer countNotAnsweredQuestions(
            boolean ownershipCheck,
            UserContract user
    ) {
        return (int) Query.count("SELECT count(*) FROM question where id NOT IN (SELECT pertanyaan_id FROM " +
                "question_answer WHERE kldi_id='" + user.getKldiId() + "') AND status='2' AND deleted_at is NULL");
    }

    public List<Question> getNotAnsweredQuestions(
            boolean ownershipCheck,
            UserContract user
    ) {
        return Query.find("SELECT * FROM question where id NOT IN (SELECT pertanyaan_id FROM question_answer WHERE " +
                "kldi_id='" + user.getKldiId() + "') AND status='2' AND deleted_at is NULL", Question.class).fetch();
    }

    public List<Question> getNotAnsweredQuestions(
            boolean ownershipCheck,
            UserContract user,
            Integer limit,
            Integer offset
    ) {
        return Query.find("SELECT * FROM question where id NOT IN (SELECT pertanyaan_id FROM question_answer WHERE " +
                "kldi_id='" + user.getKldiId() + "') AND status='2' AND deleted_at is NULL LIMIT "+limit+" OFFSET "+offset, Question.class).fetch();
    }

    public List<Question> listNotAnsweredQuestions(
            boolean ownershipCheck,
            UserContract user
    ) {
        return Query.find("SELECT * FROM question where id NOT IN (SELECT pertanyaan_id FROM question_answer WHERE " +
                "kldi_id='" + user.getKldiId() + "') AND status='2'", Question.class).fetch();
    }

    public QuestionResult getQuestionResult(DataTablePagination.Request request) {
        QuestionResult questionResult = new QuestionResult(request);
        questionResult.executeQuery();
        LogUtil.debug(TAG, questionResult);
        return questionResult;
    }

}
