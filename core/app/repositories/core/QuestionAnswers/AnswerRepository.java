package repositories.core.QuestionAnswers;

import models.QuestionAnswers.Answer;
import models.QuestionAnswers.pojos.AnswerResult;
import models.dashboard.SearchQuery;

public class AnswerRepository {

    public static final String TAG = "AnswerRepository";
    public static final AnswerRepository instance = new AnswerRepository();

    public static AnswerRepository getInstance() {
        return instance;
    }

    public Answer getAnswer(Long id) {
        final Answer model = Answer.find("id =? AND deleted_at IS NULL", id).first();
        return model;
    }

    public AnswerResult result(SearchQuery model, Long id) {
        AnswerResult result = new AnswerResult(model);
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT a.jawaban, a.kldi_id, a.created_at FROM public.question_answer as a");
        sb.append(" WHERE a.pertanyaan_id = '").append(id).append("'");
        sb.append(" ORDER BY a.id ASC ");
        result.executeQuery(sb.toString());
        return result;
    }

}
