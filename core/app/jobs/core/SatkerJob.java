package jobs.core;

import play.jobs.Job;
import play.jobs.On;
import services.core.SatkerService;

/**
 * @author HanusaCloud on 12/12/2019 1:34 PM
 */
@On("0 0 23 1,14 * ?")
public class SatkerJob extends Job {

    @Override
    public void doJob() throws Exception {
        super.doJob();
        SatkerService.syncSatker();
    }
}
