package controllers.Amel;

import com.google.gson.reflect.TypeToken;
import models.Amel.Amel;
import org.apache.commons.io.IOUtils;
import play.mvc.Controller;
import play.mvc.Http;
import repositories.core.Amel.AmelRepository;
import utils.common.JsonUtil;

import java.io.IOException;
import java.io.StringWriter;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class AmelApiController extends Controller {

    public static final String TAG = "AmelApiController";

    private static AmelRepository amelRepository = AmelRepository.getInstance();

    public void store() throws IOException {
        Http.Header jsonHeader;
        if ((jsonHeader = request.headers.get("content-type")) == null
                || !jsonHeader.value().equals("application/json")) {
            renderJSON("{\"status\":false, \"message\":\"non json content\"}");
        }
        StringWriter writer = new StringWriter();
        IOUtils.copy(request.body, writer, "UTF-8");
        Type listType = new TypeToken<ArrayList<Amel>>() {
        }.getType();
        final String jsonString = writer.toString();
        List<Amel> amels = JsonUtil.fromJson(jsonString, listType);
        if (amels.isEmpty()) {
            renderJSON("{\"status\":false, \"message\":\"content doesn't exist\"}");
        }
        Set<String> sources = amels.stream()
                .map(Amel::getKodeSatker)
                .collect(Collectors.toSet());
        if (!sources.isEmpty()) {
            amelRepository.deleteBy(sources);
        }
        for (Amel amel : amels) {
            amel.created_at = new Timestamp(System.currentTimeMillis());
        }
        Amel.batchInsert(amels);
        renderJSON("{\"status\":true, \"message\":\"Success saving or updating data\"}");
    }
}
