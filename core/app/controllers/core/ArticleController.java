package controllers.core;

import com.google.gson.JsonArray;
import constants.generated.R;
import controllers.common.HtmlBaseController;
import models.cms.Content;
import play.mvc.Router;
import repositories.cms.ContentRepository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ArticleController extends HtmlBaseController {

    public JsonArray generateJson(Integer offset, Integer limit) {
        ContentRepository repo = new ContentRepository();
        List<Content> contents = repo.getListArtikel(offset, limit, getCurrentUser());
        JsonArray jsonArray = new JsonArray();
        int i =1;
        for (Content content : contents) {
            JsonArray contentArray = new JsonArray();
            Map<String,Object> params =  new HashMap<>();
            params.put("id",content.id);
            String image = content.featured_image;
            image = "\""+image+"\"";
            String title = content.title;
            title = title.replaceAll("\\<.*?\\>", "");
            if (title.length() > 103) {
                title = title.substring(0,103);
            }
            String str = content.content;
            str = str.replaceAll("\\<.*?\\>", "");
            if (str.length() > 500) {
                str = str.substring(0, 500) + " .........";
            }
            DateFormat dateFormat = new SimpleDateFormat("dd MMMM YYY");
            String dateCreated = dateFormat.format(content.created_at);
            String detailUrl = Router.reverse(R.route.cms_publiccontentcontroller_view, params).url;
            contentArray.add("<div class='column ui card mar-l-rem'>" +
                            "<div class='ui grid'>" +
                            "<div class='four wide column padding-zero img-list' style='background-image: url("+image+")'>"+
                            "</div>"+
                            "<div class='twelve wide column left aligned'>"+
                            "<a href='"+detailUrl+"'>"+
                            "<div class='date'>"+ dateCreated+"</div>"+
                            "<div class='title-article'>"+title+"</div>"+
                            "<div class='desc'>"+str+"</div>"+
                            "</a>"+
                            "</div>"+
                            "</div>"+
                            "</div>");
            jsonArray.add(contentArray);
            i++;
        }
        return jsonArray;
    }

    public static void loadArticle(Integer offset, Integer limit) {
        renderJSON(new ArticleController().generateJson(offset, limit));
    }
}
