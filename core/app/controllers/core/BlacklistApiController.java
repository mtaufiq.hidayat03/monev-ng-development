package controllers.core;

import controllers.common.ApiBaseController;
import models.common.contracts.DataTableRequest;

/**
 * @author HanusaCloud on 9/24/2019 4:18 PM
 */
public class BlacklistApiController extends ApiBaseController {

    public static void json() {
        renderJSON(blacklistService.generateJson(new DataTableRequest(request)));
    }
}
