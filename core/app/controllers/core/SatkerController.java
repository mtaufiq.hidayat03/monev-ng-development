package controllers.core;

import controllers.common.HtmlBaseController;
import jobs.core.SatkerJob;
import utils.common.LogUtil;

/**
 * @author HanusaCloud on 12/12/2019 1:39 PM
 */
public class SatkerController extends HtmlBaseController {

    public static final String TAG = "SatkerController";

    public static void forceSync() {
        if (getCurrentUser() == null || !getCurrentUser().user.isSuperAdmin()) {
            LogUtil.debug(TAG, "not the right user");
            LandingController.index();
        }
        LogUtil.debug(TAG, "do force sync");
        new SatkerJob().now();
        ok();
    }

}
