package controllers.core;

import com.google.gson.JsonArray;
import constants.generated.R;
import controllers.common.HtmlBaseController;
import models.cms.Content;
import models.dashboard.RekapPengadaanNasional;
import models.dashboard.SearchQuery;
import models.user.management.User;
import models.user.management.contracts.UserContract;
import permission.Kldi;
import play.mvc.Router;
import procurementprofile.ProfileRepository;
import procurementprofile.models.ProcurementProfile;
import repositories.cms.ContentRepository;
import repositories.searchcontent.ArticleRepository;
import repositories.searchcontent.KeywordRecorderRepository;
import repositories.user.management.KldiRepository;
import repositories.user.management.UserRepository;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LandingController extends HtmlBaseController {

    public static final String TAG = "LandingController";

    private static final KeywordRecorderRepository keywordRepository = KeywordRecorderRepository.getInstance();

    public static void index() {
        String year = params.get("year");
        UserContract user = getCurrentUser() != null ? getCurrentUser().user : null;
        List<ProcurementProfile> profiles = ProfileRepository.getInstance().getPublishedProfiles();
        renderArgs.put("contents", ArticleRepository.getInstance().getLatest(user));
        renderArgs.put("basicCertificateMeta", adjustmentRepository.getSummaryPemegangSertifikatDasar());
        renderArgs.put("pbjMeta", adjustmentRepository.getSummaryJabfungPpbj());
        renderArgs.put("pbjcertificateMeta", adjustmentRepository.getSummaryPemegangSertifikatKompetensi());
        renderArgs.put("pbjoccupationalMeta", adjustmentRepository.getSummaryPemegangSertifikatOkupasi());
        renderArgs.put("chartable", rekapPengadaanRepository.trend10Tahun());
        RekapPengadaanNasional rekapPengadaanNasional = rekapPengadaanRepository.rekapNasional();
        if (year != null) {
            rekapPengadaanNasional = rekapPengadaanRepository.rekapNasional(year);
        }
        renderArgs.put("rekapPengadaanNasional", rekapPengadaanNasional);
        params.put("tahun", year != null ? year : String.valueOf(rekapPengadaanNasional != null ? rekapPengadaanNasional.tahun : Year.now().getValue()));
        renderArgs.put("yearNow", year != null ? year : String.valueOf(rekapPengadaanNasional != null ? rekapPengadaanNasional.tahun : Year.now().getValue()));
        renderArgs.put("listYears", rekapPengadaanRepository.listYears());
        renderArgs.put("rekapPengadaan", year != null ? rekapPengadaanRepository.listNasionalDetail(new SearchQuery(params), year) :
                rekapPengadaanRepository.listNasionalDetail(new SearchQuery(params), String.valueOf(Year.now().getValue())));
        renderArgs.put("rekapUkpbj", adjustmentRepository.getSummaryUkpbj());
        renderArgs.put("rekapAdvokasi", adjustmentRepository.getSummaryAdvokasi());
        renderArgs.put("rekapLpse", adjustmentRepository.getSummaryLpse());
        renderArgs.put("dataPenyedia", adjustmentRepository.getSummaryDataPenyedia());
        renderArgs.put("inovasiPengadaan", adjustmentRepository.getSummaryInovasiPengadaan());
        renderArgs.put("umkm", adjustmentRepository.getSummaryPdnPeranSertaUsahaKecil());
        renderArgs.put("mostSearchedKeywords", keywordRepository.getMostSearchedKeywords());
        renderTemplate(R.view.core_landing_landing_page, profiles);
    }

    public static void report(Long id) {
        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        renderArgs.put("url", Router.reverse(R.route.core_dummycontroller_downloadpdfprofile, map).url);
        renderTemplate(R.view.core_landing_report);
    }

    public static void loadMore(Integer offset, Integer limit) {
        renderJSON(new LandingController().generateJson(offset, limit));
    }

    public JsonArray generateJson(Integer offset, Integer limit) {
        ContentRepository repo = new ContentRepository();
        List<Content> contents = repo.getListArtikel(offset, limit, getCurrentUser());
        JsonArray jsonArray = new JsonArray();
        int i = 1;
        for (Content content : contents) {
            JsonArray contentArray = new JsonArray();
            Map<String, Object> params = new HashMap<>();
            params.put("id", content.id);
            String image = content.featured_image;
            image = "\"" + image + "\"";
            String title = content.title;
            title = title.replaceAll("\\<.*?\\>", "");
            if (title.length() > 103) {
                title = title.substring(0, 103);
            }
            String str = content.content;
            str = str.replaceAll("\\<.*?\\>", "");
            if (str.length() > 103) {
                str = str.substring(0, 103) + " .........";
            }
            DateFormat dateFormat = new SimpleDateFormat("MMMM dd, YYY");
            String dateCreated = dateFormat.format(content.created_at);
            String detailUrl = Router.reverse(R.route.cms_publiccontentcontroller_view, params).url;
            contentArray.add("<div class='column'>" +
                    "<div class='ui card width-full'>" +
                    // "<div class='image'>" +
                    // "<img src='"+image+"' class='ui centered image'>" +
                    // "</div>" +
                    "<div class='image-card' style='background-image: url(" + image + "),url(/public/images/no-result.png);'>" +
                    "</div>" +
                    "<div class='content left aligned'>" +
                    "<div class='title'><a href='" + detailUrl + "'>" +
                    title +
                    "<div class='meta'>" +
                    "<span class='date'>" + dateCreated + "</span>" +
                    "</div>" +
                    "<div class='description'>" +
                    str +
                    "</div>" +
                    "</div>" +
                    "</div>" +
                    "</div>");
            jsonArray.add(contentArray);
            i++;
        }
        return jsonArray;
    }

    public static void modalLogin() {
        renderTemplate(R.view.core_landing_modal_login);
    }

    public static void modalDaftar() {
        renderTemplate(R.view.core_landing_modal_daftar);
    }

    public static void modalLogout() {
        renderTemplate(R.view.core_landing_modal_logout);
    }

    public static void modalDaftarAdminkl(Long id) {
        User model = new UserRepository().getUser(id);
        KldiRepository repo = new KldiRepository();
        List<Kldi> daftarkl = repo.getListKl();
        renderArgs.put("user", model);
        renderArgs.put("daftarkl", daftarkl);
        renderTemplate(R.view.core_landing_modal_admin_kl);
    }

    public static void email() {
        renderTemplate(R.view.core_landing_email);
    }

}
