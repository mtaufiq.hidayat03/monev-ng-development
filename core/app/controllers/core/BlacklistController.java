package controllers.core;

import constants.generated.R;
import controllers.common.HtmlBaseController;
import models.Blacklist;
import models.common.contracts.DataTableRequest;

import java.util.List;

public class BlacklistController extends HtmlBaseController {

    public static void index() {
        request.params.put("length", "16");
        DataTableRequest dataTableRequest = new DataTableRequest(request);
        List<Blacklist> blacklists = blacklistRepository.list(dataTableRequest);
        renderTemplate(R.view.blacklist_index, blacklists);
    }
}
