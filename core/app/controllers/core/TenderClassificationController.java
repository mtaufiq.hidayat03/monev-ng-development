package controllers.core;

import com.google.gson.JsonObject;
import constants.generated.R;
import controllers.user.management.BaseAdminController;
import models.Configuration;
import models.common.ServiceResult;
import org.apache.commons.lang3.StringUtils;
import permission.Access;
import permission.Can;
import play.mvc.Before;
import services.core.TenderClassificationService;
import utils.common.LogUtil;

/**
 * @author HanusaCloud on 11/18/2019 10:26 AM
 */
public class TenderClassificationController extends BaseAdminController {

    private static final TenderClassificationService service = TenderClassificationService.getInstance();

    public static final String TAG = "TenderClassificationController";

    @Before
    public static void checkForPermission() {
        if (!(getCurrentUser() == null
                || getCurrentUser().user.isSuperAdmin()
                || (getCurrentUser().user.isAdminKl() && Configuration.isTenderForKlpdEnabled()))) {
            notFound();
        }
    }

    @Can({Access.SUPER_ADMIN})
    public static void index() {
        LogUtil.debug(TAG, request);
        String search;
        if ((search = params.get("args", String.class)) == null) {
            LogUtil.debug(TAG, "empty or not numeric");
            renderJSON(new ServiceResult("Oops.. no arguments"));
        }
        renderJSON(service.classify(search));
    }

    public static void search() {
        String search;
        if ((search = params.get("args", String.class)) == null) {
            LogUtil.debug(TAG, "empty or not numeric");
        }
        JsonObject jsonObject = service.classify(search);
        if (!jsonObject.get("status").getAsBoolean()) {
            flashError(jsonObject.get("message").getAsString());
        }
        renderArgs.put("search", search);
        renderArgs.put("data", jsonObject);
        renderArgs.put("showConfig", getCurrentUser().user.isSuperAdmin());
        renderArgs.put("enabledForKl", Configuration.isTenderForKlpdEnabled());
        renderTemplate(R.view.core_classification_lelang);
    }

    @Can({Access.SUPER_ADMIN})
    public static void enabledConfig(String rn, String enabledConfig) {
        if (StringUtils.isEmpty(enabledConfig)) {
            renderJSON(new ServiceResult("Missing Params!"));
        }
        Configuration model = Configuration.getTenderClassificationConfig();
        if (model == null) {
            renderJSON(new ServiceResult("Missing Params!"));
        }
        model.config_value = String.valueOf(enabledConfig.equalsIgnoreCase("enabled"));
        model.saveModel();
        renderJSON(new ServiceResult<String>(true, "success", rn));
    }

}
