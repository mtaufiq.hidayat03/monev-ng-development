package controllers.core;

import controllers.common.BaseController;
import models.cms.ContentFolder;
import play.mvc.After;
import procurementprofile.models.ProcurementProfile;
import org.apache.commons.io.FileUtils;
import org.apache.http.client.utils.URIBuilder;
import play.libs.WS;
import play.mvc.Http;
import utils.common.LogUtil;
import utils.core.JasperUtils;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.StandardCopyOption;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class DummyController extends BaseController {

    public static final String TAG = "DummyController";

    public static void downloadPdf(String report) {
        download("/reports/"+report+".pdf", "");
    }

    @After
    public static void injectHeaders() {
        response.setHeader("Content-Security-Policy",
                "default-src 'self' 'unsafe-inline' 'unsafe-eval' ");
        response.setHeader("X-Frame-Options","deny");
        response.setHeader("Referrer-Policy","strict-origin-when-cross-origin");
    }

    public static void downloadPdfProfile(Long id) {
        notFoundIfNull(id);
        final ProcurementProfile profile = ProcurementProfile.findById(id);
        notFoundIfNull(profile);
        final Integer version = profile.getLatestVersion();
        final String dirPath = ContentFolder.REPORT_CACHE + "/" + id.toString()
                + "/" + version.toString()
                + (profile.isPublished() ? "" : "/preview");
        final String filePath = dirPath + "/" + "report.pdf";
        try {
            final File dir = new File(dirPath);
            if (!dir.exists()) {
                FileUtils.forceMkdir(dir);
            }
            final File reportFile = new File(filePath);
            final long diff = reportFile.lastModified() - new Date().getTime();
            LogUtil.debug(TAG, "time diffs: " + diff);
            final long days = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
            LogUtil.debug(TAG, "in day: " + days);
            if (days <= 1 && reportFile.exists()) {
                LogUtil.debug(TAG, "cache exists and valid");
                renderBinary(
                        new FileInputStream(reportFile),
                        "Profil-Pengadaan.pdf",
                        "application/pdf",
                        false
                );
            }
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }
        LogUtil.debug(TAG, "cache not exist, download from jasper server");
        String unit = "";
        try {
            URIBuilder builder = new URIBuilder(JasperUtils.PDF_REPORT_UNIT);
            builder.addParameter("PROCUREMENT_PROFILE_ID", id.toString());
            builder.addParameter("PROCUREMENT_VERSION", version.toString());
            unit = builder.build().toString();
        } catch (Exception e) {
            LogUtil.error(TAG, e);
        }
        download(unit, filePath);
    }

    public static void download(String report, String cache) {
        LogUtil.debug(TAG, "download pdf: " + report);
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", "Basic " + Base64.getEncoder()
                .encodeToString((JasperUtils.USERNAME + ":" + JasperUtils.PASSWORD).getBytes()));
        try (WS.WSRequest request = WS.url(report).headers(headers)) {
            WS.HttpResponse responsePdf = request.get();
            LogUtil.debug(TAG, responsePdf.getHeaders());
            LogUtil.debug(TAG, "http status: " + responsePdf.getStatus());
            if (responsePdf.getStatus() != Http.StatusCode.OK) {
                notFound("Report file not found!");
            }
            java.nio.file.Files.copy(
                    responsePdf.getStream(),
                    new File(cache).toPath(),
                    StandardCopyOption.REPLACE_EXISTING);
            renderBinary(
                    responsePdf.getStream(),
                    "Profil-Pengadaan.pdf",
                    "application/pdf",
                    false
            );
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            LandingController.index();
        }
    }
}
