package controllers.QuestionAnswers;

import constants.generated.R;
import controllers.common.ApiBaseController;
import models.QuestionAnswers.Answer;
import models.QuestionAnswers.AnswerForm;
import models.common.ServiceResult;
import models.common.contracts.DataTableRequest;
import models.user.management.contracts.UserContract;
import permission.Can;
import play.mvc.Router;
import services.core.QuestionService;
import utils.common.LogUtil;

import static permission.Access.ADMIN_KL;
import static permission.Access.SUPER_ADMIN;

public class QuestionApiController extends ApiBaseController {

    public static final String TAG = "QuestionApiController";

    @Can({SUPER_ADMIN})
    public static void questionJson() {
        DataTableRequest dataTableRequest = new DataTableRequest(request);
        renderJSON(questionRepository.getQuestionResult(dataTableRequest).getResultsAsJson());
    }

    @Can({ADMIN_KL})
    public static void storeAnswerQuestion(AnswerForm model) throws Exception {
        final UserContract user = getCurrentUser().user;
        String[] pertanyaan = model.pertanyaanId.toString().split(",");
        String[] jawaban = model.jawaban.split(",");
        try {
            int notEmpty = 0;
            for (int i = 0; i < pertanyaan.length; i++) {
                if (!jawaban[i].isEmpty() && !jawaban[i].equals(" ")) {
                    Answer answer = new Answer();
                    model.jawaban = jawaban[i];
                    model.pertanyaanId = pertanyaan[i];
                    model.kldiId = user.getKldiId();
                    answer.setAnswerForm(model);
                    answer.saveModel();
                    notEmpty++;

                }
            }
            if (notEmpty == 0) {
                renderJSON(new ServiceResult(false, R.translation.answer_at_least_one_question).toJson());
            }
            if (notEmpty > 0) {
                String url = Router.reverse(R.route.user_management_usercontroller_index).url;
                renderJSON(new ServiceResult(true, R.translation.answer_is_created, url).toJson());
            }
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            renderJSON(new ServiceResult(false, R.translation.answer_cannot_created).toJson());
        }
    }
}
