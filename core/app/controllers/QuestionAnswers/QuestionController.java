package controllers.QuestionAnswers;

import constants.generated.R;
import controllers.user.management.BaseAdminController;
import models.QuestionAnswers.*;
import models.dashboard.SearchQuery;
import permission.Can;
import play.i18n.Messages;
import repositories.core.QuestionAnswers.AnswerRepository;
import repositories.core.QuestionAnswers.QuestionRepository;
import utils.common.LogUtil;

import static permission.Access.ADMIN_KL;
import static permission.Access.SUPER_ADMIN;

public class QuestionController extends BaseAdminController {

    public static final String TAG = "QuestionController";

    private static final AnswerRepository repo = AnswerRepository.getInstance();

    @Can({SUPER_ADMIN})
    public static void index() {
        renderTemplate(R.view.question_index);
    }

    @Can({SUPER_ADMIN})
    public static void inputQuestion() {
        renderArgs.put("model", new QuestionForm());
        renderArgs.put("action", "create");
        renderTemplate(R.view.question_input);
    }

    @Can({SUPER_ADMIN})
    public static void responQuestion(Long id) {
        renderArgs.put("model", questionRepository.getQuestion(id));
        renderArgs.put("result", repo.result(new SearchQuery(params), id));
        LogUtil.debug(TAG, repo.result(new SearchQuery(params), id));
        renderTemplate(R.view.question_respon);
    }

    private static void reRenderForm(QuestionForm model) {
        renderArgs.put("model", new QuestionForm());
        renderArgs.put("action", "create");
        renderTemplate(R.view.question_input);
    }

    @Can({SUPER_ADMIN})
    public static void storeQuestion(QuestionForm model) throws Exception {
        checkAuthenticity();
        validation.valid(model);
        if (validation.hasErrors()) {
            params.flash();
            flashError(R.translation.form_re_check);
            reRenderForm(model);
        }
        try {
            questionService.store(model);
            String message = Messages.get(R.translation.question_is_created);
            flash.success(message);
        } catch (Exception e) {
            LogUtil.error(TAG, e);
            flashError(R.translation.question_cannot_created);
        }
        index();
    }

    @Can({SUPER_ADMIN})
    public static void draft(Long id) {
        Question model = questionRepository.getQuestion(id);
        notFoundIfNull(model);
        model.status = QuestionStatus.DRAFT.getQuestionStatusId();
        model.saveModel();
        index();
    }

    @Can({SUPER_ADMIN})
    public static void publish(Long id) {
        Question model = questionRepository.getQuestion(id);
        notFoundIfNull(model);
        model.status = QuestionStatus.PUBLISH.getQuestionStatusId();
        model.saveModel();
        index();
    }

    public static void delete(Long id) {
        final boolean result = questionService.deleteQuestion(id);
        if (result) {
            flash.success(R.translation.question_is_deleted);
        } else {
            flash.error(R.translation.question_cannot_deleted);
        }
        index();
    }

    @Can({ADMIN_KL})
    public static void inputAnswer() {
        renderArgs.put("questions", new QuestionRepository().getNotAnsweredQuestions(true, getCurrentUser().user));
        renderArgs.put("action", "create");
        renderTemplate(R.view.question_input_answer);
    }
}
