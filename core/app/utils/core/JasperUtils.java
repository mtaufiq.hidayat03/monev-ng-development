package utils.core;

import play.Play;

/**
 * @author HanusaCloud on 11/19/2019 3:44 PM
 */
public class JasperUtils {

    public static final String TAG = "JasperUtils";

    public static final String HOST = Play.configuration.getProperty("jasper.url");
    public static final String USERNAME = Play.configuration.getProperty("jasper.username");
    public static final String PASSWORD = Play.configuration.getProperty("jasper.password");
    public static final String REPORT_UNIT = Play.configuration.getProperty("jasper.reportunit");
    public static final String PDF_REPORT_UNIT = HOST + REPORT_UNIT + ".pdf";


}
