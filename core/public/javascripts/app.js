function formatCurrenciesJuta(number) {
    if (number == null)
        return "";
    var hasil = "";
    var sufix = "";
    if (number != null) {
        // double juta = num / 1E6;
        var milyar = number / 1E9;
        var trilyun = number / 1E12;
        // pakai juta atau milyar?
        if (milyar < 1) {
            sufix = " Jt";
            number /= 1E6;
        } else if (milyar >= 1 && trilyun < 1) {
            sufix = " M";
            number /= 1E9;
        } else {
            sufix = " T";
            number /= 1E12;
        }
        hasil = number;
    }
    return hasil.toFixed(2) + sufix;

}

function formatNumberIndonesia(number) {
    console.log(new Number(number).toLocaleString("id-ID"));
    return new Number(number).toLocaleString("id-ID");

}