
    function getHashes() {
        let location = window.location.toString()
        return location.substring(location.indexOf('#')).replace('#', '').split('&')
    }

    function updateLibOnLocation(id, keyword) {
        let lastValue = getLibFromLocation(keyword)
        if (lastValue > 0) {
            window.location = window.location.toString().replace(keyword + '\=' + lastValue, keyword + '\=' + id)
        } else {
            window.location += '#' + keyword + '=' + id
        }
    }

    function getLibFromLocation(keyword) {
        let lib = getHashes().filter(e => e.includes(keyword)).map(e => {
            let result = e.split('=')
            return result.length > 1 ? (isNaN(result[1]) ? 0 : result[1]) : 0
        })
        return lib.length > 0 ? lib[0] : 0
    }