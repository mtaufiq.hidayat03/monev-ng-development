$('.panduan').html("<b>PANDUAN :</b><br/>"+$('.panduan').val());
let tokenUrl = $('#token-url').val()
let generalErrorMessage = $('#general-error-message').val()
let busyGif = $('#busy-gif').val()
let origin = $('#origin').val()
let showUrl = $('#show-url').val()
let adjusted = $('#adjusted').val()

function adjustIframes()
{
  $('iframe').each(function(){
    var
    $this       = $(this),
    proportion  = $this.data( 'proportion' ),
    w           = $this.attr('width'),
    actual_w    = $this.width();

    if ( ! proportion )
    {
        proportion = $this.attr('height') / w;
        $this.data( 'proportion', proportion );
    }

    if ( actual_w != w )
    {
        $this.css( 'height', Math.round( actual_w * proportion ) + 'px' );
        $this.css( 'width', Math.round($(window).width()) + 'px' );
    }
  });
}
if (adjusted) {
    $(window).on('resize load', adjustIframes);
} else {
    $('.preview-tableau').height("auto").width("100%").show();
}
function blockPageUI() {
    $.blockUI({
        message: '<img src="' + busyGif + '" width="50" height="50" /> Opening Visualization Tableau ....',
        baseZ: 2000 ,
        css: {
            backgroundColor: '#00000000',
            color: '#fff', border: "0px",
            fontSize:'30px'},
            overlayCSS: { opacity:0.8 }});
}
function unblockPageUI() {
    $.unblockUI();
}
function blockPreview() {
    $('.preview-tableau').block({
        message: '<img src="' + busyGif + '" width="30" height="30" /> Please Wait ....',
        baseZ: 2000 ,
        centerY: false,
        css: {
            backgroundColor: '#00000000',
            color: '#fff',
            border: "0px", fontSize:'30px', top: '50px'},overlayCSS: { opacity:1 }});
}
function unblockPreview() {
    $('.preview-tableau').unblock();
}

function getLocation(href) {
    var match = href.match(/^(?:(https?\:)\/\/)?(([^:\/?#]*)(?:\:([0-9]+))?)([\/]{0,1}[^?#]*)(\?[^#]*|)(#.*|)$/);
    return match && {
        href: href,
        protocol: match[1],
        host: match[2],
        hostname: match[3],
        port: match[4],
        pathname: match[5],
        search: match[6],
        hash: match[7]
    }
}

$(document).ready(function() {
    var dataUrl = $('.tokenized').val();
    $('.preview-tableau').show();
    var iframe = document.createElement('iframe');
    if (showUrl) {
        let original = $('.original').val()
        let url = "<a href='"+ original +"' target='_blank' class='openLink' id='openLink'>Lihat Visualisasi</a>"
        $('.tampil').html(url)
    }
    function request(data) {
        var formData = new FormData()
        formData.append('url', data.url)
        $.ajax({
            xhr: function() {
                var browser = navigator.appName;
                if(browser == "Microsoft Internet Explorer"){
                    var xhr = new window.ActiveXObject("Microsoft.XMLHTTP");
                } else {
                    var xhr = new window.XMLHttpRequest();
                }
                xhr.upload.addEventListener("progress", function(evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = (evt.loaded / evt.total) * 100;
                    }
                }, false);
                return xhr;
            },
            type: "POST",
            cache: false,
            processData: false,
            contentType: false,
            url: data.tokenUrl,
            data: formData,
            beforeSend: data.beforeSend,
            success: data.success,
            error: data.error
            })
    }
    iframe.height = $(window).height();
    iframe.width = $(window).width();

    iframe.setAttribute('allowFullScreen', '');
    iframe.setAttribute('frameBorder', '0');
    iframe.setAttribute('sc', '0');

    $('.preview-tableau').append(iframe);
    $(window).on('resize load',adjustIframes());
    $('iframe').attr('src', dataUrl)

    $('#openLink').on('click', function(e) {
        e.preventDefault();
        request({
            url: dataUrl,
            tokenUrl: tokenUrl,
            beforeSend: function(){blockPageUI();},
            success: function(result){
                if (result.status) {
                    window.open(result.url, '_blank');
                }
            },
            complete: function(){unblockPageUI();},
            error: function(){
                $('.alert-modal')
                    .addClass('alert-danger')
                    .removeClass('alert-success')
                    .removeClass('hidden').html(generalErrorMessage);
            }
        });
    });

});
$(function() {
    
});